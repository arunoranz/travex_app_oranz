package org.technomobs.travex.GooglePlus;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import org.json.JSONObject;
import org.technomobs.travex.Activity.ProfileActivity;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.Controller.Interface.GetGoogleLoginResponse;
import org.technomobs.travex.Controller.JsonObjectMaker;
import org.technomobs.travex.Controller.NetworkManager;
import org.technomobs.travex.Controller.NetworkOptions;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.Utillity.ListCreatorForNameValuePairRequest;
import org.technomobs.travex.Utillity.LogUtility;

import java.util.ArrayList;

/**
 * Created by technomobs on 2/3/16.
 */
public class GooglePlusLogin implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, GetGoogleLoginResponse {
    public static final String TAG = GooglePlusLogin.class.getSimpleName();

    Context mContext = null;
    Fragment mFragment = null;
    static GoogleApiClient sGoogleApiClient = null;
    static GoogleSignInOptions sGoogleSignInOptions = null;
    static OptionalPendingResult<GoogleSignInResult> silentSignIn = null;
    TravexApplication mTravexApplication = null;
    public static final int SIGN_IN_REQUEST = 700;
    static NetworkManager mNetworkManager = null;
    JsonObjectMaker mJsonObjectMaker = null;
    ListCreatorForNameValuePairRequest mListCreatorForNameValuePairRequest = null;
    static PreferenceManager mPreferenceManager = null;

    /**
     * For google plus login
     */
    String first_name = null;
    String email_social = null;
    String social_id = null;
    String device_id = null;
    String device_type = null;
    String auth_method = null;

    public GooglePlusLogin(Context mContext, Fragment mFragment) {
        this.mContext = mContext;
        this.mFragment = mFragment;
        sGoogleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestId()
                .requestProfile()
                .requestEmail()
                .build();
        try {
            sGoogleApiClient = new GoogleApiClient.Builder(mContext)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, sGoogleSignInOptions)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
        }
        catch(Exception ex)
        {}
        mTravexApplication = TravexApplication.getInstance();
        mNetworkManager = NetworkManager.getSingleInstance(mContext);
        mNetworkManager.setOnGetGoogleLoginResponseListener(this);
        mJsonObjectMaker = new JsonObjectMaker(mContext);
        mListCreatorForNameValuePairRequest = new ListCreatorForNameValuePairRequest(mContext);
        mPreferenceManager = new PreferenceManager(mContext);
    }

    @Override
    public void onConnected(Bundle bundle) {

        LogUtility.Log(TAG, "onConnected", null);

        if (silentSignin()) {
            LogUtility.Log(TAG, "onConnected:silentSignin", null);
        } else {
            LogUtility.Log(TAG, "onConnected:signinGoogle", null);
            signinGoogle();

        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        LogUtility.Log(TAG, "onConnectionSuspended", null);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        LogUtility.Log(TAG, "onConnectionFailed", null);

    }

    public void connect_GooglePlayServices() {
        if (mTravexApplication.isGooglePlayServiceAvailable()) {
            if (sGoogleApiClient != null) {
                LogUtility.Log(TAG, "connect_GooglePlayServices:", "sGoogleApiClient:" + sGoogleApiClient);
                //sGoogleApiClient.disconnect();
                sGoogleApiClient.connect();

            }

        }
    }

    private void signinGoogle() {


        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(sGoogleApiClient);
        mFragment.startActivityForResult(signInIntent, SIGN_IN_REQUEST);
    }


    public void getData(Intent data) {
        GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

        getData(result);

    }


    private JSONObject getJson(ArrayList<String> namePair, ArrayList<String> valuePair) {


        return mJsonObjectMaker.getJson(namePair, valuePair);
    }


    /**
     * @param request_type
     * @param url
     * @param jsonObject
     * @param request_id
     */
    private void postJsonRequest(int request_type, String url, JSONObject jsonObject, final int request_id) {

        mNetworkManager.postJsonRequest(request_type, url, jsonObject, request_id);

    }

    private JSONObject getjsonObject(String tag) {
        if (tag.equals(TravexApplication.SOCIAL_LOGIN_GOOGLE_TAG)) {
            return getJson(mListCreatorForNameValuePairRequest.getListofSocialLogin(TravexApplication.SOCIAL_LOGIN_NAME,
                            TravexApplication.SOCIAL_LOGIN_EMAIL,
                            TravexApplication.SOCIAL_LOGIN_ID,
                            TravexApplication.SOCIAL_LOGIN_DEVICE_ID,
                            TravexApplication.SOCIAL_LOGIN_DEVICE_TYPE,
                            TravexApplication.SOCIAL_LOGIN_MEMBER_AUTH_METHOD),
                    mListCreatorForNameValuePairRequest.getListofSocialLogin(first_name,
                            email_social,
                            social_id,
                            device_id,
                            device_type,
                            auth_method));

        }

        return null;

    }

    @Override
    public void onGetGoogleLoginResponse(boolean status) {

        if (status) {
            LogUtility.Log(TAG, "onGetGoogleLoginResponse:success", null);
            mTravexApplication.showToast("google plus login successful");
            mPreferenceManager.setLogin_Type(TravexApplication.GOOGLE_LOGIN);
            LogUtility.Log(TAG, "onGetGoogleLoginResponse:login type from preference", "type of login:" + mPreferenceManager.getLogin_Type());
            startProfileActivity();
        } else {
            LogUtility.Log(TAG, "onGetGoogleLoginResponse:not success", null);

        }

    }

    private void startProfileActivity() {

        Intent mIntent = new Intent(mContext, ProfileActivity.class);
        mContext.startActivity(mIntent);
        mFragment.getActivity().finish();

    }

    public static void signOut_Google() {

        if (sGoogleApiClient != null) {
            LogUtility.Log(TAG, "signOut from google", null);

            Auth.GoogleSignInApi.signOut(sGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(Status status) {

                    if (status.isSuccess()) {
                        LogUtility.Log(TAG, "signOut:onResult:success", "status:" + status.getStatusMessage());
                        mPreferenceManager.setLogin_Type(null);
                        sGoogleApiClient.disconnect();
                    } else {
                        LogUtility.Log(TAG, "signOut:onResult:not success", "status:" + status.getStatusMessage());

                    }

                }
            });
        }


    }

    public static void revoke_Google() {
        LogUtility.Log(TAG, "revoke_Google:revoke from google", null);

        if (sGoogleApiClient != null) {
            Auth.GoogleSignInApi.revokeAccess(sGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(Status status) {
                    if (status.isSuccess()) {
                        LogUtility.Log(TAG, "revoke_Google:onResult:success", "status:" + status.getStatusMessage());
                        disconnect_Google();
                    } else {
                        LogUtility.Log(TAG, "revoke_Google:onResult:not success", "status:" + status.getStatusMessage());

                    }
                }
            });
        }


    }

    public static void disconnect_Google() {
        LogUtility.Log(TAG, "disconnect_Google:disconnect from google", null);
        if (sGoogleApiClient != null) {

            sGoogleApiClient.disconnect();
        }


    }

    /**
     * To check whether the credentials of google sign in cached or not
     *
     * @return
     */
    public boolean silentSignin() {
        silentSignIn = Auth.GoogleSignInApi.silentSignIn(sGoogleApiClient);
        if (silentSignIn.isDone()) {
            LogUtility.Log(TAG, "silentSignin", null);
            GoogleSignInResult mGoogleSignInResult = silentSignIn.get();

            return getData(mGoogleSignInResult);
        }


        return false;
    }

    public void silentsignIn2() {
        silentSignIn = Auth.GoogleSignInApi.silentSignIn(sGoogleApiClient);

        silentSignIn.setResultCallback(new ResultCallback<GoogleSignInResult>() {
            @Override
            public void onResult(GoogleSignInResult googleSignInResult) {
            }
        });

    }

    private boolean getData(GoogleSignInResult result) {

        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            first_name = acct.getDisplayName();
            email_social = acct.getEmail();
            social_id = acct.getId();
            device_id = mTravexApplication.getDEVICE_ID();
            device_type = "android";
            auth_method = "gp";
            LogUtility.Log(TAG, "getData:", "name:" + acct.getDisplayName() + ",email:" + acct.getEmail() + ",id:" + acct.getId());
            postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.SOCIAL_LOGIN_URL, getjsonObject(TravexApplication.SOCIAL_LOGIN_GOOGLE_TAG),
                    TravexApplication.REQUEST_ID_SOCIAL_LOGIN_GOOGLE);

            return true;

        } else {
            mTravexApplication.showToast("google login failed");

            return false;
        }


    }


}
