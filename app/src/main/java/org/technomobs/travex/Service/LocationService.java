package org.technomobs.travex.Service;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.NetworkOnMainThreadException;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Controller.NetworkManager;
import org.technomobs.travex.Location.CurrentLocationDetails;
import org.technomobs.travex.Location.LocationCoordinates;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.Utillity.LogUtility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by technomobs on 15/2/16.
 */
public class LocationService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    public static final String TAG = LocationService.class.getSimpleName();
    LocationCoordinates mLocationCoordinates = null;
    Location mLocation = null;
    CurrentLocationDetails mCurrentLocationDetails = null;
    String mCurrentLocality = null;
    String mCurrentCountry = null;
    PreferenceManager mPreferenceManager = null;
    NetworkManager mNetworkManager = null;
    LocationRequest mLocationRequest = null;
    GoogleApiClient mGoogleApiClient = null;
    ExecuteUrlConnection mExecuteUrlConnection=null;

    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    public LocationService() {

    }

    AccessTokenTracker mAccessTokenTracker = null;
    ProfileTracker mProfileTracker = null;

    public void trackAccessTokenFacebook() {
        mAccessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                LogUtility.Log(TAG, "trackAccessTokenFacebook:onCurrentAccessTokenChanged", "currentAccessToken:" + currentAccessToken.getToken());
                if (mPreferenceManager.getLogin_Type() == TravexApplication.FACEBOOK_LOGIN)
                    mPreferenceManager.setLogin_Type(null);
            }
        };

    }

    public void trackProfileFacebook() {

        mProfileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                LogUtility.Log(TAG, "trackProfileFacebook:onCurrentProfileChanged", "currentProfile id :" + currentProfile.getId());

                if (mPreferenceManager.getLogin_Type() == TravexApplication.FACEBOOK_LOGIN)

                    mPreferenceManager.setLogin_Type(null);

            }
        };
    }

    @Override
    public void onCreate() {
        super.onCreate();
        LogUtility.Log(TAG, "onCreate", null);
        mLocationCoordinates = new LocationCoordinates(LocationService.this);
        mCurrentLocationDetails = new CurrentLocationDetails(LocationService.this);
        mPreferenceManager = new PreferenceManager(LocationService.this);
        mNetworkManager = NetworkManager.getSingleInstance(LocationService.this);

        trackAccessTokenFacebook();
        trackProfileFacebook();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LogUtility.Log(TAG, "onStartCommand", null);
        /*mLocation = mLocationCoordinates.getTheLocationFromNetwork();
        if (mLocation == null) {
            mLocation = mLocationCoordinates.getTheLocationFromPassive();
            if (mLocation == null)
                mLocation = mLocationCoordinates.getTheLocationFromGps();
        }*/


        mGoogleApiClient = new GoogleApiClient.Builder(LocationService.this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


        mGoogleApiClient.connect();
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        LogUtility.Log(TAG, "onDestroy", null);
        mGoogleApiClient.disconnect();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    protected void onHandleIntent(Intent intent) {


    }

    public void locateCurrentLocationFromGoogleMapServer() {

        if (isNetworkconnected()) {
            LogUtility.Log(TAG, "locateCurrentLocationFromGoogleMapServer:network  connected", null);

            findLocalityAndCountryFromMapApi(mPreferenceManager.getCurrentLatitude(), mPreferenceManager.getCurrentLongitude());

        } else {
            LogUtility.Log(TAG, "locateCurrentLocationFromGoogleMapServer:network not connected", null);
        }


    }

    public void findLocalityAndCountryFromMapApi(String latitude, String longitude) {
        LogUtility.Log(TAG, "findLocalityAndCountryFromMapApi", null);


        HttpGet httpGet;


        try {




            httpGet = new HttpGet("http://maps.googleapis.com/maps/api/geocode/json?latlng=" +
                    latitude + "," + longitude + "&sensor=true");
            if(mExecuteUrlConnection==null)
            {
                LogUtility.Log(TAG, "findLocalityAndCountryFromMapApi:", "mExecuteUrlConnection is null:going to initialize" );


                mExecuteUrlConnection=new ExecuteUrlConnection();
                mExecuteUrlConnection.execute(httpGet);
            }
            else
            {
                if(!mExecuteUrlConnection.isCancelled())
                {
                    LogUtility.Log(TAG, "findLocalityAndCountryFromMapApi:", "mExecuteUrlConnection is not null:going to cancel and start" );

                    mExecuteUrlConnection.cancel(true);
                    mExecuteUrlConnection=new ExecuteUrlConnection();
                    mExecuteUrlConnection.execute(httpGet);
                }

            }







        }  catch (Exception e) {
            LogUtility.Log(TAG, "findLocalityAndCountryFromMapApi:", "JSONException:" + e.getMessage());

            e.printStackTrace();
        }

    }


    private StringBuilder inputStreamToString(InputStream is) {
        LogUtility.Log(TAG, "inputStreamToString", "InputStream:" + is);
        String line = "";
        StringBuilder answer = new StringBuilder();
        BufferedReader mBufferedReader = null;
        try {
            mBufferedReader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
            try {
                while ((line = mBufferedReader.readLine()) != null) {
                    answer.append(line);
                }
            } catch (IOException e) {
                LogUtility.Log(TAG, "inputStreamToString()", "IOException:" + e.getMessage());
            }
        } catch (UnsupportedEncodingException e) {
            LogUtility.Log(TAG, "inputStreamToString()", "UnsupportedEncodingException:" + e.getMessage());
            e.printStackTrace();
        }


        return answer;
    }

    public boolean isNetworkconnected() {
        return mNetworkManager.isNetworkconnected();

    }

    @Override
    public void onConnected(Bundle bundle) {

        //LogUtility.Log(TAG, "onConnected", null);
        try{
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
        catch(Exception exc)
        {

        }


       /* mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLocation == null) {
        } else {
            LogUtility.Log(TAG, "onConnected:", "location:" + mLocation);
        }*/
        /*LogUtility.Log(TAG, "onConnected:", "location:" + mLocation);
        mCurrentLocality = mCurrentLocationDetails.getCurrentLocality(mLocation);
        mCurrentCountry = mCurrentLocationDetails.getCurrentCountryName(mLocation);
        LogUtility.Log(TAG, "onConnected:", "mCurrentLocality:" + mCurrentLocality);
        LogUtility.Log(TAG, "onConnected:", "mCurrentCountry:" + mCurrentCountry);*/


    }

    @Override
    public void onConnectionSuspended(int i) {
        LogUtility.Log(TAG, "onConnectionSuspended", null);

    }

    @Override
    public void onLocationChanged(Location location) {
        LogUtility.Log(TAG, "onLocationChanged:", null);

        mLocation = location;
        LogUtility.Log(TAG, "onLocationChanged:", "location:" + mLocation);

        try {


            if (mLocation != null) {
                LogUtility.Log(TAG, "onLocationChanged", "mLocation:latitude" + mLocation.getLatitude());
                LogUtility.Log(TAG, "onLocationChanged", "mLocation:longitude" + mLocation.getLongitude());
                mPreferenceManager.setCurrentLatitude(String.valueOf(mLocation.getLatitude()));
                mPreferenceManager.setCurrentLongitude(String.valueOf(mLocation.getLongitude()));
                locateCurrentLocationFromGoogleMapServer();




            } else {
                LogUtility.Log(TAG, "onLocationChanged:mLocation is null", null);

            }
        } catch (Exception e) {
            LogUtility.Log(TAG, "onLocationChanged:Exception", null);
        }


    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        LogUtility.Log(TAG, "onConnectionFailed", null);

    }


    public class ExecuteUrlConnection extends AsyncTask<HttpGet,Void,Void>
    {
        HttpClient client;
        StringBuilder stringBuilder;
        HttpEntity entity;
        InputStream stream;
        HttpResponse response;

        @Override
        protected Void doInBackground(HttpGet... httpGet) {
            String locality = null;
            String country_code = null;
            String admin1 = null;
            LogUtility.Log(TAG, "ExecuteUrlConnection:", "httpGet:" + httpGet[0]);

            try {
                response = client.execute(httpGet[0]);
              //  LogUtility.Log(TAG, "ExecuteUrlConnection:", "response:" + response);
                entity = response.getEntity();
               // LogUtility.Log(TAG, "ExecuteUrlConnection:", "entity:" + entity);

                stream = entity.getContent();
                LogUtility.Log(TAG, "ExecuteUrlConnection:", "stream:" + stream);
                stringBuilder=inputStreamToString(stream);
              //  LogUtility.Log(TAG, "ExecuteUrlConnection:", "stringBuilder:" + stringBuilder);
              //  LogUtility.Log(TAG, "ExecuteUrlConnection:", "jsonResult:" + stringBuilder.toString());

                JSONObject jsonResponse = null;
                jsonResponse = new JSONObject(stringBuilder.toString());



                LogUtility.Log(TAG, "ExecuteUrlConnection", "Geocoder Status:" + jsonResponse.getString("status"));
                if ("OK".equalsIgnoreCase(jsonResponse.getString("status"))) {
                    JSONArray addressComponents = ((JSONArray) jsonResponse.get("results")).getJSONObject(0).getJSONArray("address_components");
                    for (int i = 0; i < addressComponents.length(); i++) {
                        String addressComponent = ((JSONArray) ((JSONObject) addressComponents.get(i)).get("types")).getString(0);
                        if (addressComponent.equals("locality")) {
                            locality = ((JSONObject) addressComponents.get(i)).getString("long_name");
                        }
                        if (addressComponent.equals("administrative_area_level_1")) {
                            admin1 = ((JSONObject) addressComponents.get(i)).getString("long_name");
                        }
                        if (addressComponent.equals("country")) {
                            country_code = ((JSONObject) addressComponents.get(i)).getString("long_name");
                        }
                    }
                    Log.d("json result", locality + "." + admin1 + "." + country_code);
                    LogUtility.Log(TAG, "findLocalityAndCountryFromMapApi", "locality:" + locality);
                    LogUtility.Log(TAG, "findLocalityAndCountryFromMapApi", "admin1:" + admin1);
                    LogUtility.Log(TAG, "findLocalityAndCountryFromMapApi", "country_code:" + country_code);
                    mPreferenceManager.setCurrentLocalityName(locality);
                    mPreferenceManager.setCurrentcountryName(country_code);


                }
            } catch (Exception e) {
                LogUtility.Log(TAG, "ExecuteUrlConnection:", "Exception:" + e.getMessage());

                e.printStackTrace();
            }



            return null;
        }

        @Override
        protected void onPreExecute() {
            client = new DefaultHttpClient();
            LogUtility.Log(TAG, "ExecuteUrlConnection:onPreExecute", "client:" + client);

            stringBuilder = new StringBuilder();
            LogUtility.Log(TAG, "ExecuteUrlConnection:onPreExecute", "stringBuilder:" + stringBuilder);

            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }
}
