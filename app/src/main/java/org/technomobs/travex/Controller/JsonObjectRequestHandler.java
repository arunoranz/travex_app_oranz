package org.technomobs.travex.Controller;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;
import org.technomobs.travex.Utillity.LogUtility;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by technomobs on 1/4/16.
 */
public class JsonObjectRequestHandler extends JsonObjectRequest {


    String mUrl = null;
    JSONObject mJsonObject = null;
    Map<String, String> mHeaders = null;
    Response.Listener<JSONObject> mRespoJsonObjectListener = null;
    String TAG = JsonObjectRequestHandler.class.getSimpleName();
    private static final String PROTOCOL_CONTENT_TYPE = String.format("application/json", NetworkOptions.PROTOCOL_CHARSET);


    public JsonObjectRequestHandler(int method, String url, JSONObject jsonRequest, Map<String, String> headers, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
        mUrl = url;
        mJsonObject = jsonRequest;
        mHeaders = headers;
        mRespoJsonObjectListener = listener;
        setRetryPolicy(new DefaultRetryPolicy(NetworkOptions.CONNECTION_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }

    public JsonObjectRequestHandler(int method, String url, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
        mUrl = url;
        mJsonObject = jsonRequest;
        mHeaders = new HashMap<>();
        mRespoJsonObjectListener = listener;

        setRetryPolicy(new DefaultRetryPolicy(NetworkOptions.CONNECTION_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    }

    @Override
    protected void deliverResponse(JSONObject response) {
        LogUtility.Log(TAG, "deliverResponse", "response:" + response);
        JSONObject custom_object = new JSONObject();
        try {
            custom_object.accumulate("status", 10);
            custom_object.accumulate("message", "json object is null");

        } catch (JSONException e) {
            e.printStackTrace();
        }


        if (response != null)
            super.deliverResponse(response);
        else {
            super.deliverResponse(custom_object);

        }
       /* if (response != null)
            LogUtility.Log(TAG, "deliverResponse", "response:" + response);*/


        /**
         *         below statement not needed,if so ,it will cause the onResponse Callback to be triggered once again

         */


        // mRespoJsonObjectListener.onResponse(response);
    }

    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        //  LogUtility.Log(TAG, "parseNetworkResponse", "response:" + response);
        JSONObject object = null;
        try {
           /* LogUtility.Log(TAG, "parseNetworkResponse:", "statusCode:" + response.statusCode);
            LogUtility.Log(TAG, "parseNetworkResponse:", "data:" + response.data);
*/
            object = new JSONObject(new String(response.data));
           /* LogUtility.Log(TAG, "parseNetworkResponse:", "statusCode:" + response.statusCode);
            LogUtility.Log(TAG, "parseNetworkResponse:", "data:" + response.data);*/

            LogUtility.Log(TAG, "parseNetworkResponse:", "object:" + object);

        } catch (JSONException e) {
            LogUtility.Log(TAG, "parseNetworkResponse:", "JSONException:" + e.getMessage());

            e.printStackTrace();
        }


        return Response.success(object, getCacheEntry());
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        LogUtility.Log(TAG, "getHeaders:", "mHeaders:" + mHeaders);
        return mHeaders;

    }

    @Override
    public String getBodyContentType() {

        return PROTOCOL_CONTENT_TYPE;
    }


}
