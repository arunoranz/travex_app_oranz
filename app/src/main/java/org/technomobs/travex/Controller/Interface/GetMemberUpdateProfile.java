package org.technomobs.travex.Controller.Interface;

/**
 * Created by technomobs on 29/2/16.
 */
public interface GetMemberUpdateProfile {
    public void onGetMemberUpdateProfile(boolean status);
}
