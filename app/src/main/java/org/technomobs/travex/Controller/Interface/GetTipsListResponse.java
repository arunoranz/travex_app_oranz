package org.technomobs.travex.Controller.Interface;

import org.json.JSONObject;

/**
 * Created by technomobs on 16/3/16.
 */
public interface GetTipsListResponse {

    public void onGetTipsListResponse(boolean status, JSONObject jsonObject);
}
