package org.technomobs.travex.Controller.Interface;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by technomobs on 8/3/16.
 */
public interface GetResponseSearchResultsGeneral {
    /**
     *
     * @param status
     * @param jsonObjectItems  this  contains the jsonobject's id,title,latitude,longitude,subtitle,location,review,images_counter,images_videos,description
     * @param jsonObjectArrayItems this contains the jsonObjects's JsonArray item like phone numbers and title images urls
     */
    public void onGetResponseSearchResultsGeneral(boolean status, HashMap<Integer, ArrayList<String>> jsonObjectItems, HashMap<Integer, HashMap<Integer, ArrayList<String>>> jsonObjectArrayItems,String advertisement_img,String advertisement_url);
}
