package org.technomobs.travex.Controller.Interface;

/**
 * Created by technomobs on 7/4/16.
 */
public interface GetDonePlanResponse {

    public void onGetDonePlanResponse(boolean status);
}
