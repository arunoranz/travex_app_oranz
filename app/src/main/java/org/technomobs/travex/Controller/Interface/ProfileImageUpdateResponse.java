package org.technomobs.travex.Controller.Interface;

/**
 * Created by technomobs on 31/3/16.
 */
public interface ProfileImageUpdateResponse {

    public void onGetProfileImageUpdateResponse(boolean status, String member_profile_image_url);
}
