package org.technomobs.travex.Controller.Interface;

import java.util.ArrayList;

/**
 * Created by technomobs on 25/3/16.
 */
public interface GetSearchDestinations {

    public void onGetSearchDestinations(boolean value, ArrayList<String> destinations);
}
