package org.technomobs.travex.Controller.Interface;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by technomobs on 8/4/16.
 */
public interface GetTravexEventsAndMonthlyPlanResponse {

    public void onGetTravexEventAndMonthlyPlanResponse(boolean status, HashMap<String, ArrayList<String>> monthly_plan_list,HashMap<String, ArrayList<String>> travex_events);
}
