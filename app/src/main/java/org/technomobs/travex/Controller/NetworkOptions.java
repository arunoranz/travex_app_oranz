package org.technomobs.travex.Controller;

import com.android.volley.Request;

/**
 * Created by technomobs on 18/2/16.
 */
public interface NetworkOptions {

    public static final int REQUEST_TYPE_POST = Request.Method.POST;
    public static final int CONNECTION_TIMEOUT = 30 * 1000;
    public static final String PROTOCOL_CHARSET = "utf-8";
    public static final int CACHE_EXPIRE_TIME = 5 * 60 * 1000;




}
