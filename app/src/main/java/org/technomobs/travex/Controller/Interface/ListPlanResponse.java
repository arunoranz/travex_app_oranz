package org.technomobs.travex.Controller.Interface;

import org.json.JSONObject;

/**
 * Created by technomobs on 16/3/16.
 */
public interface ListPlanResponse {

    public void onGetListPlanResponse(boolean status, JSONObject jsonObject);


}
