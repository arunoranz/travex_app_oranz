package org.technomobs.travex.Controller.Interface;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by technomobs on 17/3/16.
 */
public interface AboutUsResponse {


    public void onGetAboutUsResponse(boolean status, HashMap<Integer, HashMap<Integer, ArrayList<String>>> description_teamDetails);
}
