package org.technomobs.travex.Controller.Interface;

import org.json.JSONObject;

/**
 * Created by technomobs on 15/3/16.
 */
public interface GetSearchItemResponse {

    /**
     * @param status     success/failure
     * @param jsonObject response as jsonObject
     */
    public void onGetSearchItemResponse(boolean status, JSONObject jsonObject);

}
