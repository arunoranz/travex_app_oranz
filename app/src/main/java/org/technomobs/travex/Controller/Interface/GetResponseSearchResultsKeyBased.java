package org.technomobs.travex.Controller.Interface;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by technomobs on 13/3/16.
 */
public interface GetResponseSearchResultsKeyBased {


    public void onGetSearchResultsKeyWordBased(boolean status,HashMap<Integer, ArrayList<String>> target);

}
