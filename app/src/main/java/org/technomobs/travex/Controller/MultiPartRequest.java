package org.technomobs.travex.Controller;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;

import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONException;
import org.json.JSONObject;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.Utillity.LogUtility;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by technomobs on 31/3/16.
 */
public class MultiPartRequest extends Request<JSONObject> {

    String TAG = MultiPartRequest.class.getSimpleName();
    File mFile = null;
    String mUrl = null;
    Response.Listener<JSONObject> mResponse_listener = null;
    Map<String, String> mHeaders = null;
    Context mContext = null;
    HttpEntity mHttpEntity = null;
    JSONObject mJsonObject = null;
    MultipartEntityBuilder mMultipartEntityBuilder = null;


    public MultiPartRequest(Context context, int method, String url, Map<String, String> headers, JSONObject jsonObject, Response.Listener<JSONObject> responselistener, Response.ErrorListener listener) {
        super(method, url, listener);
        mContext = context;
        mUrl = url;
        mResponse_listener = responselistener;
        mHeaders = headers;
        mJsonObject = jsonObject;
        setRetryPolicy(new DefaultRetryPolicy(NetworkOptions.CONNECTION_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        parseJsonObject(mJsonObject);

    }

    public void parseJsonObject(JSONObject mJsonObject) {
        String member_profile_image = null;
        try {
            member_profile_image = mJsonObject.getString("member_profile_image");
            mFile = new File(member_profile_image);
            mHttpEntity = buildMultiPartEntity(mFile, mJsonObject);
            LogUtility.Log(TAG, "parseJsonObject:", "mHttpEntity:" + mHttpEntity);
        } catch (JSONException e) {
            LogUtility.Log(TAG, "parseJsonObject", "JSONException");
        }

    }

    private HttpEntity buildMultiPartEntity(File file, JSONObject jsonObject) {
        mMultipartEntityBuilder = MultipartEntityBuilder.create();
        String fileName = file.getName();
        LogUtility.Log(TAG, "buildMultiPartEntity", "fileName:" + fileName);
        FileBody fileBody = new FileBody(file, ContentType.MULTIPART_FORM_DATA, fileName);
        LogUtility.Log(TAG, "buildMultiPartEntity", "fileBody:" + fileBody);
        mMultipartEntityBuilder.addPart(TravexApplication.PROFILE_IMAGE_UPDATE_MEMBER_PROFILE_IMAGE, fileBody);
        try {
            String member_id = jsonObject.getString("member_id");
            LogUtility.Log(TAG, "buildMultiPartEntity", "member_id:" + member_id);
            mMultipartEntityBuilder.addPart(TravexApplication.PROFILE_IMAGE_UPDATE_MEMBER_ID, new StringBody(member_id, ContentType.MULTIPART_FORM_DATA));


        } catch (JSONException e) {
            LogUtility.Log(TAG, "buildMultiPartEntity", "JSONException");
        }


        return mMultipartEntityBuilder.build();
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
       /* PreferenceManager mPreferenceManager = new PreferenceManager(mContext);

        mHeaders.put("X-API-KEY", mPreferenceManager.getApiKey());*/
        return mHeaders;
    }

    @Override
    public String getBodyContentType() {
        return mHttpEntity.getContentType().getValue();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            mHttpEntity.writeTo(bos);
          //  LogUtility.Log(TAG, "getBody", "ByteArrayOutputStream:" + bos);
        } catch (IOException e) {
            LogUtility.Log(TAG, "getBody", "IOException");


        }
        return bos.toByteArray();
    }

    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(new String(response.data));

            LogUtility.Log(TAG, "parseNetworkResponse", "jsonObject:" + jsonObject);
        } catch (JSONException e) {
            LogUtility.Log(TAG, "parseNetworkResponse:JSONException", null);
        }
        return Response.success(jsonObject, getCacheEntry());

    }

    @Override
    protected void deliverResponse(JSONObject response) {
        LogUtility.Log(TAG, "deliverResponse", "jsonObject:" + response);

        mResponse_listener.onResponse(response);
    }
}
