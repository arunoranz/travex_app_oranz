package org.technomobs.travex.Controller.Interface;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by technomobs on 14/3/16.
 */
public interface GetNowResponse {
    /**
     *
     * @param status success or Failure
     * @param jsonobject  containing the id,title,latitude,longitude
     * @param array_phonenumbers containing phone numbers for each of the jsonobject
     */
    public void onGetNowResponse(boolean status,HashMap<Integer, ArrayList<String>> jsonobject,HashMap<Integer, ArrayList<String>> array_phonenumbers);
}
