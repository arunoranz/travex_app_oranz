package org.technomobs.travex.Controller.Interface;

/**
 * Created by technomobs on 17/3/16.
 */
public interface DeletePlanResponse {

    public void onDeletePlanResponse(boolean status);
}
