package org.technomobs.travex.Controller;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Utillity.LogUtility;

import java.util.ArrayList;

/**
 * Created by technomobs on 18/2/16.
 */
public class JsonObjectMaker {

    Context mContext = null;
    public static final String TAG = JsonObjectMaker.class.getSimpleName();


    public JsonObjectMaker(Context context) {

        mContext = context;
    }

    private JSONObject getJsonObjectFromNameValuePairDetails(ArrayList<String> namePair, ArrayList<String> valuePair) {
        JSONObject mJsonObject = null;
        mJsonObject = new JSONObject();

        for (int k = 0; k < namePair.size(); k++) {

            try {
                mJsonObject.accumulate(namePair.get(k), valuePair.get(k));
                LogUtility.Log(TAG, "getJsonObjectFromNameValuePairDetails", "mJsonObject:" + mJsonObject);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

       // LogUtility.Log(TAG, "getJsonObjectFromNameValuePairDetails", "mJsonObject:" + mJsonObject);
        return mJsonObject;

    }


    public JSONObject getJson(ArrayList<String> namePair, ArrayList<String> valuePair) {


        return getJsonObjectFromNameValuePairDetails(namePair, valuePair);


    }

    private ArrayList<String> getNamePairOfRegistration() {
        ArrayList<String> list = new ArrayList<String>();
        list.add(TravexApplication.FIRST_NAME_REGISTRATION);
        list.add(TravexApplication.EMAIL_REGISTRATION);
        list.add(TravexApplication.PASSWORD_REGISTRATION);
        list.add(TravexApplication.DEVICE_ID_REGISTRATION);
        list.add(TravexApplication.DEVICE_TYPE_REGISTRATION);
        list.add(TravexApplication.AUTH_METHOD_REGISTRATION);
        return list;


    }

    private ArrayList<String> getNamePairOf_Splash_Feature() {
        ArrayList<String> list = new ArrayList<String>();
        list.add(TravexApplication.SPLASH_FEATURE_TYPE);
        return list;


    }


}
