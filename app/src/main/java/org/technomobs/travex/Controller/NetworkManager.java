package org.technomobs.travex.Controller;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.technomobs.travex.Activity.Fragment.MapShowingFragment;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Controller.Interface.AboutUsResponse;
import org.technomobs.travex.Controller.Interface.AddPlanResponse;
import org.technomobs.travex.Controller.Interface.AddSuggestionsResponse;
import org.technomobs.travex.Controller.Interface.ContactUsResponse;
import org.technomobs.travex.Controller.Interface.DayPlanResponse;
import org.technomobs.travex.Controller.Interface.DeletePlanResponse;
import org.technomobs.travex.Controller.Interface.EnquiryResponse;
import org.technomobs.travex.Controller.Interface.GetArticleDetailsResponse;
import org.technomobs.travex.Controller.Interface.GetArticlesListResponse;
import org.technomobs.travex.Controller.Interface.GetDonePlanResponse;
import org.technomobs.travex.Controller.Interface.GetEssentialListResponse;
import org.technomobs.travex.Controller.Interface.GetEssentialsDetailsResponse;
import org.technomobs.travex.Controller.Interface.GetEventDetailsResponse;
import org.technomobs.travex.Controller.Interface.GetFilterResponse;
import org.technomobs.travex.Controller.Interface.GetFilterResultsResponse;
import org.technomobs.travex.Controller.Interface.GetForgetPasswordResponse;
import org.technomobs.travex.Controller.Interface.GetMemberDetailsResponse;
import org.technomobs.travex.Controller.Interface.GetNowResponse;
import org.technomobs.travex.Controller.Interface.GetResponseSearchResultsGeneral;
import org.technomobs.travex.Controller.Interface.GetResponseSearchResultsKeyBased;
import org.technomobs.travex.Controller.Interface.GetSearchDestinations;
import org.technomobs.travex.Controller.Interface.GetSearchItemResponse;
import org.technomobs.travex.Controller.Interface.GetSearchTerminals;
import org.technomobs.travex.Controller.Interface.GetTipsDetailsResponse;
import org.technomobs.travex.Controller.Interface.GetTipsListResponse;
import org.technomobs.travex.Controller.Interface.GetTravexEventsAndMonthlyPlanResponse;
import org.technomobs.travex.Controller.Interface.GetWriteReviewResponse;
import org.technomobs.travex.Controller.Interface.ListPlanResponse;
import org.technomobs.travex.Controller.Interface.ProfileImageUpdateResponse;
import org.technomobs.travex.Controller.Interface.UpdatePlanResponse;
import org.technomobs.travex.Gson.JsonFeeder;
import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.Controller.Interface.GetCityResponse;
import org.technomobs.travex.Controller.Interface.GetCitySuggestionResponse;
import org.technomobs.travex.Controller.Interface.GetFeatureImagesResponse;
import org.technomobs.travex.Controller.Interface.GetGoogleLoginResponse;
import org.technomobs.travex.Controller.Interface.GetLoginResponse;
import org.technomobs.travex.Controller.Interface.GetMemberUpdateProfile;
import org.technomobs.travex.Controller.Interface.GetRegistrationResponse;
import org.technomobs.travex.Controller.Interface.GetFacebookLoginResponse;
import org.technomobs.travex.Controller.Interface.GetSplashLandingImagesResponse;
import org.technomobs.travex.Controller.Interface.GetTravexLogoutResponse;
import org.technomobs.travex.Controller.Interface.GetVerifyResponse;
import org.technomobs.travex.GooglePlus.GooglePlusLogin;
import org.technomobs.travex.Model.SpinnerCategoryViewHolder;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.Utillity.LogUtility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by technomobs on 18/2/16.
 */
public class NetworkManager implements NetworkOptions {
    HashMap<String, ArrayList<String>> monthly_plan_list = null;
    HashMap<String, ArrayList<String>> travex_events = null;
    HashMap<Integer, ArrayList<String>> phonenumbers_travex_events = null;
    public static final String TAG = NetworkManager.class.getSimpleName();
    static GetCityResponse sGetCityResponse = null;
    static GetRegistrationResponse sGetRegistrationResponse = null;
    static GetSplashLandingImagesResponse sGetSplashLandingImagesResponse = null;
    static GetFeatureImagesResponse sGetFeatureImagesResponse = null;
    static GetVerifyResponse sGetVerifyResponse = null;
    static GetCitySuggestionResponse sGetCitySuggestionResponse = null;
    static GetMemberUpdateProfile sGetMemberUpdateProfile = null;
    static GetTravexLogoutResponse sGetTravexLogoutResponse = null;
    static GetLoginResponse sGetLoginResponse = null;
    static GetFacebookLoginResponse sGetFacebookLoginResponse = null;
    static GetGoogleLoginResponse sGetGoogleLoginResponse = null;
    static GetResponseSearchResultsGeneral sGetResponseSearchResultsGeneral = null;
    static GetResponseSearchResultsKeyBased sGetResponseSearchResultsKeyBased = null;
    static GetNowResponse sGetNowResponse = null;
    static GetFilterResponse sGetFilterResponse = null;
    static GetSearchItemResponse sGetSearchItemResponse = null;
    static GetFilterResultsResponse sGetFilterResultsResponse = null;
    static GetArticlesListResponse sGetArticlesListResponse = null;
    static GetArticleDetailsResponse sGetArticleDetailsResponse = null;
    static GetEssentialListResponse sGetEssentialListResponse = null;
    static GetEssentialsDetailsResponse sGetEssentialsDetailsResponse = null;
    static GetTipsListResponse sGetTipsListResponse = null;
    static GetTipsDetailsResponse sGetTipsDetailsResponse = null;
    static AddPlanResponse sAddPlanResponse = null;
    static ListPlanResponse sListPlanResponse = null;
    static AddSuggestionsResponse sAddSuggestionsResponse = null;
    static AboutUsResponse sAboutUsResponse = null;
    static ContactUsResponse sContactUsResponse = null;
    static EnquiryResponse sEnquiryResponse = null;
    static UpdatePlanResponse sUpdatePlanResponse = null;
    static DeletePlanResponse sDeletePlanResponse = null;
    static GetSearchTerminals sGetSearchTerminals = null;
    static GetSearchDestinations sGetSearchDestinations = null;
    static ProfileImageUpdateResponse sProfileImageUpdateResponse = null;

    static GetDonePlanResponse sGetDonePlanResponse = null;
    static DayPlanResponse sDayPlanResponse = null;
    static GetTravexEventsAndMonthlyPlanResponse sGetTravexEventPlanResponse = null;
    static GetEventDetailsResponse sGetEventDetailsResponse = null;
    static GetWriteReviewResponse sGetWriteReviewResponse = null;
    static GetMemberDetailsResponse sGetMemberDetailsResponse = null;
    static GetForgetPasswordResponse sGetForgetPasswordResponse = null;


    PreferenceManager mPreferenceManager;
    Context mContext = null;
    static NetworkManager mNetworkManager = null;
    static JsonObjectRequest mJsonObjectRequest = null;
    TravexApplication mTravexApplication = null;
    String advertisement_img="",advertisement_url="";

    private NetworkManager(Context context) {

        mPreferenceManager = new PreferenceManager(context);
        mContext = context;
    }

    public static NetworkManager getSingleInstance(Context context) {
        mNetworkManager = new NetworkManager(context);
        return mNetworkManager;
    }

    public void setOnGetForgetPasswordResponseListener(GetForgetPasswordResponse getForgetPasswordResponse) {
        sGetForgetPasswordResponse = getForgetPasswordResponse;
    }

    public void setOnGetMemberDetailsResponseListener(GetMemberDetailsResponse getMemberDetailsResponse) {
        sGetMemberDetailsResponse = getMemberDetailsResponse;
    }

    public void setOnGetWriteReviewResponseListener(GetWriteReviewResponse getWriteReviewResponse) {
        sGetWriteReviewResponse = getWriteReviewResponse;
    }

    public void setOnGetEventDetailsResponseListener(GetEventDetailsResponse getEventDetailsResponse) {
        sGetEventDetailsResponse = getEventDetailsResponse;
    }

    public void setOnGetTravexEventPlanResponseListener(GetTravexEventsAndMonthlyPlanResponse getTravexEventPlanResponse) {
        sGetTravexEventPlanResponse = getTravexEventPlanResponse;
    }

    public void setOnGetDayPlanResponseListener(DayPlanResponse dayPlanResponse) {
        sDayPlanResponse = dayPlanResponse;
    }

    public void setOnGetDonePlanResponseListener(GetDonePlanResponse getDonePlanResponse) {
        sGetDonePlanResponse = getDonePlanResponse;
    }

    public void setOnProfileImageUpdateResponseListener(ProfileImageUpdateResponse profileImageUpdateResponse) {
        sProfileImageUpdateResponse = profileImageUpdateResponse;
    }

    public void setOnSearchDestinationsResponseListener(GetSearchDestinations getSearchDestinations) {
        sGetSearchDestinations = getSearchDestinations;
    }

    public void setOnSearchTerminalsResponseListener(GetSearchTerminals getSearchTerminals) {
        sGetSearchTerminals = getSearchTerminals;
    }

    public void setOnDeletePlanResponseListener(DeletePlanResponse deletePlanResponse) {
        sDeletePlanResponse = deletePlanResponse;
    }

    public void setOnUpdatePlanResponseListener(UpdatePlanResponse updatePlanResponse) {
        sUpdatePlanResponse = updatePlanResponse;
    }

    public void setOnEnquiryResponseListener(EnquiryResponse enquiryResponse) {
        sEnquiryResponse = enquiryResponse;
    }

    public void setOnContactUsResponseListener(ContactUsResponse contactUsResponse) {
        sContactUsResponse = contactUsResponse;
    }

    public void setOnAboutUsResponseListener(AboutUsResponse aboutUsResponse) {
        sAboutUsResponse = aboutUsResponse;
    }

    public void setOnAddSuggestionsResponseListener(AddSuggestionsResponse addSuggestionsResponse) {
        sAddSuggestionsResponse = addSuggestionsResponse;
    }

    public void setOnListPlanResponseListener(ListPlanResponse listPlanResponse) {
        sListPlanResponse = listPlanResponse;
    }

    public void setOnAddPlanResponseListener(AddPlanResponse addPlanResponse) {
        sAddPlanResponse = addPlanResponse;
    }

    public void setOnGetTipsListResponseListener(GetTipsListResponse getTipsListResponse) {
        sGetTipsListResponse = getTipsListResponse;
    }

    public void setOnGetTipsDetailsResponseListener(GetTipsDetailsResponse getTipsDetailsResponse) {
        sGetTipsDetailsResponse = getTipsDetailsResponse;
    }

    public void setOnGetEssentialsDetailsResponseListener(GetEssentialsDetailsResponse getEssentialsDetailsResponse) {
        sGetEssentialsDetailsResponse = getEssentialsDetailsResponse;
    }

    public void setOnGetEssentialsListResponseListener(GetEssentialListResponse getEssentialListResponse) {
        sGetEssentialListResponse = getEssentialListResponse;
    }

    public void setOnGetArticlesDetailsResponseListener(GetArticleDetailsResponse getArticleDetailsResponse) {
        sGetArticleDetailsResponse = getArticleDetailsResponse;
    }

    public void setOnGetArticlesListResponseListener(GetArticlesListResponse getArticlesListResponse) {
        sGetArticlesListResponse = getArticlesListResponse;
    }

    public void setOnGetFilterResultsResponseListener(GetFilterResultsResponse getFilterResultsResponse) {
        sGetFilterResultsResponse = getFilterResultsResponse;
    }

    public void setOnGetSearchItemResponseListener(GetSearchItemResponse getSearchItemResponse) {
        sGetSearchItemResponse = getSearchItemResponse;
    }

    public void setOnGetFilterResponseListener(GetFilterResponse getFilterResponse) {
        sGetFilterResponse = getFilterResponse;
    }

    public void setOnGetNowResponseListener(GetNowResponse getNowResponse) {
        sGetNowResponse = getNowResponse;
    }

    public void setOnGetSearchResultsKeywordbasedListener(GetResponseSearchResultsKeyBased getResponseSearchResultsKeyBased) {
        sGetResponseSearchResultsKeyBased = getResponseSearchResultsKeyBased;
    }

    public void setOnGetCitySuggestListener(GetCitySuggestionResponse getCitySuggestListener) {
        sGetCitySuggestionResponse = getCitySuggestListener;
    }

    /**
     * @param getCityResponseListener
     */
    public void setOnGetCityResponseListener(GetCityResponse getCityResponseListener) {
        sGetCityResponse = getCityResponseListener;
    }

    /**
     * @param getRegistrationResponse
     */
    public void setOnRegistrationResponseListener(GetRegistrationResponse getRegistrationResponse) {
        sGetRegistrationResponse = getRegistrationResponse;
    }

    /**
     * @param getSplashLandingImagesResponse
     */
    public void setOnSplashImagesResponseListener(GetSplashLandingImagesResponse getSplashLandingImagesResponse) {
        sGetSplashLandingImagesResponse = getSplashLandingImagesResponse;
    }

    /**
     * @param getFeatureImagesResponse
     */
    public void setOnFeatureImagesResponseListener(GetFeatureImagesResponse getFeatureImagesResponse) {
        sGetFeatureImagesResponse = getFeatureImagesResponse;
    }

    /**
     * @param getVerifyResponse
     */
    public void setOnVerifyResponseListener(GetVerifyResponse getVerifyResponse) {
        sGetVerifyResponse = getVerifyResponse;
    }

    public void setOnMemberUpdateResponseListener(GetMemberUpdateProfile getMemberUpdateProfile) {
        sGetMemberUpdateProfile = getMemberUpdateProfile;
    }

    public void setOnTravexLogoutResponseListener(GetTravexLogoutResponse getTravexLogoutResponse) {
        sGetTravexLogoutResponse = getTravexLogoutResponse;
    }

    public void setOnGetLoginResponseListener(GetLoginResponse getLoginResponse) {
        sGetLoginResponse = getLoginResponse;
    }

    public void setOnGetFacebookLoginResponseListener(GetFacebookLoginResponse getSocialLoginResponse) {
        sGetFacebookLoginResponse = getSocialLoginResponse;
    }

    public void setOnGetGoogleLoginResponseListener(GetGoogleLoginResponse getGoogleLoginResponse) {
        sGetGoogleLoginResponse = getGoogleLoginResponse;
    }

    public void setOnGetResponseSearchGeneralResultsListener(GetResponseSearchResultsGeneral getResponseSearchGeneralResultsListener) {
        sGetResponseSearchResultsGeneral = getResponseSearchGeneralResultsListener;
    }


    /*public void postJsonRequest(int request_type, String url, JSONObject jsonObject, final int request_id) {
        LogUtility.Log(TAG, "postJsonRequest", "url:" + url + ",jsonObject:" + jsonObject);
        if (isNetworkconnected()) {
            mJsonObjectRequest = new JsonObjectRequest(request_type, url, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                }
            });
            mJsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(NetworkOptions.CONNECTION_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            mTravexApplication = TravexApplication.getInstance();
            LogUtility.Log(TAG, "postJsonRequest", "mTravexApplication:" + mTravexApplication);
            mTravexApplication.addRequestToQueue(mJsonObjectRequest);
        }
    }*/

    JsonObjectRequest mJsonObjectRequestWithHeaders = null;


   /* public void postJsonRequest(int request_type, final Map<String, String> request_header, String url, JSONObject jsonObject, final int request_id) {
        LogUtility.Log(TAG, "postJsonRequest:mJsonObjectRequestWithHeaders", "request_header:" + request_header + ",url:" + url + ",jsonObject:" + jsonObject);
        mJsonObjectRequestWithHeaders = new JsonObjectRequest(request_type, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return request_header;
            }
        };
        mJsonObjectRequestWithHeaders.setRetryPolicy(new DefaultRetryPolicy(NetworkOptions.CONNECTION_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mTravexApplication = TravexApplication.getInstance();
        LogUtility.Log(TAG, "postJsonRequest for header info", "mTravexApplication:" + mTravexApplication);
        mTravexApplication.addRequestToQueue(mJsonObjectRequestWithHeaders);
    }*/

    /**
     * @param object
     * @param tag
     * @throws JSONException
     */
    public void parseJsonResponse(JSONObject object, String tag) throws JSONException {
        if (tag.equals(TravexApplication.GETCITY_TAG)) {
            int status = 0;
            JSONObject jsonObject = new JSONObject(object.toString());
            status = jsonObject.getInt("status");
            if (status == 1) {
                JSONArray jsonArray = jsonObject.getJSONArray("result");
                ArrayList<String> mArrayListForCityNames = null;
                ArrayList<String> mArrayListForCityId = null;
                ArrayList<String> mArrayListForCityLattitude = null;
                ArrayList<String> mArrayListForCityLongitude = null;
                ArrayList<String> mArrayListForCityTags = null;
                if (jsonArray.length() > 0) {
                    mArrayListForCityNames = new ArrayList<String>();
                    mArrayListForCityId = new ArrayList<String>();
                    mArrayListForCityLattitude = new ArrayList<String>();
                    mArrayListForCityLongitude = new ArrayList<String>();
                    mArrayListForCityTags = new ArrayList<>();
                    for (int k = 0; k < jsonArray.length(); k++) {
                        String id = null;
                        String city = null;
                        String latitude = null;
                        String longitude = null;
                        JSONObject jsonObject1 = jsonArray.getJSONObject(k);
                        id = jsonObject1.getString("city_id");
                        city = jsonObject1.getString("city");
                        latitude = jsonObject1.getString("latitude");
                        longitude = jsonObject1.getString("longitude");
                        JSONArray city_tags = jsonObject1.getJSONArray("city_tags");
                        if (city_tags.length() > 0) {

                            for (int l = 0; l < city_tags.length(); l++) {

                                String city_tag = null;
                                city_tag = city_tags.get(l).toString();
                                addItemToArraylist(city_tag, mArrayListForCityTags);

                            }
                        }

                        if ((id != null)) {
                            if (id.trim().length() > 0) {
                                LogUtility.Log(TAG, "parseJsonResponse:get city tag", "id:" + id);

                            } else {
                                LogUtility.Log(TAG, "parseJsonResponse: get city tag:id:is null", null);

                            }
                        }
                        if ((city != null)) {
                            if (city.trim().length() > 0) {
                                LogUtility.Log(TAG, "parseJsonResponse:get city tag", "city:" + city);
                            } else {
                                LogUtility.Log(TAG, "parseJsonResponse: get city tag:city:is null", null);
                            }
                        }
                        if ((latitude != null)) {
                            if (latitude.trim().length() > 0) {
                                LogUtility.Log(TAG, "parseJsonResponse:get city tag", "latitude:" + latitude);
                            } else {
                                LogUtility.Log(TAG, "parseJsonResponse: get city tag:latitude:is null", null);
                            }
                        }
                        if ((longitude != null)) {
                            if (longitude.trim().length() > 0) {
                                LogUtility.Log(TAG, "parseJsonResponse:get city tag", "longitude:" + longitude);
                            } else {
                                LogUtility.Log(TAG, "parseJsonResponse: get city tag:longitude:is null", null);
                            }
                        }
                        addItemToArraylist(id, mArrayListForCityId);
                        addItemToArraylist(city, mArrayListForCityNames);
                        addItemToArraylist(latitude, mArrayListForCityLattitude);
                        addItemToArraylist(longitude, mArrayListForCityLongitude);

                    }
                    mPreferenceManager.setSupportedCountryNames(mArrayListForCityNames);
                    mPreferenceManager.setSupportedCountryId(mArrayListForCityId);
                    mPreferenceManager.setSupportedCountryLatitude(mArrayListForCityLattitude);
                    mPreferenceManager.setSupportedCountryLongitude(mArrayListForCityLongitude);
                    mPreferenceManager.setSupportedCountryTags(mArrayListForCityTags);
                    mPreferenceManager.getSupportedCountryTags();
                /* mPreferenceManager.getSupportedCountryId();
            mPreferenceManager.getSupportedCountryNames();
            mPreferenceManager.getSupportedCountryLatitude();
            mPreferenceManager.getSupportedCountryLongitude();*/
                } else {
                    LogUtility.Log(TAG, "parseJsonResponse:get city tag:json array result is null", null);
                }


                if (sGetCityResponse != null) {
                    sGetCityResponse.onGetCityResponse(true);
                }
            } else {
                if (sGetCityResponse != null) {
                    sGetCityResponse.onGetCityResponse(false);
                }
            }
        } else if (tag.equals(TravexApplication.REGISTER_TAG)) {
            int status = 0;
            JSONObject mJsonObject = null;
            mJsonObject = new JSONObject(object.toString());
            status = mJsonObject.getInt("status");
            LogUtility.Log(TAG, "parseJsonResponse:register travex", "status:" + status);
            if (status == 0) {
                if (sGetRegistrationResponse != null) {
                    sGetRegistrationResponse.onRegistrationResponse(false);
                }
            } else {
                String member_id = null;
                member_id = mJsonObject.getString("id");
                LogUtility.Log(TAG, "parseJsonResponse:register travex", "member_id:" + member_id);
                mPreferenceManager.setRegisration_Id_response(member_id);
                LogUtility.Log(TAG, "parseJsonResponse:register travex", "member_id from preference:" + mPreferenceManager.getRegisration_Id_response());
                if (sGetRegistrationResponse != null) {
                    sGetRegistrationResponse.onRegistrationResponse(true);
                }
            }
        } else if (tag.equals(TravexApplication.SPLASH_TAG)) {
            ArrayList<String> mArrayListForCms_images_names = new ArrayList<String>();
            ArrayList<String> mArrayListForCms_images_content = new ArrayList<String>();
            ArrayList<String> mArrayListForCms_images_date_time_update = new ArrayList<String>();
            int status = 0;
            JSONObject obj = null;
            JSONArray mResult = null;
            JSONObject mResultSubs = null;
            obj = new JSONObject(object.toString());
            status = obj.getInt("status");

            if (status == 1) {
                mResult = obj.getJSONArray("result");
                for (int k = 0; k < mResult.length(); k++) {
                    String cms_image = null;
                    String cms_content = null;
                    String cms_date_time = null;
                    mResultSubs = mResult.getJSONObject(k);
                    cms_image = mResultSubs.getString("cms_image");
                    cms_content = mResultSubs.getString("cms_content");
                    cms_date_time = mResultSubs.getString("cms_updated_at");
                    // cms_updated_at
                    if ((cms_image != null)) {
                        if (cms_image.trim().length() > 0) {
                            LogUtility.Log(TAG, "parseJsonResponse:spalsh images", "cms_image:" + cms_image);
                        } else {
                            LogUtility.Log(TAG, "parseJsonResponse:spalsh images:cms_image:is null", null);
                        }
                    }
                    if ((cms_content != null)) {
                        if (cms_content.trim().length() > 0) {
                            LogUtility.Log(TAG, "parseJsonResponse:spalsh images", "cms_content:" + cms_content);
                        } else {
                            LogUtility.Log(TAG, "parseJsonResponse:spalsh images:cms_content:is null", null);
                        }
                    }
                    if ((cms_date_time != null)) {
                        if (cms_date_time.trim().length() > 0) {
                            LogUtility.Log(TAG, "parseJsonResponse:spalsh images", "cms_date_time:" + cms_image);
                        } else {
                            LogUtility.Log(TAG, "parseJsonResponse:spalsh images:cms_date_time:is null", null);
                        }
                    }
                    addItemToArraylist(cms_image, mArrayListForCms_images_names);
                    addItemToArraylist(cms_content, mArrayListForCms_images_content);
                    addItemToArraylist(cms_date_time, mArrayListForCms_images_date_time_update);
                }
                mPreferenceManager.setSplash_landing_images(mArrayListForCms_images_names);
                mPreferenceManager.setSplash_landing_images_content(mArrayListForCms_images_content);
                mPreferenceManager.setSplash_landing_images_update_time(mArrayListForCms_images_date_time_update);
                ArrayList<String> cms_image_asset_url = new ArrayList<String>();
                for (String s : mArrayListForCms_images_names) {
                    if (s != null) {
                        cms_image_asset_url.add(AppConstants.SPLASH_IMAGES_ASSET_URL + s);
                    } else {
                        cms_image_asset_url.add(null);
                    }
                }
                mPreferenceManager.setSplash_landing_images_asset_url(cms_image_asset_url);
                if (sGetSplashLandingImagesResponse != null) {
                    sGetSplashLandingImagesResponse.onSplashLandingImages(true);
                }
            } else {
                if (sGetSplashLandingImagesResponse != null) {
                    sGetSplashLandingImagesResponse.onSplashLandingImages(false);
                }
            }





            /*ArrayList<String> list= mPreferenceManager.getSplash_landing_images_asset_url();
            for (int k=0;k<list.size();k++)
            {
                LogUtility.Log(TAG,"parseJsonResponse:","items:"+list.get(k));
            }*/

            /*mPreferenceManager.getSplash_landing_images();
            mPreferenceManager.getSplash_landing_images_content();
            mPreferenceManager.getSplash_landing_images_asset_url();*/
        } else if (tag.equals(TravexApplication.FEATURE_TAG)) {
            ArrayList<String> mArrayListForCms_images_feature_names = new ArrayList<String>();
            ArrayList<String> mArrayListForCms_images_feature_content = new ArrayList<String>();
            ArrayList<String> mArrayListForCms_images_feature_date_time_update = new ArrayList<String>();
            ArrayList<String> mArrayListForCms_images_feature_title = new ArrayList<String>();
            ArrayList<String> mArrayListForCms_images_feature_description = new ArrayList<String>();
            int status = 0;
            JSONObject obj = null;
            JSONArray mResult = null;
            JSONObject mResultSubs = null;
            obj = new JSONObject(object.toString());
            status = obj.getInt("status");

            if (status == 1) {
                mResult = obj.getJSONArray("result");
                for (int k = 0; k < mResult.length(); k++) {
                    String cms_image = null;
                    String cms_content = null;
                    String cms_date_time = null;
                    String cms_title = null;
                    String cms_description = null;
                    mResultSubs = mResult.getJSONObject(k);
                    cms_image = mResultSubs.getString("cms_image");
                    cms_content = mResultSubs.getString("cms_content");
                    cms_date_time = mResultSubs.getString("cms_updated_at");
                    cms_title = mResultSubs.getString("cms_title");
                    cms_description = mResultSubs.getString("cms_description");
                    // cms_updated_at
                    if ((cms_image != null)) {
                        if (cms_image.trim().length() > 0) {
                            LogUtility.Log(TAG, "parseJsonResponse:feature tag", "cms_image:" + cms_image);
                        } else {
                            LogUtility.Log(TAG, "parseJsonResponse:feature tag:cms_image:is null", null);
                        }
                    }
                    if ((cms_content != null)) {
                        if (cms_content.trim().length() > 0) {
                            LogUtility.Log(TAG, "parseJsonResponse:feature tag", "cms_content:" + cms_content);
                        } else {
                            LogUtility.Log(TAG, "parseJsonResponse:feature tag:cms_content:is null", null);
                        }
                    }
                    if ((cms_date_time != null)) {
                        if (cms_date_time.trim().length() > 0) {
                            LogUtility.Log(TAG, "parseJsonResponse:feature tag", "cms_date_time:" + cms_date_time);
                        } else {
                            LogUtility.Log(TAG, "parseJsonResponse:feature tag:cms_date_time:is null", null);
                        }
                    }
                    if ((cms_title != null)) {
                        if (cms_title.trim().length() > 0) {
                            LogUtility.Log(TAG, "parseJsonResponse:feature tag", "cms_title:" + cms_title);
                        } else {
                            LogUtility.Log(TAG, "parseJsonResponse:feature tag:cms_title:is null", null);
                        }
                    }
                    if ((cms_description != null)) {
                        if (cms_description.trim().length() > 0) {
                            LogUtility.Log(TAG, "parseJsonResponse:feature tag", "cms_description:" + cms_description);
                        } else {
                            LogUtility.Log(TAG, "parseJsonResponse:feature tag:cms_description:is null", null);
                        }
                    }
                    addItemToArraylist(cms_image, mArrayListForCms_images_feature_names);
                    addItemToArraylist(cms_content, mArrayListForCms_images_feature_content);
                    addItemToArraylist(cms_date_time, mArrayListForCms_images_feature_date_time_update);
                    addItemToArraylist(cms_title, mArrayListForCms_images_feature_title);
                    addItemToArraylist(cms_description, mArrayListForCms_images_feature_description);
                }
                ArrayList<String> cms_image_asset_url_feature = new ArrayList<String>();
                for (String s : mArrayListForCms_images_feature_names) {
                    if (s != null) {
                        cms_image_asset_url_feature.add(AppConstants.FEATURE_IMAGES_ASSET_URL + s);
                    } else {
                        cms_image_asset_url_feature.add(null);
                    }
                }
                mPreferenceManager.setCms_image_asset_url_feature(cms_image_asset_url_feature);
                mPreferenceManager.setCms_image_names_feature(mArrayListForCms_images_feature_names);
                mPreferenceManager.setCms_image_content_feature(mArrayListForCms_images_feature_content);
                mPreferenceManager.setCms_image_update_date_feature(mArrayListForCms_images_feature_date_time_update);
                mPreferenceManager.setCms_image_title_feature(mArrayListForCms_images_feature_title);
                mPreferenceManager.setCms_image_description_feature(mArrayListForCms_images_feature_description);
                if (sGetFeatureImagesResponse != null) {
                    sGetFeatureImagesResponse.onFeatureImageResponse(true);
                }
            } else {
                if (sGetFeatureImagesResponse != null) {
                    sGetFeatureImagesResponse.onFeatureImageResponse(false);
                }
            }


            /*mPreferenceManager.getCms_image_names_feature();
            mPreferenceManager.getCms_image_content_feature();
            mPreferenceManager.getCms_image_update_date_feature();
            mPreferenceManager.getCms_image_title_feature();
            mPreferenceManager.getCms_image_description_feature();
            mPreferenceManager.getCms_image_asset_url_feature();*/


        } else if ((tag.equals(TravexApplication.VERIFY_REGISTRATION_TAG)) || (tag.equals(TravexApplication.LOGIN_TAG))) {
            String api_key = null;
            int status = 0;
            String member_name = null;
            String member_email = null;
            String member_id = null;
            String member_profile_image = null;
            String welcome_message = null;
            JSONObject mJsonObject = new JSONObject(object.toString());
            status = mJsonObject.getInt("status");
            if (status == 1) {
                if (tag.equals(TravexApplication.VERIFY_REGISTRATION_TAG)) {
                    LogUtility.Log(TAG, "parseJsonResponse:verify", "status:success:login session set for general account");
                } else {
                    LogUtility.Log(TAG, "parseJsonResponse:login", "status:success:login session set for general account");
                }
                mPreferenceManager.setLogin_Type(TravexApplication.TRAVEX_LOGIN);
                JSONObject mResult = mJsonObject.getJSONObject("result");
                JSONObject MessageObj = mJsonObject.getJSONObject("message");
                member_name = mResult.getString("member_name");
                member_email = mResult.getString("member_email");
                member_id = mResult.getString("member_id");
                member_profile_image= mResult.getString("member_profile_image");
                welcome_message= MessageObj.getString("message");
                if(welcome_message==null)welcome_message="";

                if(!member_profile_image.equals(""))
                {
                    member_profile_image=AppConstants.PROFILE_IMAGE_ASSET_URL+member_profile_image;
                }
                LogUtility.Log(TAG, "parseJsonResponse", "member_name:" + member_name);
                LogUtility.Log(TAG, "parseJsonResponse", "member_email:" + member_email);
                LogUtility.Log(TAG, "parseJsonResponse", "member_id:" + member_id);

                mPreferenceManager.setRegisteredUser_name(member_name);
                mPreferenceManager.setRegisteredUser_email(member_email);
                mPreferenceManager.setRegisration_Id_response(member_id);
                mPreferenceManager.setmember_profile_image(member_profile_image);
                mPreferenceManager.setWelcome_Message(welcome_message);

                api_key = mJsonObject.getString("X-API-KEY");
                LogUtility.Log(TAG, "parseJsonResponse", "api_key:" + api_key);
                mPreferenceManager.setApiKey(api_key);
                LogUtility.Log(TAG, "parseJsonResponse:", "api key from preference:" + mPreferenceManager.getApiKey());
                if (tag.equals(TravexApplication.VERIFY_REGISTRATION_TAG)) {
                    if (sGetVerifyResponse != null)
                        sGetVerifyResponse.onGetVerifyResponse(true);
                } else {
                    if (sGetLoginResponse != null) {
                        sGetLoginResponse.onGetLoginResponse(true);
                    }
                }
            } else {
                if (tag.equals(TravexApplication.VERIFY_REGISTRATION_TAG)) {
                    LogUtility.Log(TAG, "parseJsonResponse:verify", "status:failure");
                    if (sGetVerifyResponse != null)
                        sGetVerifyResponse.onGetVerifyResponse(false);
                } else {
                    LogUtility.Log(TAG, "parseJsonResponse:login", "status:failure");
                    if (sGetLoginResponse != null) {
                        sGetLoginResponse.onGetLoginResponse(false);
                    }
                }
            }
        } else if (tag.equals(TravexApplication.SUGGEST_CITY_TAG)) {
            int status = 0;
            String error_message = null;
            JSONObject jsonObject = new JSONObject(object.toString());
            status = jsonObject.getInt("status");
            if (status == 0) {
                error_message = jsonObject.getJSONObject("result").getString("error_message");
                LogUtility.Log(TAG, "parseJsonResponse:suggest city", "error_message:" + error_message);
                LogUtility.Log(TAG, "parseJsonResponse:suggest city", "status:failure");
                if (sGetCitySuggestionResponse != null)
                    sGetCitySuggestionResponse.onGetCitySuggestion(false);
            } else {
                LogUtility.Log(TAG, "parseJsonResponse:suggest city", "status:success");
                if (sGetCitySuggestionResponse != null)

                    sGetCitySuggestionResponse.onGetCitySuggestion(true);
            }
        } else if (tag.equals(TravexApplication.MEMBER_UPDATE_PROFILE_TAG)) {
            int status = 0;
            JSONObject obj = new JSONObject(object.toString());
            status = obj.getInt("status");
            LogUtility.Log(TAG, "parseJsonResponse:member update", "status:" + status);
            if (status == 1) {
                if (sGetMemberUpdateProfile != null) {
                    sGetMemberUpdateProfile.onGetMemberUpdateProfile(true);
                }
            } else {
                if (sGetMemberUpdateProfile != null) {
                    sGetMemberUpdateProfile.onGetMemberUpdateProfile(false);
                }
            }
        } else if (tag.equals(TravexApplication.LOGOUT_TAG)) {
            int status = 0;
            JSONObject obj = new JSONObject(object.toString());
            status = obj.getInt("status");
            if (status == 1) {
                LogUtility.Log(TAG, "parseJsonResponse:logout ", "status:success");
                mPreferenceManager.setLogin_Type(null);
                mPreferenceManager.setApiKey(null);
                if (sGetTravexLogoutResponse != null) {
                    sGetTravexLogoutResponse.onGetTravexLogoutResponse(true);
                }
            } else {
                LogUtility.Log(TAG, "parseJsonResponse:logout ", "status:failure");
                if (sGetTravexLogoutResponse != null) {
                    sGetTravexLogoutResponse.onGetTravexLogoutResponse(false);
                }
            }
        } else if ((tag.equals(TravexApplication.SOCIAL_LOGIN_FACEBOOK_TAG)) ||
                (tag.equals(TravexApplication.SOCIAL_LOGIN_GOOGLE_TAG))) {
            int status = 0;
            JSONObject obj = new JSONObject(object.toString());
            status = obj.getInt("status");
            String apikey = null;
            String user_id = null;
            JSONObject mResult = null;
            if (status == 1) {
                LogUtility.Log(TAG, "parseJsonResponse:social login [facebook /google]", "status:success");
                apikey = obj.getString("X-API-KEY");
                mResult = obj.getJSONObject("result");
                user_id = mResult.getString("member_id");
                mPreferenceManager.setRegisration_Id_response(user_id);
                mPreferenceManager.setApiKey(apikey);

                LogUtility.Log(TAG, "parseJsonResponse:social login ",
                        "status:success:api key:" + apikey + ",user_id:" + user_id);
                if (tag.equals(TravexApplication.SOCIAL_LOGIN_GOOGLE_TAG)) {
                    if (sGetGoogleLoginResponse != null) {
                        sGetGoogleLoginResponse.onGetGoogleLoginResponse(true);
                    }
                } else {
                    if (sGetFacebookLoginResponse != null) {
                        sGetFacebookLoginResponse.onGetFacebookLoginResponse(true);
                    }
                }
            } else {
                if (tag.equals(TravexApplication.SOCIAL_LOGIN_GOOGLE_TAG)) {
                    if (sGetGoogleLoginResponse != null) {
                        sGetGoogleLoginResponse.onGetGoogleLoginResponse(false);
                    }
                } else {
                    if (sGetFacebookLoginResponse != null) {
                        sGetFacebookLoginResponse.onGetFacebookLoginResponse(false);
                    }
                }
            }
        } else if ((tag.equals(TravexApplication.GET_GENERAL_RESULT_TAG))) {
            LogUtility.Log(TAG, "parseJsonResponse:GET_GENERAL_RESULT_TAG", null);

            HashMap<Integer, ArrayList<String>> map_singularItems = new HashMap<Integer, ArrayList<String>>();
            HashMap<Integer, HashMap<Integer, ArrayList<String>>> jsonObjectArrayItems = new HashMap<Integer, HashMap<Integer, ArrayList<String>>>();
            ArrayList<String> mArrayList_id = new ArrayList<>();
            JSONObject mJsonObject = new JSONObject(object.toString());

            int status = 0;
            status = mJsonObject.getInt("status");
            LogUtility.Log(TAG, "parseJsonResponse:GET_GENERAL_RESULT_TAG", "status:" + status);
            if (status == 1) {

                JSONArray advertisement = mJsonObject.getJSONArray("advertisements");
                for(int a=0;a<advertisement.length();a++){
                    JSONObject adver_obj=advertisement.getJSONObject(a);
//                    "image": "195737001europcar_257x206.png",
//                            "mob_image": "1786772857europcar_500x50.png",
//                            "advertisement_title": "Europcar",
//                            "advertisement_url": "https://www.europcar.com/",
//                            "advertisement_tag_position": "2"
                    advertisement_url=adver_obj.getString("advertisement_url");
                    advertisement_img=adver_obj.getString("mob_image");
                }

                JSONArray result = mJsonObject.getJSONArray("result");
                LogUtility.Log(TAG, "parseJsonResponse:success:parse get results general sesarch api", null);
                String category = null;
                category = mJsonObject.getString("category");
                if ((category != null) && (category != "")) {
                    LogUtility.Log(TAG, "parseJsonResponse:success:parse get results general sesarch api", "category:" + category);
                    mPreferenceManager.setCategory_general_search(category);

                }
                if (result.length() > 0) {
                    for (int i = 0; i < result.length(); i++) {
                        String id = null;
                        String image_name = null;
                        String title = null;
                        int counter_images = -1;
                        int counter_videos = -1;
                        String url_image = null;
                        String latitude = null;
                        String longitude = null;
                        String phone = null;
                        String subtitle = null;
                        String location = null;
                        String review = null;
                        String description = null;
                        String website = null;
                        ArrayList<String> list = new ArrayList<String>();
                        HashMap<Integer, ArrayList<String>> map = new HashMap<Integer, ArrayList<String>>();
                        JSONObject obj = result.getJSONObject(i);
                        id = obj.getString("id");
                        addItemToArraylist(id, mArrayList_id);
                        title = obj.getString("Title");
                        counter_images = obj.getInt("NoOfImages");
                        counter_videos = obj.getInt("NoOfVideos");
                        JSONObject gps = obj.getJSONObject("GPS");
                        latitude = gps.getString("latitude");
                        longitude = gps.getString("longitude");
                        JSONArray phone_array = obj.getJSONArray("PhoneNumber");
                        if (phone_array.length() > 0) {
                            ArrayList<String> phone_numbers = new ArrayList<String>();
                            for (int p = 0; p < phone_array.length(); p++) {
                                phone = phone_array.get(p).toString();
                                addItemToArraylist(phone, phone_numbers);
                            }
                            addArraylistToMap(0, phone_numbers, map);
                        } else {
                            addArraylistToMap(0, null, map);
                        }
                        JSONArray title_images_array = obj.getJSONArray("TitleImages");
                        if (title_images_array.length() > 0) {
                            ArrayList<String> title_images_name_url = new ArrayList<String>();

                            String categoryItem = MapShowingFragment.selected_category;
                            LogUtility.Log(TAG, "parseJsonResponse:", "categoryItem:" + categoryItem);
                            for (int p = 0; p < title_images_array.length(); p++) {
                                image_name = title_images_array.get(p).toString();
                                url_image = AppConstants.GET_RESULTS_GENERAL_IMAGES_URL + "/" + getCategoryItemDirectorynameForImageAssetUrl(categoryItem) + "/" + image_name;
                                LogUtility.Log(TAG, "parseJsonResponse:get general results", "image:url:" + url_image);
                                addItemToArraylist(url_image, title_images_name_url);
                            }
                            addArraylistToMap(1, title_images_name_url, map);
                        } else {
                            addArraylistToMap(1, null, map);
                        }
                        addMapToMap(i, jsonObjectArrayItems, map);
                        subtitle = obj.getString("SubTitle");
                        location = obj.getString("Location");
                        review = obj.getString("Review");
                        description = obj.getString("Description");
                        website = obj.getString("Website");
                        if (image_name != null) {
                            if (image_name.trim().length() > 0)
                                LogUtility.Log(TAG, "parseJsonResponse:", "image_name:" + image_name);
                        }
/**
 * do not change the order of addition below
 */
                        addItemToArraylist(id, list);
                        addItemToArraylist(title, list);
                        addItemToArraylist(latitude, list);
                        addItemToArraylist(longitude, list);
                        addItemToArraylist(subtitle, list);
                        addItemToArraylist(location, list);
                        addItemToArraylist(review, list);
                        if (counter_images > -1) {
                            LogUtility.Log(TAG, "parseJsonResponse:", "counter_images:" + counter_images);
                            addItemToArraylist(String.valueOf(counter_images), list);

                        } else {
                            addItemToArraylist(null, list);
                        }
                        if (counter_videos > -1) {
                            LogUtility.Log(TAG, "parseJsonResponse:", "counter_videos:" + counter_videos);
                            addItemToArraylist(String.valueOf(counter_videos), list);
                        } else {
                            addItemToArraylist(null, list);
                        }
                        addItemToArraylist(description, list);
                        addItemToArraylist(website, list);
                        addArraylistToMap(i, list, map_singularItems);
                    }

                    mPreferenceManager.setSearchResultsGeneral_id(mArrayList_id);
                    mPreferenceManager.getSearchResultsGeneral_id();
                    if (sGetResponseSearchResultsGeneral != null) {
                        sGetResponseSearchResultsGeneral.onGetResponseSearchResultsGeneral(true, map_singularItems, jsonObjectArrayItems,advertisement_img,advertisement_url);
                    }
                } else {
                    if (sGetResponseSearchResultsGeneral != null) {
                        sGetResponseSearchResultsGeneral.onGetResponseSearchResultsGeneral(true, null, null,null,null);
                    }
                }


            } else {
                LogUtility.Log(TAG, "parseJsonResponse:failed:parse get results general sesarch api", null);
                if (sGetResponseSearchResultsGeneral != null) {
                    sGetResponseSearchResultsGeneral.onGetResponseSearchResultsGeneral(false, null, null,null,null);
                }
            }
        } else if (tag.equals(TravexApplication.GET_KEYWORDSEARCH_RESULT_TAG)) {
            LogUtility.Log(TAG, "parseJsonResponse:parse get results keybased sesarch api", "object:" + object);
            int status = 0;
            JSONObject general = new JSONObject(object.toString());
            status = general.getInt("status");
            if (status == 1) {
                HashMap<Integer, ArrayList<String>> result_map = new HashMap<Integer, ArrayList<String>>();
                ArrayList<String> keyword_id_general = new ArrayList<>();
                ArrayList<String> list_suggestion = new ArrayList<>();

                LogUtility.Log(TAG, "parseJsonResponse:key based search:status success", null);
                JSONArray result = general.getJSONArray("result");

                if (result.length() > 0) {
                    for (int i = 0; i < result.length(); i++) {
                        ArrayList<String> list = new ArrayList<String>();
                        String suggestion = null;
                        String type = null;
                        String id = null;
                        JSONObject child = result.getJSONObject(i);
                        suggestion = child.getString("suggestion");
                        type = child.getString("type");
                        id = child.getString("id");
                        addItemToArraylist(id, keyword_id_general);
                        addItemToArraylist(suggestion, list_suggestion);
                      /*if (suggestion != null) {
                        if (suggestion.trim().length() > 0)
                            LogUtility.Log(TAG, "parseJsonResponse:key based search", "suggestion:" + suggestion);
                      }
                    if (id != null) {
                        if (id.trim().length() > 0)
                            LogUtility.Log(TAG, "parseJsonResponse:key based search", "id:" + id);
                    }
                    if (type != null) {
                        if (type.trim().length() > 0)
                            LogUtility.Log(TAG, "parseJsonResponse:key based search", "type:" + type);
                    }*/
                        addItemToArraylist(suggestion, list);
                        addItemToArraylist(type, list);
                        addItemToArraylist(id, list);
                        addArraylistToMap(i, list, result_map);
                    }
                    mPreferenceManager.setSearchResultsKeyWord_id(keyword_id_general);
                    mPreferenceManager.getSearchResultsKeyWord_id();

                    mPreferenceManager.setSearchResultsKeyWord_suggestion(list_suggestion);
                    mPreferenceManager.getSearchResultsKeyWord_suggestion();

                    if (sGetResponseSearchResultsKeyBased != null) {
                        sGetResponseSearchResultsKeyBased.onGetSearchResultsKeyWordBased(true, result_map);
                    }
                } else {
                    if (sGetResponseSearchResultsKeyBased != null) {
                        sGetResponseSearchResultsKeyBased.onGetSearchResultsKeyWordBased(true, null);
                    }
                }


            } else {
                if (sGetResponseSearchResultsKeyBased != null) {
                    sGetResponseSearchResultsKeyBased.onGetSearchResultsKeyWordBased(false, null);
                }
                LogUtility.Log(TAG, "parseJsonResponse:key based search:status failure", null);
            }
        } else if (tag.equals(TravexApplication.NOW_TAG)) {
            LogUtility.Log(TAG, "parseJsonResponse:now tag", "json response:" + object);
            int status = 0;
            JSONObject parent = new JSONObject(object.toString());
            status = parent.getInt("status");
            JSONArray result = parent.getJSONArray("result");
            HashMap<Integer, ArrayList<String>> map_single = new HashMap<>();
            HashMap<Integer, ArrayList<String>> map_phonenumber = new HashMap<>();
            ArrayList<String> mArraylist_general_id = new ArrayList<>();
            String category_id = null;
            if (status == 1) {
                category_id = parent.getString("category");
                LogUtility.Log(TAG, "parseJsonResponse:now tag", "json response:category_id" + category_id);
                mPreferenceManager.setCategory_now_search(category_id);
                for (int i = 0; i < result.length(); i++) {
                    ArrayList<String> list1 = new ArrayList<>();
                    ArrayList<String> list2 = new ArrayList<>();
                    String id = null;
                    String title = null;
                    String latitude = null;
                    String longitude = null;
                    JSONObject result_object = result.getJSONObject(i);
                    id = result_object.getString("id");
                    addItemToArraylist(id, mArraylist_general_id);
                    title = result_object.getString("Title");
                    JSONObject gps = result_object.getJSONObject("GPS");
                    latitude = gps.getString("latitude");
                    longitude = gps.getString("longitude");
                    addItemToArraylist(id, list1);
                    addItemToArraylist(title, list1);
                    addItemToArraylist(latitude, list1);
                    addItemToArraylist(longitude, list1);
                    addArraylistToMap(i, list1, map_single);
                    JSONArray phone_number = result_object.getJSONArray("PhoneNumber");
                    if (phone_number.length() > 0) {
                        for (int k = 0; k < phone_number.length(); k++) {
                            String phone = null;
                            phone = phone_number.get(k).toString();
                            addItemToArraylist(phone, list2);
                        }
                        addArraylistToMap(i, list2, map_phonenumber);
                    } else {
                        addArraylistToMap(i, null, map_phonenumber);
                    }
                }
                mPreferenceManager.setNow_id(mArraylist_general_id);
                mPreferenceManager.getNow_id();
                if (sGetNowResponse != null) {
                    sGetNowResponse.onGetNowResponse(true, map_single, map_phonenumber);
                }
            } else {
                if (sGetNowResponse != null) {
                    sGetNowResponse.onGetNowResponse(false, null, null);
                }
            }
        } else if (tag.equals(TravexApplication.GET_FILTERS_TAG)) {
            LogUtility.Log(TAG, "parseJsonResponse:get filters tag", "json response:" + object);
            int status = 0;
            JSONObject jsonObject = new JSONObject(object.toString());
            status = jsonObject.getInt("status");
            if (status == 1) {
                LogUtility.Log(TAG, "parseJsonResponse:get filters tag:success status", null);
                if (sGetFilterResponse != null) {
                    sGetFilterResponse.onGetFilterResponse(true, object);
                }
            } else {
                LogUtility.Log(TAG, "parseJsonResponse:get filters tag:failure status", null);
                if (sGetFilterResponse != null) {
                    sGetFilterResponse.onGetFilterResponse(false, null);
                }
            }
        } else if (tag.equals(TravexApplication.GET_SEARCHITEM_TAG)) {
            LogUtility.Log(TAG, "parseJsonResponse:get search item tag", "json response:" + object);
            int status = 0;
            JSONObject jsonObject = new JSONObject(object.toString());
            status = jsonObject.getInt("status");
            if (status == 1) {
                LogUtility.Log(TAG, "parseJsonResponse:get search item tag:success status", null);
                if (sGetSearchItemResponse != null) {
                    sGetSearchItemResponse.onGetSearchItemResponse(true, object);
                }
            } else {
                LogUtility.Log(TAG, "parseJsonResponse:get search item tag:failure status", null);
                if (sGetSearchItemResponse != null) {
                    sGetSearchItemResponse.onGetSearchItemResponse(false, null);
                }
            }

        } else if (tag.equals(TravexApplication.FILTERRESULTS_TAG)) {
            LogUtility.Log(TAG, "parseJsonResponse:get filter results  tag", "json response:" + object);
            int status = 0;
            JSONObject jsonObject = new JSONObject(object.toString());
            status = jsonObject.getInt("status");
            if (status == 1) {
                LogUtility.Log(TAG, "parseJsonResponse:get filters results tag:success status", null);
                if (sGetFilterResultsResponse != null) {
                    sGetFilterResultsResponse.onGetFilterResultsResponse(true, object);
                }
            } else {
                LogUtility.Log(TAG, "parseJsonResponse:get filters results  tag:failure status", null);
                if (sGetFilterResultsResponse != null) {
                    sGetFilterResultsResponse.onGetFilterResultsResponse(false, null);
                }
            }
        } else if ((tag.equals(TravexApplication.GET_ARTICLES_LIST_FULLFLEDGE_TAG)) ||
                (tag.equals(TravexApplication.GET_ARTICLES_LIST_CATEGORYBASED_TAG))
                ||
                (tag.equals(TravexApplication.GET_ARTICLES_LIST_FEATURED_TAG))) {
            LogUtility.Log(TAG, "parseJsonResponse:get articles list fullfledge/category based/featured  tag", "json response:" + object);
            int status = 0;
            JSONObject jsonObject = new JSONObject(object.toString());
            status = jsonObject.getInt("status");


            if (status == 1) {
                JSONArray result = jsonObject.getJSONArray("result");
                ArrayList<String> mArrayList_articles_id = new ArrayList<>();
                if (result.length() > 0) {
                    for (int i = 0; i < result.length(); i++) {
                        String articles_id = null;
                        JSONObject child = result.getJSONObject(i);
                        articles_id = child.getString("articles_id");
                        addItemToArraylist(articles_id, mArrayList_articles_id);
                    }
                    mPreferenceManager.setArticles_id(mArrayList_articles_id);
                    mPreferenceManager.getArticles_id();
                } else {
                    LogUtility.Log(TAG, "parseJsonResponse:json array is null", null);

                }

                LogUtility.Log(TAG, "parseJsonResponse:get articles list fullfledge /category based/featured  tag:success status", null);
                if (sGetArticlesListResponse != null) {
                    sGetArticlesListResponse.onGetArticlesListResponse(true, object);
                }
            } else {
                LogUtility.Log(TAG, "parseJsonResponse:get  articles list fullfledge/category based/featured   tag:failure status", null);
                if (sGetArticlesListResponse != null) {
                    sGetArticlesListResponse.onGetArticlesListResponse(false, object);
                }
            }
        } else if (tag.equals(TravexApplication.GET_ARTICLES_DETAILS_TAG)) {
            LogUtility.Log(TAG, "parseJsonResponse:get articles details  tag", "json response:" + object);
            int status = 0;
            JSONObject jsonObject = new JSONObject(object.toString());
            status = jsonObject.getInt("status");
            if (status == 1) {
                LogUtility.Log(TAG, "parseJsonResponse:get articles detailstag:success status", null);
                if (sGetArticleDetailsResponse != null) {
                    sGetArticleDetailsResponse.onGetArticlesDetailsResponse(true, object);
                }
            } else {
                LogUtility.Log(TAG, "parseJsonResponse:get articles details  tag:failure status", null);
                if (sGetArticleDetailsResponse != null) {
                    sGetArticleDetailsResponse.onGetArticlesDetailsResponse(false, null);
                }
            }
        } else if (tag.equals(TravexApplication.GET_ESSENTIALS_LIST_TAG)) {
            LogUtility.Log(TAG, "parseJsonResponse:get essentials list  tag", "json response:" + object);
            int status = 0;
            JSONObject jsonObject = new JSONObject(object.toString());
            status = jsonObject.getInt("status");

            if (status == 1) {
                JSONArray result = jsonObject.getJSONArray("result");
                ArrayList<String> mArrayList_essentials_id = new ArrayList<>();
                if (result.length() > 0) {
                    for (int i = 0; i < result.length(); i++) {
                        String essentilas_id = null;
                        JSONObject child = result.getJSONObject(i);
                        essentilas_id = child.getString("essentials_id");
                        addItemToArraylist(essentilas_id, mArrayList_essentials_id);
                    }
                    mPreferenceManager.setEssentials_id(mArrayList_essentials_id);
                    mPreferenceManager.getEssentials_id();
                } else {
                    LogUtility.Log(TAG, "parseJsonResponse:get     essentials list   tag:result array is null", null);
                }

                LogUtility.Log(TAG, "parseJsonResponse:get     essentials list       tag:success status", null);
                if (sGetEssentialListResponse != null) {
                    sGetEssentialListResponse.onGetEssentialsListResponse(true, object);
                }
            } else {
                LogUtility.Log(TAG, "parseJsonResponse:get  essentials list  tag:failure status", null);
                if (sGetEssentialListResponse != null) {
                    sGetEssentialListResponse.onGetEssentialsListResponse(false, object);
                }
            }
        } else if (tag.equals(TravexApplication.GET_ESSENTIALS_DETAILS_TAG)) {
            LogUtility.Log(TAG, "parseJsonResponse:get essentials details  tag", "json response:" + object);
            int status = 0;
            JSONObject jsonObject = new JSONObject(object.toString());
            status = jsonObject.getInt("status");
            if (status == 1) {
                LogUtility.Log(TAG, "parseJsonResponse:get essentials details tag:success status", null);
                if (sGetEssentialsDetailsResponse != null) {
                    sGetEssentialsDetailsResponse.onGetEssentialsDetailsResponse(true, object);
                }
            } else {
                LogUtility.Log(TAG, "parseJsonResponse:get essentials details  tag:failure status", null);
                if (sGetEssentialsDetailsResponse != null) {
                    sGetEssentialsDetailsResponse.onGetEssentialsDetailsResponse(false, null);
                }
            }
        } else if (tag.equals(TravexApplication.GET_TIPS_LIST_TAG)) {
            LogUtility.Log(TAG, "parseJsonResponse:get tips list  tag", "json response:" + object);
            int status = 0;
            JSONObject jsonObject = new JSONObject(object.toString());
            status = jsonObject.getInt("status");

            if (status == 1) {
                JSONArray result = jsonObject.getJSONArray("result");
                ArrayList<String> mArrayList_tips_id = new ArrayList<>();

                if (result.length() > 0) {
                    for (int i = 0; i < result.length(); i++) {
                        String tips_id = null;
                        JSONObject child = result.getJSONObject(i);
                        tips_id = child.getString("tips_id");
                        addItemToArraylist(tips_id, mArrayList_tips_id);
                    }
                    mPreferenceManager.setTips_id(mArrayList_tips_id);
                    mPreferenceManager.getTips_id();
                    LogUtility.Log(TAG, "parseJsonResponse:get     tips list       tag:success status", null);
                } else {
                    LogUtility.Log(TAG, "parseJsonResponse:get     tips list   tag:result array is null", null);

                }

                if (sGetTipsListResponse != null) {
                    sGetTipsListResponse.onGetTipsListResponse(true, object);
                }
            } else {
                LogUtility.Log(TAG, "parseJsonResponse:get  tips list  tag:failure status", null);
                if (sGetTipsListResponse != null) {
                    sGetTipsListResponse.onGetTipsListResponse(false, null);
                }
            }
        } else if (tag.equals(TravexApplication.GET_TIPS_DETAILS_TAG)) {
            LogUtility.Log(TAG, "parseJsonResponse:get tips details  tag", "json response:" + object);
            int status = 0;
            JSONObject jsonObject = new JSONObject(object.toString());
            status = jsonObject.getInt("status");
            if (status == 1) {
                LogUtility.Log(TAG, "parseJsonResponse:get tips details tag:success status", null);
                if (sGetTipsDetailsResponse != null) {
                    sGetTipsDetailsResponse.onGetTipsDetailsResponse(true, object);
                }
            } else {
                LogUtility.Log(TAG, "parseJsonResponse:get tips details  tag:failure status", null);
                if (sGetEssentialsDetailsResponse != null) {
                    sGetTipsDetailsResponse.onGetTipsDetailsResponse(false, null);
                }
            }
        } else if (tag.equals(TravexApplication.ADD_PLAN_TAG)) {
            LogUtility.Log(TAG, "parseJsonResponse:add plan  tag", "json response:" + object);
            int status = 0;
            JSONObject jsonObject = new JSONObject(object.toString());
            status = jsonObject.getInt("status");
            if (status == 1) {
                LogUtility.Log(TAG, "parseJsonResponse:add plan  tag:success status", null);
                if (sAddPlanResponse != null) {
                    sAddPlanResponse.onAddPlanResponse(true);
                }
            } else {
                LogUtility.Log(TAG, "parseJsonResponse:add plan   tag:failure status", null);
                if (sAddPlanResponse != null) {
                    sAddPlanResponse.onAddPlanResponse(false);
                }
            }
        } else if (tag.equals(TravexApplication.LIST_MYPLAN_TAG)) {
            LogUtility.Log(TAG, "parseJsonResponse:list plan  tag", "json response:" + object);
            int status = 0;
            JSONObject jsonObject = new JSONObject(object.toString());
            status = jsonObject.getInt("status");


            if (status == 1) {
                LogUtility.Log(TAG, "parseJsonResponse:list plan  tag:success status", null);
                JSONArray result_array = jsonObject.getJSONArray("result");

                ArrayList<String> list = new ArrayList<>();
                if (result_array.length() > 0) {
                    for (int i = 0; i < result_array.length(); i++) {
                        JSONObject child = result_array.getJSONObject(i);
                        String plan_id = child.getString("plan_id");
                        list.add(plan_id);
                    }
                    mPreferenceManager.setPlan_id(list);
                    mPreferenceManager.getPlan_id();
                } else {
                    LogUtility.Log(TAG, "parseJsonResponse:get      list plan  tag:result array is null", null);
                }


                if (sListPlanResponse != null) {
                    sListPlanResponse.onGetListPlanResponse(true, object);
                }
            } else {
                LogUtility.Log(TAG, "parseJsonResponse:add plan   tag:failure status", null);
                if (sListPlanResponse != null) {
                    sListPlanResponse.onGetListPlanResponse(false, null);
                }
            }
        } else if (tag.equals(TravexApplication.ADD_SUGGESTIONS_TAG)) {
            LogUtility.Log(TAG, "parseJsonResponse:add suggestions tag", "json response:" + object);
            int status = 0;
            JSONObject jsonObject = new JSONObject(object.toString());
            status = jsonObject.getInt("status");
            if (status == 1) {
                LogUtility.Log(TAG, "parseJsonResponse:add suggestions  tag:success status", null);
                if (sAddSuggestionsResponse != null) {
                    sAddSuggestionsResponse.onAddSuggestionsResponse(true);
                }
            } else {
                LogUtility.Log(TAG, "parseJsonResponse:add suggestions   tag:failure status", null);
                if (sAddSuggestionsResponse != null) {
                    sAddSuggestionsResponse.onAddSuggestionsResponse(false);
                }
            }
        } else if (tag.equals(TravexApplication.ABOUT_US_TAG)) {
            LogUtility.Log(TAG, "parseJsonResponse:about us tag", "json response:" + object);
            int status = 0;
            JSONObject jsonObject = new JSONObject(object.toString());
            status = jsonObject.getInt("status");
            if (status == 1) {
                LogUtility.Log(TAG, "parseJsonResponse:about us  tag:success status", null);
                JSONObject result_obj = jsonObject.getJSONObject("result");
                JSONArray description = result_obj.getJSONArray("description");
                JSONArray team = result_obj.getJSONArray("team");
                HashMap<Integer, HashMap<Integer, ArrayList<String>>> map_target = new HashMap<>();
                if (description.length() > 0) {
                    HashMap<Integer, ArrayList<String>> map_description = new HashMap<>();
                    // traverse the description
                    for (int i = 0; i < description.length(); i++) {
                        ArrayList<String> list = new ArrayList<>();
                        JSONObject traverse = description.getJSONObject(i);
                        String cms_content = null;
                        String cms_image = null;
                        String cms_title = null;
                        String cms_description = null;
                        String cms_updated_at = null;
                        String url_image = null;
                        cms_content = traverse.getString("cms_content");
                        cms_image = traverse.getString("cms_image");
                        cms_title = traverse.getString("cms_title");
                        cms_description = traverse.getString("cms_description");
                        cms_updated_at = traverse.getString("cms_updated_at");
                        if (cms_image != null) {
                            if (cms_image != "") {
                                url_image = AppConstants.ABOUT_US_IMAGES_ASSETS_URL + cms_image;
                            }
                        }
                        addItemToArraylist(cms_content, list);
                        addItemToArraylist(url_image, list);
                        addItemToArraylist(cms_title, list);
                        addItemToArraylist(cms_description, list);
                        addItemToArraylist(cms_updated_at, list);
                        addArraylistToMap(i, list, map_description);
                    }
                    addMapToMap(0, map_target, map_description);
                } else {
                    LogUtility.Log(TAG, "parseJsonResponse:about us  tag:success status:description map is null", null);
                    addMapToMap(0, map_target, null);
                }
                if (team.length() > 0) {
                    HashMap<Integer, ArrayList<String>> map_team = new HashMap<>();
                    // traverse the team
                    for (int i = 0; i < team.length(); i++) {
                        ArrayList<String> list = new ArrayList<>();
                        JSONObject traverse = team.getJSONObject(i);
                        String cms_content = null;
                        String cms_image = null;
                        String cms_title = null;
                        String cms_description = null;
                        String cms_updated_at = null;
                        String url_image = null;
                        cms_content = traverse.getString("cms_content");
                        cms_image = traverse.getString("cms_image");
                        cms_title = traverse.getString("cms_title");
                        cms_description = traverse.getString("cms_description");
                        cms_updated_at = traverse.getString("cms_updated_at");
                        if (cms_image != null) {
                            if (cms_image != "") {
                                url_image = AppConstants.ABOUT_US_IMAGES_ASSETS_URL + cms_image;
                            }
                        }
                        addItemToArraylist(cms_content, list);
                        addItemToArraylist(url_image, list);
                        addItemToArraylist(cms_title, list);
                        addItemToArraylist(cms_description, list);
                        addItemToArraylist(cms_updated_at, list);
                        addArraylistToMap(i, list, map_team);
                    }
                    addMapToMap(1, map_target, map_team);
                } else {
                    LogUtility.Log(TAG, "parseJsonResponse:about us  tag:success status:team map is null", null);
                    addMapToMap(1, map_target, null);
                }
                if (sAboutUsResponse != null) {
                    sAboutUsResponse.onGetAboutUsResponse(true, map_target);
                }
            } else {
                LogUtility.Log(TAG, "parseJsonResponse:add suggestions   tag:failure status", null);
                if (sAboutUsResponse != null) {
                    sAboutUsResponse.onGetAboutUsResponse(false, null);
                }
            }
        } else if (tag.equals(TravexApplication.CONTACT_US_TAG)) {
            LogUtility.Log(TAG, "parseJsonResponse:contact us tag", "json response:" + object);
            int status = 0;
            JSONObject jsonObject = new JSONObject(object.toString());
            status = jsonObject.getInt("status");
            if (status == 1) {
                LogUtility.Log(TAG, "parseJsonResponse:about us  tag:success status", null);
                JSONObject result_obj = jsonObject.getJSONObject("result");


                JSONArray location = result_obj.getJSONArray("location");
                JSONArray phone = result_obj.getJSONArray("phone");
                JSONArray facebook = result_obj.getJSONArray("facebook");
                JSONArray twitter = result_obj.getJSONArray("twitter");
                JSONArray gplus = result_obj.getJSONArray("gplus");
                JSONArray instagram = result_obj.getJSONArray("instagram");


                JSONArray pinterest = result_obj.getJSONArray("pinterest");
                LogUtility.Log(TAG, "parseJsonResponse:about us", "location:" + location.toString());
                LogUtility.Log(TAG, "parseJsonResponse:about us", "phone:" + phone.toString());
                LogUtility.Log(TAG, "parseJsonResponse:about us", "facebook:" + facebook.toString());
                LogUtility.Log(TAG, "parseJsonResponse:about us", "twitter:" + twitter.toString());
                LogUtility.Log(TAG, "parseJsonResponse:about us", "gplus:" + gplus.toString());
                LogUtility.Log(TAG, "parseJsonResponse:about us", "instagram:" + instagram.toString());
                LogUtility.Log(TAG, "parseJsonResponse:about us", "pinterest:" + pinterest.toString());


                HashMap<Integer, HashMap<Integer, ArrayList<String>>> map_target = new HashMap<>();
                if (location.length() > 0) {
                    HashMap<Integer, ArrayList<String>> map_location = new HashMap<>();
                    // traverse the description
                    for (int i = 0; i < location.length(); i++) {
                        ArrayList<String> list = new ArrayList<>();
                        JSONObject traverse = location.getJSONObject(i);
                        String cms_content = null;
                        String cms_title = null;
                        String cms_description = null;
                        String cms_updated_at = null;
                        cms_content = traverse.getString("cms_content");
                        cms_title = traverse.getString("cms_title");
                        cms_description = traverse.getString("cms_description");
                        cms_updated_at = traverse.getString("cms_updated_at");
                        LogUtility.Log(TAG, "parseJsonResponse:about us", "location details:cms_content" + cms_content);
                        LogUtility.Log(TAG, "parseJsonResponse:about us", "location details:cms_title" + cms_title);
                        LogUtility.Log(TAG, "parseJsonResponse:about us", "location details:cms_description" + cms_description);
                        LogUtility.Log(TAG, "parseJsonResponse:about us", "location details:cms_updated_at" + cms_updated_at);

                        addItemToArraylist(cms_content, list);
                        addItemToArraylist(cms_title, list);
                        addItemToArraylist(cms_description, list);
                        addItemToArraylist(cms_updated_at, list);
                        addArraylistToMap(i, list, map_location);
                    }
                    addMapToMap(0, map_target, map_location);
                } else {
                    LogUtility.Log(TAG, "parseJsonResponse:contact us  tag:success status:location map is null", null);
                    addMapToMap(0, map_target, null);
                }

                /**
                 * phone json array management
                 */
                if (phone.length() > 0) {
                    HashMap<Integer, ArrayList<String>> map_phone = new HashMap<>();
                    // traverse the description
                    for (int i = 0; i < phone.length(); i++) {
                        ArrayList<String> list = new ArrayList<>();
                        JSONObject traverse = phone.getJSONObject(i);
                        String cms_content = null;
                        String cms_title = null;
                        String cms_description = null;
                        String cms_updated_at = null;
                        cms_content = traverse.getString("cms_content");
                        cms_title = traverse.getString("cms_title");
                        cms_description = traverse.getString("cms_description");
                        cms_updated_at = traverse.getString("cms_updated_at");

                        addItemToArraylist(cms_content, list);
                        addItemToArraylist(cms_title, list);
                        addItemToArraylist(cms_description, list);
                        addItemToArraylist(cms_updated_at, list);
                        addArraylistToMap(i, list, map_phone);
                    }
                    addMapToMap(1, map_target, map_phone);
                } else {
                    LogUtility.Log(TAG, "parseJsonResponse:contact us  tag:success status:phone map is null", null);
                    addMapToMap(1, map_target, null);
                }

/**
 * facebook json array management
 */
                if (facebook.length() > 0) {
                    HashMap<Integer, ArrayList<String>> map_facebook = new HashMap<>();
                    // traverse the description
                    for (int i = 0; i < facebook.length(); i++) {
                        ArrayList<String> list = new ArrayList<>();
                        JSONObject traverse = facebook.getJSONObject(i);
                        String cms_content = null;
                        String cms_title = null;
                        String cms_description = null;
                        String cms_updated_at = null;
                        cms_content = traverse.getString("cms_content");
                        cms_title = traverse.getString("cms_title");
                        cms_description = traverse.getString("cms_description");
                        cms_updated_at = traverse.getString("cms_updated_at");

                        addItemToArraylist(cms_content, list);
                        addItemToArraylist(cms_title, list);
                        addItemToArraylist(cms_description, list);
                        addItemToArraylist(cms_updated_at, list);
                        addArraylistToMap(i, list, map_facebook);
                    }
                    addMapToMap(2, map_target, map_facebook);
                } else {
                    LogUtility.Log(TAG, "parseJsonResponse:contact us  tag:success status:facebook map is null", null);
                    addMapToMap(2, map_target, null);
                }


                /**
                 * twitter json array management
                 */
                if (twitter.length() > 0) {
                    HashMap<Integer, ArrayList<String>> map_twitter = new HashMap<>();
                    // traverse the description
                    for (int i = 0; i < twitter.length(); i++) {
                        ArrayList<String> list = new ArrayList<>();
                        JSONObject traverse = twitter.getJSONObject(i);
                        String cms_content = null;
                        String cms_title = null;
                        String cms_description = null;
                        String cms_updated_at = null;
                        cms_content = traverse.getString("cms_content");
                        cms_title = traverse.getString("cms_title");
                        cms_description = traverse.getString("cms_description");
                        cms_updated_at = traverse.getString("cms_updated_at");

                        addItemToArraylist(cms_content, list);
                        addItemToArraylist(cms_title, list);
                        addItemToArraylist(cms_description, list);
                        addItemToArraylist(cms_updated_at, list);
                        addArraylistToMap(i, list, map_twitter);
                    }
                    addMapToMap(3, map_target, map_twitter);
                } else {
                    LogUtility.Log(TAG, "parseJsonResponse:contact us  tag:success status:twitter map is null", null);
                    addMapToMap(3, map_target, null);
                }
/**
 * gplus json array management
 */
                if (gplus.length() > 0) {
                    HashMap<Integer, ArrayList<String>> map_gplus = new HashMap<>();
                    // traverse the description
                    for (int i = 0; i < gplus.length(); i++) {
                        ArrayList<String> list = new ArrayList<>();
                        JSONObject traverse = gplus.getJSONObject(i);
                        String cms_content = null;
                        String cms_title = null;
                        String cms_description = null;
                        String cms_updated_at = null;
                        cms_content = traverse.getString("cms_content");
                        cms_title = traverse.getString("cms_title");
                        cms_description = traverse.getString("cms_description");
                        cms_updated_at = traverse.getString("cms_updated_at");

                        addItemToArraylist(cms_content, list);
                        addItemToArraylist(cms_title, list);
                        addItemToArraylist(cms_description, list);
                        addItemToArraylist(cms_updated_at, list);
                        addArraylistToMap(i, list, map_gplus);
                    }
                    addMapToMap(4, map_target, map_gplus);
                } else {
                    LogUtility.Log(TAG, "parseJsonResponse:contact us  tag:success status:gplus map is null", null);
                    addMapToMap(4, map_target, null);
                }

                /**
                 * instagram json array management
                 */
                if (instagram.length() > 0) {
                    HashMap<Integer, ArrayList<String>> map_instagram = new HashMap<>();
                    // traverse the description
                    for (int i = 0; i < instagram.length(); i++) {
                        ArrayList<String> list = new ArrayList<>();
                        JSONObject traverse = instagram.getJSONObject(i);
                        String cms_content = null;
                        String cms_title = null;
                        String cms_description = null;
                        String cms_updated_at = null;
                        cms_content = traverse.getString("cms_content");
                        cms_title = traverse.getString("cms_title");
                        cms_description = traverse.getString("cms_description");
                        cms_updated_at = traverse.getString("cms_updated_at");

                        addItemToArraylist(cms_content, list);
                        addItemToArraylist(cms_title, list);
                        addItemToArraylist(cms_description, list);
                        addItemToArraylist(cms_updated_at, list);
                        addArraylistToMap(i, list, map_instagram);
                    }
                    addMapToMap(5, map_target, map_instagram);
                } else {
                    LogUtility.Log(TAG, "parseJsonResponse:contact us  tag:success status:instagram map is null", null);
                    addMapToMap(5, map_target, null);
                }
                /**
                 * instagram json array management
                 */
                if (pinterest.length() > 0) {
                    HashMap<Integer, ArrayList<String>> map_pinterest = new HashMap<>();
                    // traverse the description
                    for (int i = 0; i < pinterest.length(); i++) {
                        ArrayList<String> list = new ArrayList<>();
                        JSONObject traverse = pinterest.getJSONObject(i);
                        String cms_content = null;
                        String cms_title = null;
                        String cms_description = null;
                        String cms_updated_at = null;
                        cms_content = traverse.getString("cms_content");
                        cms_title = traverse.getString("cms_title");
                        cms_description = traverse.getString("cms_description");
                        cms_updated_at = traverse.getString("cms_updated_at");

                        addItemToArraylist(cms_content, list);
                        addItemToArraylist(cms_title, list);
                        addItemToArraylist(cms_description, list);
                        addItemToArraylist(cms_updated_at, list);
                        addArraylistToMap(i, list, map_pinterest);
                    }
                    addMapToMap(6, map_target, map_pinterest);
                } else {
                    LogUtility.Log(TAG, "parseJsonResponse:contact us  tag:success status:pinterest map is null", null);
                    addMapToMap(6, map_target, null);
                }


                if (sContactUsResponse != null) {
                    sContactUsResponse.onContactUsResponse(true, map_target);
                }
            } else {
                LogUtility.Log(TAG, "parseJsonResponse:add suggestions   tag:failure status", null);
                if (sContactUsResponse != null) {
                    sContactUsResponse.onContactUsResponse(false, null);
                }
            }
        } else if (tag.equals(TravexApplication.ENQUIRY_TAG)) {
            LogUtility.Log(TAG, "parseJsonResponse:enquiry  tag:", "json obj:" + object);
            int status = 0;
            JSONObject jsonObject = new JSONObject(object.toString());
            status = jsonObject.getInt("status");
            if (status == 1) {
                LogUtility.Log(TAG, "parseJsonResponse:enquiry  tag:success", null);
                if (sEnquiryResponse != null) {
                    sEnquiryResponse.onEnquiryResponse(true);
                }
                sEnquiryResponse.onEnquiryResponse(true);
            } else if (status == 0) {
                LogUtility.Log(TAG, "parseJsonResponse:enquiry  tag:failure", null);
                if (sEnquiryResponse != null) {
                    sEnquiryResponse.onEnquiryResponse(false);
                }
            }
        } else if (tag.equals(TravexApplication.UPDATE_PLAN_TAG)) {

            LogUtility.Log(TAG, "parseJsonResponse:update plan  tag:", "json obj:" + object);

            int status = 0;
            JSONObject jsonObject = new JSONObject(object.toString());
            status = jsonObject.getInt("status");
            if (status == 1) {
                sUpdatePlanResponse.onUpdatePlanResponse(true);
            } else {
                sUpdatePlanResponse.onUpdatePlanResponse(false);

            }


        } else if (tag.equals(TravexApplication.DELETE_PLAN_TAG)) {

            LogUtility.Log(TAG, "parseJsonResponse:delete plan  tag:", "json obj:" + object);

            int status = 0;
            JSONObject jsonObject = new JSONObject(object.toString());
            status = jsonObject.getInt("status");
            if (status == 1) {

                if (sDeletePlanResponse != null)
                    sDeletePlanResponse.onDeletePlanResponse(true);
            } else {
                if (sDeletePlanResponse != null)

                    sDeletePlanResponse.onDeletePlanResponse(false);

            }


        } else if (tag.equals(TravexApplication.DONE_PLAN_TAG)) {

            LogUtility.Log(TAG, "parseJsonResponse:done plan  tag:", "json obj:" + object);

            int status = 0;
            JSONObject jsonObject = new JSONObject(object.toString());
            status = jsonObject.getInt("status");
            if (status == 1) {

                if (sGetDonePlanResponse != null)
                    sGetDonePlanResponse.onGetDonePlanResponse(true);
            } else {
                if (sGetDonePlanResponse != null)

                    sGetDonePlanResponse.onGetDonePlanResponse(false);

            }


        } else if (tag.equals(TravexApplication.MEMBER_UPDATE_PROFILE_TAG)) {

            LogUtility.Log(TAG, "parseJsonResponse:member update profile  tag:", "json obj:" + object);

            int status = 0;
            JSONObject jsonObject = new JSONObject(object.toString());
            status = jsonObject.getInt("status");
            /*if (status == 1) {

                if (sGetDonePlanResponse != null)
                    sGetDonePlanResponse.onGetDonePlanResponse(true);
            } else {
                if (sGetDonePlanResponse != null)

                    sGetDonePlanResponse.onGetDonePlanResponse(false);

            }*/


        } else if (tag.equals(TravexApplication.MEMBER_DETAILS_TAG)) {

            LogUtility.Log(TAG, "parseJsonResponse:member details   tag:", "json obj:" + object);

            int status = 0;
            JSONObject jsonObject = new JSONObject(object.toString());
            status = jsonObject.getInt("status");
            if (status == 1) {

                if (sGetMemberDetailsResponse != null)
                    sGetMemberDetailsResponse.onGetMemberDetailsResponse(true, jsonObject);
            } else {
                if (sGetMemberDetailsResponse != null)
                    sGetMemberDetailsResponse.onGetMemberDetailsResponse(false, null);

            }


        } else if (tag.equals(TravexApplication.EVENT_DETAILS_TAG)) {

            LogUtility.Log(TAG, "parseJsonResponse: event details  tag:", "json obj:" + object);

            int status = 0;
            JSONObject jsonObject = new JSONObject(object.toString());
            status = jsonObject.getInt("status");
            if (status == 1) {

                if (sGetEventDetailsResponse != null)
                    sGetEventDetailsResponse.onGetEventDetailsResponse(true, jsonObject);
            } else {
                if (sGetEventDetailsResponse != null)
                    sGetEventDetailsResponse.onGetEventDetailsResponse(false, null);

            }


        } else if (tag.equals(TravexApplication.WRITE_REVIEW_TAG)) {

            LogUtility.Log(TAG, "parseJsonResponse: write review  tag:", "json obj:" + object);

            int status = 0;
            JSONObject jsonObject = new JSONObject(object.toString());
            status = jsonObject.getInt("status");
            if (status == 1) {

                if (sGetWriteReviewResponse != null)
                    sGetWriteReviewResponse.onGetWriteReviewResponse(true);
            } else {
                if (sGetWriteReviewResponse != null)
                    sGetWriteReviewResponse.onGetWriteReviewResponse(false);

            }


        } else if (tag.equals(TravexApplication.TRAVEX_EVENTS_AND_MONTHLY_PLAN_TAG)) {
            int status = 0;
            JSONObject jsonObject = new JSONObject(object.toString());
            status = jsonObject.getInt("status");


            if ((status == 1) ||
                    (status == 2) ||
                    (status == 3) ||
                    (status == 4)) {
                JSONObject mJsonObject_result = jsonObject.getJSONObject("result");
                if (status == 1) {
                    LogUtility.Log(TAG, "parseJsonResponse:travex event and monthly plan  tag:", "both lists and events available");

                } else if (status == 2) {
                    LogUtility.Log(TAG, "parseJsonResponse:travex event and monthly plan  tag:", "both lists and events not available");

                } else if (status == 3) {
                    LogUtility.Log(TAG, "parseJsonResponse:travex event and monthly plan  tag:", " lists not available and events available");

                } else {
                    LogUtility.Log(TAG, "parseJsonResponse:travex event and monthly plan  tag:", " lists available and events not available");

                }
                handleForMonthlyPlan(mJsonObject_result, status);
                handleForTravexEvent(mJsonObject_result, status);
                if (sGetTravexEventPlanResponse != null) {
                    sGetTravexEventPlanResponse.onGetTravexEventAndMonthlyPlanResponse(true, monthly_plan_list, travex_events);
                }

            } else {
                LogUtility.Log(TAG, "parseJsonResponse:travex event and monthly plan  tag:", "failed");
                if (sGetTravexEventPlanResponse != null) {
                    sGetTravexEventPlanResponse.onGetTravexEventAndMonthlyPlanResponse(false, null, null);
                }


            }


        } else if (tag.equals(TravexApplication.DAY_PLAN_TAG)) {

            LogUtility.Log(TAG, "parseJsonResponse:day plan  tag:", "json obj:" + object);

            int status = 0;
            JSONObject jsonObject = new JSONObject(object.toString());
            status = jsonObject.getInt("status");
            if (status == 1) {

                JSONArray result = jsonObject.getJSONArray("result");
                ArrayList<String> mArrayList_plan_id = null;
                ArrayList<String> mArrayList_category = null;
                ArrayList<String> mArrayList_category_id = null;
                ArrayList<String> mArrayList_item_id = null;
                ArrayList<String> mArrayList_item_name = null;
                ArrayList<String> mArrayList_user_id = null;
                ArrayList<String> mArrayList_date = null;
                ArrayList<String> mArrayList_time = null;
                ArrayList<String> mArrayList_description = null;
                ArrayList<String> mArrayList_latitude = null;
                ArrayList<String> mArrayList_longitude = null;
                ArrayList<String> mArrayList_complete = null;
                ArrayList<String> mArrayList_phone = null;
                ArrayList<String> mArrayList_image_url = null;
                HashMap<String, ArrayList<String>> target_map = null;

                if (result.length() > 0) {
                    mArrayList_plan_id = new ArrayList<>();
                    mArrayList_category = new ArrayList<>();
                    mArrayList_category_id = new ArrayList<>();
                    mArrayList_item_id = new ArrayList<>();
                    mArrayList_item_name = new ArrayList<>();
                    mArrayList_user_id = new ArrayList<>();
                    mArrayList_date = new ArrayList<>();
                    mArrayList_time = new ArrayList<>();
                    mArrayList_description = new ArrayList<>();
                    mArrayList_latitude = new ArrayList<>();
                    mArrayList_longitude = new ArrayList<>();
                    mArrayList_complete = new ArrayList<>();
                    mArrayList_phone = new ArrayList<>();
                    mArrayList_image_url = new ArrayList<>();
                    target_map = new HashMap<>();

                    for (int k = 0; k < result.length(); k++) {
                        JSONObject mJsonObject_result = result.getJSONObject(k);
                        String plan_id = null;
                        String category = null;
                        String category_id = null;
                        String item_id = null;
                        String item_name = null;
                        String user_id = null;
                        String date = null;
                        String time = null;
                        String description = null;
                        String latitude = null;
                        String longitude = null;
                        String complete = null;
                        String phone = null;
                        String image_url = null;
                        plan_id = mJsonObject_result.getString("plan_id");
                        LogUtility.Log(TAG, "parseJsonResponse:day plan  tag:", "plan_id:" + plan_id);

                        category = mJsonObject_result.getString("category");
                        LogUtility.Log(TAG, "parseJsonResponse:day plan  tag:", "category:" + category);

                        //  category_id = mJsonObject_result.getString("category_id");
                        item_id = mJsonObject_result.getString("item_id");
                        LogUtility.Log(TAG, "parseJsonResponse:day plan  tag:", "item_id:" + item_id);

                        item_name = mJsonObject_result.getString("item_name");
                        LogUtility.Log(TAG, "parseJsonResponse:day plan  tag:", "item_name:" + item_name);

                        user_id = mJsonObject_result.getString("user_id");
                        LogUtility.Log(TAG, "parseJsonResponse:day plan  tag:", "user_id:" + user_id);

                        date = mJsonObject_result.getString("date");
                        LogUtility.Log(TAG, "parseJsonResponse:day plan  tag:", "date:" + date);

                        time = mJsonObject_result.getString("time");
                        LogUtility.Log(TAG, "parseJsonResponse:day plan  tag:", "time:" + time);

                        description = mJsonObject_result.getString("description");
                        latitude = mJsonObject_result.getString("latitude");
                        longitude = mJsonObject_result.getString("longitude");
                        complete = mJsonObject_result.getString("complete");
                        phone = mJsonObject_result.getString("phone");
                        image_url = mJsonObject_result.getString("image");
                        LogUtility.Log(TAG, "parseJsonResponse:day plan  tag:", "image_url:" + image_url);

                        addItemToArraylist(plan_id, mArrayList_plan_id);
                        addItemToArraylist(category, mArrayList_category);
                        //   addItemToArraylist(category_id, mArrayList_category_id);
                        addItemToArraylist(item_id, mArrayList_item_id);
                        addItemToArraylist(item_name, mArrayList_item_name);
                        addItemToArraylist(user_id, mArrayList_user_id);
                        addItemToArraylist(date, mArrayList_date);
                        addItemToArraylist(time, mArrayList_time);
                        addItemToArraylist(description, mArrayList_description);
                        addItemToArraylist(latitude, mArrayList_latitude);
                        addItemToArraylist(longitude, mArrayList_longitude);
                        addItemToArraylist(complete, mArrayList_complete);
                        addItemToArraylist(phone, mArrayList_phone);
                        addItemToArraylist(image_url, mArrayList_image_url);


                    }
                    target_map.put("plan_id", mArrayList_plan_id);
                    target_map.put("category", mArrayList_category);
                    //  target_map.put("category_id", mArrayList_category_id);
                    target_map.put("item_id", mArrayList_item_id);
                    target_map.put("item_name", mArrayList_item_name);
                    target_map.put("user_id", mArrayList_user_id);
                    target_map.put("date", mArrayList_date);
                    target_map.put("time", mArrayList_time);
                    target_map.put("description", mArrayList_description);
                    target_map.put("latitude", mArrayList_latitude);
                    target_map.put("longitude", mArrayList_longitude);
                    target_map.put("complete", mArrayList_complete);
                    target_map.put("phone", mArrayList_phone);
                    target_map.put("image", mArrayList_image_url);


                } else {

                }


                if (sDayPlanResponse != null)
                    sDayPlanResponse.onGetDayPlanResponse(true, target_map);
            } else {
                if (sDayPlanResponse != null)

                    sDayPlanResponse.onGetDayPlanResponse(false, null);

            }


        } else if (tag.equals(TravexApplication.GET_SEARCH_TERMINLAS_TAG)) {

            LogUtility.Log(TAG, "parseJsonResponse:get search terminals  tag:", "json obj:" + object);

            int status = 0;
            JSONObject jsonObject = new JSONObject(object.toString());

            status = jsonObject.getInt("status");

            HashMap<Integer, ArrayList<String>> target_map = null;
            ArrayList<String> list_id = null;
            ArrayList<String> list_title = null;
            if (status == 1) {
                JSONObject jsonObject_result = jsonObject.getJSONObject("result");
                JSONArray terminals = jsonObject_result.getJSONArray("terminals");
                if (terminals.length() > 0) {
                    target_map = new HashMap<>();
                    list_title = new ArrayList<>();
                    list_id = new ArrayList<>();
                    for (int i = 0; i < terminals.length(); i++) {


                        String terminals_title = null;
                        String terminals_id = null;
                        JSONObject terminals_obj = terminals.getJSONObject(i);
                        terminals_title = terminals_obj.getString("terminals_title");
                        terminals_id = terminals_obj.getString("terminals_id");
                        addItemToArraylist(terminals_title, list_title);
                        addItemToArraylist(terminals_id, list_id);


                    }


                } else {
                    LogUtility.Log(TAG, "parseJsonResponse:get search terminals  tag:terminals is null", null);

                }
                addArraylistToMap(0, list_title, target_map);
                addArraylistToMap(1, list_id, target_map);
                if (sGetSearchTerminals != null)
                    sGetSearchTerminals.onGetSearchTerminals(true, target_map);
            } else {
                if (sGetSearchTerminals != null)

                    sGetSearchTerminals.onGetSearchTerminals(false, null);

            }


        } else if (tag.equals(TravexApplication.GET_SEARCH_DESTINATION_TAG)) {

            LogUtility.Log(TAG, "parseJsonResponse:get search destinations  tag:", "json obj:" + object);
            int status = 0;
            JSONObject jsonObject = new JSONObject(object.toString());


            status = jsonObject.getInt("status");
            ArrayList<String> list = null;
            if (status == 1) {
                JSONObject result = jsonObject.getJSONObject("result");
                JSONArray destination = result.getJSONArray("destinations");

                if (destination.length() > 0) {
                    list = new ArrayList<>();

                    for (int k = 0; k < destination.length(); k++) {

                        JSONObject airlines_destination = destination.getJSONObject(k);

                        String item = airlines_destination.getString("airlines_destination");
                        addItemToArraylist(item, list);


                    }

                } else {
                    LogUtility.Log(TAG, "parseJsonResponse:get search destinations  tag:array null", null);

                }
                if (sGetSearchDestinations != null) {
                    sGetSearchDestinations.onGetSearchDestinations(true, list);
                }


            } else {
                if (sGetSearchDestinations != null) {
                    sGetSearchDestinations.onGetSearchDestinations(false, list);
                }
            }

        } else if (tag.equals(TravexApplication.PROFILE_IMAGE_UPDATE_TAG)) {


            LogUtility.Log(TAG, "parseJsonResponse:profile image update   tag:", "json obj:" + object);
            int status = 0;
            JSONObject jsonObject = new JSONObject(object.toString());


            status = jsonObject.getInt("status");
            if (status == 1) {
                LogUtility.Log(TAG, "parseJsonResponse:profile image update   tag", "success");
                JSONObject message = jsonObject.getJSONObject("message");
                String image_name = message.getString("member_profile_image");
                LogUtility.Log(TAG, "parseJsonResponse:profile image update   tag", "image_name:" + image_name);
                String image_updated_url = AppConstants.PROFILE_IMAGE_ASSET_URL + image_name;
                LogUtility.Log(TAG, "parseJsonResponse:profile image update   tag", "profile_image_asset_url:" + image_updated_url);

                if (sProfileImageUpdateResponse != null) {
                    sProfileImageUpdateResponse.onGetProfileImageUpdateResponse(true, image_updated_url);
                }

            } else {
                LogUtility.Log(TAG, "parseJsonResponse:profile image update   tag", "failure");
                if (sProfileImageUpdateResponse != null) {
                    sProfileImageUpdateResponse.onGetProfileImageUpdateResponse(false, null);

                }

            }

        } else if (tag.equals(TravexApplication.FORGET_PASSWORD_TAG)) {


            LogUtility.Log(TAG, "parseJsonResponse:forget password    tag:", "json obj:" + object);
            int status = 0;
            JSONObject jsonObject = new JSONObject(object.toString());
            status = jsonObject.getInt("status");
            //{"status":0,"message":{"message":"Email not exist"}}
            String message ="";
            JSONObject message_obj=jsonObject.getJSONObject("message");
            if(message_obj!=null)
            {
                if(message_obj.length()>0)
                {
//                    JSONObject message_obj = message_array.getJSONObject(0);
                    message = message_obj.getString("message");
                    if(message==null)message="";
                }
            }


            if (status == 1) {

                LogUtility.Log(TAG, "parseJsonResponse:forget password    tag", "success");
                if (sGetForgetPasswordResponse != null)
                    sGetForgetPasswordResponse.onGetForgetPasswordResponse(true,message);
            } else {

                LogUtility.Log(TAG, "parseJsonResponse:forget password    tag", "failure");
                if (sGetForgetPasswordResponse != null)

                    sGetForgetPasswordResponse.onGetForgetPasswordResponse(false,message);

            }

        }

    }

    public boolean isNetworkconnected() {
        ConnectivityManager mConnectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] info = mConnectivityManager.getAllNetworkInfo();
        for (NetworkInfo i : info) {
            if (i.getState() == NetworkInfo.State.CONNECTED) {
                LogUtility.Log(TAG, "isNetworkconnected:", "status:connected");
                return true;
            }
        }
        LogUtility.Log(TAG, "isNetworkconnected:", "status:not connected");
        return false;
    }

    public boolean whetherTheCurrentLocInDatabase() {
        ArrayList<String> city = mPreferenceManager.getSupportedCountryTags();
        String currentLoc = mPreferenceManager.getCurrentLocalityName();
        for (String s : city) {
            if (s.equalsIgnoreCase(currentLoc)) {
                LogUtility.Log(TAG, "whetherTheCurrentLocInDatabase", "match found in:" + currentLoc);
                return true;
            }
        }
        LogUtility.Log(TAG, "whetherTheCurrentLocInDatabase", "match not found in:" + currentLoc);
        return false;
    }

    public Map<String, String> getHeaders() {
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("X-API-KEY", mPreferenceManager.getApiKey());
        return headers;
    }

    public String getCategoryItemDirectorynameForImageAssetUrl(String item) {
        if (item.equals("Airline")) {
            return "airlines";
        } else if (item.equals("Shopping")) {
            return "shopping";
        } else if (item.equals("Business")) {
            return "business";
        } else if (item.equals("Real Estate")) {
            return "realestate";
        } else if (item.equals("Money Exchange")) {
            return "moneyexchangers";
        } else if (item.equals("Car Rentals")) {
            return "carrentals";
        } else if (item.equals("Embasies & Consulates")) {
            return "embassy";
        } else if (item.equals("Tour & Travel")) {
            return "tourandtravel";
        } else if (item.equals("Events")) {
            return "events";
        } else if (item.equals("Eating Out")) {
            return "eatingout";
        } else if (item.equals("Stay/Hotels")) {
            return "hotels";
        } else if (item.equals("nightlife")) {
            return "airlines";
        } else if (item.equals("Spa")) {
            return "spa";
        } else if (item.equals("Things To Do")) {
            return "thingstodo";
        } else if (item.equals("Emergencies")) {
            return "emergencies";
        }
        return null;
    }

    public void addItemToArraylist(String item, ArrayList<String> list) {
        if ((item != null) && (item.trim().length() > 0)) {
            // LogUtility.Log(TAG, "addItemToArraylist:", "item:" + item + ",arraylist:" + list);
            list.add(item);
        } else {
            // LogUtility.Log(TAG, "addItemToArraylist:", "item:null");
            list.add(null);
        }
    }

    public void addArraylistToMap(int position, ArrayList<String> list, HashMap<Integer, ArrayList<String>> map) {
        map.put(position, list);
    }

    public void addMapToMap(int position, HashMap<Integer, HashMap<Integer, ArrayList<String>>> target, HashMap<Integer, ArrayList<String>> map) {
        target.put(position, map);
    }

    public void resetThePreferencesForLandingAnimationImagesAPI() {
        mPreferenceManager.setSplash_landing_images(null);
        mPreferenceManager.setSplash_landing_images_content(null);
        mPreferenceManager.setSplash_landing_images_update_time(null);
        mPreferenceManager.setSplash_landing_images_asset_url(null);


    }

    public void resetThePreferencesForFeatureAScreenImagesAPI() {
        mPreferenceManager.setCms_image_asset_url_feature(null);
        mPreferenceManager.setCms_image_content_feature(null);

        mPreferenceManager.setCms_image_description_feature(null);
        mPreferenceManager.setCms_image_update_date_feature(null);
        mPreferenceManager.setCms_image_title_feature(null);
        mPreferenceManager.setCms_image_names_feature(null);


    }

    public void resetThePreferencesForGetcityAPI() {
        mPreferenceManager.setSupportedCountryId(null);
        mPreferenceManager.setSupportedCountryLatitude(null);
        mPreferenceManager.setSupportedCountryLongitude(null);
        mPreferenceManager.setSupportedCountryNames(null);


    }

    public void resetThePreferencesForEssentialIdAPI() {

        mPreferenceManager.setEssentials_id(null);
    }

    public void resetThePreferencesForMyPlanIdAPI() {

        mPreferenceManager.setPlan_id(null);
    }

    public void resetThePreferencesForTipsIdAPI() {

        mPreferenceManager.setTips_id(null);
    }

    public void resetThePreferencesForArticleIdAPI() {

        mPreferenceManager.setArticles_id(null);

    }

    public void resetThePreferencesForSearchGeneralIdAPI() {

        mPreferenceManager.setSearchResultsGeneral_id(null);

    }

    public void resetThePreferencesForSearchKeywordBasedIdAPI() {

        mPreferenceManager.setSearchResultsKeyWord_id(null);

    }

    public void resetThePreferencesForSearchNowBasedIdAPI() {

        mPreferenceManager.setNow_id(null);

    }

    public void resetCurrentLocationDetails() {

        mPreferenceManager.setCurrentLatitude(null);
        mPreferenceManager.setCurrentLongitude(null);
        mPreferenceManager.setCurrentLocalityName(null);
        mPreferenceManager.setCurrentcountryName(null);

    }

    MultiPartRequest multiPartRequest;

    public void handleForMultipartFormData(int method_type, String url, Map<String, String> header, JSONObject jsonObject, final int request_id) {


        multiPartRequest = new MultiPartRequest(mContext, method_type, AppConstants.PROFILE_IMAGE_UPDATE_URL, header, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                // LogUtility.Log(TAG, "multiPartRequest:onResponse", "response:" + response);
                if (request_id == TravexApplication.REQUEST_ID_PROFILE_IMAGE_UPDATE) {

                    LogUtility.Log(TAG, "multiPartRequest:onResponse:REQUEST_ID_PROFILE_IMAGE_UPDATE", "response:" + response);
                    try {
                        parseJsonResponse(response, TravexApplication.PROFILE_IMAGE_UPDATE_TAG);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LogUtility.Log(TAG, "multiPartRequest:onErrorResponse", "error:" + error.getMessage());

            }
        });
        mTravexApplication = TravexApplication.getInstance();
        LogUtility.Log(TAG, "handleForMultipartFormData", "mTravexApplication:" + mTravexApplication);

        mTravexApplication.addRequestToQueue(multiPartRequest);

    }

    JsonObjectRequestHandler jsonObjectRequestHandler;

    public void postJsonRequest(int request_type, String url, JSONObject jsonObject, final int request_id) {

        LogUtility.Log(TAG, "postJsonRequest", "url:" + url + ",jsonObject:" + jsonObject);
        jsonObjectRequestHandler = new JsonObjectRequestHandler(request_type, url, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtility.Log(TAG, "postJsonRequest", "onResponse" + response.toString());
                        if (request_id == TravexApplication.REQUEST_ID_GET_CITY) {
                            LogUtility.Log(TAG, "postJsonRequest", "onResponse:get city tag" + response.toString());
                            /*JsonFeeder mJsonFeeder = new JsonFeeder(mContext);
                             mJsonFeeder.saveJsonIntoFile(AppConstants.FILE_NAME_GET_CITY_DETAILS, response.toString(), TravexApplication.GETCITY_TAG);
*/
                            try {
                                parseJsonResponse(response, TravexApplication.GETCITY_TAG);
                            } catch (JSONException e) {
                                LogUtility.Log(TAG, "postJsonRequest", "JSONException:" + e.getMessage());
                                e.printStackTrace();
                            }
                        } else if (request_id == TravexApplication.REQUEST_ID_REGISTRATION) {
                            LogUtility.Log(TAG, "postJsonRequest", "onResponse:reg tag" + response.toString());
                            /*  JsonFeeder mJsonFeeder = new JsonFeeder(mContext);

                            mJsonFeeder.saveJsonIntoFile(AppConstants.FILE_NAME_REGISTER, response.toString(), TravexApplication.REGISTER_TAG);

                          */
                            try {
                                parseJsonResponse(response, TravexApplication.REGISTER_TAG);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (request_id == TravexApplication.REQUEST_ID_SPLASH) {
                            LogUtility.Log(TAG, "postJsonRequest", "onResponse:splash tag" + response.toString());
                           /* JsonFeeder mJsonFeeder = new JsonFeeder(mContext);

                            mJsonFeeder.saveJsonIntoFile(AppConstants.FILE_NAME_SPLASH_IMAGE_ASSETS, response.toString(), TravexApplication.SPLASH_TAG);
*/
                            try {
                                parseJsonResponse(response, TravexApplication.SPLASH_TAG);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (request_id == TravexApplication.REQUEST_ID_FEATURE) {
                            LogUtility.Log(TAG, "postJsonRequest", "onResponse:feature tag" + response.toString());
                            /*JsonFeeder mJsonFeeder = new JsonFeeder(mContext);

                            mJsonFeeder.saveJsonIntoFile(AppConstants.FILE_NAME_FEATURE_ASSETS, response.toString(), TravexApplication.FEATURE_TAG);

                          */
                            try {
                                parseJsonResponse(response, TravexApplication.FEATURE_TAG);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (request_id == TravexApplication.REQUEST_ID_VERIFY_REGISTRATION) {
                            LogUtility.Log(TAG, "postJsonRequest", "onResponse:verify tag" + response.toString());

                           /* JsonFeeder mJsonFeeder = new JsonFeeder(mContext);

                            mJsonFeeder.saveJsonIntoFile(AppConstants.FILE_VERIFY_LOGIN, response.toString(), TravexApplication.VERIFY_REGISTRATION_TAG);
*/
                            try {
                                parseJsonResponse(response, TravexApplication.VERIFY_REGISTRATION_TAG);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } else if (request_id == TravexApplication.REQUEST_ID_SUGGEST_CITY) {
                            LogUtility.Log(TAG, "postJsonRequest", "onResponse:suggest city tag" + response.toString());
                            try {
                                parseJsonResponse(response, TravexApplication.SUGGEST_CITY_TAG);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } else if (request_id == TravexApplication.REQUEST_ID_LOGIN) {
                            LogUtility.Log(TAG, "postJsonRequest", "onResponse:login  tag" + response.toString());


                           /* JsonFeeder mJsonFeeder = new JsonFeeder(mContext);

                            mJsonFeeder.saveJsonIntoFile(AppConstants.FILE_VERIFY_LOGIN, response.toString(), TravexApplication.LOGIN_TAG);

                        */
                            try {
                                parseJsonResponse(response, TravexApplication.LOGIN_TAG);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } else if (request_id == TravexApplication.REQUEST_ID_SOCIAL_LOGIN_FACEBOOK) {
                            LogUtility.Log(TAG, "postJsonRequest", "onResponse:social login facebook tag" + response.toString());
                            try {
                                parseJsonResponse(response, TravexApplication.SOCIAL_LOGIN_FACEBOOK_TAG);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } else if (request_id == TravexApplication.REQUEST_ID_SOCIAL_LOGIN_GOOGLE) {
                            LogUtility.Log(TAG, "postJsonRequest", "onResponse:social login google tag" + response.toString());
                            try {
                                parseJsonResponse(response, TravexApplication.SOCIAL_LOGIN_GOOGLE_TAG);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } else if (request_id == TravexApplication.REQUEST_ID_GET_RESULTS_GENERAL) {
                            LogUtility.Log(TAG, "postJsonRequest", "onResponse:get results general tag" + response.toString());
                            try {
                                parseJsonResponse(response, TravexApplication.GET_GENERAL_RESULT_TAG);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } else if (request_id == TravexApplication.REQUEST_ID_GET_RESULTS_KEYWORD) {
                            LogUtility.Log(TAG, "postJsonRequest", "onResponse:get results key based tag" + response.toString());
                            try {
                                parseJsonResponse(response, TravexApplication.GET_KEYWORDSEARCH_RESULT_TAG);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (request_id == TravexApplication.REQUEST_ID_GET_SEARCH_ITEM) {
                            LogUtility.Log(TAG, "postJsonRequest", "onResponse:get results search  item tag" + response.toString());
                            try {
                                parseJsonResponse(response, TravexApplication.GET_SEARCHITEM_TAG);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (request_id == TravexApplication.REQUEST_ID_NOW) {
                            LogUtility.Log(TAG, "postJsonRequest", "onResponse:get results search  now tag" + response.toString());
                            try {
                                parseJsonResponse(response, TravexApplication.NOW_TAG);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (request_id == TravexApplication.REQUEST_ID_GET_FILTER) {
                            LogUtility.Log(TAG, "postJsonRequest", "onResponse:get filters search  api tag" + response.toString());
                            try {
                                parseJsonResponse(response, TravexApplication.GET_FILTERS_TAG);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (request_id == TravexApplication.REQUEST_ID_FILTER_RESULTS) {
                            LogUtility.Log(TAG, "postJsonRequest", "onResponse:get filters results  api tag" + response.toString());
                            try {
                                parseJsonResponse(response, TravexApplication.FILTERRESULTS_TAG);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if ((request_id == TravexApplication.REQUEST_ID_ARTICLE_LIST_FULLFLEDGE) ||
                                (request_id == TravexApplication.REQUEST_ID_ARTICLE_LIST_CATEGORIZED) ||
                                (request_id == TravexApplication.REQUEST_ID_ARTICLE_LIST_FEATURED)) {
                            LogUtility.Log(TAG, "postJsonRequest", "onResponse:get articles list full fledge  api tag" + response.toString());
                            if ((request_id == TravexApplication.REQUEST_ID_ARTICLE_LIST_FULLFLEDGE)) {
                                try {
                                    parseJsonResponse(response, TravexApplication.GET_ARTICLES_LIST_FULLFLEDGE_TAG);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            if ((request_id == TravexApplication.REQUEST_ID_ARTICLE_LIST_CATEGORIZED)) {
                                try {
                                    parseJsonResponse(response, TravexApplication.GET_ARTICLES_LIST_CATEGORYBASED_TAG);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            if ((request_id == TravexApplication.REQUEST_ID_ARTICLE_LIST_FEATURED)) {
                                try {
                                    parseJsonResponse(response, TravexApplication.GET_ARTICLES_LIST_FEATURED_TAG);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        } else if (request_id == TravexApplication.REQUEST_ID_ARTICLE_DETAILS) {
                            LogUtility.Log(TAG, "postJsonRequest", "onResponse:get articles results  api tag" + response.toString());
                            try {
                                parseJsonResponse(response, TravexApplication.GET_ARTICLES_DETAILS_TAG);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (request_id == TravexApplication.REQUEST_ID_ESSENTIALS_LIST) {
                            LogUtility.Log(TAG, "postJsonRequest", "onResponse:get essentials list  api tag" + response.toString());
                            try {
                                parseJsonResponse(response, TravexApplication.GET_ESSENTIALS_LIST_TAG);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (request_id == TravexApplication.REQUEST_ID_ESSENTIALS_DETAILS) {
                            LogUtility.Log(TAG, "postJsonRequest", "onResponse:get essentials details  api tag" + response.toString());
                            try {
                                parseJsonResponse(response, TravexApplication.GET_ESSENTIALS_DETAILS_TAG);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (request_id == TravexApplication.REQUEST_ID_TIPS_LIST) {
                            LogUtility.Log(TAG, "postJsonRequest", "onResponse:get tips list  api tag" + response.toString());
                            try {
                                parseJsonResponse(response, TravexApplication.GET_TIPS_LIST_TAG);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (request_id == TravexApplication.REQUEST_ID_TIPS_DETAILS) {
                            LogUtility.Log(TAG, "postJsonRequest", "onResponse:get tips details  api tag" + response.toString());
                            try {
                                parseJsonResponse(response, TravexApplication.GET_TIPS_DETAILS_TAG);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (request_id == TravexApplication.REQUEST_ID_TIPS_DETAILS) {
                            LogUtility.Log(TAG, "postJsonRequest", "onResponse:get tips details  api tag" + response.toString());
                            try {
                                parseJsonResponse(response, TravexApplication.GET_TIPS_DETAILS_TAG);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (request_id == TravexApplication.REQUEST_ID_ABOUT_US) {
                            LogUtility.Log(TAG, "postJsonRequest", "onResponse:ABOUT US  api tag" + response.toString());
                            try {
                                parseJsonResponse(response, TravexApplication.ABOUT_US_TAG);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (request_id == TravexApplication.REQUEST_ID_CONTACT_US) {
                            LogUtility.Log(TAG, "postJsonRequest", "onResponse:CONTACT US  api tag" + response.toString());
                            try {
                                parseJsonResponse(response, TravexApplication.CONTACT_US_TAG);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (request_id == TravexApplication.REQUEST_ID_ENQUIRY) {
                            LogUtility.Log(TAG, "postJsonRequest", "onResponse:ENQUIRY  api tag" + response.toString());
                            try {
                                parseJsonResponse(response, TravexApplication.ENQUIRY_TAG);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (request_id == TravexApplication.REQUEST_ID_SEARCH_TERMINALS) {
                            LogUtility.Log(TAG, "postJsonRequest", "onResponse:search terminals  api tag" + response.toString());
                            try {
                                parseJsonResponse(response, TravexApplication.GET_SEARCH_TERMINLAS_TAG);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (request_id == TravexApplication.REQUEST_ID_SEARCH_DESTINATIONS) {
                            LogUtility.Log(TAG, "postJsonRequest", "onResponse:search destinations  api tag" + response.toString());
                            try {
                                parseJsonResponse(response, TravexApplication.GET_SEARCH_DESTINATION_TAG);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (request_id == TravexApplication.REQUEST_ID_FORGET_PASSWORD) {
                            LogUtility.Log(TAG, "postJsonRequest", "onResponse:forget  password tag" + response.toString());
                            try {
                                parseJsonResponse(response, TravexApplication.FORGET_PASSWORD_TAG);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (request_id == TravexApplication.REQUEST_ID_GET_CITY) {
                    LogUtility.Log(TAG, "postJsonRequest:get city", "onErrorResponse:" + error.getMessage());
                } else if (request_id == TravexApplication.REQUEST_ID_REGISTRATION) {
                    LogUtility.Log(TAG, "postJsonRequest:reg", "onErrorResponse:" + error.getMessage());
                } else if (request_id == TravexApplication.REQUEST_ID_VERIFY_REGISTRATION) {
                    LogUtility.Log(TAG, "postJsonRequest:verify", "onErrorResponse:" + error.getMessage());
                } else if (request_id == TravexApplication.REQUEST_ID_FEATURE) {
                    LogUtility.Log(TAG, "postJsonRequest:feature", "onErrorResponse:" + error.getMessage());
                } else if (request_id == TravexApplication.REQUEST_ID_SPLASH) {
                    LogUtility.Log(TAG, "postJsonRequest:splash", "onErrorResponse:" + error.getMessage());
                } else if (request_id == TravexApplication.REQUEST_ID_LOGIN) {
                    LogUtility.Log(TAG, "postJsonRequest:login", "onErrorResponse:" + error.getMessage());
                } else if (request_id == TravexApplication.REQUEST_ID_SOCIAL_LOGIN_FACEBOOK) {
                    LogUtility.Log(TAG, "postJsonRequest:social login facebook", "onErrorResponse:" + error.getMessage());
                } else if (request_id == TravexApplication.REQUEST_ID_GET_RESULTS_GENERAL) {
                    LogUtility.Log(TAG, "postJsonRequest:get results general search api", "onErrorResponse:" + error.getMessage());
                } else if (request_id == TravexApplication.REQUEST_ID_GET_RESULTS_KEYWORD) {
                    LogUtility.Log(TAG, "postJsonRequest:get results key based search api", "onErrorResponse:" + error.getMessage());
                } else if (request_id == TravexApplication.REQUEST_ID_GET_SEARCH_ITEM) {
                    LogUtility.Log(TAG, "postJsonRequest:get search item details api", "onErrorResponse:" + error.getMessage());
                } else if (request_id == TravexApplication.REQUEST_ID_NOW) {
                    LogUtility.Log(TAG, "postJsonRequest:get now search api", "onErrorResponse:" + error.getMessage());
                } else if (request_id == TravexApplication.REQUEST_ID_GET_FILTER) {
                    LogUtility.Log(TAG, "postJsonRequest:get filters search api", "onErrorResponse:" + error.getMessage());
                } else if (request_id == TravexApplication.REQUEST_ID_FILTER_RESULTS) {
                    LogUtility.Log(TAG, "postJsonRequest:get filter results search api", "onErrorResponse:" + error.getMessage());
                } else if ((request_id == TravexApplication.REQUEST_ID_ARTICLE_LIST_FULLFLEDGE) ||
                        (request_id == TravexApplication.REQUEST_ID_ARTICLE_LIST_CATEGORIZED) ||
                        (request_id == TravexApplication.REQUEST_ID_ARTICLE_LIST_FEATURED)) {
                    LogUtility.Log(TAG, "postJsonRequest:get articles list api", "onErrorResponse:" + error.getMessage());
                } else if (request_id == TravexApplication.REQUEST_ID_ARTICLE_DETAILS) {
                    LogUtility.Log(TAG, "postJsonRequest:get articles details api", "onErrorResponse:" + error.getMessage());
                } else if (request_id == TravexApplication.REQUEST_ID_ESSENTIALS_LIST) {
                    LogUtility.Log(TAG, "postJsonRequest:get  essentials list api", "onErrorResponse:" + error.getMessage());
                } else if (request_id == TravexApplication.REQUEST_ID_ESSENTIALS_DETAILS) {
                    LogUtility.Log(TAG, "postJsonRequest:get  essentials details api", "onErrorResponse:" + error.getMessage());
                } else if (request_id == TravexApplication.REQUEST_ID_TIPS_LIST) {
                    LogUtility.Log(TAG, "postJsonRequest:get  tips list api", "onErrorResponse:" + error.getMessage());
                } else if (request_id == TravexApplication.REQUEST_ID_TIPS_DETAILS) {
                    LogUtility.Log(TAG, "postJsonRequest:get  tips details api", "onErrorResponse:" + error.getMessage());
                } else if (request_id == TravexApplication.REQUEST_ID_ABOUT_US) {
                    LogUtility.Log(TAG, "postJsonRequest:get   ABOUT US api", "onErrorResponse:" + error.getMessage());
                } else if (request_id == TravexApplication.REQUEST_ID_CONTACT_US) {
                    LogUtility.Log(TAG, "postJsonRequest:get   CONTACT US api", "onErrorResponse:" + error.getMessage());
                } else if (request_id == TravexApplication.REQUEST_ID_ENQUIRY) {
                    LogUtility.Log(TAG, "postJsonRequest:get   ENQUIRY  api", "onErrorResponse:" + error.getMessage());
                } else if (request_id == TravexApplication.REQUEST_ID_SEARCH_TERMINALS) {
                    LogUtility.Log(TAG, "postJsonRequest:get   search terminals  api", "onErrorResponse:" + error.getMessage());


                } else if (request_id == TravexApplication.REQUEST_ID_SEARCH_DESTINATIONS) {
                    LogUtility.Log(TAG, "postJsonRequest:get   search destinations  api", "onErrorResponse:" + error.getMessage());


                } else if (request_id == TravexApplication.REQUEST_ID_FORGET_PASSWORD) {


                    LogUtility.Log(TAG, "postJsonRequest:forget password  api", "onErrorResponse:" + error.getMessage());


                }
            }
        });
        mTravexApplication = TravexApplication.getInstance();
        LogUtility.Log(TAG, "jsonObjectRequestHandler", "mTravexApplication:" + mTravexApplication);
        mTravexApplication.addRequestToQueue(jsonObjectRequestHandler);


    }

    JsonObjectRequestHandler mJsonObjectRequestHandler2 = null;

    public void postJsonRequest(int request_type, final Map<String, String> request_header, String url, JSONObject jsonObject, final int request_id) {

        LogUtility.Log(TAG, "postJsonRequest:", "request_header:" + request_header + ",url:" + url + ",json:" + jsonObject);
        mJsonObjectRequestHandler2 = new JsonObjectRequestHandler(request_type, url, jsonObject, request_header, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (request_id == TravexApplication.REQUEST_ID_MEMBER_UPDATE_PROFILE) {
                    LogUtility.Log(TAG, "postJsonRequest:mJsonObjectRequestWithHeaders", "onResponse:member update profile tag" + response.toString());
                    try {
                        parseJsonResponse(response, TravexApplication.MEMBER_UPDATE_PROFILE_TAG);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (request_id == TravexApplication.REQUEST_ID_MEMBER_DETAILS) {
                    LogUtility.Log(TAG, "postJsonRequest", "onResponse:member details  api tag" + response.toString());
                    try {
                        parseJsonResponse(response, TravexApplication.MEMBER_DETAILS_TAG);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (request_id == TravexApplication.REQUEST_ID_TRAVEX_LOGOUT) {
                    LogUtility.Log(TAG, "postJsonRequest:mJsonObjectRequestWithHeaders", "onResponse:travex logout" + response.toString());
                    try {
                        parseJsonResponse(response, TravexApplication.LOGOUT_TAG);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (request_id == TravexApplication.REQUEST_ID_ADD_PLAN) {
                    LogUtility.Log(TAG, "postJsonRequest:mJsonObjectRequestWithHeaders", "onResponse:ADDPLAN" + response.toString());
                    try {
                        parseJsonResponse(response, TravexApplication.ADD_PLAN_TAG);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (request_id == TravexApplication.REQUEST_ID_LIST_MYPLAN) {
                    LogUtility.Log(TAG, "postJsonRequest:mJsonObjectRequestWithHeaders", "onResponse:LIST  PLAN" + response.toString());
                    try {
                        parseJsonResponse(response, TravexApplication.LIST_MYPLAN_TAG);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (request_id == TravexApplication.REQUEST_ID_ADD_SUGGESTIONS) {
                    LogUtility.Log(TAG, "postJsonRequest:mJsonObjectRequestWithHeaders", "onResponse:add suggestions" + response.toString());
                    try {
                        parseJsonResponse(response, TravexApplication.ADD_SUGGESTIONS_TAG);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (request_id == TravexApplication.REQUEST_ID_UPDATE_PLAN) {
                    LogUtility.Log(TAG, "postJsonRequest", "onResponse:UPDATE plan  api tag" + response.toString());
                    try {
                        parseJsonResponse(response, TravexApplication.UPDATE_PLAN_TAG);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (request_id == TravexApplication.REQUEST_ID_DELETE_PLAN) {
                    LogUtility.Log(TAG, "postJsonRequest", "onResponse:DELETE plan  api tag" + response.toString());
                    try {
                        parseJsonResponse(response, TravexApplication.DELETE_PLAN_TAG);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (request_id == TravexApplication.REQUEST_ID_DONE_PLAN) {
                    LogUtility.Log(TAG, "postJsonRequest", "onResponse:DONE plan  api tag" + response.toString());
                    try {
                        parseJsonResponse(response, TravexApplication.DONE_PLAN_TAG);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (request_id == TravexApplication.REQUEST_ID_DAY_PLAN) {
                    LogUtility.Log(TAG, "postJsonRequest", "onResponse:DAY plan  api tag" + response.toString());
                    try {
                        parseJsonResponse(response, TravexApplication.DAY_PLAN_TAG);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (request_id == TravexApplication.REQUEST_ID_TRAVEX_EVENTS_AND_MONTHLY_PLAN) {
                    LogUtility.Log(TAG, "postJsonRequest", "onResponse:travex event plan  api tag" + response.toString());
                    try {
                        parseJsonResponse(response, TravexApplication.TRAVEX_EVENTS_AND_MONTHLY_PLAN_TAG);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (request_id == TravexApplication.REQUEST_ID_EVENT_DETAILS) {
                    LogUtility.Log(TAG, "postJsonRequest", "onResponse:travex event details  api tag" + response.toString());
                    try {
                        parseJsonResponse(response, TravexApplication.EVENT_DETAILS_TAG);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (request_id == TravexApplication.REQUEST_ID_WRITE_REVIEW) {
                    LogUtility.Log(TAG, "postJsonRequest", "onResponse:write review  api tag" + response.toString());
                    try {
                        parseJsonResponse(response, TravexApplication.WRITE_REVIEW_TAG);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LogUtility.Log(TAG, "onErrorResponse", "error:" + error.getMessage());
                if (request_id == TravexApplication.REQUEST_ID_MEMBER_UPDATE_PROFILE) {
                    LogUtility.Log(TAG, "onErrorResponse", "error in member update:" + error.getMessage());
                } else if (request_id == TravexApplication.REQUEST_ID_TRAVEX_LOGOUT) {
                    LogUtility.Log(TAG, "onErrorResponse", "error in travex logout:" + error.getMessage());
                } else if (request_id == TravexApplication.REQUEST_ID_SOCIAL_LOGIN_GOOGLE) {
                    LogUtility.Log(TAG, "onErrorResponse", "error in  google login:" + error.getMessage());
                    GooglePlusLogin.disconnect_Google();
                    mTravexApplication.showToast("Error in google login:try again");
                } else if (request_id == TravexApplication.REQUEST_ID_SOCIAL_LOGIN_FACEBOOK) {
                    LogUtility.Log(TAG, "onErrorResponse", "error in  facebook login:" + error.getMessage());
                } else if (request_id == TravexApplication.REQUEST_ID_ADD_PLAN) {
                    LogUtility.Log(TAG, "onErrorResponse", "error in  add plan :" + error.getMessage());
                } else if (request_id == TravexApplication.REQUEST_ID_LIST_MYPLAN) {
                    LogUtility.Log(TAG, "onErrorResponse", "error in  list plan :" + error.getMessage());
                } else if (request_id == TravexApplication.REQUEST_ID_ADD_SUGGESTIONS) {
                    LogUtility.Log(TAG, "onErrorResponse", "error in  add suggestions :" + error.getMessage());
                } else if (request_id == TravexApplication.REQUEST_ID_UPDATE_PLAN) {

                    LogUtility.Log(TAG, "postJsonRequest:get   update plan  api", "onErrorResponse:" + error.getMessage());

                } else if (request_id == TravexApplication.REQUEST_ID_DELETE_PLAN) {
                    LogUtility.Log(TAG, "postJsonRequest:get   delete plan  api", "onErrorResponse:" + error.getMessage());


                } else if (request_id == TravexApplication.REQUEST_ID_DONE_PLAN) {
                    LogUtility.Log(TAG, "postJsonRequest:get   done plan  api", "onErrorResponse:" + error.getMessage());


                } else if (request_id == TravexApplication.REQUEST_ID_DAY_PLAN) {
                    LogUtility.Log(TAG, "postJsonRequest:get   day plan  api", "onErrorResponse:" + error.getMessage());


                } else if (request_id == TravexApplication.REQUEST_ID_TRAVEX_EVENTS_AND_MONTHLY_PLAN) {
                    LogUtility.Log(TAG, "postJsonRequest:get travex event plan  api", "onErrorResponse:" + error.getMessage());


                } else if (request_id == TravexApplication.REQUEST_ID_EVENT_DETAILS) {
                    LogUtility.Log(TAG, "postJsonRequest:get travex event details  api", "onErrorResponse:" + error.getMessage());


                } else if (request_id == TravexApplication.REQUEST_ID_WRITE_REVIEW) {
                    LogUtility.Log(TAG, "postJsonRequest:get write review", "onErrorResponse:" + error.getMessage());


                } else if (request_id == TravexApplication.REQUEST_ID_MEMBER_DETAILS) {
                    LogUtility.Log(TAG, "postJsonRequest:get member details review", "onErrorResponse:" + error.getMessage());


                }

            }
        });
        mTravexApplication = TravexApplication.getInstance();
        LogUtility.Log(TAG, "postJsonRequest", "mTravexApplication:" + mTravexApplication);
        mTravexApplication.addRequestToQueue(mJsonObjectRequestHandler2);


    }

    public void handleForTravexEvent(JSONObject jsonObject_result, int status) {
        LogUtility.Log(TAG, "handleForTravexEvent", "status:" + status);
        if ((status == 1) || (status == 3)) {
            try {
                JSONArray mJsonArray_list = jsonObject_result.getJSONArray("events");
                LogUtility.Log(TAG, "handleForTravexEvent", "events:" + mJsonArray_list);

                ArrayList<String> mArrayList_item_id = null;
                ArrayList<String> mArrayList_item_name = null;
                ArrayList<String> mArrayList_description = null;
                ArrayList<String> mArrayList_start_date = null;
                ArrayList<String> mArrayList_end_date = null;
                ArrayList<String> mArrayList_start_time = null;
                ArrayList<String> mArrayList_end_time = null;

                ArrayList<String> mArrayList_latitude = null;
                ArrayList<String> mArrayList_longitude = null;
                ArrayList<String> mArrayList_city = null;
                ArrayList<String> mArrayList_active_status = null;
                ArrayList<String> mArrayList_image_url = null;
                ArrayList<String> mArrayList_events_category = null;
                ArrayList<String> mArrayList_phone = null;

                if (mJsonArray_list.length() > 0) {
                    travex_events = new HashMap<>();

                    mArrayList_item_id = new ArrayList<>();
                    mArrayList_item_name = new ArrayList<>();
                    mArrayList_description = new ArrayList<>();
                    mArrayList_start_date = new ArrayList<>();
                    mArrayList_end_date = new ArrayList<>();
                    mArrayList_start_time = new ArrayList<>();
                    mArrayList_end_time = new ArrayList<>();

                    mArrayList_latitude = new ArrayList<>();
                    mArrayList_longitude = new ArrayList<>();
                    mArrayList_city = new ArrayList<>();
                    mArrayList_active_status = new ArrayList<>();
                    mArrayList_image_url = new ArrayList<>();
                    mArrayList_events_category = new ArrayList<>();
                    mArrayList_phone = new ArrayList<>();

                    for (int k = 0; k < mJsonArray_list.length(); k++) {
                        JSONObject mJsonObject_items = mJsonArray_list.getJSONObject(k);
                        String item_id = null;
                        String item_name = null;
                        String description = null;
                        String start_date = null;
                        String end_date = null;
                        String start_time = null;
                        String end_time = null;
                        String latitude = null;
                        String longitude = null;
                        String city = null;
                        String image = null;
                        String events_category = null;
                        String active_status = null;
                        String phone = null;


                        item_id = mJsonObject_items.getString("item_id");
                        LogUtility.Log(TAG, "handleForTravexEvent", "item_id:" + item_id);

                        item_name = mJsonObject_items.getString("item_name");
                        LogUtility.Log(TAG, "handleForTravexEvent", "item_name:" + item_name);

                        description = mJsonObject_items.getString("description");
                        LogUtility.Log(TAG, "handleForTravexEvent", "description:" + description);

                        start_date = mJsonObject_items.getString("start_date");
                        LogUtility.Log(TAG, "handleForTravexEvent", "start_date:" + start_date);

                        end_date = mJsonObject_items.getString("end_date");
                        LogUtility.Log(TAG, "handleForTravexEvent", "end_date:" + end_date);

                        start_time = mJsonObject_items.getString("start_time");
                        LogUtility.Log(TAG, "handleForTravexEvent", "start_time:" + start_time);

                        end_time = mJsonObject_items.getString("end_time");
                        LogUtility.Log(TAG, "handleForTravexEvent", "end_time:" + end_time);

                        latitude = mJsonObject_items.getString("latitude");
                        LogUtility.Log(TAG, "handleForTravexEvent", "latitude:" + latitude);

                        longitude = mJsonObject_items.getString("longitude");
                        LogUtility.Log(TAG, "handleForTravexEvent", "longitude:" + longitude);

                        city = mJsonObject_items.getString("city");
                        LogUtility.Log(TAG, "handleForTravexEvent", "city:" + city);

                        image = mJsonObject_items.getString("image");
                        LogUtility.Log(TAG, "handleForTravexEvent", "image:" + image);
                        phone = mJsonObject_items.getString("phone");
                        LogUtility.Log(TAG, "handleForTravexEvent", "phone:" + phone);

                        events_category = mJsonObject_items.getString("events_category");
                        LogUtility.Log(TAG, "handleForTravexEvent", "events_category:" + events_category);

                        active_status = mJsonObject_items.getString("active_status");
                        LogUtility.Log(TAG, "handleForTravexEvent", "active_status:" + active_status);


                        addItemToArraylist(item_id, mArrayList_item_id);
                        addItemToArraylist(item_name, mArrayList_item_name);
                        addItemToArraylist(description, mArrayList_description);
                        addItemToArraylist(start_date, mArrayList_start_date);
                        addItemToArraylist(end_date, mArrayList_end_date);
                        addItemToArraylist(start_time, mArrayList_start_time);
                        addItemToArraylist(end_time, mArrayList_end_time);
                        addItemToArraylist(latitude, mArrayList_latitude);
                        addItemToArraylist(longitude, mArrayList_longitude);
                        addItemToArraylist(city, mArrayList_city);
                        addItemToArraylist(image, mArrayList_image_url);
                        addItemToArraylist(events_category, mArrayList_events_category);
                        addItemToArraylist(active_status, mArrayList_active_status);
                        addItemToArraylist(phone, mArrayList_phone);


                    }


                }
                travex_events.put("item_id", mArrayList_item_id);
                travex_events.put("item_name", mArrayList_item_name);
                travex_events.put("description", mArrayList_description);
                travex_events.put("start_date", mArrayList_start_date);
                travex_events.put("end_date", mArrayList_end_date);
                travex_events.put("start_time", mArrayList_start_time);
                travex_events.put("end_time", mArrayList_end_time);
                travex_events.put("latitude", mArrayList_latitude);
                travex_events.put("longitude", mArrayList_longitude);
                travex_events.put("city", mArrayList_city);
                travex_events.put("image", mArrayList_image_url);
                travex_events.put("events_category", mArrayList_events_category);
                travex_events.put("active_status", mArrayList_active_status);
                travex_events.put("phone", mArrayList_phone);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


    }

    public void handleForMonthlyPlan(JSONObject jsonObject_result, int status) {

        LogUtility.Log(TAG, "handleForMonthlyPlan:", "status:" + status);
        if ((status == 1) || (status == 4))

        {
            try {
                JSONArray mJsonArray_list = jsonObject_result.getJSONArray("list");
                LogUtility.Log(TAG, "handleForMonthlyPlan:", "list:" + mJsonArray_list);

                ArrayList<String> mArrayList_plan_id = null;
                ArrayList<String> mArrayList_category = null;
                ArrayList<String> mArrayList_item_id = null;
                ArrayList<String> mArrayList_item_name = null;
                ArrayList<String> mArrayList_user_id = null;
                ArrayList<String> mArrayList_date = null;
                ArrayList<String> mArrayList_time = null;
                ArrayList<String> mArrayList_complete = null;
                ArrayList<String> mArrayList_status = null;
                if (mJsonArray_list.length() > 0) {
                    monthly_plan_list = new HashMap<>();

                    mArrayList_plan_id = new ArrayList<>();
                    mArrayList_category = new ArrayList<>();
                    mArrayList_item_id = new ArrayList<>();
                    mArrayList_item_name = new ArrayList<>();
                    mArrayList_user_id = new ArrayList<>();
                    mArrayList_date = new ArrayList<>();
                    mArrayList_time = new ArrayList<>();
                    mArrayList_complete = new ArrayList<>();
                    mArrayList_status = new ArrayList<>();

                  /*  "plan_id": "9",
                            "category": "shopping",
                            "item_id": "fubccvat_vq_1",
                            "item_name": "GOLD SOUK",
                            "user_id": "zrzore_vq_28",
                            "date": "2016-04-15",
                            "time": "15:36:00",
                            "complete": "complete",
                            "status": "1"*/

                    for (int k = 0; k < mJsonArray_list.length(); k++) {
                        JSONObject mJsonObject_items = mJsonArray_list.getJSONObject(k);
                        String plan_id = null;
                        String category = null;
                        String item_id = null;
                        String item_name = null;
                        String user_id = null;
                        String date = null;
                        String time = null;
                        String complete = null;
                        String status1 = null;

                        plan_id = mJsonObject_items.getString("plan_id");
                        category = mJsonObject_items.getString("category");
                        item_id = mJsonObject_items.getString("item_id");
                        item_name = mJsonObject_items.getString("item_name");
                        user_id = mJsonObject_items.getString("user_id");
                        date = mJsonObject_items.getString("date");
                        time = mJsonObject_items.getString("time");
                        complete = mJsonObject_items.getString("complete");
                        status1 = mJsonObject_items.getString("status");
                        LogUtility.Log(TAG, "handleForMonthlyPlan:", "plan_id:" + plan_id);

                        addItemToArraylist(plan_id, mArrayList_plan_id);
                        addItemToArraylist(category, mArrayList_category);
                        addItemToArraylist(item_id, mArrayList_item_id);
                        addItemToArraylist(item_name, mArrayList_item_name);
                        addItemToArraylist(user_id, mArrayList_user_id);
                        addItemToArraylist(date, mArrayList_date);
                        addItemToArraylist(time, mArrayList_time);
                        addItemToArraylist(complete, mArrayList_complete);
                        addItemToArraylist(status1, mArrayList_status);


                    }


                }
                mPreferenceManager.setPlan_id(mArrayList_plan_id);
                mPreferenceManager.getPlan_id();

                monthly_plan_list.put("plan_id", mArrayList_plan_id);
                monthly_plan_list.put("category", mArrayList_category);
                monthly_plan_list.put("item_id", mArrayList_item_id);
                monthly_plan_list.put("item_name", mArrayList_item_name);
                monthly_plan_list.put("user_id", mArrayList_user_id);
                monthly_plan_list.put("date", mArrayList_date);
                monthly_plan_list.put("time", mArrayList_time);
                monthly_plan_list.put("complete", mArrayList_complete);
                monthly_plan_list.put("status", mArrayList_status);

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }


    }

}
