package org.technomobs.travex.Controller.Interface;

/**
 * Created by technomobs on 20/2/16.
 */
public interface GetVerifyResponse {

    public void onGetVerifyResponse(boolean status);
}
