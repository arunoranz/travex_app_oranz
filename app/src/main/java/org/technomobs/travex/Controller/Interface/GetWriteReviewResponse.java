package org.technomobs.travex.Controller.Interface;

/**
 * Created by technomobs on 15/4/16.
 */
public interface GetWriteReviewResponse {


    public void onGetWriteReviewResponse(boolean status);
}
