package org.technomobs.travex.Controller.Interface;

/**
 * Created by technomobs on 22/2/16.
 */
public interface GetCitySuggestionResponse {

    public void onGetCitySuggestion(boolean status);
}
