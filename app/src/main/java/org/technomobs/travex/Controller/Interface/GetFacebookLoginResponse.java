package org.technomobs.travex.Controller.Interface;

/**
 * Created by technomobs on 1/3/16.
 */
public interface GetFacebookLoginResponse {

    public void onGetFacebookLoginResponse(boolean status);

}
