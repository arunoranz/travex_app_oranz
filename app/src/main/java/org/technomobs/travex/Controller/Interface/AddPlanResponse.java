package org.technomobs.travex.Controller.Interface;

/**
 * Created by technomobs on 16/3/16.
 */
public interface AddPlanResponse {

    public void onAddPlanResponse(boolean status);
}
