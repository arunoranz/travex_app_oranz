package org.technomobs.travex.Controller.Interface;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by technomobs on 7/4/16.
 */
public interface DayPlanResponse {
    public void onGetDayPlanResponse(boolean status, HashMap<String, ArrayList<String>> target_map);
}
