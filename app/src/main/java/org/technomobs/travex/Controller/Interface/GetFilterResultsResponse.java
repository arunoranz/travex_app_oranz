package org.technomobs.travex.Controller.Interface;

import org.json.JSONObject;

/**
 * Created by technomobs on 15/3/16.
 */
public interface GetFilterResultsResponse {

    public void onGetFilterResultsResponse(boolean status, JSONObject object);

}
