package org.technomobs.travex.Controller.Interface;

/**
 * Created by technomobs on 17/3/16.
 */
public interface UpdatePlanResponse {

    public void onUpdatePlanResponse(boolean status);
}
