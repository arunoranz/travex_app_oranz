package org.technomobs.travex.Controller.Interface;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by technomobs on 17/3/16.
 */
public interface ContactUsResponse {

    public void onContactUsResponse(boolean status, HashMap<Integer, HashMap<Integer, ArrayList<String>>> result_details);

}
