package org.technomobs.travex.Controller.Interface;

/**
 * Created by technomobs on 19/2/16.
 */
public interface GetFeatureImagesResponse {

    public void onFeatureImageResponse(boolean status);
}
