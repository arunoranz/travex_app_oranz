package org.technomobs.travex.Controller.Interface;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by technomobs on 25/3/16.
 */
public interface GetSearchTerminals {


    public void onGetSearchTerminals(boolean status, HashMap<Integer, ArrayList<String>> terminal_title_and_id);
}
