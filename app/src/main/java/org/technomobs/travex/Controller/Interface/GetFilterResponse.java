package org.technomobs.travex.Controller.Interface;

import org.json.JSONObject;

/**
 * Created by technomobs on 15/3/16.
 */
public interface GetFilterResponse {

    /**
     * @param status success/failure
     * @param object response as jsonObject
     */
    public void onGetFilterResponse(boolean status, JSONObject object);

}
