package org.technomobs.travex.Controller.Interface;

/**
 * Created by technomobs on 1/3/16.
 */
public interface GetLoginResponse {

    public void onGetLoginResponse(boolean status);
}
