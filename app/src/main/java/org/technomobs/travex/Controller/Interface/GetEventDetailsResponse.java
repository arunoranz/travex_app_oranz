package org.technomobs.travex.Controller.Interface;

import org.json.JSONObject;

/**
 * Created by technomobs on 8/4/16.
 */
public interface GetEventDetailsResponse {

    public void onGetEventDetailsResponse(boolean status, JSONObject jsonObject);

}
