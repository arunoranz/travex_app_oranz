package org.technomobs.travex.Controller.Interface;

import org.json.JSONObject;

/**
 * Created by technomobs on 19/4/16.
 */
public interface GetMemberDetailsResponse {

    public void onGetMemberDetailsResponse(boolean status,JSONObject jsonObject);
}
