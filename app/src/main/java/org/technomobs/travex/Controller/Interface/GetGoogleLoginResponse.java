package org.technomobs.travex.Controller.Interface;

/**
 * Created by technomobs on 2/3/16.
 */
public interface GetGoogleLoginResponse {
    public void onGetGoogleLoginResponse(boolean status);
}
