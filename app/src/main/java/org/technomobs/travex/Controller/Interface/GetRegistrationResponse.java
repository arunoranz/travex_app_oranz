package org.technomobs.travex.Controller.Interface;

import org.json.JSONObject;

/**
 * Created by technomobs on 19/2/16.
 */
public interface GetRegistrationResponse {

    public void onRegistrationResponse(boolean status);

}
