package org.technomobs.travex.Location;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import org.technomobs.travex.Utillity.LogUtility;

/**
 * Created by technomobs on 15/2/16.
 */
public class LocationCoordinates implements LocationListener {
    public static LocationManager sLocationManager = null;
    public static Context sContext = null;
    public Location mLocation = null;
    private static final long MIN_DISTANCE_FOR_UPDATE = 0;
    private static final long MIN_TIME_FOR_UPDATE = 0;
    private static final String TAG = LocationCoordinates.class.getSimpleName();

    public LocationCoordinates(Context context) {
        sLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        sContext = context;
    }


    private Location getLocation() {
        Location mLocation = null;

        mLocation = getTheLocationFromNetwork();

        /*if (mLocation != null) {
            LogUtility.Log(TAG, "getLocation:passive provider:location not null:", null);
            return mLocation;

        }

        mLocation = getTheLocationFromGps();

        if (mLocation != null) {
            LogUtility.Log(TAG, "getLocation:gps:result:location not null:", null);
            return mLocation;

        }
        mLocation = getTheLocationFromNetwork();
        if (mLocation != null) {

            LogUtility.Log(TAG, "getLocation:n/w:result:location not null:", null);

            return mLocation;

        }*/






      /*  boolean isPassiveProviderEnabled = sLocationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER);
        boolean isNetworkEnabled = sLocationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        boolean isGpsEnabled = sLocationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);*/


        /*if ((isPassiveProviderEnabled) || (isNetworkEnabled)) {
            LogUtility.Log(TAG, "getLocation:Gps or network enabled", null);
            if (isPassiveProviderEnabled) {
                try {
                    sLocationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, MIN_TIME_FOR_UPDATE, MIN_DISTANCE_FOR_UPDATE, this);

                    if (sLocationManager != null) {
                        mLocation = sLocationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
                        LogUtility.Log(TAG, "getLocation:passive provider  enabled", "mLocation:" + mLocation);

                        return mLocation;
                    }
                } catch (SecurityException e) {
                    LogUtility.Log(TAG, "getLocation:", "SecurityException:passive provider" + e.getMessage());

                }
            } else if (isNetworkEnabled) {
                try {
                    sLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_FOR_UPDATE, MIN_DISTANCE_FOR_UPDATE, this);
                    if (sLocationManager != null) {
                        mLocation = sLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        LogUtility.Log(TAG, "getLocation:network  enabled", "mLocation:" + mLocation);

                        return mLocation;
                    }
                } catch (SecurityException e) {
                    LogUtility.Log(TAG, "getLocation:", "SecurityException:network:" + e.getMessage());

                }
            }


        } else {
            LogUtility.Log(TAG, "getLocation:Gps and network not enabled", null);

        }*/
        return mLocation;
    }


    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public Location getTheLocationFromGps() {
        boolean isGpsEnabled = sLocationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);


        if (isGpsEnabled) {
            try {
                sLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_FOR_UPDATE, MIN_DISTANCE_FOR_UPDATE, this);

                if (sLocationManager != null) {
                    mLocation = sLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    LogUtility.Log(TAG, "getTheLocationFromGps:gps provider  enabled", "mLocation:" + mLocation);

                    return mLocation;
                }
            } catch (SecurityException e) {
                LogUtility.Log(TAG, "getTheLocationFromGps:", "SecurityException:gps provider" + e.getMessage());

            }
        } else {
            LogUtility.Log(TAG, "getTheLocationFromGps:gps provider not enabled", null);

        }
        return null;

    }

    public Location getTheLocationFromNetwork() {
        boolean isNetworkEnabled = sLocationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);


        if (isNetworkEnabled) {
            try {
                sLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_FOR_UPDATE, MIN_DISTANCE_FOR_UPDATE, this);

                if (sLocationManager != null) {
                    mLocation = sLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    LogUtility.Log(TAG, "getTheLocationFromNetwork:n/w provider  enabled", "mLocation:" + mLocation);

                    return mLocation;
                }
            } catch (SecurityException e) {
                LogUtility.Log(TAG, "getTheLocationFromNetwork:", "SecurityException:n/w provider" + e.getMessage());

            }
        } else {
            LogUtility.Log(TAG, "getTheLocationFromNetwork:n/w provider not enabled", null);
        }
        return null;

    }

    public Location getTheLocationFromPassive() {
        boolean isPassiveProviderEnabled = sLocationManager
                .isProviderEnabled(LocationManager.PASSIVE_PROVIDER);


        if (isPassiveProviderEnabled) {
            try {
                sLocationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, MIN_TIME_FOR_UPDATE, MIN_DISTANCE_FOR_UPDATE, this);

                if (sLocationManager != null) {
                    mLocation = sLocationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
                    LogUtility.Log(TAG, "getTheLocationFromPassive:passive provider  enabled", "mLocation:" + mLocation);

                    return mLocation;
                }
            } catch (SecurityException e) {
                LogUtility.Log(TAG, "getTheLocationFromPassive:", "SecurityException:passive provider" + e.getMessage());

            }
        } else {
            LogUtility.Log(TAG, "getTheLocationFromPassive:passive provider not enabled", null);
        }
        return null;

    }
}
