package org.technomobs.travex.Location;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;

import org.technomobs.travex.Utillity.LogUtility;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by technomobs on 15/2/16.
 */
public class CurrentLocationDetails {

    Geocoder mGeocoder = null;
    public static final String TAG = CurrentLocationDetails.class.getSimpleName();


    public CurrentLocationDetails(Context context) {
        mGeocoder = new Geocoder(context, Locale.getDefault());
        LogUtility.Log(TAG, "CurrentLocationDetails", null);
    }

    private List<Address> getLocationDetails(Location location) {
        try {
            List<Address> details = mGeocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            LogUtility.Log(TAG, "getLocationDetails", "details:" + details);

            return details;
        } catch (IOException e) {
            LogUtility.Log(TAG, "getLocationDetails", "IOException:" + e.getMessage());

            e.printStackTrace();
            return null;
        }
    }

    public String getCurrentLocality(Location location) {
        try {
            Address mAddress = getLocationDetails(location).get(0);
            LogUtility.Log(TAG, "getCurrentLocality", "current address:" + mAddress);

            String locality = mAddress.getLocality();
            LogUtility.Log(TAG, "getCurrentLocality", "current locality:" + locality);
            return locality;
        } catch (NullPointerException e) {
            LogUtility.Log(TAG, "getCurrentLocality:NullPointerException", e.getMessage());

            return null;

        } catch (ArrayIndexOutOfBoundsException e) {
            LogUtility.Log(TAG, "getCurrentLocality:ArrayIndexOutOfBoundsException", e.getMessage());

            return null;
        }


    }

    public String getCurrentCountryName(Location location) {
        try {
            Address mAddress = getLocationDetails(location).get(0);
            LogUtility.Log(TAG, "getCurrentCountryName", "current address:" + mAddress);

            String name = mAddress.getCountryName();
            LogUtility.Log(TAG, "getCurrentCountryname", "current country name:" + name);

            return name;
        } catch (Exception e) {
            LogUtility.Log(TAG, "getCurrentCountryName:exception", e.getMessage());
            return null;
        }


    }


}
