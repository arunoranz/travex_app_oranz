package org.technomobs.travex.Utillity;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by technomobs on 20/2/16.
 */
public class ListCreatorForNameValuePairRequest {


    Context mContext = null;

    public ListCreatorForNameValuePairRequest(Context context) {

        mContext = context;

    }

    public ArrayList<String> getListForRegistrationRequest(String name, String email, String pwd, String dev_id, String dev_type, String auth) {
        ArrayList<String> list = new ArrayList<String>();
        list.add(name);
        list.add(email);
        list.add(pwd);
        list.add(dev_id);
        list.add(dev_type);
        list.add(auth);
        return list;


    }

    public ArrayList<String> getListForLoginRequest(String email, String pwd) {

        ArrayList<String> list = new ArrayList<String>();
        list.add(email);
        list.add(pwd);
        return list;

    }

    public ArrayList<String> getListForCitySuggestionRequest(String member_id, String city) {

        ArrayList<String> list = new ArrayList<String>();
        list.add(member_id);
        list.add(city);
        return list;

    }

    public ArrayList<String> getListForVerifyCodeRequest(String member_id, String code) {

        ArrayList<String> list = new ArrayList<String>();
        list.add(member_id);
        list.add(code);


        return list;

    }

    public ArrayList<String> getListOfSplashLanding_ImagesRequest(String value) {
        ArrayList<String> list = new ArrayList<String>();
        list.add(value);
        return list;

    }

    public ArrayList<String> getListOfFeatureImagesRequest(String value) {
        ArrayList<String> list = new ArrayList<String>();
        list.add(value);
        return list;

    }


    public ArrayList<String> getListOfMemberUpdateProfile(String member_id, String member_gender, String member_martial_status,
                                                          String member_no_kids, String member_country, String member_currency, String member_income,
                                                          String member_zip_code, String member_languages, String member_food_preferences, String member_phobias,
                                                          String member_stay_preferences, String member_disabilities, String member_unwind) {
        ArrayList<String> list = new ArrayList<String>();
        list.add(member_id);
        list.add(member_gender);
        list.add(member_martial_status);
        list.add(member_no_kids);
        list.add(member_country);
        list.add(member_currency);
        list.add(member_income);
        list.add(member_zip_code);
        list.add(member_languages);
        list.add(member_food_preferences);
        list.add(member_phobias);
        list.add(member_stay_preferences);
        list.add(member_disabilities);
        list.add(member_unwind);
        return list;

    }

    public ArrayList<String> getListofSocialLogin(String name, String email
            , String social_id, String device_id, String device_type, String auth_method) {
        ArrayList<String> list = new ArrayList<String>();
        list.add(name);
        list.add(email);
        list.add(social_id);
        list.add(device_id);
        list.add(device_type);
        list.add(auth_method);


        return list;
    }

    public ArrayList<String> getListOfGeneralResultsSearchApi(String category, String city,
                                                              String area, String searchterm,
                                                              String alphabet, String start, String limit) {
        ArrayList<String> list = new ArrayList<String>();
        list.add(category);
        list.add(city);
        list.add(area);
        list.add(searchterm);
        list.add(alphabet);
        list.add(start);
        list.add(limit);
        return list;

    }

    public ArrayList<String> getListOfKeywordbasedlResultsSearchApi(String category, String city, String area, String term) {

        ArrayList<String> list = new ArrayList<String>();
        list.add(category);
        list.add(city);
        list.add(area);
        list.add(term);

        return list;

    }

    public ArrayList<String> getListOfSearchItemSpecificApi(String category, String item_id) {
        ArrayList<String> list = new ArrayList<String>();
        list.add(category);
        list.add(item_id);

        return list;


    }

    public ArrayList<String> getListOfSearchNowApi(String category, String city, String area, String user_flag, String user_lat, String user_lng) {
        ArrayList<String> list = new ArrayList<String>();
        list.add(category);
        list.add(city);
        list.add(area);
        list.add(user_flag);
        list.add(user_lat);
        list.add(user_lng);


        return list;


    }

    public ArrayList<String> getListOfSearchApiGetFilters(String city_id, String category) {

        ArrayList<String> list = new ArrayList<String>();
        list.add(city_id);
        list.add(category);
        return list;

    }

    /**
     * @param city_id
     * @param searchterm
     * @param area
     * @param limit
     * @param alphabet
     * @param start
     * @param setfilter
     * @param filter_keyword
     * @param filter_rating
     * @param category
     * @param item1
     * @param item2
     * @param item3
     * @return
     */
    public ArrayList<String> getListOfSearchApiGetFilterResults_Airline_Shopping_Business_Events_Spa(String city_id,
                                                                                                     String searchterm,
                                                                                                     String area, String limit,
                                                                                                     String alphabet, String start,
                                                                                                     String setfilter, String filter_keyword,
                                                                                                     String filter_rating, String category,
                                                                                                     String item1, String item2,
                                                                                                     String item3) {

        ArrayList<String> list = new ArrayList<String>();
        list.add(city_id);
        list.add(searchterm);
        list.add(area);
        list.add(limit);
        list.add(alphabet);
        list.add(start);
        list.add(setfilter);
        list.add(filter_keyword);
        list.add(filter_rating);
        list.add(category);
        list.add(item1);
        list.add(item2);
        list.add(item3);
        return list;

    }


    public ArrayList<String> getListOfSearchApiGetFilterResults_RealEstate_NightLife(String city_id,
                                                                                     String searchterm,
                                                                                     String area,
                                                                                     String limit,
                                                                                     String alphabet,
                                                                                     String start,
                                                                                     String setfilter,
                                                                                     String filter_keyword,
                                                                                     String filter_rating,
                                                                                     String category,
                                                                                     String item1, String item2,
                                                                                     String item3, String item4) {

        ArrayList<String> list = new ArrayList<String>();
        list.add(city_id);
        list.add(searchterm);
        list.add(area);
        list.add(limit);
        list.add(alphabet);
        list.add(start);
        list.add(setfilter);
        list.add(filter_keyword);
        list.add(filter_rating);
        list.add(category);
        list.add(item1);
        list.add(item2);
        list.add(item3);
        list.add(item4);
        return list;

    }

    public ArrayList<String> getListOfSearchApiGetFilterResults_moneyExchange_carRentals_Emergencies_Embasies_ThingsToDo(String city_id,
                                                                                                                         String searchterm,
                                                                                                                         String area,
                                                                                                                         String limit,
                                                                                                                         String alphabet,
                                                                                                                         String start,
                                                                                                                         String setfilter,
                                                                                                                         String filter_keyword,
                                                                                                                         String filter_rating,
                                                                                                                         String category,
                                                                                                                         String item1, String item2
    ) {

        ArrayList<String> list = new ArrayList<String>();
        list.add(city_id);
        list.add(searchterm);
        list.add(area);
        list.add(limit);
        list.add(alphabet);
        list.add(start);
        list.add(setfilter);
        list.add(filter_keyword);
        list.add(filter_rating);
        list.add(category);
        list.add(item1);
        list.add(item2);

        for (int i = 0; i < list.size(); i++) {
            LogUtility.Log("list", "search api list", "item:" + list.get(i));
        }
        return list;

    }


    public ArrayList<String> getListOfSearchApiGetFilterResults_stayHotels(String city_id,
                                                                           String searchterm,
                                                                           String area, String limit,
                                                                           String alphabet, String start,
                                                                           String setfilter, String filter_keyword,
                                                                           String filter_rating,
                                                                           String category,
                                                                           String item1,
                                                                           String item2,
                                                                           String item3,
                                                                           String item4,
                                                                           String item5,
                                                                           String item6
    ) {

        ArrayList<String> list = new ArrayList<String>();
        list.add(city_id);
        list.add(searchterm);
        list.add(area);
        list.add(limit);
        list.add(alphabet);
        list.add(start);
        list.add(setfilter);
        list.add(filter_keyword);
        list.add(filter_rating);
        list.add(category);
        list.add(item1);
        list.add(item2);
        list.add(item3);
        list.add(item4);
        list.add(item5);
        list.add(item6);

        return list;

    }

    public ArrayList<String> getListOfSearchApiGetFilterResults_TourAndTravel(String city_id,
                                                                              String searchterm,
                                                                              String area,
                                                                              String limit,
                                                                              String alphabet,
                                                                              String start,
                                                                              String setfilter,
                                                                              String filter_keyword,
                                                                              String filter_rating,
                                                                              String category,
                                                                              String item1) {
        ArrayList<String> list = new ArrayList<String>();
        list.add(city_id);
        list.add(searchterm);
        list.add(area);
        list.add(limit);
        list.add(alphabet);
        list.add(start);
        list.add(setfilter);
        list.add(filter_keyword);
        list.add(filter_rating);
        list.add(category);
        list.add(item1);
        return list;
    }

    public ArrayList<String> getListOfSearchApiGetFilterResults_EatingOut(String city_id,
                                                                          String searchterm,
                                                                          String area,
                                                                          String limit,
                                                                          String alphabet,
                                                                          String start,
                                                                          String setfilter,
                                                                          String filter_keyword,
                                                                          String filter_rating,
                                                                          String category,
                                                                          String item1,
                                                                          String item2,
                                                                          String item3,
                                                                          String item4,
                                                                          String item5,
                                                                          String item6,
                                                                          String item7

    ) {

        ArrayList<String> list = new ArrayList<String>();
        list.add(city_id);
        list.add(searchterm);
        list.add(area);
        list.add(limit);
        list.add(alphabet);
        list.add(start);
        list.add(setfilter);
        list.add(filter_keyword);
        list.add(filter_rating);
        list.add(category);
        list.add(item1);
        list.add(item2);
        list.add(item3);
        list.add(item4);
        list.add(item5);
        list.add(item6);
        list.add(item7);

        return list;

    }

    public ArrayList<String> getListOfArticles(String city_id, String month, String year) {

        ArrayList<String> list = new ArrayList<String>();
        list.add(city_id);
        list.add(month);
        list.add(year);


        return list;
    }

    public ArrayList<String> getListOfArticles(String city_id, String month, String year, String featured_category_id) {

        ArrayList<String> list = new ArrayList<String>();
        list.add(city_id);
        list.add(month);
        list.add(year);
        list.add(featured_category_id);


        return list;
    }

  /*  public ArrayList<String> getListOfArticles(String city_id, String month, String year, String category_id, String placeholder) {

        ArrayList<String> list = new ArrayList<String>();
        list.add(city_id);
        list.add(month);
        list.add(year);
        list.add(category_id);


        return list;
    }*/

    public ArrayList<String> getListOfArticlesDetails(String article_id) {

        ArrayList<String> list = new ArrayList<String>();
        list.add(article_id);

        return list;

    }

    public ArrayList<String> getListOfEssentials_List(String city_id) {

        ArrayList<String> list = new ArrayList<String>();
        list.add(city_id);

        return list;

    }

    public ArrayList<String> getListOfEssentialsDetails(String essentials_id) {

        ArrayList<String> list = new ArrayList<String>();
        list.add(essentials_id);

        return list;

    }

    public ArrayList<String> getListOfTips_List(String city_id, String category_id) {

        ArrayList<String> list = new ArrayList<String>();
        list.add(city_id);
        list.add(category_id);
        return list;

    }

    public ArrayList<String> getListOfTipsDetails(String tips_id) {

        ArrayList<String> list = new ArrayList<String>();
        list.add(tips_id);

        return list;

    }

    public ArrayList<String> getListOfAddplan(String item_id, String item_name, String time, String user_id, String date, String category) {

        ArrayList<String> list = new ArrayList<String>();
        list.add(item_id);
        list.add(item_name);
        list.add(time);
        list.add(user_id);
        list.add(date);
        list.add(category);


        return list;

    }

    public ArrayList<String> getListOfPlanList(String user_id) {
        ArrayList<String> list = new ArrayList<String>();
        list.add(user_id);
        return list;
    }

    public ArrayList<String> getListOfAddSuggestions(String member_id, String category_id, String suggestion) {
        ArrayList<String> list = new ArrayList<String>();
        list.add(member_id);
        list.add(category_id);
        list.add(suggestion);
        return list;
    }

    public ArrayList<String> getListOfAboutUs(String type) {
        ArrayList<String> list = new ArrayList<String>();
        list.add(type);

        return list;
    }

    public ArrayList<String> getListOfContactUs(String type) {
        ArrayList<String> list = new ArrayList<String>();
        list.add(type);

        return list;
    }

    public ArrayList<String> getListOfEnquiry(String name, String email, String number, String subject, String message) {
        ArrayList<String> list = new ArrayList<String>();
        list.add(name);
        list.add(email);
        list.add(number);
        list.add(subject);
        list.add(message);

        return list;
    }

    public ArrayList<String> getListOfUpdateplan(String user_id, String plan_id, String date) {
        ArrayList<String> list = new ArrayList<String>();

        list.add(user_id);

        list.add(plan_id);


        list.add(date);


        return list;
    }

    public ArrayList<String> getListOfDeleteplan(String user_id, String plan_id) {

        ArrayList<String> list = new ArrayList<String>();
        list.add(user_id);

        list.add(plan_id);

        return list;


    }

    public ArrayList<String> getListOfDoneplan(String user_id, String plan_id) {

        ArrayList<String> list = new ArrayList<String>();
        list.add(user_id);

        list.add(plan_id);

        return list;


    }

    public ArrayList<String> getListOfDayplan(String user_id, String date) {

        ArrayList<String> list = new ArrayList<String>();
        list.add(user_id);

        list.add(date);

        return list;


    }

    public ArrayList<String> getListOfTravexEventAndMonthlyPlan(String user_id, String month, String year) {

        ArrayList<String> list = new ArrayList<String>();
        list.add(user_id);

        list.add(month);
        list.add(year);


        return list;


    }

    public ArrayList<String> getListOfWriteReview(String user_id, String category,
                                                  String item_id, String item_name,
                                                  String comment, String rating) {

        ArrayList<String> list = new ArrayList<String>();
        list.add(user_id);

        list.add(category);
        list.add(item_id);
        list.add(item_name);
        list.add(comment);
        list.add(rating);


        return list;


    }

    public ArrayList<String> getListOfTravexEventDetails(String event_id) {

        ArrayList<String> list = new ArrayList<String>();
        list.add(event_id);


        return list;


    }


    public ArrayList<String> getListOfGetSearchTerminals_Destinations(String airport_id) {

        ArrayList<String> list = new ArrayList<String>();
        list.add(airport_id);

        return list;


    }

    public ArrayList<String> getListOfProfileUpdateImagecontent(String member_id, String member_profile_image_path) {

        ArrayList<String> list = new ArrayList<String>();
        list.add(member_id);
        list.add(member_profile_image_path);
        return list;


    }

    public ArrayList<String> getListOfMemberDetails(String member_id) {

        ArrayList<String> list = new ArrayList<String>();
        list.add(member_id);
        return list;


    }

    public ArrayList<String> getListOfForgetPassword(String member_email) {

        ArrayList<String> list = new ArrayList<String>();
        list.add(member_email);
        return list;


    }

    public ArrayList<String> getListOfProfileTagProfileUpdate(String member_last_name, String member_email,
                                                              String member_martial_status,
                                                              String member_profile_image,
                                                              String member_first_name,
                                                              String member_gender,
                                                              String member_no_kids) {

        ArrayList<String> list = new ArrayList<String>();
        list.add(member_last_name);
        list.add(member_email);
        list.add(member_martial_status);
        list.add(member_profile_image);
        list.add(member_first_name);
        list.add(member_gender);
        list.add(member_no_kids);

        return list;


    }

    public ArrayList<String> getListOfProfileAnswersTagProfileUpdate(String profile_questions_id, String type,
                                                                     String answer_id) {

        ArrayList<String> list = new ArrayList<String>();
        list.add(profile_questions_id);
        list.add(type);
        list.add(answer_id);


        return list;


    }
}
