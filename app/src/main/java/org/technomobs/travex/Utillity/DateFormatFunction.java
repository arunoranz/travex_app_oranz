package org.technomobs.travex.Utillity;

import java.sql.Date;
import java.text.SimpleDateFormat;

public class DateFormatFunction {

	
	public String dateformat(String Currentdateformat,String ConvertedDateFormat,String InputDate)
	{
		String Date="";
		SimpleDateFormat dateFormat = new SimpleDateFormat(Currentdateformat);
		SimpleDateFormat convertdateFormat = new SimpleDateFormat(ConvertedDateFormat);
		java.util.Date formatteddate = new Date(0);
		java.util.Date convertedDate = new Date(0);
	    
			try {
				formatteddate = dateFormat.parse(InputDate);
				Date = convertdateFormat.format(formatteddate).toString();
	    	
			} catch (Exception e) {
	         //TODO Auto-generated catch block
	        e.printStackTrace();
			}
		
		
		return Date;
	}

	public java.util.Date Dateformat_as_date(String Currentdateformat,String ConvertedDateFormat,String InputDate)
	{
		String time = InputDate;
		SimpleDateFormat dateFormat = new SimpleDateFormat(Currentdateformat);
		SimpleDateFormat dateFormat2 = new SimpleDateFormat(ConvertedDateFormat);
		java.util.Date date = null;
		java.util.Date date1 = null;
		try {
			date = dateFormat.parse(time);

			String out = dateFormat2.format(date);
			date1 = dateFormat.parse(out);
		} catch (Exception e) {
		}


		return date1;
	}

	public static String getDayNumberSuffix(int day) {
		if (day >= 11 && day <= 13) {
			return "th";
		}
		switch (day % 10) {
			case 1:
				return "st";
			case 2:
				return "nd";
			case 3:
				return "rd";
			default:
				return "th";
		}
	}
}
