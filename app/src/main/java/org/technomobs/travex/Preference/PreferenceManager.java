package org.technomobs.travex.Preference;

import android.content.Context;
import android.content.SharedPreferences;

import org.technomobs.travex.Utillity.LogUtility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by technomobs on 15/2/16.
 */
public class PreferenceManager {
    private static final String TAG = PreferenceManager.class.getSimpleName();
    private static SharedPreferences sSharedPreferences = null;
    private static SharedPreferences.Editor sEditor = null;
    private static final String TRAVEX_PREFERENCE = "org.technomobs.travex.preference";
    private static final String DEFAULT_VALUE = null;
    private static final Boolean DEFAULT_BOOLEAN = false;
    private String mCurrentLattitude = null;
    private String mCurrentLongitude = null;
    private String mCurrentLocalityName = null;
    private String mCurrentcountryName = null;
    private String mRegisration_Id_response = null;

    private String mApiKey = null;
    private Boolean mKey = null;


    private String mRegisteredUser_name = "";
    private String mRegisteredUser_email = null;


    private String mLogin_Type = null;


    private String mEmail_General_User = null;


    private String category_general_search = null;


    private String category_now_search = null;


    private boolean mLocationPermissionSetOrNot = false;

    private boolean sCurrentLocInCityList = false;


    private ArrayList<String> cms_image_names_feature = null;
    private ArrayList<String> cms_image_content_feature = null;
    private ArrayList<String> cms_image_update_date_feature = null;
    private ArrayList<String> cms_image_title_feature = null;
    private ArrayList<String> cms_image_description_feature = null;

    private ArrayList<String> cms_image_asset_url_feature = null;
    private ArrayList<String> splash_landing_images_names = null;
    private ArrayList<String> splash_landing_images_content = null;
    private ArrayList<String> splash_landing_images_asset_url = null;
    private ArrayList<String> splash_landing_images_update_time = null;


    private ArrayList<String> mSupportedCountryId = null;
    private ArrayList<String> mSupportedCountryNames = null;
    private ArrayList<String> mSupportedCountryLatitude = null;
    private ArrayList<String> mSupportedCountryLongitude = null;


    /**
     * preferences for search api in  general for all
     */
    private ArrayList<String> mSearchResultsGeneral_id = null;

    private ArrayList<String> mSearchResultsGeneral_title = null;

    private ArrayList<String> mSearchResultsGeneral_counterimages = null;
    private ArrayList<String> mSearchResultsGeneral_countervideos = null;
    private ArrayList<String> mSearchResultsGeneral_imagesurl = null;


    private ArrayList<String> mSearchResultsGeneral_latitude = null;
    private ArrayList<String> mSearchResultsGeneral_longitude = null;
    private ArrayList<String> mSearchResultsGeneral_phonenumber = null;
    private ArrayList<String> mSearchResultsGeneral_subtitle = null;
    private ArrayList<String> mSearchResultsGeneral_location = null;
    private ArrayList<String> mSearchResultsGeneral_review = null;
    private ArrayList<String> mSearchResultsGeneral_description = null;
    private ArrayList<String> mSearchResultsGeneral_website = null;


    private ArrayList<String> mSupportedCountryTags = null;


    /**
     * preferences for tips id comes from the tips list api
     */
    private ArrayList<String> mTips_id = null;

    /**
     * preferences for essentials id comes from the essentials list api
     */
    private ArrayList<String> mEssentials_id = null;

    /**
     * preferences for articles id comes from the articles list api
     */
    private ArrayList<String> mArticles_id = null;

    /**
     * preferences for search api keyword vice
     */
    private ArrayList<String> mSearchResultsKeyWord_suggestion = null;
    private ArrayList<String> mSearchResultsKeyWord_type = null;
    private ArrayList<String> mSearchResultsKeyWord_id = null;


    /**
     * preferences for search item
     */
    private ArrayList<String> mSearchItemSpecific_category = null;
    private ArrayList<String> mSearchItemSpecific_item_id = null;
    private ArrayList<String> mSearchItemSpecific_name = null;
    private ArrayList<String> mSearchItemSpecific_description = null;
    private ArrayList<String> mSearchItemSpecific_address = null;
    private ArrayList<String> mSearchItemSpecific_website = null;
    private ArrayList<String> mSearchItemSpecific_phone = null;
    private ArrayList<String> mSearchItemSpecific_latitude = null;
    private ArrayList<String> mSearchItemSpecific_longitude = null;
    private ArrayList<String> mSearchItemSpecific_area = null;
    private ArrayList<String> mSearchItemSpecific_city = null;
    private ArrayList<String> mSearchItemSpecific_district = null;
    private ArrayList<String> mSearchItemSpecific_state = null;
    private ArrayList<String> mSearchItemSpecific_country = null;
    private ArrayList<String> mSearchItemSpecific_currency = null;
    private ArrayList<String> mSearchItemSpecific_countryshort = null;
    private ArrayList<String> mSearchItemSpecific_airport = null;
    private ArrayList<String> mSearchItemSpecific_terminal = null;
    private ArrayList<String> mSearchItemSpecific_destination = null;
    private ArrayList<String> mSearchItemSpecific_rating = null;
    private ArrayList<String> mSearchItemSpecific_images = null;
    private ArrayList<String> mSearchItemSpecific_videos = null;


    private ArrayList<String> mNow_category = null;
    private ArrayList<String> mNow_id = null;
    private ArrayList<String> mNow_name = null;
    private ArrayList<String> mNow_latitude = null;
    private ArrayList<String> mNow_longitude = null;


    private ArrayList<String> mPlan_id = null;


    public ArrayList<String> getTips_id() {
        mTips_id = mTinyDB.getListString("tips_id");
        showPushDetails(mTips_id);
        return mTips_id;
    }

    public void setTips_id(ArrayList<String> mTips_id) {
        mTinyDB.putListString("tips_id", mTips_id);
    }

    public ArrayList<String> getSupportedCountryTags() {
        mSupportedCountryTags = mTinyDB.getListString("supported_country_tags");
        showPushDetails(mSupportedCountryTags);
        return mSupportedCountryTags;
    }

    public void setSupportedCountryTags(ArrayList<String> mSupportedCountryTags) {
        mTinyDB.putListString("supported_country_tags", mSupportedCountryTags);
    }

    public ArrayList<String> getEssentials_id() {
        mEssentials_id = mTinyDB.getListString("essentials_id");
        showPushDetails(mEssentials_id);
        return mEssentials_id;
    }

    public void setEssentials_id(ArrayList<String> mEssentials_id) {
        mTinyDB.putListString("essentials_id", mEssentials_id);
    }

    public ArrayList<String> getSearchResultsGeneral_id() {
        mSearchResultsGeneral_id = mTinyDB.getListString("general_search_id");
        showPushDetails(mSearchResultsGeneral_id);
        return mSearchResultsGeneral_id;
    }

    public void setSearchResultsGeneral_id(ArrayList<String> mSearchResultsGeneral_id) {
        mTinyDB.putListString("general_search_id", mSearchResultsGeneral_id);
    }

    public ArrayList<String> getPlan_id() {
        mPlan_id = mTinyDB.getListString("plan_id");
        showPushDetails(mPlan_id);
        return mPlan_id;
    }

    public void setPlan_id(ArrayList<String> mPlan_id) {
        mTinyDB.putListString("plan_id", mPlan_id);
    }

    public ArrayList<String> getNow_category() {
        mNow_category = mTinyDB.getListString("now_category");

        return mNow_category;
    }

    public void setNow_category(ArrayList<String> mNow_category) {
        mTinyDB.putListString("now_category", mNow_category);
    }

    public ArrayList<String> getNow_id() {
        mNow_id = mTinyDB.getListString("now_id");
        showPushDetails(mNow_id);
        return mNow_id;
    }

    public void setNow_id(ArrayList<String> mNow_id) {
        mTinyDB.putListString("now_id", mNow_id);
    }

    public ArrayList<String> getNow_latitude() {
        mNow_latitude = mTinyDB.getListString("now_latitude");

        return mNow_latitude;
    }

    public void setNow_latitude(ArrayList<String> mNow_latitude) {
        mTinyDB.putListString("now_latitude", mNow_latitude);
    }

    public ArrayList<String> getNow_longitude() {
        mNow_longitude = mTinyDB.getListString("now_longitude");

        return mNow_longitude;
    }

    public void setNow_longitude(ArrayList<String> mNow_longitude) {
        mTinyDB.putListString("now_longitude", mNow_longitude);
    }

    public ArrayList<String> getArticles_id() {
        mArticles_id = mTinyDB.getListString("articles_id");
        showPushDetails(mArticles_id);
        return mArticles_id;
    }

    public void setArticles_id(ArrayList<String> mArticles_id) {
        mTinyDB.putListString("articles_id", mArticles_id);
    }

    public ArrayList<String> getNow_name() {
        mNow_name = mTinyDB.getListString("now_name");

        return mNow_name;
    }

    public void setNow_name(ArrayList<String> mNow_name) {
        mTinyDB.putListString("now_name", mNow_name);

    }

    public ArrayList<String> getSearchItemSpecific_address() {
        mSearchItemSpecific_address = mTinyDB.getListString("search_item_specific_address");

        return mSearchItemSpecific_address;
    }

    public void setSearchItemSpecific_address(ArrayList<String> mSearchItemSpecific_address) {
        mTinyDB.putListString("search_item_specific_address", mSearchItemSpecific_address);
    }

    public ArrayList<String> getSearchItemSpecific_airport() {
        mSearchItemSpecific_airport = mTinyDB.getListString("search_item_specific_airport");

        return mSearchItemSpecific_airport;
    }

    public void setSearchItemSpecific_airport(ArrayList<String> mSearchItemSpecific_airport) {
        mTinyDB.putListString("search_item_specific_airport", mSearchItemSpecific_airport);
    }

    public ArrayList<String> getSearchItemSpecific_area() {
        mSearchItemSpecific_area = mTinyDB.getListString("search_item_specific_area");

        return mSearchItemSpecific_area;
    }

    public void setSearchItemSpecific_area(ArrayList<String> mSearchItemSpecific_area) {
        mTinyDB.putListString("search_item_specific_area", mSearchItemSpecific_area);
    }

    public ArrayList<String> getSearchItemSpecific_category() {
        mSearchItemSpecific_category = mTinyDB.getListString("search_item_specific_category");

        return mSearchItemSpecific_category;
    }

    public void setSearchItemSpecific_category(ArrayList<String> mSearchItemSpecific_category) {
        mTinyDB.putListString("search_item_specific_category", mSearchItemSpecific_category);
    }

    public ArrayList<String> getSearchItemSpecific_city() {
        mSearchItemSpecific_city = mTinyDB.getListString("search_item_specific_city");

        return mSearchItemSpecific_city;
    }

    public void setSearchItemSpecific_city(ArrayList<String> mSearchItemSpecific_city) {
        mTinyDB.putListString("search_item_specific_city", mSearchItemSpecific_city);
    }

    public ArrayList<String> getSearchItemSpecific_country() {
        mSearchItemSpecific_country = mTinyDB.getListString("search_item_specific_country");

        return mSearchItemSpecific_country;
    }

    public void setSearchItemSpecific_country(ArrayList<String> mSearchItemSpecific_country) {
        mTinyDB.putListString("search_item_specific_country", mSearchItemSpecific_country);
    }

    public ArrayList<String> getSearchItemSpecific_countryshort() {
        mSearchItemSpecific_countryshort = mTinyDB.getListString("search_item_specific_countryshort");

        return mSearchItemSpecific_countryshort;
    }

    public void setSearchItemSpecific_countryshort(ArrayList<String> mSearchItemSpecific_countryshort) {
        mTinyDB.putListString("search_item_specific_countryshort", mSearchItemSpecific_countryshort);
    }

    public ArrayList<String> getSearchItemSpecific_currency() {
        mSearchItemSpecific_currency = mTinyDB.getListString("search_item_specific_currency");

        return mSearchItemSpecific_currency;
    }

    public void setSearchItemSpecific_currency(ArrayList<String> mSearchItemSpecific_currency) {
        mTinyDB.putListString("search_item_specific_currency", mSearchItemSpecific_currency);
    }

    public ArrayList<String> getSearchItemSpecific_description() {
        mSearchItemSpecific_description = mTinyDB.getListString("search_item_specific_description");

        return mSearchItemSpecific_description;
    }

    public void setSearchItemSpecific_description(ArrayList<String> mSearchItemSpecific_description) {
        mTinyDB.putListString("search_item_specific_description", mSearchItemSpecific_description);
    }

    public ArrayList<String> getSearchItemSpecific_destination() {
        mSearchItemSpecific_destination = mTinyDB.getListString("search_item_specific_destination");

        return mSearchItemSpecific_destination;
    }

    public void setSearchItemSpecific_destination(ArrayList<String> mSearchItemSpecific_destination) {
        mTinyDB.putListString("search_item_specific_destination", mSearchItemSpecific_destination);
    }

    public ArrayList<String> getSearchItemSpecific_district() {
        mSearchItemSpecific_district = mTinyDB.getListString("search_item_specific_district");

        return mSearchItemSpecific_district;
    }

    public void setSearchItemSpecific_district(ArrayList<String> mSearchItemSpecific_district) {
        mTinyDB.putListString("search_item_specific_district", mSearchItemSpecific_district);
    }

    public ArrayList<String> getSearchItemSpecific_images() {
        mSearchItemSpecific_images = mTinyDB.getListString("search_item_specific_images");

        return mSearchItemSpecific_images;
    }

    public void setSearchItemSpecific_images(ArrayList<String> mSearchItemSpecific_images) {
        mTinyDB.putListString("search_item_specific_images", mSearchItemSpecific_images);
    }

    public ArrayList<String> getSearchItemSpecific_item_id() {
        mSearchItemSpecific_item_id = mTinyDB.getListString("search_item_specific_itemid");

        return mSearchItemSpecific_item_id;
    }

    public void setSearchItemSpecific_item_id(ArrayList<String> mSearchItemSpecific_item_id) {
        mTinyDB.putListString("search_item_specific_itemid", mSearchItemSpecific_item_id);
    }

    public ArrayList<String> getSearchItemSpecific_latitude() {
        mSearchItemSpecific_latitude = mTinyDB.getListString("search_item_specific_latitude");

        return mSearchItemSpecific_latitude;
    }

    public void setSearchItemSpecific_latitude(ArrayList<String> mSearchItemSpecific_latitude) {
        mTinyDB.putListString("search_item_specific_latitude", mSearchItemSpecific_latitude);
    }

    public ArrayList<String> getSearchItemSpecific_longitude() {
        mSearchItemSpecific_longitude = mTinyDB.getListString("search_item_specific_longitude");

        return mSearchItemSpecific_longitude;
    }

    public void setSearchItemSpecific_longitude(ArrayList<String> mSearchItemSpecific_longitude) {
        mTinyDB.putListString("search_item_specific_longitude", mSearchItemSpecific_longitude);
    }

    public ArrayList<String> getSearchItemSpecific_name() {
        mSearchItemSpecific_name = mTinyDB.getListString("search_item_specific_name");

        return mSearchItemSpecific_name;
    }

    public void setSearchItemSpecific_name(ArrayList<String> mSearchItemSpecific_name) {
        mTinyDB.putListString("search_item_specific_name", mSearchItemSpecific_name);
    }

    public ArrayList<String> getSearchItemSpecific_phone() {
        mSearchItemSpecific_phone = mTinyDB.getListString("search_item_specific_phone");

        return mSearchItemSpecific_phone;
    }

    public void setSearchItemSpecific_phone(ArrayList<String> mSearchItemSpecific_phone) {
        mTinyDB.putListString("search_item_specific_phone", mSearchItemSpecific_phone);
    }

    public ArrayList<String> getSearchItemSpecific_rating() {
        mSearchItemSpecific_rating = mTinyDB.getListString("search_item_specific_rating");

        return mSearchItemSpecific_rating;
    }

    public void setSearchItemSpecific_rating(ArrayList<String> mSearchItemSpecific_rating) {
        mTinyDB.putListString("search_item_specific_rating", mSearchItemSpecific_rating);
    }

    public ArrayList<String> getSearchItemSpecific_state() {
        mSearchItemSpecific_state = mTinyDB.getListString("search_item_specific_state");
        return mSearchItemSpecific_state;
    }

    public void setSearchItemSpecific_state(ArrayList<String> mSearchItemSpecific_state) {
        mTinyDB.putListString("search_item_specific_state", mSearchItemSpecific_state);
    }

    public ArrayList<String> getSearchItemSpecific_terminal() {
        mSearchItemSpecific_terminal = mTinyDB.getListString("search_item_specific_terminal");

        return mSearchItemSpecific_terminal;
    }

    public void setSearchItemSpecific_terminal(ArrayList<String> mSearchItemSpecific_terminal) {
        mTinyDB.putListString("search_item_specific_terminal", mSearchItemSpecific_terminal);
    }

    public ArrayList<String> getSearchItemSpecific_videos() {
        mSearchItemSpecific_videos = mTinyDB.getListString("search_item_specific_videos");

        return mSearchItemSpecific_videos;
    }

    public void setSearchItemSpecific_videos(ArrayList<String> mSearchItemSpecific_videos) {
        mTinyDB.putListString("search_item_specific_videos", mSearchItemSpecific_videos);
    }

    public ArrayList<String> getSearchItemSpecific_website() {
        mSearchItemSpecific_website = mTinyDB.getListString("search_item_specific_website");


        return mSearchItemSpecific_website;
    }

    public void setSearchItemSpecific_website(ArrayList<String> mSearchItemSpecific_website) {
        mTinyDB.putListString("search_item_specific_website", mSearchItemSpecific_website);
    }


    public ArrayList<String> getSearchResultsKeyWord_suggestion() {
        mSearchResultsKeyWord_suggestion = mTinyDB.getListString("search_results_keyword_suggestion");
        showPushDetails(mSearchResultsKeyWord_suggestion);
        return mSearchResultsKeyWord_suggestion;
    }

    public void setSearchResultsKeyWord_suggestion(ArrayList<String> mSearchResultsKeyWord_suggestion) {
        mTinyDB.putListString("search_results_keyword_suggestion", mSearchResultsKeyWord_suggestion);
    }

    public ArrayList<String> getSearchResultsKeyWord_type() {
        mSearchResultsKeyWord_type = mTinyDB.getListString("search_results_keyword_type");
        showPushDetails(mSearchResultsKeyWord_type);

        return mSearchResultsKeyWord_type;
    }

    public void setSearchResultsKeyWord_type(ArrayList<String> mSearchResultsKeyWord_type) {
        mTinyDB.putListString("search_results_keyword_type", mSearchResultsKeyWord_type);
    }

    public ArrayList<String> getSearchResultsKeyWord_id() {
        mSearchResultsKeyWord_id = mTinyDB.getListString("search_results_keyword_id");
        showPushDetails(mSearchResultsKeyWord_id);


        return mSearchResultsKeyWord_id;
    }

    public void setSearchResultsKeyWord_id(ArrayList<String> mSearchResultsKeyWord_id) {
        mTinyDB.putListString("search_results_keyword_id", mSearchResultsKeyWord_id);
    }


    public ArrayList<String> getSearchResultsGeneral_website() {
        mSearchResultsGeneral_website = mTinyDB.getListString("search_results_general_website_addresses");
        showPushDetails(mSearchResultsGeneral_website);

        return mSearchResultsGeneral_website;
    }

    public void setSearchResultsGeneral_website(ArrayList<String> mSearchResultsGeneral_website) {
        mTinyDB.putListString("search_results_general_website_addresses", mSearchResultsGeneral_website);
    }

    public ArrayList<String> getSearchResultsGeneral_subtitle() {

        mSearchResultsGeneral_subtitle = mTinyDB.getListString("search_results_general_sub_title");
        showPushDetails(mSearchResultsGeneral_subtitle);

        return mSearchResultsGeneral_subtitle;
    }

    public void setSearchResultsGeneral_subtitle(ArrayList<String> mSearchResultsGeneral_subtitle) {
        mTinyDB.putListString("search_results_general_sub_title", mSearchResultsGeneral_subtitle);
    }

    public ArrayList<String> getSearchResultsGeneral_review() {
        mSearchResultsGeneral_review = mTinyDB.getListString("search_results_general_reviews");
        showPushDetails(mSearchResultsGeneral_review);

        return mSearchResultsGeneral_review;
    }

    public void setSearchResultsGeneral_review(ArrayList<String> mSearchResultsGeneral_review) {
        mTinyDB.putListString("search_results_general_reviews", mSearchResultsGeneral_review);
    }

    public ArrayList<String> getSearchResultsGeneral_phonenumber() {
        mSearchResultsGeneral_phonenumber = mTinyDB.getListString("search_results_general_phonenumbers");
        showPushDetails(mSearchResultsGeneral_phonenumber);

        return mSearchResultsGeneral_phonenumber;
    }

    public void setSearchResultsGeneral_phonenumber(ArrayList<String> mSearchResultsGeneral_phonenumber) {
        mTinyDB.putListString("search_results_general_phonenumbers", mSearchResultsGeneral_phonenumber);
    }

    public ArrayList<String> getSearchResultsGeneral_longitude() {
        mSearchResultsGeneral_longitude = mTinyDB.getListString("search_results_general_longitude");
        showPushDetails(mSearchResultsGeneral_longitude);

        return mSearchResultsGeneral_longitude;
    }

    public void setSearchResultsGeneral_longitude(ArrayList<String> mSearchResultsGeneral_longitude) {
        mTinyDB.putListString("search_results_general_longitude", mSearchResultsGeneral_longitude);
    }

    public ArrayList<String> getSearchResultsGeneral_latitude() {
        mSearchResultsGeneral_latitude = mTinyDB.getListString("search_results_general_latitude");
        showPushDetails(mSearchResultsGeneral_latitude);

        return mSearchResultsGeneral_latitude;
    }

    public void setSearchResultsGeneral_latitude(ArrayList<String> mSearchResultsGeneral_latitude) {
        mTinyDB.putListString("search_results_general_latitude", mSearchResultsGeneral_latitude);
    }

    public ArrayList<String> getSearchResultsGeneral_location() {
        mSearchResultsGeneral_location = mTinyDB.getListString("search_results_general_location");
        showPushDetails(mSearchResultsGeneral_location);

        return mSearchResultsGeneral_location;
    }

    public void setSearchResultsGeneral_location(ArrayList<String> mSearchResultsGeneral_location) {
        mTinyDB.putListString("search_results_general_location", mSearchResultsGeneral_location);
    }

    public ArrayList<String> getSearchResultsGeneral_description() {

        mSearchResultsGeneral_description = mTinyDB.getListString("search_results_general_description");
        showPushDetails(mSearchResultsGeneral_description);

        return mSearchResultsGeneral_description;
    }

    public void setSearchResultsGeneral_description(ArrayList<String> mSearchResultsGeneral_description) {
        mTinyDB.putListString("search_results_general_description", mSearchResultsGeneral_description);
    }

    public ArrayList<String> getSearchResultsGeneral_title() {

        mSearchResultsGeneral_title = mTinyDB.getListString("search_results_general_title");
        showPushDetails(mSearchResultsGeneral_title);

        return mSearchResultsGeneral_title;
    }

    public void setSearchResultsGeneral_title(ArrayList<String> mSearchResultsGeneral_title) {
        mTinyDB.putListString("search_results_general_title", mSearchResultsGeneral_title);


    }

    public ArrayList<String> getSearchResultsGeneral_counterimages() {
        mSearchResultsGeneral_counterimages = mTinyDB.getListString("search_results_general_counter_images");
        showPushDetails(mSearchResultsGeneral_counterimages);

        return mSearchResultsGeneral_counterimages;
    }

    public void setSearchResultsGeneral_counterimages(ArrayList<String> mSearchResultsGeneral_counterimages) {
        mTinyDB.putListString("search_results_general_counter_images", mSearchResultsGeneral_counterimages);
    }

    public ArrayList<String> getSearchResultsGeneral_countervideos() {
        mSearchResultsGeneral_countervideos = mTinyDB.getListString("search_results_general_counter_videos");
        showPushDetails(mSearchResultsGeneral_countervideos);

        return mSearchResultsGeneral_countervideos;
    }

    public void setSearchResultsGeneral_countervideos(ArrayList<String> mSearchResultsGeneral_countervideos) {
        mTinyDB.putListString("search_results_general_counter_videos", mSearchResultsGeneral_countervideos);
    }

    public ArrayList<String> getSearchResultsGeneral_imagesurl() {
        mSearchResultsGeneral_imagesurl = mTinyDB.getListString("search_results_general_images_url");
        showPushDetails(mSearchResultsGeneral_imagesurl);
        return mSearchResultsGeneral_imagesurl;
    }

    public void setSearchResultsGeneral_imagesurl(ArrayList<String> mSearchResultsGeneral_imagesurl) {
        mTinyDB.putListString("search_results_general_images_url", mSearchResultsGeneral_imagesurl);
    }


    public String getEmail_General_User() {
        mEmail_General_User = sSharedPreferences.getString("email_general_user", DEFAULT_VALUE);

        return mEmail_General_User;
    }

    public void setEmail_General_User(String mEmail_General_User) {
        sEditor = sSharedPreferences.edit();
        sEditor.putString("email_general_user", mEmail_General_User);
        sEditor.commit();


    }

    public String getLogin_Type() {
        mLogin_Type = sSharedPreferences.getString("login_type", DEFAULT_VALUE);

        return mLogin_Type;
    }

    public void setLogin_Type(String mLogin_Type) {
        sEditor = sSharedPreferences.edit();
        sEditor.putString("login_type", mLogin_Type);
        sEditor.commit();

    }

    public String getWelcome_Message() {
        String Welcome = sSharedPreferences.getString("welcome_message", DEFAULT_VALUE);

        return Welcome;
    }

    public void setWelcome_Message(String Welcome) {
        sEditor = sSharedPreferences.edit();
        sEditor.putString("welcome_message", Welcome);
        sEditor.commit();

    }

    public String getRegisteredUser_email() {
        mRegisteredUser_email = sSharedPreferences.getString("registered_user_email", DEFAULT_VALUE);

        return mRegisteredUser_email;
    }

    public void setRegisteredUser_email(String mRegisteredUser_email) {

        sEditor = sSharedPreferences.edit();
        sEditor.putString("registered_user_email", mRegisteredUser_email);
        sEditor.commit();

    }

    public String getCategory_now_search() {
        category_now_search = sSharedPreferences.getString("category_now_search", DEFAULT_VALUE);
        return category_now_search;
    }

    public void setCategory_now_search(String category_now_search) {
        sEditor = sSharedPreferences.edit();
        sEditor.putString("category_now_search", category_now_search);
        sEditor.commit();
    }

    public String getRegisteredUser_name() {
        mRegisteredUser_name = sSharedPreferences.getString("registered_user_name", DEFAULT_VALUE);

        return mRegisteredUser_name;
    }

    public void setRegisteredUser_name(String mRegisteredUser_name) {
        sEditor = sSharedPreferences.edit();
        sEditor.putString("registered_user_name", mRegisteredUser_name);
        sEditor.commit();
    }


    public boolean getLocationPermissionSetOrNot() {
        mLocationPermissionSetOrNot = sSharedPreferences.getBoolean("location_permission_set", false);

        return mLocationPermissionSetOrNot;
    }

    public void setLocationPermissionSetOrNot(boolean mLocationPermissionSetOrNot) {
        sEditor = sSharedPreferences.edit();
        sEditor.putBoolean("location_permission_set", mLocationPermissionSetOrNot);
        sEditor.commit();
    }


    public String getRegisration_Id_response() {

        mRegisration_Id_response = sSharedPreferences.getString("reg_id_as_response", DEFAULT_VALUE);
        return mRegisration_Id_response;
    }

    public void setRegisration_Id_response(String mRegisration_Id_response) {
        sEditor = sSharedPreferences.edit();
        sEditor.putString("reg_id_as_response", mRegisration_Id_response);
        sEditor.commit();

    }

    public String getmember_profile_image() {

        String member_profile_image = sSharedPreferences.getString("profile_image", DEFAULT_VALUE);
        return member_profile_image;
    }

    public void setmember_profile_image(String member_profile_image) {
        sEditor = sSharedPreferences.edit();
        sEditor.putString("profile_image", member_profile_image);
        sEditor.commit();

    }



    /*private String mSpalshImageName1 = null;
    private String mSpalshImageName2 = null;
    private String mSpalshImageName3 = null;
    private String mSpalshImageName4 = null;
    private String mSpalshImageName5 = null;*/

    public ArrayList<String> getCms_image_names_feature() {
        cms_image_names_feature = mTinyDB.getListString("cms_image_names_feature");

        showPushDetails(cms_image_names_feature);
        return cms_image_names_feature;
    }

    public void setCms_image_names_feature(ArrayList<String> cms_image_names_feature) {
        mTinyDB.putListString("cms_image_names_feature", cms_image_names_feature);
    }

    public ArrayList<String> getCms_image_title_feature() {
        cms_image_title_feature = mTinyDB.getListString("cms_image_title_feature");
        showPushDetails(cms_image_title_feature);

        return cms_image_title_feature;
    }

    public void setCms_image_title_feature(ArrayList<String> cms_image_title_feature) {
        mTinyDB.putListString("cms_image_title_feature", cms_image_title_feature);

    }

    public ArrayList<String> getCms_image_description_feature() {
        cms_image_description_feature = mTinyDB.getListString("cms_image_description_feature");
        showPushDetails(cms_image_description_feature);

        return cms_image_description_feature;
    }

    public void setCms_image_description_feature(ArrayList<String> cms_image_description_feature) {
        mTinyDB.putListString("cms_image_description_feature", cms_image_description_feature);
    }

    public ArrayList<String> getCms_image_content_feature() {
        cms_image_content_feature = mTinyDB.getListString("cms_image_content_feature");
        showPushDetails(cms_image_content_feature);
        return cms_image_content_feature;
    }

    public void setCms_image_content_feature(ArrayList<String> cms_image_content_feature) {
        mTinyDB.putListString("cms_image_content_feature", cms_image_content_feature);
    }

    public ArrayList<String> getCms_image_update_date_feature() {
        cms_image_update_date_feature = mTinyDB.getListString("cms_image_update_date_feature");
        showPushDetails(cms_image_update_date_feature);
        return cms_image_update_date_feature;
    }

    public void setCms_image_update_date_feature(ArrayList<String> cms_image_update_date_feature) {
        mTinyDB.putListString("cms_image_update_date_feature", cms_image_update_date_feature);
    }


    public ArrayList<String> getCms_image_asset_url_feature() {
        cms_image_asset_url_feature = mTinyDB.getListString("cms_feature_images_url");
        showPushDetails(cms_image_asset_url_feature);
        return cms_image_asset_url_feature;
    }

    public void setCms_image_asset_url_feature(ArrayList<String> cms_image_asset_url_feature) {
        mTinyDB.putListString("cms_feature_images_url", cms_image_asset_url_feature);
    }


    public ArrayList<String> getSplash_landing_images() {

        splash_landing_images_names = mTinyDB.getListString("splash_landing_images_names");
        return splash_landing_images_names;
    }

    public void setSplash_landing_images(ArrayList<String> splash_landing_images_names) {
        mTinyDB.putListString("splash_landing_images_names", splash_landing_images_names);

    }


    public ArrayList<String> getSplash_landing_images_update_time() {

        splash_landing_images_update_time = mTinyDB.getListString("splash_landing_images_update_time");
        showPushDetails(splash_landing_images_update_time);
        return splash_landing_images_update_time;
    }

    public void setSplash_landing_images_update_time(ArrayList<String> splash_landing_images_update_time) {
        mTinyDB.putListString("splash_landing_images_update_time", splash_landing_images_update_time);


    }


    public ArrayList<String> getSplash_landing_images_content() {

        splash_landing_images_content = mTinyDB.getListString("splash_landing_images_content");
        showPushDetails(splash_landing_images_content);


        return splash_landing_images_content;
    }

    public void setSplash_landing_images_content(ArrayList<String> splash_landing_images_content) {

        mTinyDB.putListString("splash_landing_images_content", splash_landing_images_content);

    }


    public ArrayList<String> getSplash_landing_images_asset_url() {
        splash_landing_images_asset_url = mTinyDB.getListString("splash_landing_images_asset_url");

        return splash_landing_images_asset_url;
    }

    public void setSplash_landing_images_asset_url(ArrayList<String> splash_landing_images_asset_url) {


        mTinyDB.putListString("splash_landing_images_asset_url", splash_landing_images_asset_url);


    }


    public boolean getCurrentLocInCityListIsAvailable() {
        sCurrentLocInCityList = sSharedPreferences.getBoolean("current_loc_in_db", false);
        return sCurrentLocInCityList;
    }

    public void setCurrentLocInCityListIsAvailable(boolean sCurrentLocInCityList) {

        sEditor = sSharedPreferences.edit();
        sEditor.putBoolean("current_loc_in_db", sCurrentLocInCityList);
        sEditor.commit();
    }

    TinyDB mTinyDB = null;

    public void showPushDetails(ArrayList<String> list) {
        for (int k = 0; k < list.size(); k++) {
            LogUtility.Log(TAG, "showPushDetails", "item:" + list.get(k));

        }

    }

    public ArrayList<String> getSupportedCountryId() {
        mSupportedCountryId = mTinyDB.getListString("supported_country_id");

        showPushDetails(mSupportedCountryId);
        return mSupportedCountryId;
    }

    public void setSupportedCountryId(ArrayList<String> mSupportedCountryId) {

        mTinyDB.putListString("supported_country_id", mSupportedCountryId);
    }

    public ArrayList<String> getSupportedCountryLatitude() {
        mSupportedCountryLatitude = mTinyDB.getListString("supported_country_latitude");
        showPushDetails(mSupportedCountryLatitude);

        return mSupportedCountryLatitude;
    }

    public void setSupportedCountryLatitude(ArrayList<String> mSupportedCountryLatitude) {
        mTinyDB.putListString("supported_country_latitude", mSupportedCountryLatitude);
    }

    public ArrayList<String> getSupportedCountryLongitude() {
        mSupportedCountryLongitude = mTinyDB.getListString("supported_country_longitude");
        showPushDetails(mSupportedCountryLongitude);
        return mSupportedCountryLongitude;
    }

    public void setSupportedCountryLongitude(ArrayList<String> mSupportedCountryLongitude) {
        mTinyDB.putListString("supported_country_longitude", mSupportedCountryLongitude);

    }

    public ArrayList<String> getSupportedCountryNames() {
        mSupportedCountryNames = mTinyDB.getListString("supported_country_names");
        showPushDetails(mSupportedCountryNames);

        return mSupportedCountryNames;
    }

    public void setSupportedCountryNames(ArrayList<String> mSupportedCountryNames) {
        mTinyDB.putListString("supported_country_names", mSupportedCountryNames);

    }

   /* public String getsSpalshImageName1() {
        mSpalshImageName1 = sSharedPreferences.getString("splash_image1", DEFAULT_VALUE);
        return mSpalshImageName1;
    }

    public void setsSpalshImageName1(String mSpalshImageName1) {
        sEditor = sSharedPreferences.edit();
        sEditor.putString("splash_image1", mSpalshImageName1);
        sEditor.commit();
    }

    public String getsSpalshImageName2() {
        mSpalshImageName2 = sSharedPreferences.getString("splash_image2", DEFAULT_VALUE);

        return mSpalshImageName2;
    }

    public void setsSpalshImageName2(String mSpalshImageName2) {
        sEditor = sSharedPreferences.edit();
        sEditor.putString("splash_image2", mSpalshImageName2);
        sEditor.commit();
    }

    public String getsSpalshImageName3() {
        mSpalshImageName3 = sSharedPreferences.getString("splash_image3", DEFAULT_VALUE);

        return mSpalshImageName3;
    }

    public void setsSpalshImageName3(String mSpalshImageName3) {
        sEditor = sSharedPreferences.edit();
        sEditor.putString("splash_image3", mSpalshImageName3);
        sEditor.commit();
    }

    public String getsSpalshImageName4() {
        mSpalshImageName4 = sSharedPreferences.getString("splash_image4", DEFAULT_VALUE);

        return mSpalshImageName4;
    }

    public void setsSpalshImageName4(String mSpalshImageName4) {
        sEditor = sSharedPreferences.edit();
        sEditor.putString("splash_image4", mSpalshImageName4);
        sEditor.commit();
    }

    public String getsSpalshImageName5() {
        mSpalshImageName5 = sSharedPreferences.getString("splash_image5", DEFAULT_VALUE);

        return mSpalshImageName5;
    }

    public void setsSpalshImageName5(String mSpalshImageName5) {

        sEditor = sSharedPreferences.edit();
        sEditor.putString("splash_image5", mSpalshImageName5);
        sEditor.commit();
    }*/

    /**
     * @param mCurrentLocalityName
     */
    public void setCurrentLocalityName(String mCurrentLocalityName) {
        sEditor = sSharedPreferences.edit();
        sEditor.putString("current_locality_name", mCurrentLocalityName);
        sEditor.commit();

    }

    /**
     * @return
     */
    public String getCurrentLocalityName() {
        mCurrentLocalityName = sSharedPreferences.getString("current_locality_name", DEFAULT_VALUE);
        LogUtility.Log(TAG, "getCurrentLocalityName", "mCurrentLocalityName:" + mCurrentLocalityName);
        return mCurrentLocalityName;
    }

    /**
     * @param mCurrentcountryName
     */
    public void setCurrentcountryName(String mCurrentcountryName) {
        sEditor = sSharedPreferences.edit();
        sEditor.putString("current_country_name", mCurrentcountryName);
        sEditor.commit();
    }

    /**
     * @return
     */
    public String getCurrentcountryName() {
        mCurrentcountryName = sSharedPreferences.getString("current_country_name", DEFAULT_VALUE);
        LogUtility.Log(TAG, "getCurrentcountryName", "sCurrentCountryName:" + mCurrentcountryName);

        return mCurrentcountryName;
    }

    public void setSelectedCountryName(String mSelectedcountryName) {
        sEditor = sSharedPreferences.edit();
        sEditor.putString("selected_country_name", mSelectedcountryName);
        sEditor.commit();
    }

    public String getSelectedCountryName() {
        String mSelctedcountryName = sSharedPreferences.getString("selected_country_name", DEFAULT_VALUE);
        LogUtility.Log(TAG, "selected_country_name", "selected_country_name:" + mSelctedcountryName);

        return mSelctedcountryName;
    }

    public void setCurrentcityposition(String mCurrentcityposition) {
        sEditor = sSharedPreferences.edit();
        sEditor.putString("current_city_position", mCurrentcityposition);
        sEditor.commit();
    }

    public String getCurrentcityposition() {
        String mCurrentcityposition = sSharedPreferences.getString("current_city_position", DEFAULT_VALUE);
        LogUtility.Log(TAG, "current_city_position", "sCurrentCountryid:" + mCurrentcityposition);

        return mCurrentcityposition;
    }

    public void setCurrentcountryId(String mCurrentcountryId) {
        sEditor = sSharedPreferences.edit();
        sEditor.putString("current_country_id", mCurrentcountryId);
        sEditor.commit();
    }

//    public String getCurrentcountryId() {
//        String mCurrentcountryId = sSharedPreferences.getString("current_country_id", DEFAULT_VALUE);
//        LogUtility.Log(TAG, "getCurrentcountryid", "sCurrentCountryid:" + mCurrentcountryId);
//
//        return mCurrentcountryId;
//    }

    public void setSelectedcountryId(String SelectedcountryId) {
        sEditor = sSharedPreferences.edit();
        sEditor.putString("selected_country_id", SelectedcountryId);
        sEditor.commit();
    }

    public String getSelectedcountryId() {
        String mCurrentcountryId = sSharedPreferences.getString("selected_country_id", DEFAULT_VALUE);
        LogUtility.Log(TAG, "selected_country_id", "selected_country_id:" + mCurrentcountryId);

        return mCurrentcountryId;
    }


    public String getApiKey() {
        mApiKey = sSharedPreferences.getString("api_key_received", DEFAULT_VALUE);
        return mApiKey;
    }

    public void setApiKey(String mApiKey) {
        sEditor = sSharedPreferences.edit();
        sEditor.putString("api_key_received", mApiKey);
        sEditor.commit();
    }
public void setboolean(String key,boolean value){

        sEditor=sSharedPreferences.edit();
        sEditor.putBoolean(key, value);
        sEditor.commit();

}
    public Boolean getboolean(String key) {
        mKey = sSharedPreferences.getBoolean(key, DEFAULT_BOOLEAN);
        return mKey;
    }
    public String getCategory_general_search() {
        category_general_search = sSharedPreferences.getString("general_search_categoryid", DEFAULT_VALUE);

        return category_general_search;
    }

    public void setCategory_general_search(String category_general_search) {
        sEditor = sSharedPreferences.edit();
        sEditor.putString("general_search_categoryid", category_general_search);
        sEditor.commit();
    }

    public String getCurrentLatitude() {

        mCurrentLattitude = sSharedPreferences.getString("current_lattitude", DEFAULT_VALUE);

        return mCurrentLattitude;
    }

    public void setCurrentLatitude(String mCurrentLattitude) {

        sEditor = sSharedPreferences.edit();
        sEditor.putString("current_lattitude", mCurrentLattitude);
        sEditor.commit();


    }



    public void setSelectedLatitude(String SelectedLattitude) {

        sEditor = sSharedPreferences.edit();
        sEditor.putString("selected_lattitude", SelectedLattitude);
        sEditor.commit();
    }

    public String getSelectedLatitude() {
        String SelectedLatitude = sSharedPreferences.getString("selected_lattitude", DEFAULT_VALUE);

        return SelectedLatitude;
    }

    public void setSelectedLongitude(String SelectedLongitude) {

        sEditor = sSharedPreferences.edit();
        sEditor.putString("selected_longitude", SelectedLongitude);
        sEditor.commit();
    }

    public String getSelectedLongitude() {
        String SelectedLongitude = sSharedPreferences.getString("selected_longitude", DEFAULT_VALUE);

        return SelectedLongitude;
    }

    public String getCurrentLongitude() {
        mCurrentLongitude = sSharedPreferences.getString("current_longitude", DEFAULT_VALUE);

        return mCurrentLongitude;
    }

    public void setCurrentLongitude(String mCurrentLongitude) {
        sEditor = sSharedPreferences.edit();
        sEditor.putString("current_longitude", mCurrentLongitude);
        sEditor.commit();
    }


    /**
     * @param context
     */
    public PreferenceManager(Context context) {

        sSharedPreferences = context.getSharedPreferences(TRAVEX_PREFERENCE, Context.MODE_PRIVATE);
        mTinyDB = new TinyDB(context);

    }


}
