package org.technomobs.travex.Model;

/**
 * Created by technomobs on 8/3/16.
 */
public class SearchGeneralResultItem {

    int icon_category_main = 0;
    String title = null;

    int counter_video = 0;
    int counter_image = 0;


    public SearchGeneralResultItem( int icon_category_main,String title,int counter_video, int counter_image) {
        this.title = title;
        this.icon_category_main = icon_category_main;
        this.counter_image = counter_image;
        this.counter_video = counter_video;
    }

    public int getCounter_image() {
        return counter_image;
    }

    public int getCounter_video() {
        return counter_video;
    }

    public int getIcon_category_main() {
        return icon_category_main;
    }

    public String getTitle() {
        return title;
    }
}
