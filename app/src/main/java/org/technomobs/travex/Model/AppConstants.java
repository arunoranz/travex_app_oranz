package org.technomobs.travex.Model;

/**
 * Created by technomobs on 15/2/16.
 */
public class AppConstants {
    /**
     * these are the url's of CMS and Authentication API.
     */
    // public static final String BASE_URL_SERVER = "http://52.50.14.136";
    // public static final String BASE_URL_SERVER = "http://technomobs.com.au/Travex";
    public static final String BASE_URL_SERVER = "http://travex.org";
    public static final String REGISTRATION_URL = BASE_URL_SERVER + "/authenticate_api/register/app";
    public static final String LOGIN_URL = BASE_URL_SERVER + "/authenticate_api/login";
    public static final String VERIFY_URL = BASE_URL_SERVER + "/authenticate_api/verifycode";
    public static final String SPLASH_FEATURE_IMAGES_URL = BASE_URL_SERVER + "/cms_api/getResults";
    public static final String SPLASH_IMAGES_ASSET_URL = BASE_URL_SERVER + "/uploads/cms/splashscreen_images/";
    public static final String FEATURE_IMAGES_ASSET_URL = BASE_URL_SERVER + "/uploads/cms/features_images/";
    public static final String CITY_LIST_URL = BASE_URL_SERVER + "/cms_api/getCityList";
    public static final String AREA_LIST_URL = BASE_URL_SERVER + "/cms_api/getAreaList";
    public static final String CITY_SUGGESTION_URL = BASE_URL_SERVER + "/cms_api/city_suggession";
    public static final String MEMBER_UPDATE_PROFILE_URL = BASE_URL_SERVER + "/member_api/updateprofile";
    public static final String TRAVEX_LOGOUT_URL = BASE_URL_SERVER + "/authenticate_api/logout";
    public static final String SOCIAL_LOGIN_URL = BASE_URL_SERVER + "/authenticate_api/social_login";
    public static final String GET_RESULTS_GENERAL_URL = BASE_URL_SERVER + "/search_api/getResults";

    public static final String GET_RESULTS_GENERAL_IMAGES_URL = BASE_URL_SERVER + "/uploads";
    public static final String GET_RESULTS_KEYBASED_URL = BASE_URL_SERVER + "/search_api/getSuggestions";
    public static final String GET_RESULTS_SEARCHITEM_SPECIFIC_URL = BASE_URL_SERVER + "/search_api/getDetails";
    public static final String GET_RESULTS_NOW_URL = BASE_URL_SERVER + "/search_api/now";
    public static final String GET_FILTERS_URL = BASE_URL_SERVER + "/search_api/getFilters";
    public static final String GET_FILTER_RESULTS_URL = BASE_URL_SERVER + "/search_api/getResults";
    public static final String GET_ARTICLES_LIST_URL = BASE_URL_SERVER + "/articles_api/getArticlesList";
    public static final String GET_ARTICLES_DETAILS_URL = BASE_URL_SERVER + "/articles_api/getDetails";
    public static final String GET_ARTICLES_DETAILS_IMAGE_ASSET_URL = BASE_URL_SERVER + "/uploads/articles";


    public static final String GET_ESSENTIALS_LIST_URL = BASE_URL_SERVER + "/essentials_api/getList";
    public static final String GET_ESSENTIALS_DETAILS_URL = BASE_URL_SERVER + "/essentials_api/getDetails";
    public static final String GET_TIPS_LIST_URL = BASE_URL_SERVER + "/tips_api/getList";
    public static final String GET_TIPS_DETAILS_URL = BASE_URL_SERVER + "/tips_api/getDetails";
    public static final String ADD_PLAN_URL = BASE_URL_SERVER + "/myplan_api/addplan";
    public static final String LIST_MYPLAN_URL = BASE_URL_SERVER + "/myplan_api/list";
    public static final String ADD_SUGGESTIONS_URL = BASE_URL_SERVER + "/suggestions_api/addSuggestion";
    public static final String ABOUT_US_URL = BASE_URL_SERVER + "/cms_api/getResults";
    public static final String ABOUT_US_IMAGES_ASSETS_URL = BASE_URL_SERVER + "/uploads/cms/aboutus_images/";
    public static final String ESSENTIAL_IMAGES_ASSETS_URL = BASE_URL_SERVER + "/uploads/essentials";

    public static final String CONTACT_US_URL = BASE_URL_SERVER + "/cms_api/getResults";


    public static final String ENQUIRY_URL = BASE_URL_SERVER + "/cms_api/enquiry";
    public static final String UPDATE_PLAN_URL = BASE_URL_SERVER + "/myplan_api/updateplan";
    public static final String DELETE_PLAN_URL = BASE_URL_SERVER + "/myplan_api/deleteplan";
    public static final String DONE_PLAN_URL = BASE_URL_SERVER + "/myplan_api/done";
    public static final String DAY_PLAN_URL = BASE_URL_SERVER + "/myplan_api/dayplan";


    public static final String TRAVEX_EVENTS_AND_MONTHLY_PLAN_URL = BASE_URL_SERVER + "/myplan_api/plans";
    public static final String TRAVEX_EVENTS_DETAILS_URL = BASE_URL_SERVER + "/myplan_api/eventdetails";
    public static final String WRITE_REVIEW_URL = BASE_URL_SERVER + "/member_api/review";
    public static final String PROFILE_IMAGE_UPDATE_URL = BASE_URL_SERVER + "/member_api/updateimage";
    public static final String MEMBER_DETAILS_URL = BASE_URL_SERVER + "/member_api/memberdetails";
    public static final String MEMBER_DETAILS_WITH_OPTIONS_URL = BASE_URL_SERVER + "/member_api/editprofile";


    public static final String GET_SEARCH_TERMINALS_URL = BASE_URL_SERVER + "/search_api/getSearchTerminals";
    public static final String GET_SEARCH_DESTINATIONS_URL = BASE_URL_SERVER + "/search_api/getSearchDestinations";
    //public static final String PROFILE_IMAGE_UPDATE_URL = BASE_URL_SERVER + "/member_api/updateimage";
    public static final String PROFILE_IMAGE_ASSET_URL = BASE_URL_SERVER + "/uploads/user/";


    public static final String FORGET_PASSWORD_URL=BASE_URL_SERVER+"/authenticate_api/forgot_password";

    /**
     * file names for saving json data from server
     */
    public static final String FILE_NAME_SPLASH_IMAGE_ASSETS = "splash_image_assets.txt";
    public static final String FILE_NAME_GET_CITY_DETAILS = "get_city_details.txt";
    public static final String FILE_NAME_FEATURE_ASSETS = "feature_assets.txt";
    public static final String FILE_NAME_REGISTER = "register.txt";
    public static final String FILE_VERIFY_LOGIN="verify_login.txt";



}
