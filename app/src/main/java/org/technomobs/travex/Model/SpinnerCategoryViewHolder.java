package org.technomobs.travex.Model;

import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by technomobs on 8/3/16.
 */
public class SpinnerCategoryViewHolder {

    ImageView mImageView=null;
  static   TextView mTextView=null;

    public ImageView getmImageView() {
        return mImageView;
    }

    public void setmImageView(ImageView mImageView) {
        this.mImageView = mImageView;
    }

    public TextView getmTextView() {
        return mTextView;
    }

    public void setmTextView(TextView mTextView) {
        this.mTextView = mTextView;
    }
}
