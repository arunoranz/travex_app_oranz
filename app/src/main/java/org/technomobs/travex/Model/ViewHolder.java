package org.technomobs.travex.Model;

import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

/**
 * Created by technomobs on 4/3/16.
 */
public class ViewHolder {

    SimpleDraweeView mSimpleDraweeView_icon = null;
    TextView mTextView_title = null;

    public SimpleDraweeView getmSimpleDraweeView_icon() {
        return mSimpleDraweeView_icon;
    }

    public void setmSimpleDraweeView_icon(SimpleDraweeView mSimpleDraweeView_icon) {
        this.mSimpleDraweeView_icon = mSimpleDraweeView_icon;
    }

    public TextView getmTextView_title() {
        return mTextView_title;
    }

    public void setmTextView_title(TextView mTextView_title) {
        this.mTextView_title = mTextView_title;
    }


}
