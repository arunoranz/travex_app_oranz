package org.technomobs.travex.Model;

import android.widget.ImageView;

import com.facebook.drawee.view.SimpleDraweeView;

/**
 * Created by technomobs on 4/3/16.
 */
public class NavigationDrawerListItem {

    int icon = 0;
    String title = null;
    boolean isIconVisible = false;

    public NavigationDrawerListItem(String title) {
        this.title = title;
    }

    public NavigationDrawerListItem( int icon,String title, boolean isIconVisible) {
        this.title = title;
        this.isIconVisible = isIconVisible;
        this.icon = icon;
    }


    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public boolean isIconVisible() {
        return isIconVisible;
    }

    public void setIsIconVisible(boolean isIconVisible) {
        this.isIconVisible = isIconVisible;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
