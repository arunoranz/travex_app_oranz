package org.technomobs.travex.Model;

/**
 * Created by technomobs on 8/3/16.
 */
public class CategorySpinnerItem {

    int icon = 0;
    String text = null;

    public CategorySpinnerItem(int icon, String text) {
        this.icon = icon;
        this.text = text;
    }

    public int getIcon() {
        return icon;
    }

    public String getText() {
        return text;
    }
}
