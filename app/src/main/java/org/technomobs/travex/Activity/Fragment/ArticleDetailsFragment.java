package org.technomobs.travex.Activity.Fragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.q42.android.scrollingimageview.ScrollingImageView;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Controller.Interface.GetArticleDetailsResponse;
import org.technomobs.travex.Controller.JsonObjectMaker;
import org.technomobs.travex.Controller.NetworkManager;
import org.technomobs.travex.Controller.NetworkOptions;
import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.DateFormatFunction;
import org.technomobs.travex.Utillity.ListCreatorForNameValuePairRequest;
import org.technomobs.travex.Utillity.LogUtility;

import java.util.ArrayList;
import java.util.Map;

import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;
import jp.wasabeef.recyclerview.animators.adapters.AlphaInAnimationAdapter;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ArticleDetailsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ArticleDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ArticleDetailsFragment extends Fragment implements GetArticleDetailsResponse {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    DateFormatFunction df = new DateFormatFunction();

    //RecyclerView rv_images;
    View view;
    static Activity activity;
    static String article_id;
    private OnFragmentInteractionListener mListener;
    String country = null;
    String country_id = null;
    ViewPager vpPager;
    CirclePageIndicator indicator;
    ArticleDetailsImageAdapter ArticleDetailsImageAdapter;
    //GridView gridview;

    //API Credentials
    static NetworkManager mNetworkManager = null;
    public static final String TAG = TravelEssential.class.getSimpleName();
    JsonObjectMaker mJsonObjectMaker = null;
    ListCreatorForNameValuePairRequest mListCreatorForNameValuePairRequest = null;
    PreferenceManager mPreferenceManager = null;

    TextView review_username,tv_title,bt_submit,tv_username,tv_date,tv_article_description,tv_noreviews,tv_rating,tv_video_count,tv_image_count,add_review;
    LinearLayout lin_lay_review_and_rating,featured_articledetails_layout,featured_layout,related_layout,lin_lay_related_articles
            ,tips_layout,lin_lay_tips,review_enter_layout;
    SimpleDraweeView img_category;
    SimpleDraweeView review_Image;
    RatingBar ratingbar_main,ratingBarreview;
    EditText et_review;
    ImageView bt_close;
    
    String articles_id = "";
    String articles_city = "";
    String articles_category = "";
    String articles_title = "";
    String articles_date = "";
    String image = "";
    String articles_content  = "";
    String category_key  = "";
    String articles_description = "";
    String users_first_name="";
    String users_last_name="";
    String rating="";
    String user_full_name="";
    String NoOfImages="";
    String NoOfVideos="";

    
    ArrayList<String> related_articles_id = new ArrayList<>();
    ArrayList<String> related_articles_title = new ArrayList<>();
    ArrayList<String> images = new ArrayList<>();
    ArrayList<String> videos = new ArrayList<>();
    ArrayList<String> review_membernameList = new ArrayList<String>();
    ArrayList<String> review_cityList = new ArrayList<String>();
    ArrayList<String> review_iconList = new ArrayList<String>();
    ArrayList<String> review_dateList = new ArrayList<String>();
    ArrayList<String> review_itemnameList = new ArrayList<String>();
    ArrayList<String> review_memberimageList = new ArrayList<String>();
    ArrayList<String> review_useridList = new ArrayList<String>();
    ArrayList<String> review_ratingList = new ArrayList<String>();
    ArrayList<String> review_commentList = new ArrayList<String>();
    ArrayList<String> rating_count = new ArrayList<String>();
    ArrayList<String> feature_article_list = new ArrayList<String>();
    ArrayList<String> tips_head = new ArrayList<>();
    ArrayList<ArrayList> tips_List = new ArrayList<>();

    
//    ArrayList<String> tags_title = new ArrayList<>();
//    ArrayList<String> tags_image = new ArrayList<>();


    public ArticleDetailsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
//     * @param param1 Parameter 1.
//     * @param param2 Parameter 2.
     * @return A new instance of fragment ArticleDetailsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ArticleDetailsFragment newInstance(Activity act,String art_id){
        ArticleDetailsFragment fragment = new ArticleDetailsFragment();
        activity = act;
        article_id=art_id;
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.fragment_article_details, container, false);
        initiating_APICredentials();
        initialization();
        ChangeToolBarDesign();
        GettingPreferenceValues();
        SetValues();
        CallingAPI();

        return view;
    }

    public void SetValues()
    {
        String member_profile_image = mPreferenceManager.getmember_profile_image();
        if(member_profile_image == null) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.user).build();
            review_Image.setImageURI(imageRequest.getSourceUri());
        }
        else if(member_profile_image.equals("")) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.user).build();
            review_Image.setImageURI(imageRequest.getSourceUri());
        }
        else
        {
            Uri img_uri=Uri.parse(member_profile_image);
            ImageRequest request = ImageRequest.fromUri(img_uri);

            DraweeController controller = Fresco.newDraweeControllerBuilder()
                    .setImageRequest(request)
                    .setOldController(review_Image.getController()).build();
            //Log.e(TAG, "ImagePath uri " + img_uri);

            review_Image.setController(controller);
        }
        review_username.setText(mPreferenceManager.getRegisteredUser_name());
    }

    public void initiating_APICredentials()
    {
        mJsonObjectMaker = new JsonObjectMaker(getActivity());
        mListCreatorForNameValuePairRequest = new ListCreatorForNameValuePairRequest(getActivity());
        mNetworkManager = NetworkManager.getSingleInstance(activity);
        mNetworkManager.setOnGetArticlesDetailsResponseListener(this);
        mPreferenceManager = new PreferenceManager(getActivity());
    }

    public void initialization()
    {
        tv_title = (TextView)view.findViewById(R.id.tv_title);
        tv_username = (TextView)view.findViewById(R.id.tv_username);
        review_username=(TextView) view.findViewById(R.id.review_username);
//        tv_review_username = (TextView)view.findViewById(R.id.tv_review_username);
        tv_date = (TextView)view.findViewById(R.id.tv_date);
//        tv_review_date = (TextView)view.findViewById(R.id.tv_review_date);
        tv_article_description = (TextView)view.findViewById(R.id.tv_article_description);
        lin_lay_review_and_rating=(LinearLayout)view.findViewById(R.id.lin_lay_review_and_rating);
        tv_noreviews= (TextView)view.findViewById(R.id.tv_noreviews);
        tv_rating= (TextView)view.findViewById(R.id.tv_rating);
        tv_video_count= (TextView)view.findViewById(R.id.tv_video_count);
        tv_image_count= (TextView)view.findViewById(R.id.tv_image_count);
        featured_articledetails_layout=(LinearLayout)view.findViewById(R.id.featureddetails_layout);
        featured_layout=(LinearLayout)view.findViewById(R.id.featured_layout);
        related_layout=(LinearLayout)view.findViewById(R.id.related_layout);
        lin_lay_related_articles=(LinearLayout)view.findViewById(R.id.lin_lay_related_articles);
        tips_layout=(LinearLayout)view.findViewById(R.id.tips_layout);
        lin_lay_tips=(LinearLayout)view.findViewById(R.id.lin_lay_tips);
        img_category=(SimpleDraweeView)view.findViewById(R.id.img_category);
        review_Image=(SimpleDraweeView)view.findViewById(R.id.review_Image);
        ratingbar_main=(RatingBar)view.findViewById(R.id.ratingbar_main);
        ratingBarreview=(RatingBar)view.findViewById(R.id.ratingBarreview);

        bt_submit=(TextView)view.findViewById(R.id.bt_submit);
        add_review=(TextView)view.findViewById(R.id.add_review);
        et_review=(EditText)view.findViewById(R.id.et_review);
        bt_close=(ImageView)view.findViewById(R.id.bt_close);
        review_enter_layout=(LinearLayout)view.findViewById(R.id.review_enter_layout);


        add_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //  displayAlertDialog();
                if(mPreferenceManager.getboolean("logout")==null) {
                    Toast.makeText(activity, "You must login to write a review !", Toast.LENGTH_LONG).show();
//                    Intent logouintent = new Intent(activity,SignUpLoginMasterActivity.class);
//                    startActivity(logouintent);
                }
                else if(mPreferenceManager.getboolean("logout")==false){
                    Toast.makeText(activity,"You must login to write a review !",Toast.LENGTH_LONG).show();
//                    Intent logouintent = new Intent(activity,SignUpLoginMasterActivity.class);
//                    startActivity(logouintent);
                }else {
                    review_enter_layout.setVisibility(View.VISIBLE);
                    et_review.setFocusable(true);
                }
            }
        });

        bt_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                review_enter_layout.setVisibility(View.GONE);
                et_review.setText("");
//                et_review.setFocusable(true);
            }
        });

        bt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ratingBarreview.getRating()<=0) {
                    Toast.makeText(activity,"Please rate...",Toast.LENGTH_SHORT).show();
                }
                else if(et_review.getText().toString().equals("")) {
                    Toast.makeText(activity,"Write a review",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    review_enter_layout.setVisibility(View.GONE);
                    et_review.setText("");
//                et_review.setFocusable(true);
                    postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.WRITE_REVIEW_URL,
                            getjsonObject(TravexApplication.WRITE_REVIEW_TAG),
                            TravexApplication.REQUEST_ID_WRITE_REVIEW);
                }

            }
        });

//        rv_images=(RecyclerView)view.findViewById(R.id.rv_images);
//        rv_images.setItemAnimator(new SlideInLeftAnimator());
//        rv_images.getItemAnimator().setMoveDuration(1000);
//        LinearLayoutManager lin_lay_rv_images = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
//        rv_images.setLayoutManager(lin_lay_rv_images);

        vpPager = (ViewPager) view.findViewById(R.id.pager);
        //gridview = (GridView) view.findViewById(R.id.gridview);

    }


    public void CallingAPI()
    {
        //        //this is for articles details api
        postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_ARTICLES_DETAILS_URL,
                getjsonObject(TravexApplication.GET_ARTICLES_DETAILS_TAG), TravexApplication.REQUEST_ID_ARTICLE_DETAILS);
    }

    public void ChangeToolBarDesign()
    {
        //To change the current design of the Toolbar in this fragment
        try {
            Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.tool_bar);
            TextView textToolHeader = (TextView) toolbar.findViewById(R.id.tv_category);
//            textToolHeader.setText(MapShowingFragment.selected_category);
//            textToolHeader.setVisibility(View.VISIBLE);
            SimpleDraweeView image = (SimpleDraweeView) toolbar.findViewById(R.id.networkImageViewEqWNH);
            image.setVisibility(View.GONE);

            toolbar.setNavigationIcon(R.drawable.ic_back_small);
        }
        catch(Exception ex)
        {

        }
    }

    public void GettingPreferenceValues()
    {
        LogUtility.Log(TAG, "onEnterKeyClick", null);
        country = mPreferenceManager.getSelectedCountryName();
        //country = ((ProfileActivity) getActivity()).getSelectedCountry();
        LogUtility.Log(TAG, "onEnterKeyClick:", "selected country from actionbar:" + country);
        //country_id = ((ProfileActivity) getActivity()).fetchCountryIdForCountry(country);
        country_id = mPreferenceManager.getSelectedcountryId();
        LogUtility.Log(TAG, "onEnterKeyClick:", "country id:" + country_id);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public JSONObject getjsonObject(String tag) {
        if (tag.equals(TravexApplication.GET_ARTICLES_DETAILS_TAG)) {
            return getJson(mListCreatorForNameValuePairRequest.getListOfArticlesDetails(
                            TravexApplication.GET_ARTICLES_ID
                    ),
                    mListCreatorForNameValuePairRequest.getListOfArticlesDetails(article_id));

        }
//        else if (tag.equals(TravexApplication.WRITE_REVIEW_TAG)) {
//            return getJson(
//                    mListCreatorForNameValuePairRequest.getListOfWriteReview
//                            (TravexApplication.WRITE_REVIEW_USER_ID,
//                                    TravexApplication.WRITE_REVIEW_CATEGORY,
//                                    TravexApplication.WRITE_REVIEW_ITEM_ID,
//                                    TravexApplication.WRITE_REVIEW_ITEM_NAME,
//                                    TravexApplication.WRITE_REVIEW_COMMENT,
//                                    TravexApplication.WRITE_REVIEW_RATING
//
//                            ),
//                    mListCreatorForNameValuePairRequest.getListOfWriteReview(mPreferenceManager.getRegisration_Id_response(), MapShowingFragment.selected_categoryId, id, name,et_review.getText().toString(), String.valueOf(ratingBarreview.getRating()))
//
//            );
//
//        }
        return null;
    }


    public void postJsonRequest(int request_type, String url, JSONObject jsonObject, final int request_id) {

        mNetworkManager.postJsonRequest(request_type, url, jsonObject, request_id);
    }

    public void postJsonRequest(int request_type, final Map<String, String> request_header, String url, JSONObject jsonObject, final int request_id) {

        mNetworkManager.postJsonRequest(request_type, request_header, url, jsonObject, request_id);


    }

    public JSONObject getJson(ArrayList<String> namePair, ArrayList<String> valuePair) {

        JSONObject hdhhd = mJsonObjectMaker.getJson(namePair, valuePair);
        return mJsonObjectMaker.getJson(namePair, valuePair);
    }
    
    public void SetCategoryImages()
    {
        if(category_key.equalsIgnoreCase("hotel"))
        {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_stay_hotel).build();
            img_category.setImageURI(imageRequest.getSourceUri());
        }
        else if(category_key.equalsIgnoreCase("spa"))
        {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_spa).build();
            img_category.setImageURI(imageRequest.getSourceUri());
        }
        else if(category_key.equalsIgnoreCase("todo"))
        {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_thingstodo).build();
            img_category.setImageURI(imageRequest.getSourceUri());
        }
        else if(category_key.equalsIgnoreCase("shopping"))
        {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_shopping).build();
            img_category.setImageURI(imageRequest.getSourceUri());
        }
        else if(category_key.equalsIgnoreCase("nightlife"))
        {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_nightlife).build();
            img_category.setImageURI(imageRequest.getSourceUri());
        }
        else if(category_key.equalsIgnoreCase("eatingout"))
        {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_eatingout).build();
            img_category.setImageURI(imageRequest.getSourceUri());
        }
        else if(category_key.equalsIgnoreCase("realestate"))
        {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_realestate).build();
            img_category.setImageURI(imageRequest.getSourceUri());
        }
        else if(category_key.equalsIgnoreCase("business"))
        {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_bussiness).build();
            img_category.setImageURI(imageRequest.getSourceUri());
        }
    }

    public void SetValuesandAdapter()
    {

        //Setting Values Of Static Elements

        SetCategoryImages();

        tv_title.setText(articles_title);
        tv_username.setText(user_full_name);
//        tv_review_username.setText("By Thomson Bolgatti");
        //String articles_date_str = df.dateformat("yyyy-MM-dd","dd - MMM - yyyy",articles_date);//12 - May - 2015 format
        tv_date.setText(articles_date);
        tv_image_count.setText(NoOfImages);
        tv_video_count.setText(NoOfVideos);

        if(rating==null)rating="0";
        if(rating.equals(""))rating="0";
        ratingbar_main.setRating(Float.parseFloat(rating));
        tv_rating.setText(rating);
//        tv_review_date.setText(articles_date_str);
        tv_article_description.setText(Html.fromHtml(articles_description));
        //Setting Tags in GridView
        //gridview.setAdapter(new ArticleDetailsTagAdapter(getActivity(), tags_title, tags_image));

        //Setting Pager Image

        indicator = (CirclePageIndicator)view.findViewById(R.id.indicator);
        ArticleDetailsTitleImageAdapter adapter = new ArticleDetailsTitleImageAdapter(getActivity(),images);
        vpPager.setAdapter(adapter);
        vpPager.setPageMargin(20);
        vpPager.setCurrentItem(0);

        //Setting indicator in Pager
        indicator.setPageColor(getResources().getColor(android.R.color.white));
        indicator.setViewPager(vpPager);


        //Adding reviews
        if(review_itemnameList.size()!=0) {
            RatingBar ratingBar;
            TextView tv_desp,subtitle,title;
            SimpleDraweeView icon;

            for(int i=0;i<review_itemnameList.size();i++)
            {
                View rating_view = activity.getLayoutInflater().inflate(R.layout.rating_and_review_adapter, null);

                icon= (SimpleDraweeView)rating_view.findViewById(R.id.icon2);

                title= (TextView)rating_view.findViewById(R.id.title);
                subtitle= (TextView)rating_view.findViewById(R.id.subtitle);
                ratingBar= (RatingBar)rating_view.findViewById(R.id.ratingBar);

                tv_desp= (TextView)rating_view.findViewById(R.id.tv_desp);


                title.setText(review_membernameList.get(i));
                String date=review_dateList.get(i);
                date = df.dateformat("yyyy-MM-dd HH:mm:ss","yyyy-MM-dd",date);
                subtitle.setText(date);
                ratingBar.setRating(Integer.parseInt(review_ratingList.get(i)));
                tv_desp.setText(review_commentList.get(i));
                if(review_memberimageList.size()>0) {
                    String image_name = review_memberimageList.get(i);
                    Uri img_uri;
                    String image_url = AppConstants.PROFILE_IMAGE_ASSET_URL + image_name;
                    img_uri = Uri.parse(image_url);
                    ImageRequest request = ImageRequest.fromUri(img_uri);
                    DraweeController controller = Fresco.newDraweeControllerBuilder()
                            .setImageRequest(request)
                            .setOldController(icon.getController()).build();
                    icon.setController(controller);
                }
                else
                {
                    ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.user).build();
                    icon.setImageURI(imageRequest.getSourceUri());
                }


                lin_lay_review_and_rating.addView(rating_view);
            }


        }
        else
        {
            tv_noreviews.setVisibility(View.VISIBLE);
        }


        //Adding Featured Articles
        if (feature_article_list.size() != 0) {
            featured_layout.setVisibility(View.VISIBLE);
            for (int i = 0; i < feature_article_list.size(); i++) {
                LinearLayout hor_event_layout = new LinearLayout(activity);
                hor_event_layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                hor_event_layout.setOrientation(LinearLayout.HORIZONTAL);
                hor_event_layout.setGravity(Gravity.CENTER_VERTICAL);
                hor_event_layout.setPadding(10, 10, 10, 10);
                ImageView im = new ImageView(activity);
                im.setLayoutParams(new LinearLayout.LayoutParams(20, 20));
                im.setImageResource(R.drawable.button_action_dark);
                TextView event_text = new TextView(activity);
                event_text.setText(feature_article_list.get(i));
                event_text.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                event_text.setTextColor(Color.BLACK);
                event_text.setPadding(10, 0, 0, 0);
                hor_event_layout.addView(im);
                hor_event_layout.addView(event_text);
                featured_articledetails_layout.addView(hor_event_layout);
            }
        }

        //Adding Related Articles
        if (related_articles_id.size() != 0) {
            related_layout.setVisibility(View.VISIBLE);
            for (int i = 0; i < related_articles_id.size(); i++) {
                LinearLayout hor_event_layout = new LinearLayout(activity);
                hor_event_layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                hor_event_layout.setOrientation(LinearLayout.HORIZONTAL);
                hor_event_layout.setGravity(Gravity.CENTER_VERTICAL);
                hor_event_layout.setPadding(10, 10, 10, 10);
                ImageView im = new ImageView(activity);
                im.setLayoutParams(new LinearLayout.LayoutParams(20, 20));
                im.setImageResource(R.drawable.button_action_dark);
                TextView event_text = new TextView(activity);
                event_text.setText(related_articles_title.get(i));
                event_text.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                event_text.setTextColor(Color.BLACK);
                event_text.setPadding(10, 0, 0, 0);
                hor_event_layout.addView(im);
                hor_event_layout.addView(event_text);
                lin_lay_related_articles.addView(hor_event_layout);
            }
        }

        //Adding Tips
        if (tips_head.size() != 0) {
            tips_layout.setVisibility(View.VISIBLE);
            LinearLayout main_tips_layout = new LinearLayout(activity);


//            list_tips_layout.setOrientation(LinearLayout.VERTICAL);
//            list_tips_layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
//            list_tips_layout.setGravity(Gravity.CENTER_VERTICAL);

            main_tips_layout.setOrientation(LinearLayout.VERTICAL);
            main_tips_layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            main_tips_layout.setGravity(Gravity.CENTER_VERTICAL);

            for (int i = 0; i < tips_head.size(); i++) {
                LinearLayout list_tips_layout = new LinearLayout(activity);
                list_tips_layout = new LinearLayout(activity);
                list_tips_layout.setOrientation(LinearLayout.VERTICAL);
                list_tips_layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                list_tips_layout.setGravity(Gravity.CENTER_VERTICAL);
                LinearLayout hor_event_layout = new LinearLayout(activity);
                TextView txthead= new TextView(activity);
                txthead.setTypeface(null, Typeface.BOLD);
                txthead.setText(tips_head.get(i));
                list_tips_layout.addView(txthead);

                ArrayList<String> tips_list_string_array=new ArrayList<>();
                tips_list_string_array=tips_List.get(i);
                //
                for(int tips=0;tips<tips_list_string_array.size();tips++)
                {
                    hor_event_layout = new LinearLayout(activity);
                    hor_event_layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    hor_event_layout.setOrientation(LinearLayout.HORIZONTAL);
                    hor_event_layout.setGravity(Gravity.CENTER_VERTICAL);
                    hor_event_layout.setPadding(10, 10, 10, 10);
                    ImageView im = new ImageView(activity);
                    im.setLayoutParams(new LinearLayout.LayoutParams(20, 20));
                    im.setImageResource(R.drawable.button_action_dark);
                    TextView event_text = new TextView(activity);
                    event_text.setText(tips_list_string_array.get(tips));
                    event_text.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    event_text.setTextColor(Color.BLACK);
                    event_text.setPadding(10, 0, 0, 0);
                    hor_event_layout.addView(im);
                    hor_event_layout.addView(event_text);
                    list_tips_layout.addView(hor_event_layout);
                }
                main_tips_layout.addView(list_tips_layout);
            }
            tips_layout.addView(main_tips_layout);
        }

    }

    @Override
    public void onGetArticlesDetailsResponse(boolean status, JSONObject jsonObject) {
        articles_id = "";
        articles_city = "";
        articles_category = "";
        articles_title = "";
        articles_date = "";
        image = "";
        articles_content  = "";
        category_key="";
        articles_description = "";
        users_first_name="";
        users_last_name="";
        rating="0";

        related_articles_id = new ArrayList<>();
        related_articles_title = new ArrayList<>();
        review_membernameList = new ArrayList<String>();
        review_cityList = new ArrayList<String>();
        review_iconList = new ArrayList<String>();
        review_dateList = new ArrayList<String>();
        review_itemnameList = new ArrayList<String>();
        review_memberimageList = new ArrayList<String>();
        review_useridList = new ArrayList<String>();
        review_ratingList = new ArrayList<String>();
        review_commentList = new ArrayList<String>();
        rating_count = new ArrayList<String>();
        images = new ArrayList<>();
        videos = new ArrayList<>();
        tips_head= new ArrayList<>();
        tips_List= new ArrayList<>();
//        tags_title = new ArrayList<>();
//        tags_image = new ArrayList<>();




        if (status) {

            try {
                JSONObject result_obj = jsonObject.getJSONObject("result");
                JSONObject detail_obj = result_obj.getJSONObject("details");
                articles_id = detail_obj.getString("articles_id");
                articles_city = detail_obj.getString("articles_city");
                articles_category = detail_obj.getString("articles_category");
                articles_title = detail_obj.getString("articles_title");
                articles_date = detail_obj.getString("articles_date");
                articles_content = detail_obj.getString("articles_content");
                articles_description = detail_obj.getString("articles_description");
                users_first_name= detail_obj.getString("users_first_name");
                users_last_name= detail_obj.getString("users_last_name");
                category_key= detail_obj.getString("category_key");
                rating= detail_obj.getString("rating");
                user_full_name="By "+users_first_name+" "+users_last_name;


                JSONArray img_array = detail_obj.getJSONArray("images");
                if(img_array!=null)
                {
                    for(int i=0;i<img_array.length();i++)
                    {
//                        JSONObject imgobj = img_array.getJSONObject(i);
                        String image_url = img_array.getString(i);

                        if(!image_url.equals(""))
                        {
                            image_url=AppConstants.GET_ARTICLES_DETAILS_IMAGE_ASSET_URL+"/"+image_url;
                            images.add(image_url);
                        }
                    }
                }

                JSONArray vid_array = detail_obj.getJSONArray("videos");
                if(vid_array!=null)
                {
                    for(int i=0;i<vid_array.length();i++)
                    {
//                        JSONObject imgobj = img_array.getJSONObject(i);
                        String video_url = vid_array.getString(i);

                        if(!video_url.equals(""))
                        {
                            video_url=AppConstants.GET_ARTICLES_DETAILS_IMAGE_ASSET_URL+"/"+video_url;
                            videos.add(video_url);
                        }
                    }
                }

                NoOfImages = String.valueOf(images.size());
                NoOfVideos = String.valueOf(videos.size());


                //Featured Articles
                try {
                    JSONArray article_array = detail_obj.getJSONArray("article_tag_item");

                    //  image_size=String.valueOf(menu_img_array.length());
                    if (article_array.length() != 0) {

                        for (int ph = 0; ph < article_array.length(); ph++) {
                            JSONObject jsonBranch = article_array.getJSONObject(ph);
                            feature_article_list.add(jsonBranch.getString("name"));

                        }

                    }
                } catch (Exception e) {
                    feature_article_list = new ArrayList<String>();
                    e.printStackTrace();
                }

                //Tips
                try {
                    JSONArray tips_array = detail_obj.getJSONArray("tips");

                    //  image_size=String.valueOf(menu_img_array.length());
                    if (tips_array.length() != 0) {

                        for (int ph = 0; ph < tips_array.length(); ph++) {
                            JSONObject jsonBranch = tips_array.getJSONObject(ph);
                            tips_head.add(jsonBranch.getString("tips_title"));

                            JSONArray tips_list_array = jsonBranch.getJSONArray("tips");
                            ArrayList<String> tips_list_string_array = new ArrayList<>();
                            for(int tp=0;tp<tips_list_array.length();tp++)
                            {
                                JSONObject jsontipslist = tips_list_array.getJSONObject(tp);
                                tips_list_string_array.add(jsontipslist.getString("tips"));
                            }
                            tips_List.add(tips_list_string_array);
                        }

                    }
                } catch (Exception e) {
                    tips_head = new ArrayList<String>();
                    e.printStackTrace();
                }

            try {
                JSONArray related_articles_array = detail_obj.getJSONArray("related_articles");
                for(int i=0;i<related_articles_array.length();i++)
                {
                    JSONObject related_articles_object = related_articles_array.getJSONObject(i);
                    related_articles_id.add(related_articles_object.getString("articles_id"));
                    related_articles_title.add(related_articles_object.getString("articles_title"));
                }
            }
            catch (Exception e) {
                related_articles_id = new ArrayList<String>();
                related_articles_title = new ArrayList<String>();
                e.printStackTrace();
            }



                try {
                    JSONArray reviews_array = detail_obj.getJSONArray("reviews");

                    //     video_size=String.valueOf(speciality_array.length());
                    if (reviews_array.length() == 0) {

                    } else {
                        for (int ph = 0; ph < reviews_array.length(); ph++) {
                            JSONObject json=reviews_array.getJSONObject(ph);
                            String item_city = json.getString("member_city").toString();
                            String item_icon = json.getString("category_name").toString();
                            String item_date = json.getString("review_created_at").toString();
                            String item_name = json.getString("item_name").toString();
                            String item_membername = json.getString("member_first_name").toString();
                            String item_memberimage = json.getString("member_profile_image").toString();
                            String item_userid = json.getString("user_id").toString();
                            String item_rating = json.getString("rating").toString();
                            String item_comment = json.getString("comment").toString();

                            review_cityList.add(item_city);
                            review_iconList.add(item_icon);
                            review_dateList.add(item_date);
                            review_itemnameList.add(item_name);
                            review_membernameList.add(item_membername);
                            review_memberimageList.add(item_memberimage);
                            review_useridList.add(item_userid);
                            review_ratingList.add(item_rating);
                            review_commentList.add(item_comment);
                            rating_count.add(String.valueOf(review_ratingList.size()));


                        }

                    }
                } catch (Exception e) {
                    review_itemnameList=new ArrayList<>();
                    e.printStackTrace();
                }
                




            } catch (JSONException e) {
                e.printStackTrace();
            }
            LogUtility.Log(TAG, "onGetArticlesDetailsResponse: :success", "json data:" + jsonObject.toString());


        } else {
            LogUtility.Log(TAG, "onGetArticlesDetailsResponse: :failure", null);

        }

        SetValuesandAdapter();
    }



}
