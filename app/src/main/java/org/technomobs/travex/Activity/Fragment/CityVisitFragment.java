package org.technomobs.travex.Activity.Fragment;

import android.content.Context;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import org.technomobs.travex.Activity.MainActivity;
import org.technomobs.travex.Activity.SignUpLoginMasterActivity;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.Common;
import org.technomobs.travex.Utillity.LogUtility;

import java.util.ArrayList;

/**
 * Created by technomobs on 22/2/16.
 */
public class CityVisitFragment extends Fragment implements AdapterView.OnItemClickListener {
    public static final String TAG = CityVisitFragment.class.getSimpleName();
    ArrayList<String> mArrayListForCityNames = null;
    ArrayList<String> mArrayListForCityId = null;

    PreferenceManager mPreferenceManager = null;
    ArrayAdapter<String> mArrayAdapter = null;
    String item_countryname = null;
    String item_countryid=null;
    ImageView img_clear;
    TextView tv_username;
    View rootView;
    SimpleDraweeView image_user;
    /**
     * This Button for submitting the city which user wanted to visit on to current city location in preference
     */
    Button mButton_Submit = null;

    /**
     * This view for entering cityname on which user needed to visit
     */
    AutoCompleteTextView mSpinner_cityname = null;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onActivityCreated", null);


        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        LogUtility.Log(TAG, "onAttach", null);
        mPreferenceManager = new PreferenceManager(getActivity());
        mArrayListForCityNames = new ArrayList<String>();
        mArrayListForCityNames = mPreferenceManager.getSupportedCountryNames();
        mArrayListForCityId = mPreferenceManager.getSupportedCountryId();
        mArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.autocomplete_item, mArrayListForCityNames);

        super.onAttach(context);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        LogUtility.Log(TAG, "onConfigurationChanged", null);

        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onCreate", null);

        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        LogUtility.Log(TAG, "onDestroy", null);

        super.onDestroy();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onCreateView", null);
        rootView = inflater.inflate(R.layout.send_city_visit_fragment_layout, container, false);
        image_user = (SimpleDraweeView) rootView.findViewById(R.id.image_user);


        String member_profile_image = mPreferenceManager.getmember_profile_image();
        if(member_profile_image == null) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.user).build();
            image_user.setImageURI(imageRequest.getSourceUri());
        }
        else if(member_profile_image.equals("")) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.user).build();
            image_user.setImageURI(imageRequest.getSourceUri());
        }
        else
        {
            Uri img_uri=Uri.parse(member_profile_image);
            ImageRequest request = ImageRequest.fromUri(img_uri);

            DraweeController controller = Fresco.newDraweeControllerBuilder()
                    .setImageRequest(request)
                    .setOldController(image_user.getController()).build();
            //Log.e(TAG, "ImagePath uri " + img_uri);

            image_user.setController(controller);
        }


        tv_username = (TextView) rootView.findViewById(R.id.tv_username);
        try {
            if (!mPreferenceManager.getRegisteredUser_name().equals("")) {
                tv_username.setText(mPreferenceManager.getRegisteredUser_name());
            } else {
                tv_username.setText("");
            }
        }   catch(Exception e){
            e.printStackTrace();
            tv_username.setText("");
        }
        Common.setupUI(rootView.findViewById(R.id.rel_main),getActivity());
        img_clear=(ImageView) rootView.findViewById(R.id.img_clear);
        mButton_Submit = (Button) rootView.findViewById(R.id.submit);
        mSpinner_cityname = (AutoCompleteTextView) rootView.findViewById(R.id.city_name);
        mSpinner_cityname.setOnItemClickListener(this);
        mSpinner_cityname.setAdapter(mArrayAdapter);

        img_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSpinner_cityname.setText("");
            }
        });

        mButton_Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (item_countryname != null) {
                    updateCurrentLocationDetailsInPreference(item_countryname);

                }
            }
        });
        return rootView;
    }

    /**
     * @param localityname
     */
    public void updateCurrentLocationDetailsInPreference(String localityname) {

        /**
         * findout the index of locality name from preference arraylist ,to locate   another  details of location
         */

        int index = -1;
        for (int count = 0; count < mArrayListForCityNames.size(); count++) {
            if (localityname.equals(mArrayListForCityNames.get(count))) {
                index = count;
            }

        }
        LogUtility.Log(TAG, "updateCurrentLocationDetailsInPreference", "index of current locality in preference list is:" + index + ":locality:" + localityname);
        if (index != -1) {
            mPreferenceManager.setCurrentLocalityName(localityname);
            mPreferenceManager.setCurrentcountryName(mPreferenceManager.getSupportedCountryNames().get(index).toString());
            mPreferenceManager.setCurrentLatitude(mPreferenceManager.getSupportedCountryLatitude().get(index).toString());
            mPreferenceManager.setCurrentLongitude(mPreferenceManager.getSupportedCountryLongitude().get(index).toString());
        }

        LogUtility.Log(TAG, "updateCurrentLocationDetailsInPreference:", "parent activity is:" + getActivity().getClass().getSimpleName().toString());
        if (getActivity().getClass().getSimpleName().toString().equals(MainActivity.class.getSimpleName())) {
            MainActivity mainActivity = ((MainActivity) getActivity());
            mainActivity.startSignUp_LoginActivity();
            getActivity().finish();
        } else if (getActivity().getClass().getSimpleName().toString().equals(SignUpLoginMasterActivity.class.getSimpleName())) {
            SignUpLoginMasterActivity signUpLoginMasterActivity = (SignUpLoginMasterActivity) getActivity();
            signUpLoginMasterActivity.startProfileActivity();
            getActivity().finish();

        }


    }

    @Override
    public void onDestroyView() {
        LogUtility.Log(TAG, "onDestroyView", null);

        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        LogUtility.Log(TAG, "onDetach", null);

        super.onDetach();
    }

    @Override
    public void onPause() {
        LogUtility.Log(TAG, "onPause", null);

        super.onPause();
    }

    @Override
    public void onResume() {
        LogUtility.Log(TAG, "onResume", null);

        super.onResume();
    }

    @Override
    public void onStart() {
        LogUtility.Log(TAG, "onStart", null);

        super.onStart();
    }

    @Override
    public void onStop() {
        LogUtility.Log(TAG, "onStop", null);

        super.onStop();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onViewCreated", null);

        super.onViewCreated(view, savedInstanceState);
    }

//    @Override
//    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//
//
//
//    }
//
//    @Override
//    public void onNothingSelected(AdapterView<?> parent) {
//        item_countryname = null;
//    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        LogUtility.Log(TAG, "onItemSelected:", "item:" + parent.getItemAtPosition(position).toString());

        item_countryname = parent.getItemAtPosition(position).toString();
        position = mArrayListForCityNames.indexOf(item_countryname);
        item_countryid = mArrayListForCityId.get(position).toString();
        String current_city_latitude=mPreferenceManager.getSupportedCountryLatitude().get(position).toString();
        String current_city_longitude=mPreferenceManager.getSupportedCountryLongitude().get(position).toString();

        mPreferenceManager.setSelectedCountryName(item_countryname);
        mPreferenceManager.setSelectedcountryId(item_countryid);
        mPreferenceManager.setSelectedLatitude(current_city_latitude);
        mPreferenceManager.setSelectedLongitude(current_city_longitude);
        mPreferenceManager.setCurrentcityposition(String.valueOf(position));
        Common.setupUI(rootView.findViewById(R.id.rel_main), getActivity());

    }

}
