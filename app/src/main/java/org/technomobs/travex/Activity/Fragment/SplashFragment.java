package org.technomobs.travex.Activity.Fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;

import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import org.json.JSONObject;
import org.technomobs.travex.Activity.MainActivity;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.Controller.Interface.GetCityResponse;
import org.technomobs.travex.Controller.Interface.GetFeatureImagesResponse;
import org.technomobs.travex.Controller.Interface.GetSplashLandingImagesResponse;
import org.technomobs.travex.Controller.JsonObjectMaker;
import org.technomobs.travex.Controller.NetworkManager;
import org.technomobs.travex.Controller.NetworkOptions;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;
import org.technomobs.travex.Service.LocationService;
import org.technomobs.travex.Utillity.ListCreatorForNameValuePairRequest;
import org.technomobs.travex.Utillity.LogUtility;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.codetail.animation.SupportAnimator;
import io.codetail.animation.ViewAnimationUtils;
import io.codetail.widget.RevealFrameLayout;

//import org.technomobs.travex.Model.AppConstants;

/**
 * Created by technomobs on 16/2/16.
 */
public class SplashFragment extends android.support.v4.app.Fragment implements GetCityResponse, GetSplashLandingImagesResponse, GetFeatureImagesResponse, Animation.AnimationListener {

    public static final String TAG = SplashFragment.class.getSimpleName();
    public static final int LOCATION_PERMISSION_GROUP = 100;
    public static final int SETTINGS_APP_BACK = 101;
    public static ActivityManager sActivityManager = null;
    public static final String SERVICE_NAME = "org.technomobs.travex.Service.LocationService";
    AlertDialog mAlertDialog = null;
    static NetworkManager mNetworkManager = null;
    JsonObjectMaker mJsonObjectMaker = null;
    private Animation mAnimationFadeOut, mAnimationZoom,mAnimationFadeInLogo,mAnimationFadeOutLogo,mSlideinLeft,mSlideinTop;
    PreferenceManager mPreferenceManager = null;
    ListCreatorForNameValuePairRequest mListCreatorForNameValuePairRequest = null;
    SimpleDraweeView mSplashBg, mSplashAnimation, mLogo,mLogoSub,mLogo_yellow,mLogo_orange,mLogo_red;
    RevealFrameLayout mRevealBg;
    int mCurrentImage = 1;
    TravexApplication mTravexApplication = null;
    ArrayList<String> Splash_landing_images_asset_url = new ArrayList<String>();
    int pos=0;

    @Override
    public void onAttach(Context context) {
        LogUtility.Log(TAG, "onAttach", null);
        sActivityManager = (ActivityManager) getContext().getSystemService(Context.ACTIVITY_SERVICE);
        mJsonObjectMaker = new JsonObjectMaker(getActivity());
        mNetworkManager = NetworkManager.getSingleInstance(getActivity());
        mPreferenceManager = new PreferenceManager(getActivity());
        mListCreatorForNameValuePairRequest = new ListCreatorForNameValuePairRequest(getActivity());
        mTravexApplication = TravexApplication.getInstance();


        mNetworkManager.setOnGetCityResponseListener(this);

        mNetworkManager.setOnSplashImagesResponseListener(this);
        mNetworkManager.setOnFeatureImagesResponseListener(this);
        if (Build.VERSION.SDK_INT > 22)

        {

            if (checkLocationPermissionRuntime()) {
                startLocationService();
            } else {
                setLocationPermission();
            }
        } else

            startLocationService();
        super.onAttach(context);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onViewCreated", "mNetworkManager:" + mNetworkManager);
        if (!mPreferenceManager.getSupportedCountryNames().isEmpty()) {
            LogUtility.Log(TAG, "onViewCreated:", "get city names in db:going to clear" + mPreferenceManager.getSupportedCountryNames());

            mPreferenceManager.getSupportedCountryNames().clear();
        }

        if (!mPreferenceManager.getSupportedCountryId().isEmpty()) {
            LogUtility.Log(TAG, "onViewCreated:", "get city id in db:going to clear" + mPreferenceManager.getSupportedCountryId());

            mPreferenceManager.getSupportedCountryId().clear();
        }
        if (!mPreferenceManager.getSupportedCountryLatitude().isEmpty()) {
            LogUtility.Log(TAG, "onViewCreated:", "get city latitude in db:going to clear" + mPreferenceManager.getSupportedCountryLatitude());

            mPreferenceManager.getSupportedCountryLatitude().clear();
        }
        if (!mPreferenceManager.getSupportedCountryLongitude().isEmpty()) {
            LogUtility.Log(TAG, "onViewCreated:", "get city longitude in db:going to clear" + mPreferenceManager.getSupportedCountryLongitude());

            mPreferenceManager.getSupportedCountryLongitude().clear();
        }

//        if (!mPreferenceManager.getSplash_landing_images_asset_url().isEmpty()) {
//            LogUtility.Log(TAG, "onViewCreated:", "get splash landing images url in db:going to clear" + mPreferenceManager.getSupportedCountryLongitude());
//
//            mPreferenceManager.getSplash_landing_images_asset_url().clear();
//        }







        postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.CITY_LIST_URL, null,
                TravexApplication.REQUEST_ID_GET_CITY);
        postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.SPLASH_FEATURE_IMAGES_URL,
                getjsonObject(TravexApplication.SPLASH_TAG), TravexApplication.REQUEST_ID_SPLASH);
        postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.SPLASH_FEATURE_IMAGES_URL,
                getjsonObject(TravexApplication.FEATURE_TAG), TravexApplication.REQUEST_ID_FEATURE);
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onActivityCreated", null);







     /*   postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.MEMBER_UPDATE_PROFILE_URL,
                getjsonObject(TravexApplication.MEMBER_UPDATE_PROFILE_TAG), TravexApplication.REQUEST_ID_MEMBER_UPDATE_PROFILE);
*/

        super.onActivityCreated(savedInstanceState);
    }


    public JSONObject getjsonObject(String tag) {
        if (tag.equals(TravexApplication.SPLASH_TAG)) {

            return getJson(mListCreatorForNameValuePairRequest.getListOfSplashLanding_ImagesRequest(TravexApplication.SPLASH_FEATURE_TYPE), mListCreatorForNameValuePairRequest.getListOfSplashLanding_ImagesRequest("splashscreen"));

        } else if (tag.equals(TravexApplication.FEATURE_TAG)) {


            return getJson(mListCreatorForNameValuePairRequest.getListOfFeatureImagesRequest(TravexApplication.SPLASH_FEATURE_TYPE), mListCreatorForNameValuePairRequest.getListOfFeatureImagesRequest("features"));


        } else if (tag.equals(TravexApplication.MEMBER_UPDATE_PROFILE_TAG)) {
            return getJson(mListCreatorForNameValuePairRequest.getListOfMemberUpdateProfile
                            (TravexApplication.MEMBER_ID_UPDATE_PROFILE, TravexApplication.MEMBER_GENDER_UPDATE_PROFILE,
                                    TravexApplication.MEMBER_MARTIAL_STATUS_UPDATE_PROFILE, TravexApplication.MEMBER_NO_KIDS_UPDATE_PROFILE,
                                    TravexApplication.MEMBER_COUNTRY_UPDATE_PROFILE, TravexApplication.MEMBER_CURRENCY_UPDATE_PROFILE,
                                    TravexApplication.MEMBER_INCOME_UPDATE_PROFILE, TravexApplication.MEMBER_ZIPCODE_UPDATE_PROFILE,
                                    TravexApplication.MEMBER_LANGAUGES_UPDATE_PROFILE, TravexApplication.MEMBER_FOODPREFERENCES_UPDATE_PROFILE,
                                    TravexApplication.MEMBER_PHOBIAS_UPDATE_PROFILE, TravexApplication.MEMBER_STAYPREFERENCES_UPDATE_PROFILE,
                                    TravexApplication.MEMBER_DISABILITIES_UPDATE_PROFILE, TravexApplication.MEMBER_UNWIND_UPDATE_PROFILE


                            ),
                    mListCreatorForNameValuePairRequest.getListOfMemberUpdateProfile(
                            mPreferenceManager.getRegisration_Id_response(), "male",
                            "single", "0",
                            "india", "rs",
                            "1000", "1234",
                            "eng", "veg",
                            "1234", "kerala",
                            "no", "11234"


                    ));

        }
        return null;

    }

    public JSONObject getJson(ArrayList<String> namePair, ArrayList<String> valuePair) {


        return mJsonObjectMaker.getJson(namePair, valuePair);
    }


    /**
     * @param request_type
     * @param url
     * @param jsonObject
     * @param request_id
     */
    public void postJsonRequest(int request_type, String url, JSONObject jsonObject, final int request_id) {

        mNetworkManager.postJsonRequest(request_type, url, jsonObject, request_id);

    }

    /**
     * @param request_type
     * @param request_header
     * @param url
     * @param jsonObject
     * @param request_id
     */
    public void postJsonRequest(int request_type, final Map<String, String> request_header, String url, JSONObject jsonObject, final int request_id) {

        mNetworkManager.postJsonRequest(request_type, request_header, url, jsonObject, request_id);


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        LogUtility.Log(TAG, "onActivityResult", null);
        if (requestCode == SETTINGS_APP_BACK) {

            LogUtility.Log(TAG, "onActivityResult", null);

            if (checkLocationPermissionRuntime()) {
                startLocationService();
            } else {
                // alertDialogueForPermissions();
            }

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        LogUtility.Log(TAG, "onConfigurationChanged", null);
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onDestroy() {
        LogUtility.Log(TAG, "onDestroy", null);
        super.onDestroy();
    }

    Button ok;
    EditText mEditText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this.getActivity()));
        View view = inflater.inflate(R.layout.splash_fragment_layout, container, false);
        mSplashBg = (SimpleDraweeView) view.findViewById(R.id.splash_bg);
        mRevealBg = (RevealFrameLayout) view.findViewById(R.id.reveal_bg);
        mSplashAnimation = (SimpleDraweeView) view.findViewById(R.id.splash_animation);
        mLogo = (SimpleDraweeView) view.findViewById(R.id.logo);
        mLogoSub = (SimpleDraweeView) view.findViewById(R.id.logo_sub);
        mLogo_yellow = (SimpleDraweeView) view.findViewById(R.id.logo_yellow);
        mLogo_orange = (SimpleDraweeView) view.findViewById(R.id.logo_orange);
        mLogo_red = (SimpleDraweeView) view.findViewById(R.id.logo_red);
        Splash_landing_images_asset_url = mPreferenceManager.getSplash_landing_images_asset_url();

        ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.mipmap.splash_one).build();
        mSplashBg.setImageURI(imageRequest.getSourceUri());
//        ImageRequest ir = ImageRequestBuilder.newBuilderWithResourceId(R.mipmap.splash_two).build();
//        mSplashAnimation.setImageURI(ir.getSourceUri());


        animationInit();

//        for(int i=0;i<Splash_landing_images_asset_url.size();i++) {
//        pos=i;
//            Runnable runnable1 = new Runnable() {
//                @Override
//                public void run() {
//
//                    //mCurrentImage = pos+1;
//
//                    Uri img_uri;
//                    //changeRevealBg(R.mipmap.splash_two);
//                    if(pos==0) {
//                        changeBg(Splash_landing_images_asset_url.get(pos));
//                    }
//                    else
//                    {
//                        changeBg(Splash_landing_images_asset_url.get(pos-1));
//                        img_uri=Uri.parse(Splash_landing_images_asset_url.get(pos));
//                        ImageRequest request = ImageRequest.fromUri(img_uri);
//
//                        DraweeController controller = Fresco.newDraweeControllerBuilder()
//                                .setImageRequest(request)
//                                .setOldController(mSplashAnimation.getController()).build();
//                        //Log.e(TAG, "ImagePath uri " + img_uri);
//
//                        mSplashAnimation.setController(controller);
//                        anim(mSplashAnimation);
//                    }
//
////                    ImageRequest ir = ImageRequestBuilder.newBuilderWithResourceId(R.mipmap.splash_three).build();
////                    mSplashAnimation.setImageURI(ir.getSourceUri());
//
//                }
//            };

//                class OneShotTask implements Runnable {
//                    int str;
//                    OneShotTask(int s) { str=s; }
//                    public void run() {
//
//
//                        //mCurrentImage = pos+1;
//
//                        Uri img_uri;
//                        //changeRevealBg(R.mipmap.splash_two);
//                        if(str==0) {
//                            changeBg(Splash_landing_images_asset_url.get(str));
//                        }
//                        else
//                        {
//                            changeBg(Splash_landing_images_asset_url.get(str-1));
//                            img_uri=Uri.parse(Splash_landing_images_asset_url.get(str));
//                            ImageRequest request = ImageRequest.fromUri(img_uri);
//
//                            DraweeController controller = Fresco.newDraweeControllerBuilder()
//                                    .setImageRequest(request)
//                                    .setOldController(mSplashAnimation.getController()).build();
//                            //Log.e(TAG, "ImagePath uri " + img_uri);
//
//                            mSplashAnimation.setController(controller);
//                            anim(mSplashAnimation);
//                        }
//                    }
//                }
//
//
//
//            Handler mHandler1 = new Handler();
//            mHandler1.postDelayed(new OneShotTask(pos), 2000*(pos));
//        }
//
//        Runnable runnableLogo = new Runnable() {
//            @Override
//            public void run() {
//
//                mRevealBg.setVisibility(View.GONE);
//                mSplashBg.setVisibility(View.GONE);
//                mSplashAnimation.setVisibility(View.GONE);
//                ImageRequest imageRequestLogo = ImageRequestBuilder.newBuilderWithResourceId(R.mipmap.logo_yellow).build();
//                mLogo_yellow.setImageURI(imageRequestLogo.getSourceUri());
//                mLogo_yellow.setVisibility(View.VISIBLE);
//                mLogo_yellow.setAnimation(mAnimationZoom);
//                mLogo_yellow.startAnimation(mAnimationZoom);
//                mCurrentImage = 6;
//                LogUtility.Log(TAG, "onAnimationEnd:", null);
//            }
//        };
//        Handler mHandlerLogo = new Handler();
//        mHandlerLogo.postDelayed(runnableLogo, 2000*(pos+1));



        Runnable runnable = new Runnable() {
            @Override
            public void run() {
//                RoundingParams roundingParams = RoundingParams.fromCornersRadius(5f);
//                roundingParams.setRoundAsCircle(false);
//                mSplashAnimation.getHierarchy().setRoundingParams(roundingParams);
                mCurrentImage = 2;
                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.mipmap.splash_two).build();
                mSplashAnimation.setImageURI(imageRequest.getSourceUri());
                //anim(mSplashBg);
                anim(mSplashAnimation);

            }
        };
        Handler mHandler = new Handler();
        mHandler.postDelayed(runnable, 2000);

        Runnable runnable1 = new Runnable() {
            @Override
            public void run() {
                mCurrentImage = 2;
                //changeRevealBg(R.mipmap.splash_two);
                changeBg(R.mipmap.splash_two);
                ImageRequest ir= ImageRequestBuilder.newBuilderWithResourceId(R.mipmap.splash_three).build();
                mSplashAnimation.setImageURI(ir.getSourceUri());
                anim(mSplashAnimation);
            }
        };
        Handler mHandler1 = new Handler();
        mHandler1.postDelayed(runnable1, 4000);

        Runnable runnable2 = new Runnable() {
            @Override
            public void run() {
                mCurrentImage = 2;
                ///changeRevealBg(R.mipmap.splash_three);
                changeBg(R.mipmap.splash_three);
                ImageRequest ir2= ImageRequestBuilder.newBuilderWithResourceId(R.mipmap.splash_four).build();
                mSplashAnimation.setImageURI(ir2.getSourceUri());
                anim(mSplashAnimation);
            }
        };
        Handler mHandler2 = new Handler();
        mHandler2.postDelayed(runnable2, 6000);

        Runnable runnable3 = new Runnable() {
            @Override
            public void run() {
                mCurrentImage = 2;

                //changeRevealBg(R.mipmap.splash_four);
                changeBg(R.mipmap.splash_four);
                ImageRequest ir3= ImageRequestBuilder.newBuilderWithResourceId(R.mipmap.splash_five).build();
                mSplashAnimation.setImageURI(ir3.getSourceUri());
                anim(mSplashAnimation);

            }
        };
        Handler mHandler3 = new Handler();
        mHandler3.postDelayed(runnable3, 8000);

        Runnable runnableLogo = new Runnable() {
            @Override
            public void run() {

                mRevealBg.setVisibility(View.GONE);
                mSplashBg.setVisibility(View.GONE);
                mSplashAnimation.setVisibility(View.GONE);
                ImageRequest imageRequestLogo = ImageRequestBuilder.newBuilderWithResourceId(R.mipmap.logo_yellow).build();
                mLogo_yellow.setImageURI(imageRequestLogo.getSourceUri());
                mLogo_yellow.setVisibility(View.VISIBLE);
                mLogo_yellow.setAnimation(mAnimationZoom);
                mLogo_yellow.startAnimation(mAnimationZoom);
                mCurrentImage = 6;
                LogUtility.Log(TAG, "onAnimationEnd:", null);
            }
        };
        Handler mHandlerLogo = new Handler();
        mHandlerLogo.postDelayed(runnableLogo, 10000);

        return view;
    }

//    public void changeRevealBg(int iD){
//        mRevealBg.setBackgroundResource(iD);
//    }

    /*init animations */
    private void animationInit() {
        mAnimationFadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.scale_up);
        mAnimationFadeOut.setAnimationListener(this);
        mAnimationZoom = AnimationUtils.loadAnimation(getActivity(), R.anim.logo_scale_up);
        mAnimationZoom.setAnimationListener(this);
        mAnimationFadeOutLogo = AnimationUtils.loadAnimation(getActivity(), R.anim.fadein);
        mAnimationFadeOutLogo.setAnimationListener(this);
        mAnimationFadeInLogo= AnimationUtils.loadAnimation(getActivity(), R.anim.fadein);
        mAnimationFadeInLogo.setAnimationListener(this);

        mSlideinLeft= AnimationUtils.loadAnimation(getActivity(), R.anim.logo_left_slide);
        mSlideinLeft.setAnimationListener(this);

        mSlideinTop= AnimationUtils.loadAnimation(getActivity(), R.anim.sub_logo_slide_top);
        mSlideinTop.setAnimationListener(this);
    }

    private void anim( View myView){
        // get the center for the clipping circle
        int cx = (myView.getLeft() + myView.getRight()) / 2;
        int cy = (myView.getTop() + myView.getBottom()) / 2;

        // get the final radius for the clipping circle
        int dx = Math.max(cx, myView.getWidth() - cx);
        int dy = Math.max(cy, myView.getHeight() - cy);
        float finalRadius = (float) Math.hypot(dx, dy);

        SupportAnimator animator =
                ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0, finalRadius);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.setDuration(1200);
        animator.start();
    }

    @Override
    public void onDestroyView() {
        LogUtility.Log(TAG, "onDestroyView", null);

        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        LogUtility.Log(TAG, "onDetach", null);

        super.onDetach();
    }

    @Override
    public void onPause() {
        LogUtility.Log(TAG, "onPause", null);

        super.onPause();
    }

    @Override
    public void onResume() {
        LogUtility.Log(TAG, "onResume", null);

        super.onResume();
    }

    @Override
    public void onStart() {
        LogUtility.Log(TAG, "onStart", null);

        super.onStart();
    }

    @Override
    public void onStop() {
        LogUtility.Log(TAG, "onStop", null);

        super.onStop();
    }

    /**
     *
     */
    public void startLocationService() {

        Intent mIntentForLocation = new Intent(getActivity(), LocationService.class);
        getActivity().startService(mIntentForLocation);

    }

    @TargetApi(23)

    public void setLocationPermission() {
        if (!checkLocationPermissionRuntime()) {
            LogUtility.Log(TAG, "setLocationPermission:Location permission is going to set", null);

            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_GROUP);

        }

    }

    @TargetApi(23)
    public boolean checkLocationPermissionRuntime() {
        if ((getContext().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) || (getContext().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            LogUtility.Log(TAG, "checkLocationPermissionRuntime:Location permission not set yet", null);

            return false;
        }
        return true;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {


        if (requestCode == LOCATION_PERMISSION_GROUP) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                LogUtility.Log(TAG, "onRequestPermissionsResult:Location permission granted", null);
                startLocationService();
            } else {
                LogUtility.Log(TAG, "onRequestPermissionsResult:Location permission rejected", null);
                alertDialogueForPermissions();

            }

        }


        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /**
     * @return
     */
    public boolean isLocationServiceStarted() {
        LogUtility.Log(TAG, "isLocationServiceStarted:service name", LocationService.class.getName());
        List<ActivityManager.RunningServiceInfo> mRunningServices = sActivityManager.getRunningServices(Integer.MAX_VALUE);
        for (ActivityManager.RunningServiceInfo service : mRunningServices) {


            if (service.service.getClassName().equals(SERVICE_NAME)) {


                return true;

            }


        }


        return false;
    }

    /**
     *
     */
    public void alertDialogueForPermissions() {
        mAlertDialog = new AlertDialog.Builder(getActivity())
                .setTitle("Location Permission")
                .setMessage("User have to enable the permissions for location to go ahead ")
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })


                .setNegativeButton("go to settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        showInfoScreenOfNeyya();
                    }
                })
                .create();
        mAlertDialog.show();


    }

    /**
     *
     */
    public void showInfoScreenOfNeyya() {

        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }


    @Override
    public void onGetCityResponse(boolean status) {
        boolean value = false;
        if (status) {
            LogUtility.Log(TAG, "onGetCityResponse", "status:retrieved true");
            value = mNetworkManager.whetherTheCurrentLocInDatabase();
            if (value) {
                mPreferenceManager.setCurrentLocInCityListIsAvailable(true);
            } else {
                mPreferenceManager.setCurrentLocInCityListIsAvailable(false);

            }

        } else {
            LogUtility.Log(TAG, "onGetCityResponse", "status:retrieved failed ");
        }
    }

    @Override
    public void onSplashLandingImages(boolean status) {

        if (status) {
            LogUtility.Log(TAG, "onSplashLandingImages", "status:retrieved ok");
        } else {
            LogUtility.Log(TAG, "onSplashLandingImages", "status:retrieved failed");
        }

    }

    @Override
    public void onFeatureImageResponse(boolean status) {

        if (status) {
            LogUtility.Log(TAG, "onFeatureImageResponse", "status:retrieved ok");
        }

    }


    @Override
    public void onAnimationStart(Animation animation) {


        LogUtility.Log(TAG, "onAnimationStart", null);
    }

//    @Override
//    public void onAnimationEnd(Animation animation) {
//        if (animation == mAnimationFadeOut) {
//            RoundingParams roundingParams = RoundingParams.fromCornersRadius(5f);
//            roundingParams.setRoundAsCircle(false);
//            mSplashAnimation.getHierarchy().setRoundingParams(roundingParams);
//            if (mCurrentImage == 1) {
//                LogUtility.Log(TAG, "onAnimationEnd:", null);
//                changeBg(R.mipmap.splash_one);
//                changeImage(R.mipmap.splash_two);
//            } else if (mCurrentImage == 2) {
//                LogUtility.Log(TAG, "onAnimationEnd:", null);
//
//                changeBg(R.mipmap.splash_two);
//                changeImage(R.mipmap.splash_three);
//            } else if (mCurrentImage == 3) {
//                LogUtility.Log(TAG, "onAnimationEnd:", null);
//
//                changeBg(R.mipmap.splash_three);
//                changeImage(R.mipmap.splash_four);
//            } else if (mCurrentImage == 4) {
//                LogUtility.Log(TAG, "onAnimationEnd:", null);
//
//                changeBg(R.mipmap.splash_four);
//                changeImage(R.mipmap.splash_five);
//            } else if (mCurrentImage == 5) {
//
//
//                Runnable runnable = new Runnable() {
//                    @Override
//                    public void run() {
//
//
//                        mSplashBg.setVisibility(View.GONE);
//                        mSplashAnimation.setVisibility(View.GONE);
//                        ImageRequest imageRequestLogo = ImageRequestBuilder.newBuilderWithResourceId(R.mipmap.logo_yellow).build();
//                        mLogo_yellow.setImageURI(imageRequestLogo.getSourceUri());
//                        mLogo_yellow.setVisibility(View.VISIBLE);
//                        mLogo_yellow.setAnimation(mAnimationZoom);
//                        mLogo_yellow.startAnimation(mAnimationZoom);
//                        mCurrentImage ++;
//                        LogUtility.Log(TAG, "onAnimationEnd:", null);
//                    }
//                };
//                Handler mHandler = new Handler();
//                mHandler.postDelayed(runnable, 2000);
//
//            }
//
//        } else if (animation == mAnimationZoom) {
//            if (mCurrentImage == 6) {
//
//
//                Runnable runnable = new Runnable() {
//                    @Override
//                    public void run() {
//
//
//                        mSplashBg.setVisibility(View.GONE);
//                        mSplashAnimation.setVisibility(View.GONE);
//                        ImageRequest imageRequestLogo = ImageRequestBuilder.newBuilderWithResourceId(R.mipmap.logo_orange).build();
//                        mLogo_orange.setImageURI(imageRequestLogo.getSourceUri());
//                        mLogo_orange.setVisibility(View.VISIBLE);
//                        mLogo_orange.setAnimation(mAnimationZoom);
//                        mLogo_orange.startAnimation(mAnimationZoom);
//                        mCurrentImage ++;
//                        LogUtility.Log(TAG, "onAnimationEnd:", null);
//                    }
//                };
//                Handler mHandler = new Handler();
//                mHandler.postDelayed(runnable, 200);
//
//            }else if (mCurrentImage == 7) {
//
//
//                Runnable runnable = new Runnable() {
//                    @Override
//                    public void run() {
//
//
//                        mSplashBg.setVisibility(View.GONE);
//                        mSplashAnimation.setVisibility(View.GONE);
//                        ImageRequest imageRequestLogo = ImageRequestBuilder.newBuilderWithResourceId(R.mipmap.logo_red).build();
//                        mLogo_red.setImageURI(imageRequestLogo.getSourceUri());
//                        mLogo_red.setVisibility(View.VISIBLE);
//                        mLogo_red.setAnimation(mAnimationZoom);
//                        mLogo_red.startAnimation(mAnimationZoom);
//                        mCurrentImage ++;
//                        LogUtility.Log(TAG, "onAnimationEnd:", null);
//                    }
//                };
//                Handler mHandler = new Handler();
//                mHandler.postDelayed(runnable, 200);
//
//
//            }else if (mCurrentImage == 8) {
//
//
//                Runnable runnable = new Runnable() {
//                    @Override
//                    public void run() {
//                        mSplashBg.setVisibility(View.GONE);
//                        mSplashAnimation.setVisibility(View.GONE);
//                        ImageRequest imageRequestLogo = ImageRequestBuilder.newBuilderWithResourceId(R.mipmap.logo).build();
//                        mLogo.setImageURI(imageRequestLogo.getSourceUri());
//                        mLogo_red.setVisibility(View.GONE);
//                        mLogo_orange.setVisibility(View.GONE);
//                        mLogo_yellow.setVisibility(View.GONE);
//                        mLogo.setVisibility(View.VISIBLE);
//                        mLogo.setAnimation(mAnimationZoom);
//                        mLogo.startAnimation(mAnimationZoom);
//                        mCurrentImage ++;
//                        LogUtility.Log(TAG, "onAnimationEnd:", null);
//                    }
//                };
//                Handler mHandler = new Handler();
//                mHandler.postDelayed(runnable, 2000);
//
//
//            }else {
//                LogUtility.Log(TAG, "onAnimationEnd:mAnimationZoom", null);
//
//
//                if (!isCurrentLocationInServer()) {
//                    LogUtility.Log(TAG, "onAnimationEnd:mAnimationZoom:Current location not in server", null);
//                    MainActivity parent = (MainActivity) getActivity();
//                    LogUtility.Log(TAG, "mAnimationZoom:loading send city suggestion fragment", null);
//
//                    if (parent instanceof MainActivity)
//                        // parent.load_Send_City_Suggession_fragment();
//                        parent.load_Send_City_Visit_fragment();
//                } else {
//                    LogUtility.Log(TAG, "onAnimationEnd:mAnimationZoom:Current location  in server,going to load city visit fragment", null);
//                    ((MainActivity) getActivity()).load_Send_City_Visit_fragment();
//                }
//
//            }
//        }
//
//    }


    @Override
    public void onAnimationEnd(Animation animation) {
        if (animation == mAnimationFadeOut) {
            RoundingParams roundingParams = RoundingParams.fromCornersRadius(5f);
            roundingParams.setRoundAsCircle(false);
            mSplashAnimation.getHierarchy().setRoundingParams(roundingParams);
//            if (mCurrentImage == 1) {
//                LogUtility.Log(TAG, "onAnimationEnd:", null);
//                changeBg(R.mipmap.splash_one);
//                changeImage(R.mipmap.splash_two);
//            } else if (mCurrentImage == 2) {
//                LogUtility.Log(TAG, "onAnimationEnd:", null);
//
//                changeBg(R.mipmap.splash_two);
//                changeImage(R.mipmap.splash_three);
//            } else if (mCurrentImage == 3) {
//                LogUtility.Log(TAG, "onAnimationEnd:", null);
//
//                changeBg(R.mipmap.splash_three);
//                changeImage(R.mipmap.splash_four);
//            } else if (mCurrentImage == 4) {
//                LogUtility.Log(TAG, "onAnimationEnd:", null);
//
//                changeBg(R.mipmap.splash_four);
//                changeImage(R.mipmap.splash_five);
//            } else
            if (mCurrentImage == 5) {


                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {


                        mSplashBg.setVisibility(View.GONE);
                        mSplashAnimation.setVisibility(View.GONE);
                        ImageRequest imageRequestLogo = ImageRequestBuilder.newBuilderWithResourceId(R.mipmap.logo_yellow).build();
                        mLogo_yellow.setImageURI(imageRequestLogo.getSourceUri());
                        mLogo_yellow.setVisibility(View.VISIBLE);
                        mLogo_yellow.setAnimation(mAnimationZoom);
                        mLogo_yellow.startAnimation(mAnimationZoom);
                        mCurrentImage++;
                        LogUtility.Log(TAG, "onAnimationEnd:", null);
                    }
                };
                Handler mHandler = new Handler();
                mHandler.postDelayed(runnable, 2000);

            }

        } else if (animation == mAnimationZoom) {
            if (mCurrentImage == 6) {


                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {


                        mSplashBg.setVisibility(View.GONE);
                        mSplashAnimation.setVisibility(View.GONE);
                        ImageRequest imageRequestLogo = ImageRequestBuilder.newBuilderWithResourceId(R.mipmap.logo_orange).build();
                        mLogo_orange.setImageURI(imageRequestLogo.getSourceUri());
                        mLogo_orange.setVisibility(View.VISIBLE);
                        mLogo_orange.setAnimation(mAnimationZoom);
                        mLogo_orange.startAnimation(mAnimationZoom);
                        mCurrentImage++;
                        LogUtility.Log(TAG, "onAnimationEnd:", null);
                    }
                };
                Handler mHandler = new Handler();
                mHandler.postDelayed(runnable, 200);

            } else if (mCurrentImage == 7) {


                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {


                        mSplashBg.setVisibility(View.GONE);
                        mSplashAnimation.setVisibility(View.GONE);
                        ImageRequest imageRequestLogo = ImageRequestBuilder.newBuilderWithResourceId(R.mipmap.logo_red).build();
                        mLogo_red.setImageURI(imageRequestLogo.getSourceUri());
                        mLogo_red.setVisibility(View.VISIBLE);
                        mLogo_red.setAnimation(mAnimationZoom);
                        mLogo_red.startAnimation(mAnimationZoom);
                        mCurrentImage++;
                        LogUtility.Log(TAG, "onAnimationEnd:", null);
                    }
                };
                Handler mHandler = new Handler();
                mHandler.postDelayed(runnable, 200);


            } else if (mCurrentImage == 8) {


                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        mSplashBg.setVisibility(View.GONE);
                        mSplashAnimation.setVisibility(View.GONE);
                        ImageRequest imageRequestLogo = ImageRequestBuilder.newBuilderWithResourceId(R.mipmap.travex).build();
                        mLogo.setImageURI(imageRequestLogo.getSourceUri());
                        ImageRequest imageRequestLogoSub = ImageRequestBuilder.newBuilderWithResourceId(R.mipmap.travex_sub).build();
                        mLogoSub.setImageURI(imageRequestLogoSub.getSourceUri());
                        //mLogo_red.setVisibility(View.GONE);
                        //mLogo_orange.setVisibility(View.GONE);
                        //mLogo_yellow.setVisibility(View.GONE);
                        mLogo.setVisibility(View.VISIBLE);
                        mLogoSub.setVisibility(View.VISIBLE);
                        mLogo.setAnimation(mAnimationFadeInLogo);
                        mLogo.startAnimation(mAnimationFadeInLogo);
                        mLogoSub.setAnimation(mAnimationFadeOutLogo);
                        mLogoSub.startAnimation(mAnimationFadeOutLogo);
                        mLogoSub.setAnimation(mAnimationFadeOutLogo);
                        mLogoSub.startAnimation(mAnimationFadeOutLogo);

                        mLogoSub.setAnimation(mSlideinTop);
                        mLogoSub.startAnimation(mSlideinTop);

                        mLogo.setAnimation(mSlideinLeft);
                        mLogo.startAnimation(mSlideinLeft);

                        mCurrentImage++;
                        LogUtility.Log(TAG, "onAnimationEnd:", null);
                    }
                };
                Handler mHandler = new Handler();
                mHandler.postDelayed(runnable, 500);


            }
        } else if (animation == mSlideinLeft) {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    LogUtility.Log(TAG, "onAnimationEnd:mAnimationZoom", null);


                    if (!isCurrentLocationInServer()) {
                        LogUtility.Log(TAG, "onAnimationEnd:mAnimationZoom:Current location not in server", null);
                        MainActivity parent = (MainActivity) getActivity();
                        LogUtility.Log(TAG, "mAnimationZoom:loading send city suggestion fragment", null);

                        if (parent instanceof MainActivity)
                             parent.load_Send_City_Suggession_fragment();
                            //parent.load_Send_City_Visit_fragment();
                    } else {
                        LogUtility.Log(TAG, "onAnimationEnd:mAnimationZoom:Current location  in server,going to load city visit fragment", null);
                        ((MainActivity) getActivity()).load_Send_City_Visit_fragment();
                    }
//                    MainActivity parent = (MainActivity) getActivity();
//                    parent.load_Send_City_Suggession_fragment();

                }
            };
            Handler mHandler = new Handler();
            mHandler.postDelayed(runnable, 2000);

        }

        //if (!isCurrentLocationInServer()) {
        //            LogUtility.Log(TAG, "onAnimationEnd:mAnimationZoom:Current location not in server", null);
        //            MainActivity parent = (MainActivity) getActivity();
        //            LogUtility.Log(TAG, "mAnimationZoom:loading send city suggestion fragment", null);
		//
        //            if (parent instanceof MainActivity)
        //                parent.load_Send_City_Suggession_fragment();
        //                parent.load_Send_City_Visit_fragment();
        //        } else {
        //            LogUtility.Log(TAG, "onAnimationEnd:mAnimationZoom:Current location  in server,going to load city visit fragment", null);
        //            ((MainActivity) getActivity()).load_Send_City_Visit_fragment();
        //       }



    }

    public boolean isCurrentLocationInServer() {
//	return mPreferenceManager.getCurrentLocInCityListIsAvailable();
        MainActivity parent = (MainActivity) getActivity();

        return parent.isCurrentLocationInServer();
    }

    public boolean isCurrentLocationgetTracked() {
        MainActivity parent = (MainActivity) getActivity();

        return parent.isCurrentLocationGetTracked();
    }

    private void changeBg(int id) {
        ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(id).build();
        mSplashBg.setImageURI(imageRequest.getSourceUri());

//        Uri img_uri=Uri.parse(Image_path);
//        ImageRequest request = ImageRequest.fromUri(img_uri);
//
//        DraweeController controller = Fresco.newDraweeControllerBuilder()
//                .setImageRequest(request)
//                .setOldController(mSplashBg.getController()).build();
//        //Log.e(TAG, "ImagePath uri " + img_uri);
//
//        mSplashBg.setController(controller);
    }

    private void changeImage(final int id) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                RoundingParams roundingParams = RoundingParams.fromCornersRadius(0f);
                roundingParams.setRoundAsCircle(true);
                mSplashAnimation.getHierarchy().setRoundingParams(roundingParams);
                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(id).build();
                mSplashAnimation.setImageURI(imageRequest.getSourceUri());
                mSplashAnimation.setAnimation(mAnimationFadeOut);
                mSplashAnimation.startAnimation(mAnimationFadeOut);
                mCurrentImage++;
//                ImageRequest imageRequeste = ImageRequestBuilder.newBuilderWithResourceId(id).build();
//                mSplashBg.setImageURI(imageRequeste.getSourceUri());
            }
        };
        Handler mHandler = new Handler();
        mHandler.postDelayed(runnable, 2000);
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
        LogUtility.Log(TAG, "onAnimationRepeat:", null);

            if(animation == mSlideinLeft)
            {
                 animation.cancel();
            }
    }


}
