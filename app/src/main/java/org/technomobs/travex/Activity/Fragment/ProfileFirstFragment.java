package org.technomobs.travex.Activity.Fragment;


import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Controller.Interface.AboutUsResponse;
import org.technomobs.travex.Controller.Interface.GetMemberDetailsResponse;
import org.technomobs.travex.Controller.JsonObjectMaker;
import org.technomobs.travex.Controller.NetworkManager;
import org.technomobs.travex.Controller.NetworkOptions;
import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.Common;
import org.technomobs.travex.Utillity.ListCreatorForNameValuePairRequest;
import org.technomobs.travex.Utillity.LogUtility;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Abhi on 07-03-2016.
 */
public class ProfileFirstFragment extends Fragment implements GetMemberDetailsResponse,Animation.AnimationListener {
    static Activity activity;
    ArrayList prgmName;
    ArrayList<String> tipsname_list;
    ArrayList<String> prgmNameList;
    ArrayList<String> desigList;
    ArrayList<String> prgmImages_URL;
    TextView abouttext,aboutusdesc;
    GridView gridview;
    TextView tv_profile_name,tv_gender,tv_mail,tv_children,tv_status;
    String member_gender="",member_last_name="",member_first_name="",member_no_kids="",member_martial_status="",member_profile_image,member_email="",member_id;
    ArrayList<ArrayList> tips_MainArray = new ArrayList<ArrayList>();
    ArrayList<String> question_list = new ArrayList<String>();
    ArrayList<String> type_list = new ArrayList<String>();
    ArrayList<String> profilequestions_id_list = new ArrayList<String>();
    LinearLayout interested_layout;
    ArrayList<ArrayList> answer_MainArray;
    //API Credentials

    static NetworkManager mNetworkManager = null;
    public static final String TAG = TravelEssential.class.getSimpleName();
    JsonObjectMaker mJsonObjectMaker = null;
    ListCreatorForNameValuePairRequest mListCreatorForNameValuePairRequest = null;
    ImageView fab;
    PreferenceManager mPreferenceManager;
    View rootView;
    SimpleDraweeView profile_Image;
    Fragment fragment=null;

    Animation rotateOne, rotateTwo, rotateThree;
    RelativeLayout animation_layout;

//    ArrayList<ArrayList> selected_answer_id_list = new ArrayList<>();
    ArrayList<ArrayList> selected_answer_list = new ArrayList<>();
    //   public static String [] prgmNameList={"Let Us C","c++","JAVA","Jsp","Microsoft .Net","Android","PHP","Jquery","JavaScript"};
    //   public static int [] prgmImages={R.drawable.pic1,R.drawable.pic2,R.drawable.pic3,R.drawable.pic4,R.drawable.pic5,R.drawable.pic6,R.drawable.pic2,R.drawable.pic4,R.drawable.pic1,R.drawable.pic3,R.drawable.pic4,R.drawable.pic5,R.drawable.pic6,R.drawable.pic2,R.drawable.pic4,R.drawable.pic1};

    public static ProfileFirstFragment newInstance(Activity act){
        ProfileFirstFragment fragment = new ProfileFirstFragment();
        activity=act;

        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        rootView = inflater.inflate(R.layout.profile_fragmrnt, container, false);
        mPreferenceManager = new PreferenceManager(activity);
        interested_layout=(LinearLayout)rootView.findViewById(R.id.interested_layout);
        tv_gender=(TextView)rootView.findViewById(R.id.tv_gender);
        tv_mail=(TextView)rootView.findViewById(R.id.tv_email);
        tv_profile_name=(TextView)rootView.findViewById(R.id.profile_name);
        tv_children=(TextView)rootView.findViewById(R.id.tv_children);
        tv_status=(TextView)rootView.findViewById(R.id.tv_status);
        fab=(ImageView)rootView.findViewById(R.id. fab);
//        tv_profile_name.setText(mPreferenceManager.getRegisteredUser_name());
        profile_Image=(SimpleDraweeView)rootView.findViewById(R.id. profile_Image);

        ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.user).build();
        RoundingParams roundingParams = RoundingParams.fromCornersRadius(5f);
        roundingParams.setBorder(Color.WHITE, (float) 1.0);
        roundingParams.setRoundAsCircle(true);
        profile_Image.getHierarchy().setRoundingParams(roundingParams);
        profile_Image.setImageURI(imageRequest.getSourceUri());
        animation_layout=(RelativeLayout)rootView.findViewById(R.id.settings_animation);

        answer_MainArray = new ArrayList<ArrayList>();
        initiating_APICredentials();

        animationInit();
        animationLoading();
        Runnable run = new Runnable() {
            @Override
            public void run() {
                animation_layout.setVisibility(View.GONE);
            }
        };
        Handler handler = new Handler();
        handler.postDelayed(run, 2000);

        postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.MEMBER_DETAILS_URL,
                getjsonObject(TravexApplication.MEMBER_DETAILS_TAG),
                TravexApplication.REQUEST_ID_MEMBER_DETAILS);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = ProfileFragmentTwo.newInstance(activity);
                if (fragment != null) {

                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_container, fragment);
                    fragmentTransaction.addToBackStack(fragment.getClass().getName());
                    fragmentTransaction.commit();

                }
            }
        });

        return rootView;
    }
    public void postJsonRequest(int request_type, final Map<String, String> request_header, String url, JSONObject jsonObject, final int request_id) {

        mNetworkManager.postJsonRequest(request_type, request_header, url, jsonObject, request_id);


    }
    public JSONObject getjsonObject(String tag) {
        if (tag.equals(TravexApplication.MEMBER_DETAILS_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfMemberDetails
                            (TravexApplication.MEMBER_DETAILS_MEMBER_ID

                            ),
                   mListCreatorForNameValuePairRequest.getListOfMemberDetails(mPreferenceManager.getRegisration_Id_response())
                    //    mListCreatorForNameValuePairRequest.getListOfMemberDetails("zrzore_vq_16")

            );

        }
        return  null;
    }

    public JSONObject getJson(ArrayList<String> namePair, ArrayList<String> valuePair) {


        return mJsonObjectMaker.getJson(namePair, valuePair);
    }
    public void initiating_APICredentials()
    {
        mJsonObjectMaker = new JsonObjectMaker(activity);
        mListCreatorForNameValuePairRequest = new ListCreatorForNameValuePairRequest(activity);
        mNetworkManager = NetworkManager.getSingleInstance(activity);
        mNetworkManager.setOnGetMemberDetailsResponseListener(this);
    }


    @Override
    public void onGetMemberDetailsResponse(boolean status, JSONObject jsonObject) {
//         selected_answer_id_list = new ArrayList<>();
        selected_answer_list = new ArrayList<>();
        if (status) {
            LogUtility.Log(TAG, "onGetMemberDetailsResponse:success", "jsonObject:" + jsonObject);
            JSONObject result = null;
            try {
                result = jsonObject.getJSONObject("result");

                    JSONObject profile_obj=result.getJSONObject("profile");
                try {
                    member_gender = profile_obj.getString("member_gender");
                }
                catch(Exception s){
                    s.printStackTrace();
                }
                try{
                    member_last_name=profile_obj.getString("member_last_name");
                }
                catch(Exception s){
                    s.printStackTrace();
                }
                try{
                    member_first_name=profile_obj.getString("member_first_name");
                    tv_profile_name.setText(member_first_name);
                }
                catch(Exception s){
                    s.printStackTrace();
                }
                try {
                    member_no_kids = profile_obj.getString("member_no_kids");
                }
            catch(Exception s){
                s.printStackTrace();
            }
                try{
                    member_martial_status=profile_obj.getString("member_martial_status");
                }
                catch(Exception s){
                    s.printStackTrace();
                }
                try{
                    member_profile_image = profile_obj.getString("member_profile_image");
                    if(!member_profile_image.equals("")) {
                        Uri img_uri=Uri.parse(AppConstants.PROFILE_IMAGE_ASSET_URL+ member_profile_image);
                        ImageRequest request = ImageRequest.fromUri(img_uri);

                        DraweeController controller = Fresco.newDraweeControllerBuilder()
                                .setImageRequest(request)
                                .setOldController(profile_Image.getController()).build();
                        //Log.e(TAG, "ImagePath uri " + img_uri);

                        profile_Image.setController(controller);
                    }
                    else
                    {
                        ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.user).build();
                        profile_Image.setImageURI(imageRequest.getSourceUri());
                    }
                }
                catch(Exception s){
                    s.printStackTrace();
                }
                try{
                    member_email = profile_obj.getString("member_email");
                }
                catch(Exception s){
                    s.printStackTrace();
                }
                try{
                    member_id = profile_obj.getString("member_id");
                }
                catch(Exception s){
                    s.printStackTrace();
                }
                try{
                    tv_gender.setText(member_gender);
                }
                catch(Exception s){
                    s.printStackTrace();
                }try{
                    tv_mail.setText(member_email);
                }
                catch(Exception s){
                    s.printStackTrace();
                }
                try{
                    tv_status.setText(member_martial_status);
                }
                catch(Exception s){
                    s.printStackTrace();
                }
                try{
                    tv_children.setText(member_no_kids);
                }
                catch(Exception s){
                    s.printStackTrace();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
               JSONArray profile_array=result.getJSONArray("questions");
                question_list = new ArrayList<>();
                profilequestions_id_list = new ArrayList<>();
                type_list = new ArrayList<>();
                answer_MainArray = new ArrayList<>();
                answer_MainArray = new ArrayList<>();

                for(int i=0;i<profile_array.length();i++) {
                    JSONObject profile_obj = profile_array.getJSONObject(i);
                    question_list .add(profile_obj.getString("question"));


                    profilequestions_id_list.add(profile_obj.getString("profile_questions_id"));
                    type_list .add(profile_obj.getString("type"));
                    JSONArray user_answer_array=profile_obj.getJSONArray("user_answer");
                    ArrayList<String> answer_list = new ArrayList<String>();
                    for (int ph = 0; ph < user_answer_array.length(); ph++) {

                        String answer = user_answer_array.get(ph).toString();
                        answer_list.add(answer);


                    }
                         answer_MainArray.add(answer_list);



                    ArrayList current_answer_id=new ArrayList();
                    ArrayList current_answer=new ArrayList();
                    JSONArray user_current_answer_array=null;
                    try
                    {
                        user_current_answer_array=profile_obj.getJSONArray("user_answer");
                    }
                    catch(Exception Ex)
                    {
                        user_current_answer_array=null;
                    }

                    if(user_current_answer_array != null)
                    {
                        for (int ph = 0; ph < user_current_answer_array.length(); ph++) {

                            String obj=user_current_answer_array.getString(ph);
//                            String answer_id = obj.getString("answer_id");
//                            String answer = obj.getString("answer");
                            if(!obj.equals("null"))
                            {
//                                current_answer_id.add(answer_id);
                                current_answer.add(obj);
                            }

                        }
                        if(current_answer.size()>0) {
                            //selected_answer_id_list.add(current_answer_id);
                            selected_answer_list.add(current_answer);
                        }
                        else
                        {
//                            selected_answer_id_list.add(null);
                            selected_answer_list.add(null);
                        }

                    }
                    else
                    {
//                        selected_answer_id_list.add(null);
                        current_answer.add("Select");
                        selected_answer_list.add(current_answer);
                    }
                }

                Fill_Question_And_Answers(true);




                try {

//                    for(int j=0;j<question_list.size();j++)  {
//
//                        LinearLayout hor_event_layout = new LinearLayout(activity);
//                        View inflatedView = View.inflate(activity, R.layout.interested_infate, hor_event_layout);
//                        ArrayList<String> a_list=new ArrayList<String>();
//                        String answer_text="";
//                        try {
//                            a_list = answer_MainArray.get(j);
//                             answer_text = a_list.get(0);
//                            if(answer_text.equals("null"))answer_text="";
//                        }catch (Exception y){
//                            y.printStackTrace();
//                            answer_text="";
//                        }
//
//                        TextView tv_question = (TextView) inflatedView.findViewById(R.id.tv_question);
//                        TextView tv_answer = (TextView) inflatedView.findViewById(R.id.tv_answer);
//                        tv_answer.setText(answer_text);
//                        tv_question.setText(question_list.get(j));
//
//                        interested_layout.addView(inflatedView);
//
//                    }
                }catch(Exception e){
                    e.printStackTrace();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            LogUtility.Log(TAG, "onGetMemberDetailsResponse:failure", null);

        }

    }



    public void Fill_Question_And_Answers(Boolean ALertShow)
    {
        try {
            interested_layout.removeAllViews();
            for(int j=0;j<question_list.size();j++)  {

                LinearLayout hor_event_layout = new LinearLayout(activity);
                View inflatedView = View.inflate(activity, R.layout.interested_infate, hor_event_layout);
                ArrayList<String> a_list=new ArrayList<String>();
                String answer_text="Select";
                try {
                    a_list = selected_answer_list.get(j);
                    if(a_list !=null) {
                        if (a_list.size() > 0) {
                            answer_text = "";
                        }
                        for (int ans = 0; ans < a_list.size(); ans++) {
                            String ans_txt=a_list.get(ans);
                            if(a_list.get(ans)!=null)
                            {
                                if(!ans_txt.equalsIgnoreCase("null"))
                                {
                                    answer_text = answer_text+a_list.get(ans) + ",";
                                }
                            }
                        }

                        answer_text = Common.RemoveTheLastCharacter(answer_text);
                        if(answer_text.equals(""))
                        {
                            answer_text="Select";
                        }
                    }

                }catch (Exception y){
                    y.printStackTrace();
                    answer_text="Select";
                }

                TextView tv_question = (TextView) inflatedView.findViewById(R.id.tv_question);
                TextView tv_answer = (TextView) inflatedView.findViewById(R.id.tv_answer);
                tv_answer.setText(answer_text);
                tv_question.setText(question_list.get(j));
                inflatedView.setTag(j);
                interested_layout.addView(inflatedView);
//                inflatedView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        displayAlertDialog(Integer.parseInt(v.getTag().toString()));
//                    }
//                });

            }
//            if(ALertShow == false)
//            {
//                if(alertDialog != null)
//                {
//                    alertDialog.hide();
//                }
//            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    //Animation Functions
    public void animationLoading(){
        animation_layout.setVisibility(View.VISIBLE);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                ImageView iv1 = (ImageView) rootView.findViewById(R.id.settings_one);
                ImageView iv2 = (ImageView) rootView.findViewById(R.id.settings_two);
                ImageView iv3 = (ImageView) rootView.findViewById(R.id.settings_three);

                iv1.setAnimation(rotateOne);
                iv2.setAnimation(rotateTwo);
                iv3.setAnimation(rotateThree);

                iv1.startAnimation(rotateOne);
                iv2.startAnimation(rotateTwo);
                iv3.startAnimation(rotateThree);
            }
        };
        Handler mHandler = new Handler();
        mHandler.postDelayed(runnable, 100);

    }

    public  void  animationInit(){
        rotateOne = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_one);
        rotateOne.setAnimationListener(this);
        rotateTwo= AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_two);
        rotateTwo.setAnimationListener(this);
        rotateThree= AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_three);
        rotateThree.setAnimationListener(this);
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }


    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
