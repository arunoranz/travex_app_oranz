package org.technomobs.travex.Activity.Fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Controller.Interface.GetFilterResponse;
import org.technomobs.travex.Controller.Interface.GetFilterResultsResponse;
import org.technomobs.travex.Controller.Interface.GetSearchDestinations;
import org.technomobs.travex.Controller.Interface.GetSearchTerminals;
import org.technomobs.travex.Controller.JsonObjectMaker;
import org.technomobs.travex.Controller.NetworkManager;
import org.technomobs.travex.Controller.NetworkOptions;
import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.ListCreatorForNameValuePairRequest;
import org.technomobs.travex.Utillity.LogUtility;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Abhi on 18-03-2016.
 */
public class
        FilterFragment extends Fragment implements GetFilterResponse,GetFilterResultsResponse,GetSearchTerminals, GetSearchDestinations {
    ImageView im_one,im_two,im_three,im_four,im_five;
    static Activity activity;
    TextView applay_now;
    TimePicker timePicker1;

    int rating=0;
    String airport_id,terminal_id;
    ArrayAdapter<String> adapterTerminal,adapterDestination,adapterArea,adapterCategory ,adapterKey,adapterDeveloper,adapterStatus,adapterPurpose,
            adapterTicketcost,adapterSpaspeciality,adapterEatingCost,adapterEatingHalal,adapterEstablishment,adapterContinent,adapterCuisine,adapterFeebies,adapterStayprice,adapterStaytype,adapterStayaminities,adapterShoplocation,
            adapterNightcost,adapterNightentrytype,adapterNightspecialties,adapterAirport;
    PreferenceManager mPreferenceManager = null;
    ListCreatorForNameValuePairRequest mListCreatorForNameValuePairRequest = null;
    static NetworkManager mNetworkManager = null;
    JsonObjectMaker mJsonObjectMaker = null;
    public static final String TAG = FilterFragment.class.getSimpleName();
    static String category_item,country_id;
    TextView eatingTime,spaTime,moneyTime;
    ImageView close_btn;
    Spinner spinnerTerminal,spinnerDestination,spinnerAirport,spinnerSpaspeciality,spinnerticketCost,spinnerNightcost,spinnerNightentrytype,spinnerNightspecialties,spinnereatingCost,spinnerEatingHalal,spinnerShoplocation,spinnerFeebies,spinnerStayprice,spinnerStaytype,spinnerKey ,spinneraminities,spinnerArea,spinnerCategory,spinnerDeveloper,spinnerStatus,spinnerPurpose,spinnerEstablishmenttype,spinnercontinent,spinnerCuisine;
    ArrayList<String> filterAreaId_list,filterAreaName_list,filterCategoryId_list,filterCategoryName_list,filterKeyId_list,filterKeyName_list,
            realestate_developerId_list,realestate_developerName_list,realestate_project_purposeName_list,realestate_project_purposeId_list, realestate_project_statusName_list,
            realestate_project_statusId_list,establishmentType_list,time_list,cost_list,continent_list,establishmentId_list,eating_cuisineId_list,eating_cuisineName_list,
            typesName_list,typesId_list,stayhotels_price_range_to_list,stayhotels_price_range_from_list,stay_freebiestitle_list,stay_freebiesid_list,stay_aminitiestitle_list,stay_aminitiesid_list,
            staycurrency_list, shopLocationId_list,shopLocationName_list,eatingCost_list,eatingHalal_list,nightCost_list, nightSpecialtiesname_list,nightSpecialtiesid_list,nightEntrytypeName_list,nightEntrytypeId_list,
            ticketCostId_list,ticketCost_list,spaSpeciality_list,spaSpecialityId_list,airportId_list,airportName_list,terminalId_list,terminalName_list,destinationName_list,destinationId_list;
    ArrayList<String>  ImageUrl_List,phone_list,id_list,ph_list,title_list,subtitle_list,locatin_list,review_list,noimages_list,novideos_list,description_list,titleimages_list,website_list,lon_list,lat_list;

    public static FilterFragment newInstance(Activity act) {
        Bundle args = new Bundle();
        //   tt=text;
        activity =act;

        FilterFragment fragment = new FilterFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        TextView t=(TextView)container.findViewById(R.id.text);
//        t.setText(tt);
        return inflater.inflate(R.layout.filter_fragment,
                container, false);



    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        spinnerArea=(Spinner)getView().findViewById(R.id.area);
        spinnerKey=(Spinner)getView().findViewById(R.id.key);
        spinnerCategory=(Spinner)getView().findViewById(R.id.buisiness);

        spinnerDeveloper=(Spinner)getView().findViewById(R.id.developer);
        spinnerStatus=(Spinner)getView().findViewById(R.id.pro_status);
        spinnerPurpose=(Spinner)getView().findViewById(R.id.pro_purpose);

        spinnereatingCost=(Spinner)getView().findViewById(R.id.establishment_cost);
        spinnerEatingHalal=(Spinner)getView().findViewById(R.id.eating_halal);
        spinnercontinent=(Spinner)getView().findViewById(R.id.continent);
        eatingTime=(TextView)getView().findViewById(R.id.establishment_time);
        spaTime=(TextView)getView().findViewById(R.id.spa_time);
        moneyTime=(TextView)getView().findViewById(R.id.money_time);
        close_btn=(ImageView)getView().findViewById(R.id.close);
        spinnerEstablishmenttype=(Spinner)getView().findViewById(R.id.establishment_type);
        spinnerCuisine=(Spinner)getView().findViewById(R.id.cuisine);


        spinnerFeebies=(Spinner)getView().findViewById(R.id.freebies);
        spinneraminities=(Spinner)getView().findViewById(R.id.aminities);
        spinnerStayprice=(Spinner)getView().findViewById(R.id.hotel_price);
        spinnerStaytype=(Spinner)getView().findViewById(R.id.hotel_types);
        spinnerShoplocation=(Spinner)getView().findViewById(R.id.shop_location);

        spinnerNightcost=(Spinner)getView().findViewById(R.id.night_cost);
        spinnerNightspecialties=(Spinner)getView().findViewById(R.id.night_specialties);
        spinnerNightentrytype=(Spinner)getView().findViewById(R.id.night_entrytype);
        spinnerticketCost=(Spinner)getView().findViewById(R.id.ticket_cost);
        spinnerSpaspeciality=(Spinner)getView().findViewById(R.id.spa_specialities);
        spinnerAirport=(Spinner)getView().findViewById(R.id.airport);
        spinnerTerminal=(Spinner)getView().findViewById(R.id.terminal);
        spinnerDestination=(Spinner)getView().findViewById(R.id.destination);



        filterAreaId_list=new ArrayList<String>();
        spaSpecialityId_list=new ArrayList<String>();
        spaSpeciality_list=new ArrayList<String>();
        ticketCostId_list=new ArrayList<String>();
        ticketCost_list=new ArrayList<String>();
        filterAreaName_list=new ArrayList<String>();
        filterCategoryId_list=new ArrayList<String>();
        filterCategoryName_list=new ArrayList<String>();
        filterKeyId_list=new ArrayList<String>();
        filterKeyName_list=new ArrayList<String>();
        realestate_developerId_list=new ArrayList<String>();
        realestate_developerName_list=new ArrayList<String>();
        realestate_project_purposeName_list=new ArrayList<String>();
        realestate_project_purposeId_list=new ArrayList<String>();
        realestate_project_statusName_list=new ArrayList<String>();
        realestate_project_statusId_list=new ArrayList<String>();

        establishmentType_list=new ArrayList<String>();
        time_list=new ArrayList<String>();
        cost_list=new ArrayList<String>();
        continent_list=new ArrayList<String>();
        establishmentId_list=new ArrayList<String>();
        eating_cuisineName_list=new ArrayList<String>();
        eating_cuisineId_list=new ArrayList<String>();



        stay_aminitiesid_list=new ArrayList<String>();
        stay_aminitiestitle_list=new ArrayList<String>();
        stay_freebiesid_list=new ArrayList<String>();
        stay_freebiestitle_list=new ArrayList<String>();
        terminalId_list=new ArrayList<String>();
        terminalName_list=new ArrayList<String>();
        destinationId_list=new ArrayList<String>();
        destinationName_list=new ArrayList<String>();
        destinationName_list.add("Destination");
        terminalName_list.add("Terminal");
        terminalId_list.add("Terminal_id");

        shopLocationId_list=new ArrayList<String>();
        shopLocationName_list=new ArrayList<String>();
        airportId_list=new ArrayList<String>();
        airportName_list=new ArrayList<String>();
//        eating_cuisineId_list=new ArrayList<String>();


        typesName_list=new ArrayList<String>();
        typesId_list=new ArrayList<String>();
        stayhotels_price_range_to_list=new ArrayList<String>();
        stayhotels_price_range_from_list=new ArrayList<String>();
        staycurrency_list=new ArrayList<String>();
        eatingCost_list=new ArrayList<String>();
        eatingHalal_list=new ArrayList<String>();
        continent_list=new ArrayList<String>();

        nightCost_list=new ArrayList<String>();
        nightEntrytypeName_list=new ArrayList<String>();
        nightSpecialtiesname_list=new ArrayList<String>();
        nightSpecialtiesid_list=new ArrayList<String>();
        nightEntrytypeId_list=new ArrayList<String>();



        id_list=new ArrayList<String>();
        ph_list=new ArrayList<String>();
        title_list=new ArrayList<String>();
        subtitle_list=new ArrayList<String>();
        locatin_list=new ArrayList<String>();
        review_list=new ArrayList<String>();
        noimages_list=new ArrayList<String>();
        novideos_list=new ArrayList<String>();
        description_list=new ArrayList<String>();
        titleimages_list=new ArrayList<String>();
        lat_list=new ArrayList<String>();
        lon_list=new ArrayList<String>();
        website_list=new ArrayList<String>();


        eatingHalal_list.add("Halal");
        eatingHalal_list.add("Non-Halal");
//        eatingHalal_list.add("0");
        eatingCost_list.add("Select Cost Range");
        eatingCost_list.add("10-50 AED");
        eatingCost_list.add("50-100 AED");
        eatingCost_list.add("100-150 AED");
        eatingCost_list.add("150-200 AED");
        eatingCost_list.add("200-250 AED");
        continent_list.add("Continent");

        continent_list.add("Asia");
        continent_list.add("Africa");
        continent_list.add("Europe");
        continent_list.add("North America");
        continent_list.add("Australia");
        continent_list.add("South America");
        continent_list.add("Antartica");



        im_one=(ImageView)getView().findViewById(R.id.rate_one);
        im_two=(ImageView)getView().findViewById(R.id.rate_two);
        im_three=(ImageView)getView().findViewById(R.id.rate_three);
        im_four=(ImageView)getView().findViewById(R.id.rate_four);
        im_five=(ImageView)getView().findViewById(R.id.rate_five);
        mPreferenceManager = new PreferenceManager(activity);
        applay_now=(TextView)getView().findViewById(R.id.applay_now);
        String country = mPreferenceManager.getSelectedCountryName();
        mListCreatorForNameValuePairRequest = new ListCreatorForNameValuePairRequest(activity);
        category_item=MapShowingFragment.selected_category;
        country_id = mPreferenceManager.getSelectedcountryId();
        mJsonObjectMaker = new JsonObjectMaker(activity);
        mNetworkManager = NetworkManager.getSingleInstance(activity);
        mNetworkManager.setOnGetFilterResponseListener(this);
        mNetworkManager.setOnGetFilterResultsResponseListener(this);
        mNetworkManager.setOnSearchTerminalsResponseListener(this);
        mNetworkManager.setOnSearchDestinationsResponseListener(this);
        postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_FILTERS_URL,
                getjsonObject(TravexApplication.GET_FILTERS_TAG), TravexApplication.REQUEST_ID_GET_FILTER);
        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.popBackStack();
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        });
        im_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (!v.isActivated()) {
//                    rating = rating + 1;
//                } else {
//                    rating = rating - 1;
//                }
                rating = 1;

                if (!v.isActivated()) {

                    im_one.setImageResource(R.drawable.ic_ratingbar_yellow);


                } else {
                    im_two.setImageResource(R.drawable.ratingbar_staroff);
                    im_three.setImageResource(R.drawable.ratingbar_staroff);
                    im_four.setImageResource(R.drawable.ratingbar_staroff);
                    im_five.setImageResource(R.drawable.ratingbar_staroff);

                    im_two.setActivated(false);
                    im_three.setActivated(false);
                    im_four.setActivated(false);
                    im_five.setActivated(false);
                }

                v.setActivated(!v.isActivated());
            }
        });
        im_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(!v.isActivated()){
//                    rating=rating+1;
//                }else{
//                    rating=rating-1;
//                }
                rating = 2;
                if (!v.isActivated()) {

                    im_one.setImageResource(R.drawable.ic_ratingbar_yellow);
                    im_two.setImageResource(R.drawable.ic_ratingbar_yellow);

                    im_one.setActivated(true);
                    im_two.setActivated(true);

                } else {
                    im_three.setImageResource(R.drawable.ratingbar_staroff);
                    im_four.setImageResource(R.drawable.ratingbar_staroff);
                    im_five.setImageResource(R.drawable.ratingbar_staroff);

                    im_three.setActivated(false);
                    im_four.setActivated(false);
                    im_five.setActivated(false);
                }
                v.setActivated(!v.isActivated());
            }
        });
        im_three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(!v.isActivated()){
//                    rating=rating+1;
//                }else{
//                    rating=rating-1;
//                }
                rating = 3;
                if (!v.isActivated()) {

                    im_one.setImageResource(R.drawable.ic_ratingbar_yellow);
                    im_two.setImageResource(R.drawable.ic_ratingbar_yellow);
                    im_three.setImageResource(R.drawable.ic_ratingbar_yellow);

                    im_one.setActivated(true);
                    im_two.setActivated(true);
                    im_three.setActivated(true);
                } else {

                    im_four.setImageResource(R.drawable.ratingbar_staroff);
                    im_five.setImageResource(R.drawable.ratingbar_staroff);

                    im_four.setActivated(false);
                    im_five.setActivated(false);
                }
                v.setActivated(!v.isActivated());
            }
        });
        im_four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(!v.isActivated()){
//                    rating=rating+1;
//                }else{
//                    rating=rating-1;
//                }
                rating = 4;
                if (!v.isActivated()) {

                    im_one.setImageResource(R.drawable.ic_ratingbar_yellow);
                    im_two.setImageResource(R.drawable.ic_ratingbar_yellow);
                    im_three.setImageResource(R.drawable.ic_ratingbar_yellow);
                    im_four.setImageResource(R.drawable.ic_ratingbar_yellow);

                    im_one.setActivated(true);
                    im_two.setActivated(true);
                    im_three.setActivated(true);
                    im_four.setActivated(true);
                } else {
                    im_five.setImageResource(R.drawable.ratingbar_staroff);
                    im_five.setActivated(false);
                }
                v.setActivated(!v.isActivated());
            }
        });
        im_five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(!v.isActivated()){
//                    rating=rating+1;
//                }else{
//                    rating=rating-1;
//                }
                rating = 5;
                if (!v.isActivated()) {

                    im_one.setImageResource(R.drawable.ic_ratingbar_yellow);
                    im_two.setImageResource(R.drawable.ic_ratingbar_yellow);
                    im_three.setImageResource(R.drawable.ic_ratingbar_yellow);
                    im_four.setImageResource(R.drawable.ic_ratingbar_yellow);
                    im_five.setImageResource(R.drawable.ic_ratingbar_yellow);

                    im_one.setActivated(true);
                    im_two.setActivated(true);
                    im_three.setActivated(true);
                    im_four.setActivated(true);
                    im_five.setActivated(true);

                }
                v.setActivated(!v.isActivated());
            }
        });

        eatingTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Calendar mcurrentTime = Calendar.getInstance();
//
//
//                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
//                int minute = mcurrentTime.get(Calendar.MINUTE);
//                final int ff=mcurrentTime.get(Calendar.SECOND);
//                int am=mcurrentTime.get(Calendar.AM_PM);
//                TimePickerDialog mTimePicker;
//                mTimePicker = new TimePickerDialog(activity,new TimePickerDialog.OnTimeSetListener() {
//                    @Override
//                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//                        String output = String.format("%02d:%02d:%02d", hourOfDay, minute,00);
//                        eatingTime.setText(output);
//                    }
//                }, hour, minute, true);
//                mTimePicker.setTitle("Select Time");
//
//                mTimePicker.show();
                Calendar now = Calendar.getInstance();
                com.wdullaer.materialdatetimepicker.time.TimePickerDialog tpd = com.wdullaer.materialdatetimepicker.time.TimePickerDialog.newInstance(
                        Eating_Time_callback,
                        now.get(Calendar.HOUR_OF_DAY),
                        now.get(Calendar.MINUTE),
                        false
                );
                tpd.setAccentColor(getResources().getColor(R.color.theme));
                tpd.show(getFragmentManager(), "Timepickerdialog");



            }
        });




        spaTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                setCurrentTimeOnView();
//                addListenerOnButton();
//                Calendar mcurrentTime = Calendar.getInstance();
//                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
//                int minute = mcurrentTime.get(Calendar.MINUTE);
//                int am=mcurrentTime.get(Calendar.AM_PM);
//                TimePickerDialog mTimePicker;
//                mTimePicker = new TimePickerDialog(activity, am, new TimePickerDialog.OnTimeSetListener() {
//                    @Override
//                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//                        String output = String.format("%02d:%02d:%02d", hourOfDay, minute,00);
//                        spaTime.setText(output);
//                    }
//                }, hour, minute, true);
//                mTimePicker.setTitle("Select Time");
//                mTimePicker.show();

                Calendar now = Calendar.getInstance();
                com.wdullaer.materialdatetimepicker.time.TimePickerDialog tpd = com.wdullaer.materialdatetimepicker.time.TimePickerDialog.newInstance(
                        Spa_Time_callback,
                        now.get(Calendar.HOUR_OF_DAY),
                        now.get(Calendar.MINUTE),
                        false
                );
                tpd.setAccentColor(getResources().getColor(R.color.theme));
                tpd.show(getFragmentManager(), "Timepickerdialog");


            }
        });
        moneyTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Calendar mcurrentTime = Calendar.getInstance();
//                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
//                int minute = mcurrentTime.get(Calendar.MINUTE);
//                //  int sec=mcurrentTime.get(Calendar.SECOND);
//                int am=mcurrentTime.get(Calendar.AM_PM);
//                TimePickerDialog mTimePicker;
//                mTimePicker = new TimePickerDialog(activity, am, new TimePickerDialog.OnTimeSetListener() {
//                    @Override
//                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//                        String output = String.format("%02d:%02d:%02d", hourOfDay, minute,00);
//                        moneyTime.setText(output);
//
//                    }
//                }, hour, minute, true);
//                mTimePicker.setTitle("Select Time");
//                mTimePicker.show();

                Calendar now = Calendar.getInstance();
                com.wdullaer.materialdatetimepicker.time.TimePickerDialog tpd = com.wdullaer.materialdatetimepicker.time.TimePickerDialog.newInstance(
                        Money_Time_callback,
                        now.get(Calendar.HOUR_OF_DAY),
                        now.get(Calendar.MINUTE),
                        false
                );
                tpd.setAccentColor(getResources().getColor(R.color.theme));
                tpd.show(getFragmentManager(), "Timepickerdialog");

            }
        });
        spinnerAirport.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position!=0) {
                    airport_id = airportId_list.get(position);
                    postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_SEARCH_TERMINALS_URL,
                            getjsonObject(TravexApplication.GET_SEARCH_TERMINLAS_TAG),
                            TravexApplication.REQUEST_ID_SEARCH_TERMINALS);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerTerminal.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position!=0) {
                    terminal_id = terminalId_list.get(position);
                    postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_SEARCH_DESTINATIONS_URL,
                            getjsonObject(TravexApplication.GET_SEARCH_DESTINATION_TAG),
                            TravexApplication.REQUEST_ID_SEARCH_DESTINATIONS);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        applay_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
// this is for get filter api

//            postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_FILTERS_URL,
//                    getjsonObject(TravexApplication.GET_FILTERS_TAG), TravexApplication.REQUEST_ID_GET_FILTER);
                postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_FILTER_RESULTS_URL,getjsonObject(TravexApplication.FILTERRESULTS_TAG), TravexApplication.REQUEST_ID_FILTER_RESULTS);
            }
        });

//        TextView t=(TextView)getView().findViewById(R.id.text);
//       t.setText(tt);
//        SimpleDraweeView mSplashAnimation= (SimpleDraweeView) getView().findViewById(R.id.sdvImage);
//        ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.mipmap.bg).build();
//        mSplashAnimation.setImageURI(imageRequest.getSourceUri());
    }

    @Override
    public void onGetFilterResultsResponse(boolean status, JSONObject object) {
        if (status) {

            try {
                phone_list=new ArrayList<String>();
                ImageUrl_List=new ArrayList<String>();
                JSONArray result = object.getJSONArray("result");
                for(int i=0;i<result.length();i++){
                    JSONObject json=result.getJSONObject(i);
                    id_list.add(json.getString("id"));
                    title_list.add(json.getString("Title"));
                    locatin_list.add(json.getString("Location"));
                    review_list.add(json.getString("Review"));
                    noimages_list.add(json.getString("NoOfImages"));
                    novideos_list.add(json.getString("NoOfVideos"));
                    description_list.add(json.getString("Description"));
                    website_list.add(json.getString("Website"));

                    JSONObject jsonGps=json.getJSONObject("GPS");
                    lat_list.add(jsonGps.getString("latitude"));
                    lon_list.add(jsonGps.getString("longitude"));

                    JSONArray phone_array = json.getJSONArray("PhoneNumber");
                    String PhoneNumber = "";
                    if(phone_array.length()==0) {
                        phone_list.add("No Numbers::");
                    }
                    else{
                        for(int ph=0;ph<phone_array.length();ph++)
                        {
                            String ph_no=phone_array.get(ph).toString();

                            if(!ph_no.equals(""))
                                PhoneNumber += ph_no+"::";
                        }
                        phone_list.add(PhoneNumber);

                    }


                    JSONArray image_array = json.getJSONArray("TitleImages");
                    if(image_array.length()==0) {
                        ImageUrl_List.add("No Images::");
                    }
                    else{
                        for(int im=0;im<image_array.length();im++)
                        {
                            String ImagesURL=image_array.get(im).toString();

                            if(!ImagesURL.equals(""))
                                PhoneNumber += ImagesURL+"::";
                        }
                        ImageUrl_List.add(PhoneNumber);

                    }

                }

//                Fragment fragment = searchresultFragment.newInstance(activity, ImageUrl_List,phone_list,id_list,title_list,subtitle_list,locatin_list,review_list,noimages_list,novideos_list,description_list,website_list,lon_list,lat_list,true);
//                if (fragment != null) {
//                    FragmentManager fragmentManager = getFragmentManager();
//                    fragmentManager.beginTransaction()
//                            .replace(R.id.frame_container, fragment).commit();
//
//
//                }

            }catch(Exception e){
                e.printStackTrace();
            }
            LogUtility.Log(TAG, "onGetFilterResultsResponse: :success", null);

        } else {
            LogUtility.Log(TAG, "onGetFilterResultsResponse: :failure", null);

        }
        Fragment fragment = searchresultFragment.newInstance(activity, ImageUrl_List,phone_list,id_list,title_list,subtitle_list,locatin_list,review_list,noimages_list,novideos_list,description_list,website_list,lon_list,lat_list,true);
        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, fragment).commit();


        }
    }
    @Override
    public void onGetFilterResponse(boolean status, JSONObject object) {

        if (status) {
            try {
                if(category_item.equals("Business")) {
                    spinnerKey.setVisibility(View.VISIBLE);
                    spinnerArea.setVisibility(View.VISIBLE);
                    spinnerCategory.setVisibility(View.VISIBLE);
                    JSONObject jsonObject = object.getJSONObject("result");
                    JSONArray jsonArea = jsonObject.getJSONArray("areas");
                    filterAreaId_list.add("");
                    filterAreaName_list.add("Area");
                    for (int i = 0; i < jsonArea.length(); i++) {
                        JSONObject json = jsonArea.getJSONObject(i);
                        filterAreaId_list.add(json.getString("area_id"));
                        filterAreaName_list.add(json.getString("area"));
                    }
                    filterCategoryName_list.add("Category");
                    filterCategoryId_list.add("business_category_id");
                    JSONArray jsonCategory = jsonObject.getJSONArray("categories");
                    for (int i = 0; i < jsonCategory.length(); i++) {
                        JSONObject json = jsonCategory.getJSONObject(i);
                        filterCategoryName_list.add(json.getString("business_category_name"));
                        filterCategoryId_list.add(json.getString("business_category_id"));
                    }
                    JSONArray jsonKey = jsonObject.getJSONArray("keywords");
                    filterKeyId_list.add("");
                    filterKeyName_list.add("Keyword");
                    for (int i = 0; i < jsonKey.length(); i++) {
                        JSONObject json = jsonKey.getJSONObject(i);
                        filterKeyId_list.add(json.getString("keywords_id"));
                        filterKeyName_list.add(json.getString("category_keyword"));
                    }
                    adapterArea = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterAreaName_list);
                    adapterCategory = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterCategoryName_list);
                    adapterKey = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterKeyName_list);


                    spinnerArea.setAdapter(adapterArea);
                    spinnerCategory.setAdapter(adapterCategory);
                    spinnerKey.setAdapter(adapterKey);


                    LogUtility.Log(TAG, "onGetFilterResponse: :success", null);
                }
                else if(category_item.equals("Airline")){
                    ArrayList<String> tempterminallist = new ArrayList<String>();
                    ArrayList<String> tempdestinationlist = new ArrayList<String>();
                    tempterminallist.add("Terminal");
                    tempdestinationlist.add("Destination");
                    spinnerKey.setVisibility(View.VISIBLE);
                    spinnerAirport.setVisibility(View.VISIBLE);
                    spinnerTerminal.setVisibility(View.VISIBLE);
                    spinnerDestination.setVisibility(View.VISIBLE);

                    JSONObject jsonObject = object.getJSONObject("result");

                    JSONArray jsonKey = jsonObject.getJSONArray("keywords");
                    filterKeyId_list.add("");
                    filterKeyName_list.add("Keyword");
                    for (int i = 0; i < jsonKey.length(); i++) {
                        JSONObject json = jsonKey.getJSONObject(i);
                        filterKeyId_list.add(json.getString("keywords_id"));
                        filterKeyName_list.add(json.getString("category_keyword"));
                    }
                    JSONArray jsonAirport = jsonObject.getJSONArray("airports");
                    airportId_list.add("");
                    airportName_list.add("AirportName");

                    for (int i = 0; i < jsonAirport.length(); i++) {
                        JSONObject json = jsonAirport.getJSONObject(i);
                        airportId_list.add(json.getString("airports_id"));
                        airportName_list.add(json.getString("airports_name"));
                    }
                    adapterKey = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterKeyName_list);
                    adapterAirport = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, airportName_list);
                    adapterTerminal = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, tempterminallist);
                    adapterDestination = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, tempdestinationlist);



                    spinnerKey.setAdapter(adapterKey);
                    spinnerAirport.setAdapter(adapterAirport);
                    spinnerTerminal.setAdapter(adapterTerminal);
                    spinnerDestination.setAdapter(adapterDestination);

                    LogUtility.Log(TAG, "onGetFilterResponse: :success", null);
                }
                else if(category_item.equals("Real Estate")) {
                    spinnerKey.setVisibility(View.VISIBLE);
                    spinnerArea.setVisibility(View.VISIBLE);
                    spinnerPurpose.setVisibility(View.VISIBLE);
                    spinnerStatus.setVisibility(View.VISIBLE);
                    spinnerDeveloper.setVisibility(View.VISIBLE);
                    JSONObject jsonObject = object.getJSONObject("result");
                    JSONArray jsonArea = jsonObject.getJSONArray("areas");
                    filterAreaId_list.clear();
                    filterAreaName_list.clear();
                    filterAreaId_list.add("");
                    filterAreaName_list.add("Area");
                    for (int i = 0; i < jsonArea.length(); i++) {
                        JSONObject json = jsonArea.getJSONObject(i);
                        filterAreaId_list.add(json.getString("area_id"));
                        filterAreaName_list.add(json.getString("area"));
                    }

                    JSONArray jsonPurpose = jsonObject.getJSONArray("purpose");
                    realestate_project_purposeName_list.add("Purpose");
                    realestate_project_purposeId_list.add("");
                    for (int i = 0; i < jsonPurpose.length(); i++) {
                        JSONObject json = jsonPurpose.getJSONObject(i);
                        realestate_project_purposeName_list.add(json.getString("realestate_project_purpose_name"));
                        realestate_project_purposeId_list.add(json.getString("realestate_project_purpose_id"));
                    }
                    JSONArray jsonKey = jsonObject.getJSONArray("keywords");
                    filterKeyId_list.clear();
                    filterKeyName_list.clear();
                    filterKeyId_list.add("");
                    filterKeyName_list.add("Keyword");
                    for (int i = 0; i < jsonKey.length(); i++) {
                        JSONObject json = jsonKey.getJSONObject(i);
                        filterKeyId_list.add(json.getString("keywords_id"));
                        filterKeyName_list.add(json.getString("category_keyword"));
                    }

                    JSONArray jsonStatus = jsonObject.getJSONArray("status");
                    realestate_project_statusName_list.clear();
                    realestate_project_statusId_list.clear();
                    realestate_project_statusId_list.add("");
                    realestate_project_statusName_list.add("status");
                    for (int i = 0; i < jsonStatus.length(); i++) {
                        JSONObject json = jsonStatus.getJSONObject(i);
                        realestate_project_statusName_list.add(json.getString("realestate_project_status_name"));
                        realestate_project_statusId_list.add(json.getString("realestate_project_status_id"));
                    }
                    JSONArray jsonDevelopers = jsonObject.getJSONArray("developers");
                    realestate_developerId_list.clear();
                    realestate_developerName_list.clear();
                    realestate_developerId_list.add("");
                    realestate_developerName_list.add("developers");
                    for (int i = 0; i < jsonKey.length(); i++) {
                        JSONObject json = jsonDevelopers.getJSONObject(i);
                        realestate_developerId_list.add(json.getString("realestate_developer_id"));
                        realestate_developerName_list.add(json.getString("realestate_developer_name"));
                    }


                    adapterArea = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterAreaName_list);
                    adapterDeveloper = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, realestate_developerName_list);
                    adapterKey = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterKeyName_list);
                    adapterStatus = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, realestate_project_statusName_list);
                    adapterPurpose= new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, realestate_project_purposeName_list);


                    spinnerArea.setAdapter(adapterArea);

                    spinnerKey.setAdapter(adapterKey);
                    spinnerDeveloper.setAdapter(adapterDeveloper);
                    spinnerStatus.setAdapter(adapterStatus);
                    spinnerPurpose.setAdapter(adapterPurpose);

                    LogUtility.Log(TAG, "onGetFilterResponse: :success", null);

                }
                else if(category_item.equals("Car Rentals")){
                    spinnerKey.setVisibility(View.VISIBLE);
                    spinnerArea.setVisibility(View.VISIBLE);
                    JSONObject jsonObject = object.getJSONObject("result");
                    JSONArray jsonArea = jsonObject.getJSONArray("areas");
                    filterAreaId_list.clear();
                    filterAreaName_list.clear();
                    filterAreaId_list.add("");
                    filterAreaName_list.add("Area");
                    for (int i = 0; i < jsonArea.length(); i++) {
                        JSONObject json = jsonArea.getJSONObject(i);
                        filterAreaId_list.add(json.getString("area_id"));
                        filterAreaName_list.add(json.getString("area"));
                    }


                    JSONArray jsonKey = jsonObject.getJSONArray("keywords");
                    filterKeyId_list.clear();
                    filterKeyName_list.clear();
                    filterKeyId_list.add("");
                    filterKeyName_list.add("Keyword");
                    for (int i = 0; i < jsonKey.length(); i++) {
                        JSONObject json = jsonKey.getJSONObject(i);
                        filterKeyId_list.add(json.getString("keywords_id"));
                        filterKeyName_list.add(json.getString("category_keyword"));
                    }

                    adapterArea = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterAreaName_list);
                    adapterKey = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterKeyName_list);


                    spinnerArea.setAdapter(adapterArea);

                    spinnerKey.setAdapter(adapterKey);

                    LogUtility.Log(TAG, "onGetFilterResponse: :success", null);

                }
                else if(category_item.equals("Eating Out")){
                    spinnerKey.setVisibility(View.VISIBLE);
                    spinnerArea.setVisibility(View.VISIBLE);
                    spinnerEstablishmenttype.setVisibility(View.VISIBLE);
                    spinnereatingCost.setVisibility(View.VISIBLE);
                    eatingTime.setVisibility(View.VISIBLE);
                    spinnerEatingHalal.setVisibility(View.VISIBLE);
                    JSONObject jsonObject = object.getJSONObject("result");
                    JSONArray jsonArea = jsonObject.getJSONArray("areas");
                    filterAreaId_list.clear();
                    filterAreaName_list.clear();
                    filterAreaId_list.add("");
                    filterAreaName_list.add("Area");
                    for (int i = 0; i < jsonArea.length(); i++) {
                        JSONObject json = jsonArea.getJSONObject(i);
                        filterAreaId_list.add(json.getString("area_id"));
                        filterAreaName_list.add(json.getString("area"));
                    }
                    establishmentType_list.add("Establishments Type");
                    establishmentId_list.add("");
                    JSONArray jsonEstablishments = jsonObject.getJSONArray("establishments");
                    for (int i = 0; i < jsonEstablishments.length(); i++) {
                        JSONObject json = jsonEstablishments.getJSONObject(i);
                        establishmentType_list.add(json.getString("eating_establishments_type"));
                        establishmentId_list.add(json.getString("eating_establishments_id"));
                    }
                    JSONArray jsonKey = jsonObject.getJSONArray("keywords");
                    filterKeyId_list.clear();
                    filterKeyName_list.clear();
                    filterKeyId_list.add("");
                    filterKeyName_list.add("Keyword");
                    for (int i = 0; i < jsonKey.length(); i++) {
                        JSONObject json = jsonKey.getJSONObject(i);
                        filterKeyId_list.add(json.getString("keywords_id"));
                        filterKeyName_list.add(json.getString("category_keyword"));
                    }

                    JSONArray jsonStatus = jsonObject.getJSONArray("cuisines");

                    eating_cuisineName_list.add("Cuisine");
                    eating_cuisineId_list.add("");
                    for (int i = 0; i < jsonStatus.length(); i++) {
                        JSONObject json = jsonStatus.getJSONObject(i);
                        eating_cuisineName_list.add(json.getString("eating_cuisine_name"));
                        eating_cuisineId_list.add(json.getString("eating_cuisine_id"));
                    }
//            JSONArray jsonDevelopers = jsonObject.getJSONArray("developers");
//            realestate_developerId_list.clear();
//            realestate_developerName_list.clear();
//            realestate_developerId_list.add("developers");
//            realestate_developerName_list.add("developers");
//            for (int i = 0; i < jsonKey.length(); i++) {
//                JSONObject json = jsonDevelopers.getJSONObject(i);
//                realestate_developerId_list.add(json.getString("realestate_developer_id"));
//                realestate_developerName_list.add(json.getString("realestate_developer_name"));
//            }


                    adapterArea = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterAreaName_list);
                    adapterKey = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterKeyName_list);
                    adapterEstablishment = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, establishmentType_list);
                    adapterCuisine = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, eating_cuisineName_list);
                    adapterEatingHalal = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, eatingHalal_list);
                    adapterEatingCost = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, eatingCost_list);

                    spinnerArea.setAdapter(adapterArea);

                    spinnerKey.setAdapter(adapterKey);
                    spinnerEstablishmenttype.setAdapter(adapterEstablishment);

                    spinnerCuisine.setAdapter(adapterCuisine);
                    spinnereatingCost.setAdapter(adapterEatingCost);

                    spinnerEatingHalal.setAdapter(adapterEatingHalal);

                    LogUtility.Log(TAG, "onGetFilterResponse: :success", null);

                } else if (category_item.equals("Embasies & Consulates")){
                    spinnerKey.setVisibility(View.VISIBLE);
                    spinnercontinent.setVisibility(View.VISIBLE);

                    JSONObject jsonObject = object.getJSONObject("result");


                    JSONArray jsonKey = jsonObject.getJSONArray("keywords");
                    filterKeyId_list.clear();
                    filterKeyName_list.clear();
                    filterKeyId_list.add("");
                    filterKeyName_list.add("Keyword");
                    for (int i = 0; i < jsonKey.length(); i++) {
                        JSONObject json = jsonKey.getJSONObject(i);
                        filterKeyId_list.add(json.getString("keywords_id"));
                        filterKeyName_list.add(json.getString("category_keyword"));
                    }




                    adapterKey = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterKeyName_list);
                    adapterContinent= new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, continent_list);

                    spinnerKey.setAdapter(adapterKey);
                    spinnercontinent.setAdapter(adapterContinent);


                    LogUtility.Log(TAG, "onGetFilterResponse: :success", null);

                }
                if(category_item.equals("Stay/Hotels")){
                    spinnerKey.setVisibility(View.VISIBLE);
                    spinnerArea.setVisibility(View.VISIBLE);

                    spinnerFeebies.setVisibility(View.VISIBLE);
                    spinneraminities.setVisibility(View.VISIBLE);
                    spinnerStayprice.setVisibility(View.VISIBLE);
                    spinnerStaytype.setVisibility(View.VISIBLE);
                    JSONObject jsonObject = object.getJSONObject("result");
                    JSONArray jsonArea = jsonObject.getJSONArray("areas");
                    filterAreaId_list.add("");
                    filterAreaName_list.add("Area");
                    for (int i = 0; i < jsonArea.length(); i++) {
                        JSONObject json = jsonArea.getJSONObject(i);
                        filterAreaId_list.add(json.getString("area_id"));
                        filterAreaName_list.add(json.getString("area"));
                    }

                    JSONArray jsonTypes= jsonObject.getJSONArray("types");
                    typesName_list.add("Type");
                    typesId_list.add("");
                    stayhotels_price_range_to_list.add("price");
                    stayhotels_price_range_from_list.add("Price");
                    staycurrency_list.add("Currency");
                    for (int i = 0; i < jsonTypes.length(); i++) {
                        JSONObject json = jsonTypes.getJSONObject(i);
                        typesName_list.add(json.getString("hotels_type_title"));
                        typesId_list.add(json.getString("hoteltypes_id"));
                        stayhotels_price_range_to_list.add(json.getString("hotels_price_range_to"));
                        stayhotels_price_range_from_list.add(json.getString("hotels_price_range_from")+"-"+json.getString("hotels_price_range_to"));
                        staycurrency_list.add(json.getString("currency"));


                    }
                    JSONArray jsonAminities= jsonObject.getJSONArray("aminities");
                    stay_aminitiestitle_list.add("Aminities");
                    stay_aminitiesid_list.add("");
                    for (int i = 0; i < jsonAminities.length(); i++) {
                        JSONObject json = jsonAminities.getJSONObject(i);
                        stay_aminitiesid_list.add(json.getString("aminities_id"));
                        stay_aminitiestitle_list.add(json.getString("aminities_title"));
                    }

                    JSONArray jsonFreebies = jsonObject.getJSONArray("freebies");
                    stay_freebiesid_list.add("");
                    stay_freebiestitle_list.add("Fbies");
                    for (int i = 0; i < jsonFreebies.length(); i++) {
                        JSONObject json = jsonFreebies.getJSONObject(i);
                        stay_freebiesid_list.add(json.getString("freebies_id"));
                        stay_freebiestitle_list.add(json.getString("freebies_title"));
                    }
                    JSONArray jsonKey = jsonObject.getJSONArray("keywords");
                    filterKeyId_list.add("");
                    filterKeyName_list.add("Keyword");
                    for (int i = 0; i < jsonKey.length(); i++) {
                        JSONObject json = jsonKey.getJSONObject(i);
                        filterKeyId_list.add(json.getString("keywords_id"));
                        filterKeyName_list.add(json.getString("category_keyword"));
                    }
                    adapterArea = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterAreaName_list);
                    adapterKey = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterKeyName_list);

                    adapterFeebies = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, stay_freebiestitle_list);
                    adapterStayaminities = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, stay_aminitiestitle_list);
                    adapterStayprice = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, stayhotels_price_range_from_list);
                    adapterStaytype = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, typesName_list);
                    spinnerArea.setAdapter(adapterArea);
                    spinnerKey.setAdapter(adapterKey);
                    spinnerFeebies.setAdapter(adapterFeebies);
                    spinneraminities.setAdapter(adapterStayaminities);
                    spinnerStayprice.setAdapter(adapterStayprice);
                    spinnerStaytype.setAdapter(adapterStaytype);


                    LogUtility.Log(TAG, "onGetFilterResponse: :success", null);
                }
                else if(category_item.equals("Emergencies")){
                    spinnerKey.setVisibility(View.VISIBLE);
                    spinnerArea.setVisibility(View.VISIBLE);
                    spinnerCategory.setVisibility(View.VISIBLE);
                    JSONObject jsonObject = object.getJSONObject("result");
                    JSONArray jsonArea = jsonObject.getJSONArray("areas");
                    filterAreaId_list.clear();
                    filterAreaName_list.clear();
                    filterAreaId_list.add("");
                    filterAreaName_list.add("Area");
                    for (int i = 0; i < jsonArea.length(); i++) {
                        JSONObject json = jsonArea.getJSONObject(i);
                        filterAreaId_list.add(json.getString("area_id"));
                        filterAreaName_list.add(json.getString("area"));
                    }
                    filterCategoryName_list.clear();
                    filterCategoryId_list.clear();
                    filterCategoryName_list.add("Category");
                    filterCategoryId_list.add("");
                    JSONArray jsonCategory = jsonObject.getJSONArray("categories");
                    for (int i = 0; i < jsonCategory.length(); i++) {
                        JSONObject json = jsonCategory.getJSONObject(i);
                        filterCategoryName_list.add(json.getString("emergency_category_name"));
                        filterCategoryId_list.add(json.getString("emergency_category_id"));
                    }
                    JSONArray jsonKey = jsonObject.getJSONArray("keywords");
                    filterKeyId_list.clear();
                    filterKeyName_list.clear();
                    filterKeyId_list.add("");
                    filterKeyName_list.add("Keyword");
                    for (int i = 0; i < jsonKey.length(); i++) {
                        JSONObject json = jsonKey.getJSONObject(i);
                        filterKeyId_list.add(json.getString("keywords_id"));
                        filterKeyName_list.add(json.getString("category_keyword"));
                    }
                    adapterArea = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterAreaName_list);
                    adapterCategory = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterCategoryName_list);
                    adapterKey = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterKeyName_list);


                    spinnerArea.setAdapter(adapterArea);
                    spinnerCategory.setAdapter(adapterCategory);
                    spinnerKey.setAdapter(adapterKey);


                    LogUtility.Log(TAG, "onGetFilterResponse: :success", null);
                }
                else if(category_item.equals("Shopping")){
                    spinnerKey.setVisibility(View.VISIBLE);
                    spinnerArea.setVisibility(View.VISIBLE);
                    spinnerCategory.setVisibility(View.VISIBLE);
                    spinnerShoplocation.setVisibility(View.VISIBLE);
                    JSONObject jsonObject = object.getJSONObject("result");
                    JSONArray jsonArea = jsonObject.getJSONArray("areas");
                    filterAreaId_list.clear();
                    filterAreaName_list.clear();
                    filterAreaId_list.add("");
                    filterAreaName_list.add("Area");
                    for (int i = 0; i < jsonArea.length(); i++) {
                        JSONObject json = jsonArea.getJSONObject(i);
                        filterAreaId_list.add(json.getString("area_id"));
                        filterAreaName_list.add(json.getString("area"));
                    }
                    filterCategoryName_list.clear();
                    filterCategoryId_list.clear();
                    filterCategoryName_list.add("Shopping Category");
                    filterCategoryId_list.add("");
                    JSONArray jsonCategory = jsonObject.getJSONArray("categories");
                    for (int i = 0; i < jsonCategory.length(); i++) {
                        JSONObject json = jsonCategory.getJSONObject(i);
                        filterCategoryName_list.add(json.getString("shopping_category_name"));
                        filterCategoryId_list.add(json.getString("shopping_category_id"));
                    }
                    JSONArray jsonKey = jsonObject.getJSONArray("keywords");
                    filterKeyId_list.clear();
                    filterKeyName_list.clear();
                    filterKeyId_list.add("");
                    filterKeyName_list.add("Keyword");
                    for (int i = 0; i < jsonKey.length(); i++) {
                        JSONObject json = jsonKey.getJSONObject(i);
                        filterKeyId_list.add(json.getString("keywords_id"));
                        filterKeyName_list.add(json.getString("category_keyword"));
                    }
                    JSONArray jsonlocation = jsonObject.getJSONArray("locations");
                    shopLocationId_list.clear();
                    shopLocationName_list.clear();
                    shopLocationId_list.add("");
                    shopLocationName_list.add(" Shopping Location");
                    for (int i = 0; i < jsonlocation.length(); i++) {
                        JSONObject json = jsonlocation.getJSONObject(i);
                        shopLocationId_list.add(json.getString("shopping_location_id"));
                        shopLocationName_list.add(json.getString("shopping_location_name"));
                    }


                    adapterArea = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterAreaName_list);
                    adapterCategory = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterCategoryName_list);
                    adapterKey = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterKeyName_list);
                    adapterShoplocation = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, shopLocationName_list);


                    spinnerArea.setAdapter(adapterArea);
                    spinnerCategory.setAdapter(adapterCategory);
                    spinnerKey.setAdapter(adapterKey);
                    spinnerShoplocation.setAdapter(adapterShoplocation);


                    LogUtility.Log(TAG, "onGetFilterResponse: :success", null);
                }
                else if((category_item.equals("Car Rental"))){
                    spinnerKey.setVisibility(View.VISIBLE);
                    spinnerArea.setVisibility(View.VISIBLE);

                    JSONObject jsonObject = object.getJSONObject("result");
                    JSONArray jsonArea = jsonObject.getJSONArray("areas");
                    filterAreaId_list.clear();
                    filterAreaName_list.clear();
                    filterAreaId_list.add("");
                    filterAreaName_list.add("Area");
                    for (int i = 0; i < jsonArea.length(); i++) {
                        JSONObject json = jsonArea.getJSONObject(i);
                        filterAreaId_list.add(json.getString("area_id"));
                        filterAreaName_list.add(json.getString("area"));
                    }


                    JSONArray jsonKey = jsonObject.getJSONArray("keywords");
                    filterKeyId_list.clear();
                    filterKeyName_list.clear();
                    filterKeyId_list.add("");
                    filterKeyName_list.add("Keyword");
                    for (int i = 0; i < jsonKey.length(); i++) {
                        JSONObject json = jsonKey.getJSONObject(i);
                        filterKeyId_list.add(json.getString("keywords_id"));
                        filterKeyName_list.add(json.getString("category_keyword"));
                    }
                    adapterArea = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterAreaName_list);
                    adapterKey = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterKeyName_list);


                    spinnerArea.setAdapter(adapterArea);

                    spinnerKey.setAdapter(adapterKey);


                    LogUtility.Log(TAG, "onGetFilterResponse: :success", null);
                }
                else if(category_item.equals("Money Exchange")){
                    spinnerKey.setVisibility(View.VISIBLE);
                    spinnerArea.setVisibility(View.VISIBLE);
                    moneyTime.setVisibility(View.VISIBLE);

                    JSONObject jsonObject = object.getJSONObject("result");
                    JSONArray jsonArea = jsonObject.getJSONArray("areas");
                    filterAreaId_list.clear();
                    filterAreaName_list.clear();
                    filterAreaId_list.add("");
                    filterAreaName_list.add("Area");
                    for (int i = 0; i < jsonArea.length(); i++) {
                        JSONObject json = jsonArea.getJSONObject(i);
                        filterAreaId_list.add(json.getString("area_id"));
                        filterAreaName_list.add(json.getString("area"));
                    }


                    JSONArray jsonKey = jsonObject.getJSONArray("keywords");
                    filterKeyId_list.clear();
                    filterKeyName_list.clear();
                    filterKeyId_list.add("");
                    filterKeyName_list.add("Keyword");
                    for (int i = 0; i < jsonKey.length(); i++) {
                        JSONObject json = jsonKey.getJSONObject(i);
                        filterKeyId_list.add(json.getString("keywords_id"));
                        filterKeyName_list.add(json.getString("category_keyword"));
                    }
                    adapterArea = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterAreaName_list);
                    adapterKey = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterKeyName_list);


                    spinnerArea.setAdapter(adapterArea);

                    spinnerKey.setAdapter(adapterKey);


                    LogUtility.Log(TAG, "onGetFilterResponse: :success", null);
                }
                else if(category_item.equals("Things To Do")){
                    spinnerKey.setVisibility(View.VISIBLE);
                    spinnerArea.setVisibility(View.VISIBLE);
                    spinnerCategory.setVisibility(View.VISIBLE);

                    JSONObject jsonObject = object.getJSONObject("result");
                    JSONArray jsonArea = jsonObject.getJSONArray("areas");
                    filterAreaId_list.clear();
                    filterAreaName_list.clear();
                    filterAreaId_list.add("");
                    filterAreaName_list.add("Area");
                    for (int i = 0; i < jsonArea.length(); i++) {
                        JSONObject json = jsonArea.getJSONObject(i);
                        filterAreaId_list.add(json.getString("area_id"));
                        filterAreaName_list.add(json.getString("area"));
                    }


                    JSONArray jsonKey = jsonObject.getJSONArray("keywords");
                    filterKeyId_list.clear();
                    filterKeyName_list.clear();
                    filterKeyId_list.add("");
                    filterKeyName_list.add("Keyword");
                    for (int i = 0; i < jsonKey.length(); i++) {
                        JSONObject json = jsonKey.getJSONObject(i);
                        filterKeyId_list.add(json.getString("keywords_id"));
                        filterKeyName_list.add(json.getString("category_keyword"));
                    }
                    ArrayList<String> categorylist=new ArrayList<String>();
                    categorylist.add("Category");
                    categorylist.add("Airline");
                    categorylist.add("Eating Out");
                    categorylist.add("Stay/Hotels");
                    categorylist.add("Nightlfe");
                    categorylist.add("Spa");
                    categorylist.add("Things To Do");
                    categorylist.add("Shopping");
                    categorylist.add("Business");
                    categorylist.add("Real Estate");
                    categorylist.add("Money Exchange");
                    categorylist.add("Events");
                    categorylist.add("Emergencies");
                    categorylist.add("Car Rentals");
                    categorylist.add("Embasies & Consulates");
                    categorylist.add("Tour & Travel");
                    adapterArea = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterAreaName_list);
                    adapterKey = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterKeyName_list);
                    adapterCategory= new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, categorylist);

                    spinnerArea.setAdapter(adapterArea);

                    spinnerKey.setAdapter(adapterKey);
                    spinnerCategory.setAdapter(adapterCategory);

                    LogUtility.Log(TAG, "onGetFilterResponse: :success", null);
                }
                else if(category_item.equals("Nightlife")){
                    spinnerKey.setVisibility(View.VISIBLE);
                    spinnerArea.setVisibility(View.VISIBLE);
                    spinnerNightspecialties.setVisibility(View.VISIBLE);
                    spinnerNightentrytype.setVisibility(View.VISIBLE);
                    spinnerNightcost.setVisibility(View.VISIBLE);

                    JSONObject jsonObject = object.getJSONObject("result");
                    JSONArray jsonArea = jsonObject.getJSONArray("areas");
                    filterAreaId_list.clear();
                    filterAreaName_list.clear();
                    filterAreaId_list.add("");
                    filterAreaName_list.add("Area");
                    for (int i = 0; i < jsonArea.length(); i++) {
                        JSONObject json = jsonArea.getJSONObject(i);
                        filterAreaId_list.add(json.getString("area_id"));
                        filterAreaName_list.add(json.getString("area"));
                    }


                    JSONArray jsonKey = jsonObject.getJSONArray("keywords");
                    filterKeyId_list.clear();
                    filterKeyName_list.clear();
                    filterKeyId_list.add("");
                    filterKeyName_list.add("Keyword");
                    for (int i = 0; i < jsonKey.length(); i++) {
                        JSONObject json = jsonKey.getJSONObject(i);
                        filterKeyId_list.add(json.getString("keywords_id"));
                        filterKeyName_list.add(json.getString("category_keyword"));
                    }


                    JSONArray jsonEntrytype = jsonObject.getJSONArray("policies");
                    nightEntrytypeName_list.clear();
                    nightEntrytypeId_list.clear();
                    nightEntrytypeId_list.add("");
                    nightEntrytypeName_list.add("Entry Type");
                    for (int i = 0; i < jsonEntrytype.length(); i++) {
                        JSONObject json = jsonEntrytype.getJSONObject(i);
                        nightEntrytypeId_list.add(json.getString("night_door_policies_id"));
                        nightEntrytypeName_list.add(json.getString("night_door_policies_name"));
                    }
                    JSONArray jsonSpecialities= jsonObject.getJSONArray("specialities");
                    nightSpecialtiesid_list.clear();
                    nightSpecialtiesname_list.clear();
                    nightSpecialtiesid_list.add("");
                    nightSpecialtiesname_list.add("Specialities");
                    for (int i = 0; i < jsonSpecialities.length(); i++) {
                        JSONObject json = jsonSpecialities.getJSONObject(i);
                        nightSpecialtiesid_list.add(json.getString("night_specialities_id"));
                        nightSpecialtiesname_list.add(json.getString("night_specialities_name"));
                    }
                    nightCost_list.add("Select Cost Range");
                    nightCost_list.add("50-100 AED");
                    nightCost_list.add("100-150 AED");
                    nightCost_list.add("150-200 AED");
                    nightCost_list.add("200-250 AED");
                    adapterArea = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterAreaName_list);
                    adapterKey = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterKeyName_list);
                    adapterNightcost = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, nightCost_list);
                    adapterNightentrytype = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, nightEntrytypeName_list);
                    adapterNightspecialties = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, nightSpecialtiesname_list);

                    spinnerArea.setAdapter(adapterArea);

                    spinnerKey.setAdapter(adapterKey);
                    spinnerNightcost.setAdapter(adapterNightcost);
                    spinnerNightentrytype.setAdapter(adapterNightentrytype);
                    spinnerNightspecialties.setAdapter(adapterNightspecialties);

                    LogUtility.Log(TAG, "onGetFilterResponse: :success", null);
                }
                else if(category_item.equals("Events")){
                    spinnerKey.setVisibility(View.VISIBLE);
                    spinnerArea.setVisibility(View.VISIBLE);
                    spinnerCategory.setVisibility(View.VISIBLE);
                    spinnerticketCost.setVisibility(View.VISIBLE);

                    JSONObject jsonObject = object.getJSONObject("result");
                    JSONArray jsonArea = jsonObject.getJSONArray("areas");
                    filterAreaId_list.clear();
                    filterAreaName_list.clear();
                    filterAreaId_list.add("");
                    filterAreaName_list.add("Area");
                    for (int i = 0; i < jsonArea.length(); i++) {
                        JSONObject json = jsonArea.getJSONObject(i);
                        filterAreaId_list.add(json.getString("area_id"));
                        filterAreaName_list.add(json.getString("area"));
                    }


                    JSONArray jsonKey = jsonObject.getJSONArray("keywords");
                    filterKeyId_list.clear();
                    filterKeyName_list.clear();
                    filterKeyId_list.add("");
                    filterKeyName_list.add("Keyword");
                    for (int i = 0; i < jsonKey.length(); i++) {
                        JSONObject json = jsonKey.getJSONObject(i);
                        filterKeyId_list.add(json.getString("keywords_id"));
                        filterKeyName_list.add(json.getString("category_keyword"));
                    }

                    JSONArray jsonCategory = jsonObject.getJSONArray("categories");
                    filterCategoryName_list.clear();
                    filterCategoryId_list.clear();
                    filterCategoryId_list.add("");
                    filterCategoryName_list.add("Event Category");
                    for (int i = 0; i < jsonCategory.length(); i++) {
                        JSONObject json = jsonCategory.getJSONObject(i);
                        filterCategoryId_list.add(json.getString("event_category_id"));
                        filterCategoryName_list.add(json.getString("event_category_name"));
                    }

                    JSONArray jsonTicketcost = jsonObject.getJSONArray("cost");
                    ticketCost_list.clear();
                    ticketCostId_list.clear();
                    ticketCostId_list.add("");
                    ticketCost_list.add("Ticket Cost Range");
                    for (int i = 0; i < jsonTicketcost.length(); i++) {
                        JSONObject json = jsonTicketcost.getJSONObject(i);
                        ticketCostId_list.add(json.getString("event_tickets_id"));
                        ticketCost_list.add(json.getString("event_tickets_name"));
                    }
                    adapterArea = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterAreaName_list);
                    adapterKey = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterKeyName_list);
                    adapterTicketcost = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, ticketCost_list);
                    adapterCategory= new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterCategoryName_list);

                    spinnerArea.setAdapter(adapterArea);

                    spinnerKey.setAdapter(adapterKey);
                    spinnerCategory.setAdapter(adapterCategory);
                    spinnerticketCost.setAdapter(adapterTicketcost);

                    LogUtility.Log(TAG, "onGetFilterResponse: :success", null);
                }
                else if(category_item.equals("Spa")){
                    spinnerKey.setVisibility(View.VISIBLE);
                    spinnerArea.setVisibility(View.VISIBLE);
                    spinnerSpaspeciality.setVisibility(View.VISIBLE);
                    spaTime.setVisibility(View.VISIBLE);


                    JSONObject jsonObject = object.getJSONObject("result");
                    JSONArray jsonArea = jsonObject.getJSONArray("areas");
                    filterAreaId_list.clear();
                    filterAreaName_list.clear();
                    filterAreaId_list.add("");
                    filterAreaName_list.add("Area");
                    for (int i = 0; i < jsonArea.length(); i++) {
                        JSONObject json = jsonArea.getJSONObject(i);
                        filterAreaId_list.add(json.getString("area_id"));
                        filterAreaName_list.add(json.getString("area"));
                    }


                    JSONArray jsonKey = jsonObject.getJSONArray("keywords");
                    filterKeyId_list.clear();
                    filterKeyName_list.clear();
                    filterKeyId_list.add("");
                    filterKeyName_list.add("Keyword");
                    for (int i = 0; i < jsonKey.length(); i++) {
                        JSONObject json = jsonKey.getJSONObject(i);
                        filterKeyId_list.add(json.getString("keywords_id"));
                        filterKeyName_list.add(json.getString("category_keyword"));
                    }
                    JSONArray jsonSpaspeciality = jsonObject.getJSONArray("specialities");
                    spaSpeciality_list.clear();
                    spaSpecialityId_list.clear();
                    spaSpecialityId_list.add("");
                    spaSpeciality_list.add("Specialities");
                    for (int i = 0; i < jsonSpaspeciality.length(); i++) {
                        JSONObject json = jsonSpaspeciality.getJSONObject(i);
                        spaSpecialityId_list.add(json.getString("spa_types_id"));
                        spaSpeciality_list.add(json.getString("title"));
                    }

                    adapterArea = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterAreaName_list);
                    adapterKey = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterKeyName_list);
                    adapterSpaspeciality= new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, spaSpeciality_list);

                    spinnerArea.setAdapter(adapterArea);

                    spinnerKey.setAdapter(adapterKey);
                    spinnerSpaspeciality.setAdapter(adapterSpaspeciality);


                    LogUtility.Log(TAG, "onGetFilterResponse: :success", null);
                }
                else if(category_item.equals("Tour & Travel")){
                    spinnerKey.setVisibility(View.VISIBLE);
                    spinnerArea.setVisibility(View.VISIBLE);


                    JSONObject jsonObject = object.getJSONObject("result");
                    JSONArray jsonArea = jsonObject.getJSONArray("areas");
                    filterAreaId_list.clear();
                    filterAreaName_list.clear();
                    filterAreaId_list.add("");
                    filterAreaName_list.add("Area");
                    for (int i = 0; i < jsonArea.length(); i++) {
                        JSONObject json = jsonArea.getJSONObject(i);
                        filterAreaId_list.add(json.getString("area_id"));
                        filterAreaName_list.add(json.getString("area"));
                    }


                    JSONArray jsonKey = jsonObject.getJSONArray("keywords");
                    filterKeyId_list.clear();
                    filterKeyName_list.clear();
                    filterKeyId_list.add("");
                    filterKeyName_list.add("Keyword");
                    for (int i = 0; i < jsonKey.length(); i++) {
                        JSONObject json = jsonKey.getJSONObject(i);
                        filterKeyId_list.add(json.getString("keywords_id"));
                        filterKeyName_list.add(json.getString("category_keyword"));
                    }

                    adapterArea = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterAreaName_list);
                    adapterKey = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, filterKeyName_list);

                    spinnerArea.setAdapter(adapterArea);

                    spinnerKey.setAdapter(adapterKey);


                    LogUtility.Log(TAG, "onGetFilterResponse: :success", null);
                }
            }catch(JSONException ee){
                ee.printStackTrace();
            }

        } else {
            LogUtility.Log(TAG, "onGetFilterResponse: :failure", null);

        }


    }
    public void postJsonRequest(int request_type, String url, JSONObject jsonObject, final int request_id) {

        mNetworkManager.postJsonRequest(request_type, url, jsonObject, request_id);
    }
    public JSONObject getjsonObject(String tag) {
        if (tag.equals(TravexApplication.GET_FILTERS_TAG)) {

            return getJson(mListCreatorForNameValuePairRequest
                            .getListOfSearchApiGetFilters(TravexApplication.GET_FILTERS_CITY_ID,
                                    TravexApplication.GET_FILTERS_CATEGORY_ID
                            ),
                    mListCreatorForNameValuePairRequest.getListOfSearchApiGetFilters(country_id, category_item));


        }
        else if(tag.equals(TravexApplication.FILTERRESULTS_TAG)){

            if (category_item.equals("Airline"))

            {
                String terminal_selected="",destination_selected="",airport_selected="";
                try {
//                    terminal_selected = spinnerTerminal.getSelectedItem().toString();
                    int pos=spinnerTerminal.getSelectedItemPosition();
                    terminal_selected = terminalId_list.get(pos);

                }catch(Exception e){

                }
                try{
                 destination_selected=  spinnerDestination.getSelectedItem().toString();
                }catch(Exception e){

                }
                try{
                    int pos=spinnerAirport.getSelectedItemPosition();
//                    airport_selected =  spinnerAirport.getSelectedItem().toString();
                    airport_selected =  airportId_list.get(pos);
                }catch(Exception e){

                }

                LogUtility.Log(TAG, "category item selected is airline:getjsonObject", null);

                return getJson(mListCreatorForNameValuePairRequest
                                .getListOfSearchApiGetFilterResults_Airline_Shopping_Business_Events_Spa(
                                        TravexApplication.FILTER_RESULTS_CITY_ID,
                                        TravexApplication.FILTER_RESULTS_SEARCH_TERM,
                                        TravexApplication.FILTER_RESULTS_AREA,
                                        TravexApplication.FILTER_RESULTS_LIMIT,
                                        TravexApplication.FILTER_RESULTS_ALPHABET,
                                        TravexApplication.FILTER_RESULTS_START,
                                        TravexApplication.FILTER_RESULTS_SETFILTER,
                                        TravexApplication.FILTER_RESULTS_FILTER_KEYWORD,
                                        TravexApplication.FILTER_RESULTS_FILTER_RATING,
                                        TravexApplication.FILTER_RESULTS_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_AIRLINE_TERMINAL,
                                        TravexApplication.FILTER_RESULTS_FILTER_AIRLINE_AIRPORT_,
                                        TravexApplication.FILTER_RESULTS_FILTER_AIRLINE_DESTINATION


                                ),
                        mListCreatorForNameValuePairRequest.getListOfSearchApiGetFilterResults_Airline_Shopping_Business_Events_Spa(country_id, "", "", "15", "", "0", "1", "", "", category_item,terminal_selected,airport_selected,destination_selected));
            }
            else if (category_item.equals("Shopping"))
            {
                String KeyWordId="",AreaId="",Shoppinglocid="",categoryid="",rating_str="";

                try {
                    int pos=spinnerKey.getSelectedItemPosition();
                    KeyWordId = filterKeyId_list.get(pos);
                }
                catch(Exception ex)
                {

                }

                try {
                        int pos=spinnerArea.getSelectedItemPosition();
                        AreaId = filterAreaId_list.get(pos);
                }
                catch(Exception ex)
                {

                }
                try {
                    int pos=spinnerShoplocation.getSelectedItemPosition();
                    Shoppinglocid = shopLocationId_list.get(pos);
                }
                catch(Exception ex)
                {

                }
                try {
                    int pos=spinnerCategory.getSelectedItemPosition();
                    categoryid = filterCategoryId_list.get(pos);
                }
                catch(Exception ex)
                {

                }

                if(rating==0)
                {
                    rating_str = String.valueOf(rating);
                }

                LogUtility.Log(TAG, "category item selected is shopping:getjsonObject", null);

                return getJson(mListCreatorForNameValuePairRequest
                                .getListOfSearchApiGetFilterResults_Airline_Shopping_Business_Events_Spa(
                                        TravexApplication.FILTER_RESULTS_CITY_ID,
                                        TravexApplication.FILTER_RESULTS_SEARCH_TERM,
                                        TravexApplication.FILTER_RESULTS_AREA,
                                        TravexApplication.FILTER_RESULTS_LIMIT,
                                        TravexApplication.FILTER_RESULTS_ALPHABET,
                                        TravexApplication.FILTER_RESULTS_START,
                                        TravexApplication.FILTER_RESULTS_SETFILTER,
                                        TravexApplication.FILTER_RESULTS_FILTER_KEYWORD,
                                        TravexApplication.FILTER_RESULTS_FILTER_RATING,
                                        TravexApplication.FILTER_RESULTS_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_SHOPPING_AREA,
                                        TravexApplication.FILTER_RESULTS_FILTER_SHOPPING_LOCATION,
                                        TravexApplication.FILTER_RESULTS_FILTER_SHOPPING_CATEGORY


                                ),
                        mListCreatorForNameValuePairRequest.getListOfSearchApiGetFilterResults_Airline_Shopping_Business_Events_Spa(country_id, "", "", "", "", "", "1", KeyWordId,rating_str , category_item, AreaId, Shoppinglocid, categoryid));
            }
            else if (category_item.equals("Business"))
            {
                String AreaId="",categoryid="",Keyid="",rating_str="";
                try {
                    int pos=spinnerArea.getSelectedItemPosition();
                    AreaId = filterAreaId_list.get(pos);
                }
                catch(Exception ex)
                {

                }

                try {
                    int pos=spinnerCategory.getSelectedItemPosition();
                    categoryid = filterCategoryId_list.get(pos);
                }
                catch(Exception ex)
                {

                }
                try {
                    int pos=spinnerKey.getSelectedItemPosition();
                    Keyid = filterKeyId_list.get(pos);
                }
                catch(Exception ex)
                {

                }

                if(rating==0)
                {
                    rating_str = String.valueOf(rating);
                }
//                LogUtility.Log(TAG, "category item selected is business:getjsonObject", null);

                return getJson(mListCreatorForNameValuePairRequest
                                .getListOfSearchApiGetFilterResults_Airline_Shopping_Business_Events_Spa(
                                        TravexApplication.FILTER_RESULTS_CITY_ID,
                                        TravexApplication.FILTER_RESULTS_SEARCH_TERM,
                                        TravexApplication.FILTER_RESULTS_AREA,
                                        TravexApplication.FILTER_RESULTS_LIMIT,
                                        TravexApplication.FILTER_RESULTS_ALPHABET,
                                        TravexApplication.FILTER_RESULTS_START,
                                        TravexApplication.FILTER_RESULTS_SETFILTER,
                                        TravexApplication.FILTER_RESULTS_FILTER_KEYWORD,
                                        TravexApplication.FILTER_RESULTS_FILTER_RATING,
                                        TravexApplication.FILTER_RESULTS_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_BUSINESS_AREA,
                                        TravexApplication.FILTER_RESULTS_FILTER_BUSINESS_LOCATION,
                                        TravexApplication.FILTER_RESULTS_FILTER_BUSINESS_CATEGORY


                                ),
                        mListCreatorForNameValuePairRequest.getListOfSearchApiGetFilterResults_Airline_Shopping_Business_Events_Spa(country_id, "", "", "", "", "", "1", Keyid, rating_str, category_item, AreaId, "", categoryid));
            }
            else if (category_item.equals("Events"))

            {

//                LogUtility.Log(TAG, "category item selected is events:getjsonObject", null);
                String AreaId="",categoryid="",Keyid="";
                try {
                    int pos=spinnerArea.getSelectedItemPosition();
                    AreaId = filterAreaId_list.get(pos);
                }
                catch(Exception ex)
                {

                }

                try {
                    int pos=spinnerCategory.getSelectedItemPosition();
                    categoryid = filterCategoryId_list.get(pos);
                }
                catch(Exception ex)
                {

                }
                try {
                    int pos=spinnerKey.getSelectedItemPosition();
                    Keyid = filterKeyId_list.get(pos);
                }
                catch(Exception ex)
                {

                }

                return getJson(mListCreatorForNameValuePairRequest
                                .getListOfSearchApiGetFilterResults_Airline_Shopping_Business_Events_Spa(
                                        TravexApplication.FILTER_RESULTS_CITY_ID,
                                        TravexApplication.FILTER_RESULTS_SEARCH_TERM,
                                        TravexApplication.FILTER_RESULTS_AREA,
                                        TravexApplication.FILTER_RESULTS_LIMIT,
                                        TravexApplication.FILTER_RESULTS_ALPHABET,
                                        TravexApplication.FILTER_RESULTS_START,
                                        TravexApplication.FILTER_RESULTS_SETFILTER,
                                        TravexApplication.FILTER_RESULTS_FILTER_KEYWORD,
                                        TravexApplication.FILTER_RESULTS_FILTER_RATING,
                                        TravexApplication.FILTER_RESULTS_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_EVENTS_AREA,
                                        TravexApplication.FILTER_RESULTS_FILTER_EVENTS_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_EVENTS_TICKET_COST


                                ),
                        mListCreatorForNameValuePairRequest.getListOfSearchApiGetFilterResults_Airline_Shopping_Business_Events_Spa(country_id, "", "", "", "", "", "1", Keyid, String.valueOf(rating), category_item, AreaId, categoryid,""));//, spinnerticketCost.getSelectedItem().toString()));
            }
            else if (category_item.equals("Spa"))
            {

//                LogUtility.Log(TAG, "category item selected is spa:getjsonObject", null);

                String AreaId="",spaspecialtyid="";
                try {
                    int pos=spinnerArea.getSelectedItemPosition();
                    AreaId = filterAreaId_list.get(pos);
                }
                catch(Exception ex)
                {

                }

                try {
                    int pos=spinnerSpaspeciality.getSelectedItemPosition();
                    spaspecialtyid = spaSpecialityId_list.get(pos);
                }
                catch(Exception ex)
                {

                }

                return getJson(mListCreatorForNameValuePairRequest
                                .getListOfSearchApiGetFilterResults_Airline_Shopping_Business_Events_Spa(
                                        TravexApplication.FILTER_RESULTS_CITY_ID,
                                        TravexApplication.FILTER_RESULTS_SEARCH_TERM,
                                        TravexApplication.FILTER_RESULTS_AREA,
                                        TravexApplication.FILTER_RESULTS_LIMIT,
                                        TravexApplication.FILTER_RESULTS_ALPHABET,
                                        TravexApplication.FILTER_RESULTS_START,
                                        TravexApplication.FILTER_RESULTS_SETFILTER,
                                        TravexApplication.FILTER_RESULTS_FILTER_KEYWORD,
                                        TravexApplication.FILTER_RESULTS_FILTER_RATING,
                                        TravexApplication.FILTER_RESULTS_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_SPA_AREA,
                                        TravexApplication.FILTER_RESULTS_FILTER_SPA_SPECIALITIES,
                                        TravexApplication.FILTER_RESULTS_FILTER_SPA_TIMINGS


                                ),
                        mListCreatorForNameValuePairRequest.getListOfSearchApiGetFilterResults_Airline_Shopping_Business_Events_Spa(country_id, "", "", "", "", "", "1", "", "", category_item, AreaId, spaspecialtyid, spaTime.getText().toString()));
            }
            else if (category_item.equals("Things To Do"))
            {

//                LogUtility.Log(TAG, "category item selected is things to do:getjsonObject", null);

                String AreaId="",categoryid="",Keyid="";
                try {
                    int pos=spinnerArea.getSelectedItemPosition();
                    AreaId = filterAreaId_list.get(pos);
                }
                catch(Exception ex)
                {

                }

                try {
                    int pos=spinnerCategory.getSelectedItemPosition();
                    categoryid = filterCategoryId_list.get(pos);
                }
                catch(Exception ex)
                {

                }
                try {
                    int pos=spinnerKey.getSelectedItemPosition();
                    Keyid = filterKeyId_list.get(pos);
                }
                catch(Exception ex)
                {

                }

                return getJson(mListCreatorForNameValuePairRequest
                                .getListOfSearchApiGetFilterResults_moneyExchange_carRentals_Emergencies_Embasies_ThingsToDo(
                                        TravexApplication.FILTER_RESULTS_CITY_ID,
                                        TravexApplication.FILTER_RESULTS_SEARCH_TERM,
                                        TravexApplication.FILTER_RESULTS_AREA,
                                        TravexApplication.FILTER_RESULTS_LIMIT,
                                        TravexApplication.FILTER_RESULTS_ALPHABET,
                                        TravexApplication.FILTER_RESULTS_START,
                                        TravexApplication.FILTER_RESULTS_SETFILTER,
                                        TravexApplication.FILTER_RESULTS_FILTER_KEYWORD,
                                        TravexApplication.FILTER_RESULTS_FILTER_RATING,
                                        TravexApplication.FILTER_RESULTS_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_THINGS_TO_DO_AREA,
                                        TravexApplication.FILTER_RESULTS_FILTER_THINGS_TO_DO_CATEGORY


                                ),
                        mListCreatorForNameValuePairRequest.getListOfSearchApiGetFilterResults_moneyExchange_carRentals_Emergencies_Embasies_ThingsToDo(country_id, "", "", "", "1", Keyid, String.valueOf(rating), category_item, AreaId, category_item, "",categoryid));
            }
            else if (category_item.equals("Money Exchange"))
            {

//                LogUtility.Log(TAG, "category item selected is money exchange:getjsonObject", null);
                String AreaId="",Keyid="";
                try {
                    int pos=spinnerArea.getSelectedItemPosition();
                    AreaId = filterAreaId_list.get(pos);
                }
                catch(Exception ex)
                {

                }
                try {
                    int pos=spinnerKey.getSelectedItemPosition();
                    Keyid = filterKeyId_list.get(pos);
                }
                catch(Exception ex)
                {

                }
                return getJson(mListCreatorForNameValuePairRequest
                                .getListOfSearchApiGetFilterResults_moneyExchange_carRentals_Emergencies_Embasies_ThingsToDo(
                                        TravexApplication.FILTER_RESULTS_CITY_ID,
                                        TravexApplication.FILTER_RESULTS_SEARCH_TERM,
                                        TravexApplication.FILTER_RESULTS_AREA,
                                        TravexApplication.FILTER_RESULTS_LIMIT,
                                        TravexApplication.FILTER_RESULTS_ALPHABET,
                                        TravexApplication.FILTER_RESULTS_START,
                                        TravexApplication.FILTER_RESULTS_SETFILTER,
                                        TravexApplication.FILTER_RESULTS_FILTER_KEYWORD,
                                        TravexApplication.FILTER_RESULTS_FILTER_RATING,
                                        TravexApplication.FILTER_RESULTS_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_MONEY_EXCHANGE_AREA,
                                        TravexApplication.FILTER_RESULTS_FILTER_MONEY_EXCHANGE_TIME


                                ),
                        mListCreatorForNameValuePairRequest.getListOfSearchApiGetFilterResults_moneyExchange_carRentals_Emergencies_Embasies_ThingsToDo(country_id, "", "", "15", "", "", "1", Keyid, String.valueOf(rating), category_item, AreaId, moneyTime.getText().toString()));
            } else if (category_item.equals("Car Rentals"))

            {

//                LogUtility.Log(TAG, "category item selected is car rentals:getjsonObject", null);
                String AreaId="",Keyid="";
                try {
                    int pos=spinnerArea.getSelectedItemPosition();
                    AreaId = filterAreaId_list.get(pos);
                }
                catch(Exception ex)
                {

                }
                try {
                    int pos=spinnerKey.getSelectedItemPosition();
                    Keyid = filterKeyId_list.get(pos);
                }
                catch(Exception ex)
                {

                }
                return getJson(mListCreatorForNameValuePairRequest
                                .getListOfSearchApiGetFilterResults_moneyExchange_carRentals_Emergencies_Embasies_ThingsToDo(
                                        TravexApplication.FILTER_RESULTS_CITY_ID,
                                        TravexApplication.FILTER_RESULTS_SEARCH_TERM,
                                        TravexApplication.FILTER_RESULTS_AREA,
                                        TravexApplication.FILTER_RESULTS_LIMIT,
                                        TravexApplication.FILTER_RESULTS_ALPHABET,
                                        TravexApplication.FILTER_RESULTS_START,
                                        TravexApplication.FILTER_RESULTS_SETFILTER,
                                        TravexApplication.FILTER_RESULTS_FILTER_KEYWORD,
                                        TravexApplication.FILTER_RESULTS_FILTER_RATING,
                                        TravexApplication.FILTER_RESULTS_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_CAR_RENTALS_AREA,
                                        TravexApplication.FILTER_RESULTS_FILTER_CAR_RENTALS_TIME


                                ),
                        mListCreatorForNameValuePairRequest.getListOfSearchApiGetFilterResults_moneyExchange_carRentals_Emergencies_Embasies_ThingsToDo(country_id, "", "15", "", "", "", "1", Keyid, String.valueOf(rating), category_item,AreaId, ""));
            }
            else if (category_item.equals("Emergencies"))
            {

//                LogUtility.Log(TAG, "category item selected is emergencies:getjsonObject", null);
                String AreaId="",categoryid="",Keyid="";
                try {
                    int pos=spinnerArea.getSelectedItemPosition();
                    AreaId = filterAreaId_list.get(pos);
                }
                catch(Exception ex)
                {

                }

                try {
                    int pos=spinnerCategory.getSelectedItemPosition();
                    categoryid = filterCategoryId_list.get(pos);
                }
                catch(Exception ex)
                {

                }
                try {
                    int pos=spinnerKey.getSelectedItemPosition();
                    Keyid = filterKeyId_list.get(pos);
                }
                catch(Exception ex)
                {

                }
                return getJson(mListCreatorForNameValuePairRequest
                                .getListOfSearchApiGetFilterResults_moneyExchange_carRentals_Emergencies_Embasies_ThingsToDo(
                                        TravexApplication.FILTER_RESULTS_CITY_ID,
                                        TravexApplication.FILTER_RESULTS_SEARCH_TERM,
                                        TravexApplication.FILTER_RESULTS_AREA,
                                        TravexApplication.FILTER_RESULTS_LIMIT,
                                        TravexApplication.FILTER_RESULTS_ALPHABET,
                                        TravexApplication.FILTER_RESULTS_START,
                                        TravexApplication.FILTER_RESULTS_SETFILTER,
                                        TravexApplication.FILTER_RESULTS_FILTER_KEYWORD,
                                        TravexApplication.FILTER_RESULTS_FILTER_RATING,
                                        TravexApplication.FILTER_RESULTS_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_EMERGENCIES_AREA,
                                        TravexApplication.FILTER_RESULTS_FILTER_EMERGENCIES_CATEGORY


                                ),
                        mListCreatorForNameValuePairRequest.getListOfSearchApiGetFilterResults_moneyExchange_carRentals_Emergencies_Embasies_ThingsToDo(country_id, "", "", "15", "", "", "1", Keyid, String.valueOf(rating), category_item, AreaId,categoryid));
            }
            else if (category_item.equals("Embasies & Consulates"))
            {

                LogUtility.Log(TAG, "category item selected is embasies and consulates:getjsonObject", null);

                return getJson(mListCreatorForNameValuePairRequest
                                .getListOfSearchApiGetFilterResults_moneyExchange_carRentals_Emergencies_Embasies_ThingsToDo(
                                        TravexApplication.FILTER_RESULTS_CITY_ID,
                                        TravexApplication.FILTER_RESULTS_SEARCH_TERM,
                                        TravexApplication.FILTER_RESULTS_AREA,
                                        TravexApplication.FILTER_RESULTS_LIMIT,
                                        TravexApplication.FILTER_RESULTS_ALPHABET,
                                        TravexApplication.FILTER_RESULTS_START,
                                        TravexApplication.FILTER_RESULTS_SETFILTER,
                                        TravexApplication.FILTER_RESULTS_FILTER_KEYWORD,
                                        TravexApplication.FILTER_RESULTS_FILTER_RATING,
                                        TravexApplication.FILTER_RESULTS_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_EMBASIESCONSULATES_AREA,
                                        TravexApplication.FILTER_RESULTS_FILTER_EMBASIESCONSULATES_CONTINENT


                                ),
                        mListCreatorForNameValuePairRequest.
                                getListOfSearchApiGetFilterResults_moneyExchange_carRentals_Emergencies_Embasies_ThingsToDo
                                        (country_id, "", "", "", "", "", "1", "",String.valueOf(rating), category_item, "", spinnercontinent.getSelectedItem().toString())
                );
            }
            else if (category_item.equals("Tour & Travel"))
            {

//                LogUtility.Log(TAG, "category item selected is  tour and travel:getjsonObject", null);

                String AreaId="";
                try {
                    int pos=spinnerArea.getSelectedItemPosition();
                    AreaId = filterAreaId_list.get(pos);
                }
                catch(Exception ex)
                {

                }
                return getJson(mListCreatorForNameValuePairRequest
                                .getListOfSearchApiGetFilterResults_TourAndTravel(
                                        TravexApplication.FILTER_RESULTS_CITY_ID,
                                        TravexApplication.FILTER_RESULTS_SEARCH_TERM,
                                        TravexApplication.FILTER_RESULTS_AREA,
                                        TravexApplication.FILTER_RESULTS_LIMIT,
                                        TravexApplication.FILTER_RESULTS_ALPHABET,
                                        TravexApplication.FILTER_RESULTS_START,
                                        TravexApplication.FILTER_RESULTS_SETFILTER,
                                        TravexApplication.FILTER_RESULTS_FILTER_KEYWORD,
                                        TravexApplication.FILTER_RESULTS_FILTER_RATING,
                                        TravexApplication.FILTER_RESULTS_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_TOUR_AND_TRAVEL_AREA


                                ),
                        mListCreatorForNameValuePairRequest.
                                getListOfSearchApiGetFilterResults_TourAndTravel(
                                        country_id, "", "", "", "", "", "1", "", "", category_item, AreaId)
                );
            }
            else if (category_item.equals("Real Estate"))
            {

//                LogUtility.Log(TAG, "category item selected is  real estatae:getjsonObject", null);
                String AreaId="",Keyid="",developerId="",purpose="",statusid="";
                try {
                    int pos=spinnerArea.getSelectedItemPosition();
                    AreaId = filterAreaId_list.get(pos);
                }
                catch(Exception ex)
                {

                }
                try {
                    int pos=spinnerKey.getSelectedItemPosition();
                    Keyid = filterKeyId_list.get(pos);
                }
                catch(Exception ex)
                {

                }
                try {
                    int pos=spinnerDeveloper.getSelectedItemPosition();
                    developerId = realestate_developerId_list.get(pos);
                }
                catch(Exception ex)
                {

                }
                try {
                    int pos=spinnerPurpose.getSelectedItemPosition();
                    purpose = realestate_project_purposeId_list.get(pos);
                }
                catch(Exception ex)
                {

                }
                try {
                    int pos=spinnerStatus.getSelectedItemPosition();
                    statusid = realestate_project_statusId_list.get(pos);
                }
                catch(Exception ex)
                {

                }

                return getJson(mListCreatorForNameValuePairRequest
                                .getListOfSearchApiGetFilterResults_RealEstate_NightLife(
                                        TravexApplication.FILTER_RESULTS_CITY_ID,
                                        TravexApplication.FILTER_RESULTS_SEARCH_TERM,
                                        TravexApplication.FILTER_RESULTS_AREA,
                                        TravexApplication.FILTER_RESULTS_LIMIT,
                                        TravexApplication.FILTER_RESULTS_ALPHABET,
                                        TravexApplication.FILTER_RESULTS_START,
                                        TravexApplication.FILTER_RESULTS_SETFILTER,
                                        TravexApplication.FILTER_RESULTS_FILTER_KEYWORD,
                                        TravexApplication.FILTER_RESULTS_FILTER_RATING,
                                        TravexApplication.FILTER_RESULTS_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_REAL_ESTATE_AREA,
                                        TravexApplication.FILTER_RESULTS_FILTER_REAL_ESTATE_DEVELOPER,
                                        TravexApplication.FILTER_RESULTS_FILTER_REAL_ESTATE_PURPOSE,
                                        TravexApplication.FILTER_RESULTS_FILTER_REAL_ESTATE_STATUS


                                ),
                        mListCreatorForNameValuePairRequest.getListOfSearchApiGetFilterResults_RealEstate_NightLife(country_id, "", "", "", "", "", "1", Keyid,String.valueOf(rating), category_item, AreaId, developerId,purpose, statusid));
            }
            else if (category_item.equals("Nightlife"))
            {

//                LogUtility.Log(TAG, "category item selected is  nightlife:getjsonObject", null);

                String AreaId="",nightspid="",nightentryid="";
                try {
                    int pos=spinnerArea.getSelectedItemPosition();
                    AreaId = filterAreaId_list.get(pos);
                }
                catch(Exception ex)
                {

                }
                try {
                    int pos=spinnerNightspecialties.getSelectedItemPosition();
                    nightspid = nightSpecialtiesid_list.get(pos);
                }
                catch(Exception ex)
                {

                }
                try {
                    int pos=spinnerNightentrytype.getSelectedItemPosition();
                    nightentryid = nightEntrytypeId_list.get(pos);
                }
                catch(Exception ex)
                {

                }




                return getJson(mListCreatorForNameValuePairRequest
                                .getListOfSearchApiGetFilterResults_RealEstate_NightLife(
                                        TravexApplication.FILTER_RESULTS_CITY_ID,
                                        TravexApplication.FILTER_RESULTS_SEARCH_TERM,
                                        TravexApplication.FILTER_RESULTS_AREA,
                                        TravexApplication.FILTER_RESULTS_LIMIT,
                                        TravexApplication.FILTER_RESULTS_ALPHABET,
                                        TravexApplication.FILTER_RESULTS_START,
                                        TravexApplication.FILTER_RESULTS_SETFILTER,
                                        TravexApplication.FILTER_RESULTS_FILTER_KEYWORD,
                                        TravexApplication.FILTER_RESULTS_FILTER_RATING,
                                        TravexApplication.FILTER_RESULTS_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_NIGHTLIFE_AREA,
                                        TravexApplication.FILTER_RESULTS_FILTER_NIGHTLIFE_SPECIALITIES,
                                        TravexApplication.FILTER_RESULTS_FILTER_NIGHTLIFE_POLICY,

                                        TravexApplication.FILTER_RESULTS_FILTER_NIGHTLIFE_TIMINGS


                                ),
                        mListCreatorForNameValuePairRequest.getListOfSearchApiGetFilterResults_RealEstate_NightLife(country_id, "", "", "", "", "", "1", "",String.valueOf(rating), category_item, AreaId,nightspid, nightentryid, ""));
            }
            else if (category_item.equals("Stay/Hotels"))
            {

//                LogUtility.Log(TAG, "category item selected is  stay/hotels:getjsonObject", null);
                String AreaId="",freebiesid="",aminitiesid="";
                try {
                    int pos=spinnerArea.getSelectedItemPosition();
                    AreaId = filterAreaId_list.get(pos);
                }
                catch(Exception ex)
                {

                }
                try {
                    int pos=spinnerFeebies.getSelectedItemPosition();
                    freebiesid = stay_freebiesid_list.get(pos);
                }
                catch(Exception ex)
                {

                }
                try {
                    int pos=spinneraminities.getSelectedItemPosition();
                    aminitiesid = stay_aminitiesid_list.get(pos);
                }
                catch(Exception ex)
                {

                }



                return getJson(mListCreatorForNameValuePairRequest
                                .getListOfSearchApiGetFilterResults_stayHotels(
                                        TravexApplication.FILTER_RESULTS_CITY_ID,
                                        TravexApplication.FILTER_RESULTS_SEARCH_TERM,
                                        TravexApplication.FILTER_RESULTS_AREA,
                                        TravexApplication.FILTER_RESULTS_LIMIT,
                                        TravexApplication.FILTER_RESULTS_ALPHABET,
                                        TravexApplication.FILTER_RESULTS_START,
                                        TravexApplication.FILTER_RESULTS_SETFILTER,
                                        TravexApplication.FILTER_RESULTS_FILTER_KEYWORD,
                                        TravexApplication.FILTER_RESULTS_FILTER_RATING,
                                        TravexApplication.FILTER_RESULTS_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_STAYHOTEL_AREA,
                                        TravexApplication.FILTER_RESULTS_FILTER_STAYHOTEL_STARATE,
                                        TravexApplication.FILTER_RESULTS_FILTER_STAYHOTEL_FREEBIES,

                                        TravexApplication.FILTER_RESULTS_FILTER_STAYHOTEL_AMINITIES,
                                        TravexApplication.FILTER_RESULTS_FILTER_STAYHOTEL_PRICE,
                                        TravexApplication.FILTER_RESULTS_FILTER_STAYHOTEL_TYPE


                                ),
                        mListCreatorForNameValuePairRequest.getListOfSearchApiGetFilterResults_stayHotels(country_id, "", "", "", "", "", "1", "", "", category_item, AreaId, String.valueOf(rating), freebiesid, aminitiesid, "",""));//,spinnerStayprice.getSelectedItem().toString()));
            } else if (category_item.equals("Eating Out"))

            {

                LogUtility.Log(TAG, "category item selected is  eating out:getjsonObject", null);

                return getJson(mListCreatorForNameValuePairRequest
                                .getListOfSearchApiGetFilterResults_EatingOut(
                                        TravexApplication.FILTER_RESULTS_CITY_ID,
                                        TravexApplication.FILTER_RESULTS_SEARCH_TERM,
                                        TravexApplication.FILTER_RESULTS_AREA,
                                        TravexApplication.FILTER_RESULTS_LIMIT,
                                        TravexApplication.FILTER_RESULTS_ALPHABET,
                                        TravexApplication.FILTER_RESULTS_START,
                                        TravexApplication.FILTER_RESULTS_SETFILTER,
                                        TravexApplication.FILTER_RESULTS_FILTER_KEYWORD,
                                        TravexApplication.FILTER_RESULTS_FILTER_RATING,
                                        TravexApplication.FILTER_RESULTS_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_EATING_OUT_AREA,
                                        TravexApplication.FILTER_RESULTS_FILTER_EATING_OUT_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_EATING_OUT_ESTABLISHMENT,

                                        TravexApplication.FILTER_RESULTS_FILTER_EATING_OUT_CUISINE,
                                        TravexApplication.FILTER_RESULTS_FILTER_EATING_OUT_PRICE,
                                        TravexApplication.FILTER_RESULTS_FILTER_EATING_OUT_HALAL,
                                        TravexApplication.FILTER_RESULTS_FILTER_EATING_OUT_TIMING


                                ),
                        mListCreatorForNameValuePairRequest.getListOfSearchApiGetFilterResults_EatingOut(country_id, "", "", "", "", "", "1", "", "", category_item, "", "", "", "", "", spinnerEatingHalal.getSelectedItem().toString(), eatingTime.getText().toString()));// spinnereatingCost.getSelectedItem().toString(), spinnerEatingHalal.getSelectedItem().toString(), eatingTime.getText().toString()
            }


        }
        else if (tag.equals(TravexApplication.GET_SEARCH_TERMINLAS_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfGetSearchTerminals_Destinations
                            (TravexApplication.GET_SEARCH_TERMINALS_AIRPORT_ID

                            ),
                    mListCreatorForNameValuePairRequest.getListOfGetSearchTerminals_Destinations(airport_id)

            );
        }
        else if (tag.equals(TravexApplication.GET_SEARCH_DESTINATION_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfGetSearchTerminals_Destinations
                            (TravexApplication.GET_SEARCH_DESTINATIONS_AIRPORT_ID

                            ),
                    mListCreatorForNameValuePairRequest.getListOfGetSearchTerminals_Destinations(airport_id)

            );
        }
        return null;
    }
    public JSONObject getJson(ArrayList<String> namePair, ArrayList<String> valuePair) {


        return mJsonObjectMaker.getJson(namePair, valuePair);
    }

    public String fetchCountryIdForCountry(String country) {
        String countryId = null;
        int index = 0;
        ArrayList<String> countrynames = mPreferenceManager.getSupportedCountryNames();
        index = countrynames.lastIndexOf(country);
        LogUtility.Log(TAG, "fetchCountryIdForCountry", "index:" + index);

        countryId = mPreferenceManager.getSupportedCountryId().get(index);
        LogUtility.Log(TAG, "fetchCountryIdForCountry", "countryId:" + countryId);


        return countryId;
    }
//    public void setCurrentTimeOnView() {
//
//
//
//
//        final Calendar c = Calendar.getInstance();
//        int hour = c.get(Calendar.HOUR_OF_DAY);
//        int minute = c.get(Calendar.MINUTE);
//
//        // set current time into textview
//
//
//        // set current time into timepicker
//        timePicker1.setCurrentHour(hour);
//        timePicker1.setCurrentMinute(minute);
//
//    }
//

    @Override
    public void onGetSearchTerminals(boolean status, HashMap<Integer, ArrayList<String>> terminal_title_and_id) {
        if (status) {
            LogUtility.Log(TAG, "onGetSearchTerminals:success", null);

            if (terminal_title_and_id != null) {
                spinnerTerminal.setVisibility(View.VISIBLE);


                for (int i = 0; i < 2; i++) {

                    ArrayList<String> list = terminal_title_and_id.get(i);

                    LogUtility.Log(TAG, "onGetSearchTerminals:success:list:", (i + 1));
                    if (list != null) {


                        for (int j = 0; j < list.size(); j++) {
                            if(i==0){
                                terminalName_list.add(list.get(j));
                            }
                            else if(i==1){
                                terminalId_list.add(list.get(j));
                            }
                            LogUtility.Log(TAG, "onGetSearchTerminals:success:list item:", list.get(j));


                        }
                    }


                }
            }

            adapterTerminal = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, terminalName_list);

            spinnerTerminal.setAdapter(adapterTerminal);


        } else {
            LogUtility.Log(TAG, "onGetSearchTerminals:failure", null);

        }
    }

    @Override
    public void onGetSearchDestinations(boolean value, ArrayList<String> airline_destinations) {
        if (value) {
            LogUtility.Log(TAG, "onGetSearchDestinations:success", null);
            try {
                if (airline_destinations != null) {
                    spinnerDestination.setVisibility(View.VISIBLE);

                    for (int i = 0; i < airline_destinations.size(); i++) {
                        LogUtility.Log(TAG, "onGetSearchDestinations:success:item:", airline_destinations.get(i));
                        destinationName_list.add(airline_destinations.get(i).toString());
                    }
                } else {
                    LogUtility.Log(TAG, "onGetSearchDestinations:success:list is null", null);

                }
            }catch(Exception e){
                e.printStackTrace();
            }


        } else {
            LogUtility.Log(TAG, "onGetSearchDestinations:failure", null);

        }
        adapterDestination = new ArrayAdapter<String>(activity, R.layout.filter_spinner_item, destinationName_list);

        spinnerDestination.setAdapter(adapterDestination);

    }

    private com.wdullaer.materialdatetimepicker.time.TimePickerDialog.OnTimeSetListener Eating_Time_callback =
            new com.wdullaer.materialdatetimepicker.time.TimePickerDialog.OnTimeSetListener() {

        @Override
        public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
            try {
                   String output = String.format("%02d:%02d:%02d", hourOfDay, minute,00);
                   eatingTime.setText(output);
            }
            catch (Exception ex)
            {

            }

        }
    };

    private com.wdullaer.materialdatetimepicker.time.TimePickerDialog.OnTimeSetListener Spa_Time_callback =
            new com.wdullaer.materialdatetimepicker.time.TimePickerDialog.OnTimeSetListener() {

                @Override
                public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
                    try {
                        String output = String.format("%02d:%02d:%02d", hourOfDay, minute,00);
                        spaTime.setText(output);
                    }
                    catch (Exception ex)
                    {

                    }

                }
            };

    private com.wdullaer.materialdatetimepicker.time.TimePickerDialog.OnTimeSetListener Money_Time_callback =
            new com.wdullaer.materialdatetimepicker.time.TimePickerDialog.OnTimeSetListener() {

                @Override
                public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
                    try {
                        String output = String.format("%02d:%02d:%02d", hourOfDay, minute,00);
                        moneyTime.setText(output);
                    }
                    catch (Exception ex)
                    {

                    }

                }
            };

}
