package org.technomobs.travex.Activity.Fragment;


import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.cache.common.SimpleCacheKey;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.soundcloud.android.crop.Crop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.technomobs.travex.Activity.AfterloginKnowncity;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Controller.Interface.AboutUsResponse;
import org.technomobs.travex.Controller.Interface.GetMemberDetailsResponse;
import org.technomobs.travex.Controller.Interface.GetMemberUpdateProfile;
import org.technomobs.travex.Controller.Interface.ProfileImageUpdateResponse;
import org.technomobs.travex.Controller.JsonObjectMaker;
import org.technomobs.travex.Controller.NetworkManager;
import org.technomobs.travex.Controller.NetworkOptions;
import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.Common;
import org.technomobs.travex.Utillity.ListCreatorForNameValuePairRequest;
import org.technomobs.travex.Utillity.LogUtility;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

/**
 * Created by Abhi on 07-03-2016.
 */
public class ProfileFragmentTwo extends Fragment implements GetMemberDetailsResponse,Animation.AnimationListener,GetMemberUpdateProfile ,ProfileImageUpdateResponse {
    static Activity activity;
    ArrayList prgmName;
    ArrayList<String> tipsname_list;
    ArrayList<String> prgmNameList;
    ArrayList<String> updateanswerMain_list = new ArrayList<>();
    ArrayList<String> updatequestionMain_list = new ArrayList<>();
    ArrayList<String> updatetypeMain_list = new ArrayList<>();
    TextView abouttext,aboutusdesc;
    GridView gridview;
    TextView tv_gender,tv_children,tv_status;
    EditText tv_mail,tv_profile_name;
    String member_gender="",member_last_name="",member_first_name="",member_no_kids="",member_martial_status="",member_profile_image,member_email="",member_id;
    ArrayList<ArrayList> tips_MainArray = new ArrayList<ArrayList>();
    ArrayList<String> question_list = new ArrayList<String>();
    ArrayList<String> type_list = new ArrayList<String>();
    ArrayList<String> profilequestions_id_list = new ArrayList<String>();
    public static ArrayList<ArrayList> selected_answer_id_list = new ArrayList<>();
    public static ArrayList<ArrayList> selected_answer_list = new ArrayList<>();
    LinearLayout interested_layout;
    ArrayList<ArrayList> answerid_MainArray,answer_MainArray;
    int question_position=0;
    public static String selected_answer="";
    //API Credentials
    String Type="";
    String Image_File_Path="";

    static NetworkManager mNetworkManager = null;
    public static final String TAG = TravelEssential.class.getSimpleName();
    JsonObjectMaker mJsonObjectMaker = null;
    ListCreatorForNameValuePairRequest mListCreatorForNameValuePairRequest = null;
    ImageView fab;
    PreferenceManager mPreferenceManager;
    RelativeLayout animation_layout;
    View rootView;
    SimpleDraweeView profile_Image;
    Fragment fragment=null;
    Animation rotateOne, rotateTwo, rotateThree;
    AlertDialog.Builder dialogBuilder=null;
    AlertDialog alertDialog=null;

    ArrayList<String> Gender_List = new ArrayList<>();
    ArrayList<String> Children_List = new ArrayList<>();
    ArrayList<String> Martial_Status_List = new ArrayList<>();

    //   public static String [] prgmNameList={"Let Us C","c++","JAVA","Jsp","Microsoft .Net","Android","PHP","Jquery","JavaScript"};
    //   public static int [] prgmImages={R.drawable.pic1,R.drawable.pic2,R.drawable.pic3,R.drawable.pic4,R.drawable.pic5,R.drawable.pic6,R.drawable.pic2,R.drawable.pic4,R.drawable.pic1,R.drawable.pic3,R.drawable.pic4,R.drawable.pic5,R.drawable.pic6,R.drawable.pic2,R.drawable.pic4,R.drawable.pic1};

    public static ProfileFragmentTwo newInstance(Activity con){
        ProfileFragmentTwo fragment = new ProfileFragmentTwo();
        activity=con;

        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        rootView = inflater.inflate(R.layout.profile_fragment_two, container, false);
        mPreferenceManager = new PreferenceManager(activity);
        Gender_List = new ArrayList<>();
        Children_List = new ArrayList<>();
        Martial_Status_List = new ArrayList<>();

        Gender_List.add("Male");
        Gender_List.add("Female");
        Martial_Status_List.add("Single");
        Martial_Status_List.add("Married");
        for(int i=0;i<10;i++)
        {
            Children_List.add( String.valueOf(i));
        }




        interested_layout=(LinearLayout)rootView.findViewById(R.id.interested_layout);
        animation_layout=(RelativeLayout)rootView.findViewById(R.id.settings_animation);
        tv_gender=(TextView)rootView.findViewById(R.id.tv_gender);
        tv_children=(TextView)rootView.findViewById(R.id.tv_children);
        tv_status=(TextView)rootView.findViewById(R.id.tv_status);

        tv_gender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayAlertDialog("Gender");
            }
        });
        tv_children.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayAlertDialog("Kids");
            }
        });
        tv_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayAlertDialog("Martial");
            }
        });


        tv_mail=(EditText)rootView.findViewById(R.id.tv_email);
        tv_profile_name=(EditText)rootView.findViewById(R.id.profile_name);

        fab=(ImageView)rootView.findViewById(R.id. fab);
//        tv_profile_name.setText(mPreferenceManager.getRegisteredUser_name());
        profile_Image=(SimpleDraweeView)rootView.findViewById(R.id. profile_Image);
        profile_Image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EasyImage.openChooserWithGallery(ProfileFragmentTwo.this, "Select", 0);
            }
        });

        ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.user).build();
        RoundingParams roundingParams = RoundingParams.fromCornersRadius(5f);
        roundingParams.setBorder(Color.WHITE, (float) 1.0);
        roundingParams.setRoundAsCircle(true);
        profile_Image.getHierarchy().setRoundingParams(roundingParams);
        profile_Image.setImageURI(imageRequest.getSourceUri());

        answer_MainArray = new ArrayList<ArrayList>();
        answerid_MainArray = new ArrayList<ArrayList>();
        updateanswerMain_list = new ArrayList<String>();
        updatequestionMain_list = new ArrayList<String>();
        initiating_APICredentials();

        animationInit();
        animationLoading();
        Runnable run = new Runnable() {
            @Override
            public void run() {
                animation_layout.setVisibility(View.GONE);
            }
        };
        Handler handler = new Handler();
        handler.postDelayed(run, 2000);

        postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.MEMBER_DETAILS_WITH_OPTIONS_URL,
                getjsonObject(TravexApplication.MEMBER_DETAILS_TAG),
                TravexApplication.REQUEST_ID_MEMBER_DETAILS);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.MEMBER_UPDATE_PROFILE_URL,
                        getjsonObject(TravexApplication.MEMBER_UPDATE_PROFILE_TAG),
                        TravexApplication.REQUEST_ID_MEMBER_UPDATE_PROFILE);


            }
        });

        return rootView;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, activity, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
            }

            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                //Handle the image
                //onPhotoReturned(imageFile);

                //profile_Image.setImageURI(Uri.fromFile(imageFile));
                beginCrop(Uri.fromFile(imageFile));
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                //Cancel handling, you might wanna remove taken photo if it was canceled
                if (source == EasyImage.ImageSource.CAMERA) {
                    File photoFile = EasyImage.lastlyTakenButCanceledPhoto(activity);
                    if (photoFile != null) photoFile.delete();Toast.makeText(activity,"Cancelled",Toast.LENGTH_SHORT).show();
                }
            }


        });
//        if (requestCode == Crop.REQUEST_PICK && resultCode == activity.RESULT_OK) {
//            beginCrop(data.getData());
//        } else
        if (requestCode == Crop.REQUEST_CROP) {
            handleCrop(resultCode, data);
        }

    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(activity.getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(activity,ProfileFragmentTwo.this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == activity.RESULT_OK) {
            Fresco.getImagePipeline().evictFromMemoryCache(Crop.getOutput(result));
            Fresco.getImagePipelineFactory().getMainDiskStorageCache().remove(new SimpleCacheKey(Crop.getOutput(result).toString()));
            Fresco.getImagePipelineFactory().getSmallImageDiskStorageCache().remove(new SimpleCacheKey(Crop.getOutput(result).toString()));
            Uri Image_Uri = Crop.getOutput(result);
            Image_File_Path = Image_Uri.getPath();
            profile_Image.setImageURI(Image_Uri);

        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(activity, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
            Image_File_Path="";
        }
    }

    public void postJsonRequest(int request_type, final Map<String, String> request_header, String url, JSONObject jsonObject, final int request_id) {

        mNetworkManager.postJsonRequest(request_type, request_header, url, jsonObject, request_id);

    }


    public JSONObject getjsonObject(String tag) {
        if (tag.equals(TravexApplication.MEMBER_DETAILS_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfMemberDetails
                            (TravexApplication.MEMBER_DETAILS_MEMBER_ID

                            ),
                    mListCreatorForNameValuePairRequest.getListOfMemberDetails(mPreferenceManager.getRegisration_Id_response())
                    //    mListCreatorForNameValuePairRequest.getListOfMemberDetails("zrzore_vq_16")   mPreferenceManager.getRegisration_Id_response()

            );

        }
        else if (tag.equals(TravexApplication.MEMBER_UPDATE_PROFILE_TAG)) {
            String Gender="F",MaritalStatus="M";
            if(tv_gender.getText().toString().equalsIgnoreCase("Male"))
            {
                Gender="M";
            }
            if(tv_status.getText().toString().equalsIgnoreCase("Single"))
            {
                MaritalStatus="S";
            }

            ArrayList<JSONObject> list_json_profile_answers = new ArrayList<>();
            if (question_list != null) {
                for (int p = 0; p < question_list.size(); p++) {
                    ArrayList<String> answer_array = new ArrayList<>();
                    answer_array = selected_answer_id_list.get(p);
                    String Answers="";
                    if(answer_array !=null) {
                        for (int q = 0; q < answer_array.size(); q++) {
                            String Ans= answer_array.get(q);

                            Answers += Ans+",";
                        }
                        Answers = Common.RemoveTheLastCharacter(Answers);
                    list_json_profile_answers.add
                            (getJson(mListCreatorForNameValuePairRequest.getListOfProfileAnswersTagProfileUpdate
                                            (TravexApplication.MEMBER_PROFILE_QUESTION_ID_UPDATE_PROFILE,
                                                    TravexApplication.MEMBER_PROFILE_TYPE_UPDATE_PROFILE,
                                                    TravexApplication.MEMBER_PROFILE_ANSWER_ID_UPDATE_PROFILE
                                            ),
                                    mListCreatorForNameValuePairRequest.getListOfProfileAnswersTagProfileUpdate(profilequestions_id_list.get(p), type_list.get(p), Answers)));

                    }

                }
            }

                return getJson(mPreferenceManager.getRegisration_Id_response(),
                        mListCreatorForNameValuePairRequest.
                                getListOfProfileTagProfileUpdate(TravexApplication.MEMBER_LAST_NAME_UPDATE_PROFILE,
                                        TravexApplication.MEMBER_EMAIL_UPDATE_PROFILE,
                                        TravexApplication.MEMBER_MARTIAL_STATUS_UPDATE_PROFILE,
                                        TravexApplication.MEMBER_PROFILE_IMAGE_UPDATE_PROFILE,
                                        TravexApplication.MEMBER_FIRST_NAME_UPDATE_PROFILE,
                                        TravexApplication.MEMBER_GENDER_UPDATE_PROFILE,
                                        TravexApplication.MEMBER_NO_KIDS_UPDATE_PROFILE), mListCreatorForNameValuePairRequest.
                                getListOfProfileTagProfileUpdate(member_last_name,
                                        tv_mail.getText().toString(),
                                        MaritalStatus,
                                        member_profile_image,
                                        tv_profile_name.getText().toString(),
                                        Gender, tv_children.getText().toString()), list_json_profile_answers
                );

        }
        else if (tag.equals(TravexApplication.PROFILE_IMAGE_UPDATE_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfProfileUpdateImagecontent
                            (TravexApplication.PROFILE_IMAGE_UPDATE_MEMBER_ID, TravexApplication.PROFILE_IMAGE_UPDATE_MEMBER_PROFILE_IMAGE

                            ),
                    mListCreatorForNameValuePairRequest.getListOfProfileUpdateImagecontent(mPreferenceManager.getRegisration_Id_response(), Image_File_Path)

            );

        }
        return  null;
    }

    public JSONObject getJson(ArrayList<String> namePair, ArrayList<String> valuePair) {


        return mJsonObjectMaker.getJson(namePair, valuePair);
    }
    public void initiating_APICredentials()
    {
        mJsonObjectMaker = new JsonObjectMaker(activity);
        mListCreatorForNameValuePairRequest = new ListCreatorForNameValuePairRequest(activity);
        mNetworkManager = NetworkManager.getSingleInstance(activity);
        mNetworkManager.setOnGetMemberDetailsResponseListener(this);
        mNetworkManager.setOnMemberUpdateResponseListener(this);
        mNetworkManager.setOnProfileImageUpdateResponseListener(this);
    }


    @Override
    public void onGetMemberDetailsResponse(boolean status, JSONObject jsonObject) {

        if (status) {
            LogUtility.Log(TAG, "onGetMemberDetailsResponse:success", "jsonObject:" + jsonObject);
            JSONObject result = null;
            try {
                result = jsonObject.getJSONObject("result");

                JSONObject profile_obj=result.getJSONObject("profile");
                try {
                    member_gender = profile_obj.getString("member_gender");
                    if(member_gender == null) member_gender = "";
                }
                catch(Exception s){
                    s.printStackTrace();
                    member_gender  ="";
                }
                try{
                    member_last_name=profile_obj.getString("member_last_name");
                }
                catch(Exception s){
                    s.printStackTrace();
                }
                try{
                    member_first_name=profile_obj.getString("member_first_name");
                    tv_profile_name.setText(member_first_name);
                }
                catch(Exception s){
                    s.printStackTrace();
                }
                try {
                    member_no_kids = profile_obj.getString("member_no_kids");
                    if(member_no_kids ==null)member_no_kids="";
                }
                catch(Exception s){
                    member_no_kids="";
                    s.printStackTrace();
                }
                try{
                    member_martial_status=profile_obj.getString("member_martial_status");
                    if(member_martial_status ==null)member_martial_status="";
                }
                catch(Exception s){
                    member_martial_status="";
                    s.printStackTrace();
                }
                try{
                    member_profile_image = profile_obj.getString("member_profile_image");
                    if(!member_profile_image.equals("")) {
                        Uri img_uri=Uri.parse(AppConstants.PROFILE_IMAGE_ASSET_URL+ member_profile_image);
                        ImageRequest request = ImageRequest.fromUri(img_uri);

                        DraweeController controller = Fresco.newDraweeControllerBuilder()
                                .setImageRequest(request)
                                .setOldController(profile_Image.getController()).build();
                        //Log.e(TAG, "ImagePath uri " + img_uri);

                        profile_Image.setController(controller);
                    }
                    else
                    {
                        ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.user).build();
                        profile_Image.setImageURI(imageRequest.getSourceUri());
                    }
                }
                catch(Exception s){
                    s.printStackTrace();
                }
                try{
                    member_email = profile_obj.getString("member_email");
                }
                catch(Exception s){
                    s.printStackTrace();
                }
                try{
                    member_id = profile_obj.getString("member_id");
                }
                catch(Exception s){
                    s.printStackTrace();
                }
                try{
                    tv_gender.setText(member_gender);
                }
                catch(Exception s){
                    s.printStackTrace();
                }try{
                    tv_mail.setText(member_email);
                }
                catch(Exception s){
                    s.printStackTrace();
                }
                try{
                    tv_status.setText(member_martial_status);
                }
                catch(Exception s){
                    s.printStackTrace();
                }
                try{
                    tv_children.setText(member_no_kids);
                }
                catch(Exception s){
                    s.printStackTrace();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                selected_answer_id_list = new ArrayList<>();
                selected_answer_list = new ArrayList<>();
                JSONArray profile_array=result.getJSONArray("questions");
                question_list = new ArrayList<>();
                profilequestions_id_list = new ArrayList<>();
                type_list = new ArrayList<>();
                answer_MainArray = new ArrayList<>();
                answerid_MainArray = new ArrayList<>();


                for(int i=0;i<profile_array.length();i++) {
                    JSONObject profile_obj = profile_array.getJSONObject(i);
                    question_list.add(profile_obj.getString("question"));


                    profilequestions_id_list.add(profile_obj.getString("profile_questions_id"));
                    type_list .add(profile_obj.getString("type"));
                    JSONArray user_answer_array=profile_obj.getJSONArray("options");
                    ArrayList<String> answer_list = new ArrayList<String>();
                    ArrayList<String> answer_id_list = new ArrayList<String>();
                    for (int ph = 0; ph < user_answer_array.length(); ph++) {
                        JSONObject obj=user_answer_array.getJSONObject(ph);
                        String answer = obj.getString("answer");
                        String answer_id = obj.getString("answer_id");
                        answer_list.add(answer);
                        answer_id_list.add(answer_id);


                    }
                    answer_MainArray.add(answer_list);
                    answerid_MainArray.add(answer_id_list);


                    ArrayList current_answer_id=new ArrayList();
                    ArrayList current_answer=new ArrayList();
                    JSONArray user_current_answer_array=null;
                    try
                    {
                        user_current_answer_array=profile_obj.getJSONArray("user_answer");
                    }
                    catch(Exception Ex)
                    {
                        user_current_answer_array=null;
                    }

                    if(user_current_answer_array != null)
                    {
                        for (int ph = 0; ph < user_current_answer_array.length(); ph++) {
                            JSONObject obj=user_current_answer_array.getJSONObject(ph);
                            String answer_id = obj.getString("answer_id");
                            String answer = obj.getString("answer");
                            if(!answer_id.equals("null"))
                            {
                                current_answer_id.add(answer_id);
                                current_answer.add(answer);
                            }

                        }
                        if(current_answer_id.size()>0) {
                            selected_answer_id_list.add(current_answer_id);
                            selected_answer_list.add(current_answer);
                        }
                        else
                        {
                            selected_answer_id_list.add(null);
                            selected_answer_list.add(null);
                        }

                    }
                    else
                    {
                        selected_answer_id_list.add(null);
                        current_answer.add("Select");
                        selected_answer_list.add(current_answer);
                    }



                }

                Fill_Question_And_Answers(true);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            LogUtility.Log(TAG, "onGetMemberDetailsResponse:failure", null);

        }

    }



    public void Fill_Question_And_Answers(Boolean ALertShow)
    {
        try {
            interested_layout.removeAllViews();
            for(int j=0;j<question_list.size();j++)  {

                LinearLayout hor_event_layout = new LinearLayout(activity);
                View inflatedView = View.inflate(activity, R.layout.interested_infate, hor_event_layout);
                ArrayList<String> a_list=new ArrayList<String>();
                String answer_text="Select";
                try {
                    a_list = selected_answer_list.get(j);
                    if(a_list !=null) {
                        if (a_list.size() > 0) {
                            answer_text = "";
                        }
                        for (int ans = 0; ans < a_list.size(); ans++) {
                            String ans_txt=a_list.get(ans);
                            if(a_list.get(ans)!=null)
                            {
                                if(!ans_txt.equalsIgnoreCase("null"))
                                {
                                    answer_text = answer_text+a_list.get(ans) + ",";
                                }
                            }
                        }

                        answer_text = Common.RemoveTheLastCharacter(answer_text);
                        if(answer_text.equals(""))
                        {
                            answer_text="Select";
                        }
                    }

                }catch (Exception y){
                    y.printStackTrace();
                    answer_text="Select";
                }

                TextView tv_question = (TextView) inflatedView.findViewById(R.id.tv_question);
                TextView tv_answer = (TextView) inflatedView.findViewById(R.id.tv_answer);
                tv_answer.setText(answer_text);
                tv_question.setText(question_list.get(j));
                inflatedView.setTag(j);
                interested_layout.addView(inflatedView);
                inflatedView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        displayAlertDialog(Integer.parseInt(v.getTag().toString()));
                    }
                });

            }
            if(ALertShow == false)
            {
                if(alertDialog != null)
                {
                    alertDialog.hide();
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }





    public void displayAlertDialog(int pos) {
        question_position=pos;
        dialogBuilder = new AlertDialog.Builder(activity);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = activity.getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.layout_answeroptions_inflate, null);
        TextView done=(TextView)dialogView.findViewById(R.id.done);
        RecyclerView recyclerView = (RecyclerView) dialogView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        ArrayList<String> current_answer_array= new ArrayList<String>();
        ArrayList<String> current_answer_id_array= new ArrayList<String>();
        ArrayList<String> selected_answer_id_array= new ArrayList<String>();

        if(answer_MainArray!=null) {
            current_answer_array = answer_MainArray.get(question_position);
            current_answer_id_array = answerid_MainArray.get(question_position);
            selected_answer_id_array = selected_answer_id_list.get(question_position);

            String type = type_list.get(question_position);

            AnsweroptionAdapter adapter = new AnsweroptionAdapter(activity, current_answer_array, current_answer_id_array, selected_answer_id_array, type, question_position);
            recyclerView.setAdapter(adapter);
            dialogBuilder.setView(dialogView);


            done.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Fill_Question_And_Answers(false);
                }
            });

            alertDialog = dialogBuilder.create();
            alertDialog.show();
        }

    }

    public void displayAlertDialog(String Q_Type) {
        Type=Q_Type;
        dialogBuilder = new AlertDialog.Builder(activity);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = activity.getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.layout_answeroptions_inflate, null);
        TextView done=(TextView)dialogView.findViewById(R.id.done);
        RecyclerView recyclerView = (RecyclerView) dialogView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        ArrayList<String> current_answer_array= new ArrayList<String>();

//        String current_answer="";
        selected_answer = "";

        if(Type.equalsIgnoreCase("Gender"))
        {
            current_answer_array = Gender_List;
            selected_answer = member_gender;
        }
        else if(Type.equalsIgnoreCase("Martial"))
        {
            current_answer_array = Martial_Status_List;
            selected_answer = member_martial_status;
        }
        else if(Type.equalsIgnoreCase("Kids"))
        {
            current_answer_array = Children_List;
            selected_answer = member_no_kids;
        }

        SingleAnswerOptionAdapter adapter = new SingleAnswerOptionAdapter(activity,current_answer_array,selected_answer,Type);
        recyclerView.setAdapter(adapter);
        dialogBuilder.setView(dialogView);


        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fill_Single_Qeustion_Answer(Type);
            }
        });

        alertDialog = dialogBuilder.create();
        alertDialog.show();

    }

    public void Fill_Single_Qeustion_Answer(String Type)
    {
        if(Type.equalsIgnoreCase("Gender"))
        {
            member_gender= selected_answer;
            tv_gender.setText(selected_answer);
        }
        else if(Type.equalsIgnoreCase("Martial"))
        {
            member_martial_status= selected_answer;
            tv_status.setText(selected_answer);
        }
        else if(Type.equalsIgnoreCase("Kids"))
        {
            member_no_kids= selected_answer;
            tv_children.setText(selected_answer);
        }
        alertDialog.hide();
    }

    public void handleForMultipartFormData(int type_method, String url, Map<String, String> header, JSONObject jsonObject, int request_id) {

        LogUtility.Log(TAG, "handleForMultipartFormData", "" +
                "type_method:" + type_method + ",url:" + url + ",header:" + header + ",jsonObject:" + jsonObject);

        mNetworkManager.handleForMultipartFormData(type_method, url, header, jsonObject, request_id);

    }

    @Override
    public void onGetMemberUpdateProfile(boolean status) {
        if (status) {
//            LogUtility.Log(TAG, "onGetMemberUpdateProfile:success", null);
            Toast.makeText(activity,"Profile Status Updated",Toast.LENGTH_SHORT).show();
            mPreferenceManager.setRegisteredUser_name(tv_profile_name.getText().toString());
            mPreferenceManager.setRegisteredUser_email(tv_mail.getText().toString());
            if(!Image_File_Path.equals(""))
            {
                handleForMultipartFormData(NetworkOptions.REQUEST_TYPE_POST,
                        AppConstants.PROFILE_IMAGE_UPDATE_URL,
                        mNetworkManager.getHeaders(),
                        getjsonObject(TravexApplication.PROFILE_IMAGE_UPDATE_TAG),
                        TravexApplication.REQUEST_ID_PROFILE_IMAGE_UPDATE);
            }
            else
            {
                activity.onBackPressed();
            }



        } else {
            Toast.makeText(activity,"Profile Status Not Updated",Toast.LENGTH_SHORT).show();
//            LogUtility.Log(TAG, "onGetMemberUpdateProfile:failure", null);

        }

    }

    public JSONObject getJson(String member_id, ArrayList<String> namePairOfProfileTag,
                              ArrayList<String> valuePairOfProfileTag,
                              ArrayList<JSONObject> profileAnswers) {
        JSONObject parent_object = new JSONObject();
        JSONObject mJsonObject_profileTag = mJsonObjectMaker.getJson(namePairOfProfileTag, valuePairOfProfileTag);
        JSONArray mJsonArray_profile_answers = new JSONArray();
        for (int counter = 0; counter < profileAnswers.size(); counter++) {

            JSONObject mJsonObject = profileAnswers.get(counter);
            LogUtility.Log(TAG, "getJson:json object of profile answers:", mJsonObject);
            mJsonArray_profile_answers.put(mJsonObject);


        }

        LogUtility.Log(TAG, "getJson:json array  profile answers:", mJsonArray_profile_answers);

        try {
            parent_object.accumulate(TravexApplication.MEMBER_ID_UPDATE_PROFILE, member_id);
            parent_object.accumulate(TravexApplication.MEMBER_PROFILE_DETAILS_UPDATE_PROFILE, mJsonObject_profileTag);
            parent_object.accumulate(TravexApplication.MEMBER_PROFILE_ANSWERS_UPDATE_PROFILE, mJsonArray_profile_answers);
            LogUtility.Log(TAG, "getJson:final json object  ", parent_object);


        } catch (JSONException e) {
            e.printStackTrace();
        }


        return parent_object;
    }

    @Override
    public void onGetProfileImageUpdateResponse(boolean status, String member_profile_image_url) {
        if (status) {
            //Toast.makeText(activity,"Profile Image Updated",Toast.LENGTH_SHORT).show();
            mPreferenceManager.setmember_profile_image(member_profile_image_url);
            try
            {

//                if(member_profile_image_url == null) {
//                    ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.user).build();
//                    Navadapter.profile_image.setImageURI(imageRequest.getSourceUri());              }
//                else if(member_profile_image_url.equals("")) {
//                    ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.user).build();
//                    Navadapter.profile_image.setImageURI(imageRequest.getSourceUri());
//                }
//                else
//                {
//                    Uri img_uri=Uri.parse(member_profile_image_url);
//                    ImageRequest request = ImageRequest.fromUri(img_uri);
//
//                    DraweeController controller = Fresco.newDraweeControllerBuilder()
//                            .setImageRequest(request)
//                            .setOldController(Navadapter.profile_image.getController()).build();
//                    //Log.e(TAG, "ImagePath uri " + img_uri);
//
//                    Navadapter.profile_image.setController(controller);
//                }
                AfterloginKnowncity.mDrawerList.setAdapter(AfterloginKnowncity.adapter);
            }
            catch(Exception ex)
            {

            }

            try
            {

                if(member_profile_image_url == null) {
                    ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.user).build();
                    AfterloginKnowncity.image.setImageURI(imageRequest.getSourceUri());              }
                else if(member_profile_image_url.equals("")) {
                    ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.user).build();
                    AfterloginKnowncity.image.setImageURI(imageRequest.getSourceUri());
                }
                else
                {
                    Uri img_uri=Uri.parse(member_profile_image_url);
                    ImageRequest request = ImageRequest.fromUri(img_uri);

                    DraweeController controller = Fresco.newDraweeControllerBuilder()
                            .setImageRequest(request)
                            .setOldController(AfterloginKnowncity.image.getController()).build();
                    //Log.e(TAG, "ImagePath uri " + img_uri);

                    AfterloginKnowncity.image.setController(controller);
                }
            }
            catch(Exception ex)
            {

            }

            activity.onBackPressed();
            LogUtility.Log(TAG, "onGetProfileImageUpdateResponse:success", "updated image url:" + member_profile_image_url);


        } else {
            LogUtility.Log(TAG, "onGetProfileImageUpdateResponse:failure", null);

        }
    }

    //Animation Functions
    public void animationLoading(){
        animation_layout.setVisibility(View.VISIBLE);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                ImageView iv1 = (ImageView) rootView.findViewById(R.id.settings_one);
                ImageView iv2 = (ImageView) rootView.findViewById(R.id.settings_two);
                ImageView iv3 = (ImageView) rootView.findViewById(R.id.settings_three);

                iv1.setAnimation(rotateOne);
                iv2.setAnimation(rotateTwo);
                iv3.setAnimation(rotateThree);

                iv1.startAnimation(rotateOne);
                iv2.startAnimation(rotateTwo);
                iv3.startAnimation(rotateThree);
            }
        };
        Handler mHandler = new Handler();
        mHandler.postDelayed(runnable, 100);

    }

    public  void  animationInit(){
        rotateOne = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_one);
        rotateOne.setAnimationListener(this);
        rotateTwo= AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_two);
        rotateTwo.setAnimationListener(this);
        rotateThree= AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_three);
        rotateThree.setAnimationListener(this);
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }


    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
