package org.technomobs.travex.Activity.Fragment;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Controller.Interface.GetForgetPasswordResponse;
import org.technomobs.travex.Controller.JsonObjectMaker;
import org.technomobs.travex.Controller.NetworkManager;
import org.technomobs.travex.Controller.NetworkOptions;
import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.Common;
import org.technomobs.travex.Utillity.ListCreatorForNameValuePairRequest;
import org.technomobs.travex.Utillity.LogUtility;
import org.technomobs.travex.Activity.SignUpLoginMasterActivity;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ForgotPasswordFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ForgotPasswordFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ForgotPasswordFragment extends Fragment implements GetForgetPasswordResponse ,View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    static Activity activity;
    public static final String TAG = ForgotPasswordFragment.class.getSimpleName();
    static NetworkManager mNetworkManager = null;
    View view;
    JsonObjectMaker mJsonObjectMaker = null;
    PreferenceManager mPreferenceManager = null;
    ListCreatorForNameValuePairRequest mListCreatorForNameValuePairRequest = null;


    Button btn_submit;
    EditText et_email;
    String Email="";


    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ForgotPasswordFragment() {
        // Required empty public constructor
    }

    public ForgotPasswordFragment(Activity act) {
        activity=act;
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
//     * @param param1 Parameter 1.
//     * @param param2 Parameter 2.
     * @return A new instance of fragment ForgotPasswordFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ForgotPasswordFragment newInstance(Activity act) {
        activity=act;
        ForgotPasswordFragment fragment = new ForgotPasswordFragment();
        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_forgot_password, container, false);
        Common.setupUI(view, activity);
        initiating_APICredentials();
        initialization();
        return view;
    }

    public void initiating_APICredentials()
    {
        mJsonObjectMaker = new JsonObjectMaker(activity);
        mListCreatorForNameValuePairRequest = new ListCreatorForNameValuePairRequest(activity);
        mNetworkManager = NetworkManager.getSingleInstance(activity);
        mNetworkManager.setOnGetForgetPasswordResponseListener(this);
        mPreferenceManager = new PreferenceManager(activity);
    }

    public JSONObject getjsonObject(String tag) {
        if (tag.equals(TravexApplication.FORGET_PASSWORD_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfForgetPassword
                            (TravexApplication.MEMBER_EMAIL_FORGET_PASSWORD

                            ),
                    mListCreatorForNameValuePairRequest.getListOfForgetPassword(Email)

            );

        }
        return null;
    }

    public JSONObject getJson(ArrayList<String> namePair, ArrayList<String> valuePair) {


        return mJsonObjectMaker.getJson(namePair, valuePair);
    }

    public void postJsonRequest(int request_type, String url, JSONObject jsonObject, final int request_id) {
        LogUtility.Log(TAG, "postJsonRequest", null);
        mNetworkManager.postJsonRequest(request_type, url, jsonObject, request_id);
    }

    //Initializing Function
    public void initialization() {
        btn_submit=(Button)view.findViewById(R.id.btn_submit);
        et_email=(EditText)view.findViewById(R.id.et_email);
        btn_submit.setOnClickListener(this);
    }

    public void CallAPI()
    {
        Email=et_email.getText().toString().trim();
        if(Email.equals(""))
        {
            Toast.makeText(activity, "Please Enter Email", Toast.LENGTH_SHORT).show();
        }
        else {

            postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.FORGET_PASSWORD_URL, getjsonObject(TravexApplication.FORGET_PASSWORD_TAG),
                    TravexApplication.REQUEST_ID_FORGET_PASSWORD);
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onGetForgetPasswordResponse(boolean status,String Message) {
        if (status) {
            Toast.makeText(activity, Message, Toast.LENGTH_SHORT).show();

        } else {
            Toast.makeText(activity, Message, Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_submit:
                CallAPI();
                break;
            default:
                break;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
