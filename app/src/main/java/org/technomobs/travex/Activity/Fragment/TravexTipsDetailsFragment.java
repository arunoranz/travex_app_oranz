package org.technomobs.travex.Activity.Fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Adapter.TipsAdapter;
import org.technomobs.travex.Adapter.TipsAdapterMain;
import org.technomobs.travex.Controller.Interface.GetTipsDetailsResponse;
import org.technomobs.travex.Controller.Interface.GetTipsListResponse;
import org.technomobs.travex.Controller.JsonObjectMaker;
import org.technomobs.travex.Controller.NetworkManager;
import org.technomobs.travex.Controller.NetworkOptions;
import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.ListCreatorForNameValuePairRequest;
import org.technomobs.travex.Utillity.LogUtility;

import java.util.ArrayList;

/**
 * Created by Abhi on 07-03-2016.
 */
public class TravexTipsDetailsFragment extends Fragment implements GetTipsListResponse, GetTipsDetailsResponse,Animation.AnimationListener {
    static Context context;
    public static final String TAG = TravexTipsDetailsFragment.class.getSimpleName();
    PreferenceManager mPreferenceManager;
    private ArrayList<ArrayList> childItems = new ArrayList<ArrayList>();
    ArrayList<String> prgmNameList,desigList;
    static NetworkManager mNetworkManager = null;
    JsonObjectMaker mJsonObjectMaker = null;
    //ExpandableListView gridview;
    ListCreatorForNameValuePairRequest mListCreatorForNameValuePairRequest = null;
    String country = null;
    String country_id = null;
    String category_item = null;
    ArrayList<String> tips_id = new ArrayList<>();
    ArrayList<String> tips_title = new ArrayList<>();
    ArrayList<String> tips = new ArrayList<>();

    RelativeLayout animation_layout;
    View rootView;
    Animation rotateOne, rotateTwo, rotateThree;
    RecyclerView recyclerView;
    TextView tv_header,tv_notips;
    static String Category_Name="";

    public static TravexTipsDetailsFragment newInstance(Context con,String Category){
        TravexTipsDetailsFragment fragment = new TravexTipsDetailsFragment();
        context=con;
        Category_Name=Category;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.traveltipsdetails_fragment, container, false);
        initiating_APICredentials();
        initialization(rootView);
        GettingPreferenceValues();
        postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_TIPS_LIST_URL,
                getjsonObject(TravexApplication.GET_TIPS_LIST_TAG),
                TravexApplication.REQUEST_ID_TIPS_LIST);
        animationInit();
        animationLoading();

        Runnable run = new Runnable() {
            @Override
            public void run() {
                animation_layout.setVisibility(View.GONE);
            }
        };
        Handler handler = new Handler();
        handler.postDelayed(run, 2000);
        ///////////////////
//        postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.CITY_LIST_URL,
//                getjsonObject(TravexApplication.GETCITY_TAG), TravexApplication.REQUEST_ID_GET_CITY);
        //////////////////



//        desigList.add("CEO");
//        desigList.add("CEO");
//        desigList.add("CEO");
//        childItems.add(desigList);
//        childItems.add(desigList);
//
//        childItems.add(desigList);
//
//        childItems.add(desigList);
//
//        childItems.add(desigList);




        // gridview.setOnChildClickListener(this);
        //  gridview.setAdapter(new ImageAdapter(getActivity().getApplicationContext()));

//        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            public void onItemClick(AdapterView<?> parent, View v,
//                                    int position, long id) {
//                Toast.makeText(getActivity().getApplicationContext(), "" + position,
//                        Toast.LENGTH_SHORT).show();
//            }
//
//
//        });


        return rootView;
    }

    public void GettingPreferenceValues()
    {
        LogUtility.Log(TAG, "onEnterKeyClick", null);
        country = mPreferenceManager.getSelectedCountryName();
        //country = ((ProfileActivity) getActivity()).getSelectedCountry();
        LogUtility.Log(TAG, "onEnterKeyClick:", "selected country from actionbar:" + country);
        //country_id = ((ProfileActivity) getActivity()).fetchCountryIdForCountry(country);
        country_id = mPreferenceManager.getSelectedcountryId();
        LogUtility.Log(TAG, "onEnterKeyClick:", "country id:" + country_id);
    }


    public void initiating_APICredentials()
    {
        mJsonObjectMaker = new JsonObjectMaker(getActivity());
        mListCreatorForNameValuePairRequest = new ListCreatorForNameValuePairRequest(getActivity());
        mNetworkManager = NetworkManager.getSingleInstance(getActivity());
        mNetworkManager.setOnGetTipsListResponseListener(this);
        mNetworkManager.setOnGetTipsDetailsResponseListener(this);
        mPreferenceManager = new PreferenceManager(getActivity());
    }

    public void initialization(View view)
    {
        prgmNameList=new ArrayList<>();
        desigList=new ArrayList<>();
        mPreferenceManager = new PreferenceManager(context);
        prgmNameList=mPreferenceManager.getSupportedCountryNames();
        category_item=Category_Name;
        tv_header=(TextView)rootView.findViewById(R.id.tv_header);
        tv_notips=(TextView)rootView.findViewById(R.id.tv_notips);
        tv_header.setText(Category_Name+" Tips");
//        gridview = (ExpandableListView) view.findViewById(R.id.epandListview);
//        gridview.setDivider(null);
        animation_layout=(RelativeLayout)rootView.findViewById(R.id.settings_animation);
        recyclerView =(RecyclerView)rootView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
    }

    public void SetAdapter()
    {
//        MyExpandableAdapter adapter = new MyExpandableAdapter(tips_title, childItems);
//        adapter.setInflater((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE), getActivity());
        // Set the Adapter to expandableList
//        gridview.setAdapter(adapter);
        TipsAdapterMain TipsAdapterMain = new TipsAdapterMain(context,tips_title,childItems);
//        TipsAdapter TipsAdapter = new TipsAdapter(context,tips_title);
        recyclerView.setAdapter(TipsAdapterMain);
    }

    public JSONObject getjsonObject(String tag) {
        if (tag.equals(TravexApplication.GET_TIPS_LIST_TAG)) {
            return getJson(mListCreatorForNameValuePairRequest.getListOfTips_List(
                            TravexApplication.GET_TIPS_LIST_CITY_ID, TravexApplication.GET_TIPS_LIST_CATEGORY_ID


                    ),
                    mListCreatorForNameValuePairRequest.getListOfTips_List(country_id, getCategory_Id_Tips_For(category_item)));

        } else if (tag.equals(TravexApplication.GET_TIPS_DETAILS_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfTipsDetails(TravexApplication.GET_TIPS_DETAILS_ID),
                    mListCreatorForNameValuePairRequest.getListOfTipsDetails(mPreferenceManager.getTips_id().get(0))

            );

        }
        return null;
    }


    public void postJsonRequest(int request_type, String url, JSONObject jsonObject, final int request_id) {

        mNetworkManager.postJsonRequest(request_type, url, jsonObject, request_id);
    }

    public JSONObject getJson(ArrayList<String> namePair, ArrayList<String> valuePair) {


        return mJsonObjectMaker.getJson(namePair, valuePair);
    }



    @Override
    public void onGetTipsListResponse(boolean status, JSONObject jsonObject) {
        tips_id = new ArrayList<>();
        tips_title = new ArrayList<>();
        tips = new ArrayList<>();
        childItems = new ArrayList<>();
        if (status) {

            try {
                JSONArray result = jsonObject.getJSONArray("result");
                for(int i=0;i<result.length();i++)
                {

                    JSONObject tipsresult= result.getJSONObject(i);
                    String tips_id_str = tipsresult.getString("tips_id");
                    String tips_title_str = tipsresult.getString("tips_title");
                    tips_id.add(tips_id_str);
                    tips_title.add(tips_title_str);
                    JSONArray tips_item_array = tipsresult.getJSONArray("tips_item");
                    String tips_str="";
                    tips = new ArrayList<>();
                    for(int j=0;j<tips_item_array.length();j++)
                    {
                        JSONObject tips_object = tips_item_array.getJSONObject(j);
//                        tips_str += tips_object.getString("tips")+"||";
                        tips_str = tips_object.getString("tips");
                        tips.add(tips_str);
                    }
                    childItems.add(tips);
                }
                if(tips_title.size()>0) {
                    SetAdapter();
                }
                else
                {
                    tv_notips.setVisibility(View.VISIBLE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            LogUtility.Log(TAG, "onGetTipsListResponse: :success", "json data:" + jsonObject.toString());


        } else {
//            LogUtility.Log(TAG, "onGetTipsListResponse: :failure", null);

            tv_notips.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onGetTipsDetailsResponse(boolean status, JSONObject jsonObject) {
        if (status) {

            LogUtility.Log(TAG, "onGetTipsDetailsResponse: :success", "json data:" + jsonObject.toString());


        } else {
            LogUtility.Log(TAG, "onGetTipsDetailsResponse: :failure", null);

        }
    }


    //Animation Functions
    public void animationLoading(){
        animation_layout.setVisibility(View.VISIBLE);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                ImageView iv1 = (ImageView) rootView.findViewById(R.id.settings_one);
                ImageView iv2 = (ImageView) rootView.findViewById(R.id.settings_two);
                ImageView iv3 = (ImageView) rootView.findViewById(R.id.settings_three);

                iv1.setAnimation(rotateOne);
                iv2.setAnimation(rotateTwo);
                iv3.setAnimation(rotateThree);

                iv1.startAnimation(rotateOne);
                iv2.startAnimation(rotateTwo);
                iv3.startAnimation(rotateThree);
            }
        };
        Handler mHandler = new Handler();
        mHandler.postDelayed(runnable, 100);

    }

    public  void  animationInit(){
        rotateOne = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_one);
        rotateOne.setAnimationListener(this);
        rotateTwo= AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_two);
        rotateTwo.setAnimationListener(this);
        rotateThree= AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_three);
        rotateThree.setAnimationListener(this);
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }


    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    public static String getCategory_Id_Tips_For(String item) {

        if (item==null) {

            return null;
        } else if (item.equalsIgnoreCase("Airline")) {

            return "pngrtbel_vq_1";
        } else if (item.equalsIgnoreCase("Embasies & Consulates")) {

            return "pngrtbel_vq_2";
        } else if (item.equalsIgnoreCase("Tour & Travel")) {

            return "pngrtbel_vq_3";
        } else if (item.equalsIgnoreCase("Events")) {

            return "pngrtbel_vq_4";
        } else if (item.equalsIgnoreCase("Eating Out")) {

            return "pngrtbel_vq_5";
        } else if (item.equalsIgnoreCase("Stay/Hotels")) {

            return "pngrtbel_vq_6";
        } else if (item.equalsIgnoreCase("nightlife")) {

            return "pngrtbel_vq_7";
        } else if (item.equalsIgnoreCase("Spa")) {

            return "pngrtbel_vq_8";
        } else if (item.equalsIgnoreCase("Things To Do")) {

            return "pngrtbel_vq_9";
        } else if (item.equalsIgnoreCase("Shopping")) {

            return "pngrtbel_vq_10";
        } else if (item.equalsIgnoreCase("Business")) {

            return "pngrtbel_vq_11";
        } else if (item.equalsIgnoreCase("Real Estate")) {

            return "pngrtbel_vq_12";
        } else if (item.equalsIgnoreCase("Money Exchange")) {

            return "pngrtbel_vq_13";
        } else if (item.equalsIgnoreCase("Car Rentals")) {

            return "pngrtbel_vq_14";
        } else if (item.equalsIgnoreCase("Emergencies")) {

            return "pngrtbel_vq_15";
        }


        return null;


    }



}



/**
 * THIS IS FOR TIPS DETAILS API
 */
            /*postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_TIPS_DETAILS_URL,
                    getjsonObject(TravexApplication.GET_TIPS_DETAILS_TAG),
                    TravexApplication.REQUEST_ID_TIPS_DETAILS);*/
/**
 *this is for tip list api
 */
           /* postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_TIPS_LIST_URL,
                    getjsonObject(TravexApplication.GET_TIPS_LIST_TAG),
                    TravexApplication.REQUEST_ID_TIPS_LIST);*/






