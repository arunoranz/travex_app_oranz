package org.technomobs.travex.Activity.Fragment;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.technomobs.travex.Activity.SignUpLoginMasterActivity;
import org.technomobs.travex.Controller.JsonObjectMaker;
import org.technomobs.travex.Controller.NetworkManager;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.ListCreatorForNameValuePairRequest;
import org.technomobs.travex.Utillity.LogUtility;

/**
 * Created by technomobs on 29/2/16.
 */
public class UpdateMemberDetailsFragment extends Fragment implements View.OnClickListener {
    public static final String TAG = UpdateMemberDetailsFragment.class.getSimpleName();

    ImageView mImageView_logo = null;
    EditText mEditText_gender = null;
    EditText mEditText_status = null;
    EditText mEditText_children = null;
    EditText mEditText_nationality = null;
    EditText mEditText_languages = null;
    EditText mEditText_preffered = null;
    Button mButton_next = null;
    String gender = null;
    String status = null;
    String children = null;
    String nationality = null;
    String languages = null;
    String preffered = null;


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onActivityCreated", null);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        LogUtility.Log(TAG, "onAttach", null);


        super.onAttach(context);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        LogUtility.Log(TAG, "onConfigurationChanged", null);

        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onCreate", null);

        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onCreateView", null);
        View view = inflater.inflate(R.layout.update_member_details_fragment, container, false);
        mImageView_logo = (ImageView) view.findViewById(R.id.travex_logo);
        mEditText_gender = (EditText) view.findViewById(R.id.gender);
        mEditText_status = (EditText) view.findViewById(R.id.status);
        mEditText_children = (EditText) view.findViewById(R.id.children);
        mEditText_nationality = (EditText) view.findViewById(R.id.nationality);
        mEditText_languages = (EditText) view.findViewById(R.id.languages);
        mEditText_preffered = (EditText) view.findViewById(R.id.preffered);
        mButton_next = (Button) view.findViewById(R.id.next);


        return view;
    }

    @Override
    public void onDestroy() {
        LogUtility.Log(TAG, "onDestroy", null);

        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        LogUtility.Log(TAG, "onDestroyView", null);

        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        LogUtility.Log(TAG, "onDetach", null);

        super.onDetach();
    }

    @Override
    public void onPause() {
        LogUtility.Log(TAG, "onPause", null);

        super.onPause();
    }

    @Override
    public void onResume() {
        LogUtility.Log(TAG, "onResume", null);

        super.onResume();
    }

    @Override
    public void onStart() {
        LogUtility.Log(TAG, "onStart", null);

        super.onStart();
    }

    @Override
    public void onStop() {
        LogUtility.Log(TAG, "onStop", null);

        super.onStop();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onViewCreated", null);
        mButton_next.setOnClickListener(this);
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.next) {
            LogUtility.Log(TAG, "onClick:next clicked", null);
            gender = mEditText_gender.getText().toString();
            status = mEditText_status.getText().toString();
            children = mEditText_children.getText().toString();
            nationality = mEditText_nationality.getText().toString();
            languages = mEditText_languages.getText().toString();
            preffered = mEditText_preffered.getText().toString();

            if (
                    (gender.trim().length() > 0) && (status.trim().length() > 0) &&
                            (children.trim().length() > 0) &&
                            (nationality.trim().length() > 0) && (languages.trim().length() > 0) &&
                            (preffered.trim().length() > 0)


                    )
            {
                ((SignUpLoginMasterActivity) getActivity()).replace_with_UpdateDetails_Member_second_Fragment(gender, status, children, nationality, languages, preffered);

            }
            else
            {
                Toast.makeText(getActivity(),"all fields should be non empty",Toast.LENGTH_SHORT).show();
            }



        }

    }
}
