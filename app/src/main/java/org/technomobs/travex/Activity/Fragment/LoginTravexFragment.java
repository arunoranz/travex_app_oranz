package org.technomobs.travex.Activity.Fragment;


import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.Profile;
import com.facebook.login.LoginManager;

import org.json.JSONObject;
import org.technomobs.travex.Activity.AfterloginKnowncity;
import org.technomobs.travex.Activity.SignUpLoginMasterActivity;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.Controller.Interface.GetLoginResponse;
import org.technomobs.travex.Controller.JsonObjectMaker;
import org.technomobs.travex.Controller.NetworkManager;
import org.technomobs.travex.Controller.NetworkOptions;
import org.technomobs.travex.Facebook.FacebookLogin;
import org.technomobs.travex.GooglePlus.GooglePlusLogin;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.Common;
import org.technomobs.travex.Utillity.ListCreatorForNameValuePairRequest;
import org.technomobs.travex.Utillity.LogUtility;

import java.util.ArrayList;

/**
 * Created by technomobs on 23/2/16.
 */
public class LoginTravexFragment extends Fragment implements ForgotPasswordFragment.OnFragmentInteractionListener, View.OnClickListener, GetLoginResponse,Animation.AnimationListener{
    public static final String TAG = LoginTravexFragment.class.getSimpleName();
    EditText mEditText_email = null;
    EditText mEditText_password = null;
    Button mButton_Sign_in = null;
    RelativeLayout animation_layout;
    Button mButton_signin_facebook = null;
    Button mButton_signin_google = null;
    TextView mTextView_forget_pwd = null;
    TextView mButton_signup_travex=null;
    static NetworkManager mNetworkManager = null;
    JsonObjectMaker mJsonObjectMaker = null;
    PreferenceManager mPreferenceManager = null;
    ListCreatorForNameValuePairRequest mListCreatorForNameValuePairRequest = null;
    String email = null;
    String password = null;
    TravexApplication mTravexApplication = null;
    CallbackManager sCallbackManager = null;
    LoginManager sLoginManager = null;
    FacebookLogin mFacebookLogin = null;
    GooglePlusLogin mGooglePlusLogin = null;
    View view;
    Animation rotateOne, rotateTwo, rotateThree;
    TextView signup;
    android.support.v4.app.FragmentTransaction mFragmentTransaction = null;
    android.support.v4.app.FragmentManager mFragmentManager = null;
    @Override
    public void onAttach(Context context) {
        LogUtility.Log(TAG, "onAttach", null);
        mJsonObjectMaker = new JsonObjectMaker(getActivity());
        mNetworkManager = NetworkManager.getSingleInstance(getActivity());
        mNetworkManager.setOnGetLoginResponseListener(this);
        mPreferenceManager = new PreferenceManager(getActivity());
        mListCreatorForNameValuePairRequest = new ListCreatorForNameValuePairRequest(getActivity());
        mTravexApplication = TravexApplication.getInstance();
        sCallbackManager = mTravexApplication.getCallbackManagerForFacebook();
        sLoginManager = mTravexApplication.getLoginManagerForFacebook();
        mFacebookLogin = new FacebookLogin(getActivity(), LoginTravexFragment.this, sLoginManager, sCallbackManager);
        mGooglePlusLogin = new GooglePlusLogin(getActivity(), LoginTravexFragment.this);

        super.onAttach(context);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        LogUtility.Log(TAG, "onConfigurationChanged", null);

        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onCreate", null);

        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onCreateView", null);

        view = inflater.inflate(R.layout.activity_login, container, false);
        mEditText_email = (EditText) view.findViewById(R.id.email);
        mEditText_password = (EditText) view.findViewById(R.id.password);
        mButton_Sign_in = (Button) view.findViewById(R.id.signin_travex);
        mButton_signin_facebook = (Button) view.findViewById(R.id.signin_facebook);
        mButton_signin_google = (Button) view.findViewById(R.id.signin_google);
        mTextView_forget_pwd = (TextView) view.findViewById(R.id.forgetpassword);
        mButton_signup_travex=(TextView)view.findViewById(R.id.signup_travex);
        signup=(TextView)view.findViewById(R.id.signup);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        animation_layout=(RelativeLayout)view.findViewById(R.id.settings_animation);
        Common.setupUI(view.findViewById(R.id.rel_main), getActivity());
        animationInit();
//        mEditText_email.setOnKeyListener(new View.OnKeyListener() {
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                if (event.getAction() == KeyEvent.ACTION_DOWN) {
//                    switch (keyCode) {
//                        case KeyEvent.KEYCODE_DPAD_CENTER:
//                        case KeyEvent.KEYCODE_ENTER:
//
//                            addCourseFromTextBox();
//                            return true;
//                        default:
//                            break;
//                    }
//                }
//                return false;
//            }
//        });
        return view;
    }

    public  void  animationInit(){
        rotateOne = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_one);
        rotateOne.setAnimationListener(this);
        rotateTwo= AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_two);
        rotateTwo.setAnimationListener(this);
        rotateThree= AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_three);
        rotateThree.setAnimationListener(this);
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onActivityCreated", null);

        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        LogUtility.Log(TAG, "onActivityResult", null);

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GooglePlusLogin.SIGN_IN_REQUEST) {

            mGooglePlusLogin.getData(data);
        } else {
            sCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onDestroy() {
        LogUtility.Log(TAG, "onDestroy", null);

        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        LogUtility.Log(TAG, "onDestroyView", null);

        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        LogUtility.Log(TAG, "onDetach", null);

        super.onDetach();
    }

    @Override
    public void onPause() {
        LogUtility.Log(TAG, "onPause", null);

        super.onPause();
    }

    @Override
    public void onResume() {
        LogUtility.Log(TAG, "onResume", null);

        super.onResume();
    }

    @Override
    public void onStart() {
        LogUtility.Log(TAG, "onStart", null);

        super.onStart();
    }

    @Override
    public void onStop() {
        LogUtility.Log(TAG, "onStop", null);

        super.onStop();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onViewCreated", null);
        mButton_Sign_in.setOnClickListener(this);
//        mButton_signin_facebook.setOnClickListener(this);
//        mButton_signin_google.setOnClickListener(this);
        mButton_signup_travex.setOnClickListener(this);
        mTextView_forget_pwd.setOnClickListener(this);
        signup.setOnClickListener(this);

        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signin_travex: {
                // SETTINGS ANIMATION DEMO START


                LogUtility.Log(TAG, "onClick:signin_travex", null);
//				Intent u=new Intent(getActivity().getApplicationContext(), AfterloginKnowncity.class);
//                startActivity(u);
                if (mNetworkManager.isNetworkconnected())

                    email = mEditText_email.getText().toString();
                    password = mEditText_password.getText().toString();

                if ((email.trim().length() > 0) && (password.trim().length() > 0)) {
                    animationLoading();
                    postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.LOGIN_URL, getjsonObject(TravexApplication.LOGIN_TAG), TravexApplication.REQUEST_ID_LOGIN);
                    Runnable runnablenew = new Runnable() {
                        @Override
                        public void run() {
                            animation_layout.setVisibility(View.GONE);

                        }
                    };
                    Handler handlernew=new Handler();
                    handlernew.postDelayed(runnablenew, 2000);
                } else {
                    mTravexApplication.showToast("all fields needed to be filled");
                }


                break;
            }
//            case R.id.signin_facebook: {
//                LogUtility.Log(TAG, "onClick:signin_facebook", null);
//                if ((AccessToken.getCurrentAccessToken() == null) && (Profile.getCurrentProfile() == null)) {
//                    // sLoginManager.logInWithReadPermissions(LoginTravexFragment.this, Arrays.asList("public_profile", "email"));
//                    mFacebookLogin.loginFacebookRequest();
//                }
//                break;
//            }
//            case R.id.signin_google: {
////                LogUtility.Log(TAG, "onClick:signin_google", null);
////                mGooglePlusLogin.connect_GooglePlayServices();
//                Intent mIntent = new Intent(getActivity(), AfterloginKnowncity.class);
//                startActivity(mIntent);
//                //getActivity().finishActivity();
//                break;
//            }
            case R.id.signup_travex: {
                LogUtility.Log(TAG, "onClick:signup_travex", null);
                ((SignUpLoginMasterActivity) getActivity()).load_signupTravex_fragment();
                break;
            }
            case R.id.signup: {
                LogUtility.Log(TAG, "onClick:signup", null);
                ((SignUpLoginMasterActivity) getActivity()).load_signupTravex_fragment();
                break;
            }
            case R.id.forgetpassword: {

                ((SignUpLoginMasterActivity) getActivity()).replace_with_forgotFragment();
//                ForgotPasswordFragment ForgotPasswordFragment = org.technomobs.travex.Activity.Fragment.ForgotPasswordFragment.newInstance(getActivity());
//                mFragmentManager = getActivity().getSupportFragmentManager();
//                mFragmentTransaction = mFragmentManager.beginTransaction();
//
////                UpdateMemberDetailsSecondFragment mUpdateMemberDetailsSecondFragment = UpdateMemberDetailsSecondFragment.newInstance(gender, status, children, nationality, languages, preffered);
//                mFragmentTransaction.replace(R.id.container, ForgotPasswordFragment);
//                mFragmentTransaction.commitAllowingStateLoss();


//                FragmentManager fragmentManager;
//                fragmentManager = getActivity().getFragmentManager();
//                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                fragmentTransaction.replace(R.id.container, ForgotPasswordFragment);
//                fragmentTransaction.addToBackStack(ForgotPasswordFragment.getClass().getName());
//                fragmentTransaction.commit();


                break;
            }

        }


    }

    public JSONObject getJson(ArrayList<String> namePair, ArrayList<String> valuePair) {


        return mJsonObjectMaker.getJson(namePair, valuePair);
    }


    /**
     * @param request_type
     * @param url
     * @param jsonObject
     * @param request_id
     */
    public void postJsonRequest(int request_type, String url, JSONObject jsonObject, final int request_id) {
        mNetworkManager.postJsonRequest(request_type, url, jsonObject, request_id);

    }
    public JSONObject getjsonObject(String tag) {

        if (tag.equals(TravexApplication.LOGIN_TAG)) {


            return getJson(mListCreatorForNameValuePairRequest.getListForLoginRequest(TravexApplication.EMAIL_LOGIN, TravexApplication.PASSWORD_LOGIN),

                    mListCreatorForNameValuePairRequest.getListForLoginRequest(email, password)
            );

        }

        return null;
    }

    @Override
    public void onGetLoginResponse(boolean status) {
        if (status) {
            //LogUtility.Log(TAG, "onGetLoginResponse", "status:login  ok");
            mPreferenceManager.setLogin_Type(TravexApplication.TRAVEX_LOGIN);
            startProfileActivity();
        } else {
            Toast.makeText(getActivity(), "Invalid Username or Password",
                    Toast.LENGTH_SHORT).show();
            LogUtility.Log(TAG, "onGetLoginResponse", "status:login failed");

        }
       // animation_layout.setVisibility(View.GONE);
    }

    public void startProfileActivity() {
        ((SignUpLoginMasterActivity) getActivity()).startProfileActivity();

    }
    public void animationLoading(){
        animation_layout.setVisibility(View.VISIBLE);
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    ImageView iv1 = (ImageView) view.findViewById(R.id.settings_one);
                    ImageView iv2 = (ImageView) view.findViewById(R.id.settings_two);
                    ImageView iv3 = (ImageView) view.findViewById(R.id.settings_three);

                    iv1.setAnimation(rotateOne);
                    iv2.setAnimation(rotateTwo);
                    iv3.setAnimation(rotateThree);

                    iv1.startAnimation(rotateOne);
                    iv2.startAnimation(rotateTwo);
                    iv3.startAnimation(rotateThree);
                }
            };
            Handler mHandler = new Handler();
            mHandler.postDelayed(runnable, 100);
        }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
