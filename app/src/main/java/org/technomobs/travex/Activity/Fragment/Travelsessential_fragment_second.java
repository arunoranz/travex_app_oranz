package org.technomobs.travex.Activity.Fragment;

import android.app.Activity;
import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.google.gson.JsonObject;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.technomobs.travex.Activity.AfterloginKnowncity;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Controller.Interface.GetEssentialsDetailsResponse;
import org.technomobs.travex.Controller.JsonObjectMaker;
import org.technomobs.travex.Controller.NetworkManager;
import org.technomobs.travex.Controller.NetworkOptions;
import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.ListCreatorForNameValuePairRequest;
import org.technomobs.travex.Utillity.LogUtility;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by oranz-pc2 on 1/4/16.
 */
public class Travelsessential_fragment_second extends Fragment implements GetEssentialsDetailsResponse {
    static NetworkManager mNetworkManager = null;
    static String imageurl, descrp, essential_id,Title="";
    static Activity activity;
    static int pos;
    public SimpleDraweeView image;
    View rootView;
    WebView descp;
    PreferenceManager mPreferenceManager = null;
//    ArrayList<String> imagelist;
    ViewPager vpPager;
    ArrayList<String> Essential_Images_URL= new ArrayList<>();
    CirclePageIndicator indicator;
    ListCreatorForNameValuePairRequest mListCreatorForNameValuePairRequest = new ListCreatorForNameValuePairRequest(getActivity());
    JsonObjectMaker mJsonObjectMaker = null;

    public static Travelsessential_fragment_second newinstance(Activity act,String id) {
        Travelsessential_fragment_second fragment = new Travelsessential_fragment_second();
//        descrp = descpt;
//        imageurl = imgurl;
        essential_id = id;
//        pos = val;
        activity=act;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.travelessential_adapter, container, false);
        //Fresco.initialize(getActivity());
//        ( (AfterloginKnowncity)getActivity()).getActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        image = (SimpleDraweeView) rootView.findViewById(R.id.sdvImage);
        descp = (WebView) rootView.findViewById(R.id.desp);
        vpPager = (ViewPager) rootView.findViewById(R.id.pager);
        indicator = (CirclePageIndicator) rootView.findViewById(R.id.indicator);
        if(Essential_Images_URL.size()==0){
            Essential_Images_URL.add("");
        }

//        Essential_Viewpager_Adapter adapter = new Essential_Viewpager_Adapter(getActivity(), Essential_Images_URL);
//        adapter.notifyDataSetChanged();
//        vpPager.setAdapter(adapter);
//        vpPager.setPageMargin(20);
//        vpPager.setCurrentItem(0);





        mJsonObjectMaker = new JsonObjectMaker(activity);
        mNetworkManager = NetworkManager.getSingleInstance(activity);
        mNetworkManager.setOnGetEssentialsDetailsResponseListener(this);
        postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_ESSENTIALS_DETAILS_URL,
                getjsonObject(TravexApplication.GET_ESSENTIALS_DETAILS_TAG),
                TravexApplication.REQUEST_ID_ESSENTIALS_DETAILS);
        mPreferenceManager = new PreferenceManager(activity);
        mListCreatorForNameValuePairRequest = new ListCreatorForNameValuePairRequest(activity);
        //descp.setText(Html.fromHtml(descrp));

//        if(!imageurl.equals(""))
//        {
//            Uri img_uri=Uri.parse(imageurl);
//            ImageRequest request = ImageRequest.fromUri(img_uri);
//
//            DraweeController controller = Fresco.newDraweeControllerBuilder()
//                    .setImageRequest(request)
//                    .setOldController(image.getController()).build();
//            //Log.e(TAG, "ImagePath uri " + img_uri);
//
//            image.setController(controller);
//        }
//        else
//        {
//            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.travex_grey128).build();
//            image.setImageURI(imageRequest.getSourceUri());
//        }
        return rootView;
    }

    public void SetAdapter()
    {
        //descp.setText(Html.fromHtml(descrp));
        descp.loadData(descrp, "text/html", "UTF-8");
        Essential_Viewpager_Adapter adapter = new Essential_Viewpager_Adapter(getActivity(),Essential_Images_URL);
        vpPager.setAdapter(adapter);

//        vpPager.setPageMargin(20);
        vpPager.setCurrentItem(0);

        indicator.setFillColor(getActivity().getResources().getColor(android.R.color.white));
        indicator.setViewPager(vpPager);
    }

    public void ChangeToolBarDesign()
    {
        //To change the current design of the Toolbar in this fragment
        try {
            Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.tool_bar);
            TextView textToolHeader = (TextView) toolbar.findViewById(R.id.tv_category);
            textToolHeader.setText(Title);
            textToolHeader.setVisibility(View.VISIBLE);
            SimpleDraweeView image = (SimpleDraweeView) toolbar.findViewById(R.id.networkImageViewEqWNH);
            image.setVisibility(View.GONE);

            toolbar.setNavigationIcon(R.drawable.ic_back_small);
        }
        catch(Exception ex)
        {

        }
    }

    public JSONObject getjsonObject(String tag) {
        if (tag.equals(TravexApplication.GET_ESSENTIALS_DETAILS_TAG)) {
            return getJson(mListCreatorForNameValuePairRequest.getListOfEssentialsDetails(
                            TravexApplication.GET_ESSENTIALS_DETAILS_ID


                    ),
                    mListCreatorForNameValuePairRequest.
                            getListOfEssentialsDetails(essential_id));

        }
        return null;
    }

    public JSONObject getJson(ArrayList<String> namePair, ArrayList<String> valuePair) {


        return mJsonObjectMaker.getJson(namePair, valuePair);
    }

    public void postJsonRequest(int request_type, String url, JSONObject jsonObject, final int request_id) {

        mNetworkManager.postJsonRequest(request_type, url, jsonObject, request_id);
    }

    @Override
    public void onGetEssentialsDetailsResponse(boolean status, JSONObject jsonObject) {
        Essential_Images_URL=new ArrayList<>();
        if (status) {

            try {
                JSONObject result = jsonObject.getJSONObject("result");
                JSONObject detail = result.getJSONObject("details");
                JSONArray imagearray = detail.getJSONArray("images");

                descrp = detail.getString("essentials_text");
                Title = detail.getString("essentials_title");
                for (int i = 0; i < imagearray.length(); i++) {
//                    imagelist.add(imagearray.get(i).toString());
                    JSONObject img_obj= imagearray.getJSONObject(i);
                    String img_name=img_obj.getString("essentials_images");


                    if(img_name.equals("")){
                        Essential_Images_URL.add("");
                    }else {
                        Essential_Images_URL.add(AppConstants.ESSENTIAL_IMAGES_ASSETS_URL + "/" + img_name);
                    }
                }
                //if(Essential_Images_URL.size()>0) {
                    SetAdapter();
                //}


            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else {


        }

        ChangeToolBarDesign();
    }
}
