package org.technomobs.travex.Activity.Fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.cache.common.SimpleCacheKey;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import org.technomobs.travex.R;

import java.util.ArrayList;

/**
 * Created by ARUN on 03/13/2016.
 */
public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.ViewHolder> {
     ScrollView child_scroll;
    LayoutInflater mInflater;
    static Activity activity ;
    ArrayList<String> tv_header_list, tv_details_list,sdv_detailsimage_list,category_key_list,articles_id_list;
    //OnItemClickListener mItemClickListener;



    public ArticleAdapter(Activity act, ArrayList<String> tv_header, ArrayList<String> tv_details
            , ArrayList<String> sdv_detailsimage,ArrayList<String> category_key,ArrayList<String> articles_id) {
        activity = act;
        tv_header_list=tv_header;
        tv_details_list = tv_details;
        sdv_detailsimage_list=sdv_detailsimage;
        category_key_list=category_key;
        articles_id_list=articles_id;
        mInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

    }
//    @Override
//    public int getCount() {
//        return tv_header_list.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return null;
//    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder;
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.articleadapterview, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;

    }

//    @Override
//    public void onBindViewHolder(ViewHolder holder, int position) {
//
//        holder = new ViewHolder();
//
//        //convertView = mInflater.inflate(R.layout.articleadapterview, null);
//        holder.sdv_detailsimage= (SimpleDraweeView)convertView.findViewById(R.id.sdv_detailsimage);
//        holder.tv_header = (TextView) convertView.findViewById(R.id.tv_header);
//        holder.tv_details = (TextView) convertView.findViewById(R.id.tv_details);
//        //convertView.setTag(holder);
//
//        viewHolder.txtViewTitle.setText(itemsData[position].getTitle());
//        viewHolder.imgViewIcon.setImageResource(itemsData[position].getImageUrl());
//    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData
        //Fresco.initialize(activity);

        if(category_key_list.get(position).equalsIgnoreCase("hotel"))
        {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_stay_hotel).build();
            viewHolder.sdv_food_icon.setImageURI(imageRequest.getSourceUri());
        }
        else if(category_key_list.get(position).equalsIgnoreCase("spa"))
        {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_spa).build();
            viewHolder.sdv_food_icon.setImageURI(imageRequest.getSourceUri());
        }
        else if(category_key_list.get(position).equalsIgnoreCase("todo"))
        {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_thingstodo).build();
            viewHolder.sdv_food_icon.setImageURI(imageRequest.getSourceUri());
        }
        else if(category_key_list.get(position).equalsIgnoreCase("shopping"))
        {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_shopping).build();
            viewHolder.sdv_food_icon.setImageURI(imageRequest.getSourceUri());
        }
        else if(category_key_list.get(position).equalsIgnoreCase("nightlife"))
        {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_nightlife).build();
            viewHolder.sdv_food_icon.setImageURI(imageRequest.getSourceUri());
        }
        else if(category_key_list.get(position).equalsIgnoreCase("eatingout"))
        {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_eatingout).build();
            viewHolder.sdv_food_icon.setImageURI(imageRequest.getSourceUri());
        }
        else if(category_key_list.get(position).equalsIgnoreCase("realestate"))
        {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_realestate).build();
            viewHolder.sdv_food_icon.setImageURI(imageRequest.getSourceUri());
        }
        else if(category_key_list.get(position).equalsIgnoreCase("business"))
        {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_bussiness).build();
            viewHolder.sdv_food_icon.setImageURI(imageRequest.getSourceUri());
        }




//        if(category_key_list.get(position).equalsIgnoreCase("Airline")) {
//            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_airline).build();
//            viewHolder.sdv_food_icon.setImageURI(imageRequest.getSourceUri());
//        }else if(category_key_list.get(position).equalsIgnoreCase("Shopping")) {
//
//        }else if(category_key_list.get(position).equalsIgnoreCase("Buisiness")) {
//
//        }else if(category_key_list.get(position).equalsIgnoreCase("Real Estate")) {
//
//        }else if(category_key_list.get(position).equalsIgnoreCase("Money Exchange")) {
//            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_moneyexchange).build();
//            viewHolder.sdv_food_icon.setImageURI(imageRequest.getSourceUri());
//        }else if(category_key_list.get(position).equalsIgnoreCase("Car Rental")) {
//            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_rentcar).build();
//            viewHolder.sdv_food_icon.setImageURI(imageRequest.getSourceUri());
//        }else if(category_key_list.get(position).equalsIgnoreCase("Embasi $ Consulates")) {
//            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_embassy).build();
//            viewHolder.sdv_food_icon.setImageURI(imageRequest.getSourceUri());
//        }else if(category_key_list.get(position).equalsIgnoreCase("Tour $ Travel")) {
//            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_tour_travel).build();
//            viewHolder.sdv_food_icon.setImageURI(imageRequest.getSourceUri());
//        }else if(category_key_list.get(position).equalsIgnoreCase("Events")) {
//            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_events).build();
//            viewHolder.sdv_food_icon.setImageURI(imageRequest.getSourceUri());
//        }else if(category_key_list.get(position).equalsIgnoreCase("Eating Out")) {
//            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_eatingout).build();
//            viewHolder.sdv_food_icon.setImageURI(imageRequest.getSourceUri());
//        }else if(category_key_list.get(position).equalsIgnoreCase("Stay/Hotels")) {
//
//        }else if(category_key_list.get(position).equalsIgnoreCase("Nightlife")) {
//
//        }


        if(!sdv_detailsimage_list.get(position).equals(""))
        {
            Uri img_uri=Uri.parse(sdv_detailsimage_list.get(position));
            Fresco.getImagePipeline().evictFromMemoryCache(img_uri);
            Fresco.getImagePipelineFactory().getMainDiskStorageCache().remove(new SimpleCacheKey(img_uri.toString()));
            Fresco.getImagePipelineFactory().getSmallImageDiskStorageCache().remove(new SimpleCacheKey(img_uri.toString()));
            ImageRequest request = ImageRequest.fromUri(img_uri);

            DraweeController controller = Fresco.newDraweeControllerBuilder()
                    .setImageRequest(request)
                    .setOldController(viewHolder.sdv_detailsimage.getController()).build();
            //Log.e(TAG, "ImagePath uri " + img_uri);

            viewHolder.sdv_detailsimage.setController(controller);
        }
        else
        {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.travex_grey128).build();
            viewHolder.sdv_detailsimage.setImageURI(imageRequest.getSourceUri());
        }

        viewHolder.tv_header.setText(tv_header_list.get(position));
        viewHolder.tv_details.setText(tv_details_list.get(position));
        viewHolder.lin_lay_main.setTag(position);

        //viewHolder.sdv_detailsimage.setImageURI(img_uri);


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return tv_header_list.size();
    }

//    public interface OnItemClickListener {
//        public void onItemClick(View view , int position);
//    }
//
//    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
//        this.mItemClickListener = mItemClickListener;
//    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView tv_header, tv_details;
        public SimpleDraweeView sdv_detailsimage,sdv_food_icon;
        public LinearLayout lin_lay_main;


        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tv_header = (TextView) itemLayoutView.findViewById(R.id.tv_header);
            tv_details = (TextView) itemLayoutView.findViewById(R.id.tv_details);
            sdv_detailsimage= (SimpleDraweeView) itemLayoutView.findViewById(R.id.sdv_detailsimage);
            sdv_food_icon = (SimpleDraweeView) itemLayoutView.findViewById(R.id.sdv_food_icon);
            lin_lay_main = (LinearLayout) itemLayoutView.findViewById(R.id.lin_lay_main);
            lin_lay_main.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position= (int)lin_lay_main.getTag();
            String articles_id = articles_id_list.get(position);
            switch (v.getId())
            {
                case R.id.lin_lay_main:


                    Fragment fragment= ArticleDetailsFragment.newInstance(activity,articles_id);
                    FragmentManager fragmentManager = activity.getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_container, fragment);
                    fragmentTransaction.addToBackStack(fragment.getClass().getName());
                    fragmentTransaction.commit();
                    break;
                default:
                    break;

            }
        }


//        @Override
//        public void onClick(View v) {
//            if (mItemClickListener != null) {
//                mItemClickListener.onItemClick(v, getPosition());
//            }
//        }


    }




}


