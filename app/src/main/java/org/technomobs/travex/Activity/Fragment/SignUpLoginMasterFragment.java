package org.technomobs.travex.Activity.Fragment;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dd.CircularProgressButton;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONObject;
import org.technomobs.travex.Activity.SignUpLoginMasterActivity;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Controller.JsonObjectMaker;
import org.technomobs.travex.Controller.NetworkManager;
import org.technomobs.travex.Facebook.FacebookLogin;
import org.technomobs.travex.GooglePlus.GooglePlusLogin;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.ListCreatorForNameValuePairRequest;
import org.technomobs.travex.Utillity.LogUtility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by technomobs on 22/2/16.
 */
public class SignUpLoginMasterFragment extends Fragment implements View.OnClickListener,Animation.AnimationListener {
    public static final String TAG = SignUpLoginMasterFragment.class.getSimpleName();
    //Button mButtonForSignUpTravex = null;
    //CircularProgressButton mButtonForLoginTravex = null;
    Animation mAnimationScale, mAnimationRotate;
    int loginAnimCount = 0;
    Button mButtonForSignUpFacebook = null;
    Button mButtonForSignUpGoogle = null;
    int count,total_count;
    Timer timer;
    PreferenceManager preferenceManager;
    TextView mButtonForskip ;
    TravexApplication mTravexApplication = null;
    Profile mProfileFacebook = null;
    AccessToken mAccessToken = null;
    static LoginManager mLoginManager = null;
    static CallbackManager mCallbackManager = null;
    PreferenceManager mPreferenceManager = null;
    static NetworkManager mNetworkManager = null;
    ListCreatorForNameValuePairRequest mListCreatorForNameValuePairRequest = null;

    JsonObjectMaker mJsonObjectMaker = null;
    FacebookLogin mFacebookLogin = null;
    GooglePlusLogin mGooglePlusLogin = null;
    Button btngp = null;
    //ViewPager mViewPager_Description = null;
    PagerAdapter mPagerAdapter = null;
    Travex_Tips_Static_Fragment mTravex_Tips_Static_Fragment = null;
    Travex_Myplan_Static_Fragment mTravex_Myplan_Static_Fragment = null;
    Travex_Now_Static_Fragment mTravex_Now_Static_Fragment = null;
    Travex_Articles_Static_Fragment mTravex_Articles_Static_Fragment = null;
    Travex_Guide_Static_Fragment mTravex_Guide_Static_Fragment = null;


    static final int NUM_PAGES = 1;
    Handler mHandlerForLoadMyPlanFragment = new Handler();
    Runnable mRunnableForLoadMyPlanFragment = null;
    Handler mHandlerForLoadNowFragment = new Handler();
    Runnable mRunnableForLoadNowFragment = null;
    Handler mHandlerForLoadArticlesFragment = new Handler();
    Runnable mRunnableForLoadArticlesFragment = null;
    Handler mHandlerForLoadGuideFragment = new Handler();
    Runnable mRunnableForLoadGuideFragment = null;
    TextView mButtonForSignUpTravex = null;
    ImageView mButtonForSignUpTravexImage = null;
    RelativeLayout mButtonForSignUpTravexRL = null;
    TextView mButtonForLoginTravex = null;
    ImageView mButtonForLoginTravexImage = null;
    RelativeLayout mButtonForLoginTravexRL = null;
    CirclePageIndicator indicator;
    int[] indicator_colors;
    ArrayList<String> Title_List=new ArrayList<String>();
    ArrayList<String> Description_List=new ArrayList<String>();
    ArrayList<String> Image_URL_List=new ArrayList<String>();

    @Override
    public void onAttach(Context context) {
        LogUtility.Log(TAG, "onAttach", null);
        mJsonObjectMaker = new JsonObjectMaker(getActivity());
        mNetworkManager = NetworkManager.getSingleInstance(getActivity());
        mListCreatorForNameValuePairRequest = new ListCreatorForNameValuePairRequest(getActivity());

        mTravexApplication = TravexApplication.getInstance();
        mCallbackManager = mTravexApplication.getCallbackManagerForFacebook();
        mLoginManager = mTravexApplication.getLoginManagerForFacebook();
        mPreferenceManager = new PreferenceManager(getActivity());
        mFacebookLogin = new FacebookLogin(getActivity(), SignUpLoginMasterFragment.this, mLoginManager, mCallbackManager);
        mGooglePlusLogin = new GooglePlusLogin(getActivity(), SignUpLoginMasterFragment.this);
        mPagerAdapter = new ScreenSlidePagerAdapter(getActivity().getSupportFragmentManager());

        mTravex_Tips_Static_Fragment = new Travex_Tips_Static_Fragment();
        mTravex_Myplan_Static_Fragment = new Travex_Myplan_Static_Fragment();
        mTravex_Now_Static_Fragment = new Travex_Now_Static_Fragment();
        mTravex_Articles_Static_Fragment = new Travex_Articles_Static_Fragment();
        mTravex_Guide_Static_Fragment = new Travex_Guide_Static_Fragment();
        Title_List =mPreferenceManager.getCms_image_title_feature();
        Description_List =mPreferenceManager.getCms_image_description_feature();
        Image_URL_List =mPreferenceManager.getCms_image_asset_url_feature();
		
		mLoginManager.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                LogUtility.Log(TAG, "Fb Login:onSuccess", null);
                LogUtility.Log(TAG, "onSuccess:", "access token from login result:" + loginResult.getAccessToken().getToken());
                Toast.makeText(getActivity(), "login success to facebook", Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onCancel() {
                LogUtility.Log(TAG, "Fb Login:onCancel", null);

            }

            @Override
            public void onError(FacebookException error) {
                LogUtility.Log(TAG, "Fb Login:onError", "error:" + error.getMessage());


            }
        });
		
        mRunnableForLoadMyPlanFragment = new Runnable() {
            @Override
            public void run() {
                //mViewPager_Description.setCurrentItem(1, true);
                mHandlerForLoadNowFragment.removeCallbacks(mRunnableForLoadNowFragment);
                mHandlerForLoadNowFragment.postDelayed(mRunnableForLoadNowFragment, 1200);
            }
        };
        mRunnableForLoadNowFragment = new Runnable() {
            @Override
            public void run() {
                //mViewPager_Description.setCurrentItem(2, true);
                mHandlerForLoadArticlesFragment.removeCallbacks(mRunnableForLoadArticlesFragment);
                mHandlerForLoadArticlesFragment.postDelayed(mRunnableForLoadArticlesFragment, 1200);
            }
        };
        mRunnableForLoadArticlesFragment = new Runnable() {
            @Override
            public void run() {
                //mViewPager_Description.setCurrentItem(3, true);
                mHandlerForLoadGuideFragment.removeCallbacks(mRunnableForLoadGuideFragment);
                mHandlerForLoadGuideFragment.postDelayed(mRunnableForLoadGuideFragment, 1200);
            }
        };
        mRunnableForLoadGuideFragment = new Runnable() {
            @Override
            public void run() {
                //mViewPager_Description.setCurrentItem(4, true);

            }
        };

        super.onAttach(context);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onActivityCreated", null);

        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        LogUtility.Log(TAG, "onConfigurationChanged", null);
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onCreate", null);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        LogUtility.Log(TAG, "onDestroy", null);
        super.onDestroy();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onCreateView", null);






        View view = inflater.inflate(R.layout.slide_fragmentactivity, container, false);

        final ViewPager vpPager = (ViewPager) view.findViewById(R.id.pager);
       // MyPagerAdapter adapterViewPager = new MyPagerAdapter(getActivity().getSupportFragmentManager());
     //   vpPager.setAdapter(adapterViewPager);
     //   mButtonForSignUpTravex.setAnimation(anim.slide_in_left);
        /////////////////////edited for autoscroll//////////////////////
        indicator = (CirclePageIndicator)view.findViewById(R.id.indicator);
        indicator_colors = view.getResources().getIntArray(R.array.pager_indicator_colors);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity(),Title_List,Description_List,Image_URL_List);
        //myPager = (ViewPager) findViewById(R.id.reviewpager);
        vpPager.setAdapter(adapter);
        vpPager.setPageMargin(20);
        vpPager.setCurrentItem(0);

        count=0;
        total_count=Title_List.size();
        // Timer for auto sliding
        timer  = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        if (count <= 3) {
//                            vpPager.setCurrentItem(count);
//
//                            count++;
//                        } else {
//                            count = 0;
//                            vpPager.setCurrentItem(count);
//                        }
                        vpPager.setCurrentItem(count);

                        if(count==total_count)
                        {
                            count=0;
                        }
                        else
                        {
                            count++;
                        }

                    }
                });
            }
        }, 700, 3000);

        ///////////////////////////
        indicator.setPageColor(getResources().getColor(android.R.color.white));
        indicator.setFillColor(getResources().getColor(R.color.blue_500));
        indicator.setViewPager(vpPager);
        mButtonForSignUpFacebook = (Button) view.findViewById(R.id.signup_facebook);
        //mButtonForSignUpGoogle = (Button) view.findViewById(R.id.signup_google);
        mButtonForSignUpTravex = (TextView) view.findViewById(R.id.signup_travex);
        mButtonForSignUpTravexImage = (ImageView) view.findViewById(R.id.signup_travex_image);
        mButtonForSignUpTravexRL = (RelativeLayout) view.findViewById(R.id.signup_travex_rl);
        mButtonForLoginTravex = (TextView) view.findViewById(R.id.signin_travex);
        mButtonForLoginTravexImage = (ImageView) view.findViewById(R.id.signin_travex_image);
        mButtonForLoginTravexRL = (RelativeLayout) view.findViewById(R.id.signin_travex_rl);
//        mButtonForLoginTravex.setBackgroundResource(R.drawable.btn_background);
        mButtonForskip = (TextView) view.findViewById(R.id.skip);
        btngp = (Button) view.findViewById(R.id.btngp);
//        mViewPager_Description = (ViewPager) view.findViewById(R.id.viewpagerdescription);
//
//        mViewPager_Description.setAdapter(mPagerAdapter);
//        mViewPager_Description.setCurrentItem(0, true);
        mHandlerForLoadMyPlanFragment.removeCallbacks(mRunnableForLoadMyPlanFragment);
        mHandlerForLoadMyPlanFragment.postDelayed(mRunnableForLoadMyPlanFragment, 1200);
        animationInit();
        return view;
    }

    @Override
    public void onDestroyView() {
        LogUtility.Log(TAG, "onDestroyView", null);
        mHandlerForLoadGuideFragment.removeCallbacks(mRunnableForLoadGuideFragment);
        mHandlerForLoadArticlesFragment.removeCallbacks(mRunnableForLoadArticlesFragment);
        mHandlerForLoadNowFragment.removeCallbacks(mRunnableForLoadNowFragment);
        mHandlerForLoadMyPlanFragment.removeCallbacks(mRunnableForLoadMyPlanFragment);
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        LogUtility.Log(TAG, "onDetach", null);
        super.onDetach();
    }

    @Override
    public void onPause() {
        LogUtility.Log(TAG, "onPause", null);
        super.onPause();
    }

    @Override
    public void onResume() {
        LogUtility.Log(TAG, "onResume", null);
        super.onResume();
    }

    @Override
    public void onStart() {
        LogUtility.Log(TAG, "onStart", null);
        super.onStart();
    }

    @Override
    public void onStop() {
        LogUtility.Log(TAG, "onStop", null);
        super.onStop();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onViewCreated", null);
        mButtonForSignUpFacebook.setOnClickListener(this);
        mButtonForSignUpTravex.setOnClickListener(this);
        mButtonForSignUpTravexImage.setOnClickListener(this);
        mButtonForSignUpTravexRL.setOnClickListener(this);
        mButtonForLoginTravexImage.setOnClickListener(this);
        mButtonForLoginTravex.setOnClickListener(this);
        mButtonForLoginTravexRL.setOnClickListener(this);
        mButtonForskip.setOnClickListener(this);
        btngp.setOnClickListener(this);
       preferenceManager = new PreferenceManager(getActivity());
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

           /* case R.id.signup_google: {
                LogUtility.Log(TAG, "onClick:signup_google", null);

                break;
            }*/
            case R.id.signup_travex: {
                mButtonForSignUpTravex.setVisibility(View.INVISIBLE);
                mButtonForSignUpTravexImage.setAnimation(mAnimationScale);
                mButtonForSignUpTravexImage.startAnimation(mAnimationScale);
                loginAnimCount = 2;
                break;
            }
            case R.id.signup_travex_rl: {
                mButtonForSignUpTravex.setVisibility(View.INVISIBLE);
                mButtonForSignUpTravexImage.setAnimation(mAnimationScale);
                mButtonForSignUpTravexImage.startAnimation(mAnimationScale);
                loginAnimCount = 2;
                break;
            }
            case R.id.signup_travex_image: {
                mButtonForSignUpTravex.setVisibility(View.INVISIBLE);
                mButtonForSignUpTravexImage.setAnimation(mAnimationScale);
                mButtonForSignUpTravexImage.startAnimation(mAnimationScale);
                loginAnimCount = 2;
                break;
            }
            case R.id.signin_travex: {
                mButtonForLoginTravex.setVisibility(View.INVISIBLE);
                mButtonForLoginTravexImage.setAnimation(mAnimationScale);
                mButtonForLoginTravexImage.startAnimation(mAnimationScale);
                loginAnimCount = 0;
                break;
            }
            case R.id.signin_travex_rl: {
                mButtonForLoginTravex.setVisibility(View.INVISIBLE);
                mButtonForLoginTravexImage.setAnimation(mAnimationScale);
                mButtonForLoginTravexImage.startAnimation(mAnimationScale);
                loginAnimCount = 0;
                break;
            }
            case R.id.signin_travex_image: {
                mButtonForLoginTravex.setVisibility(View.INVISIBLE);
                mButtonForLoginTravexImage.setAnimation(mAnimationScale);
                mButtonForLoginTravexImage.startAnimation(mAnimationScale);

                loginAnimCount = 0;
               preferenceManager.setboolean("logout",true);
                break;
            }
            case R.id.skip: {
                LogUtility.Log(TAG, "onClick:skip to profile activity:guest", null);

                timer.cancel();
                preferenceManager.setRegisteredUser_name("");
                preferenceManager.setRegisteredUser_email(null);
                preferenceManager.setRegisration_Id_response(null);
                preferenceManager.setWelcome_Message("");
                preferenceManager.setmember_profile_image("");
                preferenceManager.setboolean("logout",false);
                ((SignUpLoginMasterActivity) getActivity()).startProfileActivity();

                break;
            }
            case R.id.signup_facebook: {
                LogUtility.Log(TAG, "onClick:sign up facebook", null);
                if ((AccessToken.getCurrentAccessToken() == null) && (Profile.getCurrentProfile() == null)) {
                    LogUtility.Log(TAG, "onClick:going to login fb", null);
                    timer.cancel();
                    mLoginManager.logInWithReadPermissions(SignUpLoginMasterFragment.this, Arrays.asList("public_profile", "user_friends"));
                    mFacebookLogin.loginFacebookRequest();
                } else {
                    timer.cancel();
                    LogUtility.Log(TAG, "onClick:already made the login", "access token:token" + AccessToken.getCurrentAccessToken().getToken() + ",profile id:" + Profile.getCurrentProfile().getId());
                    Toast.makeText(getActivity(), "already login made to fb", Toast.LENGTH_SHORT).show();
                    mTravexApplication.showToast("already login made to fb");
                }
                break;
            }

            case R.id.btngp: {
                LogUtility.Log(TAG, "onClick:btngp", null);
                mGooglePlusLogin.connect_GooglePlayServices();
                break;

            }


        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        LogUtility.Log(TAG, "onActivityResult", null);
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GooglePlusLogin.SIGN_IN_REQUEST) {

            mGooglePlusLogin.getData(data);
        } else {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);

        }
    }

    public void startProfileActivity() {
        ((SignUpLoginMasterActivity) getActivity()).startProfileActivity();

    }

    public JSONObject getJson(ArrayList<String> namePair, ArrayList<String> valuePair) {


        return mJsonObjectMaker.getJson(namePair, valuePair);
    }


    /**
     * @param request_type
     * @param url
     * @param jsonObject
     * @param request_id
     */
    public void postJsonRequest(int request_type, String url, JSONObject jsonObject, final int request_id) {

        mNetworkManager.postJsonRequest(request_type, url, jsonObject, request_id);

    }

    public JSONObject getjsonObject(String tag) {


        return null;
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if(loginAnimCount == 0){
            mButtonForLoginTravexImage.setVisibility(View.VISIBLE);
            mButtonForLoginTravex.setVisibility(View.INVISIBLE);
            mButtonForLoginTravexImage.setImageResource(R.drawable.gsptest);
            mButtonForLoginTravexImage.setAnimation(mAnimationRotate);
            mButtonForLoginTravexImage.startAnimation(mAnimationRotate);
            loginAnimCount = 1;
        }else if(loginAnimCount == 1){
            ((SignUpLoginMasterActivity) getActivity()).load_loginTravex_fragment();
            timer.cancel();
        }else if(loginAnimCount == 2){
            mButtonForSignUpTravexImage.setVisibility(View.VISIBLE);
            mButtonForSignUpTravex.setVisibility(View.INVISIBLE);
            mButtonForSignUpTravexImage.setImageResource(R.drawable.gsptest);
            mButtonForSignUpTravexImage.setAnimation(mAnimationRotate);
            mButtonForSignUpTravexImage.startAnimation(mAnimationRotate);
            loginAnimCount = 3;
        }else{
            ((SignUpLoginMasterActivity) getActivity()).load_signupTravex_fragment();
            timer.cancel();
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    public class ScreenSlidePagerAdapter extends FragmentPagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: {
                    return mTravex_Tips_Static_Fragment;
                }
                case 1: {
                    return mTravex_Myplan_Static_Fragment;
                }
                case 2: {
                    return mTravex_Now_Static_Fragment;
                }
                case 3: {
                    return mTravex_Articles_Static_Fragment;
                }
                case 4: {
                    return mTravex_Guide_Static_Fragment;
                }

            }


            return mTravex_Tips_Static_Fragment;
        }

        @Override
        public int getCount() {
            return 5;
        }
    }
    private void simulateSuccessProgress(final CircularProgressButton button) {
        ValueAnimator widthAnimation = ValueAnimator.ofInt(1, 100);
        widthAnimation.setDuration(1500);
        widthAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        widthAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer value = (Integer) animation.getAnimatedValue();
                button.setProgress(value);
            }
        });
        widthAnimation.start();
    }

    private void animationInit() {
        mAnimationScale = AnimationUtils.loadAnimation(getActivity(), R.anim.scale_width);
        mAnimationScale.setAnimationListener(this);
        mAnimationRotate = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate);
        mAnimationRotate.setAnimationListener(this);
    }
}





