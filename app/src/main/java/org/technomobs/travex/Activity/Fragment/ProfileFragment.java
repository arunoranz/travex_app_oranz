package org.technomobs.travex.Activity.Fragment;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONObject;
import org.technomobs.travex.Activity.ProfileActivity;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Adapter.NavigationDrawerListAdapter;
import org.technomobs.travex.Controller.Interface.GetTravexLogoutResponse;
import org.technomobs.travex.Controller.NetworkManager;
import org.technomobs.travex.Controller.NetworkOptions;
import org.technomobs.travex.GooglePlus.GooglePlusLogin;
import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.Model.NavigationDrawerListItem;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.LogUtility;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by technomobs on 1/3/16.
 */
public class ProfileFragment extends Fragment implements GetTravexLogoutResponse, AdapterView.OnItemClickListener, View.OnClickListener, AdapterView.OnItemSelectedListener {
    public static final String TAG = ProfileFragment.class.getSimpleName();
    PreferenceManager mPreferenceManager = null;
    static NetworkManager mNetworkManager = null;
    TravexApplication mTravexApplication = null;


    DrawerLayout mDrawerLayout_profile = null;
    ListView mListView_drawer = null;
    ActionBarDrawerToggle mActionBarDrawerToggle_profile = null;

    NavigationDrawerListItem mNavigationDrawerListItem = null;
    String[] menu_items_drawer = null;
    ArrayList<NavigationDrawerListItem> mArrayList_container = null;
    NavigationDrawerListAdapter mNavigationDrawerListAdapter = null;


    Google_Map_Fragment mGoogle_map_fragment = null;

    private void initNavigationDrawerListItem() {
        menu_items_drawer = new String[11];
        menu_items_drawer = getResources().getStringArray(R.array.nav_drawer_items);

        mNavigationDrawerListItem = new NavigationDrawerListItem(R.drawable.profile_sample, menu_items_drawer[0], true);
        mArrayList_container.add(mNavigationDrawerListItem);
        for (int counter = 1; counter < 11; counter++) {
            mNavigationDrawerListItem = new NavigationDrawerListItem(menu_items_drawer[counter]);
            mArrayList_container.add(mNavigationDrawerListItem);
        }
        LogUtility.Log(TAG, "initNavigationDrawerListItem", "array list size:" + mArrayList_container.size());
        for (NavigationDrawerListItem i : mArrayList_container) {
            LogUtility.Log(TAG, "initNavigationDrawerListItem", "item:" + i);
        }


    }

    @Override
    public void onAttach(Context context) {
        LogUtility.Log(TAG, "onAttach", null);
        mPreferenceManager = new PreferenceManager(getActivity());
        mNetworkManager = NetworkManager.getSingleInstance(getActivity());
        mNetworkManager.setOnTravexLogoutResponseListener(this);
        mTravexApplication = TravexApplication.getInstance();
        mGoogle_map_fragment = new Google_Map_Fragment();

       // setHasOptionsMenu(true);
        mArrayList_container = new ArrayList<NavigationDrawerListItem>();
        initNavigationDrawerListItem();

       /* if (mPreferenceManager.getLogin_Type().equals(TravexApplication.TRAVEX_LOGIN)) {
            LogUtility.Log(TAG, "onAttach:travex login enabled and going to logout", null);

            if (mNetworkManager.isNetworkconnected()) {
                postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.TRAVEX_LOGOUT_URL, null, TravexApplication.REQUEST_ID_TRAVEX_LOGOUT);

            }

        }*/

        super.onAttach(context);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onActivityCreated", null);

        mNavigationDrawerListAdapter = new NavigationDrawerListAdapter(getActivity(), mArrayList_container);
        mListView_drawer.setAdapter(mNavigationDrawerListAdapter);
        mListView_drawer.setOnItemClickListener(this);
        /*((ActionBarActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((ActionBarActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);*/
        mActionBarDrawerToggle_profile = new ActionBarDrawerToggle(getActivity(), mDrawerLayout_profile, R.drawable.ic_drawer, R.string.app_name, R.string.app_name) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        mDrawerLayout_profile.setDrawerListener(mActionBarDrawerToggle_profile);
        load_Fragment(0);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        LogUtility.Log(TAG, "onConfigurationChanged", null);

        super.onConfigurationChanged(newConfig);
        mActionBarDrawerToggle_profile.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onCreate", null);

        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onCreateView", null);
        View view = inflater.inflate(R.layout.profile_fragment_layout, container, false);


        mDrawerLayout_profile = (DrawerLayout) view.findViewById(R.id.drawer);
        mListView_drawer = (ListView) view.findViewById(R.id.drawer_slider_listview);


        return view;
    }

    @Override
    public void onDestroy() {
        LogUtility.Log(TAG, "onDestroy", null);
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        LogUtility.Log(TAG, "onDestroyView", null);
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        LogUtility.Log(TAG, "onDetach", null);
        super.onDetach();
    }

    @Override
    public void onPause() {
        LogUtility.Log(TAG, "onPause", null);
        super.onPause();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        LogUtility.Log(TAG, "onRequestPermissionsResult", null);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onResume() {
        LogUtility.Log(TAG, "onResume", null);
        super.onResume();
    }

    @Override
    public void onStart() {
        LogUtility.Log(TAG, "onStart", null);
        super.onStart();
    }

    @Override
    public void onStop() {
        LogUtility.Log(TAG, "onStop", null);
        super.onStop();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onViewCreated", null);
        super.onViewCreated(view, savedInstanceState);
    }

    public void postJsonRequest(int request_type, final Map<String, String> request_header, String url, JSONObject jsonObject, final int request_id) {

        mNetworkManager.postJsonRequest(request_type, request_header, url, jsonObject, request_id);


    }

    @Override
    public void onGetTravexLogoutResponse(boolean status) {
        if (status) {
            LogUtility.Log(TAG, "onGetTravexLogoutResponse:success", null);
            ((ProfileActivity) getActivity()).startSignUp_LoginActivity();
        } else {
            LogUtility.Log(TAG, "onGetTravexLogoutResponse:failure", null);

        }

    }

    public void logout_Facebook() {

        mTravexApplication.logOutFacebook();
    }

    public void logout_Google() {

        GooglePlusLogin.signOut_Google();
    }

    public void logout_TravexAccount() {
        if (mNetworkManager.isNetworkconnected()) {
            postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.TRAVEX_LOGOUT_URL, null, TravexApplication.REQUEST_ID_TRAVEX_LOGOUT);

        }

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        LogUtility.Log(TAG, "onItemClick:drawer listview", null);
        load_Fragment(position);


    }

    public void load_Fragment(int i) {

        Fragment fragment = null;
        switch (i) {
            case 0: {


                fragment = mGoogle_map_fragment;

                break;
            }


        }
        if (fragment != null) {

            if (i == 0) {
                boolean value = ((ProfileActivity) getActivity()).isFragmentalreadyAddedInDrawersContainer(fragment, TravexApplication.MAP_FRAGMENT_TAG);
                if (!value)
                    ((ProfileActivity) getActivity()).load_googlemap_fragment();

            }

            // update selected item and title, then close the drawer
            mListView_drawer.setItemChecked(i, true);
            mListView_drawer.setSelection(i);
            // setTitle(navMenuTitles[position]);
            mDrawerLayout_profile.closeDrawer(mListView_drawer);
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

   /* AppCompatSpinner mAppCompatSpinner = null;
    ImageView mImageView_actionItem = null;
    View view_spinner = null;
    View view_logo = null;
    ArrayAdapter<String> arrayAdapter;*/

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);

        /*inflater.inflate(R.menu.profile_menu, menu);
        view_spinner = menu.findItem(R.id.country_names).getActionView();
        view_logo = menu.findItem(R.id.travex_logo).getActionView();

        LogUtility.Log(TAG, "onCreateOptionsMenu:", "view_spinner:" + view_spinner);
        LogUtility.Log(TAG, "onCreateOptionsMenu:", "view_logo:" + view_logo);


        mAppCompatSpinner = (AppCompatSpinner) view_spinner.findViewById(R.id.action_spinner);
        LogUtility.Log(TAG, "onCreateOptionsMenu", "mAppCompatSpinner:" + mAppCompatSpinner);
        arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, mPreferenceManager.getSupportedCountryNames());
        mAppCompatSpinner.setAdapter(arrayAdapter);
        mAppCompatSpinner.setOnItemSelectedListener(this);
        mImageView_actionItem = (ImageView) view_logo.findViewById(R.id.action_logo);
        LogUtility.Log(TAG, "onCreateOptionsMenu", "mImageView_actionItem:" + mImageView_actionItem);
        mImageView_actionItem.setOnClickListener(this);*/


    }

    @Override
    public void onClick(View v) {

       /* if (v.getId() == R.id.action_logo) {


            if (!((ProfileActivity) getActivity()).isFragmentalreadyAddedInDrawersContainer(mGoogle_map_fragment, TravexApplication.MAP_FRAGMENT_TAG)) {
                LogUtility.Log(TAG, "onClick:action logo clicked:loading google map fragment", null);

                ((ProfileActivity) getActivity()).load_googlemap_fragment();


            }
        }*/

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
       // LogUtility.Log(TAG, "onItemClick:action spinner:", "item:" + mAppCompatSpinner.getItemAtPosition(position).toString());


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


}
