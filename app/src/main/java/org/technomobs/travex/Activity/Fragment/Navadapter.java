package org.technomobs.travex.Activity.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;

import java.util.ArrayList;

public class Navadapter extends BaseAdapter {
	private static final int VIEW_TYPE_HEADER = 0;
	private static final int VIEW_TYPE_FOOTER = 1;
	private static final int VIEW_TYPE_DEFAULT = 2;
	private static final int VIEW_TYPE_COUNT = 3;
	private Context context;
	LayoutInflater mInflater;
	PreferenceManager preferenceManager;
	ArrayList<String> navDrawerItemsnew;
	private String apptheme;
	public int checki = 0;
	int count = 0;
	Resources res ;
	int[] imgs ;
    public static SimpleDraweeView profile_image;

	ArrayList<Integer> navDrawerItemsnewIcons;

	public Navadapter(Context context, ArrayList<String> navDrawerItems
			) {
		this.context = context;
		res = context.getResources();
		preferenceManager = new PreferenceManager(context);

		//imgs = res.getStringArray(R.array.category_icons_spinner);

		navDrawerItemsnewIcons = new ArrayList<Integer>();

		navDrawerItemsnewIcons.add(R.drawable.about);
		navDrawerItemsnewIcons.add(R.drawable.about);
		navDrawerItemsnewIcons.add(R.drawable.user_menu);
		navDrawerItemsnewIcons.add(R.drawable.plan_menu);
		navDrawerItemsnewIcons.add(R.drawable.travel_menu);
		navDrawerItemsnewIcons.add(R.drawable.article_menu);
		navDrawerItemsnewIcons.add(R.drawable.tips_menu);
		navDrawerItemsnewIcons.add(R.drawable.suggetion_menu);
//		navDrawerItemsnewIcons.add(R.drawable.support_menu);
		navDrawerItemsnewIcons.add(R.drawable.about);
		navDrawerItemsnewIcons.add(R.drawable.contact_menu);
		navDrawerItemsnewIcons.add(R.drawable.logout_menu);

//		navDrawerItems.add("settings");
//		navDrawerItems.add("Profile Name");
//		navDrawerItems.add("My Profile");
//		navDrawerItems.add("My Plan");
//		navDrawerItems.add("Travel Essential");
//		navDrawerItems.add("Article");
//		navDrawerItems.add("Travex Tips");
//		navDrawerItems.add("My Suggestions");
//		navDrawerItems.add("Support");
//		navDrawerItems.add("About Us");
//		navDrawerItems.add("Contact Us");

		this.navDrawerItemsnew = navDrawerItems;


		mInflater = (LayoutInflater) context
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
	
	}
	@Override
	  public int getViewTypeCount() {

	   return getCount();
	  }

	  @Override
	  public int getItemViewType(int position) {

	   return position;
	   }
	@Override
	public int getCount() {

		return navDrawerItemsnew.size();
	}

	@Override
	public Object getItem(int position) {
		return navDrawerItemsnew.get(position);

	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			
			holder = new ViewHolder();

			convertView = mInflater.inflate(R.layout.audit_drawer, null);

            holder.mSplashAnimation= (SimpleDraweeView)convertView.findViewById(R.id.icon2);
            profile_image=holder.mSplashAnimation;
			holder.layoutHead= (RelativeLayout) convertView.findViewById(R.id.rl3);
			holder.layoutcontent = (RelativeLayout) convertView.findViewById(R.id.rl1);
			holder.layoutUser = (RelativeLayout) convertView.findViewById(R.id.rl2);
			holder.icon_img = (ImageView)convertView.findViewById(R.id.nav_img);

			holder.txthead = (TextView) convertView.findViewById(R.id.title);
			holder.txtusername = (TextView) convertView.findViewById(R.id.textuser);

			convertView.setTag(holder);

			holder.icon_img.setImageDrawable(context.getResources().getDrawable(navDrawerItemsnewIcons.get(position)));

		} else {
			holder = (ViewHolder) convertView.getTag();
		
	///		convertView.setTag(holder);
		}


		if(navDrawerItemsnew.get(position).toString().equals("Profile Name")){
			holder.layoutcontent.setVisibility(View.GONE);
			holder.layoutHead.setVisibility(View.GONE);
			holder.layoutUser.setVisibility(View.VISIBLE);
			holder.layoutcontent.setBackgroundColor(context.getResources().getColor(R.color.red_600));
//			holder.txtusername.setText(navDrawerItemsnew.get(position));
			//holder.txtusername.setText(preferenceManager.getRegisteredUser_name());
			try {
				if (!preferenceManager.getRegisteredUser_name().equals("")) {
					holder.txtusername.setText(preferenceManager.getRegisteredUser_name());
				} else {
					holder.txtusername.setText("");

				}
			}catch(Exception  e){
				e.printStackTrace();
				holder.txtusername.setText("");
			}
			String member_profile_image = preferenceManager.getmember_profile_image();


            if(member_profile_image == null) {
                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.user).build();
                profile_image.setImageURI(imageRequest.getSourceUri());
            }
            else if(member_profile_image.equals("")) {
                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.user).build();
                profile_image.setImageURI(imageRequest.getSourceUri());
            }
            else
            {
                Uri img_uri=Uri.parse(member_profile_image);
                ImageRequest request = ImageRequest.fromUri(img_uri);

                DraweeController controller = Fresco.newDraweeControllerBuilder()
                        .setImageRequest(request)
                        .setOldController(profile_image.getController()).build();
                //Log.e(TAG, "ImagePath uri " + img_uri);

                profile_image.setController(controller);
            }

//			ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.user).build();
//			holder.mSplashAnimation.setImageURI(imageRequest.getSourceUri());
		}else if(navDrawerItemsnew.get(position).toString().equals("settings")){
			holder.layoutcontent.setVisibility(View. GONE);
			holder.layoutUser.setVisibility(View.GONE);
			holder.layoutHead.setVisibility(View.VISIBLE);


		}
		else {
			holder.layoutHead.setVisibility(View.GONE);
			holder.layoutcontent.setVisibility(View. VISIBLE);
			holder.layoutUser.setVisibility(View.GONE);
			holder.txthead.setText(navDrawerItemsnew.get(position));
			if (navDrawerItemsnew.get(position).toString().equals("My Profile")){
//				holder.layoutcontent.setBackgroundColor(Color.RED);
//				holder.txthead.setTextColor(Color.WHITE);
			}

		}


		
//		holder.txtheadto.setTextColor(Color.WHITE);
//		holder.txthead.setTextColor(Color.YELLOW);
//		holder.txtheadon.setTextColor(Color.WHITE);
		return convertView;
	}

	private static class ViewHolder {
		public RelativeLayout layoutHead,layoutUser,layoutcontent;
	
		public TextView txthead, txtusername, txtheadto;
		public ImageView imgIcon, imgIconto,icon_img;
		SimpleDraweeView mSplashAnimation;
		// String v;
	}

	
}
