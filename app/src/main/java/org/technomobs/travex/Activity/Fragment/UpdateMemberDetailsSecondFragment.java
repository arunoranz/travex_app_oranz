package org.technomobs.travex.Activity.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONObject;
import org.technomobs.travex.Activity.SignUpLoginMasterActivity;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.Controller.Interface.GetMemberUpdateProfile;
import org.technomobs.travex.Controller.JsonObjectMaker;
import org.technomobs.travex.Controller.NetworkManager;
import org.technomobs.travex.Controller.NetworkOptions;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.ListCreatorForNameValuePairRequest;
import org.technomobs.travex.Utillity.LogUtility;

import java.util.ArrayList;
import java.util.Map;


public class UpdateMemberDetailsSecondFragment extends Fragment implements View.OnClickListener, GetMemberUpdateProfile {
    public static final String TAG = UpdateMemberDetailsSecondFragment.class.getSimpleName();

    private static final String GENDER = "gender";
    private static final String STATUS = "status";
    private static final String CHILDREN = "children";
    private static final String NATIONALITY = "nationality";
    private static final String LANGUAGES = "languages";
    private static final String PREFFERED = "preffered";


    // TODO: Rename and change types of parameters
    private String gender = null;
    private String status = null;
    private String children = null;
    private String nationality = null;
    private String languages = null;
    private String preffered = null;

    private ImageView mImageView_logo = null;
    private EditText mEditText_food_preferences = null;
    private EditText mEditText_disabilities = null;
    private EditText mEditText_prefer_unwind = null;
    private EditText mEditText_phobias = null;
    private EditText mEditText_preference_to_stay = null;
    private EditText mEditText_annual_income = null;
    private EditText mEditText_others = null;
    private Button mButton_update = null;

    private String food_preferences = null;
    private String disabilities = null;
    private String prefer_unwind = null;
    private String phobias = null;
    private String stay_preferences = null;
    private String annual_income = null;
    private String others = null;
    static NetworkManager mNetworkManager = null;
    JsonObjectMaker mJsonObjectMaker = null;
    PreferenceManager mPreferenceManager = null;
    ListCreatorForNameValuePairRequest mListCreatorForNameValuePairRequest = null;

    public UpdateMemberDetailsSecondFragment() {
        // Required empty public constructor
    }


    public static UpdateMemberDetailsSecondFragment newInstance(String gender, String status, String children,
                                                                String nationality, String languages, String preffered) {
        UpdateMemberDetailsSecondFragment fragment = new UpdateMemberDetailsSecondFragment();
        Bundle args = new Bundle();
        args.putString(GENDER, gender);
        args.putString(STATUS, status);
        args.putString(CHILDREN, children);
        args.putString(NATIONALITY, nationality);
        args.putString(LANGUAGES, languages);
        args.putString(PREFFERED, preffered);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.update_member_details_second_fragment_layout, container, false);
        mImageView_logo = (ImageView) view.findViewById(R.id.travex_logo);
        mEditText_food_preferences = (EditText) view.findViewById(R.id.food_preferences);
        mEditText_disabilities = (EditText) view.findViewById(R.id.disabilities);
        mEditText_prefer_unwind = (EditText) view.findViewById(R.id.prefer_unwind);
        mEditText_phobias = (EditText) view.findViewById(R.id.phobias);
        mEditText_preference_to_stay = (EditText) view.findViewById(R.id.stay_preferences);
        mEditText_annual_income = (EditText) view.findViewById(R.id.annual_income);
        mEditText_others = (EditText) view.findViewById(R.id.others);
        mButton_update = (Button) view.findViewById(R.id.update);
        mButton_update.setOnClickListener(this);

        return view;

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getArguments() != null) {
            gender = getArguments().getString(GENDER);
            status = getArguments().getString(STATUS);
            children = getArguments().getString(CHILDREN);
            nationality = getArguments().getString(NATIONALITY);
            languages = getArguments().getString(LANGUAGES);
            preffered = getArguments().getString(PREFFERED);
            LogUtility.Log(TAG, "onCreate", "gender:" + gender + ",status:" + status + ",children:" +

                    children + ",nationality:" + nationality + ",languages:" + languages + ",preffered:" + preffered);

        }
        mJsonObjectMaker = new JsonObjectMaker(getActivity());
        mNetworkManager = NetworkManager.getSingleInstance(getActivity());
        mNetworkManager.setOnMemberUpdateResponseListener(this);
        mPreferenceManager = new PreferenceManager(getActivity());
        mListCreatorForNameValuePairRequest = new ListCreatorForNameValuePairRequest(getActivity());
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public JSONObject getjsonObject(String tag) {

        if (tag.equals(TravexApplication.MEMBER_UPDATE_PROFILE_TAG)) {
            return getJson(mListCreatorForNameValuePairRequest.getListOfMemberUpdateProfile(
                            TravexApplication.MEMBER_ID_UPDATE_PROFILE,
                            TravexApplication.MEMBER_GENDER_UPDATE_PROFILE,
                            TravexApplication.MEMBER_MARTIAL_STATUS_UPDATE_PROFILE,
                            TravexApplication.MEMBER_NO_KIDS_UPDATE_PROFILE,
                            TravexApplication.MEMBER_COUNTRY_UPDATE_PROFILE,
                            TravexApplication.MEMBER_CURRENCY_UPDATE_PROFILE,
                            TravexApplication.MEMBER_INCOME_UPDATE_PROFILE,
                            TravexApplication.MEMBER_ZIPCODE_UPDATE_PROFILE,
                            TravexApplication.MEMBER_LANGAUGES_UPDATE_PROFILE,
                            TravexApplication.MEMBER_FOODPREFERENCES_UPDATE_PROFILE,
                            TravexApplication.MEMBER_PHOBIAS_UPDATE_PROFILE,
                            TravexApplication.MEMBER_STAYPREFERENCES_UPDATE_PROFILE,
                            TravexApplication.MEMBER_DISABILITIES_UPDATE_PROFILE,
                            TravexApplication.MEMBER_UNWIND_UPDATE_PROFILE),
                    mListCreatorForNameValuePairRequest.getListOfMemberUpdateProfile(
                            mPreferenceManager.getRegisration_Id_response(),
                            gender, status, children, nationality, null, annual_income, null, languages, food_preferences, phobias, stay_preferences, disabilities, prefer_unwind));
        }


        return null;
    }

    public JSONObject getJson(ArrayList<String> namePair, ArrayList<String> valuePair) {


        return mJsonObjectMaker.getJson(namePair, valuePair);
    }

    /**
     * @param request_type
     * @param request_header
     * @param url
     * @param jsonObject
     * @param request_id
     */
    public void postJsonRequest(int request_type, final Map<String, String> request_header, String url, JSONObject jsonObject, final int request_id) {

        mNetworkManager.postJsonRequest(request_type, request_header, url, jsonObject, request_id);


    }
    /*private String food_preferences = null;
    private String disabilities = null;
    private String prefer_unwind = null;
    private String phobias = null;
    private String stay_preferences = null;
    private String annual_income = null;
    private String others = null;*/

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.update) {

            food_preferences = mEditText_food_preferences.getText().toString();
            disabilities = mEditText_disabilities.getText().toString();
            prefer_unwind = mEditText_prefer_unwind.getText().toString();
            phobias = mEditText_phobias.getText().toString();
            stay_preferences = mEditText_preference_to_stay.getText().toString();
            annual_income = mEditText_annual_income.getText().toString();
            others = mEditText_others.getText().toString();

            if (


                    (food_preferences.trim().length() > 0) && (disabilities.trim().length() > 0) &&
                            (prefer_unwind.trim().length() > 0) && (phobias.trim().length() > 0) &&
                            (stay_preferences.trim().length() > 0) && (annual_income.trim().length() > 0) &&
                            (others.trim().length() > 0)


                    )
                postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.MEMBER_UPDATE_PROFILE_URL,
                        getjsonObject(TravexApplication.MEMBER_UPDATE_PROFILE_TAG), TravexApplication.REQUEST_ID_MEMBER_UPDATE_PROFILE);
        }
    }

    @Override
    public void onGetMemberUpdateProfile(boolean status) {
        if (status) {
            Toast.makeText(getActivity(), "updated ", Toast.LENGTH_SHORT).show();
            ((SignUpLoginMasterActivity)getActivity()).startProfileActivity();
        }
    }
}
