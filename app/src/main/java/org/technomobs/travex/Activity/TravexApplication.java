package org.technomobs.travex.Activity;

import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.login.LoginManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.Utillity.LogUtility;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


/**
 * Created by FinRobotics on 13/02/16.
 */
public class TravexApplication extends Application {

    public static final String TAG = TravexApplication.class.getSimpleName();
    private static TravexApplication mTravexApplication = null;
    private RequestQueue mRequestQueue = null;
    public static final String DEVICE_TYPE = "android";
    private String DEVICE_ID = null;
    PreferenceManager mPreferenceManager = null;


    /*public static final String LOGIN_SUCCESSFUL_ONE="";
    public static final String LOGIN_NON_VERIFIED_USER_TWO="Non-verified user! Please check your email for Activation code";
    public static final String LOGIN_SUSPENDED_USER_THREE="";*/


    /**
     * For receiving callbacks about facebook
     */
    private static CallbackManager sCallbackManager = null;


    /**
     * For Getting Loginmanager instance of Facebook
     */
    private static LoginManager sLoginManager = null;
    /**
     * Tags for json request-response  in client server communication
     */
    public static final String REGISTER_TAG = "register";
    public static final String SPLASH_TAG = "splash_tag";
    public static final String FEATURE_TAG = "feature_tag";
    public static final String LOGIN_TAG = "login_tag";
    public static final String VERIFY_REGISTRATION_TAG = "verify_tag";
    public static final String GETCITY_TAG = "get_city_tag";
    public static final String MEMBER_UPDATE_PROFILE_TAG = "update_member_profile_tag";
    public static final String SUGGEST_CITY_TAG = "suggest_city_tag";
    public static final String LOGOUT_TAG = "travex_logout_tag";
    public static final String SOCIAL_LOGIN_FACEBOOK_TAG = "social_login_facebook_tag";
    public static final String SOCIAL_LOGIN_GOOGLE_TAG = "social_login_google_tag";
    public static final String AREA_LIST_TAG = "area_list_tag";
    public static final String GET_GENERAL_RESULT_TAG = "get_results_general_tag";

    public static final String GET_GENERAL_RESULT_SEARCH_TERM_TAG = "get_results_general_search_term_tag";

    public static final String GET_KEYWORDSEARCH_RESULT_TAG = "get_results_not_general_tag";
    public static final String GET_SEARCHITEM_TAG = "get_searchitem_tag";
    public static final String NOW_TAG = "now_tag";
    public static final String GET_FILTERS_TAG = "getfilters_tag";
    public static final String FILTERRESULTS_TAG = "filter_results_tag";
    public static final String GET_ARTICLES_LIST_FULLFLEDGE_TAG = "get_articles_list_full";
    public static final String GET_ARTICLES_LIST_CATEGORYBASED_TAG = "get_articles_list_categorized";
    public static final String GET_ARTICLES_LIST_FEATURED_TAG = "get_articles_list_featured";
    public static final String GET_ARTICLES_DETAILS_TAG = "get_articles_details_tag";
    public static final String GET_ESSENTIALS_LIST_TAG = "get_essentials_list_tag";
    public static final String GET_ESSENTIALS_DETAILS_TAG = "get_essentials_details_tag";
    public static final String GET_TIPS_LIST_TAG = "get_tips_list_tag";
    public static final String GET_TIPS_DETAILS_TAG = "get_tips_details_tag";
    public static final String ADD_PLAN_TAG = "add_plan_tag";
    public static final String LIST_MYPLAN_TAG = "list_plan_tag";
    public static final String ADD_SUGGESTIONS_TAG = "add_suggestions_tag";
    public static final String ABOUT_US_TAG = "about_us_tag";
    public static final String CONTACT_US_TAG = "contact_us_tag";
    public static final String ENQUIRY_TAG = "enquiry_tag";
    public static final String GET_SEARCH_TERMINLAS_TAG = "get_search_terminals";
    public static final String GET_SEARCH_DESTINATION_TAG = "get_search_destinations";


    public static final String UPDATE_PLAN_TAG = "update_plan_tag";
    public static final String DELETE_PLAN_TAG = "delete_plan_tag";
    public static final String DONE_PLAN_TAG = "done_plan_tag";
    public static final String DAY_PLAN_TAG = "day_plan_tag";
    public static final String TRAVEX_EVENTS_AND_MONTHLY_PLAN_TAG = "travex_events_and_monthly_plan_tag";
    public static final String EVENT_DETAILS_TAG = "event_details_tag";
    public static final String WRITE_REVIEW_TAG = "write_review_tag";


    public static final String PROFILE_IMAGE_UPDATE_TAG = "profile_image_update_tag";
    public static final String MEMBER_DETAILS_TAG = "member_details_tag";
    public static final String FORGET_PASSWORD_TAG = "forget_password_tag";

    /**
     * FORGET_PASSWORD NAME PAIR
     */
    public static final String MEMBER_EMAIL_FORGET_PASSWORD="member_email";

    /**
     * MEMBER_DETAILS REQUEST NAME PAIR
     */
    public static final String MEMBER_DETAILS_MEMBER_ID = "member_id";


    /**
     * WRITE_REVIEW REQUEST NAME PAIR
     */
    public static final String WRITE_REVIEW_USER_ID = "user_id";
    public static final String WRITE_REVIEW_CATEGORY = "category";
    public static final String WRITE_REVIEW_ITEM_ID = "item_id";
    public static final String WRITE_REVIEW_ITEM_NAME = "item_name";
    public static final String WRITE_REVIEW_COMMENT = "comment";
    public static final String WRITE_REVIEW_RATING = "rating";


    /**
     * EVENT_DETAILS REQUEST NAME PAIR
     */
    public static final String EVENTS_DETAILS_ID = "events_id";

    /**
     * PROFILE_IMAGE_UPDATE REQUEST NAME PAIR
     */

    public static final String PROFILE_IMAGE_UPDATE_MEMBER_ID = "member_id";
    public static final String PROFILE_IMAGE_UPDATE_MEMBER_PROFILE_IMAGE = "member_profile_image";

    /**
     * TRAVEX_EVENTS_AND_MONTHLY_PLAN REQUEST NAME PAIR
     */
    public static final String TRAVEX_EVENTS_AND_MONTHLY_PLAN_USER_ID = "user_id";
    public static final String TRAVEX_EVENTS_AND_MONTHLY_PLAN_MONTH = "month";
    public static final String TRAVEX_EVENTS_AND_MONTHLY_PLAN_YEAR = "year";

    /**
     * DAY_PLAN REQUEST NAME PAIR
     */
    public static final String USER_ID_DAY_PLAN = "user_id";
    public static final String DATE_DAY_PLAN = "date";


    /**
     * GET_SEARCH_DESTINATIONS REQUEST NAME PAIR
     */
    public static final String GET_SEARCH_DESTINATIONS_AIRPORT_ID = "airport_id";

    /**
     * GET_SEARCH_TERMINLAS REQUEST NAME PAIR
     */
    public static final String GET_SEARCH_TERMINALS_AIRPORT_ID = "airport_id";

    /**
     * DONE_PLAN REQUEST NAME PAIR
     */
    public static final String DONE_PLAN_ID = "plan_id";
    public static final String DONE_USER_ID = "user_id";


    /**
     * DELETE_PLAN REQUEST NAME PAIR
     */
    public static final String DELETE_PLAN_ID = "plan_id";
    public static final String DELETE_USER_ID = "user_id";


    /**
     * UPDATE_PLAN REQUEST NAME PAIR
     */
    public static final String UPDATE_PLAN_ID = "plan_id";

    public static final String UPDATE_PLAN_ITEM_ID = "item_id";

    public static final String UPDATE_PLAN_ITEM_NAME = "item_name";
    public static final String UPDATE_PLAN_TIME = "time";
    public static final String UPDATE_PLAN_USER_ID = "user_id";
    public static final String UPDATE_PLAN_DATE = "date";
    public static final String UPDATE_PLAN_CATEGORY = "category";
    public static final String UPDATE_PLAN_STATUS = "status";


    /**
     * ENQUIRY REQUEST NAME PAIR
     */
    public static final String ENQUIRY_NAME = "name";
    public static final String ENQUIRY_EMAIL = "email";
    public static final String ENQUIRY_CONTACT_NUMBER = "contact_number";
    public static final String ENQUIRY_SUBJECT = "subject";
    public static final String ENQUIRY_MESSAGE = "message";


    /**
     * ABOUT US REQUEST NAME PAIR
     */

    public static final String ABOUT_US_CONTACT_US_TYPE = "type";


    /**
     * ADD SUGGESTION REQUEST NAME PAIR
     */
    public static final String ADD_SUGGESTIONS_MEMBER_ID = "member_id";
    public static final String ADD_SUGGESTIONS_CATEGORYID = "category_id";
    public static final String ADD_SUGGESTIONS_SUGGESTIONDATA = "suggestion";

    /**
     * ADD PLAN REQUEST NAME PAIR
     */
    public static final String ADD_PLAN_ITEM_ID = "item_id";
    public static final String ADD_PLAN_ITEM_NAME = "item_name";
    public static final String ADD_PLAN_TIME = "time";
    public static final String ADD_PLAN_USER_ID = "user_id";
    public static final String ADD_PLAN_DATE = "date";
    public static final String ADD_PLAN_CATEGORY = "category";


    /**
     * LIST MY PLAN REQUEST NAME PAIR
     */
    public static final String LIST_MY_PLAN_MEMBER = "user_id";


    /**
     * GET TIPS LIST REQUEST NAME PAIR
     */
    public static final String GET_TIPS_LIST_CITY_ID = "city_id";
    public static final String GET_TIPS_LIST_CATEGORY_ID = "category_id";


    /**
     * GET TIPS DETAILS REQUEST NAME PAIR
     */
    public static final String GET_TIPS_DETAILS_ID = "tips_id";

    /**
     * GET ESSENTIALS LIST REQUEST NAME PAIR
     */
    public static final String GET_ESSENTIALS_LIST_CITY_ID = "city_id";


    /**
     * GET ESSENTIALS DETAILS REQUEST NAME PAIR
     */
    public static final String GET_ESSENTIALS_DETAILS_ID = "essentials_id";


    /**
     * GET ARTICLES DETAILS REQUEST NAME PAIR
     */
    public static final String GET_ARTICLES_ID = "articles_id";


    /**
     * GET ARTICLES LIST REQUEST NAME PAIR
     */
    public static final String GET_ARTICLES_LIST_CITY_ID = "city_id";
    public static final String GET_ARTICLES_LIST_MONTH = "month";
    public static final String GET_ARTICLES_LIST_YEAR = "year";
    public static final String GET_ARTICLES_LIST_CATEGORY_ID = "category_id";
    public static final String GET_ARTICLES_LIST_FEATURED = "featured";


    /**
     * FILTER RESULTS REQUEST NAME PAIR
     */
    public static final String FILTER_RESULTS_CITY_ID = "city";
    public static final String FILTER_RESULTS_SEARCH_TERM = "searchterm";
    public static final String FILTER_RESULTS_AREA = "area";
    public static final String FILTER_RESULTS_LIMIT = "limit";
    public static final String FILTER_RESULTS_ALPHABET = "alphabet";
    public static final String FILTER_RESULTS_START = "start";
    public static final String FILTER_RESULTS_SETFILTER = "setfilter";
    public static final String FILTER_RESULTS_FILTER_KEYWORD = "filter_keyword";
    public static final String FILTER_RESULTS_FILTER_RATING = "filter_rating";
    public static final String FILTER_RESULTS_CATEGORY = "category";
    /**
     * FILTERS FOR AIRLINE
     */
    public static final String FILTER_RESULTS_FILTER_AIRLINE_TERMINAL = "filter_terminal";
    public static final String FILTER_RESULTS_FILTER_AIRLINE_AIRPORT_ = "filter_airport";
    public static final String FILTER_RESULTS_FILTER_AIRLINE_DESTINATION = "filter_destination";
    /**
     * FILTERS FOR SHOPPING
     */

    public static final String FILTER_RESULTS_FILTER_SHOPPING_AREA = "filter_area";
    public static final String FILTER_RESULTS_FILTER_SHOPPING_LOCATION = "filter_location";
    public static final String FILTER_RESULTS_FILTER_SHOPPING_CATEGORY = "filter_category";


    /**
     * FILTERS FOR BUSINESS
     */

    public static final String FILTER_RESULTS_FILTER_BUSINESS_AREA = "filter_area";
    public static final String FILTER_RESULTS_FILTER_BUSINESS_LOCATION = "filter_location";
    public static final String FILTER_RESULTS_FILTER_BUSINESS_CATEGORY = "filter_category";


    /**
     * FILTERS FOR REAL ESTATE
     */
    public static final String FILTER_RESULTS_FILTER_REAL_ESTATE_AREA = "filter_area";
    public static final String FILTER_RESULTS_FILTER_REAL_ESTATE_DEVELOPER = "filter_developer";
    public static final String FILTER_RESULTS_FILTER_REAL_ESTATE_PURPOSE = "filter_purpose";
    public static final String FILTER_RESULTS_FILTER_REAL_ESTATE_STATUS = "filter_status";

    /**
     * FILTERS FOR MONEY EXCHANGE
     */
    public static final String FILTER_RESULTS_FILTER_MONEY_EXCHANGE_AREA = "filter_area";
    public static final String FILTER_RESULTS_FILTER_MONEY_EXCHANGE_TIME = "filter_time";

    /**
     * FILTERS FOR CAR RENTALS
     */
    public static final String FILTER_RESULTS_FILTER_CAR_RENTALS_AREA = "filter_area";
    public static final String FILTER_RESULTS_FILTER_CAR_RENTALS_TIME = "filter_time";

    /**
     * FILTERS FOR EMERGENCIES
     */
    public static final String FILTER_RESULTS_FILTER_EMERGENCIES_AREA = "filter_area";
    public static final String FILTER_RESULTS_FILTER_EMERGENCIES_CATEGORY = "filter_category";


    /**
     * FILTERS FOR EMBASIES AND CONSULATES
     */
    public static final String FILTER_RESULTS_FILTER_EMBASIESCONSULATES_AREA = "filter_area";
    public static final String FILTER_RESULTS_FILTER_EMBASIESCONSULATES_CATEGORY = "filter_category";
    public static final String FILTER_RESULTS_FILTER_EMBASIESCONSULATES_CONTINENT="continent";

    /**
     * FILTERS FOR TOUR AND TRAVEL
     */
    public static final String FILTER_RESULTS_FILTER_TOUR_AND_TRAVEL_AREA = "filter_area";
    public static final String FILTER_RESULTS_FILTER_TOUR_AND_TRAVEL_KEYWORD = "filter_keyword";


    /**
     * FILTERS FOR EVENTS
     */
    public static final String FILTER_RESULTS_FILTER_EVENTS_AREA = "filter_area";
    public static final String FILTER_RESULTS_FILTER_EVENTS_CATEGORY = "filter_category";
    public static final String FILTER_RESULTS_FILTER_EVENTS_TICKET_COST = "filter_tickets_cost";

    /**
     * FILTERS FOR EATING OUT
     */

    public static final String FILTER_RESULTS_FILTER_EATING_OUT_AREA = "filter_area";
    public static final String FILTER_RESULTS_FILTER_EATING_OUT_CATEGORY = "filter_category";
    public static final String FILTER_RESULTS_FILTER_EATING_OUT_ESTABLISHMENT = "filter_establishment";
    public static final String FILTER_RESULTS_FILTER_EATING_OUT_CUISINE = "filter_cuisine";
    public static final String FILTER_RESULTS_FILTER_EATING_OUT_PRICE = "filter_price";
    public static final String FILTER_RESULTS_FILTER_EATING_OUT_HALAL = "filter_halal";
    public static final String FILTER_RESULTS_FILTER_EATING_OUT_TIMING = "filter_timing";


    /**
     * FILTERS FOR STAY/HOTEL
     */

    public static final String FILTER_RESULTS_FILTER_STAYHOTEL_AREA = "filter_area";
    public static final String FILTER_RESULTS_FILTER_STAYHOTEL_STARATE = "filter_starrate";
    public static final String FILTER_RESULTS_FILTER_STAYHOTEL_FREEBIES = "filter_freebies";
    public static final String FILTER_RESULTS_FILTER_STAYHOTEL_AMINITIES = "filter_aminities";
    public static final String FILTER_RESULTS_FILTER_STAYHOTEL_PRICE = "filter_price";
    public static final String FILTER_RESULTS_FILTER_STAYHOTEL_TYPE = "filter_type";


    /**
     * FILTERS FOR NIGHT LIFE
     */

    public static final String FILTER_RESULTS_FILTER_NIGHTLIFE_AREA = "filter_area";
    public static final String FILTER_RESULTS_FILTER_NIGHTLIFE_SPECIALITIES = "filter_specialities";
    public static final String FILTER_RESULTS_FILTER_NIGHTLIFE_POLICY = "filter_policy";
    public static final String FILTER_RESULTS_FILTER_NIGHTLIFE_TIMINGS = "filter_timing";


    /**
     * FILTERS FOR SPA
     */

    public static final String FILTER_RESULTS_FILTER_SPA_AREA = "filter_area";
    public static final String FILTER_RESULTS_FILTER_SPA_SPECIALITIES = "filter_specialities";
    public static final String FILTER_RESULTS_FILTER_SPA_TIMINGS = "filter_timing";


    /**
     * FILTERS FOR THINGS TO DO
     */

    public static final String FILTER_RESULTS_FILTER_THINGS_TO_DO_AREA = "filter_area";
    public static final String FILTER_RESULTS_FILTER_THINGS_TO_DO_CATEGORY = "filter_category";
    public static final String FILTER_RESULTS_FILTER_THINGS_TO_DO_RATING = "filter_rating";

    /**
     * Json format:NOW API :request name part in name-value pair
     */
    public static final String GET_CATEGORY_NOW = "category";
    public static final String GET_CITY_NOW = "city";
    public static final String GET_AREA_NOW = "area";
    public static final String GET_USER_FLAG_NOW = "user_flag";
    public static final String GET_USER_LATITUDE_NOW = "user_lat";
    public static final String GET_USER_LONGITUDE_NOW = "user_lng";


    /**
     * Json format:GET SEARCH ITEM SPECIFIC API :request name part in name-value pair
     */
    public static final String GET_SEARCH_ITEM_CATEGORY = "category";
    public static final String GET_SEARCH_ITEM_ID = "item_id";


    /**
     * Json format:GET RESULTS KEYWORD SEARCH API :request name part in name-value pair
     */

    public static final String GET_KEYBASED_RESULT_CATEGORY = "category";
    public static final String GET_KEYBASED_RESULT_CITYID = "city";
    public static final String GET_KEYBASED_RESULT_AREA = "area";
    public static final String GET_KEYBASED_RESULT_TERM = "term";


    /**
     * Json format:GET RESULTS GENERAL SEARCH API :request name part in name-value pair
     */
    public static final String GET_GENERAL_RESULT_CATEGORY = "category";
    public static final String GET_GENERAL_RESULT_CITYID = "city";
    public static final String GET_GENERAL_RESULT_AREA = "area";
    public static final String GET_GENERAL_RESULT_SEARCHTERM = "searchterm";
    public static final String GET_GENERAL_RESULT_ALPHABET = "alphabet";
    public static final String GET_GENERAL_RESULT_START = "start";
    public static final String GET_GENERAL_RESULT_LIMIT = "limit";


    /**
     * Json format:AREA LIST :request name part in name-value pair
     */
    public static final String CITY_ID_AREA_LIST = "city_id";

    /**
     * Json format:REGISTRATION :request name part in name-value pair
     */
    public static final String FIRST_NAME_REGISTRATION = "member_first_name";
    public static final String EMAIL_REGISTRATION = "member_email";
    public static final String PASSWORD_REGISTRATION = "member_password";
    public static final String DEVICE_ID_REGISTRATION = "member_device_id";
    public static final String DEVICE_TYPE_REGISTRATION = "member_device_type";
    public static final String AUTH_METHOD_REGISTRATION = "member_auth_method";


    /**
     * Json format:LOGIN :request
     */

    public static final String EMAIL_LOGIN = "member_email";
    public static final String PASSWORD_LOGIN = "member_password";

    /**
     * Json format:City Suggest :request
     */

    public static final String EMAIL_ID_CITY_SUGGEST = "member_email";
    public static final String CITY_SUGGEST = "city";

    /**
     * Json format:MEMBER UPDATE PROFILE :request NEW ONE
     */
    public static final String MEMBER_ID_UPDATE_PROFILE = "member_id";
    public static final String MEMBER_LAST_NAME_UPDATE_PROFILE = "member_last_name";
    public static final String MEMBER_EMAIL_UPDATE_PROFILE = "member_email";
    public static final String MEMBER_MARTIAL_STATUS_UPDATE_PROFILE = "member_martial_status";
    public static final String MEMBER_PROFILE_IMAGE_UPDATE_PROFILE = "member_profile_image";
    public static final String MEMBER_FIRST_NAME_UPDATE_PROFILE = "member_first_name";
    public static final String MEMBER_GENDER_UPDATE_PROFILE = "member_gender";
    public static final String MEMBER_NO_KIDS_UPDATE_PROFILE = "member_no_kids";

    public static final String MEMBER_PROFILE_DETAILS_UPDATE_PROFILE = "profile";

    public static final String MEMBER_PROFILE_ANSWERS_UPDATE_PROFILE = "profile_answers";
    public static final String MEMBER_PROFILE_QUESTION_ID_UPDATE_PROFILE = "profile_questions_id";
    public static final String MEMBER_PROFILE_TYPE_UPDATE_PROFILE = "type";
    public static final String MEMBER_PROFILE_ANSWER_ID_UPDATE_PROFILE = "answer_id";


    /**
     * Json format:MEMBER UPDATE PROFILE :request:OLD ONE
     */
    public static final String MEMBER_COUNTRY_UPDATE_PROFILE = "member_country";
    public static final String MEMBER_CURRENCY_UPDATE_PROFILE = "member_currency";
    public static final String MEMBER_INCOME_UPDATE_PROFILE = "member_income";
    public static final String MEMBER_ZIPCODE_UPDATE_PROFILE = "member_zip_code";
    public static final String MEMBER_LANGAUGES_UPDATE_PROFILE = "member_languages";
    public static final String MEMBER_FOODPREFERENCES_UPDATE_PROFILE = "member_food_preferences";
    public static final String MEMBER_PHOBIAS_UPDATE_PROFILE = "member_phobias";
    public static final String MEMBER_STAYPREFERENCES_UPDATE_PROFILE = "member_stay_preferences";
    public static final String MEMBER_DISABILITIES_UPDATE_PROFILE = "member_disabilities";
    public static final String MEMBER_UNWIND_UPDATE_PROFILE = "member_unwind";


    /**
     * Json format:splash landing images :request
     */
    public static final String SPLASH_FEATURE_TYPE = "type";
    /**
     * Json format:verify code :request
     */
    public static final String VERIFY_CODE_MEMBER_ID = "member_id";
    public static final String VERIFY_CODE_MEMBER_CODE = "member_varification_code";
    /**
     * Json format:social login :request
     */
    public static final String SOCIAL_LOGIN_NAME = "member_first_name";
    public static final String SOCIAL_LOGIN_EMAIL = "member_email";
    public static final String SOCIAL_LOGIN_ID = "member_social_id";
    public static final String SOCIAL_LOGIN_DEVICE_ID = "member_device_id";
    public static final String SOCIAL_LOGIN_DEVICE_TYPE = "member_device_type";
    public static final String SOCIAL_LOGIN_MEMBER_AUTH_METHOD = "member_auth_method";


    /**
     * Json format:GET FILTERS  :requesT
     */
    public static final String GET_FILTERS_CITY_ID = "city_id";
    public static final String GET_FILTERS_CATEGORY_ID = "category";


    /**
     * In order to differentiate the json requests
     */
    public static final int REQUEST_ID_REGISTRATION = 100;
    public static final int REQUEST_ID_LOGIN = 101;
    public static final int REQUEST_ID_SPLASH = 102;
    public static final int REQUEST_ID_FEATURE = 103;


    public static final int REQUEST_ID_GET_CITY = 104;
    public static final int REQUEST_ID_VERIFY_REGISTRATION = 105;

    public static final int REQUEST_ID_MEMBER_UPDATE_PROFILE = 106;
    public static final int REQUEST_ID_SUGGEST_CITY = 107;
    public static final int REQUEST_ID_TRAVEX_LOGOUT = 108;
    public static final int REQUEST_ID_SOCIAL_LOGIN_FACEBOOK = 109;
    public static final int REQUEST_ID_SOCIAL_LOGIN_GOOGLE = 110;
    public static final int REQUEST_ID_AREA_LIST = 111;
    public static final int REQUEST_ID_GET_RESULTS_GENERAL = 112;
    public static final int REQUEST_ID_GET_RESULTS_KEYWORD = 113;
    public static final int REQUEST_ID_GET_SEARCH_ITEM = 114;
    public static final int REQUEST_ID_NOW = 115;
    public static final int REQUEST_ID_GET_FILTER = 116;
    public static final int REQUEST_ID_FILTER_RESULTS = 117;
    public static final int REQUEST_ID_ARTICLE_LIST_FULLFLEDGE = 118;
    public static final int REQUEST_ID_ARTICLE_LIST_CATEGORIZED = 119;
    public static final int REQUEST_ID_ARTICLE_LIST_FEATURED = 120;
    public static final int REQUEST_ID_ARTICLE_DETAILS = 121;
    public static final int REQUEST_ID_ESSENTIALS_LIST = 122;
    public static final int REQUEST_ID_ESSENTIALS_DETAILS = 123;
    public static final int REQUEST_ID_TIPS_LIST = 124;
    public static final int REQUEST_ID_TIPS_DETAILS = 125;
    public static final int REQUEST_ID_ADD_PLAN = 126;
    public static final int REQUEST_ID_LIST_MYPLAN = 127;
    public static final int REQUEST_ID_ADD_SUGGESTIONS = 128;
    public static final int REQUEST_ID_ABOUT_US = 129;
    public static final int REQUEST_ID_CONTACT_US = 130;
    public static final int REQUEST_ID_ENQUIRY = 131;
    public static final int REQUEST_ID_UPDATE_PLAN = 132;
    public static final int REQUEST_ID_DELETE_PLAN = 133;
    public static final int REQUEST_ID_SEARCH_TERMINALS = 134;
    public static final int REQUEST_ID_SEARCH_DESTINATIONS = 135;
    public static final int REQUEST_ID_PROFILE_IMAGE_UPDATE = 136;
    public static final int REQUEST_ID_DONE_PLAN = 137;
    public static final int REQUEST_ID_DAY_PLAN = 138;
    public static final int REQUEST_ID_TRAVEX_EVENTS_AND_MONTHLY_PLAN = 139;
    public static final int REQUEST_ID_EVENT_DETAILS = 140;
    public static final int REQUEST_ID_WRITE_REVIEW = 141;
    public static final int REQUEST_ID_MEMBER_DETAILS = 142;
    public static final int REQUEST_ID_FORGET_PASSWORD = 143;



    public static final String FACEBOOK_LOGIN = "facebook_login";
    public static final String GOOGLE_LOGIN = "google_login";
    public static final String TRAVEX_LOGIN = "travex_login";
    public static final String GUEST_LOGIN = "guest_login";


    public static final String MAP_FRAGMENT_TAG = "google_map_fragment";

    public static final String My_PROFILE_FRAGMENT_TAG = "My_PROFILE";

    /**
     *
     */
    @Override
    public void onCreate() {
        super.onCreate();
        mTravexApplication = this;
        mPreferenceManager = new PreferenceManager(this);

        initFrescoLib();
        initFacebookSdk();
        showTheApplicationSignature();
    }

    /**
     *
     */
    public void initFrescoLib() {
        Fresco.initialize(this);


    }

    public void initFacebookSdk() {
        FacebookSdk.sdkInitialize(getApplicationContext());
    }

    public static TravexApplication getInstance() {
        LogUtility.Log(TAG, "getInstance", "TravexApplication:" + mTravexApplication);
        return mTravexApplication;
    }


    public TravexApplication() {
        super();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private <T> RequestQueue getRequestQueue() {

        mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        return mRequestQueue;

    }

    public <T> void addRequestToQueue(Request<T> request) {

        getRequestQueue().add(request);

    }

    public void cancellAllPendingRequests(Object tag) {

        if (mRequestQueue != null) {

            getRequestQueue().cancelAll(tag);
        }
    }

    public String getDEVICE_ID() {

        DEVICE_ID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        return DEVICE_ID;
    }

    public CallbackManager getCallbackManagerForFacebook() {

        sCallbackManager = CallbackManager.Factory.create();
        LogUtility.Log(TAG, "getCallbackManagerForFacebook:", "CallbackManager:" + sCallbackManager);

        return sCallbackManager;

    }

    public LoginManager getLoginManagerForFacebook() {
        sLoginManager = LoginManager.getInstance();
        return sLoginManager;


    }

    private void showTheApplicationSignature() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "org.technomobs.travex",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                LogUtility.Log(TAG, "showTheApplicationSignature", "key hash:" + Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

    public AccessToken getAccessToken() {
        return AccessToken.getCurrentAccessToken();
    }

    public Profile getProfile() {
        return Profile.getCurrentProfile();
    }

    public void logOutFacebook() {


        getLoginManagerForFacebook().logOut();
        mPreferenceManager.setLogin_Type(null);


    }

    public boolean isDeviceBuildIsMarsmelloworNot() {
        if (Build.VERSION.SDK_INT > 22) {
            LogUtility.Log(TAG, "isDeviceBuildIsMarsmelloworNot", "marshmellow");
            return true;

        }
        LogUtility.Log(TAG, "isDeviceBuildIsMarsmelloworNot", "not marshmellow");

        return false;


    }

    Toast mTravex_toast;
    Runnable mRunnableForToast;
    Handler mHandlerForToast = new Handler();

    public void showToast(final String message) {
        mRunnableForToast = new Runnable() {
            @Override
            public void run() {
                if (mTravex_toast != null) {
                    LogUtility.Log(TAG, "showToast:going to cancel current toast", null);
                    mTravex_toast.cancel();
                    mTravex_toast = null;

                }
                if (mTravex_toast == null)

                {
                    LogUtility.Log(TAG, "showToast:going to show latest toast", null);
                    mTravex_toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG);
                    mTravex_toast.setGravity(Gravity.BOTTOM, 0, 0);
                    mTravex_toast.show();
                }
            }
        }

        ;
        mHandlerForToast.removeCallbacks(mRunnableForToast);
        mHandlerForToast.post(mRunnableForToast);


    }

    public boolean isGooglePlayServiceAvailable() {

        if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext()) == ConnectionResult.SUCCESS) {

            LogUtility.Log(TAG, "isGooglePlayServiceAvailable:yes", null);
            return true;
        }
        LogUtility.Log(TAG, "isGooglePlayServiceAvailable:no", null);

        return false;
    }


}
