package org.technomobs.travex.Activity.Fragment;

/**
 * Created by Abhi on 01-03-2016.
 */
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import org.technomobs.travex.R;

public class FragmentOne extends Fragment {
    static String tt;
    public static FragmentOne newInstance(String text) {

        Bundle args = new Bundle();
        tt=text;
        FragmentOne fragment = new FragmentOne();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        TextView t=(TextView)container.findViewById(R.id.text);
//        t.setText(tt);
        return inflater.inflate(R.layout.fragment_one_layout,
                container, false);

    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        TextView t=(TextView)getView().findViewById(R.id.text);
//       t.setText(tt);
        SimpleDraweeView mSplashAnimation= (SimpleDraweeView) getView().findViewById(R.id.sdvImage);
        ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.mipmap.bg).build();
        mSplashAnimation.setImageURI(imageRequest.getSourceUri());
    }
}