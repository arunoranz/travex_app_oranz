package org.technomobs.travex.Activity.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import org.json.JSONObject;
import org.technomobs.travex.Activity.MainActivity;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.Controller.Interface.GetCitySuggestionResponse;
import org.technomobs.travex.Controller.JsonObjectMaker;
import org.technomobs.travex.Controller.NetworkManager;
import org.technomobs.travex.Controller.NetworkOptions;
import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.ListCreatorForNameValuePairRequest;
import org.technomobs.travex.Utillity.LogUtility;

import java.util.ArrayList;

/**
 * Created by technomobs on 22/2/16.
 */
public class CitySuggestFragment extends Fragment implements GetCitySuggestionResponse {
    public static final String TAG = CitySuggestFragment.class.getSimpleName();
    static NetworkManager mNetworkManager = null;
    JsonObjectMaker mJsonObjectMaker = null;
    PreferenceManager mPreferenceManager = null;
    ListCreatorForNameValuePairRequest mListCreatorForNameValuePairRequest = null;
    TextView tv_username,tv_current_city;
    SimpleDraweeView image_user;
    /**
     * use this button to send city that user suggested to server
     */
    ImageView mButtonForYes = null;


    /**
     * if user not interested to send city to server
     */
    ImageView mButtonForNo = null;

    /**
     * For showing dialogue on entering email
     */
    EditText mEditText_email_dialogue = null;
    Button mButton_done = null;
    Button mButton_cancel = null;

    /**
     * Response dialogue view
     */
    Button mButton_done_response = null;

    DialogueForSuggestEmail mDialogueForSuggestEmail = null;

    @Override
    public void onAttach(Context context) {
        LogUtility.Log(TAG, "onAttach", null);
        super.onAttach(context);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        mJsonObjectMaker = new JsonObjectMaker(getActivity());
        mNetworkManager = NetworkManager.getSingleInstance(getActivity());
        mPreferenceManager = new PreferenceManager(getActivity());
        mListCreatorForNameValuePairRequest = new ListCreatorForNameValuePairRequest(getActivity());
        mNetworkManager.setOnGetCitySuggestListener(this);
        mDialogueForSuggestEmail = new DialogueForSuggestEmail();


        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        LogUtility.Log(TAG, "onConfigurationChanged", null);
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onCreate", null);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onCreateView", null);

        View view = inflater.inflate(R.layout.send_city_suggession_fragment_layout, container, false);
        mButtonForYes = (ImageView) view.findViewById(R.id.button2);
        mButtonForNo = (ImageView) view.findViewById(R.id.button);
        tv_username = (TextView) view.findViewById(R.id.tv_username);
        image_user = (SimpleDraweeView) view.findViewById(R.id.image_user);
        tv_current_city=(TextView) view.findViewById(R.id.tv_username);
        try
        {
            String Current_City = mPreferenceManager.getCurrentLocalityName();
            tv_current_city.setText(Current_City);
        }
        catch(Exception ex)
        {

        }


        mPreferenceManager = new PreferenceManager(getActivity().getApplicationContext());
        String member_profile_image = mPreferenceManager.getmember_profile_image();
        if(member_profile_image == null) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.user).build();
            image_user.setImageURI(imageRequest.getSourceUri());
        }
        else if(member_profile_image.equals("")) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.user).build();
            image_user.setImageURI(imageRequest.getSourceUri());
        }
        else
        {
            Uri img_uri=Uri.parse(member_profile_image);
            ImageRequest request = ImageRequest.fromUri(img_uri);

            DraweeController controller = Fresco.newDraweeControllerBuilder()
                    .setImageRequest(request)
                    .setOldController(image_user.getController()).build();
            //Log.e(TAG, "ImagePath uri " + img_uri);

            image_user.setController(controller);
        }



        String hh=mPreferenceManager.getRegisteredUser_name();
        try {
            if (!mPreferenceManager.getRegisteredUser_name().equals("")) {
                tv_username.setText(mPreferenceManager.getRegisteredUser_name());
            } else {
                tv_username.setText("");
            }
        }catch(Exception e){
            e.printStackTrace();
            tv_username.setText("");
        }
        mButtonForNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity myActivity = (MainActivity) getActivity();
                if (myActivity instanceof MainActivity) {
                    myActivity.load_Send_City_Visit_fragment();
                    //myActivity.load_Send_City_Suggession_fragment();
                }

            }
        });
        mButtonForYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialogueForSuggestEmail.showDialogueForEnterEmail();

            }
        });
        return view;
    }

    @Override
    public void onDestroy() {
        LogUtility.Log(TAG, "onDestroy", null);
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        LogUtility.Log(TAG, "onDestroyView", null);
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        LogUtility.Log(TAG, "onDetach", null);
        super.onDetach();
    }

    @Override
    public void onPause() {
        LogUtility.Log(TAG, "onPause", null);
        super.onPause();
    }

    @Override
    public void onResume() {
        LogUtility.Log(TAG, "onResume", null);
        super.onResume();
    }

    @Override
    public void onStart() {
        LogUtility.Log(TAG, "onStart", null);
        super.onStart();
    }

    @Override
    public void onStop() {
        LogUtility.Log(TAG, "onStop", null);
        super.onStop();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void postJsonRequest(int request_type, String url, JSONObject jsonObject, final int request_id) {
        mNetworkManager.postJsonRequest(request_type, url, jsonObject, request_id);


    }

    public JSONObject getjsonObject(String tag) {
        if (tag.equals(TravexApplication.SUGGEST_CITY_TAG)) {

            return getJson(mListCreatorForNameValuePairRequest
                    .getListForCitySuggestionRequest(TravexApplication.EMAIL_ID_CITY_SUGGEST, TravexApplication.CITY_SUGGEST), mListCreatorForNameValuePairRequest
                    .getListForCitySuggestionRequest(mPreferenceManager.getEmail_General_User(), mPreferenceManager.getCurrentLocalityName()));

        }
        return null;
    }

    public JSONObject getJson(ArrayList<String> namePair, ArrayList<String> valuePair) {


        return mJsonObjectMaker.getJson(namePair, valuePair);
    }


    @Override
    public void onGetCitySuggestion(boolean status) {
        LogUtility.Log(TAG, "onGetCitySuggestion", null);
       /* MainActivity myActivity = (MainActivity) getActivity();
        if (myActivity instanceof MainActivity) {
            myActivity.load_Send_City_Visit_fragment();
        }*/

        if (status) {
            LogUtility.Log(TAG, "onGetCitySuggestion:saved successfully in server", null);
            mDialogueForSuggestEmail.dismiss();
            mDialogueForSuggestEmail.showDialogueForResponseEmail();

        } else {
            LogUtility.Log(TAG, "onGetCitySuggestion:not saved successfully in server", null);
        }

    }

    public class DialogueForSuggestEmail implements View.OnClickListener {

        Dialog mDialog = null;

        public DialogueForSuggestEmail() {
            mDialog = new Dialog(getActivity());

        }

        public void showDialogueForEnterEmail() {

            Window window = mDialog.getWindow();
            window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.CENTER);


            WindowManager.LayoutParams lp = window.getAttributes();
//            lp.dimAmount = 0.7f;
//            lp.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
            mDialog.getWindow().setAttributes(lp);
            mDialog.show();


            mDialog.setContentView(R.layout.dialogue_suggest_city_layout);
            mDialog.setTitle("Travex");

            mEditText_email_dialogue = (EditText) mDialog.findViewById(R.id.emailid);
            mButton_done = (Button) mDialog.findViewById(R.id.done);
            mButton_cancel = (Button) mDialog.findViewById(R.id.cancel);
            mButton_done.setOnClickListener(this);
            mButton_cancel.setOnClickListener(this);
            mDialog.setCancelable(false);
            mDialog.show();

        }

        public void showDialogueForResponseEmail() {
            mDialog.setContentView(R.layout.dialogue_response_server_in_suggest_city);
            mDialog.setTitle("Travex");
            mButton_done_response = (Button) mDialog.findViewById(R.id.done_response);
            mButton_done_response.setOnClickListener(this);
            mDialog.setCancelable(false);
            mDialog.show();


        }


        public void dismiss() {

            mDialog.dismiss();
        }


        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.done) {
                String email = mEditText_email_dialogue.getText().toString();
                if (email.trim().length() > 0) {
                    mPreferenceManager.setEmail_General_User(email);
                    postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.CITY_SUGGESTION_URL, getjsonObject(TravexApplication.SUGGEST_CITY_TAG),
                            TravexApplication.REQUEST_ID_SUGGEST_CITY);
                }

            } else if (v.getId() == R.id.cancel) {
                dismiss();

            } else if (v.getId() == R.id.done_response) {
                dismiss();
                MainActivity myActivity = (MainActivity) getActivity();
                if (myActivity instanceof MainActivity) {
                    myActivity.load_Send_City_Visit_fragment();
                }
            }

        }
    }

}
