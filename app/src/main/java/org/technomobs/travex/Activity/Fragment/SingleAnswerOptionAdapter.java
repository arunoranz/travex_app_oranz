package org.technomobs.travex.Activity.Fragment;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.technomobs.travex.R;

import java.util.ArrayList;

/**
 * Created by oranz-pc1 on 27/4/16.
 */
public class SingleAnswerOptionAdapter extends RecyclerView.Adapter<SingleAnswerOptionAdapter.ViewHolder> {
    static Activity activity;
    ViewHolder vh;
    View rootView;
    String question_category="";
    ArrayList<String> answer_list= new ArrayList<String>();
//    String current_answer="";
    String selected_answer="";


    static CheckBox checkBoxprevious = null;

    public SingleAnswerOptionAdapter(Activity act,ArrayList<String> answer_array
            ,String selected_answer_str,String qn_category)
    {
        activity = act;
        answer_list=answer_array;
//        current_answer=current_answer_str;
        selected_answer=selected_answer_str;
        question_category = qn_category;

    }
    @Override
    public SingleAnswerOptionAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.answeroption_adapter_layout, parent, false);
        vh = new ViewHolder(rootView);
        return vh;
    }

    @Override
    public void onBindViewHolder(final SingleAnswerOptionAdapter.ViewHolder holder, int position) {
        holder.rl_main.setTag(position);
        holder.check_box.setTag(position);
        holder.check_box.setButtonDrawable(android.R.color.white);
        holder.tv_option.setText(answer_list.get(position));
        int index=0;
        if(answer_list!=null && selected_answer!=null)
        {
            if(answer_list.get(position).equals(selected_answer))
//            if(index!=-1)
            {
                holder.check_box.setChecked(true);
                holder.check_box.setButtonDrawable(R.drawable.ic_action_tick_orange);
                checkBoxprevious=holder.check_box;
            }
        }
    }

    @Override
    public int getItemCount() {
        return answer_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        final TextView tv_option;
        final CheckBox check_box;
        final RelativeLayout rl_main;
        public ViewHolder(View v) {
            super(v);
            tv_option = (TextView) v.findViewById(R.id.tv_option);
            check_box = (CheckBox) v.findViewById(R.id.check_box);
            rl_main = (RelativeLayout) v.findViewById(R.id.rl_main);
            check_box.setOnClickListener(this);
            rl_main.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            int position= (int) v.getTag();

            switch (v.getId()) {

                case R.id.rl_main:
                    CheckBoxClick(position);
                    break;
                case R.id.check_box:
                    CheckBoxClick(position);
                    break;
                default:
                    break;
            }
        }

        public void CheckBoxClick(int position)
        {
            try
            {
//                if(question_category.equals("select"))
                {
                    if (checkBoxprevious == null) {
                        checkBoxprevious = check_box;
                    }

                    if (check_box.isChecked()) {
                        check_box.setChecked(false);
                        check_box.setButtonDrawable(android.R.color.white);
                    } else {
                        check_box.setChecked(true);
                        check_box.setButtonDrawable(R.drawable.ic_action_tick_orange);
                    }

                    if (checkBoxprevious != check_box) {
                        checkBoxprevious.setChecked(false);
                        checkBoxprevious.setButtonDrawable(android.R.color.white);
                        checkBoxprevious = check_box;
                    }
                }
//                else
//                {
//                    if(check_box.isChecked())
//                    {
//                        check_box.setChecked(false);
//                        check_box.setButtonDrawable(android.R.color.white);
//                    }
//                    else
//                    {
//                        check_box.setChecked(true);
//                        check_box.setButtonDrawable(R.drawable.ic_action_tick_orange);
//                    }
//                }

                if(check_box.isChecked())
                {
                    ProfileFragmentTwo.selected_answer = answer_list.get(position);
                }


            }
            catch (Exception exc)
            {
                Log.d("", exc.toString());
            }

        }
    }
}
