package org.technomobs.travex.Activity.Fragment;


import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONObject;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Controller.Interface.AboutUsResponse;
import org.technomobs.travex.Controller.JsonObjectMaker;
import org.technomobs.travex.Controller.NetworkManager;
import org.technomobs.travex.Controller.NetworkOptions;
import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.ListCreatorForNameValuePairRequest;
import org.technomobs.travex.Utillity.LogUtility;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Abhi on 07-03-2016.
 */
public class AboutUsFragment extends Fragment implements AboutUsResponse,Animation.AnimationListener  {
    static Activity activity;
    ArrayList prgmName;
    ArrayList<String> prgmNameList;
    ArrayList<String> desigList;
    ArrayList<String> prgmImages_URL;
    TextView abouttext,aboutusdesc;
    GridView gridview;

    //API Credentials
    static NetworkManager mNetworkManager = null;
    public static final String TAG = TravelEssential.class.getSimpleName();
    JsonObjectMaker mJsonObjectMaker = null;
    ListCreatorForNameValuePairRequest mListCreatorForNameValuePairRequest = null;


    RelativeLayout animation_layout;
    View rootView;
    Animation rotateOne, rotateTwo, rotateThree;

 //   public static String [] prgmNameList={"Let Us C","c++","JAVA","Jsp","Microsoft .Net","Android","PHP","Jquery","JavaScript"};
 //   public static int [] prgmImages={R.drawable.pic1,R.drawable.pic2,R.drawable.pic3,R.drawable.pic4,R.drawable.pic5,R.drawable.pic6,R.drawable.pic2,R.drawable.pic4,R.drawable.pic1,R.drawable.pic3,R.drawable.pic4,R.drawable.pic5,R.drawable.pic6,R.drawable.pic2,R.drawable.pic4,R.drawable.pic1};

    public static AboutUsFragment newInstance(Activity act){
        AboutUsFragment fragment = new AboutUsFragment();
        activity=act;

        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        initiating_APICredentials();

        rootView = inflater.inflate(R.layout.aboutus_fragment, container, false);
        abouttext = (TextView) rootView.findViewById(R.id.abouttext);
        aboutusdesc = (TextView) rootView.findViewById(R.id.aboutusdesc);
        gridview = (GridView) rootView.findViewById(R.id.gridview);
        animation_layout=(RelativeLayout)rootView.findViewById(R.id.settings_animation);

        //Calling AboutUs API
        postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.ABOUT_US_URL,
                getjsonObject(TravexApplication.ABOUT_US_TAG),
                TravexApplication.REQUEST_ID_ABOUT_US);


        animationInit();
        animationLoading();

        Runnable run = new Runnable() {
            @Override
            public void run() {
                animation_layout.setVisibility(View.GONE);
            }
        };
        Handler handler = new Handler();
        handler.postDelayed(run, 2000);

        prgmNameList=new ArrayList<>();
        prgmImages_URL=new ArrayList<>();
        desigList=new ArrayList<>();




        return rootView;
    }

    public void initiating_APICredentials()
    {
        mJsonObjectMaker = new JsonObjectMaker(getActivity());
        mListCreatorForNameValuePairRequest = new ListCreatorForNameValuePairRequest(getActivity());
        mNetworkManager = NetworkManager.getSingleInstance(getActivity());
        mNetworkManager.setOnAboutUsResponseListener(this);
    }

    @Override
    public void onGetAboutUsResponse(boolean status, HashMap<Integer, HashMap<Integer, ArrayList<String>>> description_teamDetails) {
        if (status) {
            HashMap<Integer, HashMap<Integer, ArrayList<String>>> map = description_teamDetails;


            LogUtility.Log(TAG, "onGetAboutUsResponse: :success", null);

            LogUtility.Log(TAG, "onGetAboutUsResponse:success:description details", null);
            HashMap<Integer, ArrayList<String>> map_description_only = map.get(0);

            if (map_description_only != null) {
                for (int i = 0; i < map_description_only.size(); i++) {
                    ArrayList<String> list = map_description_only.get(i);
                    LogUtility.Log(TAG, "onGetAboutUsResponse", "list:" + (i + 1));
                    if (list != null) {
                        for (int j = 0; j < list.size(); j++) {

                            LogUtility.Log(TAG, "onGetAboutUsResponse:description details:", "list item:" + list.get(j));
                            String Title=list.get(2);
                            String Desc = list.get(3);
                            if(Title == null) Title="";
                            if(!Title.equals(""))
                            {
                                abouttext.setText(Title);
                                aboutusdesc.setText(Desc);
                            }
                        }
                    } else {
                        LogUtility.Log(TAG, "onGetAboutUsResponse: :success:description details is null in list", null);

                    }

                }
            } else {
                LogUtility.Log(TAG, "onGetAboutUsResponse: :success:description details is null full", null);

            }


            HashMap<Integer, ArrayList<String>> map_team_only = map.get(1);
            prgmNameList.clear();
            prgmImages_URL.clear();
            desigList.clear();
            if (map_team_only != null) {
                for (int i = 0; i < map_team_only.size(); i++) {
                    ArrayList<String> list = map_team_only.get(i);
                    LogUtility.Log(TAG, "onGetAboutUsResponse", "list:" + (i + 1));
                    if (list != null) {
                        for (int j = 0; j < list.size(); j++) {
                            prgmNameList.add(list.get(2));
                            desigList.add(list.get(3));
                            prgmImages_URL.add(list.get(1));
                            break;
                            //LogUtility.Log(TAG, "onGetAboutUsResponse:team details:", "list item:" + list.get(j));
                        }
                            gridview.setAdapter(new CustomAdapter(getActivity().getApplicationContext(), prgmNameList,prgmImages_URL,desigList));

                    } else {
                        LogUtility.Log(TAG, "onGetAboutUsResponse: :success:team details is null in list", null);

                    }

                }
            } else {
                LogUtility.Log(TAG, "onGetAboutUsResponse: :success:team details is null full", null);

            }


        } else {
            LogUtility.Log(TAG, "onGetAboutUsResponse: :failure", null);
        }
    }

    public JSONObject getjsonObject(String tag) {
        if (tag.equals(TravexApplication.ABOUT_US_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfAboutUs
                            (TravexApplication.ABOUT_US_CONTACT_US_TYPE

                            ),
                    mListCreatorForNameValuePairRequest.getListOfAboutUs("aboutus")

            );

        }
        return null;
    }


    public void postJsonRequest(int request_type, String url, JSONObject jsonObject, final int request_id) {

        mNetworkManager.postJsonRequest(request_type, url, jsonObject, request_id);
    }

    public JSONObject getJson(ArrayList<String> namePair, ArrayList<String> valuePair) {


        return mJsonObjectMaker.getJson(namePair, valuePair);
    }



    //Animation Functions
    public void animationLoading(){
        animation_layout.setVisibility(View.VISIBLE);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                ImageView iv1 = (ImageView) rootView.findViewById(R.id.settings_one);
                ImageView iv2 = (ImageView) rootView.findViewById(R.id.settings_two);
                ImageView iv3 = (ImageView) rootView.findViewById(R.id.settings_three);

                iv1.setAnimation(rotateOne);
                iv2.setAnimation(rotateTwo);
                iv3.setAnimation(rotateThree);

                iv1.startAnimation(rotateOne);
                iv2.startAnimation(rotateTwo);
                iv3.startAnimation(rotateThree);
            }
        };
        Handler mHandler = new Handler();
        mHandler.postDelayed(runnable, 100);

    }

    public  void  animationInit(){
        rotateOne = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_one);
        rotateOne.setAnimationListener(this);
        rotateTwo= AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_two);
        rotateTwo.setAnimationListener(this);
        rotateThree= AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_three);
        rotateThree.setAnimationListener(this);
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }


    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
