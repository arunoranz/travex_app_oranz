package org.technomobs.travex.Activity.Fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.R;

import java.util.ArrayList;

/**
 * Created by Abhi on 09-03-2016.
 */
public class InterestedAdapter  extends RecyclerView.Adapter<InterestedAdapter.ViewHolder> {
    private static final int VIEW_TYPE_HEADER = 0;
    private static final int VIEW_TYPE_FOOTER = 1;
    private static final int VIEW_TYPE_DEFAULT = 2;
    private static final int VIEW_TYPE_COUNT = 3;
    private Context context;
    LayoutInflater mInflater;
    private String backStateName;
    String selected_category,selected_categoryid;
    ArrayList<CategoryListItems> categoryItemsnew;
    private String apptheme,category_name;
    public int checki = 0;
    int count = 0,rating;
    ArrayList<String> datelist,member_name,image_list,mtitle,msubtitle,mratetext,mratecount,mdesp,rating_list;
    Activity act;
    ViewHolder vh;
    ScrollView adapter_scroll;
    public InterestedAdapter(Activity activity,Context context,ArrayList<String> tv_title, ArrayList<String> tv_subtitle
            , ArrayList<String> rate_text,ArrayList<String> rate_count, ArrayList<String> desp, ArrayList<String> memberimg, ArrayList<String> membername,ArrayList<String> datelist) {
        this.context = context;
        this.mtitle=tv_title;
        this.msubtitle=tv_subtitle;
        this.mratetext=rate_text;
        this.mratecount=rate_count;
        this.mdesp=desp;
        this.image_list=memberimg;
        this.member_name=membername;
        this.rating_list=rate_text;
        this.datelist=datelist;
        this.categoryItemsnew = categoryItemsnew;
        this.act=activity;

        mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rating_and_review_adapter, null);
        adapter_scroll = (ScrollView) v.findViewById(R.id.adapter_scroll);
        SearchResultFragmentSecond.main_scroll.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View p_v, MotionEvent p_event) {
                adapter_scroll.getParent().requestDisallowInterceptTouchEvent(
                        false);
                // We will have to follow above for all scrollable contents
                return false;
            }
        });
        adapter_scroll.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View p_v, MotionEvent p_event) {
                // this will disallow the touch request for parent scroll on
                // touch of child view
                p_v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
        vh = new ViewHolder(v);
        return vh;
    }


    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {
        // return bookmarkPostId.size();
        return mtitle.size();
    }


    public Object getItem(int position) {
//        return categoryItemsnew.get(position);
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        {




            holder.icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                int b=Integer.parseInt(v.getTag().toString());
//                MapShowingFragment.selected_category = categoryItemsnew.get(b).getTitle();
//                MapShowingFragment.selected_categoryId = categoryItemsnew.get(b).getId();
//                MapShowingFragment.category_text.setText(categoryItemsnew.get(b).getTitle());
//                MapShowingFragment.cat_list.setVisibility(View.GONE);
                    Toast.makeText(context, "clicked", Toast.LENGTH_LONG).show();
//                    Fragment fragment=SearchResultFragmentSecond.newInstance(context);
//
//
//                    if (fragment != null) {
//                        FragmentManager fragmentManager = act.getFragmentManager();
//                        fragmentManager.beginTransaction()
//                                .replace(R.id.frame_container, fragment).commit();
//
//                        backStateName = fragment.getClass().getName();
//                        boolean fragmentPopped = fragmentManager
//                                .popBackStackImmediate(backStateName, 0);
//
//
//                    }
                }
            });
            holder.title.setText(member_name.get(position));
            holder.subtitle.setText(datelist.get(position));
            holder.ratingBar.setRating(Integer.parseInt(rating_list.get(position)));
            //       holder.ratingtext.setText(mratetext.get(position));
            //      holder.rating_count.setText(mratecount.get(position));
            holder.tv_desp.setText(mdesp.get(position));
            if(image_list.size()>0) {
                String image_name = image_list.get(position);
                Uri img_uri;
                String image_url = AppConstants.GET_RESULTS_GENERAL_IMAGES_URL + "/" + getCategoryItemDirectorynameForImageAssetUrl(MapShowingFragment.selected_category) + "/review/" + image_name;
                img_uri = Uri.parse(image_url);
                ImageRequest request = ImageRequest.fromUri(img_uri);
                DraweeController controller = Fresco.newDraweeControllerBuilder()
                        .setImageRequest(request)
                        .setOldController(holder.icon.getController()).build();
                holder.icon.setController(controller);
            }


//            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.logo).build();
//            holder.icon.setImageURI(imageRequest.getSourceUri());
//		holder.txtheadto.setTextColor(Color.WHITE);
//		holder.txthead.setTextColor(Color.YELLOW);
//		holder.txtheadon.setTextColor(Color.WHITE);

        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout Rl;
        //    public TextView head, price, distance, head1,ratingtext;

        //     public TextView camera_count,tv_desp,rating_count,subtitle,title;
        RatingBar ratingBar;
        public TextView tv_desp,subtitle,title;
        SimpleDraweeView icon;
        public ViewHolder(View v) {
            super(v);
            icon= (SimpleDraweeView)v.findViewById(R.id.icon2);
            //     ratingtext= (TextView)v.findViewById(R.id.ratingtext);
            title= (TextView)v.findViewById(R.id.title);
            subtitle= (TextView)v.findViewById(R.id.subtitle);
            ratingBar= (RatingBar)v.findViewById(R.id.ratingBar);
            //      rating_count= (TextView)v.findViewById(R.id.rating_count);
            tv_desp= (TextView)v.findViewById(R.id.tv_desp);

        }
    }
    public String getCategoryItemDirectorynameForImageAssetUrl(String item) {
        if (item.equals("Airline")) {
            return "airlines";
        } else if (item.equals("Shopping")) {
            return "shopping";
        } else if (item.equals("Business")) {
            return "business";
        } else if (item.equals("Real Estate")) {
            return "realestate";
        } else if (item.equals("Money Exchange")) {
            return "moneyexchangers";
        } else if (item.equals("Car Rentals")) {
            return "carrentals";
        } else if (item.equals("Embasies & Consulates")) {
            return "embassy";
        } else if (item.equals("Tour & Travel")) {
            return "tourandtravel";
        } else if (item.equals("Events")) {
            return "events";
        } else if (item.equals("Eating Out")) {
            return "eatingout";
        } else if (item.equals("Stay/Hotels")) {
            return "hotels";
        } else if (item.equals("nightlife")) {
            return "airlines";
        } else if (item.equals("Spa")) {
            return "spa";
        } else if (item.equals("Things To Do")) {
            return "thingstodo";
        } else if (item.equals("Emergencies")) {
            return "emergencies";
        }
        return null;
    }
}
