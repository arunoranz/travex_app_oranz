package org.technomobs.travex.Activity.Fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import org.technomobs.travex.R;

import java.util.ArrayList;

/**
 * Created by ARUN on 03/26/2016.
 */
public class AnsweroptionAdapter extends RecyclerView.Adapter<AnsweroptionAdapter.ViewHolder> {


    static Activity activity;
    ViewHolder vh;
    View rootView;
    String question_type="";
    int question_position=0;
    ArrayList<String> current_answer_array= new ArrayList<String>();
    ArrayList<String> current_answer_id_array= new ArrayList<String>();
    ArrayList<String> selected_answer_id_array= new ArrayList<String>();


    static CheckBox checkBoxprevious = null;

    public AnsweroptionAdapter(Activity act,ArrayList<String> current_answer
            ,ArrayList<String> current_answer_id,ArrayList<String> selected_answer_id,String qn_type,int qn_pos)
    {
        activity = act;
        current_answer_array=current_answer;
        current_answer_id_array=current_answer_id;
        selected_answer_id_array=selected_answer_id;
        question_type = qn_type;
        question_position = qn_pos;

    }
    @Override
    public AnsweroptionAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.answeroption_adapter_layout, parent, false);
        vh = new ViewHolder(rootView);
        return vh;
    }

    @Override
    public void onBindViewHolder(final AnsweroptionAdapter.ViewHolder holder, int position) {
        holder.rl_main.setTag(position);
        holder.check_box.setTag(position);
        holder.check_box.setButtonDrawable(android.R.color.white);
        holder.tv_option.setText(current_answer_array.get(position));
        if(selected_answer_id_array != null)
        {
            for(int i=0;i<selected_answer_id_array.size();i++)
            {
                String selected_answer_id = "";
                selected_answer_id=selected_answer_id_array.get(i);

                if(current_answer_id_array.get(position).equals(selected_answer_id))
                {
                    holder.check_box.setChecked(true);
                    holder.check_box.setButtonDrawable(R.drawable.ic_action_tick_orange);
                    checkBoxprevious = holder.check_box;
                    break;
                }
            }

        }

    }

    @Override
    public int getItemCount() {
        return current_answer_array.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        final TextView tv_option;
        final CheckBox check_box;
        final RelativeLayout rl_main;
        public ViewHolder(View v) {
            super(v);
            tv_option = (TextView) v.findViewById(R.id.tv_option);
            check_box = (CheckBox) v.findViewById(R.id.check_box);
            rl_main = (RelativeLayout) v.findViewById(R.id.rl_main);
            check_box.setOnClickListener(this);
            rl_main.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            int position= (int) v.getTag();

            switch (v.getId()) {

                case R.id.rl_main:
                    CheckBoxClick(position);
                    break;
                case R.id.check_box:
                    CheckBoxClick(position);
                    break;
                default:
                    break;
            }
        }

        public void CheckBoxClick(int position)
        {
            try
            {
                if(question_type.equals("select")) {
                    if (checkBoxprevious == null) {
                        checkBoxprevious = check_box;
                    }

                    if (check_box.isChecked()) {
                        check_box.setChecked(false);
                        check_box.setButtonDrawable(android.R.color.white);
                    } else {
                        check_box.setChecked(true);
                        check_box.setButtonDrawable(R.drawable.ic_action_tick_orange);
                    }

                    if (checkBoxprevious != check_box) {
                        checkBoxprevious.setChecked(false);
                        checkBoxprevious.setButtonDrawable(android.R.color.white);
                        checkBoxprevious = check_box;
                    }
                }
                else
                {
                    if(check_box.isChecked())
                    {
                        check_box.setChecked(false);
                        check_box.setButtonDrawable(android.R.color.white);
                    }
                    else
                    {
                        check_box.setChecked(true);
                        check_box.setButtonDrawable(R.drawable.ic_action_tick_orange);
                    }
                }
                ArrayList<String> current_selected_ans_id_array =  ProfileFragmentTwo.selected_answer_id_list.get(question_position);
                ArrayList<String> current_selected_ans_array =  ProfileFragmentTwo.selected_answer_list.get(question_position);
                int index=0;
                if(question_type.equals("select")) {
                    current_selected_ans_id_array=null;
                    current_selected_ans_array=null;
                }
                String current_answer_id = current_answer_id_array.get(position);
                String current_answer = current_answer_array.get(position);
                if(current_answer_id!=null) {

                    if(current_selected_ans_id_array==null)
                    {
                        index=-1;
                        current_selected_ans_id_array=new ArrayList<>();
                        current_selected_ans_array=new ArrayList<>();
                    }
                    else
                    {
                        index=current_selected_ans_id_array.indexOf(current_answer_id);
                    }
                }

                if(check_box.isChecked())
                {
                    if (index < 0)
                    {
//                            current_selected_ans_id_array.set(index,current_answer_id);
//                            current_selected_ans_array.set(index,current_answer);
                        current_selected_ans_id_array.add(current_answer_id);
                        current_selected_ans_array.add(current_answer);

                    }

                }
                else
                {

                        if (index >= 0)
                        {
                            current_selected_ans_id_array.remove(index);
                            current_selected_ans_array.remove(index);
                        }
                }

                ProfileFragmentTwo.selected_answer_id_list.remove(question_position);
                ProfileFragmentTwo.selected_answer_list.remove(question_position);
                ProfileFragmentTwo.selected_answer_id_list.add(question_position, current_selected_ans_id_array);
                ProfileFragmentTwo.selected_answer_list.add(question_position, current_selected_ans_array);

            }
            catch (Exception exc)
            {
                Log.d("",exc.toString());
            }

        }
    }
}
