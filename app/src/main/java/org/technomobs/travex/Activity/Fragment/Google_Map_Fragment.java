package org.technomobs.travex.Activity.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Spinner;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.technomobs.travex.Activity.ProfileActivity;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Adapter.CategoriesSpinnerAdapter;
import org.technomobs.travex.Controller.Interface.AboutUsResponse;
import org.technomobs.travex.Controller.Interface.AddPlanResponse;
import org.technomobs.travex.Controller.Interface.AddSuggestionsResponse;
import org.technomobs.travex.Controller.Interface.ContactUsResponse;
import org.technomobs.travex.Controller.Interface.DayPlanResponse;
import org.technomobs.travex.Controller.Interface.DeletePlanResponse;
import org.technomobs.travex.Controller.Interface.EnquiryResponse;
import org.technomobs.travex.Controller.Interface.GetArticleDetailsResponse;
import org.technomobs.travex.Controller.Interface.GetArticlesListResponse;
import org.technomobs.travex.Controller.Interface.GetDonePlanResponse;
import org.technomobs.travex.Controller.Interface.GetEssentialListResponse;
import org.technomobs.travex.Controller.Interface.GetEssentialsDetailsResponse;
import org.technomobs.travex.Controller.Interface.GetEventDetailsResponse;
import org.technomobs.travex.Controller.Interface.GetFilterResponse;
import org.technomobs.travex.Controller.Interface.GetFilterResultsResponse;
import org.technomobs.travex.Controller.Interface.GetForgetPasswordResponse;
import org.technomobs.travex.Controller.Interface.GetMemberDetailsResponse;
import org.technomobs.travex.Controller.Interface.GetMemberUpdateProfile;
import org.technomobs.travex.Controller.Interface.GetNowResponse;
import org.technomobs.travex.Controller.Interface.GetResponseSearchResultsGeneral;
import org.technomobs.travex.Controller.Interface.GetResponseSearchResultsKeyBased;
import org.technomobs.travex.Controller.Interface.GetSearchDestinations;
import org.technomobs.travex.Controller.Interface.GetSearchItemResponse;
import org.technomobs.travex.Controller.Interface.GetSearchTerminals;
import org.technomobs.travex.Controller.Interface.GetTipsDetailsResponse;
import org.technomobs.travex.Controller.Interface.GetTipsListResponse;
import org.technomobs.travex.Controller.Interface.GetTravexEventsAndMonthlyPlanResponse;
import org.technomobs.travex.Controller.Interface.GetWriteReviewResponse;
import org.technomobs.travex.Controller.Interface.ListPlanResponse;
import org.technomobs.travex.Controller.Interface.ProfileImageUpdateResponse;
import org.technomobs.travex.Controller.Interface.UpdatePlanResponse;
import org.technomobs.travex.Controller.JsonObjectMaker;
import org.technomobs.travex.Controller.NetworkManager;
import org.technomobs.travex.Controller.NetworkOptions;
import org.technomobs.travex.Gson.JsonObjectRequestCreator;
import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.Model.CategorySpinnerItem;
import org.technomobs.travex.Model.SpinnerCategoryViewHolder;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.ListCreatorForNameValuePairRequest;
import org.technomobs.travex.Utillity.LogUtility;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by technomobs on 4/3/16.
 */
public class Google_Map_Fragment extends Fragment implements AdapterView.OnItemSelectedListener,
        AdapterView.OnItemClickListener, GetResponseSearchResultsGeneral, GetResponseSearchResultsKeyBased,
        GetNowResponse, GetFilterResponse, GetSearchItemResponse, GetFilterResultsResponse,
        GetArticlesListResponse, GetArticleDetailsResponse, GetEssentialListResponse,
        GetEssentialsDetailsResponse, GetTipsListResponse, GetTipsDetailsResponse,
        AddPlanResponse, ListPlanResponse,
        AddSuggestionsResponse,
        AboutUsResponse,
        ContactUsResponse, EnquiryResponse,
        UpdatePlanResponse, DeletePlanResponse,
        GetSearchTerminals, GetSearchDestinations,
        ProfileImageUpdateResponse, GetDonePlanResponse,
        DayPlanResponse, GetTravexEventsAndMonthlyPlanResponse,
        GetEventDetailsResponse, GetWriteReviewResponse, GetMemberUpdateProfile, GetMemberDetailsResponse,GetForgetPasswordResponse


{
    public static final String TAG = Google_Map_Fragment.class.getSimpleName();
    GoogleMap mGoogleMap = null;
    SupportMapFragment mSupportMapFragment = null;
    ListView mListview_search_keyword = null;
    ListView mListView_search_general = null;
    ArrayAdapter<String> mArrayAdapter_categories = null;
    Spinner mSpinner_category = null;
    SearchView mSearchView_term = null;
    ArrayList<CategorySpinnerItem> mArrayList_categoryitem = null;
    String[] categories_items = null;
    TypedArray mTypedArray_category_icons = null;
    CategorySpinnerItem mCategorySpinnerItem = null;
    CategoriesSpinnerAdapter mCategoriesSpinnerAdapter = null;
    String category_item = null;
    static NetworkManager mNetworkManager = null;
    JsonObjectMaker mJsonObjectMaker = null;
    ListCreatorForNameValuePairRequest mListCreatorForNameValuePairRequest = null;
    PreferenceManager mPreferenceManager = null;
    String country = null;
    String country_id = null;
    String query = null;
    JsonObjectRequestCreator mJsonObjectRequestCreator = null;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        LogUtility.Log(TAG, "onActivityCreated", null);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LogUtility.Log(TAG, "onActivityResult", null);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        LogUtility.Log(TAG, "onAttach", null);
        mJsonObjectMaker = new JsonObjectMaker(getActivity());
        mNetworkManager = NetworkManager.getSingleInstance(getActivity());
        mNetworkManager.setOnGetResponseSearchGeneralResultsListener(this);
        mNetworkManager.setOnGetSearchResultsKeywordbasedListener(this);
        mNetworkManager.setOnGetNowResponseListener(this);
        mNetworkManager.setOnGetFilterResponseListener(this);
        mNetworkManager.setOnGetSearchItemResponseListener(this);
        mNetworkManager.setOnGetFilterResultsResponseListener(this);
        mNetworkManager.setOnGetArticlesListResponseListener(this);
        mNetworkManager.setOnGetArticlesDetailsResponseListener(this);
        mNetworkManager.setOnGetEssentialsListResponseListener(this);
        mNetworkManager.setOnGetEssentialsDetailsResponseListener(this);
        mNetworkManager.setOnGetTipsListResponseListener(this);
        mNetworkManager.setOnGetTipsDetailsResponseListener(this);
        mNetworkManager.setOnAddPlanResponseListener(this);
        mNetworkManager.setOnListPlanResponseListener(this);
        mNetworkManager.setOnAddSuggestionsResponseListener(this);
        mNetworkManager.setOnAboutUsResponseListener(this);
        mNetworkManager.setOnContactUsResponseListener(this);
        mNetworkManager.setOnEnquiryResponseListener(this);

        mNetworkManager.setOnUpdatePlanResponseListener(this);
        mNetworkManager.setOnDeletePlanResponseListener(this);
        mNetworkManager.setOnSearchTerminalsResponseListener(this);
        mNetworkManager.setOnSearchDestinationsResponseListener(this);
        mNetworkManager.setOnProfileImageUpdateResponseListener(this);
        mNetworkManager.setOnGetDonePlanResponseListener(this);
        mNetworkManager.setOnGetDayPlanResponseListener(this);
        mNetworkManager.setOnGetTravexEventPlanResponseListener(this);
        mNetworkManager.setOnGetEventDetailsResponseListener(this);
        mNetworkManager.setOnGetWriteReviewResponseListener(this);
        mNetworkManager.setOnMemberUpdateResponseListener(this);
        mNetworkManager.setOnGetMemberDetailsResponseListener(this);
        mNetworkManager.setOnGetForgetPasswordResponseListener(this);

        mPreferenceManager = new PreferenceManager(getActivity());
        mListCreatorForNameValuePairRequest = new ListCreatorForNameValuePairRequest(getActivity());
        categories_items = new String[15];
        categories_items = getResources().getStringArray(R.array.category_items_spinner);
        mTypedArray_category_icons = getResources().obtainTypedArray(R.array.category_icons_spinner);
        mArrayList_categoryitem = new ArrayList<CategorySpinnerItem>();
        for (int i = 0; i < 15; i++) {
            mCategorySpinnerItem = new CategorySpinnerItem(mTypedArray_category_icons.getResourceId(i, -1), categories_items[i]);
            mArrayList_categoryitem.add(mCategorySpinnerItem);
        }

        mCategoriesSpinnerAdapter = new CategoriesSpinnerAdapter(getActivity(), mArrayList_categoryitem);

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LogUtility.Log(TAG, "onConfigurationChanged", null);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtility.Log(TAG, "onCreate", null);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onCreateView", null);
        View view = inflater.inflate(R.layout.google_map_fragment_layout, container, false);

        mListview_search_keyword = (ListView) view.findViewById(R.id.search_keyword_listview);
        mListView_search_general = (ListView) view.findViewById(R.id.search_general_listview);
        mSpinner_category = (Spinner) view.findViewById(R.id.spinner_categories);
        mSearchView_term = (SearchView) view.findViewById(R.id.search_term);
        mSpinner_category.setOnItemSelectedListener(this);
        mSpinner_category.setAdapter(mCategoriesSpinnerAdapter);

        mSupportMapFragment = new SupportMapFragment() {

            @Override
            public void onActivityCreated(Bundle savedInstanceState) {
                super.onActivityCreated(savedInstanceState);

                mSupportMapFragment.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap googleMap) {
                        LogUtility.Log(TAG, "onMapReady:", "googleMap:" + googleMap);
                        mGoogleMap = googleMap;
                    }
                });

            }
        };
        getChildFragmentManager().beginTransaction().add(R.id.main, mSupportMapFragment).commitAllowingStateLoss();

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LogUtility.Log(TAG, "onDestroy", null);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        LogUtility.Log(TAG, "onDestroyView", null);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        LogUtility.Log(TAG, "onDetach", null);
    }

    @Override
    public void onPause() {
        super.onPause();
        LogUtility.Log(TAG, "onPause", null);
    }

    @Override
    public void onResume() {
        super.onResume();
        LogUtility.Log(TAG, "onResume", null);
    }

    @Override
    public void onStart() {
        super.onStart();
        LogUtility.Log(TAG, "onStart", null);
    }

    @Override
    public void onStop() {
        super.onStop();
        LogUtility.Log(TAG, "onStop", null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LogUtility.Log(TAG, "onViewCreated", null);
        mSearchView_term.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                LogUtility.Log(TAG, "onQueryTextSubmit", "query:" + query);
                // onEnterKeyClick();

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                LogUtility.Log(TAG, "onQueryTextChange", "newText:" + newText);
                query = newText;

                return true;
            }
        });

        mSearchView_term.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtility.Log(TAG, "mSearchView_term:onClick", null);

            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        LogUtility.Log(TAG, "onItemSelected:", null);

        SpinnerCategoryViewHolder spinnerCategoryViewHolder = new SpinnerCategoryViewHolder();
        category_item = mSpinner_category.getItemAtPosition(position).toString();
        LogUtility.Log(TAG, "onItemSelected:", "category_item:" + category_item);
        spinnerCategoryViewHolder = (SpinnerCategoryViewHolder) view.getTag();
        category_item = spinnerCategoryViewHolder.getmTextView().getText().toString();
        LogUtility.Log(TAG, "onItemSelected", "item selected from holder:" + category_item);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        LogUtility.Log(TAG, "onNothingSelected:", null);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        LogUtility.Log(TAG, "onItemClick:", null);


    }

    public void postJsonRequest(int request_type, String url, JSONObject jsonObject, final int request_id) {
        LogUtility.Log(TAG, "postJsonRequest", null);
        mNetworkManager.postJsonRequest(request_type, url, jsonObject, request_id);
    }


    public JSONObject getjsonObject(String tag) {

        if (tag.equals(TravexApplication.SUGGEST_CITY_TAG)) {

            return getJson(mListCreatorForNameValuePairRequest
                    .getListForCitySuggestionRequest(TravexApplication.EMAIL_ID_CITY_SUGGEST, TravexApplication.CITY_SUGGEST), mListCreatorForNameValuePairRequest
                    .getListForCitySuggestionRequest("arunkumar121924@gmail.com", mPreferenceManager.getCurrentLocalityName()));

        }

      else  if (tag.equals(TravexApplication.LOGIN_TAG)) {


            return getJson(mListCreatorForNameValuePairRequest.getListForLoginRequest(TravexApplication.EMAIL_LOGIN, TravexApplication.PASSWORD_LOGIN),

                    mListCreatorForNameValuePairRequest.getListForLoginRequest("arunkumar121924@gmail.com", "1234")
            );

        }

        else if (tag.equals(TravexApplication.REGISTER_TAG)) {
            return getJson(mListCreatorForNameValuePairRequest.getListForRegistrationRequest(TravexApplication.FIRST_NAME_REGISTRATION, TravexApplication.EMAIL_REGISTRATION, TravexApplication.PASSWORD_REGISTRATION, TravexApplication.DEVICE_ID_REGISTRATION, TravexApplication.DEVICE_TYPE_REGISTRATION, TravexApplication.AUTH_METHOD_REGISTRATION),
                    mListCreatorForNameValuePairRequest.getListForRegistrationRequest("arun", "arunkumar121924@gmail.com", "1234", TravexApplication.getInstance().getDEVICE_ID(), TravexApplication.DEVICE_TYPE, "app"));

        } else if (tag.equals(TravexApplication.VERIFY_REGISTRATION_TAG)) {

            return getJson(mListCreatorForNameValuePairRequest.getListForVerifyCodeRequest(TravexApplication.VERIFY_CODE_MEMBER_ID, TravexApplication.VERIFY_CODE_MEMBER_CODE), mListCreatorForNameValuePairRequest.getListForVerifyCodeRequest(mPreferenceManager.getRegisration_Id_response(), "1025"));


        } else if (tag.equals(TravexApplication.SPLASH_TAG)) {

            return getJson(mListCreatorForNameValuePairRequest.getListOfSplashLanding_ImagesRequest(TravexApplication.SPLASH_FEATURE_TYPE), mListCreatorForNameValuePairRequest.getListOfSplashLanding_ImagesRequest("splashscreen"));

        } else if (tag.equals(TravexApplication.FEATURE_TAG)) {


            return getJson(mListCreatorForNameValuePairRequest.getListOfFeatureImagesRequest(TravexApplication.SPLASH_FEATURE_TYPE), mListCreatorForNameValuePairRequest.getListOfFeatureImagesRequest("features"));


        } else if (tag.equals(TravexApplication.GET_GENERAL_RESULT_TAG)) {

            return getJson(mListCreatorForNameValuePairRequest.getListOfGeneralResultsSearchApi
                    (TravexApplication.GET_GENERAL_RESULT_CATEGORY,
                            TravexApplication.GET_GENERAL_RESULT_CITYID,
                            TravexApplication.GET_GENERAL_RESULT_AREA,
                            TravexApplication.GET_GENERAL_RESULT_SEARCHTERM,
                            TravexApplication.GET_GENERAL_RESULT_ALPHABET,
                            TravexApplication.GET_GENERAL_RESULT_START,
                            TravexApplication.GET_GENERAL_RESULT_LIMIT), mListCreatorForNameValuePairRequest.getListOfGeneralResultsSearchApi
                    (category_item, country_id, "", "", "", "", ""));

        } else if (tag.equals(TravexApplication.GET_GENERAL_RESULT_SEARCH_TERM_TAG)) {

            return getJson(mListCreatorForNameValuePairRequest.getListOfGeneralResultsSearchApi
                    (TravexApplication.GET_GENERAL_RESULT_CATEGORY,
                            TravexApplication.GET_GENERAL_RESULT_CITYID,
                            TravexApplication.GET_GENERAL_RESULT_AREA,
                            TravexApplication.GET_GENERAL_RESULT_SEARCHTERM,
                            TravexApplication.GET_GENERAL_RESULT_ALPHABET,
                            TravexApplication.GET_GENERAL_RESULT_START,
                            TravexApplication.GET_GENERAL_RESULT_LIMIT), mListCreatorForNameValuePairRequest.getListOfGeneralResultsSearchApi
                    (category_item, "pvgl_vq_1", "", "xrljbeqf_vq_3", "", "", ""));

        } else if (tag.equals(TravexApplication.GET_KEYWORDSEARCH_RESULT_TAG)) {

            return getJson(mListCreatorForNameValuePairRequest.getListOfKeywordbasedlResultsSearchApi
                    (TravexApplication.GET_KEYBASED_RESULT_CATEGORY,
                            TravexApplication.GET_KEYBASED_RESULT_CITYID,
                            TravexApplication.GET_KEYBASED_RESULT_AREA,
                            TravexApplication.GET_KEYBASED_RESULT_TERM

                    ), mListCreatorForNameValuePairRequest.getListOfKeywordbasedlResultsSearchApi
                    (category_item, country_id, "", query));
        } else if (tag.equals(TravexApplication.GET_SEARCHITEM_TAG)) {

            return getJson(mListCreatorForNameValuePairRequest
                            .getListOfSearchItemSpecificApi(TravexApplication.GET_SEARCH_ITEM_CATEGORY, TravexApplication.GET_SEARCH_ITEM_ID),
                    mListCreatorForNameValuePairRequest.getListOfSearchItemSpecificApi("Airline", "nveyvarf_vq_1"));


        } else if (tag.equals(TravexApplication.NOW_TAG)) {

            return getJson(mListCreatorForNameValuePairRequest
                            .getListOfSearchNowApi(TravexApplication.GET_CATEGORY_NOW,
                                    TravexApplication.GET_CITY_NOW,
                                    TravexApplication.GET_AREA_NOW,
                                    TravexApplication.GET_USER_FLAG_NOW,
                                    TravexApplication.GET_USER_LATITUDE_NOW,
                                    TravexApplication.GET_USER_LONGITUDE_NOW





                                    ),
                    mListCreatorForNameValuePairRequest.getListOfSearchNowApi
                            ("Shopping", country_id, "","1","25.154","55.254"));


        } else if (tag.equals(TravexApplication.GET_FILTERS_TAG)) {

            return getJson(mListCreatorForNameValuePairRequest
                            .getListOfSearchApiGetFilters(TravexApplication.GET_FILTERS_CITY_ID,
                                    TravexApplication.GET_FILTERS_CATEGORY_ID
                            ),
                    mListCreatorForNameValuePairRequest.getListOfSearchApiGetFilters("pvgl_vq_3", category_item));


        } else if (tag.equals(TravexApplication.FILTERRESULTS_TAG)) {

            if (category_item.equals("Airline"))

            {

                LogUtility.Log(TAG, "category item selected is airline:getjsonObject", null);

                return getJson(mListCreatorForNameValuePairRequest
                                .getListOfSearchApiGetFilterResults_Airline_Shopping_Business_Events_Spa(
                                        TravexApplication.FILTER_RESULTS_CITY_ID,
                                        TravexApplication.FILTER_RESULTS_SEARCH_TERM,
                                        TravexApplication.FILTER_RESULTS_AREA,
                                        TravexApplication.FILTER_RESULTS_LIMIT,
                                        TravexApplication.FILTER_RESULTS_ALPHABET,
                                        TravexApplication.FILTER_RESULTS_START,
                                        TravexApplication.FILTER_RESULTS_SETFILTER,
                                        TravexApplication.FILTER_RESULTS_FILTER_KEYWORD,
                                        TravexApplication.FILTER_RESULTS_FILTER_RATING,
                                        TravexApplication.FILTER_RESULTS_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_AIRLINE_TERMINAL,
                                        TravexApplication.FILTER_RESULTS_FILTER_AIRLINE_AIRPORT_,
                                        TravexApplication.FILTER_RESULTS_FILTER_AIRLINE_DESTINATION


                                ),
                        mListCreatorForNameValuePairRequest.getListOfSearchApiGetFilterResults_Airline_Shopping_Business_Events_Spa("pvgl_vq_3", "", "", "", "", "", "1", "", "", category_item, "", "", ""));
            } else if (category_item.equals("Shopping"))

            {

                LogUtility.Log(TAG, "category item selected is shopping:getjsonObject", null);

                return getJson(mListCreatorForNameValuePairRequest
                                .getListOfSearchApiGetFilterResults_Airline_Shopping_Business_Events_Spa(
                                        TravexApplication.FILTER_RESULTS_CITY_ID,
                                        TravexApplication.FILTER_RESULTS_SEARCH_TERM,
                                        TravexApplication.FILTER_RESULTS_AREA,
                                        TravexApplication.FILTER_RESULTS_LIMIT,
                                        TravexApplication.FILTER_RESULTS_ALPHABET,
                                        TravexApplication.FILTER_RESULTS_START,
                                        TravexApplication.FILTER_RESULTS_SETFILTER,
                                        TravexApplication.FILTER_RESULTS_FILTER_KEYWORD,
                                        TravexApplication.FILTER_RESULTS_FILTER_RATING,
                                        TravexApplication.FILTER_RESULTS_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_SHOPPING_AREA,
                                        TravexApplication.FILTER_RESULTS_FILTER_SHOPPING_LOCATION,
                                        TravexApplication.FILTER_RESULTS_FILTER_SHOPPING_CATEGORY


                                ),
                        mListCreatorForNameValuePairRequest.getListOfSearchApiGetFilterResults_Airline_Shopping_Business_Events_Spa(country_id, "", "", "", "", "", "1", "", "", category_item, "", "", ""));
            } else if (category_item.equals("Business"))

            {

                LogUtility.Log(TAG, "category item selected is business:getjsonObject", null);

                return getJson(mListCreatorForNameValuePairRequest
                                .getListOfSearchApiGetFilterResults_Airline_Shopping_Business_Events_Spa(
                                        TravexApplication.FILTER_RESULTS_CITY_ID,
                                        TravexApplication.FILTER_RESULTS_SEARCH_TERM,
                                        TravexApplication.FILTER_RESULTS_AREA,
                                        TravexApplication.FILTER_RESULTS_LIMIT,
                                        TravexApplication.FILTER_RESULTS_ALPHABET,
                                        TravexApplication.FILTER_RESULTS_START,
                                        TravexApplication.FILTER_RESULTS_SETFILTER,
                                        TravexApplication.FILTER_RESULTS_FILTER_KEYWORD,
                                        TravexApplication.FILTER_RESULTS_FILTER_RATING,
                                        TravexApplication.FILTER_RESULTS_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_BUSINESS_AREA,
                                        TravexApplication.FILTER_RESULTS_FILTER_BUSINESS_LOCATION,
                                        TravexApplication.FILTER_RESULTS_FILTER_BUSINESS_CATEGORY


                                ),
                        mListCreatorForNameValuePairRequest.getListOfSearchApiGetFilterResults_Airline_Shopping_Business_Events_Spa(country_id, "", "", "", "", "", "1", "", "", category_item, "", "", ""));
            } else if (category_item.equals("Events"))

            {

                LogUtility.Log(TAG, "category item selected is events:getjsonObject", null);

                return getJson(mListCreatorForNameValuePairRequest
                                .getListOfSearchApiGetFilterResults_Airline_Shopping_Business_Events_Spa(
                                        TravexApplication.FILTER_RESULTS_CITY_ID,
                                        TravexApplication.FILTER_RESULTS_SEARCH_TERM,
                                        TravexApplication.FILTER_RESULTS_AREA,
                                        TravexApplication.FILTER_RESULTS_LIMIT,
                                        TravexApplication.FILTER_RESULTS_ALPHABET,
                                        TravexApplication.FILTER_RESULTS_START,
                                        TravexApplication.FILTER_RESULTS_SETFILTER,
                                        TravexApplication.FILTER_RESULTS_FILTER_KEYWORD,
                                        TravexApplication.FILTER_RESULTS_FILTER_RATING,
                                        TravexApplication.FILTER_RESULTS_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_EVENTS_AREA,
                                        TravexApplication.FILTER_RESULTS_FILTER_EVENTS_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_EVENTS_TICKET_COST


                                ),
                        mListCreatorForNameValuePairRequest.getListOfSearchApiGetFilterResults_Airline_Shopping_Business_Events_Spa(country_id, "", "", "", "", "", "1", "", "", category_item, "", "", ""));
            } else if (category_item.equals("Spa"))

            {

                LogUtility.Log(TAG, "category item selected is spa:getjsonObject", null);

                return getJson(mListCreatorForNameValuePairRequest
                                .getListOfSearchApiGetFilterResults_Airline_Shopping_Business_Events_Spa(
                                        TravexApplication.FILTER_RESULTS_CITY_ID,
                                        TravexApplication.FILTER_RESULTS_SEARCH_TERM,
                                        TravexApplication.FILTER_RESULTS_AREA,
                                        TravexApplication.FILTER_RESULTS_LIMIT,
                                        TravexApplication.FILTER_RESULTS_ALPHABET,
                                        TravexApplication.FILTER_RESULTS_START,
                                        TravexApplication.FILTER_RESULTS_SETFILTER,
                                        TravexApplication.FILTER_RESULTS_FILTER_KEYWORD,
                                        TravexApplication.FILTER_RESULTS_FILTER_RATING,
                                        TravexApplication.FILTER_RESULTS_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_SPA_AREA,
                                        TravexApplication.FILTER_RESULTS_FILTER_SPA_SPECIALITIES,
                                        TravexApplication.FILTER_RESULTS_FILTER_SPA_TIMINGS


                                ),
                        mListCreatorForNameValuePairRequest.getListOfSearchApiGetFilterResults_Airline_Shopping_Business_Events_Spa(country_id, "", "", "", "", "", "1", "", "", category_item, "", "", ""));
            } else if (category_item.equals("Things To Do"))

            {

                LogUtility.Log(TAG, "category item selected is things to do:getjsonObject", null);

                return getJson(mListCreatorForNameValuePairRequest
                                .getListOfSearchApiGetFilterResults_moneyExchange_carRentals_Emergencies_Embasies_ThingsToDo(
                                        TravexApplication.FILTER_RESULTS_CITY_ID,
                                        TravexApplication.FILTER_RESULTS_SEARCH_TERM,
                                        TravexApplication.FILTER_RESULTS_AREA,
                                        TravexApplication.FILTER_RESULTS_LIMIT,
                                        TravexApplication.FILTER_RESULTS_ALPHABET,
                                        TravexApplication.FILTER_RESULTS_START,
                                        TravexApplication.FILTER_RESULTS_SETFILTER,
                                        TravexApplication.FILTER_RESULTS_FILTER_KEYWORD,
                                        TravexApplication.FILTER_RESULTS_FILTER_RATING,
                                        TravexApplication.FILTER_RESULTS_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_THINGS_TO_DO_AREA,
                                        TravexApplication.FILTER_RESULTS_FILTER_THINGS_TO_DO_CATEGORY


                                ),
                        mListCreatorForNameValuePairRequest.getListOfSearchApiGetFilterResults_moneyExchange_carRentals_Emergencies_Embasies_ThingsToDo(country_id, "", "", "", "", "", "1", "", "", category_item, "", ""));
            } else if (category_item.equals("Money Exchange"))

            {

                LogUtility.Log(TAG, "category item selected is money exchange:getjsonObject", null);

                return getJson(mListCreatorForNameValuePairRequest
                                .getListOfSearchApiGetFilterResults_moneyExchange_carRentals_Emergencies_Embasies_ThingsToDo(
                                        TravexApplication.FILTER_RESULTS_CITY_ID,
                                        TravexApplication.FILTER_RESULTS_SEARCH_TERM,
                                        TravexApplication.FILTER_RESULTS_AREA,
                                        TravexApplication.FILTER_RESULTS_LIMIT,
                                        TravexApplication.FILTER_RESULTS_ALPHABET,
                                        TravexApplication.FILTER_RESULTS_START,
                                        TravexApplication.FILTER_RESULTS_SETFILTER,
                                        TravexApplication.FILTER_RESULTS_FILTER_KEYWORD,
                                        TravexApplication.FILTER_RESULTS_FILTER_RATING,
                                        TravexApplication.FILTER_RESULTS_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_MONEY_EXCHANGE_AREA,
                                        TravexApplication.FILTER_RESULTS_FILTER_MONEY_EXCHANGE_TIME


                                ),
                        mListCreatorForNameValuePairRequest.getListOfSearchApiGetFilterResults_moneyExchange_carRentals_Emergencies_Embasies_ThingsToDo(country_id, "", "", "", "", "", "1", "", "", category_item, "", ""));
            } else if (category_item.equals("Car Rentals"))

            {

                LogUtility.Log(TAG, "category item selected is car rentals:getjsonObject", null);

                return getJson(mListCreatorForNameValuePairRequest
                                .getListOfSearchApiGetFilterResults_moneyExchange_carRentals_Emergencies_Embasies_ThingsToDo(
                                        TravexApplication.FILTER_RESULTS_CITY_ID,
                                        TravexApplication.FILTER_RESULTS_SEARCH_TERM,
                                        TravexApplication.FILTER_RESULTS_AREA,
                                        TravexApplication.FILTER_RESULTS_LIMIT,
                                        TravexApplication.FILTER_RESULTS_ALPHABET,
                                        TravexApplication.FILTER_RESULTS_START,
                                        TravexApplication.FILTER_RESULTS_SETFILTER,
                                        TravexApplication.FILTER_RESULTS_FILTER_KEYWORD,
                                        TravexApplication.FILTER_RESULTS_FILTER_RATING,
                                        TravexApplication.FILTER_RESULTS_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_CAR_RENTALS_AREA,
                                        TravexApplication.FILTER_RESULTS_FILTER_CAR_RENTALS_TIME


                                ),
                        mListCreatorForNameValuePairRequest.getListOfSearchApiGetFilterResults_moneyExchange_carRentals_Emergencies_Embasies_ThingsToDo(country_id, "", "", "", "", "", "1", "", "", category_item, "", ""));
            } else if (category_item.equals("Emergencies"))

            {

                LogUtility.Log(TAG, "category item selected is emergencies:getjsonObject", null);

                return getJson(mListCreatorForNameValuePairRequest
                                .getListOfSearchApiGetFilterResults_moneyExchange_carRentals_Emergencies_Embasies_ThingsToDo(
                                        TravexApplication.FILTER_RESULTS_CITY_ID,
                                        TravexApplication.FILTER_RESULTS_SEARCH_TERM,
                                        TravexApplication.FILTER_RESULTS_AREA,
                                        TravexApplication.FILTER_RESULTS_LIMIT,
                                        TravexApplication.FILTER_RESULTS_ALPHABET,
                                        TravexApplication.FILTER_RESULTS_START,
                                        TravexApplication.FILTER_RESULTS_SETFILTER,
                                        TravexApplication.FILTER_RESULTS_FILTER_KEYWORD,
                                        TravexApplication.FILTER_RESULTS_FILTER_RATING,
                                        TravexApplication.FILTER_RESULTS_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_EMERGENCIES_AREA,
                                        TravexApplication.FILTER_RESULTS_FILTER_EMERGENCIES_CATEGORY


                                ),
                        mListCreatorForNameValuePairRequest.getListOfSearchApiGetFilterResults_moneyExchange_carRentals_Emergencies_Embasies_ThingsToDo(country_id, "", "", "", "", "", "1", "", "", category_item, "", ""));
            } else if (category_item.equals("Embasies & Consulates"))

            {

                LogUtility.Log(TAG, "category item selected is embasies and consulates:getjsonObject", null);

                return getJson(mListCreatorForNameValuePairRequest
                                .getListOfSearchApiGetFilterResults_moneyExchange_carRentals_Emergencies_Embasies_ThingsToDo(
                                        TravexApplication.FILTER_RESULTS_CITY_ID,
                                        TravexApplication.FILTER_RESULTS_SEARCH_TERM,
                                        TravexApplication.FILTER_RESULTS_AREA,
                                        TravexApplication.FILTER_RESULTS_LIMIT,
                                        TravexApplication.FILTER_RESULTS_ALPHABET,
                                        TravexApplication.FILTER_RESULTS_START,
                                        TravexApplication.FILTER_RESULTS_SETFILTER,
                                        TravexApplication.FILTER_RESULTS_FILTER_KEYWORD,
                                        TravexApplication.FILTER_RESULTS_FILTER_RATING,
                                        TravexApplication.FILTER_RESULTS_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_EMBASIESCONSULATES_AREA,
                                        TravexApplication.FILTER_RESULTS_FILTER_EMBASIESCONSULATES_CATEGORY


                                ),
                        mListCreatorForNameValuePairRequest.
                                getListOfSearchApiGetFilterResults_moneyExchange_carRentals_Emergencies_Embasies_ThingsToDo
                                        (country_id, "", "", "", "", "", "1", "", "", category_item, "", "")
                );
            } else if (category_item.equals("Tour & Travel"))

            {

                LogUtility.Log(TAG, "category item selected is  tour and travel:getjsonObject", null);

                return getJson(mListCreatorForNameValuePairRequest
                                .getListOfSearchApiGetFilterResults_TourAndTravel(
                                        TravexApplication.FILTER_RESULTS_CITY_ID,
                                        TravexApplication.FILTER_RESULTS_SEARCH_TERM,
                                        TravexApplication.FILTER_RESULTS_AREA,
                                        TravexApplication.FILTER_RESULTS_LIMIT,
                                        TravexApplication.FILTER_RESULTS_ALPHABET,
                                        TravexApplication.FILTER_RESULTS_START,
                                        TravexApplication.FILTER_RESULTS_SETFILTER,
                                        TravexApplication.FILTER_RESULTS_FILTER_KEYWORD,
                                        TravexApplication.FILTER_RESULTS_FILTER_RATING,
                                        TravexApplication.FILTER_RESULTS_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_TOUR_AND_TRAVEL_AREA


                                ),
                        mListCreatorForNameValuePairRequest.
                                getListOfSearchApiGetFilterResults_TourAndTravel(
                                        country_id, "", "", "", "", "", "1", "", "", category_item, "")
                );
            } else if (category_item.equals("Real Estate"))

            {

                LogUtility.Log(TAG, "category item selected is  real estatae:getjsonObject", null);


                return getJson(mListCreatorForNameValuePairRequest
                                .getListOfSearchApiGetFilterResults_RealEstate_NightLife(
                                        TravexApplication.FILTER_RESULTS_CITY_ID,
                                        TravexApplication.FILTER_RESULTS_SEARCH_TERM,
                                        TravexApplication.FILTER_RESULTS_AREA,
                                        TravexApplication.FILTER_RESULTS_LIMIT,
                                        TravexApplication.FILTER_RESULTS_ALPHABET,
                                        TravexApplication.FILTER_RESULTS_START,
                                        TravexApplication.FILTER_RESULTS_SETFILTER,
                                        TravexApplication.FILTER_RESULTS_FILTER_KEYWORD,
                                        TravexApplication.FILTER_RESULTS_FILTER_RATING,
                                        TravexApplication.FILTER_RESULTS_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_REAL_ESTATE_AREA,
                                        TravexApplication.FILTER_RESULTS_FILTER_REAL_ESTATE_DEVELOPER,
                                        TravexApplication.FILTER_RESULTS_FILTER_REAL_ESTATE_PURPOSE,

                                        TravexApplication.FILTER_RESULTS_FILTER_REAL_ESTATE_STATUS


                                ),
                        mListCreatorForNameValuePairRequest.getListOfSearchApiGetFilterResults_RealEstate_NightLife(country_id, "", "", "", "", "", "1", "", "", category_item, "", "", "", ""));
            } else if (category_item.equals("Nightlife"))

            {

                LogUtility.Log(TAG, "category item selected is  nightlife:getjsonObject", null);

                return getJson(mListCreatorForNameValuePairRequest
                                .getListOfSearchApiGetFilterResults_RealEstate_NightLife(
                                        TravexApplication.FILTER_RESULTS_CITY_ID,
                                        TravexApplication.FILTER_RESULTS_SEARCH_TERM,
                                        TravexApplication.FILTER_RESULTS_AREA,
                                        TravexApplication.FILTER_RESULTS_LIMIT,
                                        TravexApplication.FILTER_RESULTS_ALPHABET,
                                        TravexApplication.FILTER_RESULTS_START,
                                        TravexApplication.FILTER_RESULTS_SETFILTER,
                                        TravexApplication.FILTER_RESULTS_FILTER_KEYWORD,
                                        TravexApplication.FILTER_RESULTS_FILTER_RATING,
                                        TravexApplication.FILTER_RESULTS_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_NIGHTLIFE_AREA,
                                        TravexApplication.FILTER_RESULTS_FILTER_NIGHTLIFE_SPECIALITIES,
                                        TravexApplication.FILTER_RESULTS_FILTER_NIGHTLIFE_POLICY,

                                        TravexApplication.FILTER_RESULTS_FILTER_NIGHTLIFE_TIMINGS


                                ),
                        mListCreatorForNameValuePairRequest.getListOfSearchApiGetFilterResults_RealEstate_NightLife(country_id, "", "", "", "", "", "1", "", "", category_item, "", "", "", ""));
            } else if (category_item.equals("Stay/Hotels"))

            {

                LogUtility.Log(TAG, "category item selected is  stay/hotels:getjsonObject", null);

                return getJson(mListCreatorForNameValuePairRequest
                                .getListOfSearchApiGetFilterResults_stayHotels(
                                        TravexApplication.FILTER_RESULTS_CITY_ID,
                                        TravexApplication.FILTER_RESULTS_SEARCH_TERM,
                                        TravexApplication.FILTER_RESULTS_AREA,
                                        TravexApplication.FILTER_RESULTS_LIMIT,
                                        TravexApplication.FILTER_RESULTS_ALPHABET,
                                        TravexApplication.FILTER_RESULTS_START,
                                        TravexApplication.FILTER_RESULTS_SETFILTER,
                                        TravexApplication.FILTER_RESULTS_FILTER_KEYWORD,
                                        TravexApplication.FILTER_RESULTS_FILTER_RATING,
                                        TravexApplication.FILTER_RESULTS_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_STAYHOTEL_AREA,
                                        TravexApplication.FILTER_RESULTS_FILTER_STAYHOTEL_STARATE,
                                        TravexApplication.FILTER_RESULTS_FILTER_STAYHOTEL_FREEBIES,

                                        TravexApplication.FILTER_RESULTS_FILTER_STAYHOTEL_AMINITIES,
                                        TravexApplication.FILTER_RESULTS_FILTER_STAYHOTEL_PRICE,
                                        TravexApplication.FILTER_RESULTS_FILTER_STAYHOTEL_TYPE



                                        ),
                        mListCreatorForNameValuePairRequest.getListOfSearchApiGetFilterResults_stayHotels(country_id, "", "", "", "", "", "1", "", "", category_item, "", "", "", "", "",""));
            } else if (category_item.equals("Eating Out"))

            {

                LogUtility.Log(TAG, "category item selected is  eating out:getjsonObject", null);

                return getJson(mListCreatorForNameValuePairRequest
                                .getListOfSearchApiGetFilterResults_EatingOut(
                                        TravexApplication.FILTER_RESULTS_CITY_ID,
                                        TravexApplication.FILTER_RESULTS_SEARCH_TERM,
                                        TravexApplication.FILTER_RESULTS_AREA,
                                        TravexApplication.FILTER_RESULTS_LIMIT,
                                        TravexApplication.FILTER_RESULTS_ALPHABET,
                                        TravexApplication.FILTER_RESULTS_START,
                                        TravexApplication.FILTER_RESULTS_SETFILTER,
                                        TravexApplication.FILTER_RESULTS_FILTER_KEYWORD,
                                        TravexApplication.FILTER_RESULTS_FILTER_RATING,
                                        TravexApplication.FILTER_RESULTS_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_EATING_OUT_AREA,
                                        TravexApplication.FILTER_RESULTS_FILTER_EATING_OUT_CATEGORY,
                                        TravexApplication.FILTER_RESULTS_FILTER_EATING_OUT_ESTABLISHMENT,

                                        TravexApplication.FILTER_RESULTS_FILTER_EATING_OUT_CUISINE,
                                        TravexApplication.FILTER_RESULTS_FILTER_EATING_OUT_PRICE,
                                        TravexApplication.FILTER_RESULTS_FILTER_EATING_OUT_HALAL,
                                        TravexApplication.FILTER_RESULTS_FILTER_EATING_OUT_TIMING


                                ),
                        mListCreatorForNameValuePairRequest.getListOfSearchApiGetFilterResults_EatingOut(country_id, "", "", "", "", "", "1", "", "", category_item, "", "", "", "", "", "", ""));
            }


        } else if (tag.equals(TravexApplication.GET_ARTICLES_LIST_FULLFLEDGE_TAG)) {

            return getJson(mListCreatorForNameValuePairRequest.getListOfArticles(
                            TravexApplication.GET_ARTICLES_LIST_CITY_ID,
                            TravexApplication.GET_ARTICLES_LIST_MONTH,
                            TravexApplication.GET_ARTICLES_LIST_YEAR

                    ),
                    mListCreatorForNameValuePairRequest.getListOfArticles(country_id, "01", "2016"));

        } else if (tag.equals(TravexApplication.GET_ARTICLES_LIST_CATEGORYBASED_TAG)) {
            return getJson(mListCreatorForNameValuePairRequest.getListOfArticles(
                            TravexApplication.GET_ARTICLES_LIST_CITY_ID,
                            TravexApplication.GET_ARTICLES_LIST_MONTH,
                            TravexApplication.GET_ARTICLES_LIST_YEAR, TravexApplication.GET_ARTICLES_LIST_CATEGORY_ID

                    ),
                    mListCreatorForNameValuePairRequest.getListOfArticles(country_id, "01", "2016", "pngrtbel_vq_7"));


        } else if (tag.equals(TravexApplication.GET_ARTICLES_LIST_FEATURED_TAG)) {
            return getJson(mListCreatorForNameValuePairRequest.getListOfArticles(
                            TravexApplication.GET_ARTICLES_LIST_CITY_ID,
                            TravexApplication.GET_ARTICLES_LIST_MONTH,
                            TravexApplication.GET_ARTICLES_LIST_YEAR,
                            TravexApplication.GET_ARTICLES_LIST_FEATURED

                    ),
                    mListCreatorForNameValuePairRequest.getListOfArticles(country_id, "01", "2016", "1"));

        } else if (tag.equals(TravexApplication.GET_ARTICLES_DETAILS_TAG)) {
            return getJson(mListCreatorForNameValuePairRequest.getListOfArticlesDetails(
                            TravexApplication.GET_ARTICLES_ID


                    ),
                    mListCreatorForNameValuePairRequest.getListOfArticlesDetails(mPreferenceManager.getArticles_id().get(0)));

        } else if (tag.equals(TravexApplication.GET_ESSENTIALS_LIST_TAG)) {
            return getJson(mListCreatorForNameValuePairRequest.getListOfEssentials_List(
                            TravexApplication.GET_ESSENTIALS_LIST_CITY_ID


                    ),
                    mListCreatorForNameValuePairRequest.getListOfEssentials_List(country_id));

        } else if (tag.equals(TravexApplication.GET_ESSENTIALS_DETAILS_TAG)) {
            return getJson(mListCreatorForNameValuePairRequest.getListOfEssentialsDetails(
                            TravexApplication.GET_ESSENTIALS_DETAILS_ID


                    ),
                    mListCreatorForNameValuePairRequest.
                            getListOfEssentialsDetails(mPreferenceManager.getEssentials_id().get(0)));

        } else if (tag.equals(TravexApplication.GET_TIPS_LIST_TAG)) {
            return getJson(mListCreatorForNameValuePairRequest.getListOfTips_List(
                            TravexApplication.GET_TIPS_LIST_CITY_ID, TravexApplication.GET_TIPS_LIST_CATEGORY_ID


                    ),
                    mListCreatorForNameValuePairRequest.getListOfTips_List(country_id, getCategory_Id_Tips_For(category_item)));

        } else if (tag.equals(TravexApplication.GET_TIPS_DETAILS_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfTipsDetails(TravexApplication.GET_TIPS_DETAILS_ID),
                    mListCreatorForNameValuePairRequest.getListOfTipsDetails(mPreferenceManager.getTips_id().get(0))

            );

        } else if (tag.equals(TravexApplication.ADD_PLAN_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfAddplan
                            (TravexApplication.ADD_PLAN_ITEM_ID,
                                    TravexApplication.ADD_PLAN_ITEM_NAME,
                                    TravexApplication.ADD_PLAN_TIME,
                                    TravexApplication.ADD_PLAN_USER_ID,
                                    TravexApplication.ADD_PLAN_DATE,
                                    TravexApplication.ADD_PLAN_CATEGORY
                            ),
                    mListCreatorForNameValuePairRequest.getListOfAddplan("pngrtbel_vq_7", "asasasasa", "00:15 Am", mPreferenceManager.getRegisration_Id_response(), "2016-03-23", category_item)

            );

        } else if (tag.equals(TravexApplication.UPDATE_PLAN_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfUpdateplan
                            (TravexApplication.UPDATE_PLAN_USER_ID,
                                    TravexApplication.UPDATE_PLAN_ID,
                                    TravexApplication.UPDATE_PLAN_DATE


                            ),
                    mListCreatorForNameValuePairRequest.
                            getListOfUpdateplan(mPreferenceManager.getRegisration_Id_response(),
                                    "1", "2016-03-30")

            );

        } else if (tag.equals(TravexApplication.DELETE_PLAN_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfDeleteplan
                            (

                                    TravexApplication.DELETE_USER_ID, TravexApplication.DELETE_PLAN_ID

                            ),
                    mListCreatorForNameValuePairRequest.
                            getListOfDeleteplan(mPreferenceManager.getRegisration_Id_response(), "1")

            );

        } else if (tag.equals(TravexApplication.DAY_PLAN_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfDayplan
                            (

                                    TravexApplication.USER_ID_DAY_PLAN, TravexApplication.DATE_DAY_PLAN

                            ),
                    mListCreatorForNameValuePairRequest.
                            getListOfDayplan("zrzore_vq_25", "2016-04-11")

            );

        } else if (tag.equals(TravexApplication.DONE_PLAN_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfDoneplan
                            (

                                    TravexApplication.DONE_USER_ID, TravexApplication.DONE_PLAN_ID

                            ),
                    mListCreatorForNameValuePairRequest.
                            getListOfDoneplan(mPreferenceManager.getRegisration_Id_response(), "1")

            );

        } else if (tag.equals(TravexApplication.TRAVEX_EVENTS_AND_MONTHLY_PLAN_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfTravexEventAndMonthlyPlan
                            (

                                    TravexApplication.TRAVEX_EVENTS_AND_MONTHLY_PLAN_USER_ID,
                                    TravexApplication.TRAVEX_EVENTS_AND_MONTHLY_PLAN_MONTH,
                                    TravexApplication.TRAVEX_EVENTS_AND_MONTHLY_PLAN_YEAR

                            ),
                    mListCreatorForNameValuePairRequest.
                            getListOfTravexEventAndMonthlyPlan("zrzore_vq_28", "04", "2016")

            );
//mPreferenceManager.getRegisration_Id_response()  for getting user id
        } else if (tag.equals(TravexApplication.EVENT_DETAILS_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfTravexEventDetails
                            (TravexApplication.EVENTS_DETAILS_ID

                            ),
                    mListCreatorForNameValuePairRequest.getListOfTravexEventDetails("riragf_vq_1")

            );

        } else if (tag.equals(TravexApplication.LIST_MYPLAN_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfPlanList
                            (TravexApplication.LIST_MY_PLAN_MEMBER

                            ),
                    mListCreatorForNameValuePairRequest.getListOfPlanList(mPreferenceManager.getRegisration_Id_response())

            );

        } else if (tag.equals(TravexApplication.WRITE_REVIEW_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfWriteReview
                            (TravexApplication.WRITE_REVIEW_USER_ID,
                                    TravexApplication.WRITE_REVIEW_CATEGORY,
                                    TravexApplication.WRITE_REVIEW_ITEM_ID,
                                    TravexApplication.WRITE_REVIEW_ITEM_NAME,
                                    TravexApplication.WRITE_REVIEW_COMMENT,
                                    TravexApplication.WRITE_REVIEW_RATING

                            ),
                    mListCreatorForNameValuePairRequest.getListOfWriteReview(mPreferenceManager.getRegisration_Id_response(), "pngrtbel_vq_1", "nveyvarf_vq_1", "Aeroflot Russian  Airlines", "test comment", "3")

            );

        } else if (tag.equals(TravexApplication.ADD_SUGGESTIONS_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfAddSuggestions
                            (TravexApplication.ADD_SUGGESTIONS_MEMBER_ID,
                                    TravexApplication.ADD_SUGGESTIONS_CATEGORYID,
                                    TravexApplication.ADD_SUGGESTIONS_SUGGESTIONDATA
                            ),
                    mListCreatorForNameValuePairRequest.getListOfAddSuggestions(mPreferenceManager.getRegisration_Id_response(), getCategory_Id_Tips_For(category_item), "ccxcxvxvx")

            );

        } else if (tag.equals(TravexApplication.ABOUT_US_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfAboutUs
                            (TravexApplication.ABOUT_US_CONTACT_US_TYPE

                            ),
                    mListCreatorForNameValuePairRequest.getListOfAboutUs("aboutus")

            );

        } else if (tag.equals(TravexApplication.CONTACT_US_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfContactUs
                            (TravexApplication.ABOUT_US_CONTACT_US_TYPE

                            ),
                    mListCreatorForNameValuePairRequest.getListOfContactUs("contactus")

            );

        } else if (tag.equals(TravexApplication.ENQUIRY_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfEnquiry
                            (TravexApplication.ENQUIRY_NAME,
                                    TravexApplication.ENQUIRY_EMAIL,
                                    TravexApplication.ENQUIRY_CONTACT_NUMBER,
                                    TravexApplication.ENQUIRY_SUBJECT,
                                    TravexApplication.ENQUIRY_MESSAGE
                            ),
                    mListCreatorForNameValuePairRequest.getListOfEnquiry("arun", "arunkumar121924@gmail.com",
                            "9567032005", "nothing", "this is for nothing")

            );

        } else if (tag.equals(TravexApplication.GET_SEARCH_TERMINLAS_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfGetSearchTerminals_Destinations
                            (TravexApplication.GET_SEARCH_TERMINALS_AIRPORT_ID

                            ),
                    mListCreatorForNameValuePairRequest.getListOfGetSearchTerminals_Destinations("nvecbegf_vq_1")

            );

        } else if (tag.equals(TravexApplication.GET_SEARCH_DESTINATION_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfGetSearchTerminals_Destinations
                            (TravexApplication.GET_SEARCH_DESTINATIONS_AIRPORT_ID

                            ),
                    mListCreatorForNameValuePairRequest.getListOfGetSearchTerminals_Destinations("nvecbegf_vq_1")

            );

        } else if (tag.equals(TravexApplication.PROFILE_IMAGE_UPDATE_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfProfileUpdateImagecontent
                            (TravexApplication.PROFILE_IMAGE_UPDATE_MEMBER_ID, TravexApplication.PROFILE_IMAGE_UPDATE_MEMBER_PROFILE_IMAGE

                            ),
                    mListCreatorForNameValuePairRequest.getListOfProfileUpdateImagecontent(mPreferenceManager.getRegisration_Id_response(), "/storage/sdcard0/DCIM/Camera/Sree.jpg")

            );

        } else if (tag.equals(TravexApplication.MEMBER_UPDATE_PROFILE_TAG)) {


            ArrayList<JSONObject> list_json_profile_answers = new ArrayList<>();
            list_json_profile_answers.add
                    (getJson(mListCreatorForNameValuePairRequest.getListOfProfileAnswersTagProfileUpdate
                                    (TravexApplication.MEMBER_PROFILE_QUESTION_ID_UPDATE_PROFILE,
                                            TravexApplication.MEMBER_PROFILE_TYPE_UPDATE_PROFILE,
                                            TravexApplication.MEMBER_PROFILE_ANSWER_ID_UPDATE_PROFILE
                                    ),
                            mListCreatorForNameValuePairRequest.getListOfProfileAnswersTagProfileUpdate("1", "select", "2")));
            list_json_profile_answers.add
                    (getJson(mListCreatorForNameValuePairRequest.getListOfProfileAnswersTagProfileUpdate
                                    (TravexApplication.MEMBER_PROFILE_QUESTION_ID_UPDATE_PROFILE,
                                            TravexApplication.MEMBER_PROFILE_TYPE_UPDATE_PROFILE,
                                            TravexApplication.MEMBER_PROFILE_ANSWER_ID_UPDATE_PROFILE
                                    ),
                            mListCreatorForNameValuePairRequest.getListOfProfileAnswersTagProfileUpdate("2", "multiple", "2,3")));
            list_json_profile_answers.add
                    (getJson(mListCreatorForNameValuePairRequest.getListOfProfileAnswersTagProfileUpdate
                                    (TravexApplication.MEMBER_PROFILE_QUESTION_ID_UPDATE_PROFILE,
                                            TravexApplication.MEMBER_PROFILE_TYPE_UPDATE_PROFILE,
                                            TravexApplication.MEMBER_PROFILE_ANSWER_ID_UPDATE_PROFILE
                                    ),
                            mListCreatorForNameValuePairRequest.getListOfProfileAnswersTagProfileUpdate("3", "multiple", "6,7")));


            LogUtility.Log(TAG, "MEMBER_UPDATE_PROFILE_TAG:", "list_json_profile_answers size:" + list_json_profile_answers.size());

            return getJson("zrzore_vq_16",
                    mListCreatorForNameValuePairRequest.
                            getListOfProfileTagProfileUpdate(TravexApplication.MEMBER_LAST_NAME_UPDATE_PROFILE,
                                    TravexApplication.MEMBER_EMAIL_UPDATE_PROFILE,
                                    TravexApplication.MEMBER_MARTIAL_STATUS_UPDATE_PROFILE,
                                    TravexApplication.MEMBER_PROFILE_IMAGE_UPDATE_PROFILE,
                                    TravexApplication.MEMBER_FIRST_NAME_UPDATE_PROFILE,
                                    TravexApplication.MEMBER_GENDER_UPDATE_PROFILE,
                                    TravexApplication.MEMBER_NO_KIDS_UPDATE_PROFILE), mListCreatorForNameValuePairRequest.
                            getListOfProfileTagProfileUpdate("joseph",
                                    "vineeth@technomobs.com",
                                    "Married",
                                    "fgdfgdf",
                                    "vineeth",
                                    "Male", "0"), list_json_profile_answers
            );


        } else if (tag.equals(TravexApplication.MEMBER_DETAILS_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfMemberDetails
                            (TravexApplication.MEMBER_DETAILS_MEMBER_ID

                            ),
                    mListCreatorForNameValuePairRequest.getListOfMemberDetails("zrzore_vq_16")

            );

        }
        else if (tag.equals(TravexApplication.FORGET_PASSWORD_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfForgetPassword
                            (TravexApplication.MEMBER_EMAIL_FORGET_PASSWORD

                            ),
                    mListCreatorForNameValuePairRequest.getListOfForgetPassword(mPreferenceManager.getRegisteredUser_email())

            );

        }

        return null;

    }


    public JSONObject getJson(ArrayList<String> namePair, ArrayList<String> valuePair) {


        return mJsonObjectMaker.getJson(namePair, valuePair);
    }

    public JSONObject getJson(String member_id, ArrayList<String> namePairOfProfileTag,
                              ArrayList<String> valuePairOfProfileTag,
                              ArrayList<JSONObject> profileAnswers) {
        JSONObject parent_object = new JSONObject();
        JSONObject mJsonObject_profileTag = mJsonObjectMaker.getJson(namePairOfProfileTag, valuePairOfProfileTag);
        JSONArray mJsonArray_profile_answers = new JSONArray();
        for (int counter = 0; counter < profileAnswers.size(); counter++) {

            JSONObject mJsonObject = profileAnswers.get(counter);
            LogUtility.Log(TAG, "getJson:json object of profile answers:", mJsonObject);
            mJsonArray_profile_answers.put(mJsonObject);


        }

        LogUtility.Log(TAG, "getJson:json array  profile answers:", mJsonArray_profile_answers);

        try {
            parent_object.accumulate(TravexApplication.MEMBER_ID_UPDATE_PROFILE, member_id);
            parent_object.accumulate(TravexApplication.MEMBER_PROFILE_DETAILS_UPDATE_PROFILE, mJsonObject_profileTag);
            parent_object.accumulate(TravexApplication.MEMBER_PROFILE_ANSWERS_UPDATE_PROFILE, mJsonArray_profile_answers);
            LogUtility.Log(TAG, "getJson:final json object  ", parent_object);


        } catch (JSONException e) {
            e.printStackTrace();
        }


        return parent_object;
    }

    @Override
    public void onGetResponseSearchResultsGeneral(boolean status, HashMap<Integer, ArrayList<String>> jsonObjectItems, HashMap<Integer, HashMap<Integer, ArrayList<String>>> jsonObjectArrayItems, String advertisement_img, String advertisement_url) {


        if (status) {
            LogUtility.Log(TAG, "onGetResponseSearchResultsGeneral:true", null);

            HashMap<Integer, ArrayList<String>> object = jsonObjectItems;

            HashMap<Integer, HashMap<Integer, ArrayList<String>>> array_items = jsonObjectArrayItems;

            if (object != null) {
                LogUtility.Log(TAG, "onGetResponseSearchResultsGeneral:singular items", null);
                for (int i = 0; i < object.size(); i++) {
                    LogUtility.Log(TAG, "onGetResponseSearchResultsGeneral:singular items", "json obj:" + (i + 1));
                    ArrayList<String> list = object.get(i);
                    if (list != null) {
                        for (int j = 0; j < list.size(); j++) {

                            if (list.get(j) != null) {
                                LogUtility.Log(TAG, "onGetResponseSearchResultsGeneral:singular items", "item:" + list.get(j));

                            } else {
                                LogUtility.Log(TAG, "onGetResponseSearchResultsGeneral:singular items:", "item:null");

                            }


                        }
                    } else {

                    }


                }
            }

            if (array_items != null) {
                LogUtility.Log(TAG, "onGetResponseSearchResultsGeneral:array items", null);

                for (int i = 0; i < array_items.size(); i++) {

                    LogUtility.Log(TAG, "onGetResponseSearchResultsGeneral:array items", "json object:" + (i + 1));
                    HashMap<Integer, ArrayList<String>> map = array_items.get(i);

                    if (map != null) {
                        for (int j = 0; j < map.size(); j++) {
                            ArrayList<String> list = map.get(j);

                            LogUtility.Log(TAG, "onGetResponseSearchResultsGeneral:array items", "arraylist:" + (j + 1));
                            if (list != null) {
                                for (int k = 0; k < list.size(); k++) {

                                    if (list.get(k) != null) {
                                        LogUtility.Log(TAG, "onGetResponseSearchResultsGeneral:array items", "item:" + list.get(k));

                                    } else {
                                        LogUtility.Log(TAG, "onGetResponseSearchResultsGeneral:array items", "item:null");

                                    }


                                }
                            } else {

                                LogUtility.Log(TAG, "onGetResponseSearchResultsGeneral:array items", "arraylist:" + (j + 1) + ":is null");

                            }


                        }
                    }


                }
            }


        } else {

            LogUtility.Log(TAG, "onGetResponseSearchResultsGeneral:false", null);

        }

    }

    String suggestion = null;
    String id = null;


    int i = 0;

    public void onEnterKeyClick() {

        LogUtility.Log(TAG, "onEnterKeyClick", null);
        country = ((ProfileActivity) getActivity()).getSelectedCountry();
        LogUtility.Log(TAG, "onEnterKeyClick:", "selected country from actionbar:" + country);
        country_id = ((ProfileActivity) getActivity()).fetchCountryIdForCountry(country);
        LogUtility.Log(TAG, "onEnterKeyClick:", "country id:" + country_id);

        //id = ((ProfileActivity) getActivity()).fetchItemidFortheSuggestion(suggestion);
        if (category_item == null) {
            SpinnerCategoryViewHolder spinnerCategoryViewHolder = new SpinnerCategoryViewHolder();
            CategorySpinnerItem categorySpinnerItem = (CategorySpinnerItem) mSpinner_category.getItemAtPosition(0);
            category_item = categorySpinnerItem.getText();
            LogUtility.Log(TAG, "onEnterKeyClick:l", "item at 0th is:" + category_item);


        } else {
            LogUtility.Log(TAG, "onEnterKeyClick:", "category item:" + category_item);

        }
        if (query != null) {

            if (query.trim().length() > 0) {
                LogUtility.Log(TAG, "onEnterKeyClick:query is not null", null);
               /* postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_RESULTS_KEYBASED_URL,
                        getjsonObject(TravexApplication.GET_KEYWORDSEARCH_RESULT_TAG), TravexApplication.REQUEST_ID_GET_RESULTS_KEYWORD);*/
            } else {
                LogUtility.Log(TAG, "onEnterKeyClick:query is  null", null);
                suggestion = mPreferenceManager.getSearchResultsKeyWord_suggestion().get(i);
                LogUtility.Log(TAG, "onEnterKeyClick:query is null", "suggestion:" + suggestion);
                i++;
/**
 * this is for the search api to be called after clicking on the suggestion which is as a result of key based search
 */
                /*postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_RESULTS_GENERAL_URL,
                        getjsonObject(TravexApplication.GET_GENERAL_RESULT_SEARCH_TERM_TAG), TravexApplication.REQUEST_ID_GET_RESULTS_GENERAL);*/
            }


        } else {
            LogUtility.Log(TAG, "onEnterKeyClick:query is  null", null);

            File file = new File("/storage/sdcard0/DCIM/Camera/Sree.jpg");
            if (file.exists()) {
                LogUtility.Log(TAG, "onEnterKeyClick:file exist", null);
            } else {
                LogUtility.Log(TAG, "onEnterKeyClick:file not exist", null);

            }

            /**
             *
             *  this is for forget password
             */

             postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.FORGET_PASSWORD_URL, getjsonObject(TravexApplication.FORGET_PASSWORD_TAG),
                    TravexApplication.REQUEST_ID_FORGET_PASSWORD);

/**
 *
 * this is for suggest city api
 */
            /*postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.CITY_SUGGESTION_URL, getjsonObject(TravexApplication.SUGGEST_CITY_TAG),
                    TravexApplication.REQUEST_ID_SUGGEST_CITY);*/




            /*postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.LOGIN_URL,
                    getjsonObject(TravexApplication.LOGIN_TAG), TravexApplication.REQUEST_ID_LOGIN);*/

// json object request for verification after registration ,NEW Structure instead of getjsonObject()
          /*  mJsonObjectRequestCreator = new JsonObjectRequestCreator(getActivity());
            JSONObject mJsonObject = mJsonObjectRequestCreator.createJsonObjectForVerification(mPreferenceManager.getRegisration_Id_response(),"1234");
     //////////  LogUtility.Log(TAG, "mJsonObject:", mJsonObject);*/


            /**
             * this is for verification
             */
          /*  postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.VERIFY_URL,
                    getjsonObject(TravexApplication.VERIFY_REGISTRATION_TAG),
                    TravexApplication.REQUEST_ID_VERIFY_REGISTRATION);*/

/**
 * this is for registration
 */
          /*  postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.REGISTRATION_URL,
                    getjsonObject(TravexApplication.REGISTER_TAG), TravexApplication.REQUEST_ID_REGISTRATION);
*/

            // this is the new architecture given below register for json object
       /*     mJsonObjectRequestCreator = new JsonObjectRequestCreator(getActivity());
            JSONObject mJsonObject = mJsonObjectRequestCreator.createRequestForRegistrationTravex("arun", "arunkumar121924@gmail.com",
                    "1234", TravexApplication.getInstance().getDEVICE_ID(), TravexApplication.DEVICE_TYPE, "app");
*/
            /////////   LogUtility.Log(TAG, "mJsonObject:", mJsonObject);



            /**
             * this is for getting city list
             */
           /* postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.CITY_LIST_URL, null,
                    TravexApplication.REQUEST_ID_GET_CITY);*/
            /**
             * this is for getting splash images
             */
// this is the new architecture given below
          /*  mJsonObjectRequestCreator = new JsonObjectRequestCreator(getActivity());
            JSONObject mJsonObject = mJsonObjectRequestCreator.createRequestForSplashScreenAssets();*/
            /////////   LogUtility.Log(TAG, "mJsonObject:", mJsonObject);


            // this is the old architecture given below

          /*  postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.SPLASH_FEATURE_IMAGES_URL,
                    getjsonObject(TravexApplication.SPLASH_TAG), TravexApplication.REQUEST_ID_SPLASH);*/
            /**
             * this is for getting feature images
             */

            // this is the new architecture given below

           /* mJsonObjectRequestCreator = new JsonObjectRequestCreator(getActivity());
            JSONObject mJsonObject = mJsonObjectRequestCreator.createRequestForFeatureAssets();*/
            /////////   LogUtility.Log(TAG, "mJsonObject:", mJsonObject);


            // this is the old architecture given below

          /*  postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.SPLASH_FEATURE_IMAGES_URL,
                    getjsonObject(TravexApplication.FEATURE_TAG), TravexApplication.REQUEST_ID_FEATURE);*/

/**
 * this is for upload image in to server[profile pic updation]
 */

         /*   handleForMultipartFormData(NetworkOptions.REQUEST_TYPE_POST,
                    AppConstants.PROFILE_IMAGE_UPDATE_URL,
                    mNetworkManager.getHeaders(),
                    getjsonObject(TravexApplication.PROFILE_IMAGE_UPDATE_TAG),
                    TravexApplication.REQUEST_ID_PROFILE_IMAGE_UPDATE);*/

/**
 * This is for search destinations api
 */

          /*  postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_SEARCH_DESTINATIONS_URL,
                    getjsonObject(TravexApplication.GET_SEARCH_DESTINATION_TAG),
                    TravexApplication.REQUEST_ID_SEARCH_DESTINATIONS);*/


            /**
             *  * this is for the search api to be called after clicking on the suggestion which is as a result of key based search

             */
             /*postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_RESULTS_GENERAL_URL,
                        getjsonObject(TravexApplication.GET_GENERAL_RESULT_SEARCH_TERM_TAG), TravexApplication.REQUEST_ID_GET_RESULTS_GENERAL);

*/

            /**
             * this is for get  search terminals    api
             */
           /* postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_SEARCH_TERMINALS_URL,
                    getjsonObject(TravexApplication.GET_SEARCH_TERMINLAS_TAG),
                    TravexApplication.REQUEST_ID_SEARCH_TERMINALS);
*/

            /**
             * this is for delete plan api
             */
           /* postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.DELETE_PLAN_URL,
                    getjsonObject(TravexApplication.DELETE_PLAN_TAG),
                    TravexApplication.REQUEST_ID_DELETE_PLAN);*/

            /**
             * this is for update plan api
             */
          /*  postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.UPDATE_PLAN_URL,
                    getjsonObject(TravexApplication.UPDATE_PLAN_TAG),
                    TravexApplication.REQUEST_ID_UPDATE_PLAN);*/
/**
 * this is for retrieving member details with options
 */
           /* postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.MEMBER_DETAILS_WITH_OPTIONS_URL,
                    getjsonObject(TravexApplication.MEMBER_DETAILS_TAG),
                    TravexApplication.REQUEST_ID_MEMBER_DETAILS);*/


/**
 * this is for retrieving member details without options
 */
         /*   postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.MEMBER_DETAILS_URL,
                    getjsonObject(TravexApplication.MEMBER_DETAILS_TAG),
                    TravexApplication.REQUEST_ID_MEMBER_DETAILS);*/

/**
 * This is for updating member profile new api
 */
            /*postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.MEMBER_UPDATE_PROFILE_URL,
                    getjsonObject(TravexApplication.MEMBER_UPDATE_PROFILE_TAG),
                    TravexApplication.REQUEST_ID_MEMBER_UPDATE_PROFILE);*/

/**
 * this is for enquiry api
 */
           /* postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.ENQUIRY_URL,
                    getjsonObject(TravexApplication.ENQUIRY_TAG),
                    TravexApplication.REQUEST_ID_ENQUIRY);*/

/**
 * this is for  contact us api
 */

           /* postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.CONTACT_US_URL,
                    getjsonObject(TravexApplication.CONTACT_US_TAG),
                    TravexApplication.REQUEST_ID_CONTACT_US);*/
/**
 * this is for  about us api
 */
           /* postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.ABOUT_US_URL,
                    getjsonObject(TravexApplication.ABOUT_US_TAG),
                    TravexApplication.REQUEST_ID_ABOUT_US);*/
/**
 * this is for  add suggestions api
 */
            /*postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.ADD_SUGGESTIONS_URL,
                    getjsonObject(TravexApplication.ADD_SUGGESTIONS_TAG),
                    TravexApplication.REQUEST_ID_ADD_SUGGESTIONS);*/

/**
 * this is for list myplan api:not using :its merged with travex events api
 */
            /*postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.LIST_MYPLAN_URL,
                    getjsonObject(TravexApplication.LIST_MYPLAN_TAG),
                    TravexApplication.REQUEST_ID_LIST_MYPLAN);*/
/**
 * this is for add plan api
 */
           /* postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.ADD_PLAN_URL,
                    getjsonObject(TravexApplication.ADD_PLAN_TAG),
                    TravexApplication.REQUEST_ID_ADD_PLAN);*/
/**
 *
 * this is for day plan api
 */
         /*   postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.DAY_PLAN_URL,
                    getjsonObject(TravexApplication.DAY_PLAN_TAG),
                    TravexApplication.REQUEST_ID_DAY_PLAN);*/

/**
 * this is for travex events and monthly plan list api
 */
         /*   postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.TRAVEX_EVENTS_AND_MONTHLY_PLAN_URL,
                    getjsonObject(TravexApplication.TRAVEX_EVENTS_AND_MONTHLY_PLAN_TAG),
                    TravexApplication.REQUEST_ID_TRAVEX_EVENTS_AND_MONTHLY_PLAN);*/

  /* postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.TRAVEX_EVENTS_DETAILS_URL,
                    getjsonObject(TravexApplication.EVENT_DETAILS_TAG),
                    TravexApplication.REQUEST_ID_EVENT_DETAILS);*/

/**
 * this is for done plan api
 */
           /* postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.DONE_PLAN_URL,
                    getjsonObject(TravexApplication.DONE_PLAN_TAG),
                    TravexApplication.REQUEST_ID_DONE_PLAN);*/

/**
 * this is for write review api
 */

            /*postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.WRITE_REVIEW_URL,
                    getjsonObject(TravexApplication.WRITE_REVIEW_TAG),
                    TravexApplication.REQUEST_ID_WRITE_REVIEW);*/


/**
 * THIS IS FOR TIPS DETAILS API
 */
          /*  postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_TIPS_DETAILS_URL,
                    getjsonObject(TravexApplication.GET_TIPS_DETAILS_TAG),
                    TravexApplication.REQUEST_ID_TIPS_DETAILS);*/
            /**
             *this is for tip list api
             */
           /* postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_TIPS_LIST_URL,
                    getjsonObject(TravexApplication.GET_TIPS_LIST_TAG),
                    TravexApplication.REQUEST_ID_TIPS_LIST);*/

            /**
             * this is for essential details api
             */
          /*  postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_ESSENTIALS_DETAILS_URL,
                    getjsonObject(TravexApplication.GET_ESSENTIALS_DETAILS_TAG),
                    TravexApplication.REQUEST_ID_ESSENTIALS_DETAILS);*/

/**
 * this is for essentials list api
 */
            /*postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_ESSENTIALS_LIST_URL,
                    getjsonObject(TravexApplication.GET_ESSENTIALS_LIST_TAG),
                    TravexApplication.REQUEST_ID_ESSENTIALS_LIST);*/

/**
 *  this is for articles details api
 */

           /* postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_ARTICLES_DETAILS_URL,
                    getjsonObject(TravexApplication.GET_ARTICLES_DETAILS_TAG), TravexApplication.REQUEST_ID_ARTICLE_DETAILS);
*/

            /**
             * this is for featured article list
             */
          /*  postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_ARTICLES_LIST_URL,
                    getjsonObject(TravexApplication.GET_ARTICLES_LIST_FEATURED_TAG), TravexApplication.REQUEST_ID_ARTICLE_LIST_FEATURED);
*/


            /**
             * this is for categorized article list
             */
           /* postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_ARTICLES_LIST_URL,
                    getjsonObject(TravexApplication.GET_ARTICLES_LIST_CATEGORYBASED_TAG), TravexApplication.REQUEST_ID_ARTICLE_LIST_CATEGORIZED);

           */

            /**
             * this is for articles list full
             */

            /*postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_ARTICLES_LIST_URL,
                    getjsonObject(TravexApplication.GET_ARTICLES_LIST_FULLFLEDGE_TAG), TravexApplication.REQUEST_ID_ARTICLE_LIST_FULLFLEDGE);*/
/**
 * this is for filter results api
 */
            /*postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_FILTER_RESULTS_URL,
                    getjsonObject(TravexApplication.FILTERRESULTS_TAG), TravexApplication.REQUEST_ID_FILTER_RESULTS);
*/
            /**
             * this is for get filter api
             */
         /*   postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_FILTERS_URL,
                    getjsonObject(TravexApplication.GET_FILTERS_TAG), TravexApplication.REQUEST_ID_GET_FILTER);*/

/**
 *
 *  This is for now search api
 */
            /*postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_RESULTS_NOW_URL,
                    getjsonObject(TravexApplication.NOW_TAG), TravexApplication.REQUEST_ID_NOW);*/

/**
 *   search item specific
 */


/**
 * This is for search item details api
 */
           /* postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_RESULTS_SEARCHITEM_SPECIFIC_URL,
                    getjsonObject(TravexApplication.GET_SEARCHITEM_TAG), TravexApplication.REQUEST_ID_GET_SEARCH_ITEM);
*/
            /**
             *
             * the below post for search general
             */

           /* postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_RESULTS_GENERAL_URL,
                    getjsonObject(TravexApplication.GET_GENERAL_RESULT_TAG), TravexApplication.REQUEST_ID_GET_RESULTS_GENERAL);*/
        }

    }

    @Override
    public void onGetSearchResultsKeyWordBased(boolean status, HashMap<Integer, ArrayList<String>> target) {


        if (status) {
            HashMap<Integer, ArrayList<String>> object = target;

            if (object != null) {
                LogUtility.Log(TAG, "onGetSearchResultsKeyWordBased:success", null);
                LogUtility.Log(TAG, "onGetSearchResultsKeyWordBased:singular items", null);
                for (int i = 0; i < object.size(); i++) {
                    LogUtility.Log(TAG, "onGetSearchResultsKeyWordBased:singular items", "json obj:" + (i + 1));
                    ArrayList<String> list = object.get(i);
                    if (list != null) {
                        for (int j = 0; j < list.size(); j++) {

                            if (list.get(j) != null) {
                                LogUtility.Log(TAG, "onGetSearchResultsKeyWordBased:singular items", "item:" + list.get(j));

                            } else {
                                LogUtility.Log(TAG, "onGetSearchResultsKeyWordBased:singular items:", "item:null");

                            }


                        }
                    } else {
                        LogUtility.Log(TAG, "onGetSearchResultsKeyWordBased:singular items:list is null", null);

                    }


                }
            } else {
                LogUtility.Log(TAG, "onGetSearchResultsKeyWordBased:success:map is null", null);

            }

        } else {
            LogUtility.Log(TAG, "onGetSearchResultsKeyWordBased:failed", null);

        }

    }

    @Override
    public void onGetNowResponse(boolean status, HashMap<Integer, ArrayList<String>> jsonobject, HashMap<Integer, ArrayList<String>> array_phonenumbers) {

        HashMap<Integer, ArrayList<String>> object = jsonobject;


        LogUtility.Log(TAG, "onGetNowResponse:success", null);
        LogUtility.Log(TAG, "onGetNowResponse:singular items", null);
        for (int i = 0; i < object.size(); i++) {
            LogUtility.Log(TAG, "onGetNowResponse:singular items", "json obj:" + (i + 1));
            ArrayList<String> list = object.get(i);
            if (list != null) {
                for (int j = 0; j < list.size(); j++) {

                    if (list.get(j) != null) {
                        LogUtility.Log(TAG, "onGetNowResponse:singular items", "item:" + list.get(j));

                    } else {
                        LogUtility.Log(TAG, "onGetNowResponse:singular items:", "item:null");

                    }


                }
            } else {

            }


        }


        for (int i = 0; i < array_phonenumbers.size(); i++) {
            LogUtility.Log(TAG, "onGetNowResponse:array_phonenumbers items", "json obj:" + (i + 1));
            ArrayList<String> list = array_phonenumbers.get(i);
            if (list != null) {
                for (int j = 0; j < list.size(); j++) {

                    if (list.get(j) != null) {
                        LogUtility.Log(TAG, "onGetNowResponse:array_phonenumbers items", "item:" + list.get(j));

                    } else {
                        LogUtility.Log(TAG, "onGetNowResponse:array_phonenumbers items:", "item:null");

                    }


                }
            } else {

            }


        }


    }

    public String getCategory_Id_Tips_For(String item) {

        if (item.equals("Airline")) {

            return "pngrtbel_vq_1";
        } else if (item.equals("Shopping")) {

            return "pngrtbel_vq_10";
        } else if (item.equals("Business")) {

            return "pngrtbel_vq_11";
        } else if (item.equals("Real Estate")) {

            return "pngrtbel_vq_12";
        } else if (item.equals("Money Exchange")) {

            return "pngrtbel_vq_13";
        } else if (item.equals("Car Rentals")) {

            return "pngrtbel_vq_14";
        } else if (item.equals("Embasies & Consulates")) {

            return "pngrtbel_vq_2";
        } else if (item.equals("Tour & Travel")) {

            return "pngrtbel_vq_3";
        } else if (item.equals("Events")) {

            return "pngrtbel_vq_4";
        } else if (item.equals("Eating Out")) {

            return "pngrtbel_vq_5";
        } else if (item.equals("Stay/Hotels")) {

            return "pngrtbel_vq_6";
        } else if (item.equals("nightlife")) {

            return "pngrtbel_vq_7";
        } else if (item.equals("Spa")) {

            return "pngrtbel_vq_8";
        } else if (item.equals("Things To Do")) {

            return "pngrtbel_vq_9";
        } else if (item.equals("Emergencies")) {

            return "pngrtbel_vq_15";
        }


        return null;


    }

    /**
     * @param request_type
     * @param request_header
     * @param url
     * @param jsonObject
     * @param request_id
     */
    public void postJsonRequest(int request_type, final Map<String, String> request_header, String url, JSONObject jsonObject, final int request_id) {

        mNetworkManager.postJsonRequest(request_type, request_header, url, jsonObject, request_id);


    }

    @Override
    public void onGetFilterResponse(boolean status, JSONObject object) {

        if (status) {

            LogUtility.Log(TAG, "onGetFilterResponse: :success", null);

        } else {
            LogUtility.Log(TAG, "onGetFilterResponse: :failure", null);

        }


    }

    @Override
    public void onGetSearchItemResponse(boolean status, JSONObject jsonObject) {
        if (status) {

            LogUtility.Log(TAG, "onGetSearchItemResponse: :success", null);

        } else {
            LogUtility.Log(TAG, "onGetSearchItemResponse: :failure", null);

        }
    }

    @Override
    public void onGetFilterResultsResponse(boolean status, JSONObject object) {
        if (status) {

            LogUtility.Log(TAG, "onGetFilterResultsResponse: :success", null);

        } else {
            LogUtility.Log(TAG, "onGetFilterResultsResponse: :failure", null);

        }

    }

    @Override
    public void onGetArticlesListResponse(boolean status, JSONObject object) {


        if (status) {

            LogUtility.Log(TAG, "onGetArticlesListResponse: :success", "json data:" + object.toString());


        } else {
            LogUtility.Log(TAG, "onGetArticlesListResponse: :failure", null);

        }


    }

    @Override
    public void onGetArticlesDetailsResponse(boolean status, JSONObject jsonObject) {
        if (status) {

            LogUtility.Log(TAG, "onGetArticlesDetailsResponse: :success", "json data:" + jsonObject.toString());


        } else {
            LogUtility.Log(TAG, "onGetArticlesDetailsResponse: :failure", null);

        }
    }

    @Override
    public void onGetEssentialsListResponse(boolean status, JSONObject jsonObject) {
        if (status) {

            LogUtility.Log(TAG, "onGetEssentialsListResponse: :success", "json data:" + jsonObject.toString());


        } else {
            LogUtility.Log(TAG, "onGetEssentialsListResponse: :failure", null);

        }
    }

    @Override
    public void onGetEssentialsDetailsResponse(boolean status, JSONObject jsonObject) {
        if (status) {

            LogUtility.Log(TAG, "onGetEssentialsDetailsResponse: :success", "json data:" + jsonObject.toString());


        } else {
            LogUtility.Log(TAG, "onGetEssentialsDetailsResponse: :failure", null);

        }


    }

    @Override
    public void onGetTipsDetailsResponse(boolean status, JSONObject jsonObject) {
        if (status) {

            LogUtility.Log(TAG, "onGetTipsDetailsResponse: :success", "json data:" + jsonObject.toString());


        } else {
            LogUtility.Log(TAG, "onGetTipsDetailsResponse: :failure", null);

        }
    }

    @Override
    public void onGetTipsListResponse(boolean status, JSONObject jsonObject) {
        if (status) {

            LogUtility.Log(TAG, "onGetTipsListResponse: :success", "json data:" + jsonObject.toString());


        } else {
            LogUtility.Log(TAG, "onGetTipsListResponse: :failure", null);

        }
    }

    @Override
    public void onAddPlanResponse(boolean status) {
        if (status) {

            LogUtility.Log(TAG, "onAddPlanResponse: :success", null);


        } else {
            LogUtility.Log(TAG, "onAddPlanResponse: :failure", null);
        }
    }

    @Override
    public void onGetListPlanResponse(boolean status, JSONObject jsonObject) {
        if (status) {

            LogUtility.Log(TAG, "onGetListPlanResponse: :success", "jsonObject:" + jsonObject);


        } else {
            LogUtility.Log(TAG, "onGetListPlanResponse: :failure", null);
        }
    }

    @Override
    public void onAddSuggestionsResponse(boolean status) {
        if (status) {

            LogUtility.Log(TAG, "onAddSuggestionsResponse: :success", null);


        } else {
            LogUtility.Log(TAG, "onAddSuggestionsResponse: :failure", null);
        }
    }


    @Override
    public void onGetAboutUsResponse(boolean status, HashMap<Integer, HashMap<Integer, ArrayList<String>>> description_teamDetails) {
        if (status) {
            HashMap<Integer, HashMap<Integer, ArrayList<String>>> map = description_teamDetails;


            LogUtility.Log(TAG, "onGetAboutUsResponse: :success", null);

            LogUtility.Log(TAG, "onGetAboutUsResponse:success:description details", null);
            HashMap<Integer, ArrayList<String>> map_description_only = map.get(0);

            if (map_description_only != null) {
                for (int i = 0; i < map_description_only.size(); i++) {
                    ArrayList<String> list = map_description_only.get(i);
                    LogUtility.Log(TAG, "onGetAboutUsResponse", "list:" + (i + 1));
                    if (list != null) {
                        for (int j = 0; j < list.size(); j++) {

                            LogUtility.Log(TAG, "onGetAboutUsResponse:description details:", "list item:" + list.get(j));
                        }
                    } else {
                        LogUtility.Log(TAG, "onGetAboutUsResponse: :success:description details is null in list", null);

                    }

                }
            } else {
                LogUtility.Log(TAG, "onGetAboutUsResponse: :success:description details is null full", null);

            }


            HashMap<Integer, ArrayList<String>> map_team_only = map.get(1);
            if (map_team_only != null) {
                for (int i = 0; i < map_team_only.size(); i++) {
                    ArrayList<String> list = map_team_only.get(i);
                    LogUtility.Log(TAG, "onGetAboutUsResponse", "list:" + (i + 1));
                    if (list != null) {
                        for (int j = 0; j < list.size(); j++) {

                            LogUtility.Log(TAG, "onGetAboutUsResponse:team details:", "list item:" + list.get(j));
                        }
                    } else {
                        LogUtility.Log(TAG, "onGetAboutUsResponse: :success:team details is null in list", null);

                    }

                }
            } else {
                LogUtility.Log(TAG, "onGetAboutUsResponse: :success:team details is null full", null);

            }


        } else {
            LogUtility.Log(TAG, "onGetAboutUsResponse: :failure", null);
        }
    }

    @Override
    public void onContactUsResponse(boolean status, HashMap<Integer, HashMap<Integer, ArrayList<String>>> result_details) {
        if (status) {
            HashMap<Integer, HashMap<Integer, ArrayList<String>>> map = result_details;


            LogUtility.Log(TAG, "onContactUsResponse: :success", null);

            LogUtility.Log(TAG, "onContactUsResponse:success: details", null);


            for (int i = 0; i < map.size(); i++) {
                HashMap<Integer, ArrayList<String>> map_details = map.get(i);
                if (map_details != null) {
                    for (int j = 0; j < map_details.size(); j++) {
                        ArrayList<String> list = map_details.get(j);

                        if (list != null) {
                            for (int k = 0; k < list.size(); k++) {

                                LogUtility.Log(TAG, "onContactUsResponse:map details:", "list item:" + list.get(k));
                            }
                        } else {
                            LogUtility.Log(TAG, "onContactUsResponse: :success:list is null", null);

                        }
                    }
                } else {
                    LogUtility.Log(TAG, "onContactUsResponse: :success:map details is null in list", null);

                }


            }


        } else {
            LogUtility.Log(TAG, "onContactUsResponse: :failure", null);
        }
    }

    @Override
    public void onEnquiryResponse(boolean status) {
        if (status) {
            LogUtility.Log(TAG, "onEnquiryResponse:success", null);
        } else {
            LogUtility.Log(TAG, "onEnquiryResponse:failure", null);

        }

    }

    @Override
    public void onUpdatePlanResponse(boolean status) {
        if (status) {
            LogUtility.Log(TAG, "onUpdatePlanResponse:success", null);
        } else {
            LogUtility.Log(TAG, "onUpdatePlanResponse:failure", null);

        }
    }

    @Override
    public void onDeletePlanResponse(boolean status) {
        if (status) {
            LogUtility.Log(TAG, "onDeletePlanResponse:success", null);
        } else {
            LogUtility.Log(TAG, "onDeletePlanResponse:failure", null);

        }
    }

    @Override
    public void onGetSearchTerminals(boolean status, HashMap<Integer, ArrayList<String>> terminal_title_and_id) {
        if (status) {
            LogUtility.Log(TAG, "onGetSearchTerminals:success", null);

            if (terminal_title_and_id != null) {
                for (int i = 0; i < 2; i++) {

                    ArrayList<String> list = terminal_title_and_id.get(i);
                    LogUtility.Log(TAG, "onGetSearchTerminals:success:list:", (i + 1));
                    if (list != null) {


                        for (int j = 0; j < list.size(); j++) {

                            LogUtility.Log(TAG, "onGetSearchTerminals:success:list item:", list.get(j));


                        }
                    }


                }
            }


        } else {
            LogUtility.Log(TAG, "onGetSearchTerminals:failure", null);

        }
    }

    @Override
    public void onGetSearchDestinations(boolean value, ArrayList<String> airline_destinations) {
        if (value) {
            LogUtility.Log(TAG, "onGetSearchDestinations:success", null);

            if (airline_destinations != null) {
                for (int i = 0; i < airline_destinations.size(); i++) {
                    LogUtility.Log(TAG, "onGetSearchDestinations:success:item:", airline_destinations.get(i));
                }
            } else {
                LogUtility.Log(TAG, "onGetSearchDestinations:success:list is null", null);

            }


        } else {
            LogUtility.Log(TAG, "onGetSearchDestinations:failure", null);

        }
    }

    public void handleForMultipartFormData(int type_method, String url, Map<String, String> header, JSONObject jsonObject, int request_id) {

        LogUtility.Log(TAG, "handleForMultipartFormData", "" +
                "type_method:" + type_method + ",url:" + url + ",header:" + header + ",jsonObject:" + jsonObject);

        mNetworkManager.handleForMultipartFormData(type_method, url, header, jsonObject, request_id);

    }

    @Override
    public void onGetProfileImageUpdateResponse(boolean status, String member_profile_image_url) {
        if (status) {

            LogUtility.Log(TAG, "onGetProfileImageUpdateResponse:success", "updated image url:" + member_profile_image_url);


        } else {
            LogUtility.Log(TAG, "onGetProfileImageUpdateResponse:failure", null);

        }
    }

    @Override
    public void onGetDonePlanResponse(boolean status) {
        if (status) {

            LogUtility.Log(TAG, "onGetDonePlanResponse:success", null);


        } else {
            LogUtility.Log(TAG, "onGetDonePlanResponse:failure", null);

        }
    }

    @Override
    public void onGetDayPlanResponse(boolean status, HashMap<String, ArrayList<String>> target_map) {
        if (status) {

            LogUtility.Log(TAG, "onGetDayPlanResponse:success", null);

            if (target_map != null) {
                showItems("plan_id", target_map);
                showItems("category", target_map);
                // showItems("category_id", target_map);
                showItems("item_id", target_map);
                showItems("item_name", target_map);
                showItems("user_id", target_map);
                showItems("date", target_map);
                showItems("time", target_map);
                showItems("description", target_map);
                showItems("latitude", target_map);
                showItems("longitude", target_map);
                showItems("complete", target_map);
                showItems("phone", target_map);
                showItems("image", target_map);


            } else {

            }


        } else {
            LogUtility.Log(TAG, "onGetDayPlanResponse:failure", null);

        }
    }

    public void showItems(String identifier, HashMap<String, ArrayList<String>> map) {

        ArrayList<String> items = map.get(identifier);

        if (items != null) {
            for (int k = 0; k < items.size(); k++) {
                String item = items.get(k);
                if (item != null) {
                    LogUtility.Log(TAG, "showItems:", identifier + ":" + item);

                } else {
                    LogUtility.Log(TAG, "showItems:", identifier + ":" + "null");

                }


            }
        }


    }

    @Override
    public void onGetTravexEventAndMonthlyPlanResponse(boolean status, HashMap<String, ArrayList<String>> monthly_plan_list, HashMap<String, ArrayList<String>> travex_events) {
        if (status) {


            LogUtility.Log(TAG, "onGetTravexEventAndMonthlyPlanResponse:success", null);
            if (monthly_plan_list != null) {

                showItems("plan_id", monthly_plan_list);
                showItems("category", monthly_plan_list);
                showItems("item_id", monthly_plan_list);
                showItems("item_name", monthly_plan_list);
                showItems("user_id", monthly_plan_list);
                showItems("date", monthly_plan_list);
                showItems("time", monthly_plan_list);
                showItems("complete", monthly_plan_list);
                showItems("status", monthly_plan_list);

            }
            if (travex_events != null) {

                showItems("item_id", travex_events);
                showItems("item_name", travex_events);
                showItems("description", travex_events);
                showItems("start_date", travex_events);
                showItems("end_date", travex_events);
                showItems("start_time", travex_events);
                showItems("end_time", travex_events);
                showItems("latitude", travex_events);
                showItems("longitude", travex_events);
                showItems("city", travex_events);
                showItems("image", travex_events);
                showItems("events_category", travex_events);
                showItems("active_status", travex_events);
                showItems("phone", travex_events);


            }
           /* if(phonenumbers_travex_events!=null)
            {


                for(int k=0;k<phonenumbers_travex_events.size();k++)
                {

                    ArrayList<String> numbers=phonenumbers_travex_events.get(k);
                    if(numbers!=null)
                    {
                        for(int j=0;j<numbers.size();j++)
                        {
                            String item=numbers.get(j);
                            if(item!=null)
                            {
                                LogUtility.Log(TAG,"onGetTravexEventAndMonthlyPlanResponse","items:"+item);

                            }

                        }

                    }


                }

            }*/


        } else {
            LogUtility.Log(TAG, "onGetTravexEventAndMonthlyPlanResponse:failure", null);

        }
    }

    @Override
    public void onGetEventDetailsResponse(boolean status, JSONObject jsonObject) {

        if (status) {
            LogUtility.Log(TAG, "onGetEventDetailsResponse:success", "jsonObject:" + jsonObject);

        } else {
            LogUtility.Log(TAG, "onGetEventDetailsResponse:failure", null);

        }

    }

    @Override
    public void onGetWriteReviewResponse(boolean status) {
        if (status) {
            LogUtility.Log(TAG, "onGetWriteReviewResponse:success", null);

        } else {
            LogUtility.Log(TAG, "onGetWriteReviewResponse:failure", null);

        }

    }

    @Override
    public void onGetMemberUpdateProfile(boolean status) {
        if (status) {
            LogUtility.Log(TAG, "onGetMemberUpdateProfile:success", null);

        } else {
            LogUtility.Log(TAG, "onGetMemberUpdateProfile:failure", null);

        }

    }

    @Override
    public void onGetMemberDetailsResponse(boolean status, JSONObject jsonObject) {
        if (status) {
            LogUtility.Log(TAG, "onGetMemberDetailsResponse:success", "jsonObject:" + jsonObject);

        } else {
            LogUtility.Log(TAG, "onGetMemberDetailsResponse:failure", null);

        }

    }

    @Override
    public void onGetForgetPasswordResponse(boolean status,String Message) {
        if (status) {
            LogUtility.Log(TAG, "onGetForgetPasswordResponse:success",null);

        } else {
            LogUtility.Log(TAG, "onGetForgetPasswordResponse:failure", null);

        }
    }
}
