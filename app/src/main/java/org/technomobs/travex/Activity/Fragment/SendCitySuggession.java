package org.technomobs.travex.Activity.Fragment;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import org.json.JSONObject;
import org.technomobs.travex.Activity.MainActivity;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.Controller.Interface.GetCitySuggestionResponse;
import org.technomobs.travex.Controller.JsonObjectMaker;
import org.technomobs.travex.Controller.NetworkManager;
import org.technomobs.travex.Controller.NetworkOptions;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.ListCreatorForNameValuePairRequest;
import org.technomobs.travex.Utillity.LogUtility;

import java.util.ArrayList;

/**
 * Created by technomobs on 22/2/16.
 */
public class SendCitySuggession extends Fragment implements GetCitySuggestionResponse {
    public static final String TAG = SendCitySuggession.class.getSimpleName();
    static NetworkManager mNetworkManager = null;
    JsonObjectMaker mJsonObjectMaker = null;
    PreferenceManager mPreferenceManager = null;
    ListCreatorForNameValuePairRequest mListCreatorForNameValuePairRequest = null;

    /**
     * use this button to send city that user suggested to server
     */
    ImageView mButtonForYes = null;


    /**
     * if user not interested to send city to server
     */
    ImageView mButtonForNo = null;

    @Override
    public void onAttach(Context context) {
        LogUtility.Log(TAG, "onAttach", null);
        super.onAttach(context);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        mJsonObjectMaker = new JsonObjectMaker(getActivity());
        mNetworkManager = NetworkManager.getSingleInstance(getActivity());
        mPreferenceManager = new PreferenceManager(getActivity());
        mListCreatorForNameValuePairRequest = new ListCreatorForNameValuePairRequest(getActivity());
        mNetworkManager.setOnGetCitySuggestListener(this);


        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        LogUtility.Log(TAG, "onConfigurationChanged", null);
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onCreate", null);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onCreateView", null);

        View view = inflater.inflate(R.layout.send_city_suggession_fragment_layout, container, false);
        mButtonForYes = (ImageView) view.findViewById(R.id.button2);
        mButtonForNo = (ImageView) view.findViewById(R.id.button);
//////////////////


        /////////////////////
        mButtonForNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity myActivity = (MainActivity) getActivity();
                if (myActivity instanceof MainActivity) {
                    myActivity.load_Send_City_Visit_fragment();
                }

            }
        });
        mButtonForYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.CITY_SUGGESTION_URL, getjsonObject(TravexApplication.SUGGEST_CITY_TAG),
                        TravexApplication.REQUEST_ID_SUGGEST_CITY);
            }
        });
        return view;
    }

    @Override
    public void onDestroy() {
        LogUtility.Log(TAG, "onDestroy", null);
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        LogUtility.Log(TAG, "onDestroyView", null);
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        LogUtility.Log(TAG, "onDetach", null);
        super.onDetach();
    }

    @Override
    public void onPause() {
        LogUtility.Log(TAG, "onPause", null);
        super.onPause();
    }

    @Override
    public void onResume() {
        LogUtility.Log(TAG, "onResume", null);
        super.onResume();
    }

    @Override
    public void onStart() {
        LogUtility.Log(TAG, "onStart", null);
        super.onStart();
    }

    @Override
    public void onStop() {
        LogUtility.Log(TAG, "onStop", null);
        super.onStop();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onViewCreated", null);
        super.onViewCreated(view, savedInstanceState);
    }

    public void postJsonRequest(int request_type, String url, JSONObject jsonObject, final int request_id) {
        mNetworkManager.postJsonRequest(request_type, url, jsonObject, request_id);


    }

    public JSONObject getjsonObject(String tag) {
        if (tag.equals(TravexApplication.SUGGEST_CITY_TAG)) {

            return getJson(mListCreatorForNameValuePairRequest.getListForCitySuggestionRequest(TravexApplication.EMAIL_ID_CITY_SUGGEST, TravexApplication.CITY_SUGGEST), mListCreatorForNameValuePairRequest.getListForCitySuggestionRequest(mPreferenceManager.getRegisration_Id_response(), mPreferenceManager.getCurrentLocalityName()));

        }
        return null;
    }

    public JSONObject getJson(ArrayList<String> namePair, ArrayList<String> valuePair) {


        return mJsonObjectMaker.getJson(namePair, valuePair);
    }


    @Override
    public void onGetCitySuggestion(boolean status) {
        LogUtility.Log(TAG, "onGetCitySuggestion", null);
        MainActivity myActivity = (MainActivity) getActivity();
        if (myActivity instanceof MainActivity) {
            myActivity.load_Send_City_Visit_fragment();
        }

        if (status) {
            LogUtility.Log(TAG, "onGetCitySuggestion:saved successfully in server", null);
        } else {
            LogUtility.Log(TAG, "onGetCitySuggestion:not saved successfully in server", null);
        }

    }
}
