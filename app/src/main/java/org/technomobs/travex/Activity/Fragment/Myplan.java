package org.technomobs.travex.Activity.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Controller.Interface.AddPlanResponse;
import org.technomobs.travex.Controller.Interface.DayPlanResponse;
import org.technomobs.travex.Controller.Interface.GetTravexEventsAndMonthlyPlanResponse;
import org.technomobs.travex.Controller.Interface.ListPlanResponse;
import org.technomobs.travex.Controller.JsonObjectMaker;
import org.technomobs.travex.Controller.NetworkManager;
import org.technomobs.travex.Controller.NetworkOptions;
import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.DateFormatFunction;
import org.technomobs.travex.Utillity.ListCreatorForNameValuePairRequest;
import org.technomobs.travex.Utillity.LogUtility;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Abhi on 11-03-2016.
 */
public class
        Myplan extends Fragment implements ListPlanResponse, AddPlanResponse,GetTravexEventsAndMonthlyPlanResponse,DayPlanResponse
        ,Animation.AnimationListener{
    private static final String dateTemplate = "MMMM yyyy";
    static NetworkManager mNetworkManager = null;
    PreferenceManager mPreferenceManager = null;
    //  Context context;
    Activity act;
    String temp_date,current_date;
    View rootView;
    ArrayAdapter<String> list_adapter,travex_list_adapter;
    ArrayList<String> Plan_ID,Plan_category,Plan_item,Plan_member,Plan_date,Plan_time,Plan_status,Plan_created,items,Plan_item_userId,Plan_itemid;
    JsonObjectMaker mJsonObjectMaker = null;
    ListCreatorForNameValuePairRequest mListCreatorForNameValuePairRequest = null;

    Animation rotateOne, rotateTwo, rotateThree;
    RelativeLayout animation_layout;
    //    public Myplan(Context con){
//        context=con;
//
//    }
    TextView tv_todays_event,tv_travex_events,tv_nodata;
    HashMap<String, ArrayList<String>> fetchtodo, fetchtodo_duedate;
    String date_month_year = "";
    ArrayList<String> due_date_List, todays_eventlist,travex_eventlist,travex_eventdesc;
    int check = 0;
    private int poststatus = 0;
    private Button currentMonth;
    private ImageView prevMonth;
    private ImageView nextMonth;
    private GridView calendarView;
    private GridCellAdapter adapter;
    private Calendar _calendar;
    private int month, year;
    ArrayList<String> plan_id_list = new ArrayList<>();
    ArrayList<String> category_list = new ArrayList<>();
    ArrayList<String> item_id_list = new ArrayList<>();
    ArrayList<String> item_name_list = new ArrayList<>();
    ArrayList<String> user_id_list = new ArrayList<>();
    ArrayList<String> date_list = new ArrayList<>();
    ArrayList<String> time_list = new ArrayList<>();
    ArrayList<String> complete_list = new ArrayList<>();
    ArrayList<String> status_list = new ArrayList<>();

    ArrayList<String> events_item_id = new ArrayList<>();
    ArrayList<String> events_item_name= new ArrayList<>();
    ArrayList<String> events_description= new ArrayList<>();
    ArrayList<String> events_start_date= new ArrayList<>();
    ArrayList<String> events_end_date= new ArrayList<>();
    ArrayList<String> events_start_time= new ArrayList<>();
    ArrayList<String> events_end_time= new ArrayList<>();
    ArrayList<String> events_latitude= new ArrayList<>();
    ArrayList<String> events_longitude= new ArrayList<>();
    ArrayList<String> events_phone= new ArrayList<>();
    ArrayList<String> events_city= new ArrayList<>();
    ArrayList<String> events_image= new ArrayList<>();
    ArrayList<String> active_status= new ArrayList<>();

    ArrayList<String> day_plan_id = new ArrayList<>();
    ArrayList<String> day_category = new ArrayList<>();
    ArrayList<String> day_item_id = new ArrayList<>();
    ArrayList<String> day_item_name = new ArrayList<>();
    ArrayList<String> day_user_id = new ArrayList<>();
    ArrayList<String> day_date = new ArrayList<>();
    ArrayList<String> day_time = new ArrayList<>();
    ArrayList<String> day_complete = new ArrayList<>();
    ArrayList<String> day_status = new ArrayList<>();
    ArrayList<String> day_desc = new ArrayList<>();
    
    
    View.OnClickListener Onclk = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == prevMonth) {
                if (month <= 1) {
                    month = 12;
                    year--;
                } else {
                    month--;
                }
                //	Log.d(tag, "Setting Prev Month in GridCellAdapter: " + "Month: " + month + " Year: " + year);
                setGridCellAdapterToDate(month, year);
            } else if (v == nextMonth) {
                if (month > 11) {
                    month = 1;
                    year++;
                } else {
                    month++;
                }
                //	Log.d(tag, "Setting Next Month in GridCellAdapter: " + "Month: " + month + " Year: " + year);
                setGridCellAdapterToDate(month, year);
            }
            animationLoading();

            Runnable run = new Runnable() {
                @Override
                public void run() {
                    animation_layout.setVisibility(View.GONE);
                }
            };
            Handler handler = new Handler();
            handler.postDelayed(run, 2000);
            postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.TRAVEX_EVENTS_AND_MONTHLY_PLAN_URL,
                    getjsonObject(TravexApplication.TRAVEX_EVENTS_AND_MONTHLY_PLAN_TAG),
                    TravexApplication.REQUEST_ID_TRAVEX_EVENTS_AND_MONTHLY_PLAN);

        }
    };


//    private ListView todays_event_list;
    RecyclerView rv_travex_events,rv_todaysevents;
    private boolean all_checked = true, worksheet_checked = false, product_checked = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        act = getActivity();

        rootView = inflater.inflate(R.layout.myplan_fragment, container, false);
        initiating_APICredentials();
        animation_layout=(RelativeLayout)rootView.findViewById(R.id.settings_animation);



        Plan_ID=new ArrayList<String>();
        Plan_category=new ArrayList<String>();
        Plan_item=new ArrayList<String>();
        Plan_member=new ArrayList<String>();
        Plan_date=new ArrayList<String>();
        Plan_time=new ArrayList<String>();
        Plan_status=new ArrayList<String>();
        Plan_created=new ArrayList<String>();
        Plan_item_userId=new ArrayList<String>();
        Plan_itemid=new ArrayList<String>();


        events_item_id = new ArrayList<>();
        events_item_name= new ArrayList<>();
        events_description= new ArrayList<>();
        events_start_date= new ArrayList<>();
        events_end_date= new ArrayList<>();
        events_start_time= new ArrayList<>();
        events_end_time= new ArrayList<>();
        events_latitude= new ArrayList<>();
        events_longitude= new ArrayList<>();
        events_phone= new ArrayList<>();
        events_city= new ArrayList<>();
        events_image= new ArrayList<>();
        active_status= new ArrayList<>();
        
        
        
        try {
            due_date_List = new ArrayList<String>();
            pickDueDates();
        } catch (Exception e) {
            e.printStackTrace();
        }
        _calendar = Calendar.getInstance(Locale.getDefault());
        month = _calendar.get(Calendar.MONTH) + 1;
        year = _calendar.get(Calendar.YEAR);

        prevMonth = (ImageView) this.rootView.findViewById(R.id.prevMonth);
        prevMonth.setOnClickListener(Onclk);

        currentMonth = (Button) this.rootView.findViewById(R.id.currentMonth);
        currentMonth.setText(DateFormat.format(dateTemplate, _calendar.getTime()));

        nextMonth = (ImageView) this.rootView.findViewById(R.id.nextMonth);
        nextMonth.setOnClickListener(Onclk);

        calendarView = (GridView) this.rootView.findViewById(R.id.calendar);
        tv_todays_event = (TextView) this.rootView.findViewById(R.id.tv_todays_event);
        tv_travex_events = (TextView) this.rootView.findViewById(R.id.tv_travex_events);
        tv_nodata= (TextView) this.rootView.findViewById(R.id.tv_nodata);
        rv_travex_events = (RecyclerView) rootView.findViewById(R.id.rv_travex_events);
        rv_travex_events.setNestedScrollingEnabled(false);
        rv_travex_events.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_todaysevents = (RecyclerView) rootView.findViewById(R.id.rv_todaysevents);
        rv_todaysevents.setNestedScrollingEnabled(false);
        rv_todaysevents.setLayoutManager(new LinearLayoutManager(getActivity()));


        //   accountoutletlist.setOnItemClickListener(OnItemclk);
//        postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.LIST_MYPLAN_URL,
//                getjsonObject(TravexApplication.LIST_MYPLAN_TAG),
//                TravexApplication.REQUEST_ID_LIST_MYPLAN);
        animationInit();
        animationLoading();

        Runnable run = new Runnable() {
            @Override
            public void run() {
                animation_layout.setVisibility(View.GONE);
            }
        };
        Handler handler = new Handler();
        handler.postDelayed(run, 2000);
        postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.TRAVEX_EVENTS_AND_MONTHLY_PLAN_URL,
                getjsonObject(TravexApplication.TRAVEX_EVENTS_AND_MONTHLY_PLAN_TAG),
                TravexApplication.REQUEST_ID_TRAVEX_EVENTS_AND_MONTHLY_PLAN);

//        postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.ADD_PLAN_URL,
//                getjsonObject(TravexApplication.ADD_PLAN_TAG),
//                TravexApplication.REQUEST_ID_ADD_PLAN);
//        todays_eventlist = new ArrayList<String>();
//        for(int i=0;i<Plan_category.size();i++){
//            todays_eventlist.add(Plan_category.get(i));
//        }
//
//        travex_eventlist = new ArrayList<String>();
//        for(int i=0;i<Plan_ID.size();i++){
//            todays_eventlist.add(Plan_ID.get(i));
//        }



        adapter = new GridCellAdapter(getActivity().getApplicationContext(), R.id.calendar_day_gridcell, month, year);
        adapter.notifyDataSetChanged();
        calendarView.setAdapter(adapter);


        tv_todays_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                SetTodaysEventAdapter();
            }
        });
        tv_travex_events.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                SetTravexEventAdapter();
            }
        });


        return rootView;
    }

    private void setGridCellAdapterToDate(int month, int year) {
        adapter = new GridCellAdapter(getActivity().getApplicationContext(), R.id.calendar_day_gridcell, month, year);
        _calendar.set(year, month - 1, _calendar.get(Calendar.DAY_OF_MONTH));
        currentMonth.setText(DateFormat.format(dateTemplate, _calendar.getTime()));
        adapter.notifyDataSetChanged();
        calendarView.setAdapter(adapter);
    }

    public void initiating_APICredentials() {
        mJsonObjectMaker = new JsonObjectMaker(act);
        mNetworkManager = NetworkManager.getSingleInstance(act);
        mPreferenceManager = new PreferenceManager(act);
        mListCreatorForNameValuePairRequest = new ListCreatorForNameValuePairRequest(act);
        mNetworkManager.setOnListPlanResponseListener(this);
        mNetworkManager.setOnAddPlanResponseListener(this);
        mNetworkManager.setOnGetTravexEventPlanResponseListener(this);
        mNetworkManager.setOnGetDayPlanResponseListener(this);

    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
//        act.getWindow().setSoftInputMode(
//                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//        context.getResources().getDrawable(R.drawable.comment);
    }
    @Override
    public void onAddPlanResponse(boolean status) {
        if (status) {




        } else {

        }
    }

    @Override
    public void onGetListPlanResponse(boolean status, JSONObject jsonObject) {

        if (status) {
            Plan_ID.clear();
            Plan_category.clear();
            Plan_item.clear();
            Plan_member.clear();
            Plan_date.clear();
            Plan_time.clear();
            Plan_status.clear();
            Plan_created.clear();

            try {
                JSONArray result = jsonObject.getJSONArray("result");
                for(int i= 0; i < result.length(); i++) {
                    JSONObject planItem = result.getJSONObject(i);
                    Plan_ID.add(planItem.getString("plan_id"));
                    Plan_category.add(planItem.getString("category"));
                    Plan_item.add(planItem.getString("item_name"));
                    Plan_itemid.add(planItem.getString("item_id"));
                    Plan_item_userId.add(planItem.getString("user_id"));
                    //Plan_member.add(jjj.getString("my_plan_member"));
                    Plan_date.add(planItem.getString("date"));
                    Plan_time.add(planItem.getString("time"));
                    Plan_status.add(planItem.getString("status"));
                    Plan_created.add(planItem.getString("date"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Calendar cal=Calendar.getInstance();
            int year = cal.get(Calendar.YEAR);
            int month = cal.get(Calendar.MONTH)+1;
            int day = cal.get(Calendar.DAY_OF_MONTH);
            String cur_date = year+"-"+String.format("%02d", month)+"-"+String.format("%02d", day);


            todays_eventlist = new ArrayList<String>();
            for(int i=0;i<Plan_item.size();i++){
                if(Plan_date.get(i).equals(cur_date))
                todays_eventlist.add(Plan_item.get(i));
            }

//            travex_eventlist = new ArrayList<String>();
//            travex_eventdesc = new ArrayList<String>();
//            for(int i=0;i<Plan_ID.size();i++){
//                travex_eventlist.add(Plan_ID.get(i));
//            }
//            list_view.setVisibility(View.VISIBLE);
//            list_adapter = new ArrayAdapter<String>(getActivity().getApplicationContext(), android.R.layout.simple_dropdown_item_1line, todays_eventlist);
//            list_view.setAdapter(list_adapter);
            //SetTravexEventAdapter();

            if(jsonObject!=null){

          }



        } else {

        }
        setGridCellAdapterToDate(month,year);
    }

    private void pickDueDates() {
        due_date_List = fetchtodo.get("DueDate");

    }

    @SuppressWarnings("unused")
    private Typeface getTypeFace() {

        Typeface tf = Typeface.createFromAsset(act.getAssets(), "MyriadPro_Regular.otf");
        return tf;
    }

    public void postJsonRequest(int request_type, final Map<String, String> request_header, String url, JSONObject jsonObject, final int request_id) {

        mNetworkManager.postJsonRequest(request_type, request_header, url, jsonObject, request_id);

    }


    public JSONObject getJson(ArrayList<String> namePair, ArrayList<String> valuePair) {

        return mJsonObjectMaker.getJson(namePair, valuePair);
    }

    public JSONObject getjsonObject(String tag) {


        if (tag.equals(TravexApplication.DELETE_PLAN_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfDeleteplan
                            (

                                    TravexApplication.DELETE_USER_ID, TravexApplication.DELETE_PLAN_ID

                            ),
                    mListCreatorForNameValuePairRequest.
                            getListOfDeleteplan(mPreferenceManager.getRegisration_Id_response(), "1")

            );

        }else if (tag.equals(TravexApplication.LIST_MYPLAN_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfPlanList
                            (TravexApplication.LIST_MY_PLAN_MEMBER

                            ),
                    mListCreatorForNameValuePairRequest.getListOfPlanList(mPreferenceManager.getRegisration_Id_response())

            );

        }
        else if (tag.equals(TravexApplication.TRAVEX_EVENTS_AND_MONTHLY_PLAN_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfTravexEventAndMonthlyPlan
                            (

                                    TravexApplication.TRAVEX_EVENTS_AND_MONTHLY_PLAN_USER_ID,
                                    TravexApplication.TRAVEX_EVENTS_AND_MONTHLY_PLAN_MONTH,
                                    TravexApplication.TRAVEX_EVENTS_AND_MONTHLY_PLAN_YEAR

                            ),
                    mListCreatorForNameValuePairRequest.
                            getListOfTravexEventAndMonthlyPlan(mPreferenceManager.getRegisration_Id_response(),String.valueOf(month) ,String.valueOf(year) )
//mPreferenceManager.getRegisration_Id_response(),zrzore_vq_28
            );
//mPreferenceManager.getRegisration_Id_response()  for getting user id
        }else if (tag.equals(TravexApplication.DAY_PLAN_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfDayplan
                            (

                                    TravexApplication.USER_ID_DAY_PLAN, TravexApplication.DATE_DAY_PLAN

                            ),
                    mListCreatorForNameValuePairRequest.
                            getListOfDayplan(mPreferenceManager.getRegisration_Id_response(), date_month_year)   //"2016-04-15"

            );

        }

        return null;

    }
    public String getCategory_Id_Tips_For(String item) {

        if (item.equals("Airline")) {

            return "pngrtbel_vq_1";
        } else if (item.equals("Shopping")) {

            return "pngrtbel_vq_10";
        } else if (item.equals("Business")) {

            return "pngrtbel_vq_11";
        } else if (item.equals("Real Estate")) {

            return "pngrtbel_vq_12";
        } else if (item.equals("Money Exchange")) {

            return "pngrtbel_vq_13";
        } else if (item.equals("Car Rentals")) {

            return "pngrtbel_vq_14";
        } else if (item.equals("Embasies & Consulates")) {

            return "pngrtbel_vq_2";
        } else if (item.equals("Tour & Travel")) {

            return "pngrtbel_vq_3";
        } else if (item.equals("Events")) {

            return "pngrtbel_vq_4";
        } else if (item.equals("Eating Out")) {

            return "pngrtbel_vq_5";
        } else if (item.equals("Stay/Hotels")) {

            return "pngrtbel_vq_6";
        } else if (item.equals("nightlife")) {

            return "pngrtbel_vq_7";
        } else if (item.equals("Spa")) {

            return "pngrtbel_vq_8";
        } else if (item.equals("Things To Do")) {

            return "pngrtbel_vq_9";
        } else if (item.equals("Emergencies")) {

            return "pngrtbel_vq_15";
        }


        return null;


    }

    @Override
    public void onGetTravexEventAndMonthlyPlanResponse(boolean status, HashMap<String, ArrayList<String>> monthly_plan_list, HashMap<String, ArrayList<String>> travex_events) {
        if (status) {
            plan_id_list = new ArrayList<>();
            category_list = new ArrayList<>();
            item_id_list = new ArrayList<>();
            item_name_list = new ArrayList<>();
            user_id_list = new ArrayList<>();
            date_list = new ArrayList<>();
            time_list = new ArrayList<>();
            complete_list = new ArrayList<>();
            status_list = new ArrayList<>();


            events_item_id = new ArrayList<>();
            events_item_name= new ArrayList<>();
            events_description= new ArrayList<>();
            events_start_date= new ArrayList<>();
            events_end_date= new ArrayList<>();
            events_start_time= new ArrayList<>();
            events_end_time= new ArrayList<>();
            events_latitude= new ArrayList<>();
            events_longitude= new ArrayList<>();
            events_phone= new ArrayList<>();
            events_city= new ArrayList<>();
            events_image= new ArrayList<>();
            active_status= new ArrayList<>();
            todays_eventlist = new ArrayList<String>();
            travex_eventlist= new ArrayList<String>();
                    
                    
            if(monthly_plan_list!=null)
            {
                for(int k=0;k<monthly_plan_list.size();k++)
                {
                    plan_id_list=monthly_plan_list.get("plan_id");
                    category_list=monthly_plan_list.get("category");
                    item_id_list=monthly_plan_list.get("item_id");
                    item_name_list=monthly_plan_list.get("item_name");
                    user_id_list=monthly_plan_list.get("user_id");
                    date_list=monthly_plan_list.get("date");
                    time_list=monthly_plan_list.get("time");
                    complete_list=monthly_plan_list.get("complete");
                    status_list=monthly_plan_list.get("status");
                }

                Calendar cal=Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH)+1;
                int day = cal.get(Calendar.DAY_OF_MONTH);
                String cur_date = year+"-"+String.format("%02d", month)+"-"+String.format("%02d", day);


                todays_eventlist = item_name_list;
//                for(int i=0;i<item_name_list.size();i++){
//                    //if(item_name_list.get(i).equals(cur_date))
//                        todays_eventlist.add(item_name_list.get(i));
//                }

            }



            if(travex_events!=null)
            {
                for(int k=0;k<travex_events.size();k++)
                {
                    events_item_id=travex_events.get("item_id");
                    events_item_name=travex_events.get("item_name");
                    events_description=travex_events.get("description");
                    events_start_date=travex_events.get("start_date");
                    events_end_date=travex_events.get("end_date");
                    events_start_time=travex_events.get("start_time");
                    events_end_time=travex_events.get("end_time");
                    events_latitude=travex_events.get("latitude");
                    events_longitude=travex_events.get("longitude");
                    events_phone=travex_events.get("phone");
                    events_city=travex_events.get("city");
                    events_image=travex_events.get("image");
                    active_status=travex_events.get("active_status");
//                    showItems("events_category", travex_events);
//                    showItems("active_status", travex_events);
                }


                travex_eventlist =events_item_name;
                travex_eventdesc =events_description;
//                for(int i=0;i<Plan_ID.size();i++){
//                    travex_eventlist.add(Plan_ID.get(i));
//                }


            }



        } else {
            Toast.makeText(getActivity(),"No Events Available",Toast.LENGTH_SHORT).show();;

        }
//        list_view.setVisibility(View.VISIBLE);
//        list_adapter = new ArrayAdapter<String>(getActivity().getApplicationContext(), android.R.layout.simple_dropdown_item_1line, todays_eventlist);
//        list_view.setAdapter(list_adapter);
        SetTravexEventAdapter();

        postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.LIST_MYPLAN_URL,
        getjsonObject(TravexApplication.LIST_MYPLAN_TAG),
        TravexApplication.REQUEST_ID_LIST_MYPLAN);
//        postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.DAY_PLAN_URL,
//                getjsonObject(TravexApplication.DAY_PLAN_TAG),
//                TravexApplication.REQUEST_ID_DAY_PLAN);

    }



    @Override
    public void onGetDayPlanResponse(boolean status, HashMap<String, ArrayList<String>> target_map) {
        //Day Plan List
        day_plan_id = new ArrayList<>();
        day_category = new ArrayList<>();
        day_item_id = new ArrayList<>();
        day_item_name = new ArrayList<>();
        day_user_id = new ArrayList<>();
        day_date = new ArrayList<>();
        day_time = new ArrayList<>();
        day_complete = new ArrayList<>();
        day_status = new ArrayList<>();
        day_desc= new ArrayList<>();

        todays_eventlist = new ArrayList<String>();

        if (status) {
            if(target_map!=null)
            {
                for(int k=0;k<target_map.size();k++)
                {
                    day_plan_id=target_map.get("plan_id");
                    day_category=target_map.get("category");
                    day_item_id=target_map.get("item_id");
                    day_item_name=target_map.get("item_name");
                    day_user_id=target_map.get("user_id");
                    day_date=target_map.get("date");
                    day_time=target_map.get("time");
                    day_complete=target_map.get("complete");
                    day_status=target_map.get("status");


                }

                Calendar cal=Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH)+1;
                int day = cal.get(Calendar.DAY_OF_MONTH);
                String cur_date = year+"-"+String.format("%02d", month)+"-"+String.format("%02d", day);



                todays_eventlist = day_item_name;

                for(int i =0;i<todays_eventlist.size();i++)
                {
                    day_desc.add("");
                }

//                for(int i=0;i<item_name_list.size();i++){
//                    //if(item_name_list.get(i).equals(cur_date))
//                        todays_eventlist.add(item_name_list.get(i));
//                }


            }


        } else {
            //LogUtility.Log(TAG, "onGetDayPlanResponse:failure", null);
//            Toast.makeText(getActivity(),"No Plans found",Toast.LENGTH_SHORT).show();
        }

        SetTodaysEventAdapter();
    }

    public void SetTravexEventAdapter()
    {
        rv_travex_events.setVisibility(View.VISIBLE);
        rv_todaysevents.setVisibility(View.GONE);
        tv_travex_events.setBackground(getResources().getDrawable(R.drawable.bottom_line_orange));
        tv_todays_event.setBackground(getResources().getDrawable(R.drawable.bottom_line_black_dark));
        if(travex_eventlist !=null) {
            if(travex_eventlist.size()>0)
            {
                tv_nodata.setVisibility(View.GONE);
            }
            else
            {
                tv_nodata.setVisibility(View.VISIBLE);
            }
            TravexEventsAdapter TravexEventsAdapter = new TravexEventsAdapter(act, travex_eventlist, travex_eventlist,active_status,events_item_id,events_item_name);
            rv_travex_events.setAdapter(TravexEventsAdapter);
        }
        else
        {
            tv_nodata.setVisibility(View.VISIBLE);
        }
    }

    public void SetTodaysEventAdapter()
    {
        rv_travex_events.setVisibility(View.GONE);
        rv_todaysevents.setVisibility(View.VISIBLE);
        tv_todays_event.setBackground(getResources().getDrawable(R.drawable.bottom_line_orange));
        tv_travex_events.setBackground(getResources().getDrawable(R.drawable.bottom_line_black_dark));
        if(todays_eventlist !=null) {
            if(todays_eventlist.size()>0)
            {
                tv_nodata.setVisibility(View.GONE);
            }
            else
            {
                tv_nodata.setVisibility(View.VISIBLE);
            }
            TravexEventsAdapter TravexEventsAdapter = new TravexEventsAdapter(act, todays_eventlist, null,null,null,null);
            rv_todaysevents.setAdapter(TravexEventsAdapter);
        }
        else
        {
            tv_nodata.setVisibility(View.VISIBLE);
        }

    }



    //Animation Functions
    public void animationLoading(){
        animation_layout.setVisibility(View.VISIBLE);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                ImageView iv1 = (ImageView) rootView.findViewById(R.id.settings_one);
                ImageView iv2 = (ImageView) rootView.findViewById(R.id.settings_two);
                ImageView iv3 = (ImageView) rootView.findViewById(R.id.settings_three);

                iv1.setAnimation(rotateOne);
                iv2.setAnimation(rotateTwo);
                iv3.setAnimation(rotateThree);

                iv1.startAnimation(rotateOne);
                iv2.startAnimation(rotateTwo);
                iv3.startAnimation(rotateThree);
            }
        };
        Handler mHandler = new Handler();
        mHandler.postDelayed(runnable, 100);

    }

    public  void  animationInit(){
        rotateOne = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_one);
        rotateOne.setAnimationListener(this);
        rotateTwo= AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_two);
        rotateTwo.setAnimationListener(this);
        rotateThree= AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_three);
        rotateThree.setAnimationListener(this);
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }


    @Override
    public void onAnimationRepeat(Animation animation) {

    }






    // ///////////////////////////////////////////////////////////////////////////////////////
    // Inner Class
    public class GridCellAdapter extends BaseAdapter  {
        private static final int DAY_OFFSET = 1;
        private final Context _context;
        private final List<String> list;
        private final String[] weekdays = new String[]{"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
        private final String[] months = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

        private final String[] monthsNumeric = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"};

        public int[] daysOfMonth ;
        private final HashMap<String, Integer> eventsPerMonthMap;
        //private final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy");
        @SuppressLint("SimpleDateFormat")
        private final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MMM-dd");
        private int daysInMonth;
        private int currentDayOfMonth;
        private int currentWeekDay;
        private Button gridcell;
        private LinearLayout lin_lay_red_marker;
        private TextView num_events_per_day;
        int curr_position=0;
        // Days in Current Month
        @SuppressWarnings("unchecked")
        public GridCellAdapter(Context context, int textViewResourceId, int month, int year) {
            super();
            this._context = context;
            this.list = new ArrayList<String>();
            //	Log.d(tag, "==> Passed in Date FOR Month: " + month + " " + "Year: " + year);
            Calendar calendar = Calendar.getInstance();
            setCurrentDayOfMonth(calendar.get(Calendar.DAY_OF_MONTH));
            setCurrentWeekDay(calendar.get(Calendar.DAY_OF_WEEK));

            if (calendar.getActualMaximum(calendar.DAY_OF_YEAR) > 365)
            {
                daysOfMonth = new int[]{31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
            }
            else
            {
                daysOfMonth = new int[]{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
            }

            //						Log.d(tag, "New Calendar:= " + calendar.getTime().toString());
            //						Log.d(tag, "CurrentDayOfWeek :" + getCurrentWeekDay());
            //						Log.d(tag, "CurrentDayOfMonth :" + getCurrentDayOfMonth());

            // Print Month
            printMonth(month, year);

            // Find Number of Events
            eventsPerMonthMap = findNumberOfEventsPerMonth(year, month);
        }

        private String getMonthAsString(int i) {
            return months[i];
        }

        @SuppressWarnings("unused")
        private String getWeekDayAsString(int i) {
            return weekdays[i];
        }

        private int getNumberOfDaysOfMonth(int i) {
            return daysOfMonth[i];
        }

        @Override
        public String getItem(int position) {
            return list.get(position);
        }

        @Override
        public int getCount() {
            return list.size();
        }

        /**
         * Prints Month
         *
         * @param mm
         * @param yy
         */
        @SuppressWarnings("unused")
        private void printMonth(int mm, int yy) {
            //	Log.d(tag, "==> printMonth: mm: " + mm + " " + "yy: " + yy);
            // The number of days to leave blank at
            // the start of this month.
            int trailingSpaces = 0;
            int leadSpaces = 0;
            int daysInPrevMonth = 0;
            int prevMonth = 0;
            int prevYear = 0;
            int nextMonth = 0;
            int nextYear = 0;

            int currentMonth = mm - 1;
            String currentMonthName = getMonthAsString(currentMonth);
            daysInMonth = getNumberOfDaysOfMonth(currentMonth);

            //	Log.d(tag, "Current Month: " + " " + currentMonthName + " having " + daysInMonth + " days.");

            // Gregorian Calendar : MINUS 1, set to FIRST OF MONTH
            GregorianCalendar cal = new GregorianCalendar(yy, currentMonth, 1);
            //Log.d(tag, "Gregorian Calendar:= " + cal.getTime().toString());

            if (currentMonth == 11) {
                prevMonth = currentMonth - 1;
                daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
                nextMonth = 0;
                prevYear = yy;
                nextYear = yy + 1;
                //		Log.d(tag, "*->PrevYear: " + prevYear + " PrevMonth:" + prevMonth + " NextMonth: " + nextMonth + " NextYear: " + nextYear);
            } else if (currentMonth == 0) {
                prevMonth = 11;
                prevYear = yy - 1;
                nextYear = yy;
                daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
                nextMonth = 1;
                //		Log.d(tag, "**--> PrevYear: " + prevYear + " PrevMonth:" + prevMonth + " NextMonth: " + nextMonth + " NextYear: " + nextYear);
            } else {
                prevMonth = currentMonth - 1;
                nextMonth = currentMonth + 1;
                nextYear = yy;
                prevYear = yy;
                daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
                //		Log.d(tag, "***---> PrevYear: " + prevYear + " PrevMonth:" + prevMonth + " NextMonth: " + nextMonth + " NextYear: " + nextYear);
            }

            // Compute how much to leave before before the first day of the
            // month.
            // getDay() returns 0 for Sunday.
            int currentWeekDay = cal.get(Calendar.DAY_OF_WEEK) - 1;
            trailingSpaces = currentWeekDay;

            //						Log.d(tag, "Week Day:" + currentWeekDay + " is " + getWeekDayAsString(currentWeekDay));
            //						Log.d(tag, "No. Trailing space to Add: " + trailingSpaces);
            //						Log.d(tag, "No. of Days in Previous Month: " + daysInPrevMonth);

            if (cal.isLeapYear(cal.get(Calendar.YEAR)) && mm == 1) {
                ++daysInMonth;
            }

            // Trailing Month days
            for (int i = 0; i < trailingSpaces; i++) {
                //		Log.d(tag, "PREV MONTH:= " + prevMonth + " => " + getMonthAsString(prevMonth) + " " + String.valueOf((daysInPrevMonth - trailingSpaces + DAY_OFFSET) + i));
                list.add(String.valueOf((daysInPrevMonth - trailingSpaces + DAY_OFFSET) + i) + "-GREY" + "-" + getMonthAsString(prevMonth) + "-" + prevYear);
            }

            // Current Month Days
            for (int i = 1; i <= daysInMonth; i++) {
                //	Log.d(currentMonthName, String.valueOf(i) + " " + getMonthAsString(currentMonth) + " " + yy);
                if (i == getCurrentDayOfMonth()) {
                    list.add(String.valueOf(i) + "-BLUE" + "-" + getMonthAsString(currentMonth) + "-" + yy);
                } else {
                    list.add(String.valueOf(i) + "-WHITE" + "-" + getMonthAsString(currentMonth) + "-" + yy);
                }
            }

            // Leading Month days
            for (int i = 0; i < list.size() % 7; i++) {
                //		Log.d(tag, "NEXT MONTH:= " + getMonthAsString(nextMonth));
                String newDay = "";
                list.add(String.valueOf(i + 1) + "-GREY" + "-" + getMonthAsString(nextMonth) + "-" + nextYear);
                //list.add(newDay + "-GREY" + "-" + getMonthAsString(nextMonth) + "-" + nextYear);
            }
        }


        @SuppressWarnings("rawtypes")
        private HashMap findNumberOfEventsPerMonth(int year, int month) {
            HashMap map = new HashMap<String, Integer>();

            return map;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            check = 0;
            View row = convertView;
            if (row == null) {
                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.calendar_day_gridcell, parent, false);
            }

            // Get a reference to the Day gridcell
            gridcell = (Button) row.findViewById(R.id.calendar_day_gridcell);
            lin_lay_red_marker= (LinearLayout) row.findViewById(R.id.lin_lay_red_marker);

            gridcell.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(Previousview == null)
                    {
                        Previousview=view;
                    }
                    if(Previousview!=view)
                    {
                        Previousview.setBackgroundResource(android.R.color.white);
                        Previousview=view;
                    }
                    view.setBackgroundResource(R.drawable.dot);
                    date_month_year = (String) view.getTag();


                    postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.DAY_PLAN_URL,
                            getjsonObject(TravexApplication.DAY_PLAN_TAG),
                            TravexApplication.REQUEST_ID_DAY_PLAN);


                    //view.setBackgroundResource(R.drawable.dot);

                    //Toast.makeText(getActivity().getApplicationContext(), "Selected: " + date_month_year.trim() + " **", Toast.LENGTH_LONG).show();

                    //gridcell.setBackgroundColor(getResources().getColor(R.color.red_600));
                }
            });



//            curr_position = curr_position+1;
//            int pos=curr_position-1;



            // ACCOUNT FOR SPACING

            //	Log.d(tag, "Current Day: " + getCurrentDayOfMonth());
            String[] day_color = list.get(position).split("-");
            String day = day_color[0];
            Calendar calendar=Calendar.getInstance();
            DateFormatFunction df=new DateFormatFunction();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String formattedDate = sdf.format(calendar.getTime());
            current_date = df.dateformat("yyyy-MM-dd", "dd-MMMM-yyyy",formattedDate );

//            if (theday.equalsIgnoreCase("1")) {
//                theday = "01";
//            } else if (theday.equalsIgnoreCase("2")) {
//                theday = "02";
//            } else if (theday.equalsIgnoreCase("3")) {
//                theday = "03";
//            } else if (theday.equalsIgnoreCase("4")) {
//                theday = "04";
//            } else if (theday.equalsIgnoreCase("5")) {
//                theday = "05";
//            } else if (theday.equalsIgnoreCase("6")) {
//                theday = "06";
//            } else if (theday.equalsIgnoreCase("7")) {
//                theday = "07";
//            } else if (theday.equalsIgnoreCase("8")) {
//                theday = "08";
//            } else if (theday.equalsIgnoreCase("9")) {
//                theday = "09";
//            }

            String themonth = day_color[2];
            String theyear = day_color[3];
            temp_date=day+"-"+themonth+"-"+theyear;
            String plan_dates="";
            plan_dates=day+"-"+themonth+"-"+theyear;
            plan_dates= df.dateformat("dd-MMMM-yyyy","yyyy-MM-dd",plan_dates);

//            int integer_day=Integer.valueOf(day) ;
//            int integer_month=Integer.valueOf(themonth) ;
//            integer_month=integer_month+1;
//            String dateText = theyear + "-" + String.format("%02d", integer_month) + "-" + String.format("%02d", integer_day);
            if(Plan_date!=null)
            {
                for(int i=0;i<Plan_date.size();i++)
                {
                    String day_events_date=Plan_date.get(i);
                    if(day_events_date.equals(plan_dates))
                    {
                        lin_lay_red_marker.setVisibility(View.VISIBLE);

                        break;
                    }
//                    else
//                    {
//                        lin_lay_red_marker.setVisibility(View.GONE);
//                    }
//                    gridcell.setText(day);
                }
            }
            // Set the Day GridCell
            gridcell.setText(day);
            // gridcell.setTag(theday + "-" + themonth + "-" + theyear);


            if (themonth.equalsIgnoreCase(months[0])) {
                themonth = monthsNumeric[0];
            } else if (themonth.equalsIgnoreCase(months[1])) {
                themonth = monthsNumeric[1];
            } else if (themonth.equalsIgnoreCase(months[2])) {
                themonth = monthsNumeric[2];
            } else if (themonth.equalsIgnoreCase(months[3])) {
                themonth = monthsNumeric[3];
            } else if (themonth.equalsIgnoreCase(months[4])) {
                themonth = monthsNumeric[4];
            } else if (themonth.equalsIgnoreCase(months[5])) {
                themonth = monthsNumeric[5];
            } else if (themonth.equalsIgnoreCase(months[6])) {
                themonth = monthsNumeric[6];
            } else if (themonth.equalsIgnoreCase(months[7])) {
                themonth = monthsNumeric[7];
            } else if (themonth.equalsIgnoreCase(months[8])) {
                themonth = monthsNumeric[8];
            } else if (themonth.equalsIgnoreCase(months[9])) {
                themonth = monthsNumeric[9];
            } else if (themonth.equalsIgnoreCase(months[10])) {
                themonth = monthsNumeric[10];
            } else if (themonth.equalsIgnoreCase(months[11])) {
                themonth = monthsNumeric[11];
            } else if (themonth.equalsIgnoreCase(months[12])) {
                themonth = monthsNumeric[12];
            }

            //gridcell.setTag(theday + "-" + themonth + "-"+theyear);
            gridcell.setTag(theyear + "-" + themonth + "-" + day);
            gridcell.setTextSize(15);
            //	Log.d(tag, "Setting GridCell " + theday + "-" + themonth + "-" + theyear);

            if (day_color[1].equals("GREY")) {
                gridcell.setTextColor(Color.LTGRAY);
                gridcell.setEnabled(false);
                check = 1;
            }
            if (day_color[1].equals("WHITE")) {
                gridcell.setTextColor(Color.BLACK);
                gridcell.setEnabled(true);
            }
            if (day_color[1].equalsIgnoreCase("BLUE")) {
                gridcell.setTextColor(getResources().getColor(R.color.static_text_color));
                gridcell.setEnabled(true);
            }
//            String datestr = gridcell.getTag().toString();
//            if(datestr.equals(date_month_year))
//            {
//                row.setBackgroundResource(R.drawable.dot);
//            }
//            else
//            {
//                row.setBackgroundResource(0);
//            }
            //row.setBackgroundResource(R.drawable.dot);

            DateFormatFunction dff = new DateFormatFunction();

//            for (int i = 0; i < due_date_List.size(); i++) {
//                String datetext = dff.dateformat("yyyy-MM-dd", "dd-MM-yyyy", due_date_List.get(i));
//                if (dateText.equalsIgnoreCase(datetext) && check == 0) {
//                    gridcell.setTextColor(Color.BLUE);
//                }
//            }
        if (current_date.equals(temp_date)){
            gridcell.setTextColor(getResources().getColor(R.color.theme));
           // gridcell.setBackgroundResource(R.drawable.cc);
        }

//            else
//            {
//                lin_lay_red_marker.setVisibility(View.GONE);
//            }

            return row;
        }
        View Previousview=null;



        public int getCurrentDayOfMonth() {
            return currentDayOfMonth;
        }

        private void setCurrentDayOfMonth(int currentDayOfMonth) {
            this.currentDayOfMonth = currentDayOfMonth;
        }

        public int getCurrentWeekDay() {
            return currentWeekDay;
        }

        public void setCurrentWeekDay(int currentWeekDay) {
            this.currentWeekDay = currentWeekDay;
        }
    }


}