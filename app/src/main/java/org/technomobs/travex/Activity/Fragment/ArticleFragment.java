package org.technomobs.travex.Activity.Fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Gallery;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.q42.android.scrollingimageview.ScrollingImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Controller.Interface.GetArticleDetailsResponse;
import org.technomobs.travex.Controller.Interface.GetArticlesListResponse;
import org.technomobs.travex.Controller.JsonObjectMaker;
import org.technomobs.travex.Controller.NetworkManager;
import org.technomobs.travex.Controller.NetworkOptions;
import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.DateFormatFunction;
import org.technomobs.travex.Utillity.ListCreatorForNameValuePairRequest;
import org.technomobs.travex.Utillity.LogUtility;

import java.util.ArrayList;
import java.util.Calendar;

import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;
import jp.wasabeef.recyclerview.animators.adapters.AlphaInAnimationAdapter;

/**
 * Created by Abhi on 07-03-2016.
 */
public class ArticleFragment extends Fragment implements GetArticlesListResponse, GetArticleDetailsResponse
        ,View.OnClickListener,Animation.AnimationListener  {
    static Activity activity;
    private final int REPEAT_COUNT = 1;
    private final int REPEAT_MODE = 7;
    Gallery image_gallery;
    ArrayList<Integer> ImagesArray=new ArrayList<Integer>();
    ArticleAdapter ArticleAdapter ;
    ArticleImageAdapter ArticleImageAdapter;

    //HorizontalScrollView horizontal_scroll;
    RecyclerView recyclerView;
    View rootView;
    ViewGroup.MarginLayoutParams imageViewParams;
    //LinearLayout layout;
    RecyclerView rv_images;
    LinearLayout lin_lay_not_found;
//    ArrayList<String> tv_header_list = new ArrayList<String>();
//    ArrayList<String> tv_details_list = new ArrayList<String>();
//    ArrayList<String> sdv_detailsimage_list = new ArrayList<String>();
    ArrayList<String> articles_id_list = new ArrayList<>();
    ArrayList<String> articles_title_list = new ArrayList<>();
    ArrayList<String> image_list = new ArrayList<>();
    ArrayList<String> articles_content_list = new ArrayList<>();
    ArrayList<String> articles_category_list = new ArrayList<>();
    ArrayList<String> category_name_list = new ArrayList<>();
    ArrayList<String> category_key_list = new ArrayList<>();


    //API Credentials
    static NetworkManager mNetworkManager = null;
    public static final String TAG = TravelEssential.class.getSimpleName();
    JsonObjectMaker mJsonObjectMaker = null;
    ListCreatorForNameValuePairRequest mListCreatorForNameValuePairRequest = null;
    PreferenceManager mPreferenceManager = null;

    String country = null;
    String country_id = null;
    String category_name,Featured;
    int year,month;
    ImageView img_left,img_right;
    TextView tv_date;
    DateFormatFunction df = new DateFormatFunction();

    Animation rotateOne, rotateTwo, rotateThree;
    RelativeLayout animation_layout;

    public static ArticleFragment newInstance(Activity act){
        ArticleFragment fragment = new ArticleFragment();
        activity=act;
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Fresco.initialize(activity);
        rootView = inflater.inflate(R.layout.articlefragmentview, container, false);
        initiating_APICredentials();
        initialization(rootView);
        GettingPreferenceValues();
        CallingAPI();

        animationInit();
        animationLoading();

        Runnable run = new Runnable() {
            @Override
            public void run() {
                animation_layout.setVisibility(View.GONE);
            }
        };
        Handler handler = new Handler();
        handler.postDelayed(run, 2000);
        ChangeToolBarDesignBackToPrevious();


//            ObjectAnimator animator = ObjectAnimator.ofInt(horizontal_scroll, "scrollX", horizontal_scroll.getWidth());
//            animator.setDuration(800);
//            animator.start();
        //}
        return rootView;
    }

    public void initiating_APICredentials()
    {
        mJsonObjectMaker = new JsonObjectMaker(getActivity());
        mListCreatorForNameValuePairRequest = new ListCreatorForNameValuePairRequest(getActivity());
        mNetworkManager = NetworkManager.getSingleInstance(getActivity());
        mNetworkManager.setOnGetArticlesListResponseListener(this);
        mNetworkManager.setOnGetArticlesDetailsResponseListener(this);
        mPreferenceManager = new PreferenceManager(getActivity());
    }

    public void initialization(View view)
    {
        animation_layout=(RelativeLayout)rootView.findViewById(R.id.settings_animation);
        lin_lay_not_found=(LinearLayout)rootView.findViewById(R.id.lin_lay_not_found);
        imageViewParams = new ViewGroup.MarginLayoutParams(
                ViewGroup.MarginLayoutParams.MATCH_PARENT,
                ViewGroup.MarginLayoutParams.MATCH_PARENT);
        //layout = (LinearLayout) view.findViewById(R.id.linear);

        //horizontal_scroll  = (HorizontalScrollView)view.findViewById(R.id.horizontal_scroll);
        recyclerView= (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        rv_images= (RecyclerView)view.findViewById(R.id.rv_images);
        img_left=(ImageView)view.findViewById(R.id.img_left);
        img_right=(ImageView)view.findViewById(R.id.img_right);
        tv_date=(TextView)view.findViewById(R.id.tv_date);
        img_right.setOnClickListener(this);
        img_left.setOnClickListener(this);

        category_name = MapShowingFragment.selected_category;
        Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH)+1;

        String Date_Str=String.format("%02d", month)+"-"+String.valueOf(year);
        Date_Str = df.dateformat("MM-yyyy", "MMMM yyyy", Date_Str);
        tv_date.setText(Date_Str);
        Featured = null;
//        ScrollingImageView scrollingBackground = (ScrollingImageView) view.findViewById(R.id.scrollingimageview);
//        scrollingBackground.stop();
//        scrollingBackground.start();


    }

    public void GettingPreferenceValues()
    {
        LogUtility.Log(TAG, "onEnterKeyClick", null);
        country = mPreferenceManager.getSelectedCountryName();
        //country = ((ProfileActivity) getActivity()).getSelectedCountry();
        LogUtility.Log(TAG, "onEnterKeyClick:", "selected country from actionbar:" + country);
        //country_id = ((ProfileActivity) getActivity()).fetchCountryIdForCountry(country);
        country_id = mPreferenceManager.getSelectedcountryId();
        LogUtility.Log(TAG, "onEnterKeyClick:", "country id:" + country_id);
    }

    public void CallingAPI()
    {
        if(MapShowingFragment.getCategory_Id_Tips_For(category_name) == null && Featured==null) {

            //this is for articles list full
            postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_ARTICLES_LIST_URL,
                    getjsonObject(TravexApplication.GET_ARTICLES_LIST_FULLFLEDGE_TAG), TravexApplication.REQUEST_ID_ARTICLE_LIST_FULLFLEDGE);

        }
        else if(Featured==null) {
            //this is for categorized article list
            postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_ARTICLES_LIST_URL,
                    getjsonObject(TravexApplication.GET_ARTICLES_LIST_CATEGORYBASED_TAG), TravexApplication.REQUEST_ID_ARTICLE_LIST_FULLFLEDGE);
        }
        else
        {
//this is for featured article list
            postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_ARTICLES_LIST_URL,
                    getjsonObject(TravexApplication.GET_ARTICLES_LIST_FEATURED_TAG), TravexApplication.REQUEST_ID_ARTICLE_LIST_FULLFLEDGE);
        }




//        //this is for articles details api
//        postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_ARTICLES_DETAILS_URL,
//                getjsonObject(TravexApplication.GET_ARTICLES_DETAILS_TAG), TravexApplication.REQUEST_ID_ARTICLE_DETAILS);
    }

    public void ChangeToolBarDesignBackToPrevious()
    {
        //To change the current design of the Toolbar in this fragment
        Toolbar toolbar = (Toolbar)getActivity().findViewById(R.id.tool_bar);
        TextView textToolHeader = (TextView) toolbar.findViewById(R.id.tv_category);
        //textToolHeader.setText(MapShowingFragment.selected_category);
//        textToolHeader.setVisibility(View.GONE);
        SimpleDraweeView image = (SimpleDraweeView)toolbar.findViewById(R.id.networkImageViewEqWNH);
        image.setVisibility(View.VISIBLE);

        toolbar.setNavigationIcon(R.drawable.ic_action_menu);
    }

    public void SetAdapter()
    {
//        String URL = "https://www.planwallpaper.com/static/images/Winter-Tiger-Wild-Cat-Images.jpg";
//        tv_header_list.add("Dubais Hottest night life");
//        tv_details_list.add("kefhieq fh eifiuwfe iweufh wefiuwhf wefiuhe fiwufu jhd jhgdwf  khwkjf kjwfk. jqqkue kuwefhwefh wefuhi weiuf wefhiwhf. gjhwebf iuwhf iwefi wfewef iwehfiweb fiwe fiw efwiefwef wegf iwef wef wiegf wiefg wef weifgwe fiuwef wief weugfi wefuwe fiwef. kefhieq fh eifiuwfe iweufh wefiuwhf wefiuhe fiwufu jhd jhgdwf  khwkjf kjwfk. jqqkue kuwefhwefh wefuhi weiuf wefhiwhf. gjhwebf iuwhf iwefi wfewef iwehfiweb fiwe fiw efwiefwef wegf iwef wef wiegf wiefg wef weifgwe fiuwef wief weugfi wefuwe fiwef");
//        sdv_detailsimage_list.add(URL);
//        tv_header_list.add("Dubais Hottest night life");
//        tv_details_list.add("kefhieq fh eifiuwfe iweufh wefiuwhf wefiuhe fiwufu jhd jhgdwf  khwkjf kjwfk. jqqkue kuwefhwefh wefuhi weiuf wefhiwhf. gjhwebf iuwhf iwefi wfewef iwehfiweb fiwe fiw efwiefwef wegf iwef wef wiegf wiefg wef weifgwe fiuwef wief weugfi wefuwe fiwef. kefhieq fh eifiuwfe iweufh wefiuwhf wefiuhe fiwufu jhd jhgdwf  khwkjf kjwfk. jqqkue kuwefhwefh wefuhi weiuf wefhiwhf. gjhwebf iuwhf iwefi wfewef iwehfiweb fiwe fiw efwiefwef wegf iwef wef wiegf wiefg wef weifgwe fiuwef wief weugfi wefuwe fiwef");
//        sdv_detailsimage_list.add(URL);
//
//        for (int i = 0; i < image_list.size(); i++) {
//            SimpleDraweeView imageView = new SimpleDraweeView(activity);
////            imageViewParams.setMargins(0, 0, 10, 0);
//            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
////            imageView.setLayoutParams(imageViewParams);
//            imageView.setId(i);
////            imageView.setImageBitmap(BitmapFactory.decodeResource(
////                    getResources(), R.drawable.travex_logo));
//            Uri img_uri=Uri.parse(image_list.get(i));
////            ImageRequest request = ImageRequest.fromUri(img_uri);
////
////            DraweeController controller = Fresco.newDraweeControllerBuilder()
////                    .setImageRequest(request)
////                    .setOldController(imageView.getController()).build();
////            //Log.e(TAG, "ImagePath uri " + img_uri);
////
////            imageView.setController(controller);
//            imageView.setImageURI(img_uri);
//
//            layout.addView(imageView);
//        }

        ArrayList<String>Image_URL_List = new ArrayList<>();
        for(int i=0;i<image_list.size();i++)
        {
            if(!image_list.get(i).equals(""))
            {
                Image_URL_List.add(image_list.get(i));
            }
        }

        rv_images.setItemAnimator(new SlideInLeftAnimator());
        rv_images.getItemAnimator().setMoveDuration(1000);
        LinearLayoutManager lin_lay_rv_images = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
        rv_images.setLayoutManager(lin_lay_rv_images);

        ArticleImageAdapter = new ArticleImageAdapter(activity, Image_URL_List,articles_title_list);
        AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(ArticleImageAdapter);
        alphaAdapter.setDuration(1000);
        rv_images.setAdapter(alphaAdapter);

        //rv_images.setAdapter(ArticleImageAdapter);
        //rv_images.scrollToPosition(6);

        ArticleAdapter = new ArticleAdapter(activity,articles_title_list,articles_content_list,image_list,category_key_list,articles_id_list);
        recyclerView.setAdapter(ArticleAdapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());


//        ArticleAdapter.SetOnItemClickListener(new org.technomobs.travex.Activity.Fragment.ArticleAdapter.OnItemClickListener() {
//
//            @Override
//            public void onItemClick(View v, int position) {
//                // TODO Auto-generated method stub
//                Toast.makeText(activity, position, Toast.LENGTH_SHORT);
//
//            }
//        });

        //horizontal_scroll.setAnimation(AnimationUtils.loadAnimation(activity, R.anim.slide_out_left));
    }

    @Override
    public void onGetArticlesListResponse(boolean status, JSONObject object) {
        lin_lay_not_found.setVisibility(View.GONE);
        articles_id_list = new ArrayList<>();
        articles_title_list = new ArrayList<>();
        image_list = new ArrayList<>();
        articles_content_list = new ArrayList<>();
        articles_category_list = new ArrayList<>();
        category_name_list = new ArrayList<>();
        category_key_list = new ArrayList<>();

        if (status) {

            LogUtility.Log(TAG, "onGetArticlesListResponse: :success", "json data:" + object.toString());
            try {
                JSONArray result_array = object.getJSONArray("result");
                for(int i=0;i<result_array.length();i++)
                {
                    JSONObject data_object = result_array.getJSONObject(i);

                    String articles_id_str="",articles_title_str="",image_str="",articles_content_str="",articles_category_str=""
                            ,category_name_str="",category_key_str="";

                    if(articles_id_str==null)articles_id_str="";
                    if(articles_title_str==null)articles_title_str="";
                    if(image_str==null)image_str="";
                    if(articles_content_str==null)articles_content_str="";
                    if(articles_category_str==null)articles_category_str="";
                    if(category_name_str==null)category_name_str="";
                    if(category_key_str==null)category_key_str="";

                    articles_id_str = data_object.getString("articles_id");
                    articles_title_str = data_object.getString("articles_title");
                    image_str = data_object.getString("image");
                    articles_content_str = data_object.getString("articles_content");
                    articles_category_str = data_object.getString("articles_category");
                    category_name_str = data_object.getString("category_name");
                    category_key_str = data_object.getString("category_key");
                    if(!image_str.equals(""))
                    {
                        image_str=AppConstants.GET_ARTICLES_DETAILS_IMAGE_ASSET_URL+"/"+image_str;
                    }

                    articles_id_list.add(articles_id_str);
                    articles_title_list.add(articles_title_str);
                    image_list.add(image_str);
                    articles_content_list.add(articles_content_str);
                    articles_category_list.add(articles_category_str);
                    category_name_list.add(category_name_str);
                    category_key_list.add(category_key_str);

                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            LogUtility.Log(TAG, "onGetArticlesListResponse: :failure", null);
            lin_lay_not_found.setVisibility(View.VISIBLE);
            String errormessage = "";
            //{"status":0,"result":{"error_message":"No Articles Found"}}
            try {
                JSONObject result_array = object.getJSONObject("result");
                errormessage=result_array.getString("error_message");

                //Toast.makeText(activity, errormessage, Toast.LENGTH_SHORT).show();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        //Setting Adapter
        try {
            SetAdapter();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onGetArticlesDetailsResponse(boolean status, JSONObject jsonObject) {
        if (status) {

            LogUtility.Log(TAG, "onGetArticlesDetailsResponse: :success", "json data:" + jsonObject.toString());


        } else {
            LogUtility.Log(TAG, "onGetArticlesDetailsResponse: :failure", null);

        }
    }


    public JSONObject getjsonObject(String tag) {
        if (tag.equals(TravexApplication.GET_ARTICLES_LIST_FULLFLEDGE_TAG)) {

            return getJson(mListCreatorForNameValuePairRequest.getListOfArticles(
                            TravexApplication.GET_ARTICLES_LIST_CITY_ID,
                            TravexApplication.GET_ARTICLES_LIST_MONTH,
                            TravexApplication.GET_ARTICLES_LIST_YEAR

                    ),
                    mListCreatorForNameValuePairRequest.getListOfArticles(country_id,  Integer.toString(month), Integer.toString(year)));

        } else if (tag.equals(TravexApplication.GET_ARTICLES_LIST_CATEGORYBASED_TAG)) {
            return getJson(mListCreatorForNameValuePairRequest.getListOfArticles(
                            TravexApplication.GET_ARTICLES_LIST_CITY_ID,
                            TravexApplication.GET_ARTICLES_LIST_MONTH,
                            TravexApplication.GET_ARTICLES_LIST_YEAR, TravexApplication.GET_ARTICLES_LIST_CATEGORY_ID

                    ),
                    mListCreatorForNameValuePairRequest.getListOfArticles(country_id, Integer.toString(month), Integer.toString(year), MapShowingFragment.getCategory_Id_Tips_For(category_name)));


        } else if (tag.equals(TravexApplication.GET_ARTICLES_LIST_FEATURED_TAG)) {
            return getJson(mListCreatorForNameValuePairRequest.getListOfArticles(
                            TravexApplication.GET_ARTICLES_LIST_CITY_ID,
                            TravexApplication.GET_ARTICLES_LIST_MONTH,
                            TravexApplication.GET_ARTICLES_LIST_YEAR,
                            TravexApplication.GET_ARTICLES_LIST_FEATURED

                    ),
                    mListCreatorForNameValuePairRequest.getListOfArticles(country_id,Integer.toString(month), Integer.toString(year), Featured));

        }
        else if (tag.equals(TravexApplication.GET_ARTICLES_DETAILS_TAG)) {
            return getJson(mListCreatorForNameValuePairRequest.getListOfArticlesDetails(
                            TravexApplication.GET_ARTICLES_ID


                    ),
                    mListCreatorForNameValuePairRequest.getListOfArticlesDetails(mPreferenceManager.getArticles_id().get(0)));

        }
        return null;
    }


    public void postJsonRequest(int request_type, String url, JSONObject jsonObject, final int request_id) {

        mNetworkManager.postJsonRequest(request_type, url, jsonObject, request_id);
    }

    public JSONObject getJson(ArrayList<String> namePair, ArrayList<String> valuePair) {

    JSONObject hdhhd = mJsonObjectMaker.getJson(namePair, valuePair);
        return mJsonObjectMaker.getJson(namePair, valuePair);
    }


    @Override
    public void onClick(View v) {
        String Date_Str="";
        Runnable run;
        Handler handler;
        switch (v.getId())
        {
            case R.id.img_left:

                animationInit();
                animationLoading();

                run = new Runnable() {
                    @Override
                    public void run() {
                        animation_layout.setVisibility(View.GONE);
                    }
                };
                handler = new Handler();
                handler.postDelayed(run, 2000);


                if(month == 1)
                {
                    month=12;
                    year=year-1;
                }
                else
                {
                    month = month-1;
                }
                Date_Str=String.format("%02d", month)+"-"+String.valueOf(year);
                Date_Str = df.dateformat("MM-yyyy", "MMMM yyyy", Date_Str);
                tv_date.setText(Date_Str);
                CallingAPI();
                break;
            case R.id.img_right:

                animationInit();
                animationLoading();

                run = new Runnable() {
                    @Override
                    public void run() {
                        animation_layout.setVisibility(View.GONE);
                    }
                };
                handler = new Handler();
                handler.postDelayed(run, 2000);


                if(month == 12)
                {
                    year=year+1;
                    month=1;
                }
                else
                {
                    month = month+1;
                }
                Date_Str=String.format("%02d", month)+"-"+String.valueOf(year);
                Date_Str = df.dateformat("MM-yyyy", "MMMM yyyy", Date_Str);
                tv_date.setText(Date_Str);
                CallingAPI();
                break;

            default:
                break;
        }
    }


    //Animation Functions
    public void animationLoading(){
        animation_layout.setVisibility(View.VISIBLE);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                ImageView iv1 = (ImageView) rootView.findViewById(R.id.settings_one);
                ImageView iv2 = (ImageView) rootView.findViewById(R.id.settings_two);
                ImageView iv3 = (ImageView) rootView.findViewById(R.id.settings_three);

                iv1.setAnimation(rotateOne);
                iv2.setAnimation(rotateTwo);
                iv3.setAnimation(rotateThree);

                iv1.startAnimation(rotateOne);
                iv2.startAnimation(rotateTwo);
                iv3.startAnimation(rotateThree);
            }
        };
        Handler mHandler = new Handler();
        mHandler.postDelayed(runnable, 100);

    }

    public  void  animationInit(){
        rotateOne = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_one);
        rotateOne.setAnimationListener(this);
        rotateTwo= AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_two);
        rotateTwo.setAnimationListener(this);
        rotateThree= AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_three);
        rotateThree.setAnimationListener(this);
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }


    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}

