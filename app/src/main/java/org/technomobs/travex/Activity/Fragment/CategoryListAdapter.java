package org.technomobs.travex.Activity.Fragment;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import org.technomobs.travex.R;

import java.util.ArrayList;

/**
 * Created by Abhi on 03-03-2016.
 */
public class CategoryListAdapter  extends BaseAdapter {
    private static final int VIEW_TYPE_HEADER = 0;
    private static final int VIEW_TYPE_FOOTER = 1;
    private static final int VIEW_TYPE_DEFAULT = 2;
    private static final int VIEW_TYPE_COUNT = 3;
    private Context context;
    LayoutInflater mInflater;
    String selected_category,selected_categoryid;
    ArrayList<CategoryListItems> categoryItemsnew;
    private String apptheme,category_name;
    public int checki = 0;
    int count = 0;

    public CategoryListAdapter(Context context, ArrayList<CategoryListItems> categoryItemsnew
    ) {
        this.context = context;

        this.categoryItemsnew = categoryItemsnew;


        mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

    }
    @Override
    public int getViewTypeCount() {

        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }
    @Override
    public int getCount() {

        return categoryItemsnew.size();
    }

    @Override
    public Object getItem(int position) {
        return categoryItemsnew.get(position);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {

            holder = new ViewHolder();

            convertView = mInflater.inflate(R.layout.category_adapter, null);

            holder.icon= (SimpleDraweeView)convertView.findViewById(R.id.icon);
            holder.layout= (RelativeLayout) convertView.findViewById(R.id.rl);

            holder.txt = (TextView) convertView.findViewById(R.id.title);
            holder.layout.setTag(position);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();

            ///		convertView.setTag(holder);
        }
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int b=Integer.parseInt(v.getTag().toString());
                MapShowingFragment.selected_category = categoryItemsnew.get(b).getTitle();
                MapShowingFragment.selected_categoryId = categoryItemsnew.get(b).getId();
                MapShowingFragment.category_text.setText(categoryItemsnew.get(b).getTitle());
                MapShowingFragment.cat_list.setVisibility(View.GONE);
                Toast.makeText(context,"clicked",Toast.LENGTH_LONG).show();
            }
        });
        holder.txt.setText(categoryItemsnew.get(position).getTitle().toString());
        if(categoryItemsnew.get(position).getTitle().toString().equals("Airline")) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_airline).build();
            holder.icon.setImageURI(imageRequest.getSourceUri());
        }else if(categoryItemsnew.get(position).getTitle().toString().equals("Shopping")) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_shopping).build();
            holder.icon.setImageURI(imageRequest.getSourceUri());
        }else if(categoryItemsnew.get(position).getTitle().toString().equals("Buisiness")) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_bussiness).build();
            holder.icon.setImageURI(imageRequest.getSourceUri());
        }else if(categoryItemsnew.get(position).getTitle().toString().equals("Real Estate")) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_realestate).build();
            holder.icon.setImageURI(imageRequest.getSourceUri());
        }else if(categoryItemsnew.get(position).getTitle().toString().equals("Money Exchange")) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_moneyexchange).build();
            holder.icon.setImageURI(imageRequest.getSourceUri());
        }else if(categoryItemsnew.get(position).getTitle().toString().equals("Car Rental")) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_rentcar).build();
            holder.icon.setImageURI(imageRequest.getSourceUri());
        }else if(categoryItemsnew.get(position).getTitle().toString().equals("Embasi $ Consulates")) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_embassy).build();
            holder.icon.setImageURI(imageRequest.getSourceUri());
        }else if(categoryItemsnew.get(position).getTitle().toString().equals("Tour $ Travel")) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_tour_travel).build();
            holder.icon.setImageURI(imageRequest.getSourceUri());
        }else if(categoryItemsnew.get(position).getTitle().toString().equals("Events")) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_events).build();
            holder.icon.setImageURI(imageRequest.getSourceUri());
        }else if(categoryItemsnew.get(position).getTitle().toString().equals("Eating Out")) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_eatingout).build();
            holder.icon.setImageURI(imageRequest.getSourceUri());
        }else if(categoryItemsnew.get(position).getTitle().toString().equals("Stay/Hotels")) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_stay_hotel).build();
            holder.icon.setImageURI(imageRequest.getSourceUri());
        }else if(categoryItemsnew.get(position).getTitle().toString().equals("Nightlife")) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_nightlife).build();
            holder.icon.setImageURI(imageRequest.getSourceUri());
        }

//		holder.txtheadto.setTextColor(Color.WHITE);
//		holder.txthead.setTextColor(Color.YELLOW);
//		holder.txtheadon.setTextColor(Color.WHITE);
        return convertView;
    }

    private static class ViewHolder {
        public RelativeLayout layout;

        public TextView txt;

        SimpleDraweeView icon;
        // String v;
    }


}

