package org.technomobs.travex.Activity.Fragment;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;

import org.technomobs.travex.R;

import java.util.ArrayList;

/**
 * Created by Abhi on 01-03-2016.
 */
public class MyPagerAdapter extends PagerAdapter {
    int size;
    Activity act;
    View layout;
    TextView pagenumber1, pagenumber2, pagenumber3, pagenumber4, pagenumber5;
    ImageView pageImage;
    ArrayList<String> images_list;
    Button click;

    public MyPagerAdapter(Activity mainActivity, ArrayList<String> images) {
        // TODO Auto-generated constructor stub
        images_list = images;
        act = mainActivity;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return images_list.size();
    }

    @Override
    public Object instantiateItem(View container, int position) {
        // TODO Auto-generated method stub
        //Fresco.initialize(act);
        LayoutInflater inflater = (LayoutInflater) act
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layout = inflater.inflate(R.layout.travelessential_adapter, null);
        SimpleDraweeView sdvImage= (SimpleDraweeView)layout.findViewById(R.id.sdvImage);


        Uri img_uri=Uri.parse(images_list.get(position));
        ImageRequest request = ImageRequest.fromUri(img_uri);
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setImageRequest(request)
                .setOldController(sdvImage.getController()).build();
        //Log.e(TAG, "ImagePath uri " + img_uri);

        sdvImage.setController(controller);


        ((ViewPager) container).addView(layout, 0);
        return layout;
    }
    @Override
    public void destroyItem(View arg0, int arg1, Object arg2) {
        ((ViewPager) arg0).removeView((View) arg2);
    }
    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == ((View) arg1);
    }
    @Override
    public Parcelable saveState() {
        return null;
    }

}
