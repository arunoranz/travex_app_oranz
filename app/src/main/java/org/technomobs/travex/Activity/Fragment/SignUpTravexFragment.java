package org.technomobs.travex.Activity.Fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import org.json.JSONObject;
import org.technomobs.travex.Activity.SignUpLoginMasterActivity;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Controller.Interface.GetRegistrationResponse;
import org.technomobs.travex.Controller.Interface.GetVerifyResponse;
import org.technomobs.travex.Controller.JsonObjectMaker;
import org.technomobs.travex.Controller.NetworkManager;
import org.technomobs.travex.Controller.NetworkOptions;
import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.Common;
import org.technomobs.travex.Utillity.ListCreatorForNameValuePairRequest;
import org.technomobs.travex.Utillity.LogUtility;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SignupTravexFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SignupTravexFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SignupTravexFragment extends Fragment implements View.OnKeyListener, View.OnClickListener, GetRegistrationResponse, GetVerifyResponse {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    static Activity activity;

    private OnFragmentInteractionListener mListener;
    public static final String TAG = SignupTravexFragment.class.getSimpleName();
    EditText mEditText_Name = null;
    EditText mEditText_Email = null;
    EditText mEditText_Password = null;
    EditText mEditText_confirm_password = null;
    TextView mButton_sign_up = null;
    static NetworkManager mNetworkManager = null;
    JsonObjectMaker mJsonObjectMaker = null;
    PreferenceManager mPreferenceManager = null;
    ListCreatorForNameValuePairRequest mListCreatorForNameValuePairRequest = null;
    static final String VERIFICATION_FAILED = "verification_failed";
    TravexApplication mTravexApplication = null;
    RelativeLayout rel_signup_background;

    /**
     * views for dialogue verify
     */
    EditText mEditText_verify_code = null;
    TextView mTextView_message = null;
    Button mButton_Done = null;
    Button mButton_Cancel = null;
    SignupTravexFragment.VerifyDialogue mVerifyDialogue = null;

    /**
     * view for dialogue on verification success
     */
    ImageView mImageView_Logo = null;
    SimpleDraweeView mImageView_Photo_upload = null;
    TextView mTextView_name_registered_user = null;
    TextView mTextView_email_registered_user = null;
    TextView mTextView_current_location = null;
    Button mButton_skip = null;
    Button mButton_update = null;
    View view;



    public SignupTravexFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
//     * @param param1 Parameter 1.
//     * @param param2 Parameter 2.
     * @return A new instance of fragment SignupTravexFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SignupTravexFragment newInstance(Activity act) {
        activity=act;
        SignupTravexFragment fragment = new SignupTravexFragment();
        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        LogUtility.Log(TAG, "onCreateView", null);

        view = inflater.inflate(R.layout.fragment_signup_travex, container, false);
        Common.setupUI(view, activity);
        mEditText_Name = (EditText) view.findViewById(R.id.name);
        mEditText_Email = (EditText) view.findViewById(R.id.email);
        mEditText_Password = (EditText) view.findViewById(R.id.password);
        mEditText_confirm_password = (EditText) view.findViewById(R.id.confirm_password);
        mButton_sign_up = (TextView) view.findViewById(R.id.signup_travex);
        rel_signup_background= (RelativeLayout) view.findViewById(R.id.rel_signup_background);
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(this);
        mButton_sign_up.setOnClickListener(this);
        mVerifyDialogue = new VerifyDialogue();
        clearAllFieldsSignUPTravex();
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        LogUtility.Log(TAG, "onKey:setOnKeyListener:onKey", null);
        if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
            LogUtility.Log(TAG, "onKey:setOnKeyListener:onKey:keycode back pressed", null);

            replaceWithParentFragment();
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.signup_travex) {
            String emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";



            LogUtility.Log(TAG, "onClick:signup_travex", null);
            if (!((mEditText_Name.getText().toString().trim().length() > 0) &&
                    (mEditText_Email.getText().toString().trim().length() > 0) &&
                    (mEditText_Password.getText().toString().trim().length() > 0) &&
                    (mEditText_confirm_password.getText().toString().trim().length() > 0))) {
                Toast.makeText(getContext(), "Please fill all the fields", Toast.LENGTH_LONG).show();
            }
            else if(!mEditText_Email.getText().toString().trim().matches(emailPattern)){
                Toast.makeText(getActivity(), "Invalid email address",
                        Toast.LENGTH_SHORT).show();
                mEditText_Email.setText("");
            }
            else  if (!(mEditText_Password.getText().toString().equals(mEditText_confirm_password.getText().toString()))) {
                Toast.makeText(getContext(),"Incorrect Password.Reenter password",Toast.LENGTH_LONG).show();
                //      mEditText_Password.setText("");
                mEditText_confirm_password.setText("");
            }
            else{
                rel_signup_background.setVisibility(View.VISIBLE);
                postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.REGISTRATION_URL,
                        getjsonObject(TravexApplication.REGISTER_TAG), TravexApplication.REQUEST_ID_REGISTRATION);
            }
//            if ((mEditText_Name.getText().toString().trim().length() > 0) &&
//                    (mEditText_Email.getText().toString().trim().length() > 0) &&
//                    (mEditText_Password.getText().toString().trim().length() > 0) &&
//                    (mEditText_confirm_password.getText().toString().trim().length() > 0)) {
//                LogUtility.Log(TAG, "onClick: all the fields are not empty", null);
//
//                if (mEditText_Password.getText().toString().equals(mEditText_confirm_password.getText().toString())) {
//                    LogUtility.Log(TAG, "onClick: all the fields are not empty and password and confirm password are equivallent", null);
//                    postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.REGISTRATION_URL,
//                            getjsonObject(TravexApplication.REGISTER_TAG), TravexApplication.REQUEST_ID_REGISTRATION);
//                }
//
//
//            } else {
//                mTravexApplication.showToast("Fields must be non empty");
//            }

        }
    }

    public void postJsonRequest(int request_type, String url, JSONObject jsonObject, final int request_id) {
        mNetworkManager.postJsonRequest(request_type, url, jsonObject, request_id);

    }

    public JSONObject getjsonObject(String tag) {
        if (tag.equals(TravexApplication.REGISTER_TAG)) {
            return getJson(mListCreatorForNameValuePairRequest.getListForRegistrationRequest(TravexApplication.FIRST_NAME_REGISTRATION, TravexApplication.EMAIL_REGISTRATION, TravexApplication.PASSWORD_REGISTRATION, TravexApplication.DEVICE_ID_REGISTRATION, TravexApplication.DEVICE_TYPE_REGISTRATION, TravexApplication.AUTH_METHOD_REGISTRATION),
                    mListCreatorForNameValuePairRequest.getListForRegistrationRequest(mEditText_Name.getText().toString(), mEditText_Email.getText().toString(), mEditText_Password.getText().toString(), TravexApplication.getInstance().getDEVICE_ID(), TravexApplication.DEVICE_TYPE, "app"));

        } else if (tag.equals(TravexApplication.VERIFY_REGISTRATION_TAG)) {

            if (mEditText_verify_code.getText().toString().trim().length() > 0) {
                return getJson(mListCreatorForNameValuePairRequest.getListForVerifyCodeRequest(TravexApplication.VERIFY_CODE_MEMBER_ID, TravexApplication.VERIFY_CODE_MEMBER_CODE), mListCreatorForNameValuePairRequest.getListForVerifyCodeRequest(mPreferenceManager.getRegisration_Id_response(), mEditText_verify_code.getText().toString()));

            }

        }

        return null;
    }

    public JSONObject getJson(ArrayList<String> namePair, ArrayList<String> valuePair) {
        return mJsonObjectMaker.getJson(namePair, valuePair);
    }


    @Override
    public void onRegistrationResponse(boolean status) {
        rel_signup_background.setVisibility(View.GONE);
        if (status) {
            LogUtility.Log(TAG, "onRegistrationResponse", "status:retrieved ok");

            mVerifyDialogue = new VerifyDialogue();
            mVerifyDialogue.showDialogueForVerify("ok");
            /*ok.setVisibility(View.VISIBLE);
            mEditText.setVisibility(View.VISIBLE);
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LogUtility.Log(TAG, "onRegistrationResponse:", "text in edittext:" + mEditText.getText().toString());
                    postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.VERIFY_URL, getjsonObject(TravexApplication.VERIFY_REGISTRATION_TAG), TravexApplication.REQUEST_ID_VERIFY_REGISTRATION);

                }
            });*/


        } else {
//            LogUtility.Log(TAG, "onRegistrationResponse", "status:retrieved not ok");
            Toast.makeText(getActivity(), "Email Already Exists", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onGetVerifyResponse(boolean status) {
        if (status) {
            LogUtility.Log(TAG, "onGetVerifyResponse:", "verification success");
            mVerifyDialogue = new VerifyDialogue();
            mVerifyDialogue.showDialogueForSuccessfulVerification();


        } else {
            LogUtility.Log(TAG, "onGetVerifyResponse:", "verification not success");
            mVerifyDialogue = new VerifyDialogue();
            mVerifyDialogue.showDialogueForVerify(VERIFICATION_FAILED);

        }

    }

    public class VerifyDialogue implements View.OnClickListener {
        public VerifyDialogue() {
            //builder = new AlertDialog.Builder(getActivity());
            mDialog = new Dialog(getActivity());
        }
        //AlertDialog.Builder builder;
        Dialog mDialog = null;
        String name_tag = null;
        String email_tag = null;
        String locality_tag = null;

        /**
         * To Show the dialogue For entering verify code first time or after failure
         *
         * @param status To check whether the dialogue for entering code first time or after first chance
         */
        public void showDialogueForVerify(String status) {
            mDialog.setContentView(R.layout.dialogue_verification_first_time_layout);
            mDialog.setTitle("Travex");
            mEditText_verify_code = (EditText) mDialog.findViewById(R.id.verify_code);
            mButton_Done = (Button) mDialog.findViewById(R.id.done);
            mButton_Cancel = (Button) mDialog.findViewById(R.id.cancel);
            mTextView_message = (TextView) mDialog.findViewById(R.id.travex_sub_title);

            if (status.equals(VERIFICATION_FAILED)) {
                mTextView_message.setText("verification failed!!please try again\n");
            }
            mButton_Done.setOnClickListener(this);
            mButton_Cancel.setOnClickListener(this);
            mDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {

                    if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                        LogUtility.Log(TAG, "VerifyDialogue:onKey:setOnKeyListener:onKey:keycode back pressed", null);
                        mDialog.dismiss();
                        clearAllFieldsSignUPTravex();

                        replaceWithParentFragment();
                        return true;
                    }


                    return false;
                }
            });

            mDialog.show();


        }

        /**
         * To show the dialogue on verification success
         */
        public void showDialogueForSuccessfulVerification() {
//            mDialog.setContentView(R.layout.dialogue_verification_successful_layout);
            mDialog.setContentView(R.layout.after_signupsecond);

//            mImageView_Logo = (ImageView) mDialog.findViewById(R.id.travex_logo);
            mImageView_Photo_upload = (SimpleDraweeView) mDialog.findViewById(R.id.img_user);
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.user).build();
            RoundingParams roundingParams = RoundingParams.fromCornersRadius(5f);
            roundingParams.setBorder(Color.WHITE, (float) 1.0);
            roundingParams.setRoundAsCircle(true);
            mImageView_Photo_upload.getHierarchy().setRoundingParams(roundingParams);
            mImageView_Photo_upload.setImageURI(imageRequest.getSourceUri());

            mTextView_name_registered_user = (TextView) mDialog.findViewById(R.id.tv_name_user);
            mTextView_email_registered_user = (TextView) mDialog.findViewById(R.id.tv_email);
            mTextView_current_location = (TextView) mDialog.findViewById(R.id.tv_location);
            mButton_skip = (Button) mDialog.findViewById(R.id.btn_skip);
            mButton_update = (Button) mDialog.findViewById(R.id.btn_update);
            name_tag = mTextView_name_registered_user.getText().toString();
            email_tag = mTextView_email_registered_user.getText().toString();
            locality_tag = mTextView_current_location.getText().toString();
            mTextView_name_registered_user.setText(name_tag + mPreferenceManager.getRegisteredUser_name());
            mTextView_email_registered_user.setText(email_tag + mPreferenceManager.getRegisteredUser_email());
            mTextView_current_location.setText(locality_tag + mPreferenceManager.getSelectedCountryName());
            mDialog.show();


            mButton_skip.setOnClickListener(this);
            mButton_update.setOnClickListener(this);



            mDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                        LogUtility.Log(TAG, "VerifyDialogue:onKey:setOnKeyListener:onKey:keycode back pressed", null);
                        mDialog.dismiss();
                        clearAllFieldsSignUPTravex();
                        replaceWithParentFragment();
                        return true;
                    }
                    return false;
                }
            });


        }

        public void dismissDialogue() {
            if (mDialog != null)
                mDialog.dismiss();

        }


        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.done) {
                if (mEditText_verify_code.getText().toString().trim().length() > 0) {
                    LogUtility.Log(TAG, "VerifyDialogue::onClick:done", null);
                    dismissDialogue();
                    postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.VERIFY_URL, getjsonObject(TravexApplication.VERIFY_REGISTRATION_TAG), TravexApplication.REQUEST_ID_VERIFY_REGISTRATION);

                } else {
                    mEditText_verify_code.requestFocus();
                    //Toast.makeText(getActivity(), "enter the verify code", Toast.LENGTH_SHORT).show();
                    mTravexApplication.showToast("Enter the verification code");
                }


            } else if (v.getId() == R.id.cancel) {
                LogUtility.Log(TAG, "VerifyDialogue::onClick:cancel", null);
                dismissDialogue();
                clearAllFieldsSignUPTravex();
                replaceWithParentFragment();


            } else if (v.getId() == R.id.btn_skip) {
                mPreferenceManager.setboolean("logout",true);
                LogUtility.Log(TAG, "VerifyDialogue successful ::onClick:skip", null);

                dismissDialogue();
                clearAllFieldsSignUPTravex();
                if (mPreferenceManager.getCurrentLocalityName() != null) {
                    if (mPreferenceManager.getCurrentLocInCityListIsAvailable()) {

                        LogUtility.Log(TAG, "VerifyDialogue:successful", "current city in server");
                        ((SignUpLoginMasterActivity) getActivity()).startProfileActivity();


                    } else {

                        LogUtility.Log(TAG, "VerifyDialogue: successful", "current city not  in server:load visit city fragment");
                        ((SignUpLoginMasterActivity) getActivity()).replace_with_VisitCityFragment();


                    }


                } else {
                    LogUtility.Log(TAG, "VerifyDialogue:successful", "current city not tracked");


                    ((SignUpLoginMasterActivity) getActivity()).replace_with_VisitCityFragment();

                }


            } else if (v.getId() == R.id.btn_update) {
                mPreferenceManager.setboolean("logout",true);
                LogUtility.Log(TAG, "VerifyDialogue successful ::onClick:update", null);
                dismissDialogue();
                clearAllFieldsSignUPTravex();

//                ((SignUpLoginMasterActivity) getActivity()).replace_with_UpdateDetails_MemberFragment();
                ((SignUpLoginMasterActivity) getActivity()).startProfileActivity_with_OwnFragment(mTravexApplication.My_PROFILE_FRAGMENT_TAG);
            }


        }


    }

    public void replaceWithParentFragment() {
        ((SignUpLoginMasterActivity) getActivity()).replace_with_masterFragment();

    }

    public void clearAllFieldsSignUPTravex() {
        mEditText_Name.setText("");
        mEditText_Email.setText("");
        mEditText_Password.setText("");
        mEditText_confirm_password.setText("");
    }


    @Override
    public void onAttach(Context context) {
        LogUtility.Log(TAG, "onAttach", null);
        mJsonObjectMaker = new JsonObjectMaker(getActivity());
        mNetworkManager = NetworkManager.getSingleInstance(getActivity());
        mPreferenceManager = new PreferenceManager(getActivity());
        mListCreatorForNameValuePairRequest = new ListCreatorForNameValuePairRequest(getActivity());
        mTravexApplication = TravexApplication.getInstance();
        mNetworkManager.setOnRegistrationResponseListener(this);
        mNetworkManager.setOnVerifyResponseListener(this);
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}
