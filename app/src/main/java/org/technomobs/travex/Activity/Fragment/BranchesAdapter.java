package org.technomobs.travex.Activity.Fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import org.technomobs.travex.R;

import java.util.ArrayList;

/**
 * Created by Abhi on 09-03-2016.
 */
public class BranchesAdapter  extends RecyclerView.Adapter<BranchesAdapter.ViewHolder> {


    LayoutInflater mInflater;
    String selected_category,selected_categoryid;
    ArrayList<CategoryListItems> categoryItemsnew;
    private String apptheme,category_name;
    ArrayList<String> branch_id_list;
    ArrayList<String> branch_name_list;
    ArrayList<String> branch_address_list;

    int count = 0;
    Activity act;
    ViewHolder vh;
    public BranchesAdapter(Activity activity,ArrayList<String> branch_id,ArrayList<String> branch_name
            ,ArrayList<String> branch_address) {
        branch_id_list = branch_id;
        branch_name_list = branch_name;
        branch_address_list = branch_address;
        this.act=activity;

        mInflater = (LayoutInflater) act
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.branches_adapter, parent, false);

        vh = new ViewHolder(v);
        return vh;
    }


    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {
        // return bookmarkPostId.size();
        return branch_id_list.size();
    }


    public Object getItem(int position) {
        return categoryItemsnew.get(position);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        {
            holder.branchname_text.setText(branch_name_list.get(position));
            holder.branchname_address.setText(branch_address_list.get(position));

        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView branchname_text,branchname_address;

        public ViewHolder(View v) {
            super(v);
            branchname_text=(TextView)v.findViewById(R.id.branchname_text);
            branchname_address=(TextView)v.findViewById(R.id.branchname_address);

        }
    }

}
