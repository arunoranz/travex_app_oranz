package org.technomobs.travex.Activity.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import org.technomobs.travex.R;

import java.util.ArrayList;

/**
 * Created by Abhi on 14-03-2016.
 */
public class CategoryListAdapternew  extends RecyclerView.Adapter<CategoryListAdapternew.ViewHolder> {
    private static final int VIEW_TYPE_HEADER = 0;
    private static final int VIEW_TYPE_FOOTER = 1;
    private static final int VIEW_TYPE_DEFAULT = 2;
    private static final int VIEW_TYPE_COUNT = 3;
    private Context context;
    LayoutInflater mInflater;
    String selected_category,selected_categoryid;
    ArrayList<CategoryListItems> categoryItemsnew;
    TypedArray categoryItemIcons;
    private String apptheme,category_name;
    public int checki = 0;

    int count = 0;
    Activity act;
    ViewHolder vh;
    public CategoryListAdapternew(Context context, ArrayList<CategoryListItems> categoryItemsnew, TypedArray categoryItemIcons
    ) {
        this.context = context;

        this.categoryItemsnew = categoryItemsnew;
        this.categoryItemIcons= categoryItemIcons;

        mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_adapter, parent, false);

        vh = new ViewHolder(v);
        return vh;
    }


    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {
        // return bookmarkPostId.size();
        return categoryItemsnew.size();
    }


    public Object getItem(int position) {
        return categoryItemsnew.get(position);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        {





            for(int i=0;i<categoryItemsnew.size();i++)
            {
                holder.layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int b = Integer.parseInt(v.getTag().toString());
                        MapShowingFragment.selected_category = categoryItemsnew.get(b).getTitle();
                        MapShowingFragment.selected_categoryId = categoryItemsnew.get(b).getId();
                        MapShowingFragment.category_text.setText(categoryItemsnew.get(b).getTitle());
                        MapShowingFragment.cat_list.setVisibility(View.GONE);
                        Toast.makeText(context, "clicked", Toast.LENGTH_LONG).show();
                    }
                });
                holder.layout.setTag(position);
                holder.txt.setText(categoryItemsnew.get(position).getTitle().toString());
                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(categoryItemIcons.getResourceId(position, -1)).build();
                holder.icon.setImageURI(imageRequest.getSourceUri());
            }

//            if(categoryItemsnew.get(position).getTitle().toString().equals("Airline")) {
//                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_airline).build();
//                holder.icon.setImageURI(imageRequest.getSourceUri());
//            }else if(categoryItemsnew.get(position).getTitle().toString().equals("Shopping")) {
//                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_shopping).build();
//                holder.icon.setImageURI(imageRequest.getSourceUri());
//            }else if(categoryItemsnew.get(position).getTitle().toString().equals("Buisiness")) {
//                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_bussiness).build();
//                holder.icon.setImageURI(imageRequest.getSourceUri());
//            }else if(categoryItemsnew.get(position).getTitle().toString().equals("Real Estate")) {
//                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_realestate).build();
//                holder.icon.setImageURI(imageRequest.getSourceUri());
//            }else if(categoryItemsnew.get(position).getTitle().toString().equals("Money Exchange")) {
//                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_moneyexchange).build();
//                holder.icon.setImageURI(imageRequest.getSourceUri());
//            }else if(categoryItemsnew.get(position).getTitle().toString().equals("Car Rental")) {
//                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_rentcar).build();
//                holder.icon.setImageURI(imageRequest.getSourceUri());
//            }else if(categoryItemsnew.get(position).getTitle().toString().equals("Embasi $ Consulates")) {
//                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_embassy).build();
//                holder.icon.setImageURI(imageRequest.getSourceUri());
//            }else if(categoryItemsnew.get(position).getTitle().toString().equals("Tour $ Travel")) {
//                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_tour_travel).build();
//                holder.icon.setImageURI(imageRequest.getSourceUri());
//            }else if(categoryItemsnew.get(position).getTitle().toString().equals("Events")) {
//                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_events).build();
//                holder.icon.setImageURI(imageRequest.getSourceUri());
//            }else if(categoryItemsnew.get(position).getTitle().toString().equals("Eating Out")) {
//                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_eatingout).build();
//                holder.icon.setImageURI(imageRequest.getSourceUri());
//            }else if(categoryItemsnew.get(position).getTitle().toString().equals("Stay/Hotels")) {
//                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_stay_hotel).build();
//                holder.icon.setImageURI(imageRequest.getSourceUri());
//            }else if(categoryItemsnew.get(position).getTitle().toString().equals("Nightlife")) {
//                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_nightlife).build();
//                holder.icon.setImageURI(imageRequest.getSourceUri());
//            }
//		holder.txtheadto.setTextColor(Color.WHITE);
//		holder.txthead.setTextColor(Color.YELLOW);
//		holder.txtheadon.setTextColor(Color.WHITE);

        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout layout;

        public TextView txt;



        public TextView camera_count,File_count,country_name,selected_name;
        SimpleDraweeView icon;
        public ViewHolder(View v) {
            super(v);
       icon= (SimpleDraweeView)v.findViewById(R.id.icon);
          layout= (RelativeLayout) v.findViewById(R.id.rl);

      txt = (TextView) v.findViewById(R.id.title);
       // layout.setTag(position);


        }
    }

}

