package org.technomobs.travex.Activity.Fragment;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.technomobs.travex.R;

import java.util.ArrayList;

/**
 * Created by ARUN on 04/02/2016.
 */
public class ArticleDetailsTagAdapter extends BaseAdapter {

    ArrayList<String> title_list,imageURL;
    Activity activity;
    private static LayoutInflater inflater=null;
    public ArticleDetailsTagAdapter(Activity act , ArrayList<String> title, ArrayList<String> image) {
        // TODO Auto-generated constructor stub

        title_list=title;
        activity=act;
        imageURL=image;
        inflater = ( LayoutInflater )activity.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return title_list.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        TextView tv2;
        ImageView img;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder=new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.articledetailstagsadapter_layout, null);
        //holder.tv=(TextView) rowView.findViewById(R.id.textView1);
        holder.tv2=(TextView) rowView.findViewById(R.id.textView2);
        holder.img=(ImageView) rowView.findViewById(R.id.imageView1);

        //holder.tv.setText(result.get(position));
        holder.tv2.setText(title_list.get(position));
        Uri imageUri = Uri.parse(imageURL.get(position));
        holder.img.setImageURI(imageUri);

        rowView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
            }
        });

        return rowView;
    }
}
