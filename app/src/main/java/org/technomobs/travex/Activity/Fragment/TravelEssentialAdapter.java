package org.technomobs.travex.Activity.Fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.net.Uri;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.facebook.cache.common.SimpleCacheKey;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import org.technomobs.travex.R;

import java.util.ArrayList;

/**
 * Created by ARUN on 03/19/2016.
 */
public class TravelEssentialAdapter extends RecyclerView.Adapter<TravelEssentialAdapter.ViewHolder>{

    static Activity activity ;
    ArrayList<String> tv_title_list, tv_desc_list,image_list,id_list;
    Fragment fragment;

    Travelsessential_fragment_second mtravexesssentialfragsecond;
    public TravelEssentialAdapter(Activity act, ArrayList<String> tv_title, ArrayList<String> tv_desc, ArrayList<String> Essential_Images_URL, ArrayList<String> idlist) {
        activity = act;
        tv_title_list=tv_title;
        tv_desc_list = tv_desc;
        id_list = idlist;
        image_list = Essential_Images_URL;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.travelessentialadapter_fragment, null);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            holder.tv_title.setText(tv_title_list.get(position));
            holder.tv_desc.setText(tv_desc_list.get(position));
            holder.root.setTag(position);
            if (!image_list.get(position).equals("")) {
                Uri img_uri = Uri.parse(image_list.get(position));
                Fresco.getImagePipeline().evictFromMemoryCache(img_uri);
                Fresco.getImagePipelineFactory().getMainDiskStorageCache().remove(new SimpleCacheKey(img_uri.toString()));
                Fresco.getImagePipelineFactory().getSmallImageDiskStorageCache().remove(new SimpleCacheKey(img_uri.toString()));
                ImageRequest request = ImageRequest.fromUri(img_uri);
                DraweeController controller = Fresco.newDraweeControllerBuilder().setImageRequest(request)
                        .setOldController(holder.iv_image.getController()).build();
                holder.iv_image.setController(controller);
            }
        }
        catch(Exception ex)
        {

        }
    }

    @Override
    public int getItemCount() {
        return image_list.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public final TextView tv_title, tv_desc;
        public final SimpleDraweeView iv_image;
        public final RelativeLayout root;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tv_title= (TextView) itemLayoutView.findViewById(R.id.tv_title);
            tv_desc = (TextView) itemLayoutView.findViewById(R.id.tv_desc);
            iv_image = (SimpleDraweeView) itemLayoutView.findViewById(R.id.image_list);
            root = (RelativeLayout) itemLayoutView.findViewById(R.id.root_lay);
            root.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            int pos=Integer.parseInt(root.getTag().toString());
            switch (v.getId()){
                case R.id.root_lay:
//                    Fragment fragment=Travelsessential_fragment_second.newinstance(tv_desc_list.get(pos),image_list.get(pos),id_list.get(pos),pos);
                    Fragment fragment=Travelsessential_fragment_second.newinstance(activity,id_list.get(pos));
                    FragmentManager fragmentManager = activity.getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_container, fragment);
                    fragmentTransaction.addToBackStack(fragment.getClass().getName());
                    fragmentTransaction.commit();
                    break;
                default:
                    break;
            }
        }
    }
}
