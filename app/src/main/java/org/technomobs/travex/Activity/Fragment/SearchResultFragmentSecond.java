package org.technomobs.travex.Activity.Fragment;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.common.util.UriUtil;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.viewpagerindicator.CirclePageIndicator;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.technomobs.travex.Activity.AfterloginKnowncity;
import org.technomobs.travex.Activity.SignUpLoginMasterActivity;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Controller.Interface.AddPlanResponse;
import org.technomobs.travex.Controller.Interface.GetSearchItemResponse;
import org.technomobs.travex.Controller.Interface.GetWriteReviewResponse;
import org.technomobs.travex.Controller.JsonObjectMaker;
import org.technomobs.travex.Controller.NetworkManager;
import org.technomobs.travex.Controller.NetworkOptions;
import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.Common;
import org.technomobs.travex.Utillity.DateFormatFunction;
import org.technomobs.travex.Utillity.ListCreatorForNameValuePairRequest;
import org.technomobs.travex.Utillity.LogUtility;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Abhi on 10-03-2016.
 */
public class SearchResultFragmentSecond extends Fragment implements GetSearchItemResponse,Animation.AnimationListener,View.OnClickListener,GetWriteReviewResponse,AddPlanResponse {
    static Activity activity;

    static ScrollView main_scroll;
    ReviewAndRatingAdapter adapter;
    MenuAdapter menuadapter;
    FragmentManager fragmentManager;
    AddToMyplanFragment fragment;
    PreferenceManager mPreferenceManager = null;
    FreebiesImageAdapter freebiesadapter;
    AminitiesImageAdapter aminitiesadapter;
    CategoryImageAdapter categoryImageAdapter;
    RatingBar ratingBar,ratingBarreview;
    String tagItem_name;
    SimpleDraweeView review_Image;
    RelativeLayout review_headlayout,animation_layout;
    Animation rotateOne, rotateTwo, rotateThree;
    LinearLayout add_layout,branch_layout;
    LinearLayout review_enter_layout;
    LinearLayout lin_lay_review_and_rating;
    ImageView bt_close;
    RecyclerView recycler_view_categories, mRecyclerView,planRecyclerView,recycler_view_branches,recycler_view_menu,recycler_view_freebies,recycler_view_aminities;
    TextView tv_noreviews,review_username,bt_submit,add_review,tv_rating_count,tv_timeperiod,tv_venue,tv_price,tv_tag_head,freebies_head,amminities_head,tv_addtomyplan,address_text,menu_text,address_head,from_to,description_text,description_head;
    EditText et_review;
    LinearLayout time_layout,venue_layout,featured_article_layout,events_layout,tag_layout,tagdetails_layout,featured_articledetails_layout,eventsdetails_layout,
            tips_layout,tipsdetails_layout,service_details_layout,service_layout,cuisinesdetails_newlayout,cuisines_layout,
            agentsdetails_layout,agents_layout,policiesdetails_layout,policies_layout,specialitiesdetails_layout,specialities_layout,description_layout,
            categoriesdetails_layout,categories_layout,layout_scroll;
//    ScrollView child_scroll;
    ScrollView child_scroll_name;
    DatePickerDialog dpd;
    TimePickerDialog tpd;
    AddPlanAdapter planadapter;
    BranchesAdapter branchesAdapter;
    LinearLayout lin_lay_call,lin_lay_location,lin_lay_review,lin_lay_gps,lin_lay_site;

    ListCreatorForNameValuePairRequest mListCreatorForNameValuePairRequest = null;
    static NetworkManager mNetworkManager = null;
    JsonObjectMaker mJsonObjectMaker = null;
    DateFormatFunction df = new DateFormatFunction();
    ArrayList<String> tv_title_list = new ArrayList<String>();
    ArrayList<String> ratingtext = new ArrayList<String>();
    ArrayList<String> tv_desp = new ArrayList<String>();
    ArrayList<String> rating_count = new ArrayList<String>();
    ArrayList<String> tv_subtitle_list = new ArrayList<String>();
    ArrayList<String> feature_article_list = new ArrayList<String>();
    ArrayList<String> events_list = new ArrayList<String>();
    ArrayList<String> service_details_list = new ArrayList<String>();
    ArrayList<ArrayList> tips_MainArray = new ArrayList<ArrayList>();

    ArrayList<String> phone_list = new ArrayList<String>();
    ArrayList<String> tagItem_list = new ArrayList<String>();
    ArrayList<String> tagItem_rental_list = new ArrayList<String>();
    ArrayList<String> imgItem_list = new ArrayList<String>();

    ArrayList<String> time_duration_list = new ArrayList<String>();
    ArrayList<String> time_departure_list = new ArrayList<String>();
    ArrayList<String> time_day_list = new ArrayList<String>();
    ArrayList<String> videoItem_list = new ArrayList<String>();
    ArrayList<String> branchesItem_list = new ArrayList<String>();
    ArrayList<String> branchesItemid_list = new ArrayList<String>();
    ArrayList<String> branchesItemaddress_list = new ArrayList<String>();
    ArrayList<String> cuisinesItem_list = new ArrayList<String>();
    ArrayList<String> tipid_list = new ArrayList<String>();
    ArrayList<String> menu_imgItem_list = new ArrayList<String>();
    ArrayList<String> tiptitle_list = new ArrayList<String>();
    ArrayList<String> cuisines_list = new ArrayList<String>();
    ArrayList<String> agents_list = new ArrayList<String>();
    ArrayList<String> specialities_list = new ArrayList<String>();
    ArrayList<String> policies_list = new ArrayList<String>();
    ArrayList<String> categoryItem_list = new ArrayList<String>();
    ArrayList<String> categoryItem_list_name = new ArrayList<String>();
    ArrayList<String> freebies_img_list = new ArrayList<String>();
    ArrayList<String> freebies_list = new ArrayList<String>();
    ArrayList<String> aminities_img_list = new ArrayList<String>();
    ArrayList<String> aminities_list = new ArrayList<String>();
    ArrayList<String> menu_nameList = new ArrayList<String>();

    ArrayList<String> review_membernameList = new ArrayList<String>();
    ArrayList<String> review_cityList = new ArrayList<String>();
    ArrayList<String> review_iconList = new ArrayList<String>();
    ArrayList<String> review_dateList = new ArrayList<String>();
    ArrayList<String> review_itemnameList = new ArrayList<String>();
    ArrayList<String> review_memberimageList = new ArrayList<String>();
    ArrayList<String> review_useridList = new ArrayList<String>();
    ArrayList<String> review_ratingList = new ArrayList<String>();
    ArrayList<String> review_commentList = new ArrayList<String>();
    private Calendar _calendar;
    private int month, year;
    View view;
    private ImageView prevMonth,nextMonth;
    String TAG,rating_total="0";
    TextView branches_head,selected_name,country_name,video_count,image_count,rating_total_text;
    String  price_Text,end_date,start_date,eating_type,eating_halal,eating_typeicon,video_size,image_size,state,currency,city,city_id,id,airport,area,terminal="",description="",name,longitude,latitude,district,website,destination="",country,country_short,
    address="",Time="",date_month_year;
    static String selected_id;

    ViewPager vpPager;
    CirclePageIndicator indicator;

    //SimpleDraweeView sdv_detailsimage;
    private static final String dateTemplate = "MMMM yyyy";
    public static SearchResultFragmentSecond newInstance(Activity act,String select_id) {
        SearchResultFragmentSecond fragment = new SearchResultFragmentSecond();
        activity = act;
        selected_id=select_id;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


         view = inflater.inflate(R.layout.after_searchfragment_second, container, false);
        Common.setupUI(view,activity);
        initialisation();
        animation_layout=(RelativeLayout)view.findViewById(R.id.settings_animation);

        animationInit();
        animationLoading();

        // Get changed text s here and use it
        Runnable run = new Runnable() {
            @Override
            public void run() {
//                        for (int j = 0; j < searchListItems.size(); j++) {
//                            if (!((searchListItems.get(j).getTitle().toString()).toLowerCase()).contains(Search_text)) {
//                                searchListItems.remove(j);
//
//                            } else {
//                                search_list.setVisibility(View.VISIBLE);
//                            }
//                        }
                //query = Search_text.toString().trim();
                postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_RESULTS_SEARCHITEM_SPECIFIC_URL,
                        getjsonObject(TravexApplication.GET_SEARCHITEM_TAG), TravexApplication.REQUEST_ID_GET_SEARCH_ITEM);


            }
        };
        Handler handler = new Handler();
        handler.postDelayed(run, 2000);

        main_scroll.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View p_v, MotionEvent p_event) {
//                child_scroll.getParent().requestDisallowInterceptTouchEvent(false);
                child_scroll_name.getParent().requestDisallowInterceptTouchEvent(false);
                //       loadDetails();
                // We will have to follow above for all scrollable contents
                return false;
            }
        });
 /*postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.WRITE_REVIEW_URL,
                    getjsonObject(TravexApplication.WRITE_REVIEW_TAG),
                    TravexApplication.REQUEST_ID_WRITE_REVIEW);*/
        add_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

             //  displayAlertDialog();
                if(mPreferenceManager.getboolean("logout")==null) {
                    Toast.makeText(activity,"You must login to write a review !",Toast.LENGTH_LONG).show();
//                    Intent logouintent = new Intent(activity,SignUpLoginMasterActivity.class);
//                    startActivity(logouintent);
                }
                else if(mPreferenceManager.getboolean("logout")==false){
                    Toast.makeText(activity,"You must login to write a review !",Toast.LENGTH_LONG).show();
//                    Intent logouintent = new Intent(activity,SignUpLoginMasterActivity.class);
//                    startActivity(logouintent);
                }else {
                    review_enter_layout.setVisibility(View.VISIBLE);
                    et_review.setFocusable(true);
                }
            }
        });

        bt_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                review_enter_layout.setVisibility(View.GONE);
                et_review.setText("");
//                et_review.setFocusable(true);
            }
        });
        bt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ratingBarreview.getRating()<=0) {
                    Toast.makeText(activity,"Please rate...",Toast.LENGTH_SHORT).show();
                }
                else if(et_review.getText().toString().equals("")) {
                    Toast.makeText(activity,"Write a review",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    review_enter_layout.setVisibility(View.GONE);
                    et_review.setText("");
//                et_review.setFocusable(true);
                    postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.WRITE_REVIEW_URL,
                            getjsonObject(TravexApplication.WRITE_REVIEW_TAG),
                            TravexApplication.REQUEST_ID_WRITE_REVIEW);
                }

            }
        });
        mRecyclerView.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View p_v, MotionEvent p_event) {
                // this will disallow the touch request for parent scroll on
                // touch of child view
                p_v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
//        child_scroll.setOnTouchListener(new View.OnTouchListener() {
//            public boolean onTouch(View p_v, MotionEvent p_event) {
//                // this will disallow the touch request for parent scroll on
//                // touch of child view
//                p_v.getParent().requestDisallowInterceptTouchEvent(true);
//                return false;
//            }
//        });
        child_scroll_name.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View p_v, MotionEvent p_event) {
                // this will disallow the touch request for parent scroll on
                // touch of child view
                p_v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });



        tv_addtomyplan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                fragment =AddToMyplanFragment.newInstance(activity.getApplicationContext(),id,name);
//                fragmentManager = getFragmentManager();
//                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                fragmentTransaction.replace(R.id.frame_container, fragment);
//                fragmentTransaction.addToBackStack(fragment.getClass().getName());
//                fragmentTransaction.commit();
//                DatePickerDialog dd = new DatePickerDialog();
                try {

                    if(mPreferenceManager.getboolean("logout")==null) {
//                        Toast.makeText(activity,"You must login to add plan!",Toast.LENGTH_LONG).show();
                        Intent logouintent = new Intent(activity,SignUpLoginMasterActivity.class);
                        startActivity(logouintent);
                    }
                    else if(mPreferenceManager.getboolean("logout")==false){
//                        Toast.makeText(activity,"You must login to add plan !",Toast.LENGTH_LONG).show();
                        Intent logouintent = new Intent(activity,SignUpLoginMasterActivity.class);
                        startActivity(logouintent);
                    }else {
                        Calendar now = Calendar.getInstance();
                        dpd = DatePickerDialog.newInstance(
                                Date_callback,
                                now.get(Calendar.YEAR),
                                now.get(Calendar.MONTH),
                                now.get(Calendar.DAY_OF_MONTH)
                        );
                        dpd.setAccentColor(getResources().getColor(R.color.theme));
                        dpd.setMinDate(now);
                        dpd.show(getFragmentManager(), "Datepickerdialog");
                    }



                }
                catch(Exception ex)
                {
                    Log.println(0,"DateTimePicker",ex.toString());
                }
            }
        });


        return view;
    }


    private DatePickerDialog.OnDateSetListener Date_callback = new DatePickerDialog.OnDateSetListener() {


        @Override
        public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
            try {
                int month=monthOfYear+1;
                date_month_year = year + "-" + month + "-" + dayOfMonth;
                Calendar now = Calendar.getInstance();
                tpd = TimePickerDialog.newInstance(
                        Time_callback,
                        now.get(Calendar.HOUR_OF_DAY),
                        now.get(Calendar.MINUTE),
                        false
                );
                tpd.setAccentColor(getResources().getColor(R.color.theme));
                tpd.show(getFragmentManager(), "Timepickerdialog");

            }
            catch(Exception ex)
            {
                Log.println(0,"Timepickerdialog",ex.toString());
            }
        }
    };


    private TimePickerDialog.OnTimeSetListener Time_callback = new TimePickerDialog.OnTimeSetListener() {

        @Override
        public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
            try {
                String fff = String.valueOf(hourOfDay+minute+second);
                String _Time = hourOfDay+":"+minute;
                Time=df.dateformat("HH:mm","hh:mm a",_Time);

                postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.ADD_PLAN_URL,
                        getjsonObject(TravexApplication.ADD_PLAN_TAG),
                        TravexApplication.REQUEST_ID_ADD_PLAN);

            }
            catch (Exception ex)
            {

            }

        }
    };

    View.OnClickListener Onclk=new View.OnClickListener() {


        @Override
        public void onClick(View v)
        {
            if (v == prevMonth)
            {
                if (month <= 1)
                {
                    month = 12;
                    year--;
                }
                else
                {
                    month--;
                }
                //	Log.d(tag, "Setting Prev Month in GridCellAdapter: " + "Month: " + month + " Year: " + year);
//                setGridCellAdapterToDate(month, year);
            }
            else if (v == nextMonth)
            {
                if (month > 11)
                {
                    month = 1;
                    year++;
                }
                else
                {
                    month++;
                }
                //	Log.d(tag, "Setting Next Month in GridCellAdapter: " + "Month: " + month + " Year: " + year);
//                setGridCellAdapterToDate(month, year);
            }


        }
    };
//    public class RecyclerViewDisabler implements RecyclerView.OnItemTouchListener {
//
//        @Override
//        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
//            return true;
//        }
//
//        @Override
//        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
//
//        }
//
//        @Override
//        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
//
//        }
//    }
public void postJsonRequest(int request_type, String url, JSONObject jsonObject, final int request_id) {

    mNetworkManager.postJsonRequest(request_type, url, jsonObject, request_id);
}
    public void postJsonRequest(int request_type, final Map<String, String> request_header, String url, JSONObject jsonObject, final int request_id) {

        mNetworkManager.postJsonRequest(request_type, request_header, url, jsonObject, request_id);


    }
    public JSONObject getjsonObject(String tag) {
        if (tag.equals(TravexApplication.GET_SEARCHITEM_TAG)) {

            return getJson(mListCreatorForNameValuePairRequest
                            .getListOfSearchItemSpecificApi(TravexApplication.GET_SEARCH_ITEM_CATEGORY, TravexApplication.GET_SEARCH_ITEM_ID),
                    mListCreatorForNameValuePairRequest.getListOfSearchItemSpecificApi(MapShowingFragment.selected_category, selected_id));


        }
        else if (tag.equals(TravexApplication.WRITE_REVIEW_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfWriteReview
                            (TravexApplication.WRITE_REVIEW_USER_ID,
                                    TravexApplication.WRITE_REVIEW_CATEGORY,
                                    TravexApplication.WRITE_REVIEW_ITEM_ID,
                                    TravexApplication.WRITE_REVIEW_ITEM_NAME,
                                    TravexApplication.WRITE_REVIEW_COMMENT,
                                    TravexApplication.WRITE_REVIEW_RATING

                            ),
                    mListCreatorForNameValuePairRequest.getListOfWriteReview(mPreferenceManager.getRegisration_Id_response(), MapShowingFragment.selected_categoryId, id, name,et_review.getText().toString(), String.valueOf(ratingBarreview.getRating()))

            );

        }
        else if (tag.equals(TravexApplication.ADD_PLAN_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfAddplan
                            (TravexApplication.ADD_PLAN_ITEM_ID,
                                    TravexApplication.ADD_PLAN_ITEM_NAME,
                                    TravexApplication.ADD_PLAN_TIME,
                                    TravexApplication.ADD_PLAN_USER_ID,
                                    TravexApplication.ADD_PLAN_DATE,
                                    TravexApplication.ADD_PLAN_CATEGORY
                            ),
                    mListCreatorForNameValuePairRequest.getListOfAddplan(id,name, Time, mPreferenceManager.getRegisration_Id_response(),date_month_year.trim(), MapShowingFragment.selected_category)

            );
        }
        return null;
        }
    public JSONObject getJson(ArrayList<String> namePair, ArrayList<String> valuePair) {
        return mJsonObjectMaker.getJson(namePair, valuePair);
    }
    @Override
    public void onGetSearchItemResponse(boolean status, JSONObject jsonObject) {
        if (status) {
            try {
               // if(MapShowingFragment.selected_category.equals("Airline")){
                    JSONObject result = jsonObject.getJSONObject("result");
                    try {
                        state = result.getString("state");
                    }catch(Exception estate){
                        estate.printStackTrace();
                        }
                    try {
                        currency = result.getString("currency");
                    }catch(Exception ecurrency){
                        ecurrency.printStackTrace();
                    }
                    try{
                    city = result.getString("city");
                    }catch(Exception ecity){
                        ecity.printStackTrace();
                    }
                    try{
                    city_id = result.getString("city_id");
                    }catch(Exception ecityid){
                        ecityid.printStackTrace();
                    }
                    try{
                    id = result.getString("id");
                    }catch(Exception eid){
                        eid.printStackTrace();
                    }
                    try{
                    airport = result.getString("airport");
                    }catch(Exception eairport){
                        eairport.printStackTrace();
                    }
                    try{
                    area = result.getString("area");
                    }catch(Exception earea){
                        earea.printStackTrace();
                    }
                    try{
                    terminal = result.getString("terminal");
                    }catch(Exception eterminal){
                        eterminal.printStackTrace();
                    }
                    try {
                        description = result.getString("description");
                    } catch (Exception e) {
                        description="";
                        e.printStackTrace();
                    }
                    try {
                        name = result.getString("name");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        longitude = result.getString("longitude");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        latitude = result.getString("latitude");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        district = result.getString("district");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        website = result.getString("website");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        destination = result.getString("destination");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        country = result.getString("country");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        country_short = result.getString("country_short");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        address = result.getString("address");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        rating_total = result.getString("rating");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    /////////eating////
                    try {
                        eating_halal = result.getString("halal");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        eating_type = result.getString("type");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        eating_typeicon = result.getString("type_icon");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                try {
                    start_date= result.getString("start_date");
                    //start_date= df.dateformat("yyyy/mm/dd","EEEE, dd"+"rd "+"MMMM yyyy",start_date);
                    String First=df.dateformat("yyyy-mm-dd", "EEEE, ", start_date);
                    String dd=df.dateformat("yyyy-mm-dd", "d", start_date);
                    int day =Integer.parseInt(df.dateformat("yyyy-mm-dd", "d", start_date));
                    String Suffix =df.getDayNumberSuffix(day);
                    String Second=df.dateformat("yyyy-mm-dd", "MMMM yyyy", start_date);

                    start_date =  First+day+Suffix+Second;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    end_date = result.getString("end_date");
                    String First=df.dateformat("yyyy-mm-dd", "EEEE, ", end_date);
                    int day =Integer.parseInt(df.dateformat("yyyy-mm-dd", "d", end_date));
                    String Suffix =df.getDayNumberSuffix(day);
                    String Second=df.dateformat("yyyy-mm-dd", "MMMM yyyy", end_date);
                    end_date= First+day+Suffix+Second;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    JSONArray tickets=result.getJSONArray("tickets");
                    for(int t=0;t<tickets.length();t++){
                        JSONObject obj=tickets.getJSONObject(t);
                        if(t==0){
                            price_Text=obj.get("price")+currency;
                        }else {
                            price_Text = price_Text+"," + obj.get("price") + currency;
                        }
                    }
                    area = result.getString("area");
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                    try {
//                        JSONArray menu_img_array = result.getJSONArray("menu_images");
//                        String menu_imgItem = "";
//                      //  image_size=String.valueOf(menu_img_array.length());
//                        if (menu_img_array.length() == 0) {
//                            menu_imgItem_list.add("No Images::");
//                        } else {
//                            for (int ph = 0; ph < menu_img_array.length(); ph++) {
//                                String item = menu_img_array.get(ph).toString();
//
//                                if (!item.equals(""))
//                                    menu_imgItem += item + "::";
//                            }
//                            menu_imgItem_list.add(menu_imgItem);
//
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }

                    try {
                        JSONArray cuisines_array = result.getJSONArray("cuisines");
                        String cuisinesItem = "";
                        //  image_size=String.valueOf(menu_img_array.length());
                        if (cuisines_array.length() == 0) {
                           cuisinesItem_list.add("No Cuisines::");
                        } else {
                            for (int ph = 0; ph < cuisines_array.length(); ph++) {
                                String item = cuisines_array.get(ph).toString();

                                if (!item.equals(""))
                                    cuisinesItem += item + "::";
                            }
                            cuisinesItem_list.add(cuisinesItem);

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        JSONArray branches_array = result.getJSONArray("branches");

                        //  image_size=String.valueOf(menu_img_array.length());
                        if (branches_array.length() != 0) {

                            for (int ph = 0; ph < branches_array.length(); ph++) {
                                JSONObject jsonBranch = branches_array.getJSONObject(ph);
                                branchesItem_list.add(jsonBranch.getString("name"));
                                branchesItemid_list.add(jsonBranch.getString("id"));
                                branchesItemaddress_list.add(jsonBranch.getString("address"));
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    ////////////////

                    JSONArray phone_array = result.getJSONArray("phone");
                    String PhoneNumber = "";
                    if (phone_array.length() == 0) {
                        phone_list.add("No Numbers::");
                    } else {
                        for (int ph = 0; ph < phone_array.length(); ph++) {
                            String ph_no = phone_array.get(ph).toString();

                            if (!ph_no.equals(""))
                                PhoneNumber += ph_no + "::";
                        }
                        phone_list.add(PhoneNumber);

                    }

                    try {
                        JSONArray tag_array = result.getJSONArray("tagItems");
                        String tagItem = "";
                        if (tag_array.length() == 0) {
                            tagItem_list.add("No Tagitem::");
                        } else {
                            for (int ph = 0; ph < tag_array.length(); ph++) {
                                String item = tag_array.get(ph).toString();

                                if (!item.equals(""))
                                    tagItem += item + "::";
                            }
                            tagItem_list.add(tagItem);

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

/////////edited for rental in airline
                try {
                    JSONObject tag_obj= result.getJSONObject("tagItems");
                    Iterator<String> keys = tag_obj.keys();

                    JSONArray tag_array=(JSONArray)tag_obj.get(keys.next());





                        for (int ph = 0; ph < tag_array.length(); ph++) {
                           JSONObject obj= tag_array.getJSONObject(ph);

                            tagItem_name = obj.getString("category");
                        tagItem_rental_list.add(obj.getString("name"));
}

                } catch (Exception e) {
                    e.printStackTrace();
                }
                    try {
                        JSONArray img_array = result.getJSONArray("images");
                        String imgItem = "";
                        image_size=String.valueOf(img_array.length());
                        if (img_array.length() == 0) {
                            imgItem_list.add("No Images::");
                        } else {
                            for (int ph = 0; ph < img_array.length(); ph++) {
                                String item = img_array.get(ph).toString();

                                if (!item.equals(""))
                                    imgItem += AppConstants.GET_RESULTS_GENERAL_IMAGES_URL + "/" + getCategoryItemDirectorynameForImageAssetUrl(MapShowingFragment.selected_category) + "/" + item + "::";
                            }
                            imgItem_list.add(imgItem);

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        JSONArray video_array = result.getJSONArray("videos");
                        String videoItem = "";
                        video_size=String.valueOf(video_array.length());
                        if (video_array.length() == 0) {
                            videoItem_list.add("No Videos::");
                        } else {
                            for (int ph = 0; ph < video_array.length(); ph++) {
                                String item = video_array.get(ph).toString();

                                if (!item.equals(""))
                                    videoItem += item + "::";
                            }
                            videoItem_list.add(videoItem);

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    JSONObject timing=null;
                    try {
                         timing = result.getJSONObject("timings");
                        JSONArray mon = timing.getJSONArray("mon");
                        for (int i = 0; i < mon.length(); i++) {
                            JSONObject jsonMon = mon.getJSONObject(i);
                            time_day_list.add(jsonMon.getString("day"));
                            if(!MapShowingFragment.selected_category.equals("Airline")) {
                                time_departure_list.add(jsonMon.getString("open_time"));
                                time_duration_list.add(jsonMon.getString("close_time"));
                            }
                            else {
                                time_departure_list.add(jsonMon.getString("departure_time"));
                                time_duration_list.add(jsonMon.getString("duration"));
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        JSONArray sun = timing.getJSONArray("sun");
                        for (int i = 0; i < sun.length(); i++) {
                            JSONObject jsonMon = sun.getJSONObject(i);
                            time_day_list.add(jsonMon.getString("day"));
                            if(!MapShowingFragment.selected_category.equals("Airline")) {
                                time_departure_list.add(jsonMon.getString("open_time"));
                                time_duration_list.add(jsonMon.getString("close_time"));
                            }
                            else {
                                time_departure_list.add(jsonMon.getString("departure_time"));
                                time_duration_list.add(jsonMon.getString("duration"));
                            }

                        }
                    } catch (Exception ee) {
                        ee.printStackTrace();
                    }
                    try {
                        JSONArray tue = timing.getJSONArray("tue");
                        for (int i = 0; i < tue.length(); i++) {
                            JSONObject jsonMon = tue.getJSONObject(i);
                            time_day_list.add(jsonMon.getString("day"));
                            if(!MapShowingFragment.selected_category.equals("Airline")) {
                                time_departure_list.add(jsonMon.getString("open_time"));
                                time_duration_list.add(jsonMon.getString("close_time"));
                            }
                            else {
                                time_departure_list.add(jsonMon.getString("departure_time"));
                                time_duration_list.add(jsonMon.getString("duration"));
                            }

                        }
                    } catch (Exception eee) {
                        eee.printStackTrace();
                    }
                    try {
                        JSONArray wed = timing.getJSONArray("wed");
                        for (int i = 0; i < wed.length(); i++) {
                            JSONObject jsonMon = wed.getJSONObject(i);
                            time_day_list.add(jsonMon.getString("day"));
                            if(!MapShowingFragment.selected_category.equals("Airline")) {
                                time_departure_list.add(jsonMon.getString("open_time"));
                                time_duration_list.add(jsonMon.getString("close_time"));
                            }
                            else {
                                time_departure_list.add(jsonMon.getString("departure_time"));
                                time_duration_list.add(jsonMon.getString("duration"));
                            }

                        }
                    } catch (Exception eeee) {
                        eeee.printStackTrace();
                    }
                    try {
                        JSONArray the = timing.getJSONArray("thu");
                        for (int i = 0; i < the.length(); i++) {
                            JSONObject jsonMon = the.getJSONObject(i);
                            time_day_list.add(jsonMon.getString("day"));
                            if(!MapShowingFragment.selected_category.equals("Airline")) {
                                time_departure_list.add(jsonMon.getString("open_time"));
                                time_duration_list.add(jsonMon.getString("close_time"));
                            }
                            else {
                                time_departure_list.add(jsonMon.getString("departure_time"));
                                time_duration_list.add(jsonMon.getString("duration"));
                            }

                        }
                    } catch (Exception eeeee) {
                        eeeee.printStackTrace();
                    }
                    try {
                        JSONArray fri = timing.getJSONArray("fri");
                        for (int i = 0; i < fri.length(); i++) {
                            JSONObject jsonMon = fri.getJSONObject(i);
                            time_day_list.add(jsonMon.getString("day"));
                            if(!MapShowingFragment.selected_category.equals("Airline")) {
                                time_departure_list.add(jsonMon.getString("open_time"));
                                time_duration_list.add(jsonMon.getString("close_time"));
                            }
                            else {
                                time_departure_list.add(jsonMon.getString("departure_time"));
                                time_duration_list.add(jsonMon.getString("duration"));
                            }

                        }
                    } catch (Exception eeeeee) {
                        eeeeee.printStackTrace();
                    }
                    JSONArray sat = null;
                    try {
                        sat = timing.getJSONArray("sat");

                        for (int i = 0; i < sat.length(); i++) {
                            JSONObject jsonMon = sat.getJSONObject(i);
                            time_day_list.add(jsonMon.getString("day"));
                            if(!MapShowingFragment.selected_category.equals("Airline")) {
                                time_departure_list.add(jsonMon.getString("open_time"));
                                time_duration_list.add(jsonMon.getString("close_time"));
                            }
                            else {
                                time_departure_list.add(jsonMon.getString("departure_time"));
                                time_duration_list.add(jsonMon.getString("duration"));
                            }

                        }
                    } catch (Exception eeeeeee) {
                        eeeeeee.printStackTrace();
                    }

                try {
                    JSONArray tips_main_array = result.getJSONArray("tips");
                    for(int i=0;i<tips_main_array.length();i++){
                        ArrayList<String> tipsname_list = new ArrayList<String>();
                        JSONObject object=tips_main_array.getJSONObject(i);
                        tiptitle_list.add(object.getString("tips_title"));
                        tipid_list.add(object.getString("tips_id"));
                        JSONArray JsonTipsarray=object.getJSONArray("tips");
                                for(int j=0;j<JsonTipsarray.length();j++){
                                    JSONObject objectsub=JsonTipsarray.getJSONObject(j);
                                    tipsname_list.add(objectsub.getString("tips"));                                }
                        tips_MainArray.add(tipsname_list);
                        System.out.println("gggggg");
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
                try {
                    JSONArray categories_array = result.getJSONArray("categories");

                    //  image_size=String.valueOf(menu_img_array.length());
                    if (categories_array.length() != 0) {

                        for (int ph = 0; ph < categories_array.length(); ph++) {
                            JSONObject jsonBranch = categories_array.getJSONObject(ph);


                            Iterator<String> keys = jsonBranch.keys();

                            String tag_array=(String)jsonBranch.get(keys.next());

                      //      categoryItem_list.add(jsonBranch.getString("business_category_name"));
                            categoryItem_list.add(tag_array);
                            String jj;

                        }
                    if(MapShowingFragment.selected_category.equals("Events")){
                        for (int ph = 0; ph < categories_array.length(); ph++) {
                            JSONObject jsonBranch = categories_array.getJSONObject(ph);




                            String tag_array=jsonBranch.getString("title");

                            //      categoryItem_list.add(jsonBranch.getString("business_category_name"));
                            categoryItem_list_name.add(tag_array);


                        }
                    }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    JSONArray article_array = result.getJSONArray("articles");

                    //  image_size=String.valueOf(menu_img_array.length());
                    if (article_array.length() != 0) {

                        for (int ph = 0; ph < article_array.length(); ph++) {
                            JSONObject jsonBranch = article_array.getJSONObject(ph);
                            feature_article_list.add(jsonBranch.getString("articles_title"));

                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    JSONArray speciality_array = result.getJSONArray("specialities");
                    String videoItem = "";
               //     video_size=String.valueOf(speciality_array.length());
                    if (speciality_array.length() == 0) {

                    } else {
                        for (int ph = 0; ph < speciality_array.length(); ph++) {
                            String item = speciality_array.get(ph).toString();

                            if (!item.equals(""))
                               specialities_list.add(item);
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    JSONArray speciality_array = result.getJSONArray("door_policies");
                    String videoItem = "";
                    //     video_size=String.valueOf(speciality_array.length());
                    if (speciality_array.length() == 0) {

                    } else {
                        for (int ph = 0; ph < speciality_array.length(); ph++) {
                            String item = speciality_array.get(ph).toString();

                            if (!item.equals(""))
                                policies_list.add(item);
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    JSONArray cuisines_array = result.getJSONArray("cuisines");
                    String videoItem = "";
                    //     video_size=String.valueOf(speciality_array.length());
                    if (cuisines_array.length() == 0) {

                    } else {
                        for (int ph = 0; ph < cuisines_array.length(); ph++) {
                            String item = cuisines_array.get(ph).toString();

                            if (!item.equals(""))
                                cuisines_list.add(item);
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    JSONArray menu_array = result.getJSONArray("menu_images");
                    String videoItem = "";
                    //     video_size=String.valueOf(speciality_array.length());
                    if (menu_array.length() == 0) {

                    } else {
                        for (int ph = 0; ph < menu_array.length(); ph++) {
                            String item = menu_array.get(ph).toString();

                            if (!item.equals(""))
                                menu_imgItem_list.add(item);
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    JSONArray service_array = result.getJSONArray("services");
                    String videoItem = "";
                    //     video_size=String.valueOf(speciality_array.length());
                    if (service_array.length() == 0) {

                    } else {
                        for (int ph = 0; ph < service_array.length(); ph++) {
                            String item = service_array.get(ph).toString();

                            if (!item.equals(""))
                               service_details_list.add(item);
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    JSONArray agents_array = result.getJSONArray("agents");
                    String videoItem = "";
                    //     video_size=String.valueOf(speciality_array.length());
                    if (agents_array.length() == 0) {

                    } else {
                        for (int ph = 0; ph < agents_array.length(); ph++) {
                            JSONObject json=agents_array.getJSONObject(ph);
                            String item = json.getString("name").toString();

                            if (!item.equals(""))
                                agents_list.add(item);
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    JSONArray aminities_array = result.getJSONArray("aminities");

                    //     video_size=String.valueOf(speciality_array.length());
                    if (aminities_array.length() == 0) {

                    } else {
                        for (int ph = 0; ph < aminities_array.length(); ph++) {
                            JSONObject json=aminities_array.getJSONObject(ph);
                            String item = json.getString("title").toString();
                            String item_icon = json.getString("icon_name").toString();


                            if (!item.equals(""))
                               aminities_list.add(item);
                            if (!item_icon.equals(""))
                            aminities_img_list.add(item_icon);

                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    JSONArray freebies_array = result.getJSONArray("freebies");

                    //     video_size=String.valueOf(speciality_array.length());
                    if (freebies_array.length() == 0) {

                    } else {
                        for (int ph = 0; ph < freebies_array.length(); ph++) {
                            JSONObject json=freebies_array.getJSONObject(ph);
                            String item = json.getString("title").toString();
                            String item_icon = json.getString("icon_name").toString();


                            if (!item.equals(""))
                                freebies_list.add(item);
                            if (!item_icon.equals(""))
                                freebies_img_list.add(item_icon);

                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    JSONArray reviews_array = result.getJSONArray("reviews");

                    //     video_size=String.valueOf(speciality_array.length());
                    if (reviews_array.length() == 0) {

                    } else {
                        for (int ph = 0; ph < reviews_array.length(); ph++) {
                            JSONObject json=reviews_array.getJSONObject(ph);
                            String item_city = json.getString("member_city").toString();
                            String item_icon = json.getString("category_name").toString();
                            String item_date = json.getString("review_created_at").toString();
                            String item_name = json.getString("item_name").toString();
                            String item_membername = json.getString("member_first_name").toString();
                            String item_memberimage = json.getString("member_profile_image").toString();
                            String item_userid = json.getString("user_id").toString();
                            String item_rating = json.getString("rating").toString();
                            String item_comment = json.getString("comment").toString();

                            review_cityList.add(item_city);
                            review_iconList.add(item_icon);
                            review_dateList.add(item_date);
                            review_itemnameList.add(item_name);
                            review_membernameList.add(item_membername);
                            review_memberimageList.add(item_memberimage);
                            review_useridList.add(item_userid);
                            review_ratingList.add(item_rating);
                            review_commentList.add(item_comment);
                            rating_count.add(String.valueOf(review_ratingList.size()));


                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                ////
                loadDetails();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            LogUtility.Log(TAG, "onGetSearchItemResponse: :success", null);

        } else {
            LogUtility.Log(TAG, "onGetSearchItemResponse: :failure", null);

        }
    }
public void loadDetails(){
    //Review And Ratings Set
    if(review_itemnameList.size()!=0) {
//        mRecyclerView.setVisibility(View.VISIBLE);
//        tv_noreviews.setVisibility(View.GONE);
//        //review_headlayout.setVisibility(View.VISIBLE);
//
//        tv_rating_count.setText("("+String.valueOf(review_itemnameList.size())+")");
//        LinearLayoutManager mLayoutManager = new LinearLayoutManager(activity.getApplicationContext());
//    //        mRecyclerView.setLayoutManager(mLayoutManager);
//        mRecyclerView.setLayoutManager(new LinearLayoutManager(activity));
//
//        adapter = new ReviewAndRatingAdapter(activity, activity.getApplicationContext(), review_itemnameList, tv_subtitle_list, review_ratingList, rating_count, review_commentList, review_memberimageList,review_membernameList,review_dateList);
//        mRecyclerView.setAdapter(adapter);



        RatingBar ratingBar;
        TextView tv_desp,subtitle,title;
        SimpleDraweeView icon;

        for(int i=0;i<review_itemnameList.size();i++)
        {
            View rating_view = activity.getLayoutInflater().inflate(R.layout.rating_and_review_adapter, null);

            icon= (SimpleDraweeView)rating_view.findViewById(R.id.icon2);

            title= (TextView)rating_view.findViewById(R.id.title);
            subtitle= (TextView)rating_view.findViewById(R.id.subtitle);
            ratingBar= (RatingBar)rating_view.findViewById(R.id.ratingBar);
//            LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
//            stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
//            try {
//                stars.setTint(activity.getResources().getColor(R.color.grey_300));
//            }
//            catch(Exception ex)
//            {
//            }

            tv_desp= (TextView)rating_view.findViewById(R.id.tv_desp);


            title.setText(review_membernameList.get(i));
            String date=review_dateList.get(i);
            date = df.dateformat("yyyy-MM-dd HH:mm:ss","yyyy-MM-dd",date);
            subtitle.setText(date);
            ratingBar.setRating(Integer.parseInt(review_ratingList.get(i)));
            //       ratingtext.setText(mratetext.get(position));
            //      rating_count.setText(mratecount.get(position));
            tv_desp.setText(review_commentList.get(i));
            if(review_memberimageList.size()>0) {
                String image_name = review_memberimageList.get(i);
                Uri img_uri;
                String image_url = AppConstants.PROFILE_IMAGE_ASSET_URL + image_name;
                img_uri = Uri.parse(image_url);
                ImageRequest request = ImageRequest.fromUri(img_uri);
                DraweeController controller = Fresco.newDraweeControllerBuilder()
                        .setImageRequest(request)
                        .setOldController(icon.getController()).build();
                icon.setController(controller);
            }
            else
            {
                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.user).build();
                icon.setImageURI(imageRequest.getSourceUri());
            }


            lin_lay_review_and_rating.addView(rating_view);
        }


    }
    else
    {
        tv_noreviews.setVisibility(View.VISIBLE);
    }
    try {
        description_text.setText(Html.fromHtml(description));
    }catch(Exception e){
        e.printStackTrace();
    }
    if(description.equals("")){
        layout_scroll.setVisibility(View.GONE);

    }
    selected_name.setText(name);
    if(MapShowingFragment.selected_category.equals("Airline")) {
        country_name.setText(airport);
    }else{
        country_name.setText(country);
//        country_name.setVisibility(View.GONE);
    }
    image_count.setText(image_size);
    video_count.setText(video_size);
    rating_total_text.setText(rating_total);
    ratingBar.setRating(Float.valueOf(rating_total));
    if(imgItem_list.size()>0){




        String images[] = imgItem_list.get(0).split("::");

        String image_name = images[0];
        ArrayList<String> image_array= new ArrayList<>();
        Uri img_uri;
        for(int i=0;i<images.length;i++)
        {
            image_array.add(images[i]);
        }

//        if(image_name.equalsIgnoreCase("No Images"))
//        {
//            image_array.add(image_name);
//
//        }
//        else
//        {
//            String image_url = AppConstants.GET_RESULTS_GENERAL_IMAGES_URL + "/" + getCategoryItemDirectorynameForImageAssetUrl(MapShowingFragment.selected_category) + "/" + image_name;
//
//             img_uri= Uri.parse(image_url);
//        }

//        ImageRequest request = ImageRequest.fromUri(img_uri);
//
//
//        DraweeController controller = Fresco.newDraweeControllerBuilder()
//                .setImageRequest(request)
//                .setOldController(sdv_detailsimage.getController()).build();
//        //Log.e(TAG, "ImagePath uri " + img_uri);
//
//       sdv_detailsimage.setController(controller);

        SearchResultPagerAdapter adapter = new SearchResultPagerAdapter(activity, image_array,0,null);
        vpPager.setAdapter(adapter);

//        vpPager.setPageMargin(20);
        vpPager.setCurrentItem(0);

        indicator.setFillColor(activity.getResources().getColor(android.R.color.white));
        indicator.setViewPager(vpPager);

    }
    if(MapShowingFragment.selected_category.equals("Airline")) {
       // events_layout.setVisibility(View.VISIBLE);
        if(feature_article_list.size()!=0) {
            featured_article_layout.setVisibility(View.VISIBLE);
        }
        if(time_day_list.size()!=0) {
       //     planRecyclerView.setVisibility(View.VISIBLE);
            add_layout.setVisibility(View.VISIBLE);

        }
        if (tiptitle_list.size() != 0) {
            tips_layout.setVisibility(View.VISIBLE);
        }
        if(!(terminal.equals("")&&destination.equals(""))) {
            from_to.setVisibility(View.VISIBLE);
            from_to.setText("From: " + terminal + " To: " + destination);
        }

    }
    else if(MapShowingFragment.selected_category.equals("Business")){
       //category notreturned


        if(!address.equals("")) {
            address_head.setVisibility(View.VISIBLE);
            address_text.setVisibility(View.VISIBLE);
            address_text.setText(address);
        }
//        if(categoryItem_list.size()!=0) {
//            categories_layout.setVisibility(View.VISIBLE);
//
//        }




        if(feature_article_list.size()!=0) {
            featured_article_layout.setVisibility(View.VISIBLE);
        }
        if(events_list.size()!=0) {
            events_layout.setVisibility(View.VISIBLE);
        }

    }
    else if(MapShowingFragment.selected_category.equals("Car Rentals")){
        if(service_details_list.size()!=0) {
            service_layout.setVisibility(View.VISIBLE);
        }
        if (tiptitle_list.size() != 0) {
            tips_layout.setVisibility(View.VISIBLE);
        }

        if(events_list.size()!=0) {
            events_layout.setVisibility(View.VISIBLE);
        }
        if(feature_article_list.size()!=0) {
            featured_article_layout.setVisibility(View.VISIBLE);
        }
        if(branchesItem_list.size()!=0) {
        //    recycler_view_branches.setVisibility(View.VISIBLE);
            branch_layout.setVisibility(View.VISIBLE);
            branches_head.setVisibility(View.VISIBLE);
        }

    } else if(MapShowingFragment.selected_category.equals("Eating Out")){
        if(branchesItem_list.size()!=0) {
      //      recycler_view_branches.setVisibility(View.VISIBLE);
            branch_layout.setVisibility(View.VISIBLE);
            branches_head.setVisibility(View.VISIBLE);
        }
        if(cuisines_list.size()!=0) {
            cuisines_layout.setVisibility(View.VISIBLE);
        }

        if(feature_article_list.size()!=0) {
            featured_article_layout.setVisibility(View.VISIBLE);
        }
        if(menu_imgItem_list.size()!=0) {
            menu_text.setVisibility(View.VISIBLE);
            recycler_view_menu.setVisibility(View.VISIBLE);
        }


    } else if(MapShowingFragment.selected_category.equals("Embasies & Consulates")){
        if(feature_article_list.size()!=0) {
            featured_article_layout.setVisibility(View.VISIBLE);
        }
        if(events_list.size()!=0) {
            events_layout.setVisibility(View.VISIBLE);
        }
    }else if(MapShowingFragment.selected_category.equals("Emergencies")){

        if(feature_article_list.size()!=0) {
            featured_article_layout.setVisibility(View.VISIBLE);
        }
        if(events_list.size()!=0) {
            events_layout.setVisibility(View.VISIBLE);
        }
        if(!address.equals("")) {
            address_text.setVisibility(View.VISIBLE);
            address_head.setVisibility(View.VISIBLE);
        }
    }else if(MapShowingFragment.selected_category.equals("Events")){

        if(feature_article_list.size()!=0) {
            featured_article_layout.setVisibility(View.VISIBLE);
        }
        if(!start_date.equals("")){
            tv_timeperiod.setText(start_date+"   -   "+end_date);
            time_layout.setVisibility(View.VISIBLE);
        }
        if(!area.equals("")){
        tv_venue.setText(area);
            venue_layout.setVisibility(View.VISIBLE);
        }
        if(!price_Text.equals("")){
        tv_price.setText(price_Text);
            tv_price.setVisibility(View.VISIBLE);
        }

    }else if(MapShowingFragment.selected_category.equals("Money Exchange")){
        if (tiptitle_list.size() != 0) {
            tips_layout.setVisibility(View.VISIBLE);
        }

        if(feature_article_list.size()!=0) {
            featured_article_layout.setVisibility(View.VISIBLE);
        }
        if(events_list.size()!=0) {
            events_layout.setVisibility(View.VISIBLE);
        }
        if(branchesItem_list.size()!=0) {
          //  recycler_view_branches.setVisibility(View.VISIBLE);
            branch_layout.setVisibility(View.VISIBLE);
            branches_head.setVisibility(View.VISIBLE);
        }
    }
    else if(MapShowingFragment.selected_category.equals("Shopping")){
        if (tiptitle_list.size() != 0) {
            tips_layout.setVisibility(View.VISIBLE);
        }
        if(feature_article_list.size()!=0) {
            featured_article_layout.setVisibility(View.VISIBLE);
        }
        if(time_day_list.size()!=0) {
       //     planRecyclerView.setVisibility(View.VISIBLE);
            add_layout.setVisibility(View.VISIBLE);
        }
    }
    else if(MapShowingFragment.selected_category.equals("Real Estate")){
        if (tiptitle_list.size() != 0) {
            tips_layout.setVisibility(View.VISIBLE);
        }
        if(agents_list.size()!=0) {
            agents_layout.setVisibility(View.VISIBLE);
        }
//       featured_article_layout.setVisibility(View.VISIBLE);
//        planRecyclerView.setVisibility(View.VISIBLE);
    }
    else if(MapShowingFragment.selected_category.equals("Tour & Travel")){
        if (tiptitle_list.size() != 0) {
            tips_layout.setVisibility(View.VISIBLE);
        }

    }
    else if(MapShowingFragment.selected_category.equals("Spa")){
        if (tiptitle_list.size() != 0) {
            tips_layout.setVisibility(View.VISIBLE);
        }
        if(time_day_list.size()!=0) {
        //    planRecyclerView.setVisibility(View.VISIBLE);
            add_layout.setVisibility(View.VISIBLE);
        }
//        if(specialities_list.size()!=0) {
//            specialities_layout.setVisibility(View.VISIBLE);
//        }
    }
    else if(MapShowingFragment.selected_category.equals("Things To Do")){
        if (tiptitle_list.size() != 0) {
            tips_layout.setVisibility(View.VISIBLE);
        }

    }
    else if(MapShowingFragment.selected_category.equals("Stay/Hotels")){
        if (tiptitle_list.size() != 0) {
            tips_layout.setVisibility(View.VISIBLE);
        }

        if(aminities_list.size()!=0) {
            recycler_view_aminities.setVisibility(View.VISIBLE);
            amminities_head.setVisibility(View.VISIBLE);
        }
        if(freebies_list.size()!=0) {
            recycler_view_freebies.setVisibility(View.VISIBLE);
            freebies_head.setVisibility(View.VISIBLE);
        }
    }
//    else if(MapShowingFragment.selected_category.equals("Tour & Travel")){
//        tips_layout.setVisibility(View.VISIBLE);
//
//    }
//    else if(MapShowingFragment.selected_category.equals("Tour & Travel")){
//        tips_layout.setVisibility(View.VISIBLE);
//
//    }

    //////////Airline////////
//        feature_article_list.add("Featured Article Injected humour or randomised ewords");
//        feature_article_list.add("Featured Article Even slightly believable.If you are going to use a passage");
    if (tagItem_rental_list.size() != 0) {
        tagItem_name = tagItem_name.substring(0, 1).toUpperCase() + tagItem_name.substring(1);
        tv_tag_head.setText(tagItem_name);
        tag_layout.setVisibility(View.VISIBLE);
        tagdetails_layout.setVisibility(View.VISIBLE);
        for (int i = 0; i < tagItem_rental_list.size(); i++) {
            LinearLayout hor_event_layout = new LinearLayout(activity);


            hor_event_layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            hor_event_layout.setOrientation(LinearLayout.HORIZONTAL);
            hor_event_layout.setGravity(Gravity.CENTER_VERTICAL);
            hor_event_layout.setPadding(10, 10, 10, 10);
            ImageView im = new ImageView(activity);
            im.setLayoutParams(new LinearLayout.LayoutParams(10, 10));
            im.setImageResource(R.drawable.dot);

            TextView event_text = new TextView(activity);
            event_text.setText(tagItem_rental_list.get(i));
            event_text.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            event_text.setTextColor(Color.BLACK);
            event_text.setPadding(10,0,0,0);

            hor_event_layout.addView(im);
            hor_event_layout.addView(event_text);
            tagdetails_layout.addView(hor_event_layout);
        }
    }
        if (feature_article_list.size() != 0) {

            for (int i = 0; i < feature_article_list.size(); i++) {
                LinearLayout hor_event_layout = new LinearLayout(activity);


                hor_event_layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                hor_event_layout.setOrientation(LinearLayout.HORIZONTAL);
                hor_event_layout.setGravity(Gravity.CENTER_VERTICAL);
                hor_event_layout.setPadding(10, 10, 10, 10);
                ImageView im = new ImageView(activity);
                im.setLayoutParams(new LinearLayout.LayoutParams(10, 10));
                im.setImageResource(R.drawable.dot);
                TextView event_text = new TextView(activity);
                event_text.setText(feature_article_list.get(i));
                event_text.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                event_text.setTextColor(Color.BLACK);
                event_text.setPadding(10, 0, 0, 0);
                hor_event_layout.addView(im);
                hor_event_layout.addView(event_text);
                featured_articledetails_layout.addView(hor_event_layout);
            }
        }
        events_list.add("Events Injected humour or randomised ewords");
        events_list.add("Events Even slightly believable.If you are going to use a passage");
        if (events_list.size() != 0) {

            for (int i = 0; i < events_list.size(); i++) {
                LinearLayout hor_event_layout = new LinearLayout(activity);


                hor_event_layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                hor_event_layout.setOrientation(LinearLayout.HORIZONTAL);
                hor_event_layout.setGravity(Gravity.CENTER_VERTICAL);
                hor_event_layout.setPadding(10, 10, 10, 10);
                ImageView im = new ImageView(activity);
                im.setLayoutParams(new LinearLayout.LayoutParams(10, 10));
                im.setImageResource(R.drawable.dot);

                TextView event_text = new TextView(activity);
                event_text.setText(events_list.get(i));
                event_text.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                event_text.setTextColor(Color.BLACK);
                event_text.setPadding(10,0,0,0);

                hor_event_layout.addView(im);
                hor_event_layout.addView(event_text);
                eventsdetails_layout.addView(hor_event_layout);
            }
        }


    if (time_day_list.size() != 0) {
        String TimeFrom="",TimeTo="",durationtime="";

        for (int i = 0; i < time_day_list.size(); i++) {
            String Day=time_day_list.get(i);
            if(Day.equals("sun"))Day="Sunday";
            if(Day.equals("mon"))Day="Monday";
            if(Day.equals("tue"))Day="Tuesday";
            if(Day.equals("wed"))Day="Wednesday";
            if(Day.equals("thu"))Day="Thursday";
            if(Day.equals("fri"))Day="Friday";
            if(Day.equals("sat"))Day="Saturday";


            LinearLayout hor_event_layout = new LinearLayout(activity);
            View inflatedView = View.inflate(activity, R.layout.addplan_adapter,hor_event_layout);
            TextView plantext=(TextView)inflatedView.findViewById(R.id.plan_text);
            //TextView add_plan=(TextView)inflatedView.findViewById(R.id.add_plan);
            if(MapShowingFragment.selected_category.equalsIgnoreCase("Airline"))
            {
                TimeFrom =  time_departure_list.get(i);
                Date DepTime =  df.Dateformat_as_date("h:mm:ss", "h:mm:ss", TimeFrom);

                String durationtime_array[] =  time_duration_list.get(i).split(" ");
                durationtime = durationtime_array[0];
                int durtime = Integer.parseInt(durationtime.trim());
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(DepTime);
                calendar.add(Calendar.HOUR, durtime);
                Date Arrivingtime = calendar.getTime();
                int hh=Arrivingtime.getHours();
                int mm=Arrivingtime.getMinutes();
                int ss=Arrivingtime.getSeconds();
                TimeTo = String.format("%02d", hh)+":"+String.format("%02d", mm)+":"+String.format("%02d", ss);
            }

            else
            {
                TimeFrom =  time_departure_list.get(i);
                TimeTo =time_duration_list.get(i);
            }
            plantext.setText(Day+" : From " + TimeFrom + " To " + TimeTo);
            if(MapShowingFragment.selected_category.equalsIgnoreCase("Airline"))
            {
                plantext.setText(Day.substring(0,2).toUpperCase()+" " + TimeFrom + " " + durationtime);
            }
            else if(MapShowingFragment.selected_category.equalsIgnoreCase("Shopping"))
            {
                //Wed: 19:10:00 - 19:10:00
                plantext.setText(Day+" : From " + TimeFrom + " To " + TimeTo);
            }
            else if(MapShowingFragment.selected_category.equalsIgnoreCase("Eating Out") ||
                    MapShowingFragment.selected_category.equalsIgnoreCase("Spa"))
            {
                //Wed: 19:10:00 - 19:10:00
                plantext.setText(Day.substring(0,2)+" :  " + TimeFrom + " - " + TimeTo);
            }
            else if(MapShowingFragment.selected_category.equalsIgnoreCase("Things to do"))
            {
                //Wed: 19:10:00 - 19:10:00
                plantext.setText(Day+" " + df.dateformat("HH:mm:ss", "HH:mm", TimeFrom) + " - " + df.dateformat("HH:mm:ss","HH:mm a",TimeTo));
            }
//           if(MapShowingFragment.selected_category.equalsIgnoreCase("Shopping")){
//                add_plan.setVisibility(View.GONE);
//            }
//            add_plan.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Toast.makeText(activity, "Add To Plan", Toast.LENGTH_LONG).show();
//                }
//            });
            add_layout.addView(inflatedView);
        }
    }

    if (branchesItemid_list.size() != 0) {
        String TimeFrom,TimeTo;

        for (int i = 0; i < branchesItemid_list.size(); i++) {
            LinearLayout hor_event_layout = new LinearLayout(activity);
            View inflatedView = View.inflate(activity, R.layout.branches_adapter,hor_event_layout);
            TextView branchname_text=(TextView)inflatedView.findViewById(R.id.branchname_text);
            TextView branchname_address=(TextView)inflatedView.findViewById(R.id.branchname_address);


            branchname_text.setText(branchesItem_list.get(i));
            branchname_address.setText(branchesItemaddress_list.get(i));

            branch_layout.addView(inflatedView);
        }
    }
//    LinearLayoutManager mLayoutManager = new LinearLayoutManager(activity.getApplicationContext());
//    planRecyclerView.setLayoutManager(new LinearLayoutManager(activity));
//    planadapter = new AddPlanAdapter(activity, activity.getApplicationContext(), time_day_list, time_departure_list, time_duration_list);
//    planRecyclerView.setAdapter(planadapter);

    ///buisiness////
//    recycler_view_branches.setLayoutManager(new LinearLayoutManager(activity));
//    branchesAdapter = new BranchesAdapter(activity,branchesItemid_list,branchesItem_list,branchesItemaddress_list);
//    recycler_view_branches.setAdapter(branchesAdapter);
//    tips_list.add("Injected humour or randomised ewords");
//    tips_list.add("Even slightly believable.If you are going to use a passage");
    if (tiptitle_list.size() != 0) {
        for(int t=0;t<tiptitle_list.size();t++) {
            LinearLayout tipsMain_layout = new LinearLayout(activity);


            tipsMain_layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            tipsMain_layout.setOrientation(LinearLayout.VERTICAL);
            tipsMain_layout.setGravity(Gravity.CENTER_VERTICAL);

            TextView tips_title_text = new TextView(activity);
            tips_title_text.setText(tiptitle_list.get(t));
            tips_title_text.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            tips_title_text.setTextColor(Color.BLACK);
            tips_title_text.setPadding(10, 0, 0, 0);
            tips_title_text.setTextSize(17);


            tipsMain_layout.addView(tips_title_text);
            ArrayList<String> tipNameList=tips_MainArray.get(t);
            for (int i = 0; i < tipNameList.size(); i++) {
                LinearLayout hor_event_layout = new LinearLayout(activity);


                hor_event_layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                hor_event_layout.setOrientation(LinearLayout.HORIZONTAL);
                hor_event_layout.setGravity(Gravity.CENTER_VERTICAL);
                hor_event_layout.setPadding(10, 10, 10, 10);
                ImageView im = new ImageView(activity);
                im.setLayoutParams(new LinearLayout.LayoutParams(10, 10));
                im.setImageResource(R.drawable.dot);

                TextView event_text = new TextView(activity);
                event_text.setText(tipNameList.get(i));
                event_text.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                event_text.setTextColor(Color.BLACK);
                event_text.setPadding(10, 0, 0, 0);

                hor_event_layout.addView(im);
                hor_event_layout.addView(event_text);
                tipsMain_layout.addView(hor_event_layout);
            }

            tipsdetails_layout.addView(tipsMain_layout);
        }
    }
    if (categoryItem_list.size() != 0) {
        categories_layout.setVisibility(View.VISIBLE);
        if(MapShowingFragment.selected_category.equals("Events")){
                recycler_view_categories.setVisibility(View.VISIBLE);
                recycler_view_categories.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));

                categoryImageAdapter = new CategoryImageAdapter(activity, activity.getApplicationContext(), categoryItem_list,categoryItem_list_name);
                recycler_view_categories.setAdapter(categoryImageAdapter);

        }
        else {
            categoriesdetails_layout.setVisibility(View.VISIBLE);
        }
        for (int i = 0; i < categoryItem_list.size(); i++) {
            LinearLayout hor_event_layout = new LinearLayout(activity);


            hor_event_layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            hor_event_layout.setOrientation(LinearLayout.HORIZONTAL);
            hor_event_layout.setGravity(Gravity.CENTER_VERTICAL);
            hor_event_layout.setPadding(10, 10, 10, 10);
            ImageView im = new ImageView(activity);
            im.setLayoutParams(new LinearLayout.LayoutParams(10, 10));
            im.setImageResource(R.drawable.dot);
            TextView event_text = new TextView(activity);
            event_text.setText(categoryItem_list.get(i));
            event_text.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            event_text.setTextColor(Color.BLACK);
            event_text.setPadding(10, 0, 0, 0);
            hor_event_layout.addView(im);
            hor_event_layout.addView(event_text);
            categoriesdetails_layout.addView(hor_event_layout);
        }
    }
///carrentals///
//   service_details_list.add("Service Injected humour or randomised ewords");
//    service_details_list.add("Service Even slightly believable.");
    if (service_details_list.size() != 0) {

        for (int i = 0; i < service_details_list.size(); i++) {
            LinearLayout hor_event_layout = new LinearLayout(activity);


            hor_event_layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            hor_event_layout.setOrientation(LinearLayout.HORIZONTAL);
            hor_event_layout.setGravity(Gravity.CENTER_VERTICAL);
            hor_event_layout.setPadding(10, 10, 10, 10);
            ImageView im = new ImageView(activity);
            im.setLayoutParams(new LinearLayout.LayoutParams(10, 10));
            im.setImageResource(R.drawable.dot);
            TextView event_text = new TextView(activity);
            event_text.setText(service_details_list.get(i));
            event_text.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            event_text.setTextColor(Color.BLACK);
            event_text.setPadding(10, 0, 0, 0);
            hor_event_layout.addView(im);
            hor_event_layout.addView(event_text);
            service_details_layout.addView(hor_event_layout);
        }
    }
    /////eating out////////
    if(menu_imgItem_list!=null) {
        if(menu_imgItem_list.size()>0) {
            recycler_view_menu.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
            menuadapter = new MenuAdapter(activity, menu_imgItem_list);
            recycler_view_menu.setAdapter(menuadapter);
        }
    }

//////Stayhotel
    recycler_view_aminities.setLayoutManager(new LinearLayoutManager(activity,LinearLayoutManager.HORIZONTAL,false));
    if(aminities_list.size()!=0) {
        aminitiesadapter = new AminitiesImageAdapter(activity, activity.getApplicationContext(), aminities_img_list, aminities_list);
        recycler_view_aminities.setAdapter(aminitiesadapter);
    }

    recycler_view_freebies.setLayoutManager(new LinearLayoutManager(activity,LinearLayoutManager.HORIZONTAL,false));
    if(freebies_list.size()!=0) {
        freebiesadapter = new FreebiesImageAdapter(activity, activity.getApplicationContext(), freebies_img_list, freebies_list);
        recycler_view_freebies.setAdapter(freebiesadapter);
    }
//    cuisines_list.add("Cuisines Injected humour or randomised ewords");
//    cuisines_list.add("Cuisines Even slightly believable.If you are going to use a passage");
    if (cuisines_list.size() != 0) {

        for (int i = 0; i < cuisines_list.size(); i++) {
            LinearLayout hor_event_layout = new LinearLayout(activity);


            hor_event_layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            hor_event_layout.setOrientation(LinearLayout.HORIZONTAL);
            hor_event_layout.setGravity(Gravity.CENTER_VERTICAL);
            hor_event_layout.setPadding(10, 10, 10, 10);
            ImageView im = new ImageView(activity);
            im.setLayoutParams(new LinearLayout.LayoutParams(10, 10));
            im.setImageResource(R.drawable.dot);
            TextView event_text = new TextView(activity);
            event_text.setText(cuisines_list.get(i));
            event_text.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            event_text.setTextColor(Color.BLACK);
            event_text.setPadding(10, 0, 0, 0);
            hor_event_layout.addView(im);
            hor_event_layout.addView(event_text);
            cuisinesdetails_newlayout.addView(hor_event_layout);
        }
    }

///Realestate
//    agents_list.add("Agents  Injected humour or randomised ewords");
//    agents_list.add("Agents  Even slightly believable.If you are going to use a passage");
    if (agents_list.size() != 0) {

        for (int i = 0; i < agents_list.size(); i++) {
            LinearLayout hor_event_layout = new LinearLayout(activity);


            hor_event_layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            hor_event_layout.setOrientation(LinearLayout.HORIZONTAL);
            hor_event_layout.setGravity(Gravity.CENTER_VERTICAL);
            hor_event_layout.setPadding(10, 10, 10, 10);
            ImageView im = new ImageView(activity);
            im.setLayoutParams(new LinearLayout.LayoutParams(10, 10));
            im.setImageResource(R.drawable.dot);
            TextView event_text = new TextView(activity);
            event_text.setText(agents_list.get(i));
            event_text.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            event_text.setTextColor(Color.BLACK);
            event_text.setPadding(10, 0, 0, 0);
            hor_event_layout.addView(im);
            hor_event_layout.addView(event_text);
            agentsdetails_layout.addView(hor_event_layout);
        }
    }

///Spa
//    specialities_list.add("Specialities  Injected humour or randomised ewords");
//    specialities_list.add("Specialities  Even slightly believable.If you are going to use a passage");
    if (specialities_list.size() != 0) {
        specialities_layout.setVisibility(View.VISIBLE);
        for (int i = 0; i < specialities_list.size(); i++) {
            LinearLayout hor_event_layout = new LinearLayout(activity);


            hor_event_layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            hor_event_layout.setOrientation(LinearLayout.HORIZONTAL);
            hor_event_layout.setGravity(Gravity.CENTER_VERTICAL);
            hor_event_layout.setPadding(10, 10, 10, 10);
            ImageView im = new ImageView(activity);
            im.setLayoutParams(new LinearLayout.LayoutParams(10, 10));
            im.setImageResource(R.drawable.dot);
            TextView event_text = new TextView(activity);
            event_text.setText(specialities_list.get(i));
            event_text.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            event_text.setTextColor(Color.BLACK);
            event_text.setPadding(10, 0, 0, 0);
            hor_event_layout.addView(im);
            hor_event_layout.addView(event_text);
            specialitiesdetails_layout.addView(hor_event_layout);
        }
    }

    if (policies_list.size() != 0) {
        policies_layout.setVisibility(View.VISIBLE);
        for (int i = 0; i < policies_list.size(); i++) {
            LinearLayout hor_event_layout = new LinearLayout(activity);


            hor_event_layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            hor_event_layout.setOrientation(LinearLayout.HORIZONTAL);
            hor_event_layout.setGravity(Gravity.CENTER_VERTICAL);
            hor_event_layout.setPadding(10, 10, 10, 10);
            ImageView im = new ImageView(activity);
            im.setLayoutParams(new LinearLayout.LayoutParams(10, 10));
            im.setImageResource(R.drawable.dot);
            TextView event_text = new TextView(activity);
            event_text.setText(policies_list.get(i));
            event_text.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            event_text.setTextColor(Color.BLACK);
            event_text.setPadding(10, 0, 0, 0);
            hor_event_layout.addView(im);
            hor_event_layout.addView(event_text);
            policiesdetails_layout.addView(hor_event_layout);
        }
    }
    animation_layout.setVisibility(View.GONE);
    main_scroll.setVisibility(View.VISIBLE);
}
public void initialisation(){
    //Fresco.initialize(activity);
    vpPager = (ViewPager) view.findViewById(R.id.pager);
    indicator = (CirclePageIndicator) view.findViewById(R.id.indicator);
    mNetworkManager = NetworkManager.getSingleInstance(activity);
    mNetworkManager.setOnAddPlanResponseListener(this);
    mNetworkManager.setOnGetWriteReviewResponseListener(this);
    mPreferenceManager = new PreferenceManager(activity);
    review_enter_layout=(LinearLayout)view.findViewById(R.id.review_enter_layout);
    lin_lay_review_and_rating=(LinearLayout)view.findViewById(R.id.lin_lay_review_and_rating);
    mRecyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
    recycler_view_categories = (RecyclerView) view.findViewById(R.id.recycler_view_categories);
    planRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_addplan);
    recycler_view_branches = (RecyclerView) view.findViewById(R.id.recycler_view_branches);
    recycler_view_menu = (RecyclerView) view.findViewById(R.id.recycler_view_menu);
    recycler_view_aminities = (RecyclerView) view.findViewById(R.id.recycler_view_aminities);
    recycler_view_freebies = (RecyclerView) view.findViewById(R.id.recycler_view_freebies);
    main_scroll = (ScrollView) view.findViewById(R.id.main_scroll);
    //child_scroll = (ScrollView) view.findViewById(R.id.child_scroll);
    child_scroll_name = (ScrollView) view.findViewById(R.id.child_scroll_name);
    tv_addtomyplan = (TextView) view.findViewById(R.id.tv_addtomyplan);
    address_head = (TextView) view.findViewById(R.id.address_head);
    from_to = (TextView) view.findViewById(R.id.from_to);
    menu_text = (TextView) view.findViewById(R.id.menu_text);
    featured_article_layout=(LinearLayout)view.findViewById(R.id.featured_layout);
    venue_layout=(LinearLayout)view.findViewById(R.id.venue_layout);
    time_layout=(LinearLayout)view.findViewById(R.id.timeperiod_layout);
    events_layout=(LinearLayout)view.findViewById(R.id.events_layout);
    featured_articledetails_layout=(LinearLayout)view.findViewById(R.id.featureddetails_layout);
    tagdetails_layout=(LinearLayout)view.findViewById(R.id.tagdetails_layout);
    tag_layout=(LinearLayout)view.findViewById(R.id.tag_layout);
    eventsdetails_layout=(LinearLayout)view.findViewById(R.id.eventsdetails_layout);
    tipsdetails_layout=(LinearLayout)view.findViewById(R.id.tipsdetails_layout);
    tips_layout=(LinearLayout)view.findViewById(R.id.tips_layout);
    service_details_layout=(LinearLayout)view.findViewById(R.id.servicesdetails_layout);
    service_layout=(LinearLayout)view.findViewById(R.id.services_layout);
    cuisinesdetails_newlayout=(LinearLayout)view.findViewById(R.id.cuisinesdetails_newlayout);
    cuisines_layout=(LinearLayout)view.findViewById(R.id.cuisines_layout);
    agentsdetails_layout=(LinearLayout)view.findViewById(R.id.agentsdetails_layout);
    agents_layout=(LinearLayout)view.findViewById(R.id.agents_layout);
    policiesdetails_layout=(LinearLayout)view.findViewById(R.id.policiesdetails_layout);
    policies_layout=(LinearLayout)view.findViewById(R.id.policies_layout);
    specialitiesdetails_layout=(LinearLayout)view.findViewById(R.id.specialitiesdetails_layout);
    specialities_layout=(LinearLayout)view.findViewById(R.id.specialities_layout);
    description_layout=(LinearLayout)view.findViewById(R.id.description_layout);
    categoriesdetails_layout=(LinearLayout)view.findViewById(R.id.categoriesdetails_layout);
    categories_layout=(LinearLayout)view.findViewById(R.id.categories_layout);
    layout_scroll=(LinearLayout)view.findViewById(R.id.layout_scroll);
    animation_layout=(RelativeLayout)view.findViewById(R.id.animation_layout);
    review_headlayout=(RelativeLayout)view.findViewById(R.id.review_headlayout);
    add_layout=(LinearLayout)view.findViewById(R.id.add_layout);
    branch_layout=(LinearLayout)view.findViewById(R.id.branch_layout);
    ratingBar=(RatingBar)view.findViewById(R.id.ratingBar);
    ratingBarreview=(RatingBar)view.findViewById(R.id.ratingBarreview);
//    LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
//    stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
//    try {
//        stars.setTint(getResources().getColor(R.color.grey_300));
//    }
//    catch(Exception ex)
//    {
//
//    }


//    LayerDrawable ratingBarreview_stars = (LayerDrawable) ratingBarreview.getProgressDrawable();
//    ratingBarreview_stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
//    try {
//        ratingBarreview_stars.setTint(getResources().getColor(R.color.grey_300));
//    }
//    catch(Exception ex)
//    {
//
//    }


    review_Image=(SimpleDraweeView)view.findViewById(R.id.review_Image);
    //ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.user).build();

    String member_profile_image = mPreferenceManager.getmember_profile_image();


    if(member_profile_image == null) {
        ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.user).build();
        review_Image.setImageURI(imageRequest.getSourceUri());
    }
    else if(member_profile_image.equals("")) {
        ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.user).build();
        review_Image.setImageURI(imageRequest.getSourceUri());
    }
    else
    {
        Uri img_uri=Uri.parse(member_profile_image);
        ImageRequest request = ImageRequest.fromUri(img_uri);

        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setImageRequest(request)
                .setOldController(review_Image.getController()).build();
        //Log.e(TAG, "ImagePath uri " + img_uri);

        review_Image.setController(controller);
    }

    review_username=(TextView) view.findViewById(R.id.review_username);
    review_username.setText(mPreferenceManager.getRegisteredUser_name());


    lin_lay_call=(LinearLayout)view.findViewById(R.id.lin_lay_call);
    lin_lay_location=(LinearLayout)view.findViewById(R.id.lin_lay_location);
    lin_lay_review=(LinearLayout)view.findViewById(R.id.lin_lay_review);
    lin_lay_gps=(LinearLayout)view.findViewById(R.id.lin_lay_gps);
    lin_lay_site=(LinearLayout)view.findViewById(R.id.lin_lay_site);

    lin_lay_call.setOnClickListener(this);
    lin_lay_location.setOnClickListener(this);
    lin_lay_review.setOnClickListener(this);
    lin_lay_gps.setOnClickListener(this);
    lin_lay_site.setOnClickListener(this);

    address_text=(TextView)view.findViewById(R.id.address_text);
    add_review=(TextView)view.findViewById(R.id.add_review);

    tv_rating_count= (TextView)view.findViewById(R.id.rating_count);
    tv_noreviews= (TextView)view.findViewById(R.id.tv_noreviews);
    description_text=(TextView)view.findViewById(R.id.description_text);
    description_head=(TextView)view.findViewById(R.id.description_head);
    selected_name=(TextView)view.findViewById(R.id.selected_name);
    country_name=(TextView)view.findViewById(R.id.country_name);
    video_count=(TextView)view.findViewById(R.id.video_count);
    image_count=(TextView)view.findViewById(R.id.image_count);
    //sdv_detailsimage=(SimpleDraweeView)view.findViewById(R.id.sdvImage);
    rating_total_text=(TextView)view.findViewById(R.id.rating_total);
    branches_head=(TextView)view.findViewById(R.id.branch_head);
    amminities_head=(TextView)view.findViewById(R.id.amminities_head);
    freebies_head=(TextView)view.findViewById(R.id.freebies_head);
    tv_tag_head=(TextView)view.findViewById(R.id.tv_tag_head);
    tv_price=(TextView)view.findViewById(R.id.tv_price);
    tv_timeperiod=(TextView)view.findViewById(R.id.tv_timeperiod);
    tv_venue=(TextView)view.findViewById(R.id.tv_venue);
    et_review=(EditText)view.findViewById(R.id.et_review);
    bt_submit=(TextView)view.findViewById(R.id.bt_submit);
    bt_close=(ImageView)view.findViewById(R.id.bt_close);



    TAG = Google_Map_Fragment.class.getSimpleName();
    mJsonObjectMaker = new JsonObjectMaker(activity);
    mNetworkManager = NetworkManager.getSingleInstance(activity);
    mNetworkManager.setOnGetSearchItemResponseListener(this);
    mListCreatorForNameValuePairRequest = new ListCreatorForNameValuePairRequest(activity);
}
    public String getCategoryItemDirectorynameForImageAssetUrl(String item) {
        if (item.equals("Airline")) {
            return "airlines";
        } else if (item.equals("Shopping")) {
            return "shopping";
        } else if (item.equals("Business")) {
            return "business";
        } else if (item.equals("Real Estate")) {
            return "realestate";
        } else if (item.equals("Money Exchange")) {
            return "moneyexchangers";
        } else if (item.equals("Car Rentals")) {
            return "carrentals";
        } else if (item.equals("Embasies & Consulates")) {
            return "embassy";
        } else if (item.equals("Tour & Travel")) {
            return "tourandtravel";
        } else if (item.equals("Events")) {
            return "events";
        } else if (item.equals("Eating Out")) {
            return "eatingout";
        } else if (item.equals("Stay/Hotels")) {
            return "hotels";
        } else if (item.equals("nightlife")) {
            return "airlines";
        } else if (item.equals("Spa")) {
            return "spa";
        } else if (item.equals("Things To Do")) {
            return "thingstodo";
        } else if (item.equals("Emergencies")) {
            return "emergencies";
        }
        return null;
    }
    public  void  animationInit(){
        rotateOne = AnimationUtils.loadAnimation(activity, R.anim.rotate_one);
        rotateOne.setAnimationListener(this);
        rotateTwo= AnimationUtils.loadAnimation(activity, R.anim.rotate_two);
        rotateTwo.setAnimationListener(this);
        rotateThree= AnimationUtils.loadAnimation(activity, R.anim.rotate_three);
        rotateThree.setAnimationListener(this);
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
    public void animationLoading(){
        animation_layout.setVisibility(View.VISIBLE);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                ImageView iv1 = (ImageView) view.findViewById(R.id.settings_one);
                ImageView iv2 = (ImageView) view.findViewById(R.id.settings_two);
                ImageView iv3 = (ImageView) view.findViewById(R.id.settings_three);

                iv1.setAnimation(rotateOne);
                iv2.setAnimation(rotateTwo);
                iv3.setAnimation(rotateThree);

                iv1.startAnimation(rotateOne);
                iv2.startAnimation(rotateTwo);
                iv3.startAnimation(rotateThree);
            }
        };
        Handler mHandler = new Handler();
        mHandler.postDelayed(runnable, 100);

    }

    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.lin_lay_call:
                if(phone_list.size()>0)
                {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = activity.getLayoutInflater();

                    View dialogView = inflater.inflate(R.layout.mobilenumberlistingadapter_layout, null);

                    RecyclerView recyclerView = (RecyclerView) dialogView.findViewById(R.id.recyclerView);
                    recyclerView.setLayoutManager(new LinearLayoutManager(activity));
                    MobileNumberListingAdapter adapter = new MobileNumberListingAdapter(activity, phone_list);
                    recyclerView.setAdapter(adapter);

                    dialogBuilder.setView(dialogView);

                    AlertDialog alertDialog = dialogBuilder.create();
//                    alertDialog.getWindow().setLayout(200, 400);
                    alertDialog.show();
                }
                else
                {
                    Toast.makeText(activity, "Not available", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.lin_lay_location:
                break;
            case R.id.lin_lay_review:
                break;
            case R.id.lin_lay_gps:
                String Lat=latitude;
                String Long=longitude;
                if(Lat==null) Lat="";
                if(Lat=="") Lat="0";
                if(Long==null) Long="";
                if(Long=="") Long="0";
                double latitude= Double.parseDouble(Lat);
                double longitude= Double.parseDouble(Long);
                Intent navigation = new Intent(Intent.ACTION_VIEW, Uri
                        .parse("http://maps.google.com/maps?saddr="
                                + latitude + ","
                                + longitude + "&daddr="
                                + latitude + "," + longitude));
                activity.startActivity(navigation);
                break;
            case R.id.lin_lay_site:

            String website_url=website;

            if(website_url == null)website_url="";
            if(!website_url.equalsIgnoreCase(""))
            {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(website_url));
                activity.startActivity(browserIntent);
            }
            else
            {
                Toast.makeText(activity,"Not available",Toast.LENGTH_SHORT).show();
            }

                break;
            default:
                break;
        }
    }
    @Override
    public void onGetWriteReviewResponse(boolean status) {
        if (status) {
            LogUtility.Log(TAG, "onGetWriteReviewResponse:success", null);
            Toast.makeText(activity,"Review has been submitted for approval!",Toast.LENGTH_LONG).show();

        } else {
            LogUtility.Log(TAG, "onGetWriteReviewResponse:failure", null);

        }

    }

    @Override
    public void onAddPlanResponse(boolean status) {
        if (status) {

            //    LogUtility.Log(TAG, "onAddPlanResponse: :success", null);
            Toast.makeText(activity,"Plan Added",Toast.LENGTH_SHORT).show();

        } else {
            //    LogUtility.Log(TAG, "onAddPlanResponse: :failure", null);
            Toast.makeText(activity,"Plan Already Added",Toast.LENGTH_SHORT).show();;
        }
    }

    public void displayAlertDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = activity.getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.mobilenumberlistingadapter_layout, null);

        RecyclerView recyclerView = (RecyclerView) dialogView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        MobileNumberListingAdapter adapter = new MobileNumberListingAdapter(activity, phone_list);
        recyclerView.setAdapter(adapter);

        dialogBuilder.setView(dialogView);

        AlertDialog alertDialog = dialogBuilder.create();
//                    alertDialog.getWindow().setLayout(200, 400);
        alertDialog.show();
    }
}
