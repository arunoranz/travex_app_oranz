package org.technomobs.travex.Activity.Fragment;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.facebook.cache.common.SimpleCacheKey;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;

import java.util.ArrayList;

/**
 * Created by oranz-pc2 on 2/4/16.
 */
public class Essential_Viewpager_Adapter extends PagerAdapter {
    Activity act;
    View layout;
    SimpleDraweeView simpleDraweeView;
    ArrayList<String> imgurl;
    public Essential_Viewpager_Adapter(Activity activity,ArrayList<String> imagelist) {
        // TODO Auto-generated constructor stub
       act=activity;
        imgurl=imagelist;
    }
    @Override
    public Object instantiateItem(View container, int position) {
        // TODO Auto-generated method stub
        try
        {
            LayoutInflater inflater = (LayoutInflater) act
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            layout = inflater.inflate(R.layout.essential_viewpager_adapter, null);
            //Fresco.initialize(act);
            simpleDraweeView= (SimpleDraweeView) layout.findViewById(R.id.sdvImage);
            if(imgurl!=null) {
                if (!imgurl.get(position).equals("")) {
                    Uri uri = Uri.parse(imgurl.get(position));
                    Fresco.getImagePipeline().evictFromMemoryCache(uri);
                    Fresco.getImagePipelineFactory().getMainDiskStorageCache().remove(new SimpleCacheKey(uri.toString()));
                    Fresco.getImagePipelineFactory().getSmallImageDiskStorageCache().remove(new SimpleCacheKey(uri.toString()));
                    ImageRequest request = ImageRequest.fromUri(uri);

                    DraweeController controller = Fresco.newDraweeControllerBuilder()
                            .setImageRequest(request)
                            .setOldController(simpleDraweeView.getController()).build();
                    //Log.e(TAG, "ImagePath uri " + img_uri);

                    simpleDraweeView.setController(controller);
                }
            }
//            else {
//                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.travex_grey128).build();
//                simpleDraweeView.setImageURI(imageRequest.getSourceUri());
//            }


        }
        catch(Exception ex)
        {

        }
        ((ViewPager) container).addView(layout, 0);
        return layout;
    }
    @Override
    public int getCount() {
        return imgurl.size();
    }
    @Override
    public void destroyItem(View arg0, int arg1, Object arg2) {
        ((ViewPager) arg0).removeView((View) arg2);
    }
    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == ((View) arg1);
    }
    @Override
    public Parcelable saveState() {
        return null;
    }
}
