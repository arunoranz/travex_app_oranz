package org.technomobs.travex.Activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import org.technomobs.travex.Activity.Fragment.ExceptionHandler;
import org.technomobs.travex.Activity.Fragment.CitySuggestFragment;
import org.technomobs.travex.Activity.Fragment.CityVisitFragment;
import org.technomobs.travex.Activity.Fragment.SplashFragment;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;
import org.technomobs.travex.Service.LocationService;
import org.technomobs.travex.Utillity.LogUtility;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = MainActivity.class.getSimpleName();

    FragmentTransaction mFragmentTransaction = null;
    FragmentManager mFragmentManager = null;
    SplashFragment mSplashFragment = null;
    CitySuggestFragment mSend_city_suggession_fragment = null;
    CityVisitFragment mSendCityVisit = null;
    TravexApplication mTravexApplication = null;
    public static final int LOCATION_PERMISSION_GROUP = 100;
    public static final int SETTINGS_APP_BACK = 101;
    AlertDialog mAlertDialog = null;
    PreferenceManager mPreferenceManager = null;
    public static ActivityManager sActivityManager = null;
    public static final String SERVICE_NAME = "org.technomobs.travex.Service.LocationService";
    LocationManager mLocationManager = null;
    AlertDialog mAlertDialog_gps = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        LogUtility.Log(TAG, "onCreate", null);
        mTravexApplication=TravexApplication.getInstance();
        mSplashFragment = new SplashFragment();
        mSendCityVisit = new CityVisitFragment();
        mSend_city_suggession_fragment = new CitySuggestFragment();
        mFragmentManager = getSupportFragmentManager();
        sActivityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        mPreferenceManager = new PreferenceManager(this);
        if (!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            mAlertDialog_gps = new AlertDialog.Builder(this).setTitle("Gps Warning")
                    .setCancelable(false)
                    .setMessage("You have to switch on the GPS in settings manually,in order to proceed")
                    .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), 90);


                        }
                    })
                    .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            goAhead();
                        }
                    })
                    .create();
            mAlertDialog_gps.show();
        } else {
         //   Toast.makeText(MainActivity.this, "Gps is on", Toast.LENGTH_LONG).show();
            goAhead();
        }







        /*Intent fake = new Intent(MainActivity.this, SignUpLoginMasterActivity.class);
        startActivity(fake);*/
       /* if (!isLocationServiceStarted()) {
            LogUtility.Log(TAG, "onCreate:location service not yet started:going to start", null);
            startLocationService();
        } else {
            LogUtility.Log(TAG, "onCreate:location service  started", null);

        }*/


    }

    @Override
    public void onBackPressed() {
        LogUtility.Log(TAG, "onBackPressed", null);

        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        LogUtility.Log(TAG, "onDestroy", null);

        super.onDestroy();
    }

    @Override
    protected void onPause() {
        LogUtility.Log(TAG, "onPause", null);

        super.onPause();
    }

    @Override
    protected void onRestart() {
        LogUtility.Log(TAG, "onRestart", null);

        super.onRestart();
    }

    @Override
    protected void onResume() {
        LogUtility.Log(TAG, "onResume", null);

        super.onResume();
    }

    @Override
    protected void onStart() {
        LogUtility.Log(TAG, "onStart", null);
        super.onStart();
    }

    @Override
    protected void onStop() {
        LogUtility.Log(TAG, "onStop", null);

        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        LogUtility.Log(TAG, "onActivityResult", null);

        if (requestCode == SETTINGS_APP_BACK) {

            LogUtility.Log(TAG, "onActivityResult", null);

            if (checkLocationPermissionRuntime()) {
                setLocationPermissionSetOrNot(true);

                if (!isLocationServiceStarted()) {
                    startLocationService();

                } else {
                    LogUtility.Log(TAG, "Service already started", null);
                }
            } else {
                setLocationPermissionSetOrNot(false);


            }
            load_Splash_fragment();


        } else if (requestCode == 90) {

            LogUtility.Log(TAG, "onActivityResult:gps", null);
            if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                LogUtility.Log(TAG, "onActivityResult:gps:enabled", null);

            } else {
                LogUtility.Log(TAG, "onActivityResult:gps:disabled", null);

            }
            goAhead();



        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void load_Splash_fragment() {
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.add(R.id.container, mSplashFragment);
        mFragmentTransaction.addToBackStack(null);
        mFragmentTransaction.commitAllowingStateLoss();


    }

    public void load_Send_City_Suggession_fragment() {
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.container, mSend_city_suggession_fragment);
        mFragmentTransaction.addToBackStack(null);

        mFragmentTransaction.commitAllowingStateLoss();


    }

    public void load_Send_City_Visit_fragment() {
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.container, mSendCityVisit);
        mFragmentTransaction.addToBackStack(null);

        mFragmentTransaction.commitAllowingStateLoss();


    }

    public void startSignUp_LoginActivity() {
        Intent mIntentForStart = new Intent(MainActivity.this, SignUpLoginMasterActivity.class);
        startActivity(mIntentForStart);
        finish();


    }

    public void startProfileActivity() {
        //Intent mIntentForStart = new Intent(MainActivity.this, SplashFragment.class);
//		Intent mIntentForStart = new Intent(MainActivity.this, AfterloginKnowncity.class);
        Intent mIntentForStart = new Intent(MainActivity.this, AfterloginKnowncity.class);
        startActivity(mIntentForStart);
        finish();
    }

    /**
     *
     */
    public void startLocationService() {
new Thread(new Runnable() {
    @Override
    public void run() {
        Intent mIntentForLocation = new Intent(MainActivity.this, LocationService.class);
        startService(mIntentForLocation);
		//Intent mIntentForLocation = new Intent(this, LocationService.class);
        //startService(mIntentForLocation);
    }
}).start();


    }

    @TargetApi(23)

    public void setLocationPermission() {
        if (!checkLocationPermissionRuntime()) {
            LogUtility.Log(TAG, "setLocationPermission:Location permission is going to set", null);

            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_GROUP);

        }

    }

    @TargetApi(23)
    public boolean checkLocationPermissionRuntime() {
        if ((checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) || (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            LogUtility.Log(TAG, "checkLocationPermissionRuntime:Location permission not set yet", null);

            return false;
        }
        return true;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {


        if (requestCode == LOCATION_PERMISSION_GROUP) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                LogUtility.Log(TAG, "onRequestPermissionsResult:Location permission granted", null);

                if (!isLocationServiceStarted()) {
                    startLocationService();

                } else {
                    LogUtility.Log(TAG, "Service already started", null);
                }
                load_Splash_fragment();

            } else {
                LogUtility.Log(TAG, "onRequestPermissionsResult:Location permission rejected", null);
                alertDialogueForPermissions();

            }


        }


        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void setLocationPermissionSetOrNot(boolean status) {
        mPreferenceManager.setLocationPermissionSetOrNot(status);
    }

    /**
     *
     */
    public void alertDialogueForPermissions() {
        mAlertDialog = new AlertDialog.Builder(MainActivity.this)
                .setTitle("Location Permission")
                .setMessage("User have to enable the permissions for location to go ahead ")
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        load_Splash_fragment();
                        setLocationPermissionSetOrNot(false);
                    }
                })


                .setNegativeButton("go to settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        showInfoScreenOfNeyya();
                    }
                })
                .create();
        mAlertDialog.show();


    }

    /**
     *
     */
    public void showInfoScreenOfNeyya() {

        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    public boolean isCurrentLocationInServer() {

        return mPreferenceManager.getCurrentLocInCityListIsAvailable();


    }

    public boolean isCurrentLocationGetTracked() {


        if (mPreferenceManager.getCurrentLocalityName() != null) {
            LogUtility.Log(TAG, "isCurrentLocationGetTracked:current location tracked", null);
            return true;
        } else {
            LogUtility.Log(TAG, "isCurrentLocationGetTracked:current location not tracked and is null", null);

            return false;
        }

    }

    /**
     * @return
     */
    public boolean isLocationServiceStarted() {
        LogUtility.Log(TAG, "isLocationServiceStarted:service name", LocationService.class.getName());
        List<ActivityManager.RunningServiceInfo> mRunningServices = sActivityManager.getRunningServices(Integer.MAX_VALUE);
        for (ActivityManager.RunningServiceInfo service : mRunningServices) {


            if (service.service.getClassName().equals(SERVICE_NAME)) {


                return true;

            }


        }


        return false;
    }

    public void goAhead() {
        LogUtility.Log(TAG,"goAhead",null);
//           startProfileActivity();
         // mTravexApplication.logOutFacebook();
        if (mPreferenceManager.getLogin_Type() != null) {

            if ((mPreferenceManager.getLogin_Type().equals(TravexApplication.TRAVEX_LOGIN)) ||
                    (mPreferenceManager.getLogin_Type().equals(TravexApplication.FACEBOOK_LOGIN)) ||
                    (mPreferenceManager.getLogin_Type().equals(TravexApplication.GOOGLE_LOGIN))||
                    (mPreferenceManager.getLogin_Type().equals(TravexApplication.GUEST_LOGIN))


                    ) {
                LogUtility.Log(TAG, " login already:", "type:" + mPreferenceManager.getLogin_Type());
                startProfileActivity();

            }
            else
            {
                LogUtility.Log(TAG, " login already:", "type:" + mPreferenceManager.getLogin_Type());
            }


        } else {

            LogUtility.Log(TAG, " login not made:", null);

            if (mTravexApplication.isDeviceBuildIsMarsmelloworNot())

            {
                LogUtility.Log(TAG, "device android version is marshmello", null);
                if (checkLocationPermissionRuntime()) {
                    setLocationPermissionSetOrNot(true);

                    if (!isLocationServiceStarted()) {
                        startLocationService();

                    } else {
                        LogUtility.Log(TAG, "Service already started", null);
                    }
                    load_Splash_fragment();
                } else {
                    setLocationPermissionSetOrNot(false);

                    setLocationPermission();
                }
            } else {
                if (!isLocationServiceStarted()) {
                    startLocationService();

                } else {
                    LogUtility.Log(TAG, "Service already started", null);
                }
                load_Splash_fragment();

            }


        }
    }

}
