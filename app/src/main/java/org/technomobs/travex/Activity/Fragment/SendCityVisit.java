package org.technomobs.travex.Activity.Fragment;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import org.json.JSONObject;
import org.technomobs.travex.Activity.MainActivity;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.Controller.JsonObjectMaker;
import org.technomobs.travex.Controller.NetworkManager;
import org.technomobs.travex.Controller.NetworkOptions;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.ListCreatorForNameValuePairRequest;
import org.technomobs.travex.Utillity.LogUtility;

import java.util.ArrayList;

//import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by technomobs on 22/2/16.
 */
public class SendCityVisit extends Fragment {
    public static final String TAG = SendCityVisit.class.getSimpleName();
    ArrayList<String> mArrayListForCityNames = null;

    ArrayAdapter<String> mArrayAdapter = null;
    //CircleImageView profile_image;

    static NetworkManager mNetworkManager = null;
    JsonObjectMaker mJsonObjectMaker = null;
    PreferenceManager mPreferenceManager = null;
    ListCreatorForNameValuePairRequest mListCreatorForNameValuePairRequest = null;

    /**
     * This Button for submitting the city which user wanted to visit on to current city location in preference
     */
    Button mButton_Submit = null;

    /**
     * This view for entering cityname on which user needed to visit
     */
    AutoCompleteTextView mEditText_CityName = null;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onActivityCreated", null);

        mJsonObjectMaker = new JsonObjectMaker(getActivity());
        mJsonObjectMaker = new JsonObjectMaker(getActivity());
        mNetworkManager = NetworkManager.getSingleInstance(getActivity());
        mPreferenceManager = new PreferenceManager(getActivity());
        mListCreatorForNameValuePairRequest = new ListCreatorForNameValuePairRequest(getActivity());
      //  mNetworkManager.setOnGetCitySuggestListener(getActivity().getApplicationContext());

        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        LogUtility.Log(TAG, "onAttach", null);
        mPreferenceManager = new PreferenceManager(getActivity());
        mArrayListForCityNames = new ArrayList<String>();
        mArrayListForCityNames = mPreferenceManager.getSupportedCountryNames();
        mArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.city_dropdown_layout, mArrayListForCityNames);

        super.onAttach(context);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        LogUtility.Log(TAG, "onConfigurationChanged", null);

        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onCreate", null);

        super.onCreate(savedInstanceState);

//        //////
//        new AlertDialog.Builder(getActivity().getApplicationContext())
//                .setTitle("Delete entry")
//                .setMessage("Are you sure you want to delete this entry?")
//                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        // continue with delete
////                        MainActivity myActivity = (MainActivity) getActivity();
////                        if (myActivity instanceof MainActivity) {
////                            myActivity.load_Send_City_Visit_fragment();
////                        }
//                    }
//                })
//                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        // do nothing
//                        postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.CITY_SUGGESTION_URL, getjsonObject(TravexApplication.SUGGEST_CITY_TAG),
//                                TravexApplication.REQUEST_ID_SUGGEST_CITY);
//                    }
//                })
//
//                .show();
//                ///////
    }

    @Override
    public void onDestroy() {
        LogUtility.Log(TAG, "onDestroy", null);

        super.onDestroy();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onCreateView", null);
        View view = inflater.inflate(R.layout.send_city_visit_fragment_layout, container, false);
        //////


//        AlertDialog alert = new AlertDialog.Builder(getActivity()).create();
//
//        alert.setMessage("Send Your Current Location To Server");
//        alert.setCancelMessage(null);
//
//        alert.setButton("OK", new DialogInterface.OnClickListener() {
//
//            public void onClick(DialogInterface dialog, int which) {
//                //do somthing or dismiss dialog by                dialog.dismiss();
//
//            }
//        });
//        alert.setButton2("cancel", new DialogInterface.OnClickListener() {
//
//            public void onClick(DialogInterface dialog, int which) {
//                //do somthing or dismiss dialog by                dialog.dismiss();
//                postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.CITY_SUGGESTION_URL, getjsonObject(TravexApplication.SUGGEST_CITY_TAG),
//                        TravexApplication.REQUEST_ID_SUGGEST_CITY);
//
//            }
//        });
//
//        alert.show();

//
//        new AlertDialog.Builder(getActivity().getApplicationContext())
//                .setTitle("Delete entry")
//                .setMessage("Are you sure you want to delete this entry?")
//                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        // continue with delete
////                        MainActivity myActivity = (MainActivity) getActivity();
////                        if (myActivity instanceof MainActivity) {
////                            myActivity.load_Send_City_Visit_fragment();
////                        }
//                    }
//                })
//                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        // do nothing
//                        postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.CITY_SUGGESTION_URL, getjsonObject(TravexApplication.SUGGEST_CITY_TAG),
//                                TravexApplication.REQUEST_ID_SUGGEST_CITY);
//                    }
//                })
//
//                .show();
        ///////
        mButton_Submit = (Button) view.findViewById(R.id.submit);
        mEditText_CityName = (AutoCompleteTextView) view.findViewById(R.id.city_name);
        mEditText_CityName.setThreshold(2);
        mEditText_CityName.setAdapter(mArrayAdapter);
      //  profile_image=(CircleImageView)view.findViewById(R.id.profile_image);
//        profile_image.setImageResource(R.mipmap.avtar);

        mButton_Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEditText_CityName.getText().toString().trim().length() > 0) {
                    //updateCurrentLocationDetailsInPreference(mEditText_CityName.getText().toString());
                }
            }
        });
        return view;
    }

//    public void updateCurrentLocationDetailsInPreference(String localityname) {
//
//        /**
//         * findout the index of locality name from preference arraylist ,to locate   another  details of location
//         */
//
//        int index = -1;
//        for (int count = 0; count < mArrayListForCityNames.size(); count++) {
//            if (localityname.equals(mArrayListForCityNames.get(count))) {
//                index = count;
//            }
//
//        }
//        LogUtility.Log(TAG, "updateCurrentLocationDetailsInPreference", "index of current locality in preference list is:" + index+":locality:"+localityname);
//        if (index != -1) {
//            mPreferenceManager.setCurrentLocalityName(localityname);
//            mPreferenceManager.setCurrentcountryName(mPreferenceManager.getSupportedCountryNames().get(index).toString());
//            mPreferenceManager.setCurrentLatitude(mPreferenceManager.getSupportedCountryLatitude().get(index).toString());
//            mPreferenceManager.setCurrentLongitude(mPreferenceManager.getSupportedCountryLongitude().get(index).toString());
//        }
//
//        ((MainActivity) getActivity()).startSignUp_LoginActivity();
//        getActivity().finish();
//
//    }

    @Override
    public void onDestroyView() {
        LogUtility.Log(TAG, "onDestroyView", null);

        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        LogUtility.Log(TAG, "onDetach", null);

        super.onDetach();
    }

    @Override
    public void onPause() {
        LogUtility.Log(TAG, "onPause", null);

        super.onPause();
    }

    @Override
    public void onResume() {
        LogUtility.Log(TAG, "onResume", null);

        super.onResume();
    }

    @Override
    public void onStart() {
        LogUtility.Log(TAG, "onStart", null);

        super.onStart();
    }

    @Override
    public void onStop() {
        LogUtility.Log(TAG, "onStop", null);

        super.onStop();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onViewCreated", null);

        super.onViewCreated(view, savedInstanceState);
    }
    public void postJsonRequest(int request_type, String url, JSONObject jsonObject, final int request_id) {
        mNetworkManager.postJsonRequest(request_type, url, jsonObject, request_id);


    }

    public JSONObject getjsonObject(String tag) {
        if (tag.equals(TravexApplication.SUGGEST_CITY_TAG)) {

            return getJson(mListCreatorForNameValuePairRequest.getListForCitySuggestionRequest(TravexApplication.EMAIL_ID_CITY_SUGGEST, TravexApplication.CITY_SUGGEST), mListCreatorForNameValuePairRequest.getListForCitySuggestionRequest(mPreferenceManager.getRegisration_Id_response(), mPreferenceManager.getCurrentLocalityName()));

        }
        return null;
    }
    public JSONObject getJson(ArrayList<String> namePair, ArrayList<String> valuePair) {


        return mJsonObjectMaker.getJson(namePair, valuePair);
    }



    public void onGetCitySuggestion(boolean status) {
        LogUtility.Log(TAG, "onGetCitySuggestion", null);
        MainActivity myActivity = (MainActivity) getActivity();
        if (myActivity instanceof MainActivity) {
            myActivity.load_Send_City_Visit_fragment();
        }

        if (status) {
            LogUtility.Log(TAG, "onGetCitySuggestion:saved successfully in server", null);
        } else {
            LogUtility.Log(TAG, "onGetCitySuggestion:not saved successfully in server", null);
        }

    }

}
