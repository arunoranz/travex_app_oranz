package org.technomobs.travex.Activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import org.technomobs.travex.Activity.Fragment.ExceptionHandler;
import org.technomobs.travex.Activity.Fragment.Google_Map_Fragment;
import org.technomobs.travex.Activity.Fragment.ProfileFragment;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.LogUtility;

import java.util.ArrayList;

/**
 * Created by technomobs on 23/2/16.
 */
public class ProfileActivity extends ActionBarActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    public static final String TAG = ProfileActivity.class.getSimpleName();
    PreferenceManager mPreferenceManager = null;
    ProfileFragment mProfileFragment = null;
    FragmentTransaction mFragmentTransaction = null;
    FragmentManager mFragmentManager = null;
    Google_Map_Fragment mGoogle_map_fragment = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        LogUtility.Log(TAG, "onCreate", null);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_master_activity_layout);
        mPreferenceManager = new PreferenceManager(ProfileActivity.this);
        mProfileFragment = new ProfileFragment();
        mGoogle_map_fragment = new Google_Map_Fragment();
        mFragmentManager = getSupportFragmentManager();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        load_profile_master_fragment();

    }

    @Override
    protected void onDestroy() {
        LogUtility.Log(TAG, "onDestroy", null);
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        LogUtility.Log(TAG, "onStop", null);
        super.onStop();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        LogUtility.Log(TAG, "onConfigurationChanged", null);
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        LogUtility.Log(TAG, "onAttachFragment", null);
        super.onAttachFragment(fragment);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        LogUtility.Log(TAG, "onActivityResult", null);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        LogUtility.Log(TAG, "onBackPressed", null);

        Fragment fragment = mFragmentManager.findFragmentById(R.id.container);
        if (fragment instanceof ProfileFragment) {
            LogUtility.Log(TAG, "onBackPressed:profile fragment", null);
        } else if (fragment instanceof Google_Map_Fragment) {
            LogUtility.Log(TAG, "onBackPressed:googlmap fragment", null);
        }


    }

    AppCompatSpinner mAppCompatSpinner = null;
    ImageView mImageView_actionItem = null;
    View view_spinner = null;
    View view_logo = null;
    ArrayAdapter<String> arrayAdapter;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.profile_menu, menu);
        view_spinner = menu.findItem(R.id.country_names).getActionView();
        view_logo = menu.findItem(R.id.travex_logo).getActionView();

        LogUtility.Log(TAG, "onCreateOptionsMenu:", "view_spinner:" + view_spinner);
        LogUtility.Log(TAG, "onCreateOptionsMenu:", "view_logo:" + view_logo);


        mAppCompatSpinner = (AppCompatSpinner) view_spinner.findViewById(R.id.action_spinner);
        LogUtility.Log(TAG, "onCreateOptionsMenu", "mAppCompatSpinner:" + mAppCompatSpinner);
        arrayAdapter = new ArrayAdapter<String>(ProfileActivity.this, android.R.layout.simple_spinner_dropdown_item, mPreferenceManager.getSupportedCountryNames());
        mAppCompatSpinner.setAdapter(arrayAdapter);
        mAppCompatSpinner.setOnItemSelectedListener(this);
        mImageView_actionItem = (ImageView) view_logo.findViewById(R.id.action_logo);
        LogUtility.Log(TAG, "onCreateOptionsMenu", "mImageView_actionItem:" + mImageView_actionItem);
        mImageView_actionItem.setOnClickListener(this);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        LogUtility.Log(TAG, "onPause", null);
        super.onPause();
    }

    @Override
    protected void onResume() {
        LogUtility.Log(TAG, "onResume", null);
        super.onResume();
    }

    @Override
    protected void onStart() {
        LogUtility.Log(TAG, "onStart", null);
        super.onStart();
    }

    @Override
    protected void onResumeFragments() {
        LogUtility.Log(TAG, "onResumeFragments", null);
        super.onResumeFragments();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        LogUtility.Log(TAG, "onRequestPermissionsResult", null);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void load_profile_master_fragment() {
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.add(R.id.container, mProfileFragment);
        mFragmentTransaction.commitAllowingStateLoss();


    }

    public void load_googlemap_fragment() {
        LogUtility.Log(TAG, "load_googlemap_fragment", null);
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.add(R.id.fragment_container, mGoogle_map_fragment);
        mFragmentTransaction.commitAllowingStateLoss();


    }

    public void startSignUp_LoginActivity() {
        Intent mIntentForStart = new Intent(ProfileActivity.this, SignUpLoginMasterActivity.class);
        startActivity(mIntentForStart);
        finish();


    }

    public boolean isFragmentalreadyAddedInDrawersContainer(Fragment f, String tag) {


        Fragment mFragment = mFragmentManager.findFragmentById(R.id.fragment_container);
        LogUtility.Log(TAG, "isFragmentalreadyAddedInDrawersContainer:", "fragment in container is " + mFragment);
        LogUtility.Log(TAG, "isFragmentalreadyAddedInDrawersContainer:", "fragment as parameter is" + f);


        if (tag.equals(TravexApplication.MAP_FRAGMENT_TAG)) {
            if ((mFragment instanceof Google_Map_Fragment) && (f instanceof Google_Map_Fragment)) {
                return true;
            }
            return false;
        }


        return false;
    }


    public String fetchItemidFortheSuggestion(String suggestion) {
        LogUtility.Log(TAG,"fetchItemidFortheSuggestion",null);

        String id = null;
        int index = 0;
        ArrayList<String> suggestions = mPreferenceManager.getSearchResultsKeyWord_suggestion();
        index = suggestions.lastIndexOf(suggestion);
        id = mPreferenceManager.getSearchResultsKeyWord_id().get(index);


        return id;


    }

    public String fetchCountryIdForCountry(String country) {
        String countryId = null;
        int index = 0;
        ArrayList<String> countrynames = mPreferenceManager.getSupportedCountryNames();
        index = countrynames.lastIndexOf(country);
        LogUtility.Log(TAG, "fetchCountryIdForCountry", "index:" + index);

        countryId = mPreferenceManager.getSupportedCountryId().get(index);
        LogUtility.Log(TAG, "fetchCountryIdForCountry", "countryId:" + countryId);


        /*for(String s:countrynames)
        {
            if(s.equals(country))
            {
                countrynames.lastIndexOf()
            }


        }*/


        return countryId;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.action_logo) {


            if (!isFragmentalreadyAddedInDrawersContainer(mGoogle_map_fragment, TravexApplication.MAP_FRAGMENT_TAG)) {
                LogUtility.Log(TAG, "onClick:action logo clicked:loading google map fragment", null);

                load_googlemap_fragment();


            }
        }
    }

    String country = null;

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        country = mAppCompatSpinner.getItemAtPosition(position).toString();
        LogUtility.Log(TAG, "onItemClick:action spinner:", "item:" + country);

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public String getSelectedCountry() {
        if (country == null) {

            country = mAppCompatSpinner.getItemAtPosition(0).toString();
        }
        return country;

    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        //LogUtility.Log(TAG, "dispatchKeyEvent", "key code :key code:" + event.getKeyCode() + ",keycode search int value:" + KeyEvent.KEYCODE_SEARCH);


        if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
            if (event.getAction() == KeyEvent.ACTION_UP) {
                LogUtility.Log(TAG, "dispatchKeyEvent", "key code enter");
                mGoogle_map_fragment.onEnterKeyClick();
            }

        }

        return true;
    }
}
