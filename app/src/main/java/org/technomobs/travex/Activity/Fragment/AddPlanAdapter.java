package org.technomobs.travex.Activity.Fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.DateFormatFunction;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Abhi on 09-03-2016.
 */
public class AddPlanAdapter  extends RecyclerView.Adapter<AddPlanAdapter.ViewHolder> {

    private Context context;
    LayoutInflater mInflater;
    String selected_category,selected_categoryid;
    ArrayList<CategoryListItems> categoryItemsnew;
    private String apptheme,category_name;
    public int checki = 0;
    int count = 0;
    Activity act;
    ArrayList<String> dayList=new ArrayList<String>();
    ArrayList<String> departureList=new ArrayList<String>();
    ArrayList<String> durationList=new ArrayList<String>();
    DateFormatFunction df = new DateFormatFunction();
    ViewHolder vh;
    public AddPlanAdapter(Activity activity,Context context,ArrayList<String> dayList,ArrayList<String> departureList,ArrayList<String> durationList) {
        this.context = context;
        this.act=activity;
        this.dayList=dayList;
        this.departureList=departureList;
        this.durationList=durationList;
        mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.addplan_adapter, parent, false);

        vh = new ViewHolder(v);
        return vh;
    }


    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {
        return this.dayList.size();

    }


    public Object getItem(int position) {
        return departureList.get(position);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        {
            String TimeFrom="";
            String TimeTo="";
            if(MapShowingFragment.selected_category.equalsIgnoreCase("Airline"))
            {
                TimeFrom =  departureList.get(position);
                Date DepTime =  df.Dateformat_as_date("h:mm:ss", "h:mm:ss", TimeFrom);

                String durationtime_array[] =  durationList.get(position).split(" ");
                String durationtime = durationtime_array[0];
                int durtime = Integer.parseInt(durationtime.trim());
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(DepTime);
                calendar.add(Calendar.HOUR, durtime);
                Date Arrivingtime = calendar.getTime();
                int hh=Arrivingtime.getHours();
                int mm=Arrivingtime.getMinutes();
                int ss=Arrivingtime.getSeconds();
                TimeTo = String.format("%02d", hh)+":"+String.format("%02d", mm)+":"+String.format("%02d", ss);
            }
            else
            {
                TimeFrom =  departureList.get(position);
                TimeTo =durationList.get(position);
            }

            holder.plantext.setText(" From "+ TimeFrom + " To "+ TimeTo);
            //holder.plantext.setText(dayList.get(position));
            holder.add_plan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "Add To Plan", Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView plantext,add_plan;
        SimpleDraweeView icon;
        public ViewHolder(View v) {
            super(v);
            plantext=(TextView)v.findViewById(R.id.plan_text);
            add_plan=(TextView)v.findViewById(R.id.add_plan);

        }
    }

}
