package org.technomobs.travex.Activity.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Environment;

import org.technomobs.travex.Activity.SignUpLoginMasterActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Calendar;

public class ExceptionHandler implements
		Thread.UncaughtExceptionHandler {
	String mydate = java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
	Calendar c = Calendar.getInstance(); 
	private final Activity myContext;
	private final String LINE_SEPARATOR = "\n";

	public ExceptionHandler(Activity context) {
		myContext = context;
	}

	public void uncaughtException(Thread thread, Throwable exception) {
		StringWriter stackTrace = new StringWriter();
		exception.printStackTrace(new PrintWriter(stackTrace));
		StringBuilder errorReport = new StringBuilder();
		errorReport.append("************ CAUSE OF ERROR ************\n\n");
		errorReport.append("Date : ");
		errorReport.append(mydate+"\n");		
		errorReport.append(stackTrace.toString());
		errorReport.append("\n************ DEVICE INFORMATION ***********\n");
		errorReport.append("Brand: ");
		errorReport.append(Build.BRAND);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("Device: ");
		errorReport.append(Build.DEVICE);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("Model: ");
		errorReport.append(Build.MODEL);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("Id: ");
		errorReport.append(Build.ID);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("Product: ");
		errorReport.append(Build.PRODUCT);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("\n************ FIRMWARE ************\n");
		errorReport.append("SDK: ");
		errorReport.append(Build.VERSION.SDK);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("Release: ");
		errorReport.append(Build.VERSION.RELEASE);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("Incremental: ");
		errorReport.append(Build.VERSION.INCREMENTAL);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("--------------------------------------------------------------------------------------------\n");
		File sdCard = Environment.getExternalStorageDirectory();  
		 File dir = new File (sdCard.getAbsolutePath() + "/Travex/Logs");
		 dir.mkdirs();  
		 File file = new File(dir, "logcat.txt");  
		 if(!file.exists()){
 			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
 		}
		 try {    
			  //to write logcat in text file  
			  FileOutputStream fOut = new FileOutputStream(file,true);  
			  OutputStreamWriter osw = new OutputStreamWriter(fOut);			  
			  osw.append(errorReport.toString());		  			                
			        osw.flush();  
			        osw.close();  

			 } catch (FileNotFoundException e) {  
			  e.printStackTrace();  
			 } catch (IOException e) {  
			  e.printStackTrace();  
			 }
		Intent intent = new Intent(myContext, SignUpLoginMasterActivity.class);
		intent.putExtra("error", errorReport.toString());
		myContext.startActivity(intent);

		android.os.Process.killProcess(android.os.Process.myPid());
		System.exit(1000);
	}

}