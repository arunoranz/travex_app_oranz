package org.technomobs.travex.Activity.Fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import org.technomobs.travex.R;

import java.util.ArrayList;

/**
 * Created by ARUN on 04/01/2016.
 */
public class ArticleDetailsImageAdapter extends RecyclerView.Adapter<ArticleDetailsImageAdapter.ViewHolder> {
    ScrollView child_scroll;
    LayoutInflater mInflater;
    Context context ;
    ArrayList<String> tv_header_list, tv_details_list,sdv_detailsimage_list;

    public ArticleDetailsImageAdapter(Context context, ArrayList<String> sdv_imagelist, ArrayList<String> tv_details) {
        this.context = context;
        sdv_detailsimage_list=sdv_imagelist;
        tv_details_list=tv_details;
        mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

    }
//    @Override
//    public int getCount() {
//        return tv_header_list.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return null;
//    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder;
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.articleimgslide, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;

    }

//    @Override
//    public void onBindViewHolder(ViewHolder holder, int position) {
//
//        holder = new ViewHolder();
//
//        //convertView = mInflater.inflate(R.layout.articleadapterview, null);
//        holder.sdv_detailsimage= (SimpleDraweeView)convertView.findViewById(R.id.sdv_detailsimage);
//        holder.tv_header = (TextView) convertView.findViewById(R.id.tv_header);
//        holder.tv_details = (TextView) convertView.findViewById(R.id.tv_details);
//        //convertView.setTag(holder);
//
//        viewHolder.txtViewTitle.setText(itemsData[position].getTitle());
//        viewHolder.imgViewIcon.setImageResource(itemsData[position].getImageUrl());
//    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData
        //Fresco.initialize(context);

        if(!sdv_detailsimage_list.get(position).equals("")) {
            Uri img_uri = Uri.parse(sdv_detailsimage_list.get(position));


            ImageRequest request = ImageRequest.fromUri(img_uri);

            DraweeController controller = Fresco.newDraweeControllerBuilder()
                    .setImageRequest(request)
                    .setOldController(viewHolder.sdv_images.getController()).build();
            //Log.e(TAG, "ImagePath uri " + img_uri);

            viewHolder.sdv_images.setController(controller);
        }
        //viewHolder.sdv_detailsimage.setImageURI(img_uri);


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return sdv_detailsimage_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public SimpleDraweeView sdv_images;
        public TextView tv_details;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            sdv_images= (SimpleDraweeView) itemLayoutView.findViewById(R.id.sdv_images);
            tv_details= (TextView) itemLayoutView.findViewById(R.id.tv_details);
            tv_details.setVisibility(View.VISIBLE);
        }
    }




}
