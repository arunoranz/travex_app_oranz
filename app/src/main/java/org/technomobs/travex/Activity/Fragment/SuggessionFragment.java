package org.technomobs.travex.Activity.Fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONObject;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Controller.Interface.AddSuggestionsResponse;
import org.technomobs.travex.Controller.JsonObjectMaker;
import org.technomobs.travex.Controller.NetworkManager;
import org.technomobs.travex.Controller.NetworkOptions;
import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.Common;
import org.technomobs.travex.Utillity.ListCreatorForNameValuePairRequest;
import org.technomobs.travex.Utillity.LogUtility;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Abhi on 05-04-2016.
 */
public class SuggessionFragment extends Fragment implements AddSuggestionsResponse,View.OnClickListener {
    View rootView;
    static Activity activity;
    static NetworkManager mNetworkManager = null;
    public static final String TAG = TravelEssential.class.getSimpleName();
    JsonObjectMaker mJsonObjectMaker = null;
    ListCreatorForNameValuePairRequest mListCreatorForNameValuePairRequest = null;
    PreferenceManager preferenceManager;
    ArrayList<String> mArrayListForCityNames = new ArrayList<String>();
    ArrayList<String> mArrayListForCityId = new ArrayList<String>();
    ArrayList<String> category_list = new ArrayList<>();
    ArrayList<String> city_id_list = new ArrayList<>();
    String[] categories_items;
    Spinner spinner_category;
    EditText et_suggestion;
    Button btn_submit;

    public static SuggessionFragment newInstance(Activity act){
        SuggessionFragment fragment = new SuggessionFragment();
        activity=act;

        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.suggession_fragment, container, false);
        Common.setupUI(rootView, getActivity());
        initiating_APICredentials();
        Initialization();
        SetValues();

        return rootView;
    }

    public void Initialization()
    {
        categories_items = new String[15];
        categories_items = getResources().getStringArray(R.array.category_items_spinner);
        spinner_category = (Spinner) rootView.findViewById(R.id.spinner_city);
        et_suggestion= (EditText) rootView.findViewById(R.id.et_suggestion);
        btn_submit= (Button) rootView.findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(this);
    }

    public void initiating_APICredentials()
    {
        mJsonObjectMaker = new JsonObjectMaker(getActivity());
        mListCreatorForNameValuePairRequest = new ListCreatorForNameValuePairRequest(getActivity());
        mNetworkManager = NetworkManager.getSingleInstance(activity);
        preferenceManager = new PreferenceManager(getActivity());
        mNetworkManager.setOnAddSuggestionsResponseListener(this);
    }

    public void SetValues()
    {
        category_list.add("Catergory");
        for(int i=0;i<categories_items.length;i++)
        {
            category_list.add(categories_items[i]);
        }

        ArrayAdapter spinnerArrayAdapter = new ArrayAdapter(activity,R.layout.spinner_layout_white,category_list);
        spinner_category.setAdapter(spinnerArrayAdapter);
    }

    public void Call_API()
    {
        int selected_position = spinner_category.getSelectedItemPosition();
        String Suggestion=et_suggestion.getText().toString().trim();
        if(selected_position==0)
        {
            Toast.makeText(activity, "Please select category", Toast.LENGTH_SHORT).show();
        }
        else if(Suggestion.equals(""))
        {
            Toast.makeText(activity, "Please enter suggestion", Toast.LENGTH_SHORT).show();
        }
        else {
            postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.ADD_SUGGESTIONS_URL,
                    getjsonObject(TravexApplication.ADD_SUGGESTIONS_TAG),
                    TravexApplication.REQUEST_ID_ADD_SUGGESTIONS);
        }
    }


    public JSONObject getjsonObject(String tag) {
        String category_item=spinner_category.getSelectedItem().toString();
        String Suggestion=et_suggestion.getText().toString();
        if (tag.equals(TravexApplication.ADD_SUGGESTIONS_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfAddSuggestions
                            (TravexApplication.ADD_SUGGESTIONS_MEMBER_ID,
                                    TravexApplication.ADD_SUGGESTIONS_CATEGORYID,
                                    TravexApplication.ADD_SUGGESTIONS_SUGGESTIONDATA
                            ),
                    mListCreatorForNameValuePairRequest.getListOfAddSuggestions(preferenceManager.getRegisration_Id_response(), MapShowingFragment.getCategory_Id_Tips_For(category_item), Suggestion)

            );

        }
        return null;
    }


    public JSONObject getJson(ArrayList<String> namePair, ArrayList<String> valuePair) {


        return mJsonObjectMaker.getJson(namePair, valuePair);
    }

    public void postJsonRequest(int request_type, final Map<String, String> request_header, String url, JSONObject jsonObject, final int request_id) {

        mNetworkManager.postJsonRequest(request_type, request_header, url, jsonObject, request_id);


    }

    @Override
    public void onAddSuggestionsResponse(boolean status) {
        if (status) {
            spinner_category.setSelection(0);
            et_suggestion.setText("");
            Toast.makeText(activity,"Suggestion Added",Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(activity,"Suggestion Not Added",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_submit:
                Call_API();
                break;
            default:
                break;
        }
    }
}
