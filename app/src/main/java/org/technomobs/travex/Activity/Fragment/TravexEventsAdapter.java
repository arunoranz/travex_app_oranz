package org.technomobs.travex.Activity.Fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONObject;
import org.technomobs.travex.Activity.SignUpLoginMasterActivity;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Controller.Interface.AddPlanResponse;
import org.technomobs.travex.Controller.Interface.DeletePlanResponse;
import org.technomobs.travex.Controller.JsonObjectMaker;
import org.technomobs.travex.Controller.NetworkManager;
import org.technomobs.travex.Controller.NetworkOptions;
import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.DateFormatFunction;
import org.technomobs.travex.Utillity.ListCreatorForNameValuePairRequest;
import org.technomobs.travex.Utillity.LogUtility;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;

/**
 * Created by oranz-pc1 on 29/4/16.
 */
public class TravexEventsAdapter extends RecyclerView.Adapter<TravexEventsAdapter.ViewHolder> implements AddPlanResponse,DeletePlanResponse {

    static Activity activity ;
    ArrayList<String> title_list, desc_list,active_status_list,events_item_id_list,events_item_name_list;
    static NetworkManager mNetworkManager = null;
    PreferenceManager mPreferenceManager = null;
    JsonObjectMaker mJsonObjectMaker = null;
    ListCreatorForNameValuePairRequest mListCreatorForNameValuePairRequest = null;
    DatePickerDialog dpd;
    TimePickerDialog tpd;
    String date_month_year="",Time="",item_id="",item_name="";
    DateFormatFunction df = new DateFormatFunction();
    //OnItemClickListener mItemClickListener;



    public TravexEventsAdapter(Activity act, ArrayList<String> title, ArrayList<String> desc,ArrayList<String> active_status
    ,ArrayList<String> events_item_id,ArrayList<String> events_item_name) {
        activity = act;
        title_list=title;
        desc_list = desc;
        active_status_list=active_status;
        events_item_id_list=events_item_id;
        events_item_name_list=events_item_name;
        initiating_APICredentials();
    }
//    @Override
//    public int getCount() {
//        return tv_header_list.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return null;
//    }
public void initiating_APICredentials() {
    mJsonObjectMaker = new JsonObjectMaker(activity);
    mNetworkManager = NetworkManager.getSingleInstance(activity);
    mPreferenceManager = new PreferenceManager(activity);
    mListCreatorForNameValuePairRequest = new ListCreatorForNameValuePairRequest(activity);
    mNetworkManager.setOnDeletePlanResponseListener(this);
    mNetworkManager.setOnAddPlanResponseListener(this);

}
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder;
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.travexeventsadapter_layout, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;

    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.img_close.setTag(position);
        holder.tv_title.setText(title_list.get(position));
        if(desc_list!=null) {
            holder.tv_desc.setText(desc_list.get(position));
        }
        if(active_status_list!=null)
        {
            String status = active_status_list.get(position);
            if(status.equals("2") || !status.equals("4"))
            {
                holder.img_close.setImageResource(R.drawable.plus32);
            }
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return title_list.size();
    }

    @Override
    public void onAddPlanResponse(boolean status) {
        if (status) {

        } else {

        }
    }

    @Override
    public void onDeletePlanResponse(boolean status) {
        if (status) {

        } else {

        }
    }


    public JSONObject getjsonObject(String tag) {
        if (tag.equals(TravexApplication.UPDATE_PLAN_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfUpdateplan
                            (TravexApplication.UPDATE_PLAN_USER_ID,
                                    TravexApplication.UPDATE_PLAN_ID,
                                    TravexApplication.UPDATE_PLAN_DATE


                            ),
                    mListCreatorForNameValuePairRequest.
                            getListOfUpdateplan(mPreferenceManager.getRegisration_Id_response(),
                                    item_id, date_month_year.trim())

            );

        }
        else if (tag.equals(TravexApplication.ADD_PLAN_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfAddplan
                            (TravexApplication.ADD_PLAN_ITEM_ID,
                                    TravexApplication.ADD_PLAN_ITEM_NAME,
                                    TravexApplication.ADD_PLAN_TIME,
                                    TravexApplication.ADD_PLAN_USER_ID,
                                    TravexApplication.ADD_PLAN_DATE,
                                    TravexApplication.ADD_PLAN_CATEGORY
                            ),
                    mListCreatorForNameValuePairRequest.getListOfAddplan(item_id,item_name, Time, mPreferenceManager.getRegisration_Id_response(),date_month_year.trim(), MapShowingFragment.selected_category)

            );
        }
        else if (tag.equals(TravexApplication.DELETE_PLAN_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfDeleteplan
                            (

                                    TravexApplication.DELETE_USER_ID, TravexApplication.DELETE_PLAN_ID

                            ),
                    mListCreatorForNameValuePairRequest.
                            getListOfDeleteplan(mPreferenceManager.getRegisration_Id_response(), "1")

            );

        }
        return null;
    }

    public void postJsonRequest(int request_type, final Map<String, String> request_header, String url, JSONObject jsonObject, final int request_id) {

        mNetworkManager.postJsonRequest(request_type, request_header, url, jsonObject, request_id);


    }


    public JSONObject getJson(ArrayList<String> namePair, ArrayList<String> valuePair) {


        return mJsonObjectMaker.getJson(namePair, valuePair);
    }

//    public interface OnItemClickListener {
//        public void onItemClick(View view , int position);
//    }
//
//    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
//        this.mItemClickListener = mItemClickListener;
//    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        final TextView tv_title, tv_desc;
        final ImageView img_close;
        final RelativeLayout rel_main;


        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tv_title = (TextView) itemLayoutView.findViewById(R.id.tv_title);
            tv_desc = (TextView) itemLayoutView.findViewById(R.id.tv_desc);
            img_close = (ImageView) itemLayoutView.findViewById(R.id.img_close);
            rel_main = (RelativeLayout) itemLayoutView.findViewById(R.id.rel_main);


        }

        @Override
        public void onClick(View v) {
            int position= (int)img_close.getTag();

            switch(v.getId())
            {
                case R.id.img_close:
                    String status = active_status_list.get(position);
                    item_id=events_item_id_list.get(position);
                    item_name=events_item_name_list.get(position);
                    if(status.equals("2") || !status.equals("4"))
                    {
                        try {


                            Calendar now = Calendar.getInstance();
                            dpd = DatePickerDialog.newInstance(
                                    Date_callback,
                                    now.get(Calendar.YEAR),
                                    now.get(Calendar.MONTH),
                                    now.get(Calendar.DAY_OF_MONTH)
                            );
                            dpd.setAccentColor(activity.getResources().getColor(R.color.theme));
                            dpd.setMinDate(now);
                            dpd.show(activity.getFragmentManager(), "Datepickerdialog");




                        }
                        catch(Exception ex)
                        {
                            Log.println(0, "DateTimePicker", ex.toString());
                        }
                    }
                    else
                    {

                    }

                    break;
                default:
                    break;
            }


        }


        private DatePickerDialog.OnDateSetListener Date_callback = new DatePickerDialog.OnDateSetListener() {


            @Override
            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                try {
                    int month=monthOfYear+1;
                    date_month_year = year + "-" + month + "-" + dayOfMonth;
                    Calendar now = Calendar.getInstance();
                    tpd = TimePickerDialog.newInstance(
                            Time_callback,
                            now.get(Calendar.HOUR_OF_DAY),
                            now.get(Calendar.MINUTE),
                            false
                    );
                    tpd.setAccentColor(activity.getResources().getColor(R.color.theme));
                    tpd.show(activity.getFragmentManager(), "Timepickerdialog");

                }
                catch(Exception ex)
                {
                    Log.println(0,"Timepickerdialog",ex.toString());
                }
            }
        };


        private TimePickerDialog.OnTimeSetListener Time_callback = new TimePickerDialog.OnTimeSetListener() {

            @Override
            public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
                try {
                    String fff = String.valueOf(hourOfDay+minute+second);
                    String _Time = hourOfDay+":"+minute;
                    Time=df.dateformat("HH:mm","hh:mm a",_Time);

//                    postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.ADD_PLAN_URL,
//                            getjsonObject(TravexApplication.ADD_PLAN_TAG),
//                            TravexApplication.REQUEST_ID_ADD_PLAN);
                    postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.UPDATE_PLAN_URL,
                            getjsonObject(TravexApplication.UPDATE_PLAN_TAG),
                            TravexApplication.REQUEST_ID_UPDATE_PLAN);

                }
                catch (Exception ex)
                {

                }

            }
        };



    }




}
