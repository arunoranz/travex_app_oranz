package org.technomobs.travex.Activity;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import org.technomobs.travex.Activity.Fragment.AboutUsFragment;
import org.technomobs.travex.Activity.Fragment.ArticleDetailsFragment;
import org.technomobs.travex.Activity.Fragment.ArticleFragment;
import org.technomobs.travex.Activity.Fragment.ContactFragment;
import org.technomobs.travex.Activity.Fragment.ExceptionHandler;
import org.technomobs.travex.Activity.Fragment.MapShowingFragment;
import org.technomobs.travex.Activity.Fragment.Myplan;
import org.technomobs.travex.Activity.Fragment.Navadapter;
import org.technomobs.travex.Activity.Fragment.ProfileFirstFragment;
import org.technomobs.travex.Activity.Fragment.ProfileFragmentTwo;
import org.technomobs.travex.Activity.Fragment.SearchResultFragmentSecond;
import org.technomobs.travex.Activity.Fragment.SuggessionFragment;
import org.technomobs.travex.Activity.Fragment.TravelEssential;
import org.technomobs.travex.Activity.Fragment.TravexTipsDetailsFragment;
import org.technomobs.travex.Activity.Fragment.Travelsessential_fragment_second;
import org.technomobs.travex.Activity.Fragment.TravexTipsFragment;
import org.technomobs.travex.Activity.Fragment.searchresultFragment;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;

import java.util.ArrayList;

/**
 * Created by Abhi on 02-03-2016.
 */
public class AfterloginKnowncity extends AppCompatActivity {
    public static final String TAG = AfterloginKnowncity.class.getSimpleName();
    public  static int PLACE_PICKER_REQUEST=1958;
    public static int LOCATION_PICKER_REQUEST = 2;
    ArrayList<String> navDrawerItems;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    Fragment fragment;
    FrameLayout frame;
    private String backStateName;
    private float lastTranslate = 0.0f;
    public static boolean   active_nowbtn=true;
    public static ListView mDrawerList;
    public static Navadapter adapter;
    PreferenceManager preferenceManager;
    private final static String TAG_FRAGMENT = "TAG_FRAGMENT";
    FragmentManager fragmentManager;
    ArrayList<String> mArrayListForCityNames = new ArrayList<String>();
    ArrayList<String> mArrayListForCityId = new ArrayList<String>();
    ArrayList<String> city_name_list = new ArrayList<>();
    ArrayList<String> city_id_list = new ArrayList<>();
    Boolean city_first_time_loading = true;  //using for preventing the listner first call in City Spinner
    Spinner customSpinner;
    TextView tv_country;
    AlertDialog alertDialog;
    public static SimpleDraweeView image;
    static String FragmentName="";
    TravexApplication mTravexApplication = null;

    //private Stack<Fragment> fragmentStack = new Stack<Fragment>();

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.todo_activity);
//        moveDrawerToTop();

        try {
            Intent myIntent = getIntent();
            FragmentName = myIntent.getStringExtra("FragmentName");
            if(FragmentName==null)FragmentName="";
        }
        catch (Exception ex)
        {
            FragmentName="";
        }
        mTravexApplication = TravexApplication.getInstance();
        preferenceManager = new PreferenceManager(this);
//        final ActionBar bar = this.getSupportActionBar();
//        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#a81d1b")));
//        bar.setDisplayHomeAsUpEnabled(true);
//        bar.setHomeAsUpIndicator(R.drawable.ic_action_menu);
//        bar.setDisplayShowTitleEnabled(false);
//        bar.setDisplayShowCustomEnabled(true);
//        bar.setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));

        //Getting All Cities
        mArrayListForCityNames = preferenceManager.getSupportedCountryNames();
        mArrayListForCityId = preferenceManager.getSupportedCountryId();

        for(int i=0;i<mArrayListForCityId.size();i++)
        {
            city_id_list.add(mArrayListForCityId.get(i));
        }

        for(int i=0;i<mArrayListForCityNames.size();i++)
        {
            city_name_list.add(mArrayListForCityNames.get(i));
        }



        Toolbar toolbar= (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#a81d1b")));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_menu);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        fragment = null;


//
        int current_city_position=0;
        if(preferenceManager.getCurrentcityposition()!=null)
        {
            current_city_position = Integer.parseInt(preferenceManager.getCurrentcityposition());
        }


        customSpinner = (Spinner) findViewById(R.id.barTitle);
        tv_country = (TextView) findViewById(R.id.tv_country);
        ArrayAdapter spinnerArrayAdapter = new ArrayAdapter(this,R.layout.spinner_text_color_white,city_name_list);
        customSpinner.setAdapter(spinnerArrayAdapter);
        customSpinner.setSelection(current_city_position);
        tv_country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AfterloginKnowncity.this);
                alertDialogBuilder.setMessage("Are you sure,You want to change your current city?");

                alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        customSpinner.performClick();
                    }
                });

                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.hide();
                    }
                });

                alertDialog = alertDialogBuilder.create();


                Fragment Current_Fragment = (Fragment) getFragmentManager().findFragmentById(R.id.frame_container);
                if (Current_Fragment instanceof MapShowingFragment || Current_Fragment instanceof ProfileFirstFragment ||  Current_Fragment instanceof ProfileFragmentTwo) {
                    alertDialog.show();
                }


            }
        });



        customSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String country_name = city_name_list.get(position);
                String country_id = city_id_list.get(position);
                String current_city_latitude = preferenceManager.getSupportedCountryLatitude().get(position).toString();
                String current_city_longitude = preferenceManager.getSupportedCountryLongitude().get(position).toString();

                preferenceManager.setSelectedCountryName(country_name);
                preferenceManager.setSelectedcountryId(country_id);
                preferenceManager.setSelectedLatitude(current_city_latitude);
                preferenceManager.setSelectedLongitude(current_city_longitude);
                tv_country.setText(country_name);
                if (city_first_time_loading == false) {
                    preferenceManager.setCurrentcityposition(String.valueOf(position));
                    Fragment Current_Fragment = (Fragment) getFragmentManager().findFragmentById(R.id.frame_container);
                    if(Current_Fragment instanceof MapShowingFragment) {
                        Intent intent = getIntent();
                        finish();
                        startActivity(intent);
                    }
                }
                city_first_time_loading = false;

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        
        String member_profile_image = preferenceManager.getmember_profile_image();
        image = (SimpleDraweeView) findViewById(R.id.networkImageViewEqWNH);
//        image.setImageResource(R.drawable.user);
        if(member_profile_image == null) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.user).build();
            image.setImageURI(imageRequest.getSourceUri());              }
        else if(member_profile_image.equals("")) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.user).build();
            image.setImageURI(imageRequest.getSourceUri());
        }
        else
        {
            Uri img_uri=Uri.parse(member_profile_image);
            ImageRequest request = ImageRequest.fromUri(img_uri);

            DraweeController controller = Fresco.newDraweeControllerBuilder()
                    .setImageRequest(request)
                    .setOldController(image.getController()).build();
            //Log.e(TAG, "ImagePath uri " + img_uri);

            image.setController(controller);
        }

//
//        // Set the on click listener for the title
//        customTitle.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //Toast.makeText(AfterloginKnowncity.this,"CLICKED",Toast.LENGTH_SHORT).show();
//                Log.w("MainActivity", "ActionBar's title clicked.");
//            }
//        });
//        // Apply the custom view
//        bar.setCustomView(customView);
//        int titleId = getResources().getIdentifier("action_bar_title", "id", "android");
//        TextView abTitle = (TextView) findViewById(titleId);
//        abTitle.setTextColor(Color.parseColor("#FFFFFF"));
//        abTitle.setText("Dubai");
        navDrawerItems = new ArrayList<String>();
        navDrawerItems.add("settings");
        navDrawerItems.add("Profile Name");
        navDrawerItems.add("My Profile");
        navDrawerItems.add("My Plan");

        navDrawerItems.add("Travel Essential");
        navDrawerItems.add("Article");
        navDrawerItems.add("Travex Tips");
        navDrawerItems.add("My Suggestions");
//        navDrawerItems.add("Support");
        navDrawerItems.add("About Us");
        navDrawerItems.add("Contact Us");


        if(preferenceManager.getboolean("logout")!=null) {

            if(preferenceManager.getboolean("logout")==false)
            {
                navDrawerItems.add("Log In");
            }
            else
            {
                navDrawerItems.add("Log Out");
            }

//                if (categoryItemsnew.get(position).getTitle().toString().equals("Logout")) {
//                    ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_emergencies).build();
//                    holder.icon.setImageURI(imageRequest.getSourceUri());
//                }
        }
        else
        {
            navDrawerItems.add("Log In");
        }

        mDrawerList = (ListView) findViewById(R.id.list_slidermenu);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        frame = (FrameLayout) findViewById(R.id.frame_container);
        //mDrawerList.setFooterDividersEnabled(false);
        //mDrawerList.setDivider(getDrawable(R.drawable.edittext_bottom_line));
        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());
        adapter = new Navadapter(getApplicationContext(),
                navDrawerItems);
        mDrawerList.setAdapter(adapter);




       // getActionBar().setDisplayHomeAsUpEnabled(true);
//        getActionBar().setHomeButtonEnabled(true);



            mDrawerToggle =new ActionBarDrawerToggle(this,mDrawerLayout,R.string.city_name,R.string.city_name){
                @SuppressLint("NewApi")
                public void onDrawerSlide(View drawerView, float slideOffset)
                {       super.onDrawerSlide(drawerView,slideOffset);
                    float moveFactor = (mDrawerList.getWidth() * slideOffset);
//                    if(slideOffset > 0.5){
//                        bar.setBackgroundDrawable(null);
//                        bar.hide();
//                    } else {
//                        bar.show();
//
//                        if(slideOffset < 0.1){
//                            bar.setBackgroundDrawable(layerDrawable);
//                        }
//                    }
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                    {
                        frame.setTranslationX(moveFactor);
                    }
                    else
                    {
                        TranslateAnimation anim = new TranslateAnimation(lastTranslate, moveFactor, 0.0f, 0.0f);
                        anim.setDuration(0);
                        anim.setFillAfter(true);
                        frame.startAnimation(anim);

                        lastTranslate = moveFactor;
                    }
                }
            public void onDrawerClosed(View view) {

                active_nowbtn=false;
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {

                active_nowbtn=false;
                invalidateOptionsMenu();
            }

       };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        displayView(0);

//        fragment=new MapShowingFragment();
//        if (fragment != null) {
//            fragmentManager = getFragmentManager();
//            fragmentManager.beginTransaction()
//                    .replace(R.id.frame_container, fragment).commit();
//            backStateName = fragment.getClass().getName();
//            boolean fragmentPopped = fragmentManager
//                    .popBackStackImmediate(backStateName, 0);
//            mDrawerLayout.closeDrawer(mDrawerList);
//
//
//        }
    }
//    private void moveDrawerToTop() {
//        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        DrawerLayout drawer = (DrawerLayout) inflater.inflate(R.layout.decor, null); // "null" is important.
//
//        // HACK: "steal" the first child of decor view
//        ViewGroup decor = (ViewGroup) getWindow().getDecorView();
//        View child = decor.getChildAt(0);
//        decor.removeView(child);
//        LinearLayout container = (LinearLayout) drawer.findViewById(R.id.drawer_content); // This is the container we defined just now.
//        container.addView(child, 0);
//
//
//        // Make the drawer replace the first child
//        decor.addView(drawer);
//    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


            getMenuInflater().inflate(R.menu.main_list, menu);


        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Fragment searchresultFragment = new searchresultFragment();
        Fragment SearchResultFragmentSecond = new SearchResultFragmentSecond();
        Fragment Travelsessential_fragment_second = new Travelsessential_fragment_second();
        Fragment currentFragment = this.getFragmentManager().findFragmentById(R.id.frame_container);

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            if(currentFragment instanceof searchresultFragment )
            {
//                onBackPressed();
                startActivity(new Intent(this, AfterloginKnowncity.class));
                return true;

            }
            else if(currentFragment instanceof Travelsessential_fragment_second || currentFragment instanceof SearchResultFragmentSecond || currentFragment instanceof ArticleDetailsFragment)
            {
                onBackPressed();
            }
            else
            {
                mDrawerToggle.onOptionsItemSelected(item);
            }
            //getName().equals("org.technomobs.travex.Activity.Fragment.MapShowingFragment")

            return true;
        } else if (id == R.id.menu_item) {
            startActivity(new Intent(this, AfterloginKnowncity.class));
            return true;
        }
//        else if (id == android.R.id.title) {
//            Toast.makeText(this, "Title", Toast.LENGTH_SHORT).show();
//            return true;
//        }


        return super.onOptionsItemSelected(item);
    }

    private void displayView(int position) {
        fragment=null;
        // update the main content by replacing fragments

        if (navDrawerItems.get(position).equals("Travel Essential")) {

            fragment = TravelEssential.newInstance(AfterloginKnowncity.this);
        }
        if (navDrawerItems.get(position).equals("About Us")) {

            //fragment = new PosVisibilityFragment(getApplicationContext(),"availability",activityNameArray.get(position-2),activityid);
            fragment = AboutUsFragment.newInstance(AfterloginKnowncity.this);


        }
        if (navDrawerItems.get(position).equals("Travex Tips")) {

            //fragment = new PosVisibilityFragment(getApplicationContext(),"availability",activityNameArray.get(position-2),activityid);
            fragment = TravexTipsFragment.newInstance(AfterloginKnowncity.this);


        }
        if (navDrawerItems.get(position).equals("Support")) {

            //fragment = new PosVisibilityFragment(getApplicationContext(),"availability",activityNameArray.get(position-2),activityid);
            fragment = new MapShowingFragment();

        }
//        if (navDrawerItems.get(position).equals("My Suggestions")){
//            fragment = SuggessionFragment.newInstance(this);
//        }
//        if (navDrawerItems.get(position).equals("My Plan")) {
//
//            //fragment = new PosVisibilityFragment(getApplicationContext(),"availability",activityNameArray.get(position-2),activityid);
//            fragment = new Myplan();
//
//        }
//        if (navDrawerItems.get(position).equals("My Profile")) {
//
//            //fragment = new PosVisibilityFragment(getApplicationContext(),"availability",activityNameArray.get(position-2),activityid);
//            fragment = ProfileFirstFragment.newInstance(getApplicationContext());
//
//
//        }

		if(navDrawerItems.get(position).equals("Article")){

            //fragment = new PosVisibilityFragment(getApplicationContext(),"availability",activityNameArray.get(position-2),activityid);
            fragment = ArticleFragment.newInstance(this);


        }
        if(navDrawerItems.get(position).equals("Contact Us")){

            //fragment = new PosVisibilityFragment(getApplicationContext(),"availability",activityNameArray.get(position-2),activityid);
            fragment = ContactFragment.newInstance(AfterloginKnowncity.this);


        }
        if (fragment == null) {
            if(FragmentName.equals(mTravexApplication.My_PROFILE_FRAGMENT_TAG))
            {
                fragment = ProfileFirstFragment.newInstance(this);
                FragmentName="";
            }
            else if (navDrawerItems.get(position).equals("Log Out")) {
                preferenceManager.setRegisteredUser_name("");
                preferenceManager.setmember_profile_image("");
                preferenceManager.setRegisteredUser_email(null);
                preferenceManager.setLogin_Type(null);
                preferenceManager.setRegisration_Id_response(null);

                Intent logouintent = new Intent(AfterloginKnowncity.this,SignUpLoginMasterActivity.class);
                startActivity(logouintent);
            }
            else if (navDrawerItems.get(position).equals("Log In")) {
                preferenceManager.setRegisteredUser_name("");
                preferenceManager.setmember_profile_image("");
                preferenceManager.setRegisteredUser_email(null);
                preferenceManager.setLogin_Type(null);
                preferenceManager.setRegisration_Id_response(null);
                fragment=new MapShowingFragment();
                Intent logouintent = new Intent(AfterloginKnowncity.this,SignUpLoginMasterActivity.class);
                startActivity(logouintent);
            }
            else if (navDrawerItems.get(position).equals("My Profile")) {
                if(preferenceManager.getboolean("logout")==null) {
                    Toast.makeText(AfterloginKnowncity.this,"Please Login..",Toast.LENGTH_SHORT).show();
                }
                else if(preferenceManager.getboolean("logout")==false)
                {
                    Toast.makeText(AfterloginKnowncity.this,"Please Login..",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    fragment = ProfileFirstFragment.newInstance(this);
                }
            }
            else if (navDrawerItems.get(position).equals("My Plan")) {
                if(preferenceManager.getboolean("logout")==null) {
                    Toast.makeText(AfterloginKnowncity.this,"Please Login..",Toast.LENGTH_SHORT).show();
                }
                else if(preferenceManager.getboolean("logout")==false)
                {
                    Toast.makeText(AfterloginKnowncity.this,"Please Login..",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    fragment = new Myplan();
                }
            }
            else if (navDrawerItems.get(position).equals("My Suggestions")) {
                if(preferenceManager.getboolean("logout")==null) {
                    Toast.makeText(AfterloginKnowncity.this,"Please Login..",Toast.LENGTH_SHORT).show();
                }
                else if(preferenceManager.getboolean("logout")==false)
                {
                    Toast.makeText(AfterloginKnowncity.this,"Please Login..",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    fragment = SuggessionFragment.newInstance(this);
                }
            }
            else if (navDrawerItems.get(position).equals("My Suggestions")) {
                if(preferenceManager.getboolean("logout")==null) {
                    Toast.makeText(AfterloginKnowncity.this,"Please Login..",Toast.LENGTH_SHORT).show();
                }
                else if(preferenceManager.getboolean("logout")==false)
                {
                    Toast.makeText(AfterloginKnowncity.this,"Please Login..",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    fragment = SuggessionFragment.newInstance(this);
                }
            }
            else
            {
                fragment=new MapShowingFragment();
            }

//            Fragment firstfragment=new MapShowingFragment();
//            fragmentStack.push(firstfragment);
//            fragmentManager = getFragmentManager();
//            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//            fragmentTransaction.replace(R.id.frame_container, firstfragment);
//            fragmentTransaction.addToBackStack(null);
//            fragmentTransaction.commit();
//            fragmentManager = getFragmentManager();
//            FragmentTransaction ft = fragmentManager.beginTransaction();
//            ft.add(R.id.container, firstfragment);
//            fragmentStack.push(firstfragment);
//            ft.commit();
        }
        //Common
        if (fragment != null) {

            fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame_container, fragment);
            fragmentTransaction.addToBackStack(fragment.getClass().getName());
            fragmentTransaction.commit();

        }

    }

    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {

            mDrawerLayout.closeDrawers();
            displayView(position);
        }

    }
    @Override
    public void onBackPressed() {
//        if (getFragmentManager().getBackStackEntryCount() == 0) {
//
//        } else {
//            getFragmentManager().popBackStack();
//        }
//        fragmentManager = getFragmentManager();
//        if (fragmentManager.getBackStackEntryCount() > 0) {
//            Log.i("MainActivity", "popping backstack");
//            fragmentManager.popBackStack();
//        } else {
//            Log.i("MainActivity", "nothing on backstack, calling super");
//            super.onBackPressed();
//        }
        int backstackcount = fragmentManager.getBackStackEntryCount();


        if(backstackcount>1) {
            String fragment_name = fragmentManager.getBackStackEntryAt(backstackcount-2).getName();
//          if(fragment_name.equals("org.technomobs.travex.Activity.Fragment.MapShowingFragment"))
//          {
//              fragmentManager.popBackStack();
//              fragmentManager = getFragmentManager();
//              FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//              fragmentTransaction.replace(R.id.frame_container, new MapShowingFragment());
//              fragmentTransaction.commit();
//          }
//            else {
              fragmentManager.popBackStack();
//          }
        }
        else
        {
            super.onBackPressed();
        }
//        super.onBackPressed();

//        if (fragmentStack.size() == 2) {
//            FragmentTransaction ft = fragmentManager.beginTransaction();
//            fragmentStack.lastElement().onPause();
//            ft.remove(fragmentStack.pop());
//            fragmentStack.lastElement().onResume();
//            ft.show(fragmentStack.lastElement());
//            ft.commit();
//        } else {
//            super.onBackPressed();
//        }

    }
    static String country = null;
//    public String getSelectedCountry() {
//        if (country == null) {
//
//            country = mAppCompatSpinner.getItemAtPosition(0).toString();
//        }
//        return country;
//
//    }

//    public String fetchCountryIdForCountry(String country) {
////        String countryId = null;
////        int index = 0;
////        ArrayList<String> countrynames = mPreferenceManager.getSupportedCountryNames();
////        index = countrynames.lastIndexOf(country);
////        LogUtility.Log(TAG, "fetchCountryIdForCountry", "index:" + index);
////
////        countryId = mPreferenceManager.getSupportedCountryId().get(index);
////        LogUtility.Log(TAG, "fetchCountryIdForCountry", "countryId:" + countryId);
//
//
//        /*for(String s:countrynames)
//        {
//            if(s.equals(country))
//            {
//                countrynames.lastIndexOf()
//            }
//
//
//        }*/
//
//
//        return countryId;
//    }
}
