package org.technomobs.travex.Activity.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Controller.Interface.AddPlanResponse;
import org.technomobs.travex.Controller.Interface.ListPlanResponse;
import org.technomobs.travex.Controller.JsonObjectMaker;
import org.technomobs.travex.Controller.NetworkManager;
import org.technomobs.travex.Controller.NetworkOptions;
import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.DateFormatFunction;
import org.technomobs.travex.Utillity.ListCreatorForNameValuePairRequest;
import org.technomobs.travex.Utillity.LogUtility;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Abhi on 11-03-2016.
 */
public class AddToMyplanFragment extends Fragment implements AddPlanResponse{
    private static final String dateTemplate = "MMMM yyyy";
    static NetworkManager mNetworkManager = null;
    PreferenceManager mPreferenceManager = null;
    Activity act;
    View rootView;
    String  output="";
    ArrayList<String> Plan_ID,Plan_category,Plan_item,Plan_member,Plan_date,Plan_time,Plan_status,Plan_created,items;
    JsonObjectMaker mJsonObjectMaker = null;
    ListCreatorForNameValuePairRequest mListCreatorForNameValuePairRequest = null;

    HashMap<String, ArrayList<String>> fetchtodo, fetchtodo_duedate;
    String date_month_year;
    ArrayList<String> due_date_List;
    int check = 0;
    private int poststatus = 0;
    private Button currentMonth;
    private ImageView prevMonth;
    private ImageView nextMonth;
    private GridView calendarView;
    private GridCellAdapter adapter;
    private Calendar _calendar;
    private int month, year;
    static Context context;
    static String plan_id="",plan_name="";
    public static AddToMyplanFragment newInstance(Context con,String select_id,String name) {
        AddToMyplanFragment fragment = new AddToMyplanFragment();
        context = con;
        plan_id=select_id;
        plan_name=name;
        return fragment;
    }
    View.OnClickListener Onclk = new View.OnClickListener() {


        @Override
        public void onClick(View v) {
            if (v == prevMonth) {
                if (month <= 1) {
                    month = 12;
                    year--;
                } else {
                    month--;
                }
                //	Log.d(tag, "Setting Prev Month in GridCellAdapter: " + "Month: " + month + " Year: " + year);
                setGridCellAdapterToDate(month, year);
            } else if (v == nextMonth) {
                if (month > 11) {
                    month = 1;
                    year++;
                } else {
                    month++;
                }
                //	Log.d(tag, "Setting Next Month in GridCellAdapter: " + "Month: " + month + " Year: " + year);
                setGridCellAdapterToDate(month, year);
            }


        }
    };
  //  private ListView list_view,todays_event_list;
    private boolean all_checked = true, worksheet_checked = false, product_checked = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        act = getActivity();

        rootView = inflater.inflate(R.layout.addto_myplan_fragment, container, false);
        initiating_APICredentials();




        Plan_ID=new ArrayList<String>();
        Plan_category=new ArrayList<String>();
        Plan_item=new ArrayList<String>();
        Plan_member=new ArrayList<String>();
        Plan_date=new ArrayList<String>();
        Plan_time=new ArrayList<String>();
        Plan_status=new ArrayList<String>();
        Plan_created=new ArrayList<String>();

        mNetworkManager.setOnAddPlanResponseListener(this);
        try {
            due_date_List = new ArrayList<String>();
            pickDueDates();
        } catch (Exception e) {
            e.printStackTrace();
        }
        _calendar = Calendar.getInstance(Locale.getDefault());
        month = _calendar.get(Calendar.MONTH) + 1;
        year = _calendar.get(Calendar.YEAR);

        prevMonth = (ImageView) this.rootView.findViewById(R.id.prevMonth);
        prevMonth.setOnClickListener(Onclk);

        currentMonth = (Button) this.rootView.findViewById(R.id.currentMonth);
        currentMonth.setText(DateFormat.format(dateTemplate, _calendar.getTime()));
        nextMonth = (ImageView) this.rootView.findViewById(R.id.nextMonth);
        nextMonth.setOnClickListener(Onclk);
        calendarView = (GridView) this.rootView.findViewById(R.id.calendar);
        adapter = new GridCellAdapter(getActivity().getApplicationContext(), R.id.calendar_day_gridcell, month, year);
        adapter.notifyDataSetChanged();
        calendarView.setAdapter(adapter);
        return rootView;    }

    private void setGridCellAdapterToDate(int month, int year) {
        adapter = new GridCellAdapter(getActivity().getApplicationContext(), R.id.calendar_day_gridcell, month, year);
        _calendar.set(year, month - 1, _calendar.get(Calendar.DAY_OF_MONTH));
        currentMonth.setText(DateFormat.format(dateTemplate, _calendar.getTime()));
        adapter.notifyDataSetChanged();
        calendarView.setAdapter(adapter);
    }

    public void initiating_APICredentials() {
        mJsonObjectMaker = new JsonObjectMaker(getActivity());
        mNetworkManager = NetworkManager.getSingleInstance(getActivity());
        mPreferenceManager = new PreferenceManager(getActivity());
        mListCreatorForNameValuePairRequest = new ListCreatorForNameValuePairRequest(getActivity());

    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
    }
    private void pickDueDates() {
        due_date_List = fetchtodo.get("DueDate");
    }

    @SuppressWarnings("unused")
    private Typeface getTypeFace() {

        Typeface tf = Typeface.createFromAsset(act.getAssets(), "MyriadPro_Regular.otf");
        return tf;
    }







    public String getCategory_Id_Tips_For(String item) {

        if (item.equals("Airline")) {

            return "pngrtbel_vq_1";
        } else if (item.equals("Shopping")) {

            return "pngrtbel_vq_10";
        } else if (item.equals("Business")) {

            return "pngrtbel_vq_11";
        } else if (item.equals("Real Estate")) {

            return "pngrtbel_vq_12";
        } else if (item.equals("Money Exchange")) {

            return "pngrtbel_vq_13";
        } else if (item.equals("Car Rentals")) {

            return "pngrtbel_vq_14";
        } else if (item.equals("Embasies & Consulates")) {

            return "pngrtbel_vq_2";
        } else if (item.equals("Tour & Travel")) {

            return "pngrtbel_vq_3";
        } else if (item.equals("Events")) {

            return "pngrtbel_vq_4";
        } else if (item.equals("Eating Out")) {

            return "pngrtbel_vq_5";
        } else if (item.equals("Stay/Hotels")) {

            return "pngrtbel_vq_6";
        } else if (item.equals("nightlife")) {

            return "pngrtbel_vq_7";
        } else if (item.equals("Spa")) {

            return "pngrtbel_vq_8";
        } else if (item.equals("Things To Do")) {

            return "pngrtbel_vq_9";
        } else if (item.equals("Emergencies")) {

            return "pngrtbel_vq_15";
        }


        return null;


    }
    // ///////////////////////////////////////////////////////////////////////////////////////
    // Inner Class
    public class GridCellAdapter extends BaseAdapter implements View.OnClickListener {
        private static final int DAY_OFFSET = 1;
        private final Context _context;
        private final List<String> list;
        private final String[] weekdays = new String[]{"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
        private final String[] months = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

        private final String[] monthsNumeric = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"};

        private final int[] daysOfMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        private final HashMap<String, Integer> eventsPerMonthMap;
        //private final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy");
        @SuppressLint("SimpleDateFormat")
        private final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MMM-dd");
        private int daysInMonth;
        private int currentDayOfMonth;
        private int currentWeekDay;
        private Button gridcell;
        private TextView num_events_per_day;

        // Days in Current Month
        @SuppressWarnings("unchecked")
        public GridCellAdapter(Context context, int textViewResourceId, int month, int year) {
            super();
            this._context = context;
            this.list = new ArrayList<String>();
            //	Log.d(tag, "==> Passed in Date FOR Month: " + month + " " + "Year: " + year);
            Calendar calendar = Calendar.getInstance();
            setCurrentDayOfMonth(calendar.get(Calendar.DAY_OF_MONTH));
            setCurrentWeekDay(calendar.get(Calendar.DAY_OF_WEEK));
            //						Log.d(tag, "New Calendar:= " + calendar.getTime().toString());
            //						Log.d(tag, "CurrentDayOfWeek :" + getCurrentWeekDay());
            //						Log.d(tag, "CurrentDayOfMonth :" + getCurrentDayOfMonth());

            // Print Month
            printMonth(month, year);

            // Find Number of Events
            eventsPerMonthMap = findNumberOfEventsPerMonth(year, month);
        }

        private String getMonthAsString(int i) {
            return months[i];
        }

        @SuppressWarnings("unused")
        private String getWeekDayAsString(int i) {
            return weekdays[i];
        }

        private int getNumberOfDaysOfMonth(int i) {
            return daysOfMonth[i];
        }

        @Override
        public String getItem(int position) {
            return list.get(position);
        }

        @Override
        public int getCount() {
            return list.size();
        }

        /**
         * Prints Month
         *
         * @param mm
         * @param yy
         */
        @SuppressWarnings("unused")
        private void printMonth(int mm, int yy) {
            //	Log.d(tag, "==> printMonth: mm: " + mm + " " + "yy: " + yy);
            // The number of days to leave blank at
            // the start of this month.
            int trailingSpaces = 0;
            int leadSpaces = 0;
            int daysInPrevMonth = 0;
            int prevMonth = 0;
            int prevYear = 0;
            int nextMonth = 0;
            int nextYear = 0;

            int currentMonth = mm - 1;
            String currentMonthName = getMonthAsString(currentMonth);
            daysInMonth = getNumberOfDaysOfMonth(currentMonth);

            //	Log.d(tag, "Current Month: " + " " + currentMonthName + " having " + daysInMonth + " days.");

            // Gregorian Calendar : MINUS 1, set to FIRST OF MONTH
            GregorianCalendar cal = new GregorianCalendar(yy, currentMonth, 1);
            //Log.d(tag, "Gregorian Calendar:= " + cal.getTime().toString());

            if (currentMonth == 11) {
                prevMonth = currentMonth - 1;
                daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
                nextMonth = 0;
                prevYear = yy;
                nextYear = yy + 1;
                //		Log.d(tag, "*->PrevYear: " + prevYear + " PrevMonth:" + prevMonth + " NextMonth: " + nextMonth + " NextYear: " + nextYear);
            } else if (currentMonth == 0) {
                prevMonth = 11;
                prevYear = yy - 1;
                nextYear = yy;
                daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
                nextMonth = 1;
                //		Log.d(tag, "**--> PrevYear: " + prevYear + " PrevMonth:" + prevMonth + " NextMonth: " + nextMonth + " NextYear: " + nextYear);
            } else {
                prevMonth = currentMonth - 1;
                nextMonth = currentMonth + 1;
                nextYear = yy;
                prevYear = yy;
                daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
                //		Log.d(tag, "***---> PrevYear: " + prevYear + " PrevMonth:" + prevMonth + " NextMonth: " + nextMonth + " NextYear: " + nextYear);
            }

            // Compute how much to leave before before the first day of the
            // month.
            // getDay() returns 0 for Sunday.
            int currentWeekDay = cal.get(Calendar.DAY_OF_WEEK) - 1;
            trailingSpaces = currentWeekDay;

            //						Log.d(tag, "Week Day:" + currentWeekDay + " is " + getWeekDayAsString(currentWeekDay));
            //						Log.d(tag, "No. Trailing space to Add: " + trailingSpaces);
            //						Log.d(tag, "No. of Days in Previous Month: " + daysInPrevMonth);

            if (cal.isLeapYear(cal.get(Calendar.YEAR)) && mm == 1) {
                ++daysInMonth;
            }

            // Trailing Month days
            for (int i = 0; i < trailingSpaces; i++) {
                //		Log.d(tag, "PREV MONTH:= " + prevMonth + " => " + getMonthAsString(prevMonth) + " " + String.valueOf((daysInPrevMonth - trailingSpaces + DAY_OFFSET) + i));
                list.add(String.valueOf((daysInPrevMonth - trailingSpaces + DAY_OFFSET) + i) + "-GREY" + "-" + getMonthAsString(prevMonth) + "-" + prevYear);
            }

            // Current Month Days
            for (int i = 1; i <= daysInMonth; i++) {
                //	Log.d(currentMonthName, String.valueOf(i) + " " + getMonthAsString(currentMonth) + " " + yy);
                if (i == getCurrentDayOfMonth()) {
                    list.add(String.valueOf(i) + "-BLUE" + "-" + getMonthAsString(currentMonth) + "-" + yy);
                } else {
                    list.add(String.valueOf(i) + "-WHITE" + "-" + getMonthAsString(currentMonth) + "-" + yy);
                }
            }

            // Leading Month days
            for (int i = 0; i < list.size() % 7; i++) {
                //		Log.d(tag, "NEXT MONTH:= " + getMonthAsString(nextMonth));
                String newDay = "";
                list.add(String.valueOf(i + 1) + "-GREY" + "-" + getMonthAsString(nextMonth) + "-" + nextYear);
                //list.add(newDay + "-GREY" + "-" + getMonthAsString(nextMonth) + "-" + nextYear);
            }
        }


        @SuppressWarnings("rawtypes")
        private HashMap findNumberOfEventsPerMonth(int year, int month) {
            HashMap map = new HashMap<String, Integer>();

            return map;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            check = 0;
            View row = convertView;
            if (row == null) {
                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.calendar_day_gridcell, parent, false);
            }

            // Get a reference to the Day gridcell
            gridcell = (Button) row.findViewById(R.id.calendar_day_gridcell);
            gridcell.setOnClickListener(this);

            // ACCOUNT FOR SPACING

            //	Log.d(tag, "Current Day: " + getCurrentDayOfMonth());
            String[] day_color = list.get(position).split("-");
            String theday = day_color[0];
            Calendar calendar=Calendar.getInstance();
            DateFormatFunction df=new DateFormatFunction();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String formattedDate = sdf.format(calendar.getTime());
            String current_date = df.dateformat("yyyy-MM-dd", "dd-MMMM-yyyy",formattedDate );

            if (theday.equalsIgnoreCase("1")) {
                theday = "01";
            } else if (theday.equalsIgnoreCase("2")) {
                theday = "02";
            } else if (theday.equalsIgnoreCase("3")) {
                theday = "03";
            } else if (theday.equalsIgnoreCase("4")) {
                theday = "04";
            } else if (theday.equalsIgnoreCase("5")) {
                theday = "05";
            } else if (theday.equalsIgnoreCase("6")) {
                theday = "06";
            } else if (theday.equalsIgnoreCase("7")) {
                theday = "07";
            } else if (theday.equalsIgnoreCase("8")) {
                theday = "08";
            } else if (theday.equalsIgnoreCase("9")) {
                theday = "09";
            }

            String themonth = day_color[2];
            String theyear = day_color[3];
            String temp_date=theday+"-"+themonth+"-"+theyear;

            // Set the Day GridCell
            gridcell.setText(theday);
            // gridcell.setTag(theday + "-" + themonth + "-" + theyear);


            if (themonth.equalsIgnoreCase(months[0])) {
                themonth = monthsNumeric[0];
            } else if (themonth.equalsIgnoreCase(months[1])) {
                themonth = monthsNumeric[1];
            } else if (themonth.equalsIgnoreCase(months[2])) {
                themonth = monthsNumeric[2];
            } else if (themonth.equalsIgnoreCase(months[3])) {
                themonth = monthsNumeric[3];
            } else if (themonth.equalsIgnoreCase(months[4])) {
                themonth = monthsNumeric[4];
            } else if (themonth.equalsIgnoreCase(months[5])) {
                themonth = monthsNumeric[5];
            } else if (themonth.equalsIgnoreCase(months[6])) {
                themonth = monthsNumeric[6];
            } else if (themonth.equalsIgnoreCase(months[7])) {
                themonth = monthsNumeric[7];
            } else if (themonth.equalsIgnoreCase(months[8])) {
                themonth = monthsNumeric[8];
            } else if (themonth.equalsIgnoreCase(months[9])) {
                themonth = monthsNumeric[9];
            } else if (themonth.equalsIgnoreCase(months[10])) {
                themonth = monthsNumeric[10];
            } else if (themonth.equalsIgnoreCase(months[11])) {
                themonth = monthsNumeric[11];
            } else if (themonth.equalsIgnoreCase(months[12])) {
                themonth = monthsNumeric[12];
            }

            String dateText = theday + "-" + themonth + "-" + theyear;

            //gridcell.setTag(theday + "-" + themonth + "-"+theyear);
            gridcell.setTag(theyear + "-" + themonth + "-" + theday);
            gridcell.setTextSize(15);
            //	Log.d(tag, "Setting GridCell " + theday + "-" + themonth + "-" + theyear);

            if (day_color[1].equals("GREY")) {
                gridcell.setTextColor(Color.LTGRAY);
                gridcell.setEnabled(false);
                check = 1;
            }
            if (day_color[1].equals("WHITE")) {
                gridcell.setTextColor(Color.BLACK);
                gridcell.setEnabled(true);
            }
            if (day_color[1].equalsIgnoreCase("BLUE")) {
                gridcell.setTextColor(getResources().getColor(R.color.static_text_color));
                gridcell.setEnabled(true);
            }

            DateFormatFunction dff = new DateFormatFunction();

            for (int i = 0; i < due_date_List.size(); i++) {
                String datetext = dff.dateformat("yyyy-MM-dd", "dd-MM-yyyy", due_date_List.get(i));
                if (dateText.equalsIgnoreCase(datetext) && check == 0) {


                    gridcell.setTextColor(Color.BLUE);
          //          gridcell.setBackgroundResource(R.drawable.cc);


                    // gridcell.setText("Match Found = " + dateText);
                    //		System.out.println("Match Found = " + dateText);
                }
            }
            if (current_date.equals(temp_date)){
                gridcell.setTextColor(getResources().getColor(R.color.theme));
             //   gridcell.setBackgroundResource(R.drawable.cc);
            }

            return row;
        }

        @Override
        public void onClick(View view) {
            date_month_year = (String) view.getTag();
            //Toast.makeText(getActivity().getApplicationContext(), "Selected: " + date_month_year.trim() + " **", Toast.LENGTH_LONG).show();
            gridcell.setTextColor(Color.WHITE);
     //       gridcell.setBackgroundResource(R.drawable.cc);

            Calendar mcurrentTime = Calendar.getInstance();


            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            final int ff=mcurrentTime.get(Calendar.SECOND);
            int am=mcurrentTime.get(Calendar.AM_PM);
            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(getActivity(),am,new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                        if(hourOfDay>12)
                        {
                            int h=hourOfDay-12;
                            output = String.format("%02d:%02d", h, minute);
                            output=output+" Pm";
                        }
                        if(hourOfDay==12)
                        {
                            output = String.format("%02d:%02d",hourOfDay, minute);
                            output=output+" Pm";
                        }
                        if(hourOfDay<12)
                        {
                            output = String.format("%02d:%02d", hourOfDay, minute);
                            output=output+" Am";

                        }
                 //    output = String.format("%02d:%02d", hourOfDay, minute);

                  try {
                      postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.ADD_PLAN_URL,
                              getjsonObject(TravexApplication.ADD_PLAN_TAG),
                              TravexApplication.REQUEST_ID_ADD_PLAN);
                  }catch(Exception e){
                      e.printStackTrace();
                  }
                }
            }, hour, minute, false);
            mTimePicker.setTitle("Select Time");

            mTimePicker.show();

        }


        public int getCurrentDayOfMonth() {
            return currentDayOfMonth;
        }

        private void setCurrentDayOfMonth(int currentDayOfMonth) {
            this.currentDayOfMonth = currentDayOfMonth;
        }

        public int getCurrentWeekDay() {
            return currentWeekDay;
        }

        public void setCurrentWeekDay(int currentWeekDay) {
            this.currentWeekDay = currentWeekDay;
        }
    }

    public void postJsonRequest(int request_type, final Map<String, String> request_header, String url, JSONObject jsonObject, final int request_id) {

        mNetworkManager.postJsonRequest(request_type, request_header, url, jsonObject, request_id);


    }
    public JSONObject getjsonObject(String tag) {
        if (tag.equals(TravexApplication.ADD_PLAN_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfAddplan
                            (TravexApplication.ADD_PLAN_ITEM_ID,
                                    TravexApplication.ADD_PLAN_ITEM_NAME,
                                    TravexApplication.ADD_PLAN_TIME,
                                    TravexApplication.ADD_PLAN_USER_ID,
                                    TravexApplication.ADD_PLAN_DATE,
                                    TravexApplication.ADD_PLAN_CATEGORY
                            ),
                    mListCreatorForNameValuePairRequest.getListOfAddplan(plan_id,plan_name, output, mPreferenceManager.getRegisration_Id_response(),date_month_year.trim(), MapShowingFragment.selected_category)

            );
        }
        return null;
    }
    public JSONObject getJson(ArrayList<String> namePair, ArrayList<String> valuePair) {


        return mJsonObjectMaker.getJson(namePair, valuePair);
    }
    @Override
    public void onAddPlanResponse(boolean status) {
        if (status) {

        //    LogUtility.Log(TAG, "onAddPlanResponse: :success", null);
            Toast.makeText(getActivity(),"Plan Added",Toast.LENGTH_SHORT).show();

        } else {
        //    LogUtility.Log(TAG, "onAddPlanResponse: :failure", null);
            Toast.makeText(getActivity(),"Plan Already Added",Toast.LENGTH_SHORT).show();;
        }
    }
}