package org.technomobs.travex.Activity.Fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import org.technomobs.travex.R;

import java.util.ArrayList;

/**
 * Created by Abhi on 09-03-2016.
 */
public class SearchListAdapter extends BaseAdapter {

    private static final int VIEW_TYPE_HEADER = 0;
    private static final int VIEW_TYPE_FOOTER = 1;
    private static final int VIEW_TYPE_DEFAULT = 2;
    private static final int VIEW_TYPE_COUNT = 3;
    private Context context;
    LayoutInflater mInflater;
    String selected_category,selected_categoryid;
    ArrayList<CategoryListItems> categoryItemsnew;
    ArrayList<String> categoryItemsIDnew,searchResultsTypeArrayList ;
    private String apptheme,category_name,Type;
    public int checki = 0;
    int count = 0;
    Activity act;
    private String backStateName;
    public SearchListAdapter(Activity activity,Context context, ArrayList<CategoryListItems> categoryItemsnew,ArrayList<String> categoryItemsIDnew,ArrayList<String> searchResultsTypeArrayList
    ) {
        this.context = context;
this.act=activity;
        this.categoryItemsnew = categoryItemsnew;
        this.categoryItemsIDnew = categoryItemsIDnew;
        this.searchResultsTypeArrayList = searchResultsTypeArrayList;
        this.Type=Type;

        mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

    }
    @Override
    public int getViewTypeCount() {

        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }
    @Override
    public int getCount() {

        return categoryItemsnew.size();
    }

    @Override
    public Object getItem(int position) {
        return categoryItemsnew.get(position);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {

            holder = new ViewHolder();

            convertView = mInflater.inflate(R.layout.category_adapter, null);

            holder.icon= (SimpleDraweeView)convertView.findViewById(R.id.icon);
            holder.layout= (RelativeLayout) convertView.findViewById(R.id.rl);

            holder.txt = (TextView) convertView.findViewById(R.id.title);
            //android:background="@drawable/bottom_line_black"
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();

            ///		convertView.setTag(holder);
        }
//        holder.layout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Fragment fragment=searchresultFragment.newInstance(context,categoryItemsIDnew.get(position));
//
//
//                if (fragment != null) {
//                    FragmentManager fragmentManager = act.getFragmentManager();
//                    fragmentManager.beginTransaction()
//                            .replace(R.id.frame_container, fragment).commit();
//
//
//                    backStateName = fragment.getClass().getName();
//                    boolean fragmentPopped = fragmentManager
//                            .popBackStackImmediate(backStateName, 0);
//
//            }
//            }
//        });
        holder.txt.setText(categoryItemsnew.get(position).getTitle().toString());
        ImageRequest imageRequest=null;
        if(searchResultsTypeArrayList.get(position).equals("keyword")){
            if(MapShowingFragment.selected_category.equals("Airline")){
                imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_airline).build();
            } else if(MapShowingFragment.selected_category.equals("Business")){
                imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_bussiness).build();
            }  else if(MapShowingFragment.selected_category.equals("Car Rentals")){
                imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_rentcar).build();
            }else if(MapShowingFragment.selected_category.equals("Eating Out")){
                imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_eatingout).build();
            } else if(MapShowingFragment.selected_category.equals("Embasies & Consulates")){
                imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_embassy).build();
            }else if(MapShowingFragment.selected_category.equals("Emergencies")){
                imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_emergencies).build();
            }else if(MapShowingFragment.selected_category.equals("Events")){
                imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_events).build();
            } else if(MapShowingFragment.selected_category.equals("Shopping")){
                imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_shopping).build();
            }else if(MapShowingFragment.selected_category.equals("Money Exchange")){
                imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_moneyexchange).build();
            } else if(MapShowingFragment.selected_category.equals("Real Estate")){
                imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_realestate).build();
            } else if(MapShowingFragment.selected_category.equals("Tour & Travel")){
                imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_tour_travel).build();
            } else if(MapShowingFragment.selected_category.equals("Spa")){
                imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_spa).build();
            }else if(MapShowingFragment.selected_category.equals("Things To Do")){
                imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_thingstodo).build();
            } else if(MapShowingFragment.selected_category.equals("Stay/Hotels")){
                imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_stay_hotel).build();
            }

        }else if(searchResultsTypeArrayList.get(position).equals("location")){
             imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_search_location).build();
        }

        holder.icon.setImageURI(imageRequest.getSourceUri());


//        if(categoryItemsnew.get(position).getId().toString().equals("1")) {
//            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_airline).build();
//            holder.icon.setImageURI(imageRequest.getSourceUri());
//        }else if(categoryItemsnew.get(position).getId().toString().equals("2")) {
//            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_shopping).build();
//            holder.icon.setImageURI(imageRequest.getSourceUri());
//        }else if(categoryItemsnew.get(position).getId().toString().equals("3")) {
//            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_bussiness).build();
//            holder.icon.setImageURI(imageRequest.getSourceUri());
//        }else if(categoryItemsnew.get(position).getId().toString().equals("4")) {
//            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_realestate).build();
//            holder.icon.setImageURI(imageRequest.getSourceUri());
//        }else if(categoryItemsnew.get(position).getId().toString().equals("5")) {
//            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_moneyexchange).build();
//            holder.icon.setImageURI(imageRequest.getSourceUri());
//        }else if(categoryItemsnew.get(position).getId().toString().equals("6")) {
//            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_rentcar).build();
//            holder.icon.setImageURI(imageRequest.getSourceUri());
//        }else if(categoryItemsnew.get(position).getId().toString().equals("7")) {
//            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_embassy).build();
//            holder.icon.setImageURI(imageRequest.getSourceUri());
//        }else if(categoryItemsnew.get(position).getId().toString().equals("8")) {
//            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_tour_travel).build();
//            holder.icon.setImageURI(imageRequest.getSourceUri());
//        }else if(categoryItemsnew.get(position).getId().toString().equals("9")) {
//            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_events).build();
//            holder.icon.setImageURI(imageRequest.getSourceUri());
//        }else if(categoryItemsnew.get(position).getId().toString().equals("10")) {
//            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_eatingout).build();
//            holder.icon.setImageURI(imageRequest.getSourceUri());
//        }else if(categoryItemsnew.get(position).getId().toString().equals("11")) {
//            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_stay_hotel).build();
//            holder.icon.setImageURI(imageRequest.getSourceUri());
//        }else if(categoryItemsnew.get(position).getId().toString().equals("12")) {
//            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_nightlife).build();
//            holder.icon.setImageURI(imageRequest.getSourceUri());
//        }

//		holder.txtheadto.setTextColor(Color.WHITE);
//		holder.txthead.setTextColor(Color.YELLOW);
//		holder.txtheadon.setTextColor(Color.WHITE);
        return convertView;
    }

    private static class ViewHolder {
        public RelativeLayout layout;

        public TextView txt;

        SimpleDraweeView icon;
        // String v;
    }

}
