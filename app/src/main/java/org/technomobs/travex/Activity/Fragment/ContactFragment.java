package org.technomobs.travex.Activity.Fragment;


import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;
import org.technomobs.travex.Activity.AfterloginKnowncity;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Controller.Interface.ContactUsResponse;
import org.technomobs.travex.Controller.Interface.EnquiryResponse;
import org.technomobs.travex.Controller.JsonObjectMaker;
import org.technomobs.travex.Controller.NetworkManager;
import org.technomobs.travex.Controller.NetworkOptions;
import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.Common;
import org.technomobs.travex.Utillity.ListCreatorForNameValuePairRequest;
import org.technomobs.travex.Utillity.LogUtility;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ARUN on 03/18/2016.
 */
public class ContactFragment extends Fragment implements ContactUsResponse,EnquiryResponse,View.OnClickListener,Animation.AnimationListener {
    static Activity activity;
    //API Credentials
    static NetworkManager mNetworkManager = null;
    public static final String TAG = ContactFragment.class.getSimpleName();
    JsonObjectMaker mJsonObjectMaker = null;
    ListCreatorForNameValuePairRequest mListCreatorForNameValuePairRequest = null;
    EditText edit_name,edit_email,edit_contactnumber,edit_subject,edit_message;
    Button btn_submit;
    TextView tv_address,tv_phone,tv_location_title,tv_phone_title;
    ImageView img_facebook,img_twitter,img_gplus,img_instagram,img_pin;
    String name,email,contactnumber,subject,message;
    ArrayList<String> SociaMediaURL_List = new ArrayList<String>();
    RelativeLayout animation_layout;
    View rootView;
    LinearLayout pop_layout;
    Animation rotateOne, rotateTwo, rotateThree;

    public static ContactFragment newInstance(Activity act){
        ContactFragment fragment = new ContactFragment();
        activity=act;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onCreate", null);

        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_contact, container, false);
        initiating_APICredentials();
        initialization();
        Common.setupUI(rootView, activity);
        //Retrieving Data For Cotact Us
        postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.CONTACT_US_URL,
                getjsonObject(TravexApplication.CONTACT_US_TAG),
                TravexApplication.REQUEST_ID_CONTACT_US);
        animationInit();
        animationLoading();

        Runnable run = new Runnable() {
            @Override
            public void run() {
                animation_layout.setVisibility(View.GONE);
            }
        };
        Handler handler = new Handler();
        handler.postDelayed(run, 2000);
        return rootView;
    }

    public void initiating_APICredentials()
    {
        mJsonObjectMaker = new JsonObjectMaker(activity);
        mListCreatorForNameValuePairRequest = new ListCreatorForNameValuePairRequest(activity);
        mNetworkManager = NetworkManager.getSingleInstance(activity);
        mNetworkManager.setOnContactUsResponseListener(this);
        mNetworkManager.setOnEnquiryResponseListener(this);
    }

    //Initializing Function
    public void initialization() {
        edit_name=(EditText)rootView.findViewById(R.id.edit_name);
        edit_email=(EditText)rootView.findViewById(R.id.edit_email);
        edit_contactnumber=(EditText)rootView.findViewById(R.id.edit_contactnumber);
        edit_subject=(EditText)rootView.findViewById(R.id.edit_subject);
        edit_message=(EditText)rootView.findViewById(R.id.edit_message);
        btn_submit=(Button)rootView.findViewById(R.id.btn_submit);
        tv_address=(TextView)rootView.findViewById(R.id.tv_address);
        tv_phone=(TextView)rootView.findViewById(R.id.tv_phone);
        tv_location_title=(TextView)rootView.findViewById(R.id.tv_location_title);
        tv_phone_title=(TextView)rootView.findViewById(R.id.tv_phone_title);
        img_facebook=(ImageView)rootView.findViewById(R.id.img_facebook);
        img_twitter=(ImageView)rootView.findViewById(R.id.img_twitter);
        img_gplus=(ImageView)rootView.findViewById(R.id.img_gplus);
        img_instagram=(ImageView)rootView.findViewById(R.id.img_instagram);
        img_pin=(ImageView)rootView.findViewById(R.id.img_pin);
        animation_layout=(RelativeLayout)rootView.findViewById(R.id.settings_animation);
        pop_layout=(LinearLayout)rootView.findViewById(R.id.pop_layout);

        //Click Listners
        btn_submit.setOnClickListener(this);
        img_facebook.setOnClickListener(this);
        img_twitter.setOnClickListener(this);
        img_gplus.setOnClickListener(this);
        img_instagram.setOnClickListener(this);
        img_pin.setOnClickListener(this);
    }

    @Override
    public void onContactUsResponse(boolean status, HashMap<Integer, HashMap<Integer, ArrayList<String>>> result_details) {
        if (status) {
            HashMap<Integer, HashMap<Integer, ArrayList<String>>> map = result_details;


            LogUtility.Log(TAG, "onContactUsResponse: :success", null);

            LogUtility.Log(TAG, "onContactUsResponse:success: details", null);


            for (int i = 0; i < map.size(); i++) {
                HashMap<Integer, ArrayList<String>> map_details = map.get(i);
                if (map_details != null) {
                    for (int j = 0; j < map_details.size(); j++) {
                        ArrayList<String> list = map_details.get(j);

                        if (list != null) {
                            for (int k = 0; k < list.size(); k++) {
                                String Type,Title,Details;
                                Type = list.get(0);
                                if(!Type.equals(null))
                                {
                                    Title = list.get(1);
                                    if(Title.equals(null))Title="";
                                    Title = Title.toString().trim();

                                    Details = list.get(2);
                                    if(Details.equals(null))Details="";
                                    Details = Details.toString().trim();

                                    Fill_Details(Type, Title, Details);
                                }
                                else
                                {
                                    Toast.makeText(activity, "Contact Details Not Available", Toast.LENGTH_SHORT).show();
                                }

                                break;
                                //LogUtility.Log(TAG, "onContactUsResponse:map details:", "list item:" + list.get(k));

                            }
                        } else {
                            LogUtility.Log(TAG, "onContactUsResponse: :success:list is null", null);

                        }

                    }
                } else {
                    LogUtility.Log(TAG, "onContactUsResponse: :success:map details is null in list", null);

                }


            }


        } else {
            LogUtility.Log(TAG, "onContactUsResponse: :failure", null);
            Toast.makeText(activity, "Contact Details Not Available",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onEnquiryResponse(boolean status) {
        if (status) {
            Toast.makeText(activity, "Enquiry Send",
                    Toast.LENGTH_SHORT).show();
            LogUtility.Log(TAG, "onEnquiryResponse:success", null);
            Intent g=new Intent(activity, AfterloginKnowncity.class);
            startActivity(g);

        } else {
            Toast.makeText(activity, "Enquiry Failed",
                    Toast.LENGTH_SHORT).show();
            LogUtility.Log(TAG, "onEnquiryResponse:failure", null);
        }

    }

    public JSONObject getjsonObject(String tag) {
        if (tag.equals(TravexApplication.CONTACT_US_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfContactUs
                            (TravexApplication.ABOUT_US_CONTACT_US_TYPE

                            ),
                    mListCreatorForNameValuePairRequest.getListOfContactUs("contactus")

            );

        }else if (tag.equals(TravexApplication.ENQUIRY_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfEnquiry
                            (TravexApplication.ENQUIRY_NAME,
                                    TravexApplication.ENQUIRY_EMAIL,
                                    TravexApplication.ENQUIRY_CONTACT_NUMBER,
                                    TravexApplication.ENQUIRY_SUBJECT,
                                    TravexApplication.ENQUIRY_MESSAGE
                            ),
                    mListCreatorForNameValuePairRequest.getListOfEnquiry(name, email,
                            contactnumber,subject,message)
            );

        }
        return null;
    }


    public void postJsonRequest(int request_type, String url, JSONObject jsonObject, final int request_id) {

        mNetworkManager.postJsonRequest(request_type, url, jsonObject, request_id);
    }

    public JSONObject getJson(ArrayList<String> namePair, ArrayList<String> valuePair) {


        return mJsonObjectMaker.getJson(namePair, valuePair);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==btn_submit.getId())
        {
            String emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

            name=edit_name.getText().toString().trim();
            email=edit_email.getText().toString().trim();
            contactnumber=edit_contactnumber.getText().toString().trim();
            subject=edit_subject.getText().toString().trim();
            message=edit_message.getText().toString().trim();
            if(name.equals(""))
            {
                Toast.makeText(activity, "Please Enter Your Name",
                        Toast.LENGTH_SHORT).show();
            }

            else if(email.equals(""))
            {
                Toast.makeText(activity, "Please Enter Your Email",
                        Toast.LENGTH_SHORT).show();
            }
            else if(!email.matches(emailPattern)){
                Toast.makeText(activity, "Invalid email address",
                        Toast.LENGTH_SHORT).show();
            }
            else if(contactnumber.equals(""))
            {
                Toast.makeText(activity, "Please Enter Your Contact No.",
                        Toast.LENGTH_SHORT).show();
            }
            else if(subject.equals(""))
            {
                Toast.makeText(activity, "Please Enter Subject",
                        Toast.LENGTH_SHORT).show();
            }
            else
            {
                postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.ENQUIRY_URL,
                        getjsonObject(TravexApplication.ENQUIRY_TAG),
                        TravexApplication.REQUEST_ID_ENQUIRY);
            }




        }
        else if(v.getId()==img_facebook.getId())
        {
            String facebookURL = SociaMediaURL_List.get(0);
            CallURL(facebookURL);
        }
        else if(v.getId()==img_twitter.getId())
        {
            String twitterURL = SociaMediaURL_List.get(1);
            CallURL(twitterURL);
        }
        else if(v.getId()==img_gplus.getId())
        {
            String gplusURL = SociaMediaURL_List.get(2);
            CallURL(gplusURL);
        }
        else if(v.getId()==img_instagram.getId())
        {
            String instagramURL = SociaMediaURL_List.get(3);
            CallURL(instagramURL);
        }
        else if(v.getId()==img_pin.getId())
        {
            String pinURL = SociaMediaURL_List.get(4);
            CallURL(pinURL);
        }
    }

    public void Fill_Details(String Type,String Title,String Details)
    {

        if(Type.equals("location"))
        {
            tv_location_title.setText(Title);
            tv_address.setText(Details);
        }
        else if(Type.equals("phone"))
        {
            tv_phone_title.setText(Title);
            tv_phone.setText(Details);
        }
        else if(Type.equals("facebook"))
        {
            SociaMediaURL_List.add(Details);
        }
        else if(Type.equals("twitter"))
        {
            SociaMediaURL_List.add(Details);
        }
        else if(Type.equals("gplus"))
        {
            SociaMediaURL_List.add(Details);
        }
        else if(Type.equals("instagram"))
        {
            SociaMediaURL_List.add(Details);
        }
        else if(Type.equals("pinterest"))
        {
            SociaMediaURL_List.add(Details);
        }
    }

    public void CallURL(String URL)
    {
        Uri uri = Uri.parse(URL); // missing 'http://' will cause crashed
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

    //Animation Functions
    public void animationLoading(){
        animation_layout.setVisibility(View.VISIBLE);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                ImageView iv1 = (ImageView) rootView.findViewById(R.id.settings_one);
                ImageView iv2 = (ImageView) rootView.findViewById(R.id.settings_two);
                ImageView iv3 = (ImageView) rootView.findViewById(R.id.settings_three);

                iv1.setAnimation(rotateOne);
                iv2.setAnimation(rotateTwo);
                iv3.setAnimation(rotateThree);

                iv1.startAnimation(rotateOne);
                iv2.startAnimation(rotateTwo);
                iv3.startAnimation(rotateThree);
            }
        };
        Handler mHandler = new Handler();
        mHandler.postDelayed(runnable, 100);

    }

    public  void  animationInit(){
        rotateOne = AnimationUtils.loadAnimation(activity, R.anim.rotate_one);
        rotateOne.setAnimationListener(this);
        rotateTwo= AnimationUtils.loadAnimation(activity, R.anim.rotate_two);
        rotateTwo.setAnimationListener(this);
        rotateThree= AnimationUtils.loadAnimation(activity, R.anim.rotate_three);
        rotateThree.setAnimationListener(this);
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }


    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
