package org.technomobs.travex.Activity.Fragment;


import android.app.Activity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.lukedeighton.wheelview.WheelView;
import com.lukedeighton.wheelview.adapter.WheelArrayAdapter;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;
//import com.twotoasters.jazzylistview.JazzyEffect;
//import com.twotoasters.jazzylistview.JazzyHelper;
//import com.twotoasters.jazzylistview.recyclerview.JazzyRecyclerViewScrollListener;

import org.json.JSONObject;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Controller.Interface.AddSuggestionsResponse;
import org.technomobs.travex.Controller.Interface.GetNowResponse;
import org.technomobs.travex.Controller.Interface.GetResponseSearchResultsGeneral;
import org.technomobs.travex.Controller.Interface.GetResponseSearchResultsKeyBased;
import org.technomobs.travex.Controller.JsonObjectMaker;
import org.technomobs.travex.Controller.NetworkManager;
import org.technomobs.travex.Controller.NetworkOptions;
import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.Common;
import org.technomobs.travex.Utillity.ListCreatorForNameValuePairRequest;
import org.technomobs.travex.Utillity.LogUtility;
import org.technomobs.travex.Utillity.OnSwipeTouchListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Abhi on 03-03-2016.
 */
public class MapShowingFragment extends Fragment implements  GetResponseSearchResultsGeneral, GetResponseSearchResultsKeyBased
        ,GetNowResponse,AddSuggestionsResponse,Animation.AnimationListener {
    //By Oranz
    //For Rotaion
    LinearLayout linlay_company_details;
    ImageView fab,fab_left;
    private static int ITEM_COUNT = 20;
    LinearLayout.LayoutParams paramsleft;
    LinearLayout.LayoutParams paramsright;
    LinearLayout linlay_left,linlay;

    //private static Bitmap  imageScaled;

    FloatingActionButton actionButton;
    FloatingActionButton imageOriginal,imageScaled;
    RelativeLayout searchnotfound;
    private int dialerHeight, dialerWidth;
    MediaPlayer mp;
    String[] categories_items;
    int[] category_wheelview_icons;
    static String Search_ID="";

    static ListView cat_list;
    View rootView;
    static Context context;

    static Activity act;
    RelativeLayout animation_layout;
    int anim_item=0;
    private String backStateName;
    Animation animBounce,animaFadein,animaFadeout;
    PreferenceManager PreferenceManager;
    TextView slider_btn,tv_compnyname;
    static ListView search_list;
    JSONObject jSearchResult = null;
    ArrayList<String> searchResultsIDArrayList = new ArrayList<String>();
    ArrayList<String> searchResultsTypeArrayList = new ArrayList<String>();
    ArrayAdapter adapter;
   	static TextView category_text;
    String[] items;
    ArrayList<String> item_idList,searchitems,searchitem_idList;
    //SubActionButton[] subactbtn;
    private GoogleMap googleMap;
    //   FloatingActionButton floatButton;
    int _xDelta,_yDelta;
    ImageView search_btn,phone_click,gps_click;
    SearchListAdapter adapter1;
    RelativeLayout butons_layout;
    Animation rotateOne, rotateTwo, rotateThree;
    private static final String KEY_TRANSITION_EFFECT = "transition_effect_flip";
//    private int mCurrentTransitionEffect = JazzyHelper.FLIP;
    public static String selected_category,selected_categoryId;
    private ArrayList<CategoryListItems> categoryListItemses,searchListItems;

    ArrayList<String> id_array=new ArrayList<>();
    ArrayList<String> longitude_array=new ArrayList<>();
    ArrayList<String> latitude_array=new ArrayList<>();
    ArrayList<String> phonenumber_array=new ArrayList<>();
    //ListCreatorForNameValuePairRequest ListCreatorForNameValuePairRequest ;
    //JsonObjectMaker mJsonObjectMaker = null;
    EditText search_edit;
    Button bu1,bu2, bu3,bu4;
    String country = null;
    String country_id = null;
    String category_item = null;
    String query = null;
    TextView search_text;

    String suggestion_id = null;
    String item_id = null;
//    JazzyEffect jazzyEffect;
//    JazzyRecyclerViewScrollListener jazzyScrollListener;
    CharSequence Search_text;

    //From TechnoMobs
    public static final String TAG = Google_Map_Fragment.class.getSimpleName();
    JsonObjectMaker mJsonObjectMaker = null;
    static NetworkManager mNetworkManager = null;

    ListCreatorForNameValuePairRequest mListCreatorForNameValuePairRequest = null;
    TypedArray mTypedArray_category_icons = null;
    TypedArray TypedArray_WheelView_Icons = null;
    static ArrayList<Drawable> category_icons;
    static ArrayList<Drawable> category_icons_left;
    WheelView wheelview,wheelview_left;
    MapView mMapView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.mapshowing_fragment, container, false);
        Common.setupUI(rootView.findViewById(R.id.rel_main), getActivity());
        act = getActivity();
        PreferenceManager = new PreferenceManager(getActivity());
        mMapView = (MapView) rootView.findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume();// needed to get the map to display immediately
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        GetCurrentLocation();

        context= act;
        categories_items = new String[15];
        category_wheelview_icons = new int[15];
        category_wheelview_icons= getResources().getIntArray(R.array.category_items_spinner);
        categories_items = getResources().getStringArray(R.array.category_items_spinner);
        
        mTypedArray_category_icons = getResources().obtainTypedArray(R.array.category_icons_spinner);
        TypedArray_WheelView_Icons = getResources().obtainTypedArray(R.array.category_icons_wheelview);

        category_icons= new ArrayList<Drawable>();
        category_icons_left= new ArrayList<Drawable>();
//        for(int i=0;i<category_wheelview_icons.length;i++)
//        {
//            category_icons.add(ResourcesCompat.getDrawable(getResources(),category_wheelview_icons[i],null));
//            category_icons_left.add(ResourcesCompat.getDrawable(getResources(),category_wheelview_icons[i],null));
//        }
        for(int i=0;i<TypedArray_WheelView_Icons.length();i++)
        {
            category_icons.add(ResourcesCompat.getDrawable(getResources(), TypedArray_WheelView_Icons.getResourceId(i, -1), null));
            category_icons_left.add(ResourcesCompat.getDrawable(getResources(),TypedArray_WheelView_Icons.getResourceId(i, -1),null));
        }
//        category_icons.add(ResourcesCompat.getDrawable(getResources(),R.drawable.category_airlines,null));
//        category_icons.add(ResourcesCompat.getDrawable(getResources(), R.drawable.eatingout,null));
//        category_icons.add(ResourcesCompat.getDrawable(getResources(), R.drawable.stay_hotel,null));
//        category_icons.add(ResourcesCompat.getDrawable(getResources(), R.drawable.nightlife,null));
//        category_icons.add(ResourcesCompat.getDrawable(getResources(), R.drawable.spa,null));
//        category_icons.add(ResourcesCompat.getDrawable(getResources(), R.drawable.thingstodo,null));
//        category_icons.add(ResourcesCompat.getDrawable(getResources(), R.drawable.shopping,null));
//        category_icons.add(ResourcesCompat.getDrawable(getResources(), R.drawable.bussiness_freezone,null));
//        category_icons.add(ResourcesCompat.getDrawable(getResources(), R.drawable.relaestate,null));
//        category_icons.add(ResourcesCompat.getDrawable(getResources(), R.drawable.money,null));
//        category_icons.add(ResourcesCompat.getDrawable(getResources(), R.drawable.events,null));
//        category_icons.add(ResourcesCompat.getDrawable(getResources(), R.drawable.emergencies,null));
//        category_icons.add(ResourcesCompat.getDrawable(getResources(), R.drawable.carrental,null));
//        category_icons.add(ResourcesCompat.getDrawable(getResources(), R.drawable.embas_consultancy,null));
//        category_icons.add(ResourcesCompat.getDrawable(getResources(), R.drawable.tour_travel,null));
        

//        category_icons_left.add(ResourcesCompat.getDrawable(getResources(),R.drawable.category_airlines,null));
//        category_icons_left.add(ResourcesCompat.getDrawable(getResources(), R.drawable.eatingout,null));
//        category_icons_left.add(ResourcesCompat.getDrawable(getResources(), R.drawable.stay_hotel,null));
//        category_icons_left.add(ResourcesCompat.getDrawable(getResources(), R.drawable.nightlife,null));
//        category_icons_left.add(ResourcesCompat.getDrawable(getResources(), R.drawable.spa,null));
//        category_icons_left.add(ResourcesCompat.getDrawable(getResources(), R.drawable.thingstodo,null));
//        category_icons_left.add(ResourcesCompat.getDrawable(getResources(), R.drawable.shopping,null));
//        category_icons_left.add(ResourcesCompat.getDrawable(getResources(), R.drawable.bussiness_freezone,null));
//        category_icons_left.add(ResourcesCompat.getDrawable(getResources(), R.drawable.relaestate,null));
//        category_icons_left.add(ResourcesCompat.getDrawable(getResources(), R.drawable.money,null));
//        category_icons_left.add(ResourcesCompat.getDrawable(getResources(), R.drawable.events,null));
//        category_icons_left.add(ResourcesCompat.getDrawable(getResources(), R.drawable.emergencies,null));
//        category_icons_left.add(ResourcesCompat.getDrawable(getResources(), R.drawable.carrental,null));
//        category_icons_left.add(ResourcesCompat.getDrawable(getResources(), R.drawable.embas_consultancy,null));
//        category_icons_left.add(ResourcesCompat.getDrawable(getResources(), R.drawable.tour_travel,null));

//        LinearLayout.LayoutParams

//        paramsleft = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
//        paramsleft.gravity = Gravity.BOTTOM|Gravity.LEFT;
//
//        paramsright = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
//        paramsright.gravity = Gravity.BOTTOM|Gravity.RIGHT;

        linlay_company_details = (LinearLayout)rootView.findViewById(R.id.linlay_company_details);
        linlay_company_details.setVisibility(View.GONE);
        linlay_left = (LinearLayout)rootView.findViewById(R.id.linlay_left);
        linlay = (LinearLayout)rootView.findViewById(R.id.linlay);
        fab = (ImageView)rootView.findViewById(R.id.fab);
        fab_left= (ImageView)rootView.findViewById(R.id.fab_left);
        wheelview=(WheelView)rootView.findViewById(R.id.wheelview);
        wheelview_left=(WheelView)rootView.findViewById(R.id.wheelview_left);
        searchnotfound=(RelativeLayout)rootView.findViewById(R.id.lay_srchnotfound);
        search_text=(TextView)rootView.findViewById(R.id.search_text);
        wheelview.setVisibility(View.GONE);
        linlay.setVisibility(View.GONE);
        wheelview_left.setVisibility(View.GONE);
        linlay_left.setVisibility(View.GONE);
        ChangeToolBarDesignBackToPrevious();
        //wheelview.setLayoutParams(paramsright);
        mp = MediaPlayer.create(context, R.raw.beep);

        fab.setOnTouchListener(new OnSwipeTouchListener(act) {
            @Override
            public void onClick() {
                //mp.start();

                if (wheelview.getVisibility() == View.VISIBLE) {
                    wheelview.setVisibility(View.GONE);
                    linlay.setVisibility(View.GONE);

                } else {
                    wheelview.setVisibility(View.VISIBLE);
                    linlay.setVisibility(View.VISIBLE);
                }

                // your on click here
            }

            public void onSwipeTop()
            {
                //Toast.makeText(act, "top", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeRight() {


                //Toast.makeText(act, "right", Toast.LENGTH_SHORT).show();

            }

            public void onSwipeLeft() {
                Point point = getPointOfView(fab);
                Log.d(TAG, "view point x,y (" + point.x + ", " + point.y + ")");
                int pointdrawfrom = point.x;
                int pointdrawto = point.x * -1;
                TranslateAnimation animation = new TranslateAnimation(0.0f, pointdrawto + 120,
                        0.0f, 0.0f);          //  new TranslateAnimation(xFrom,xTo, yFrom,yTo)
                animation.setDuration(1000);  // animation duration
                //animation.setRepeatCount(5);  // animation repeat count
                //animation.setRepeatMode(2);   // repeat animation (left to right, right to left )
                //animation.setFillAfter(true);
//
                fab.startAnimation(animation);
                linlay_left.startAnimation(animation);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        fab.setVisibility(View.GONE);
                        fab_left.setVisibility(View.GONE);
                        wheelview.setVisibility(View.GONE);
                        linlay.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        //animation.clearAnimation();
                        fab_left.setVisibility(View.VISIBLE);
//                        if(wheelview.isShown())
//                        {
//                            wheelview_left.setVisibility(View.VISIBLE);
//                            linlay_left.setVisibility(View.VISIBLE);
//                        }
                        //wheelview_left.setVisibility(View.VISIBLE);

                        //fab.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                //Toast.makeText(act, "left", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeBottom() {
                //Toast.makeText(act, "bottom", Toast.LENGTH_SHORT).show();
            }


        });

        fab_left.setOnTouchListener(new OnSwipeTouchListener(act) {
            @Override
            public void onClick() {
                //mp.start();

                if(wheelview_left.getVisibility()==View.VISIBLE)
                {
                    wheelview_left.setVisibility(View.GONE);
                    linlay_left.setVisibility(View.GONE);

                }
                else
                {
                    wheelview_left.setVisibility(View.VISIBLE);
                    linlay_left.setVisibility(View.VISIBLE);

                }

            }

            public void onSwipeTop() {
                //Toast.makeText(act, "top", Toast.LENGTH_SHORT).show();
            }
            public void onSwipeRight() {

                Point point = getPointOfView(fab);
                Log.d(TAG, "view point x,y (" + point.x + ", " + point.y + ")");
                int pointdrawfrom = point.x;
                int pointdrawto = point.x * -1;
                TranslateAnimation animation = new TranslateAnimation(0.0f,pointdrawfrom - 120,
                        0.0f, 0.0f);          //  new TranslateAnimation(xFrom,xTo, yFrom,yTo)
                animation.setDuration(1000);  // animation duration
                //animation.setRepeatCount(5);  // animation repeat count
                //animation.setRepeatMode(2);   // repeat animation (left to right, right to left )
                //animation.setFillAfter(true);
//
                fab_left.startAnimation(animation);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        fab_left.setVisibility(View.GONE);
                        wheelview_left.setVisibility(View.GONE);
                        linlay_left.setVisibility(View.GONE);
                        //wheelview_left.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        //animation.clearAnimation();
                        fab.setVisibility(View.VISIBLE);
                        //wheelview.setVisibility(View.VISIBLE);
                       // wheelview.setLayoutParams(paramsright);
                        //fab.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                //Toast.makeText(act, "right", Toast.LENGTH_SHORT).show();

            }
            public void onSwipeLeft() {

                //Toast.makeText(act, "left", Toast.LENGTH_SHORT).show();
            }
            public void onSwipeBottom() {
                //Toast.makeText(act, "bottom", Toast.LENGTH_SHORT).show();
            }


        });

        //ListCreatorForNameValuePairRequest = new ListCreatorForNameValuePairRequest(act);
		//mJsonObjectMaker = new JsonObjectMaker(act);
////

        // Create an icon
        animation_layout=(RelativeLayout)rootView.findViewById(R.id.settings_animation);

        animationInit();
        ImageView icon = new ImageView(act.getApplicationContext());
        icon.setImageResource(R.drawable.red_roling_menu);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(wheelview.getVisibility()==View.VISIBLE)
//                {
//                    wheelview.setVisibility(View.GONE);
//                }
//                else
//                {
//                    wheelview.setVisibility(View.VISIBLE);
//                }
//            }
//        });

//         actionButton = new FloatingActionButton.Builder(act)
//                .setContentView(icon)
//                .build();
//        imageOriginal=actionButton;
////        SubActionButton.Builder itemBuilder = new SubActionButton.Builder(act);
//        // repeat many times:
//
//        for(int i=0;i<categories_items.length;i++)
//        {
//            SubActionButton.Builder itemBuilder = new SubActionButton.Builder(act);
//            ImageView itemIcon1 = new ImageView(act.getApplicationContext());
//            itemIcon1.setId(i);
//            itemIcon1.setImageResource(mTypedArray_category_icons.getResourceId(i, -1));
//            SubActionButton button1 = itemBuilder.setContentView(itemIcon1).build();
//            button1.setId(i);
//            subactbtn.add(button1);
//
//        }
//
//            FloatingActionMenu actionMenu=new FloatingActionMenu.Builder(act)
//            .addSubActionView(subactbtn.get(0))
//            .addSubActionView(subactbtn.get(1))
//            .addSubActionView(subactbtn.get(2))
//            .addSubActionView(subactbtn.get(3))
//            .addSubActionView(subactbtn.get(4))
//            .addSubActionView(subactbtn.get(5))
//            .addSubActionView(subactbtn.get(6))
//            .addSubActionView(subactbtn.get(7))
//            .addSubActionView(subactbtn.get(8))
//            .addSubActionView(subactbtn.get(9))
//            .addSubActionView(subactbtn.get(10))
//            .addSubActionView(subactbtn.get(11))
//            .addSubActionView(subactbtn.get(12))
//            .addSubActionView(subactbtn.get(13))
//            .addSubActionView(subactbtn.get(14))
//                    .setStartAngle(0)
//                    .setEndAngle(360)
//                    .enableAnimations()
//                    .attachTo(actionButton)
//                    .build();




    //    GetCurrentLocation();
        search_edit=(EditText)rootView.findViewById(R.id.search_edittext);
        slider_btn=(TextView)rootView.findViewById(R.id.slider_btn);
        tv_compnyname=(TextView)rootView.findViewById(R.id.tv_compnyname);
        phone_click=(ImageView)rootView.findViewById(R.id.phone_click);
        gps_click=(ImageView)rootView.findViewById(R.id.gps_click);

        phone_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               int position = id_array.indexOf(Search_ID);
                ArrayList<String> Phone_Numbers = new ArrayList<>();
                if (phonenumber_array.get(position) != null) {
                    String[] Images = phonenumber_array.get(position).toString().split("::");
                    for (int i = 0; i < Images.length; i++) {
                        Phone_Numbers.add(Images[i]);
                    }
                }




                if(Phone_Numbers.size()>0)
                {
                    if(!Phone_Numbers.get(0).equalsIgnoreCase("No Numbers"))
                    {
                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(act);
                        // ...Irrelevant code for customizing the buttons and title
                        LayoutInflater inflater = act.getLayoutInflater();

                        View dialogView = inflater.inflate(R.layout.mobilenumberlistingadapter_layout, null);

                        RecyclerView recyclerView = (RecyclerView) dialogView.findViewById(R.id.recyclerView);
                        recyclerView.setLayoutManager(new LinearLayoutManager(act));
                        MobileNumberListingAdapter adapter = new MobileNumberListingAdapter(act, Phone_Numbers);
                        recyclerView.setAdapter(adapter);

                        dialogBuilder.setView(dialogView);

                        AlertDialog alertDialog = dialogBuilder.create();
//                    alertDialog.getWindow().setLayout(200, 400);
                        alertDialog.show();
                    }
                    else
                    {
                        Toast.makeText(act,"Not available",Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        gps_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = id_array.indexOf(Search_ID);
                String Lat=latitude_array.get(position);
                String Long=longitude_array.get(position);
                if(Lat==null) Lat="";
                if(Lat=="") Lat="0";
                if(Long==null) Long="";
                if(Long=="") Long="0";
                double latitude= Double.parseDouble(Lat);
                double longitude= Double.parseDouble(Long);
                Intent navigation = new Intent(Intent.ACTION_VIEW, Uri
                        .parse("http://maps.google.com/maps?saddr="
                                + latitude + ","
                                + longitude + "&daddr="
                                + latitude + "," + longitude));
                act.startActivity(navigation);
            }
        });

        tv_compnyname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment=SearchResultFragmentSecond.newInstance(act,Search_ID);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment);
                fragmentTransaction.addToBackStack(fragment.getClass().getName());
                fragmentTransaction.commit();
            }
        });
        category_text=(TextView)rootView.findViewById(R.id.category_text);

        search_list=(ListView)rootView.findViewById(R.id.search_list);
        // floatButton=(FloatingActionButton)rootView.findViewById(R.id.fab);
        search_btn=(ImageView)rootView.findViewById(R.id.search_btn);

        //items=new ArrayList<String>();
        item_idList=new ArrayList<String>();
        //items = getResources().getStringArray(R.array.category_items_spinner);
//        items.add("Airline");
//        items.add("Shopping");
//        items.add("Buisiness");
//        items.add("Real Estate");
//        items.add("Money Exchange");
//        items.add("Car Rental");
//        items.add("Embasi $ Consulates");
//        items.add("Tour $ Travel");
//        items.add("Events");
//        items.add("Eating Out");
//        items.add("Stay/Hotels");
//        items.add("Nightlife");

//        jazzyScrollListener = new JazzyRecyclerViewScrollListener();
        mJsonObjectMaker = new JsonObjectMaker(getActivity());
        mNetworkManager = NetworkManager.getSingleInstance(getActivity());
        mNetworkManager.setOnGetResponseSearchGeneralResultsListener(this);
        mNetworkManager.setOnGetSearchResultsKeywordbasedListener(this);
        mNetworkManager.setOnGetNowResponseListener(this);
        mNetworkManager.setOnAddSuggestionsResponseListener(this);
        mListCreatorForNameValuePairRequest = new ListCreatorForNameValuePairRequest(getActivity());



        categoryListItemses = new ArrayList<CategoryListItems>();
        searchListItems = new ArrayList<CategoryListItems>();

        for(int i=0;i<categories_items.length;i++) {
//            item_idList.add(String.valueOf(i+1));
            String category_name = categories_items[i];
            String category_id = getCategory_Id_Tips_For(categories_items[i]);

            item_idList.add(getCategory_Id_Tips_For(categories_items[i]));

            categoryListItemses.add(new CategoryListItems(categories_items[i].toString(),item_idList.get(i).toString()));
        }
//        if(preferences.getboolean("logout")) {
//            item_idList.add("9999");
//            categoryListItemses.add(new CategoryListItems("Logout", "9999"));
//        }
        cat_list=(ListView)rootView.findViewById(R.id.category_list);
//        GridLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
//        cat_list.setLayoutManager(mLayoutManager);

        CategoryListAdapternew adapter = new CategoryListAdapternew(getActivity().getApplicationContext(),
                categoryListItemses,mTypedArray_category_icons);

//        SwingBottomInAnimationAdapter animationAdapter = new SwingBottomInAnimationAdapter(adapter);
//        animationAdapter.setAbsListView(cat_list);
//        cat_list.setAdapter(adapter);

//        searchListItems.add(new CategoryListItems("Air Malasia", "2"));
//        searchListItems.add(new CategoryListItems("Air India","1"));
//        searchListItems.add(new CategoryListItems("Dubai Airlines", "1"));
//        searchListItems.add(new CategoryListItems("British Airways", "1"));
//        adapter1 = new SearchListAdapter(getActivity(),getActivity().getApplicationContext(),
//                searchListItems);

//mArrayList_categoryitem = new ArrayList<CategorySpinnerItem>();
        //cat_list.setOnScrollListener(jazzyScrollListener);







//        if (savedInstanceState != null) {
//            mCurrentTransitionEffect = savedInstanceState.getInt(KEY_TRANSITION_EFFECT, JazzyHelper.FLIP);
//            setupJazziness(mCurrentTransitionEffect);
//        }
        //    cat_list.setAdapter(adapter);

//        ArrayAdapter<String> itemsAdapter =
//                new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, items);
//        cat_list.setAdapter(itemsAdapter);
        cat_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selected_category = categoryListItemses.get(position).getTitle();
                selected_categoryId = categoryListItemses.get(position).getId();
                category_text.setText(categoryListItemses.get(position).getTitle());
                cat_list.setVisibility(View.GONE);
            }
        });

        category_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (cat_list.isShown()) {
                    cat_list.setVisibility(View.GONE);
                    search_list.setVisibility(View.GONE);
                    slider_btn.setVisibility(View.VISIBLE);
                    searchnotfound.setVisibility(View.GONE);
                } else {
                    cat_list.setVisibility(View.VISIBLE);
                    search_list.setVisibility(View.GONE);
                    Categorylistviewadapter adapter = new Categorylistviewadapter(getActivity().getApplicationContext(),categoryListItemses);
                    AlphaInAnimationAdapter animationAdapter = new AlphaInAnimationAdapter(adapter);
                    animationAdapter.setAbsListView(cat_list);

                     cat_list.setDivider(getResources().getDrawable(R.color.list_item_title));

                    cat_list.setAdapter(animationAdapter);

                    slider_btn.setVisibility(View.GONE);
                    searchnotfound.setVisibility(View.GONE);

                }


            }
        });


//WheelView.Lay
        searchitems=new ArrayList<String >();
        searchitem_idList=new ArrayList<String>();
//        searchitems.add("Air Malasia");
//        searchitems.add("Air India");
//        searchitems.add("Dubai Airlines");
//        searchitems.add("British Airways");
//        searchitem_idList.add


        search_list.setVisibility(View.GONE);
        search_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String search_string = "";
                search_string=selected_category;
                if(search_string==null)search_string="";
                if(search_string.equals(""))
                {
                    Toast.makeText(act,"Please choose a category",Toast.LENGTH_SHORT).show();
                }
                else
                {

//                    animationLoading();
//
//                    Runnable run = new Runnable() {
//                        @Override
//                        public void run() {
//
//                            onEnterKeyClick();
//                            animation_layout.setVisibility(View.GONE);
//                        }
//                    };
//                    Handler handler = new Handler();
//                    handler.postDelayed(run,2000);
//                    //search_list.setAdapter(adapter1);
                    //suggestion_id = searchResultsIDArrayList.get(position).toString();
                    country_id = PreferenceManager.getSelectedcountryId();
                    if (country_id == null)country_id="";
                    if(suggestion_id==null)suggestion_id="";

                    Fragment fragment=searchresultFragment.newInstance(act,suggestion_id,country_id);


                    if (fragment != null) {
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.frame_container, fragment);
                        fragmentTransaction.addToBackStack(fragment.getClass().getName());
                        fragmentTransaction.commit();
                    }
                }

            }
        });
        search_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                search_list.setVisibility(View.GONE);

                //search_list.setAdapter(adapter1);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                animationLoading();
                Search_text = s;
                // Get changed text s here and use it
                Runnable run = new Runnable() {
                    @Override
                    public void run() {
//                        for (int j = 0; j < searchListItems.size(); j++) {
//                            if (!((searchListItems.get(j).getTitle().toString()).toLowerCase()).contains(Search_text)) {
//                                searchListItems.remove(j);
//
//                            } else {
//                                search_list.setVisibility(View.VISIBLE);
//                            }
//                        }
                        //query = Search_text.toString().trim();
                        onEnterKeyClick();
                        animation_layout.setVisibility(View.GONE);

                        //if (searchListItems.size() != 0)
                        {
                            search_list.setVisibility(View.VISIBLE);
                            cat_list.setVisibility(View.GONE);
                            //search_list.setAdapter(adapter1);
                        }
                    }
                };
                Handler handler = new Handler();
                handler.postDelayed(run,2000);
            }

        });
        search_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                suggestion_id = searchResultsIDArrayList.get(position).toString();
                if(suggestion_id==null)suggestion_id="";

                Fragment fragment=searchresultFragment.newInstance(act,suggestion_id,country_id);


                if (fragment != null) {
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_container, fragment);
                    fragmentTransaction.addToBackStack(fragment.getClass().getName());
                    fragmentTransaction.commit();
                }
                //item_id = ((ProfileActivity) getActivity()).fetchItemidFortheSuggestion(suggestion);
            }
        });




        //Fill WheelView

        wheelview = (WheelView) rootView.findViewById(R.id.wheelview);

        //create data for the adapter
        ITEM_COUNT=category_icons.size();
        List<Map.Entry<String, Integer>> entries = new ArrayList<Map.Entry<String, Integer>>(ITEM_COUNT);
        for (int i = 0; i < ITEM_COUNT; i++) {
            Map.Entry<String, Integer> entry = MaterialColor.random(context, "\\D*_500$");
            entries.add(entry);
        }

        //populate the adapter, that knows how to draw each item (as you would do with a ListAdapter)
        wheelview.setAdapter(new WheelViewAdapter(entries));
        wheelview_left.setAdapter(new WheelViewLeftAdapter(entries));

        //a listener for receiving a callback for when the item closest to the selection angle changes
        wheelview.setOnWheelItemSelectedListener(new WheelView.OnWheelItemSelectListener() {
            @Override
            public void onWheelItemSelected(WheelView parent, Drawable itemDrawable, int position) {
                //get the item at this position
                Map.Entry<String, Integer> selectedEntry = ((WheelViewAdapter) parent.getAdapter()).getItem(position);
                parent.setSelectionColor(getContrastColor(selectedEntry));


            }
        });

        wheelview.setOnWheelItemClickListener(new WheelView.OnWheelItemClickListener() {
            @Override
            public void onWheelItemClick(WheelView parent, int position, boolean isSelected) {

                String msg = String.valueOf(position) + " " + isSelected;
                //Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                country = PreferenceManager.getSelectedCountryName();
                selected_category = categoryListItemses.get(position).getTitle();
                selected_categoryId = categoryListItemses.get(position).getId();
                MapShowingFragment.category_text.setText(selected_category);
                //country_id = fetchCountryIdForCountry(country);
                country_id = PreferenceManager.getSelectedcountryId();
                category_item = selected_category;
                WheelViewClick();

            }
        });


        //a listener for receiving a callback for when the item closest to the selection angle changes
        wheelview_left.setOnWheelItemSelectedListener(new WheelView.OnWheelItemSelectListener() {
            @Override
            public void onWheelItemSelected(WheelView parent, Drawable itemDrawable, int position) {
                //get the item at this position
                Map.Entry<String, Integer> selectedEntry = ((WheelViewLeftAdapter) parent.getAdapter()).getItem(position);
                parent.setSelectionColor(getContrastColor(selectedEntry));
            }
        });

        wheelview_left.setOnWheelItemClickListener(new WheelView.OnWheelItemClickListener() {
            @Override
            public void onWheelItemClick(WheelView parent, int position, boolean isSelected) {
                String msg = String.valueOf(position) + " " + isSelected;
                //Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                country = PreferenceManager.getSelectedCountryName();
                selected_category=categoryListItemses.get(position).getTitle();
                selected_categoryId = categoryListItemses.get(position).getId();
                MapShowingFragment.category_text.setText(selected_category);
                //country_id = fetchCountryIdForCountry(country);
                country_id =  PreferenceManager.getSelectedcountryId();
                category_item = selected_category;
                WheelViewClick();

            }
        });

        //initialise the selection drawable with the first contrast color
        wheelview.setSelectionColor(getContrastColor(entries.get(0)));
        wheelview_left.setSelectionColor(getContrastColor(entries.get(0)));

        butons_layout=(RelativeLayout)rootView.findViewById(R.id.butons_layout);
        animaFadein = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_left);
        animaFadein.setAnimationListener(this);
        animaFadeout = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_out_left);
        animaFadeout.setAnimationListener(this);


        animBounce = AnimationUtils.loadAnimation(getActivity(), R.anim.animationex);
        animBounce.setAnimationListener(this);
        bu1=(Button)rootView.findViewById(R.id.button1);
        bu2=(Button)rootView.findViewById(R.id.button2);
        bu3=(Button)rootView.findViewById(R.id.button3);
        bu4=(Button)rootView.findViewById(R.id.button4);
        butons_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                butons_layout.setVisibility(View.GONE);
                butons_layout.setAnimation(animaFadeout);
                butons_layout.startAnimation(animaFadeout);

                slider_btn.setVisibility(View.VISIBLE);
            }
        });
        slider_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                anim_item = 0;
                bu2.setVisibility(View.INVISIBLE);
                bu3.setVisibility(View.INVISIBLE);
                bu4.setVisibility(View.INVISIBLE);
                butons_layout.setVisibility(View.VISIBLE);
                butons_layout.setAnimation(animaFadein);
                butons_layout.startAnimation(animaFadein);
                bu1.setAnimation(animBounce);
                bu1.startAnimation(animBounce);

                slider_btn.setVisibility(View.GONE);


            }
        });
        bu1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment=new Myplan();


                if (fragment != null) {
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.frame_container, fragment).commit();
                    backStateName = fragment.getClass().getName();
                    boolean fragmentPopped = fragmentManager
                            .popBackStackImmediate(backStateName, 0);



                }
            }
        });
        bu2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment=TravelEssential.newInstance(act);


                if (fragment != null) {
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.frame_container, fragment).commit();
                    backStateName = fragment.getClass().getName();
                    boolean fragmentPopped = fragmentManager
                            .popBackStackImmediate(backStateName, 0);



                }
            }
        });
        bu3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment= TravexTipsFragment.newInstance(act);


                if (fragment != null) {
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.frame_container, fragment).commit();
                    backStateName = fragment.getClass().getName();
                    boolean fragmentPopped = fragmentManager
                            .popBackStackImmediate(backStateName, 0);



                }
            }
        });
        bu4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = ArticleFragment.newInstance(getActivity());

                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment);
//            fragmentStack.lastElement().onPause();
//            fragmentTransaction.hide(fragmentStack.lastElement());
//            fragmentStack.push(fragment);
                fragmentTransaction.addToBackStack(fragment.getClass().getName());
                fragmentTransaction.commit();
            }
        });


        /*
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //wheelView.setSelectionAngle(-wheelView.getAngleForPosition(5));
                wheelView.setMidSelected();
            }
        }, 3000); */



                //////////////////////
//        floatButton.setBackgroundColor(Color.parseColor("#a81d1b"));
//
//        floatButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(getActivity().getApplicationContext(), "Clicked", Toast.LENGTH_LONG).show();
//            }
//        });
//                floatButton.setOnDragListener(new View.OnDragListener() {
//                    @Override
//                    public boolean onDrag(View v, DragEvent event) {
//                       // floatButton.hide();
//                        Toast.makeText(getActivity().getApplicationContext(), "Clicked", Toast.LENGTH_LONG).show();
//                        return true;
//                    }
//                });
//        floatButton.setOnTouchListener(new View.OnTouchListener()
//        {
//            public boolean onTouch(View v, MotionEvent e)
//            {
//                final int X = (int) e.getRawX();
//                final int Y = (int) e.getRawY();
//                switch(e.getAction() & MotionEvent.ACTION_MASK)
//                {
//                    case MotionEvent.ACTION_DOWN:
//                        RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) v.getLayoutParams();
//                        _xDelta = X - lParams.leftMargin;
//                        _yDelta = Y - lParams.topMargin;
//                        break;
//                    case MotionEvent.ACTION_UP:
//                        break;
//                    case MotionEvent.ACTION_POINTER_DOWN:
//                        break;
//                    case MotionEvent.ACTION_POINTER_UP:
//                        break;
//                    case MotionEvent.ACTION_MOVE:
//                        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) v.getLayoutParams();
//                        layoutParams.leftMargin = Math.max((X - _xDelta), 0);
//                        layoutParams.topMargin = Math.max((Y - _yDelta),0);
//                        layoutParams.rightMargin = 0;
//                        layoutParams.bottomMargin = 0;
//                        v.setLayoutParams(layoutParams);
//
//                        break;
//                }
//                rootView.invalidate();
//                return false;
//            }
//        });
                ///////////////////////

        String Welcome_Message = PreferenceManager.getWelcome_Message();
        if(!Welcome_Message.equals(""))
        {
            PreferenceManager.setWelcome_Message("");
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(act);
            alertDialogBuilder.setMessage(Welcome_Message);

            alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {

                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }

        return rootView;
    }


    public void ChangeToolBarDesignBackToPrevious()
    {
        //To change the current design of the Toolbar in this fragment
        Toolbar toolbar = (Toolbar)getActivity().findViewById(R.id.tool_bar);
        TextView textToolHeader = (TextView) toolbar.findViewById(R.id.tv_category);
        //textToolHeader.setText(MapShowingFragment.selected_category);
        textToolHeader.setVisibility(View.GONE);
        SimpleDraweeView image = (SimpleDraweeView)toolbar.findViewById(R.id.networkImageViewEqWNH);
        image.setVisibility(View.VISIBLE);

        toolbar.setNavigationIcon(R.drawable.ic_action_menu);
    }

    //WheelView Functions

    //get the materials darker contrast
    private int getContrastColor(Map.Entry<String, Integer> entry) {
        String colorName = MaterialColor.getColorName(entry);
        return MaterialColor.getContrastColor(colorName);
    }

    public void CallSuggestionAPI()
    {
        postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.ADD_SUGGESTIONS_URL,
                getjsonObject(TravexApplication.ADD_SUGGESTIONS_TAG),
                TravexApplication.REQUEST_ID_ADD_SUGGESTIONS);
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        //getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }

    @Override
    public void onGetNowResponse(boolean status, HashMap<Integer, ArrayList<String>> jsonobject, HashMap<Integer, ArrayList<String>> array_phonenumbers) {
        longitude_array=new ArrayList<>();
        latitude_array=new ArrayList<>();
        phonenumber_array=new ArrayList<>();
        id_array=new ArrayList<>();

        if (status) {
            googleMap.clear();
            for(int i=0;i<jsonobject.size();i++)
            {
                double lat_double=0,long_double=0;



                ArrayList<String> phone = array_phonenumbers.get(0);

                if(phone != null)
                {
                    String PhoneNumber = "";
                    for(int ph=0;ph<phone.size();ph++)
                    {
                        PhoneNumber += phone.get(ph)+"::";
                    }
                    phonenumber_array.add(PhoneNumber);
                }
                else
                {
                    phonenumber_array.add("No Numbers::");
                }

                String Id=jsonobject.get(i).get(0);
                id_array.add(Id);

                String Title=jsonobject.get(i).get(1);
                if(Title==null)Title="";


                String latitude=  jsonobject.get(i).get(2);
                String longitude= jsonobject.get(i).get(3);

                if(latitude==null)latitude="0";
                if(longitude==null)longitude="0";
                latitude_array.add(latitude);
                longitude_array.add(longitude);


                if(latitude != null && longitude !=null)
                {
                    latitude=latitude.trim();
                    longitude=longitude.trim();
                    if(latitude=="")latitude = "0";
                    if(longitude=="")longitude = "0";
                    try {
                        lat_double = Double.parseDouble(latitude);
                        long_double = Double.parseDouble(longitude);
                    }
                    catch(NumberFormatException exc)
                    {
                        lat_double=0;
                        long_double=0;
                        Toast.makeText(act,"Invalid Latitude and Longitude",Toast.LENGTH_SHORT).show();
                    }
                    catch(Exception exc)
                    {
                        lat_double=0;
                        long_double=0;
                    }


                    FillMarker(selected_category, lat_double, long_double,Title+"::"+Id.trim());


                }
            }
            LogUtility.Log(TAG, "onGetNowResponse:true", null);
        } else {

            LogUtility.Log(TAG, "onGetNowResponse:false", null);

        }
    }


    static class WheelViewAdapter extends WheelArrayAdapter<Map.Entry<String, Integer>> {
        WheelViewAdapter(List<Map.Entry<String, Integer>> entries) {
            super(entries);
        }

        @Override
        public Drawable getDrawable(int position) {
            Drawable[] drawable = new Drawable[] {createOvalDrawable(getItem(position).getValue()),
                    category_icons.get(position)
            };
            return new LayerDrawable(drawable);
        }

        private Drawable createOvalDrawable(int color) {
            ShapeDrawable shapeDrawable = new ShapeDrawable(new OvalShape());
            //shapeDrawable.getPaint().setColor(color);
            return shapeDrawable;
        }
    }

    static class WheelViewLeftAdapter extends WheelArrayAdapter<Map.Entry<String, Integer>> {
        WheelViewLeftAdapter(List<Map.Entry<String, Integer>> entries) {
            super(entries);
        }

        @Override
        public Drawable getDrawable(int position) {
            Drawable[] drawable = new Drawable[] {createOvalDrawable(getItem(position).getValue()),
                    category_icons_left.get(position)
            };
            return new LayerDrawable(drawable);
        }

        private Drawable createOvalDrawable(int color) {
            ShapeDrawable shapeDrawable = new ShapeDrawable(new OvalShape());
            //shapeDrawable.getPaint().setColor(color);
            return shapeDrawable;
        }
    }

//    public JSONObject getjsonObject(String tag) {
//        if (tag.equals(TravexApplication.REGISTER_TAG)) {
//            return getJson(ListCreatorForNameValuePairRequest.getListOfSearchItemSpecificApi(TravexApplication.GET_CATEGORY_NOW, TravexApplication.GET_CITY_NOW, TravexApplication.GET_AREA_NOW),
//                    ListCreatorForNameValuePairRequest.getListOfSearchItemSpecificApi(mEditText_Name.getText().toString(), mEditText_Email.getText().toString(), mEditText_Password.getText().toString()));
//
//        }
//
//
//
//        return null;
//    }

//    public JSONObject getJson(ArrayList<String> namePair, ArrayList<String> valuePair) {
//
//
//        return mJsonObjectMaker.getJson(namePair, valuePair);
//    }

    private  void GetCurrentLocation() {


        double lat =0;
        double lng =0;
        //PreferenceManager mPreferenceManager = new PreferenceManager(getActivity());

        // lat = Double.parseDouble(mPreferenceManager.getCurrentLatitude());
//        lng=  Double.parseDouble(mPreferenceManager.getCurrentLongitude());

//        double[] d = getlocation();
//        lat = d[0];
//        lng = d[1];
        country = PreferenceManager.getSelectedCountryName();
        LogUtility.Log(TAG, "onEnterKeyClick:", "selected country from actionbar:" + country);
//        country_id = fetchCountryIdForCountry(country);
        country_id =  PreferenceManager.getSelectedcountryId();
        try{
            lat = Double.parseDouble(PreferenceManager.getSelectedLatitude());
            lng = Double.parseDouble(PreferenceManager.getSelectedLongitude());
        }
        catch(Exception ex)
        {
            lat=0;
            lng=0;
        }




            if (googleMap != null) {
                googleMap.clear();
            }
            googleMap = mMapView.getMap();

        /**
         * this is for  add suggestions api
         */
        if(lat!=0 || lng!=0) {
            //MarkerOptions marker = new MarkerOptions().position(new LatLng(lat, lng)).title(country);

            // Changing marker icon
            //marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE));

            // adding marker
            //googleMap.addMarker(marker);
            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(lat, lng)).zoom(10.0f).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

//        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat,lng), 10.0f));
//
//        Marker TP = googleMap.addMarker(new MarkerOptions().
//                position(new LatLng(lat, lng)).title("Current Location").icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_embas_consultancy)));
            googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override

                public boolean onMarkerClick(Marker marker) {
                    String TitleandID=marker.getTitle();
                    String[] TitleandID_array=TitleandID.split("::");
                    String Title=TitleandID_array[0];
                    Search_ID=TitleandID_array[1];

                    tv_compnyname.setText(Title);
                    if (linlay_company_details.getVisibility() == View.GONE) {
                        linlay_company_details.setVisibility(View.VISIBLE);
                    }
                    return false;
                }
            });

            googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    linlay_company_details.setVisibility(View.GONE);
                }
            });
        }
    }

    public double[] getlocation() {
        LocationManager lm = (LocationManager)getActivity().getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        List<String> providers = lm.getProviders(true);

        Location l = null;
        for (int i = 0; i < providers.size(); i++) {
            try {
                l = lm.getLastKnownLocation(providers.get(i));
            }catch(Exception e){
                e.printStackTrace();//lm.getLastKnownLocation(providers.get(i));
            }
            if (l != null)
                break;
        }
        double[] gps = new double[2];

        if (l != null) {
            gps[0] = l.getLatitude();
            gps[1] = l.getLongitude();
        }
        return gps;
    }




//    @Override
//    public void onResume(){
//        super.onResume();
//        GetCurrentLocation();
//        // put your code here...
//
//    }

    private MapFragment getMapFragment() {
        FragmentManager fm = null;

        Log.d("v", "sdk: " + Build.VERSION.SDK_INT);
        Log.d("v", "release: " + Build.VERSION.RELEASE);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Log.d("v", "using getFragmentManager");
            fm = getFragmentManager();
        } else {
            Log.d("v", "using getChildFragmentManager");
            fm = getChildFragmentManager();
        }

        return (MapFragment) fm.findFragmentById(R.id.map);
    }

//    @Override
//    public void onDestroyView() {
//        super.onDestroyView();
//        try {
//            MapFragment f = getMapFragment();
//            if (f != null){
//                getFragmentManager().beginTransaction().remove(f).commit();
//            }
//        }
//        catch (Exception exc)
//        {}
//
//    }

    public void animationLoading(){
        animation_layout.setVisibility(View.VISIBLE);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                ImageView iv1 = (ImageView) rootView.findViewById(R.id.settings_one);
                ImageView iv2 = (ImageView) rootView.findViewById(R.id.settings_two);
                ImageView iv3 = (ImageView) rootView.findViewById(R.id.settings_three);

                iv1.setAnimation(rotateOne);
                iv2.setAnimation(rotateTwo);
                iv3.setAnimation(rotateThree);

                iv1.startAnimation(rotateOne);
                iv2.startAnimation(rotateTwo);
                iv3.startAnimation(rotateThree);
            }
        };
        Handler mHandler = new Handler();
        mHandler.postDelayed(runnable, 100);

    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if (anim_item == 0) {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    bu2.setAnimation(animBounce);
                    bu2.setVisibility(View.VISIBLE);
                    bu2.startAnimation(animBounce);
                    anim_item = 1;
                }
            };
            Handler mHandler = new Handler();
            mHandler.postDelayed(runnable, 200);

        } else if (anim_item == 1) {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    bu3.setAnimation(animBounce);
                    bu3.setVisibility(View.VISIBLE);
                    bu3.startAnimation(animBounce);
                    anim_item = 2;
                }
            };
            Handler mHandler = new Handler();
            mHandler.postDelayed(runnable, 250);
        } else if (anim_item == 2) {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    bu4.setAnimation(animBounce);
                    bu4.setVisibility(View.VISIBLE);
                    bu4.startAnimation(animBounce);
                    anim_item =3;
                }
            };
            Handler mHandler = new Handler();
            mHandler.postDelayed(runnable, 200);

        }

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
    public  void  animationInit(){
        rotateOne = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_one);
        rotateOne.setAnimationListener(this);
        rotateTwo= AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_two);
        rotateTwo.setAnimationListener(this);
        rotateThree= AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_three);
        rotateThree.setAnimationListener(this);
    }

//    private void setupJazziness(int effect) {
//        mCurrentTransitionEffect = effect;
//        jazzyScrollListener.setTransitionEffect(mCurrentTransitionEffect);
//    }





    //From TechnoMobs
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        LogUtility.Log(TAG, "onActivityCreated", null);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LogUtility.Log(TAG, "onActivityResult", null);
    }

//    @Override
//    public void onAttach(Context context) {
//        GetCurrentLocation();
//    }


    /*@Override
    public void onAttach(Context context) {
        super.onAttach(context);
        LogUtility.Log(TAG, "onAttach", null);
        mJsonObjectMaker = new JsonObjectMaker(getActivity());
        mNetworkManager = NetworkManager.getSingleInstance(getActivity());
        mNetworkManager.setOnGetResponseSearchGeneralResultsListener(this);
        mNetworkManager.setOnGetSearchResultsKeywordbasedListener(this);
        mPreferenceManager = new PreferenceManager(getActivity());
        mListCreatorForNameValuePairRequest = new ListCreatorForNameValuePairRequest(getActivity());
        categories_items = new String[15];
        categories_items = getResources().getStringArray(R.array.category_items_spinner);
        mTypedArray_category_icons = getResources().obtainTypedArray(R.array.category_icons_spinner);

        categoryListItemses = new ArrayList<CategoryListItems>();
        searchListItems = new ArrayList<CategoryListItems>();

        for(int i=0;i<categories_items.length;i++) {
            item_idList.add(String.valueOf(i+1));
            categoryListItemses.add(new CategoryListItems(categories_items[i].toString(),item_idList.get(i).toString()));
        }
        cat_list=(RecyclerView)rootView.findViewById(R.id.category_list);
        GridLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
        cat_list.setLayoutManager(mLayoutManager);

        CategoryListAdapternew adapter = new CategoryListAdapternew(getActivity().getApplicationContext(),
                categoryListItemses,mTypedArray_category_icons);

//        SwingBottomInAnimationAdapter animationAdapter = new SwingBottomInAnimationAdapter(adapter);
//        animationAdapter.setAbsListView(cat_list);
        cat_list.setAdapter(adapter);

        searchListItems.add(new CategoryListItems("Air Malasia","2"));
        searchListItems.add(new CategoryListItems("Air India","1"));
        searchListItems.add(new CategoryListItems("Dubai Airlines", "1"));
        searchListItems.add(new CategoryListItems("British Airways", "1"));
        adapter1 = new SearchListAdapter(getActivity(),getActivity().getApplicationContext(),
                searchListItems);

        //mArrayList_categoryitem = new ArrayList<CategorySpinnerItem>();
        cat_list.setOnScrollListener(jazzyScrollListener);
//        for (int i = 0; i < 15; i++) {
//            mCategorySpinnerItem = new CategorySpinnerItem(mTypedArray_category_icons.getResourceId(i, -1), categories_items[i]);
//
//            mArrayList_categoryitem.add(mCategorySpinnerItem);
//        }

        //mCategoriesSpinnerAdapter = new CategoriesSpinnerAdapter(getActivity(), mArrayList_categoryitem);

    }*/

    @Override
    public void onGetResponseSearchResultsGeneral(boolean status, HashMap<Integer, ArrayList<String>> jsonObjectItems, HashMap<Integer, HashMap<Integer, ArrayList<String>>> jsonObjectArrayItems,String advertisement_img,String advertisement_url) {
        if (status) {
            LogUtility.Log(TAG, "onGetResponseSearchResultsGeneral:true", null);
            HashMap<Integer, ArrayList<String>> result_details = jsonObjectItems;

            for (int i = 0; i < result_details.size(); i++) {
                ArrayList<String> list = result_details.get(i);
//                if (map_details != null) {
//                    for (int j = 0; j < map_details.size(); j++) {
//                        ArrayList<String> list = map_details.get(j);

                        if (list != null) {
                            for (int k = 0; k < list.size(); k++) {
                                String Type,Title;
                         //       String Details;
                                Type = list.get(0);
                                if(!Type.equals(null))
                                {
                                    Title = list.get(1);
                                    if(Title.equals(null))Title="";
                                    Title = Title.toString().trim();

//                                    Details = list.get(2);
//                                    if(Details.equals(null))Details="";
//                                    Details = Details.toString().trim();

                                    //Fill_Details(Type, Title, Details);
                                }
                                else
                                {
                                    Toast.makeText(getActivity(), "Contact Details Not Available",
                                            Toast.LENGTH_SHORT).show();
                                }
                                //if(list.get(k))
                                LogUtility.Log(TAG, "onContactUsResponse:map details:", "list item:" + list.get(k));
                            }
                        } else {
                            LogUtility.Log(TAG, "onContactUsResponse: :success:list is null", null);

                        }
                    }
            }
         else {

            LogUtility.Log(TAG, "onGetResponseSearchResultsGeneral:false", null);

        }

    }

    @Override
    public void onGetSearchResultsKeyWordBased(boolean status,HashMap<Integer, ArrayList<String>> target) {

        searchResultsIDArrayList.clear();
        searchListItems.clear();
        searchnotfound.setVisibility(View.GONE);

        try {

            if (status) {

                for (int i = 0; i < target.size(); i++) {

                    searchResultsIDArrayList.add(target.get(i).get(2));
                    searchResultsTypeArrayList.add(target.get(i).get(1));

                    searchListItems.add(new CategoryListItems(target.get(i).get(0), target.get(i).get(2)));
                }

                Log.e("SEARCHRESULTSARRAYLIST", searchListItems.toString());
                if (searchListItems.size() != 0) {
                    adapter1 = new SearchListAdapter(getActivity(), getActivity().getApplicationContext(),
                            searchListItems,searchResultsIDArrayList,searchResultsTypeArrayList);
                    search_list.setVisibility(View.VISIBLE);
                    cat_list.setVisibility(View.GONE);

                }
                search_list.setAdapter(adapter1);
                Log.e("SEARCHRESULTSARRAYLIST ", String.valueOf(searchListItems.size()));
                LogUtility.Log(TAG, "onGetSearchResultsKeyWordBased:success", null);
            } else {
                LogUtility.Log(TAG, "onGetSearchResultsKeyWordBased:failed", null);
                searchnotfound.setVisibility(View.VISIBLE);
                search_text.setText(query);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    @Override
    public void onAddSuggestionsResponse(boolean status) {
        if (status) {

            LogUtility.Log(TAG, "onAddSuggestionsResponse: :success", null);


        } else {
            LogUtility.Log(TAG, "onAddSuggestionsResponse: :failure", null);
        }
    }




    public String fetchCountryIdForCountry(String country) {
        String countryId = null;
        int index = 0;
        ArrayList<String> countrynames = PreferenceManager.getSupportedCountryNames();
        index = countrynames.lastIndexOf(country);
        LogUtility.Log(TAG, "fetchCountryIdForCountry", "index:" + index);

        countryId = PreferenceManager.getSupportedCountryId().get(index);
        LogUtility.Log(TAG, "fetchCountryIdForCountry", "countryId:" + countryId);


        return countryId;
    }





    public void onEnterKeyClick() {
        query=search_edit.getText().toString();
        LogUtility.Log(TAG, "onEnterKeyClick", null);
        country = PreferenceManager.getSelectedCountryName();
        LogUtility.Log(TAG, "onEnterKeyClick:", "selected country from actionbar:" + country);
//        country_id = fetchCountryIdForCountry(country);
        country_id =  PreferenceManager.getSelectedcountryId();
        LogUtility.Log(TAG, "onEnterKeyClick:", "country id:" + country_id);
        if (selected_category != null) {
            category_item = selected_category;
            LogUtility.Log(TAG, "onEnterKeyClick:l", "item at 0th is:" + category_item);


            if (query != null) {

                if (query.trim().length() > 0) {
                    LogUtility.Log(TAG, "onEnterKeyClick:query is not null", null);
                    postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_RESULTS_KEYBASED_URL,
                            getjsonObject(TravexApplication.GET_KEYWORDSEARCH_RESULT_TAG), TravexApplication.REQUEST_ID_GET_RESULTS_KEYWORD);

                } else {
                    LogUtility.Log(TAG, "onEnterKeyClick:query is  null", null);

//                    postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_RESULTS_GENERAL_URL,
//                            getjsonObject(TravexApplication.GET_GENERAL_RESULT_TAG), TravexApplication.REQUEST_ID_GET_RESULTS_GENERAL);

                    search_list.setVisibility(View.GONE);
                    cat_list.setVisibility(View.GONE);


                }


            } else {


//                searchResultsIDArrayList.clear();
//                searchListItems.clear();
//                adapter1 = new SearchListAdapter(getActivity(), getActivity().getApplicationContext(),
//                        searchListItems,searchResultsIDArrayList);
                search_list.setVisibility(View.GONE);
                cat_list.setVisibility(View.GONE);
//                search_list.setAdapter(adapter1);
//                LogUtility.Log(TAG, "onEnterKeyClick:query is  null", null);
//
//                postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_RESULTS_NOW_URL,
//                        getjsonObject(TravexApplication.NOW_TAG), TravexApplication.REQUEST_ID_NOW);
//
//
///**
// *   search item specific
// */
//                suggestion = mPreferenceManager.getSearchResultsKeyWord_suggestion().get(0);
//                LogUtility.Log(TAG, "onEnterKeyClick:query is null", "suggestion:" + suggestion);
//                id = ((ProfileActivity) getActivity()).fetchItemidFortheSuggestion(suggestion);


            /*postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_RESULTS_SEARCHITEM_SPECIFIC_URL,
                    getjsonObject(TravexApplication.GET_SEARCHITEM_TAG), TravexApplication.REQUEST_ID_GET_SEARCH_ITEM);*/

                /**
                 *
                 * the below post for search general
                 */

           /* postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_RESULTS_GENERAL_URL,
                    getjsonObject(TravexApplication.GET_GENERAL_RESULT_TAG), TravexApplication.REQUEST_ID_GET_RESULTS_GENERAL);*/
            }


        } else {
            LogUtility.Log(TAG, "onEnterKeyClick:", "category item:" + category_item);

        }


    }




    public void postJsonRequest(int request_type, String url, JSONObject jsonObject, final int request_id) {

        mNetworkManager.postJsonRequest(request_type, url, jsonObject, request_id);
    }
    public void postJsonRequest(int request_type, final Map<String, String> request_header, String url, JSONObject jsonObject, final int request_id) {

        mNetworkManager.postJsonRequest(request_type, request_header, url, jsonObject, request_id);


    }

    public JSONObject getJson(ArrayList<String> namePair, ArrayList<String> valuePair,String tag) {

        return mJsonObjectMaker.getJson(namePair, valuePair);
    }

    public JSONObject getjsonObject(String tag) {
        if (tag.equals(TravexApplication.GET_GENERAL_RESULT_SEARCH_TERM_TAG)) {

            return getJson(mListCreatorForNameValuePairRequest.getListOfGeneralResultsSearchApi
                    (TravexApplication.GET_GENERAL_RESULT_CATEGORY,
                            TravexApplication.GET_GENERAL_RESULT_CITYID,
                            TravexApplication.GET_GENERAL_RESULT_AREA,
                            TravexApplication.GET_GENERAL_RESULT_SEARCHTERM,
                            TravexApplication.GET_GENERAL_RESULT_ALPHABET,
                            TravexApplication.GET_GENERAL_RESULT_START,
                            TravexApplication.GET_GENERAL_RESULT_LIMIT)
                    , mListCreatorForNameValuePairRequest.getListOfGeneralResultsSearchApi
                    (category_item, country_id, "", suggestion_id, "", "", ""), tag);



        } else if (tag.equals(TravexApplication.GET_GENERAL_RESULT_TAG)) {

            return getJson(mListCreatorForNameValuePairRequest.getListOfGeneralResultsSearchApi
                    (TravexApplication.GET_GENERAL_RESULT_CATEGORY,
                            TravexApplication.GET_GENERAL_RESULT_CITYID,
                            TravexApplication.GET_GENERAL_RESULT_AREA,
                            TravexApplication.GET_GENERAL_RESULT_SEARCHTERM,
                            TravexApplication.GET_GENERAL_RESULT_ALPHABET,
                            TravexApplication.GET_GENERAL_RESULT_START,
                            TravexApplication.GET_GENERAL_RESULT_LIMIT), mListCreatorForNameValuePairRequest.getListOfGeneralResultsSearchApi
                    (category_item, country_id, "", "", "", "", ""), tag);


        } else if (tag.equals(TravexApplication.GET_KEYWORDSEARCH_RESULT_TAG)) {

            return getJson(mListCreatorForNameValuePairRequest.getListOfKeywordbasedlResultsSearchApi
                    (TravexApplication.GET_KEYBASED_RESULT_CATEGORY,
                            TravexApplication.GET_KEYBASED_RESULT_CITYID,
                            TravexApplication.GET_KEYBASED_RESULT_AREA,
                            TravexApplication.GET_KEYBASED_RESULT_TERM

                    ), mListCreatorForNameValuePairRequest.getListOfKeywordbasedlResultsSearchApi
                    (category_item, country_id, "", query), tag);
        } else if (tag.equals(TravexApplication.GET_SEARCHITEM_TAG)) {

            return getJson(mListCreatorForNameValuePairRequest
                            .getListOfSearchItemSpecificApi(TravexApplication.GET_SEARCH_ITEM_CATEGORY, TravexApplication.GET_SEARCH_ITEM_ID),
                    mListCreatorForNameValuePairRequest.getListOfSearchItemSpecificApi(category_item, item_id), tag);


        } else if (tag.equals(TravexApplication.NOW_TAG)) {

            return getJson(mListCreatorForNameValuePairRequest
                            .getListOfSearchNowApi(TravexApplication.GET_CATEGORY_NOW,
                                    TravexApplication.GET_CITY_NOW,
                                    TravexApplication.GET_AREA_NOW,
                                    TravexApplication.GET_USER_FLAG_NOW,
                                    TravexApplication.GET_USER_LATITUDE_NOW,
                                    TravexApplication.GET_USER_LONGITUDE_NOW),
                    mListCreatorForNameValuePairRequest.getListOfSearchNowApi(category_item, country_id, "","1",PreferenceManager.getSelectedLatitude(),PreferenceManager.getSelectedLongitude()),tag);


        }else if (tag.equals(TravexApplication.ADD_SUGGESTIONS_TAG)) {
            return getJson(
                    mListCreatorForNameValuePairRequest.getListOfAddSuggestions
                            (TravexApplication.ADD_SUGGESTIONS_MEMBER_ID,
                                    TravexApplication.ADD_SUGGESTIONS_CATEGORYID,
                                    TravexApplication.ADD_SUGGESTIONS_SUGGESTIONDATA
                            ),
                    mListCreatorForNameValuePairRequest.getListOfAddSuggestions(PreferenceManager.getRegisration_Id_response(), getCategory_Id_Tips_For(category_item), "ccxcxvxvx"),tag

            );

        }




        return null;

    }



    public void WheelViewClick()
    {

        postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_RESULTS_NOW_URL,getjsonObject(TravexApplication.NOW_TAG), TravexApplication.REQUEST_ID_NOW);

        wheelview.setVisibility(View.GONE);

        wheelview_left.setVisibility(View.GONE);
         // start animation
//        Animation animationnow = AnimationUtils.loadAnimation(getActivity(), R.anim.animationnow);
//        animationnow.setAnimationListener(this);
//
//        fab.setAnimation(animationnow);
//
//
//        fab.startAnimation(animationnow);

//        ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 1.0f, 1.0f, Animation.RELATIVE_TO_SELF,1.0f, Animation.RELATIVE_TO_SELF, 0.5f);
//        anim.setDuration(700);
//        fab.startAnimation(anim);

    }


    private Point getPointOfView(View view) {
        int[] location = new int[2];
        view.getLocationInWindow(location);
        return new Point(location[0], location[1]);
    }

    public static String getCategory_Id_Tips_For(String item) {

        if (item==null) {

            return null;
        } else if (item.equalsIgnoreCase("Airline")) {

            return "pngrtbel_vq_1";
        } else if (item.equalsIgnoreCase("Embasies & Consulates")) {

            return "pngrtbel_vq_2";
        } else if (item.equalsIgnoreCase("Tour & Travel")) {

            return "pngrtbel_vq_3";
        } else if (item.equalsIgnoreCase("Events")) {

            return "pngrtbel_vq_4";
        } else if (item.equalsIgnoreCase("Eating Out")) {

            return "pngrtbel_vq_5";
        } else if (item.equalsIgnoreCase("Stay/Hotels")) {

            return "pngrtbel_vq_6";
        } else if (item.equalsIgnoreCase("nightlife")) {

            return "pngrtbel_vq_7";
        } else if (item.equalsIgnoreCase("Spa")) {

            return "pngrtbel_vq_8";
        } else if (item.equalsIgnoreCase("Things To Do")) {

            return "pngrtbel_vq_9";
        } else if (item.equalsIgnoreCase("Shopping")) {

            return "pngrtbel_vq_10";
        } else if (item.equalsIgnoreCase("Business")) {

            return "pngrtbel_vq_11";
        } else if (item.equalsIgnoreCase("Real Estate")) {

            return "pngrtbel_vq_12";
        } else if (item.equalsIgnoreCase("Money Exchange")) {

            return "pngrtbel_vq_13";
        } else if (item.equalsIgnoreCase("Car Rentals")) {

            return "pngrtbel_vq_14";
        } else if (item.equalsIgnoreCase("Emergencies")) {

            return "pngrtbel_vq_15";
        }


        return null;


    }

    public void FillMarker(String Category_Item,double Latitude,double Longitude,String Title)
    {
        if(Category_Item!=null)
        {
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Latitude, Longitude), 10.0f));
            TypedArray images = getResources().obtainTypedArray(R.array.category_map_icons);
            if (Category_Item.equals("Airline")) {

                googleMap.addMarker(new MarkerOptions().position(new LatLng(Latitude, Longitude)).title(Title)
                        .icon(BitmapDescriptorFactory.fromResource(images.getResourceId(0, -1))));

            }else if (Category_Item.equals("Embasies & Consulates")) {

                googleMap.addMarker(new MarkerOptions().position(new LatLng(Latitude, Longitude)).title(Title)
                        .icon(BitmapDescriptorFactory.fromResource(images.getResourceId(1, -1))));

            } else if (Category_Item.equals("Tour & Travel")) {

                googleMap.addMarker(new MarkerOptions().position(new LatLng(Latitude, Longitude)).title(Title)
                        .icon(BitmapDescriptorFactory.fromResource(images.getResourceId(2, -1))));

            } else if (Category_Item.equals("Events")) {

                googleMap.addMarker(new MarkerOptions().position(new LatLng(Latitude, Longitude)).title(Title)
                        .icon(BitmapDescriptorFactory.fromResource(images.getResourceId(3, -1))));

            } else if (Category_Item.equals("Eating Out")) {

                googleMap.addMarker(new MarkerOptions().position(new LatLng(Latitude, Longitude)).title(Title)
                        .icon(BitmapDescriptorFactory.fromResource(images.getResourceId(4, -1))));

            } else if (Category_Item.equals("Stay/Hotels")) {

                googleMap.addMarker(new MarkerOptions().position(new LatLng(Latitude, Longitude)).title(Title)
                        .icon(BitmapDescriptorFactory.fromResource(images.getResourceId(5, -1))));

            } else if (Category_Item.equals("nightlife")) {

                googleMap.addMarker(new MarkerOptions().position(new LatLng(Latitude, Longitude)).title(Title)
                        .icon(BitmapDescriptorFactory.fromResource(images.getResourceId(6, -1))));

            } else if (Category_Item.equals("Spa")) {

                googleMap.addMarker(new MarkerOptions().position(new LatLng(Latitude, Longitude)).title(Title)
                        .icon(BitmapDescriptorFactory.fromResource(images.getResourceId(7, -1))));

            } else if (Category_Item.equals("Things To Do")) {

                googleMap.addMarker(new MarkerOptions().position(new LatLng(Latitude, Longitude)).title(Title)
                        .icon(BitmapDescriptorFactory.fromResource(images.getResourceId(8, -1))));

            } else if (Category_Item.equals("Shopping")) {

                googleMap.addMarker(new MarkerOptions().position(new LatLng(Latitude, Longitude)).title(Title)
                        .icon(BitmapDescriptorFactory.fromResource(images.getResourceId(9, -1))));

            } else if (Category_Item.equals("Business")) {

                googleMap.addMarker(new MarkerOptions().position(new LatLng(Latitude, Longitude)).title(Title)
                        .icon(BitmapDescriptorFactory.fromResource(images.getResourceId(10, -1))));

            } else if (Category_Item.equals("Real Estate")) {

                googleMap.addMarker(new MarkerOptions().position(new LatLng(Latitude, Longitude)).title(Title)
                        .icon(BitmapDescriptorFactory.fromResource(images.getResourceId(11, -1))));

            } else if (Category_Item.equals("Money Exchange")) {

                googleMap.addMarker(new MarkerOptions().position(new LatLng(Latitude, Longitude)).title(Title)
                        .icon(BitmapDescriptorFactory.fromResource(images.getResourceId(12, -1))));

            } else if (Category_Item.equals("Car Rentals")) {

                googleMap.addMarker(new MarkerOptions().position(new LatLng(Latitude, Longitude)).title(Title)
                        .icon(BitmapDescriptorFactory.fromResource(images.getResourceId(13, -1))));

            }  else if (Category_Item.equals("Emergencies")) {

                googleMap.addMarker(new MarkerOptions().position(new LatLng(Latitude, Longitude)).title(Title)
                        .icon(BitmapDescriptorFactory.fromResource(images.getResourceId(14, -1))));

            }
        }

    }



}
