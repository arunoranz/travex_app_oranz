package org.technomobs.travex.Activity.Fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.common.util.UriUtil;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.R;

import java.util.ArrayList;

/**
 * Created by Abhi on 09-03-2016.
 */
public class CategoryImageAdapter  extends RecyclerView.Adapter<CategoryImageAdapter.ViewHolder> {

    private Context context;
    LayoutInflater mInflater;
    String selected_category,selected_categoryid;
    ArrayList<CategoryListItems> categoryItemsnew;
    private String apptheme,category_name;

    int count = 0;
    Activity act;
    ArrayList<String> imgItem_list=new ArrayList<String>();
    ArrayList<String> name_list=new ArrayList<String>();
    ViewHolder vh;
    public CategoryImageAdapter(Activity activity,Context context,ArrayList<String> img_List,ArrayList<String> nameList) {
        this.context = context;
        name_list=nameList;
        imgItem_list=img_List;
        this.act=activity;

        mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.aminities_img_adapter, parent, false);

        vh = new ViewHolder(v);
        return vh;
    }


    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {
        // return bookmarkPostId.size();
        return imgItem_list.size();
    }


    public Object getItem(int position) {
        return imgItem_list.get(position);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        {
            holder.nameText.setText(name_list.get(position));
            if(imgItem_list.size()>0) {
                String image_name = imgItem_list.get(position);
                Uri img_uri;
              //  String image_url = AppConstants.GET_RESULTS_GENERAL_IMAGES_URL + "/" + getCategoryItemDirectorynameForImageAssetUrl(MapShowingFragment.selected_category) + "/aminity/" + image_name;
                String image_url =image_name;
                img_uri = Uri.parse(image_url);
                ImageRequest request = ImageRequest.fromUri(img_uri);
                DraweeController controller = Fresco.newDraweeControllerBuilder()
                        .setImageRequest(request)
                        .setOldController(holder.icon.getController()).build();
                holder.icon.setController(controller);
            }
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView nameText;
        SimpleDraweeView icon;
        public ViewHolder(View v) {
            super(v);
            nameText=(TextView)v.findViewById(R.id.name);
            icon=(SimpleDraweeView)v.findViewById(R.id.menu_icon);
        }
    }

}
