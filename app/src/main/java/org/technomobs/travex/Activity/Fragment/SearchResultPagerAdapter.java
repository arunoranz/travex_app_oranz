package org.technomobs.travex.Activity.Fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.cache.common.SimpleCacheKey;
import com.facebook.common.util.UriUtil;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.soundcloud.android.crop.Crop;

import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;

import java.util.ArrayList;

/**
 * Created by ARUN on 03/26/2016.
 */
public class SearchResultPagerAdapter extends PagerAdapter {

    int size;
    Activity act;
    View layout;
    TextView pagenumber1, pagenumber2, pagenumber3, pagenumber4, pagenumber5;
    ImageView pageImage;
    Button click;
    //String list_position;
    String selected_id;
    PreferenceManager mPreferenceManager = null;
    String  img_url;
    SimpleDraweeView mSplashAnimation;
//    ArrayList<String> Title_List=new ArrayList<String>();
//    ArrayList<String> Description_List=new ArrayList<String>();
    ArrayList<String> Image_URL_List=new ArrayList<String>();

    public SearchResultPagerAdapter(Activity mainActivity,ArrayList<String> Image_URL,int listposition,String id) {
        // TODO Auto-generated constructor stub
        Image_URL_List = Image_URL;
        act = mainActivity;
        selected_id=id;
        //list_position=String.valueOf(listposition);
        mPreferenceManager = new PreferenceManager(act);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return Image_URL_List.size();
    }

    @Override
    public Object instantiateItem(View container, int position) {
        // TODO Auto-generated method stub
        LayoutInflater inflater = (LayoutInflater) act
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layout = inflater.inflate(R.layout.searchresult_pager_adapter, null);

        mSplashAnimation= (SimpleDraweeView)layout.findViewById(R.id.sdvImage);
      //  LinearLayout lin_lay_desc =(LinearLayout)layout.findViewById(R.id.lin_lay_desc);
        TextView head=(TextView)layout.findViewById(R.id.head);
        TextView description=(TextView)layout.findViewById(R.id.description);
        img_url = Image_URL_List.get(position);
        if(selected_id!=null)
        {
            mSplashAnimation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Fragment fragment=SearchResultFragmentSecond.newInstance(act,selected_id);
                    if (fragment != null) {
                        FragmentManager fragmentManager = act.getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.frame_container, fragment);
                        fragmentTransaction.addToBackStack(fragment.getClass().getName());
                        fragmentTransaction.commit();


                    }
                }
            });
        }


        Uri uri=null;
        if(!img_url.equalsIgnoreCase("No Images"))
        {
            uri = Uri.parse(img_url);
            Fresco.getImagePipeline().evictFromMemoryCache(uri);
            Fresco.getImagePipelineFactory().getMainDiskStorageCache().remove(new SimpleCacheKey(uri.toString()));
            Fresco.getImagePipelineFactory().getSmallImageDiskStorageCache().remove(new SimpleCacheKey(uri.toString()));
            ImageRequest request = ImageRequest.fromUri(uri);

            DraweeController controller = Fresco.newDraweeControllerBuilder()
                    .setImageRequest(request)
                    .setOldController(mSplashAnimation.getController()).build();
            mSplashAnimation.setController(controller);
        }



        ((ViewPager) container).addView(layout, 0);
        return layout;
    }
    @Override
    public void destroyItem(View arg0, int arg1, Object arg2) {
        ((ViewPager) arg0).removeView((View) arg2);
    }
    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == ((View) arg1);
    }
    @Override
    public Parcelable saveState() {
        return null;
    }


}
