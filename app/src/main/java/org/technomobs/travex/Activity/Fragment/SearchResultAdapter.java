package org.technomobs.travex.Activity.Fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.SyncStateContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONObject;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Controller.Interface.GetWriteReviewResponse;
import org.technomobs.travex.Controller.JsonObjectMaker;
import org.technomobs.travex.Controller.NetworkManager;
import org.technomobs.travex.Controller.NetworkOptions;
import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.ListCreatorForNameValuePairRequest;
import org.technomobs.travex.Utillity.LogUtility;

import java.util.ArrayList;
import java.util.Map;

import static android.widget.LinearLayout.*;

/**
 * Created by Abhi on 09-03-2016.
 */
public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.ViewHolder> {
    private static final int VIEW_TYPE_HEADER = 0;
    private static final int VIEW_TYPE_FOOTER = 1;
    private static final int VIEW_TYPE_DEFAULT = 2;
    private static final int VIEW_TYPE_COUNT = 3;
    private String backStateName;

//    LayoutInflater mInflater;
    String selected_category, selected_categoryid;
    public int checki = 0;
    int count = 0;
    static Activity act;
    ViewHolder vh;
    View rootView;
    static ArrayList<String> id_list = new ArrayList<>();
    static ArrayList<String> Title_list = new ArrayList<>();
    static ArrayList<String> Latitude_list = new ArrayList<>();
    static ArrayList<String> Longitude_list = new ArrayList<>();
    ArrayList<String> Subtitle_list = new ArrayList<>();
    ArrayList<String> Location_list = new ArrayList<>();
    ArrayList<String> Image_count_list = new ArrayList<>();
    ArrayList<String> Video_count_list = new ArrayList<>();
    ArrayList<String> Review_list = new ArrayList<>();
    ArrayList<String> Description_list = new ArrayList<>();
    static ArrayList<String> Website_list = new ArrayList<>();
    ArrayList<String> ImageUrl_List = new ArrayList<>();
    static ArrayList<String> phone_list = new ArrayList<>();
    static PreferenceManager PreferenceManager =null;
    static String id="",name="";

  //  ViewPager vpPager;
    CirclePageIndicator indicator;

    public SearchResultAdapter(Activity activity, ArrayList<String> id, ArrayList<String> Title, ArrayList<String> Latitude
            , ArrayList<String> Longitude, ArrayList<String> Subtitle, ArrayList<String> Location, ArrayList<String> Review
            , ArrayList<String> Image_count, ArrayList<String> video_count, ArrayList<String> Description, ArrayList<String> Website
            , ArrayList<String> ImageUrl_List, ArrayList<String> phone_list) {

        this.act = activity;
        this.id_list = id;
        this.Title_list = Title;
        this.Latitude_list = Latitude;
        this.Longitude_list = Longitude;
        this.Subtitle_list = Subtitle;
        this.Location_list = Location;
        this.Review_list = Review;
        this.Image_count_list = Image_count;
        this.Video_count_list = video_count;
        this.Description_list = Description;
        this.Website_list = Website;
        this.ImageUrl_List = ImageUrl_List;
        this.phone_list = phone_list;


//        mInflater = (LayoutInflater) act
//                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        try {
            // create a new view
            rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.aftersearch_selected_adapter, parent, false);


            Initialization();

            vh = new ViewHolder(rootView);
            return vh;
        }
        catch(Exception ex)
        {

        }
        return null;
    }

    public void Initialization() {
       // vpPager = (ViewPager) rootView.findViewById(R.id.pager);
        indicator = (CirclePageIndicator) rootView.findViewById(R.id.indicator);
        PreferenceManager = new PreferenceManager(act);
    }

    public void FillData(int position) {
        //Setting Images
        ArrayList<String> ImageUrl = new ArrayList<>();
        if (ImageUrl_List.get(position) != null) {
            String[] Images = ImageUrl_List.get(position).toString().split("::");
            for (int i = 0; i < Images.length; i++) {
                ImageUrl.add(Images[i].replace(" ","%20"));
            }
        }

        SearchResultPagerAdapter adapter = new SearchResultPagerAdapter(act, ImageUrl,position,id_list.get(position));
        vh.vpPager.setAdapter(adapter);

//        vpPager.setPageMargin(20);
       vh.vpPager.setCurrentItem(0);

        indicator.setFillColor(act.getResources().getColor(android.R.color.white));
        indicator.setViewPager(vh.vpPager);

        vh.tv_title.setText(Title_list.get(position));
        if (Description_list != null) {

            if (Description_list.size()>0) {
                vh.tv_discription.setText(Html.fromHtml(Description_list.get(position)));
            } else {
                vh.tv_discription.setText("");
            }

        }
        vh.image_count.setText(Image_count_list.get(position));
        vh.video_count.setText(Video_count_list.get(position));
        vh.lin_lay_call.setTag(position);
        vh.lin_lay_gps.setTag(position);
        vh.lin_lay_site.setTag(position);
        vh.lin_lay_review.setTag(position);

    }


    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {
        // return bookmarkPostId.size();
        return id_list.size();
    }


    public Object getItem(int position) {
        return position;

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        FillData(position);
//        holder.vpPager.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int p=position;
//            }
//        });

//            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.logo).build();
//            holder.icon.setImageURI(imageRequest.getSourceUri());

    }


    public static class ViewHolder extends RecyclerView.ViewHolder implements OnClickListener,GetWriteReviewResponse {
        AlertDialog alertDialog;
        RatingBar ratingBarreview;
        static NetworkManager mNetworkManager = null;
        EditText et_review;
        JsonObjectMaker mJsonObjectMaker = null;
        ListCreatorForNameValuePairRequest mListCreatorForNameValuePairRequest = null;


        public RecyclerView recyclerView;
        RelativeLayout rel_lay_phonenumber;
        public TextView image_count, video_count, tv_discription, tv_title;
        LinearLayout lin_lay_call, lin_lay_location, lin_lay_review, lin_lay_gps, lin_lay_site;
        SimpleDraweeView icon;
        ViewPager vpPager;

        public ViewHolder(View v) {
            super(v);
       //icon= (SimpleDraweeView)v.findViewById(R.id.sdvImage);
            vpPager = (ViewPager) v.findViewById(R.id.pager);
            lin_lay_call = (LinearLayout) v.findViewById(R.id.lin_lay_call);
            lin_lay_location = (LinearLayout) v.findViewById(R.id.lin_lay_location);
            lin_lay_review = (LinearLayout) v.findViewById(R.id.lin_lay_review);
            lin_lay_gps = (LinearLayout) v.findViewById(R.id.lin_lay_gps);
            lin_lay_site = (LinearLayout) v.findViewById(R.id.lin_lay_site);
            //pager_layout = (LinearLayout) v.findViewById(R.id.pager_layout);
            rel_lay_phonenumber = (RelativeLayout) v.findViewById(R.id.rel_lay_phonenumber);

            tv_title = (TextView) v.findViewById(R.id.tv_title);
            tv_discription = (TextView) v.findViewById(R.id.tv_discription);
            image_count = (TextView) v.findViewById(R.id.image_count);
            video_count = (TextView) v.findViewById(R.id.video_count);

            recyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);
            recyclerView.setHasFixedSize(true);
            GridLayoutManager mLayoutManager = new GridLayoutManager(act, 1);
            recyclerView.setLayoutManager(mLayoutManager);

            lin_lay_call.setOnClickListener(this);
            lin_lay_gps.setOnClickListener(this);
            lin_lay_site.setOnClickListener(this);
            lin_lay_review.setOnClickListener(this);
//            pager_layout.setOnClickListener(this);
            mNetworkManager = NetworkManager.getSingleInstance(act);
            mJsonObjectMaker = new JsonObjectMaker(act);
            mListCreatorForNameValuePairRequest = new ListCreatorForNameValuePairRequest(act);
            mNetworkManager.setOnGetWriteReviewResponseListener(this);
        }


        @Override
        public void onClick(View v) {
            int position =0;
            switch (v.getId()) {

                case R.id.lin_lay_call:

                    position = (int) lin_lay_call.getTag();
                    ArrayList<String> Phone_Numbers = new ArrayList<>();
                    if (phone_list.get(position) != null) {
                        String[] Images = phone_list.get(position).toString().split("::");
                        for (int i = 0; i < Images.length; i++) {
                            Phone_Numbers.add(Images[i]);
                        }
                    }




                    if(Phone_Numbers.size()>0)
                    {
                        if(!Phone_Numbers.get(0).equalsIgnoreCase("No Numbers"))
                        {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(act);
                            // ...Irrelevant code for customizing the buttons and title
                            LayoutInflater inflater = act.getLayoutInflater();

                            View dialogView = inflater.inflate(R.layout.mobilenumberlistingadapter_layout, null);

                            RecyclerView recyclerView = (RecyclerView) dialogView.findViewById(R.id.recyclerView);
                            recyclerView.setLayoutManager(new LinearLayoutManager(act));
                            MobileNumberListingAdapter adapter = new MobileNumberListingAdapter(act, Phone_Numbers);
                            recyclerView.setAdapter(adapter);

                            dialogBuilder.setView(dialogView);

                            AlertDialog alertDialog = dialogBuilder.create();
//                    alertDialog.getWindow().setLayout(200, 400);
                            alertDialog.show();
                        }
                        else
                        {
                            Toast.makeText(act,"Not available",Toast.LENGTH_SHORT).show();
                        }

                    }




                    break;

                case R.id.lin_lay_gps:
                    position = (int) lin_lay_call.getTag();
                    String Lat=Latitude_list.get(position);
                    String Long=Longitude_list.get(position);
                    if(Lat==null) Lat="";
                    if(Lat=="") Lat="0";
                    if(Long==null) Long="";
                    if(Long=="") Long="0";
                    double latitude= Double.parseDouble(Lat);
                    double longitude= Double.parseDouble(Long);
                    Intent navigation = new Intent(Intent.ACTION_VIEW, Uri
                            .parse("http://maps.google.com/maps?saddr="
                                    + latitude + ","
                                    + longitude + "&daddr="
                                    + latitude + "," + longitude));
                    act.startActivity(navigation);
                    break;

                case R.id.lin_lay_site:

                    if(Website_list.size()>0)
                    {
                        String website_url=Website_list.get(0);

                        if(website_url == null)website_url="";
                        if(!website_url.equalsIgnoreCase(""))
                        {
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(website_url));
                            act.startActivity(browserIntent);
                        }
                        else
                        {
                            Toast.makeText(act,"Not available",Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Toast.makeText(act,"Not available",Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.lin_lay_review:
                    position = (int) lin_lay_call.getTag();
                    id=id_list.get(position);
                    name=Title_list.get(position);
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(act);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = act.getLayoutInflater();

                    View writereview_layout = inflater.inflate(R.layout.writereview_layout, null);

                    String member_profile_image = PreferenceManager.getmember_profile_image();
                    SimpleDraweeView review_Image=(SimpleDraweeView)writereview_layout.findViewById(R.id.review_Image);
//        image.setImageResource(R.drawable.user);
                    if(member_profile_image == null) {
                        ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.user).build();
                        review_Image.setImageURI(imageRequest.getSourceUri());                    }
                    else if(member_profile_image.equals("")) {
                        ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.user).build();
                        review_Image.setImageURI(imageRequest.getSourceUri());
                    }
                    else
                    {
                        Uri img_uri=Uri.parse(member_profile_image);
                        ImageRequest request = ImageRequest.fromUri(img_uri);

                        DraweeController controller = Fresco.newDraweeControllerBuilder()
                                .setImageRequest(request)
                                .setOldController(review_Image.getController()).build();
                        //Log.e(TAG, "ImagePath uri " + img_uri);

                        review_Image.setController(controller);
                    }

//                    ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.user).build();
//
//                    review_Image.setImageURI(imageRequest.getSourceUri());
                    TextView review_username=(TextView) writereview_layout.findViewById(R.id.review_username);
                    review_username.setText(PreferenceManager.getRegisteredUser_name());

                    ratingBarreview=(RatingBar)writereview_layout.findViewById(R.id.ratingBarreview);
//                    LayerDrawable stars = (LayerDrawable) ratingBarreview.getProgressDrawable();
//                    stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
//
//                    try {
//                        stars.setTint(act.getResources().getColor(R.color.grey_300));
//                    }
//                    catch(Exception ex)
//                    {
//
//                    }

                    et_review=(EditText)writereview_layout.findViewById(R.id.et_review);
                    TextView bt_submit=(TextView)writereview_layout.findViewById(R.id.bt_submit);
                    ImageView bt_close=(ImageView)writereview_layout.findViewById(R.id.bt_close);
                    bt_submit.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(ratingBarreview.getRating()<=0) {
                                Toast.makeText(act,"Please rate...",Toast.LENGTH_SHORT).show();
                            }
                            else if(et_review.getText().toString().equals("")) {
                                Toast.makeText(act,"Write a review",Toast.LENGTH_SHORT).show();
                            }
                            else
                            {
                                alertDialog.hide();
                                et_review.setText("");
                                postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, mNetworkManager.getHeaders(), AppConstants.WRITE_REVIEW_URL,
                                        getjsonObject(TravexApplication.WRITE_REVIEW_TAG),
                                        TravexApplication.REQUEST_ID_WRITE_REVIEW);
                            }
                        }
                    });
                    bt_close.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.hide();
                        }
                    });


                    dialogBuilder.setView(writereview_layout);

                    alertDialog = dialogBuilder.create();
//                    alertDialog.getWindow().setLayout(200, 400);
                    if(PreferenceManager.getboolean("logout")==null) {
                        Toast.makeText(act,"You must login to write a review !",Toast.LENGTH_LONG).show();
//                    Intent logouintent = new Intent(activity,SignUpLoginMasterActivity.class);
//                    startActivity(logouintent);
                    }
                    else if(PreferenceManager.getboolean("logout")==false){
                        Toast.makeText(act,"You must login to write a review !",Toast.LENGTH_LONG).show();
//                    Intent logouintent = new Intent(activity,SignUpLoginMasterActivity.class);
//                    startActivity(logouintent);
                    }else {
                        alertDialog.show();
                        et_review.setFocusable(true);
                    }

                    break;

                default:
                    break;

            }

        }


        public void postJsonRequest(int request_type, final Map<String, String> request_header, String url, JSONObject jsonObject, final int request_id) {

            mNetworkManager.postJsonRequest(request_type, request_header, url, jsonObject, request_id);
        }

        public JSONObject getJson(ArrayList<String> namePair, ArrayList<String> valuePair) {
            return mJsonObjectMaker.getJson(namePair, valuePair);
        }


        public JSONObject getjsonObject(String tag) {
            if (tag.equals(TravexApplication.WRITE_REVIEW_TAG)) {
                return getJson(
                        mListCreatorForNameValuePairRequest.getListOfWriteReview
                                (TravexApplication.WRITE_REVIEW_USER_ID,
                                        TravexApplication.WRITE_REVIEW_CATEGORY,
                                        TravexApplication.WRITE_REVIEW_ITEM_ID,
                                        TravexApplication.WRITE_REVIEW_ITEM_NAME,
                                        TravexApplication.WRITE_REVIEW_COMMENT,
                                        TravexApplication.WRITE_REVIEW_RATING

                                ),
                        mListCreatorForNameValuePairRequest.getListOfWriteReview(PreferenceManager.getRegisration_Id_response(), MapShowingFragment.selected_categoryId, id, name, et_review.getText().toString(), String.valueOf(ratingBarreview.getRating()))

                );

            }
            return null;
        }


        @Override
        public void onGetWriteReviewResponse(boolean status) {
            if (status) {

                Toast.makeText(act,"Review has been submitted for approval!",Toast.LENGTH_LONG).show();

            } else {

                Toast.makeText(act,"Review not Added!",Toast.LENGTH_LONG).show();
            }

        }
    }



}
