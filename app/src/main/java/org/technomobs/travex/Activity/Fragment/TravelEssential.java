package org.technomobs.travex.Activity.Fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Controller.Interface.GetEssentialListResponse;
import org.technomobs.travex.Controller.Interface.GetEssentialsDetailsResponse;
import org.technomobs.travex.Controller.JsonObjectMaker;
import org.technomobs.travex.Controller.NetworkManager;
import org.technomobs.travex.Controller.NetworkOptions;
import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.ListCreatorForNameValuePairRequest;
import org.technomobs.travex.Utillity.LogUtility;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;

/**
 * Created by Abhi on 05-03-2016.
 */
public class TravelEssential extends Fragment implements GetEssentialListResponse,
        GetEssentialsDetailsResponse,Animation.AnimationListener {
    static Activity activity;
    //API Credentials
    static NetworkManager mNetworkManager = null;
    public static final String TAG = TravelEssential.class.getSimpleName();
    JsonObjectMaker mJsonObjectMaker = null;
    ListCreatorForNameValuePairRequest mListCreatorForNameValuePairRequest = null;
    String country = null;
    String country_id = null;
    PreferenceManager mPreferenceManager = null;
    ViewPager vpPager;
    TravelEssentialAdapter TravelEssentialAdapter;
    RecyclerView recyclerView;

    ArrayList<String> Essential_Title = new ArrayList<>();
    ArrayList<String> Essential_Text = new ArrayList<>();
    ArrayList<String> Essential_id_list= new ArrayList<>();
    ArrayList<String> Essential_Images_URL= new ArrayList<>();
    CircleIndicator defaultIndicator;
    MyPagerAdapter adapterViewPager;

    RelativeLayout animation_layout;
    View rootView;
    Animation rotateOne, rotateTwo, rotateThree;

    public static TravelEssential newInstance(Activity act){
        TravelEssential fragment = new TravelEssential();
        activity=act;

        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.travel_essential, container, false);

        initiating_APICredentials();
        GettingPreferenceValues();
        initialization(rootView);
        ChangeToolBarDesignBackToPrevious();
//        postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_ESSENTIALS_DETAILS_URL,
//                getjsonObject(TravexApplication.GET_ESSENTIALS_DETAILS_TAG),
//                TravexApplication.REQUEST_ID_ESSENTIALS_DETAILS);

        postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_ESSENTIALS_LIST_URL,
                getjsonObject(TravexApplication.GET_ESSENTIALS_LIST_TAG),
                TravexApplication.REQUEST_ID_ESSENTIALS_LIST);
        animationInit();
        animationLoading();

        Runnable run = new Runnable() {
            @Override
            public void run() {
                animation_layout.setVisibility(View.GONE);
            }
        };
        Handler handler = new Handler();
        handler.postDelayed(run, 2000);




        return rootView;
    }

    public void ChangeToolBarDesignBackToPrevious()
    {
        //To change the current design of the Toolbar in this fragment
        Toolbar toolbar = (Toolbar)getActivity().findViewById(R.id.tool_bar);
        TextView textToolHeader = (TextView) toolbar.findViewById(R.id.tv_category);
        //textToolHeader.setText(MapShowingFragment.selected_category);
        textToolHeader.setVisibility(View.GONE);
        SimpleDraweeView image = (SimpleDraweeView)toolbar.findViewById(R.id.networkImageViewEqWNH);
        image.setVisibility(View.VISIBLE);

        toolbar.setNavigationIcon(R.drawable.ic_action_menu);
    }


    public void initiating_APICredentials()
    {
        mJsonObjectMaker = new JsonObjectMaker(getActivity());
        mListCreatorForNameValuePairRequest = new ListCreatorForNameValuePairRequest(getActivity());
        mNetworkManager = NetworkManager.getSingleInstance(getActivity());
        mNetworkManager.setOnGetEssentialsListResponseListener(this);
        mNetworkManager.setOnGetEssentialsDetailsResponseListener(this);
        mPreferenceManager = new PreferenceManager(getActivity());
    }

    public void GettingPreferenceValues()
    {
        LogUtility.Log(TAG, "onEnterKeyClick", null);
        country = mPreferenceManager.getSelectedCountryName();
        //country = ((ProfileActivity) getActivity()).getSelectedCountry();
        LogUtility.Log(TAG, "onEnterKeyClick:", "selected country from actionbar:" + country);
        //country_id = ((ProfileActivity) getActivity()).fetchCountryIdForCountry(country);
        country_id = mPreferenceManager.getSelectedcountryId();
        LogUtility.Log(TAG, "onEnterKeyClick:", "country id:" + country_id);
    }

    public void initialization(View view)
    {
//        vpPager = (ViewPager) view.findViewById(R.id.pager);
        defaultIndicator = (CircleIndicator)view. findViewById(R.id.indicator);

        recyclerView= (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        animation_layout=(RelativeLayout)rootView.findViewById(R.id.settings_animation);
    }



    public JSONObject getjsonObject(String tag) {
        if (tag.equals(TravexApplication.GET_ESSENTIALS_LIST_TAG)) {
            return getJson(mListCreatorForNameValuePairRequest.getListOfEssentials_List(
                            TravexApplication.GET_ESSENTIALS_LIST_CITY_ID


                    ),
                    mListCreatorForNameValuePairRequest.getListOfEssentials_List(country_id));

        } else if (tag.equals(TravexApplication.GET_ESSENTIALS_DETAILS_TAG)) {
            return getJson(mListCreatorForNameValuePairRequest.getListOfEssentialsDetails(
                            TravexApplication.GET_ESSENTIALS_DETAILS_ID


                    ),
                    mListCreatorForNameValuePairRequest.
                            getListOfEssentialsDetails(mPreferenceManager.getEssentials_id().get(0)));

        }
        return null;
    }


    public void postJsonRequest(int request_type, String url, JSONObject jsonObject, final int request_id) {

        mNetworkManager.postJsonRequest(request_type, url, jsonObject, request_id);
    }

    public JSONObject getJson(ArrayList<String> namePair, ArrayList<String> valuePair) {


        return mJsonObjectMaker.getJson(namePair, valuePair);
    }



    @Override
    public void onGetEssentialsListResponse(boolean status, JSONObject jsonObject) {
        if (status) {

            try {
                Essential_id_list.clear();
                Essential_Title.clear();
                Essential_Text.clear();
                Essential_Images_URL.clear();
                JSONObject jsonObj = new JSONObject(jsonObject.toString());
                JSONArray result = jsonObject.getJSONArray("result");
                for (int i = 0; i < result.length(); i++) {

                    JSONObject child = result.getJSONObject(i);
                    Essential_id_list.add(child.getString("essentials_id"));
                    Essential_Title.add(child.getString("essentials_title"));
                    Essential_Text.add(child.getString("essentials_text"));

                    JSONArray images = child.getJSONArray("images");
                    for (int j = 0; j < images.length(); j++) {
                        JSONObject childimages = images.getJSONObject(j);
                        if(childimages.getString("essentials_images").equals("")){
                            Essential_Images_URL.add("");
                        }else {
                            Essential_Images_URL.add(AppConstants.ESSENTIAL_IMAGES_ASSETS_URL + "/" + childimages.getString("essentials_images"));
                            break;
                        }
                    }
                }

                //Setting RecycleView Adapter
                TravelEssentialAdapter = new TravelEssentialAdapter(activity,Essential_Title,Essential_Text,Essential_Images_URL,Essential_id_list);
                recyclerView.setAdapter(TravelEssentialAdapter);


//                adapterViewPager = new MyPagerAdapter(getActivity(),Essential_Images_URL);
//                vpPager.setAdapter(adapterViewPager);
//                defaultIndicator.setViewPager(vpPager);
                //

            } catch (JSONException e) {
                e.printStackTrace();
            }

//            postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_ESSENTIALS_DETAILS_URL,
//            getjsonObject(TravexApplication.GET_ESSENTIALS_DETAILS_TAG),
//            TravexApplication.REQUEST_ID_ESSENTIALS_DETAILS);



            LogUtility.Log(TAG, "onGetEssentialsListResponse: :success", "json data:" + jsonObject.toString());


        } else {
            LogUtility.Log(TAG, "onGetEssentialsListResponse: :failure", null);

        }
    }

    @Override
    public void onGetEssentialsDetailsResponse(boolean status, JSONObject jsonObject) {
        if (status) {

            LogUtility.Log(TAG, "onGetEssentialsDetailsResponse: :success", "json data:" + jsonObject.toString());


        } else {
            LogUtility.Log(TAG, "onGetEssentialsDetailsResponse: :failure", null);

        }
    }


    //Animation Functions
    public void animationLoading(){
        animation_layout.setVisibility(View.VISIBLE);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                ImageView iv1 = (ImageView) rootView.findViewById(R.id.settings_one);
                ImageView iv2 = (ImageView) rootView.findViewById(R.id.settings_two);
                ImageView iv3 = (ImageView) rootView.findViewById(R.id.settings_three);

                iv1.setAnimation(rotateOne);
                iv2.setAnimation(rotateTwo);
                iv3.setAnimation(rotateThree);

                iv1.startAnimation(rotateOne);
                iv2.startAnimation(rotateTwo);
                iv3.startAnimation(rotateThree);
            }
        };
        Handler mHandler = new Handler();
        mHandler.postDelayed(runnable, 100);

    }

    public  void  animationInit(){
        rotateOne = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_one);
        rotateOne.setAnimationListener(this);
        rotateTwo= AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_two);
        rotateTwo.setAnimationListener(this);
        rotateThree= AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_three);
        rotateThree.setAnimationListener(this);
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }


    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}






