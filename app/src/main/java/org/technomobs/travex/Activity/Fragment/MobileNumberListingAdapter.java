package org.technomobs.travex.Activity.Fragment;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import org.technomobs.travex.R;

import java.util.ArrayList;

/**
 * Created by ARUN on 03/26/2016.
 */
public class MobileNumberListingAdapter extends RecyclerView.Adapter<MobileNumberListingAdapter.ViewHolder> {


    static Activity act;
    ViewHolder vh;
    View rootView;
    ArrayList<String> Mobile_Number_List = new ArrayList<>();

    public MobileNumberListingAdapter(Activity activity,ArrayList<String> Mobile_Number)
    {
        this.Mobile_Number_List = Mobile_Number;
    }
    @Override
    public MobileNumberListingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.mobilenumberlisting_alertlayout, parent, false);
        vh = new ViewHolder(rootView);
        return vh;
    }

    @Override
    public void onBindViewHolder(MobileNumberListingAdapter.ViewHolder holder, int position) {
        String Mobile_Number = Mobile_Number_List.get(position);
        holder.tv_phone_number.setText(Mobile_Number);
    }

    @Override
    public int getItemCount() {
        return Mobile_Number_List.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_phone_number;
        public ViewHolder(View v) {
            super(v);
            tv_phone_number = (TextView) v.findViewById(R.id.tv_phone_number);

        }


    }
}
