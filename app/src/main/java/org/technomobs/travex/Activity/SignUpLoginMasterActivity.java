package org.technomobs.travex.Activity;

import android.support.v4.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import org.technomobs.travex.Activity.Fragment.CityVisitFragment;
import org.technomobs.travex.Activity.Fragment.ExceptionHandler;
import org.technomobs.travex.Activity.Fragment.ForgotPasswordFragment;
import org.technomobs.travex.Activity.Fragment.LoginTravexFragment;
import org.technomobs.travex.Activity.Fragment.SignUpLoginMasterFragment;
import org.technomobs.travex.Activity.Fragment.SignupTravexFragment;
import org.technomobs.travex.Activity.Fragment.UpdateMemberDetailsFragment;
import org.technomobs.travex.Activity.Fragment.UpdateMemberDetailsSecondFragment;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.LogUtility;

/**
 * Created by technomobs on 22/2/16.
 * <p/>
 * do not override onActivityResult() in this class,if so will make problems
 */
public class SignUpLoginMasterActivity extends AppCompatActivity implements ForgotPasswordFragment.OnFragmentInteractionListener
        ,SignupTravexFragment.OnFragmentInteractionListener{
    public static final String TAG = SignUpLoginMasterActivity.class.getSimpleName();
    FragmentTransaction mFragmentTransaction = null;
    FragmentManager mFragmentManager = null;
    SignUpLoginMasterFragment mSignUpLoginFragment = null;
    ForgotPasswordFragment mForgotPasswordFragment = null;
    SignupTravexFragment mSignUpTravexFragment = null;
    LoginTravexFragment mLoginTravexFragment = null;
    UpdateMemberDetailsSecondFragment mUpdateMemberDetailsSecondFragment = null;

    PreferenceManager mPreferenceManager = null;

    CityVisitFragment mCityVisitFragment = null;
    UpdateMemberDetailsFragment mUpdateMemberDetailsFragment = null;

    @Override
    public void onBackPressed() {
        LogUtility.Log(TAG, "onBackPressed", null);
        android.support.v4.app.Fragment f = mFragmentManager.findFragmentById(R.id.container);
        if (f instanceof SignupTravexFragment || f instanceof ForgotPasswordFragment) {
            LogUtility.Log(TAG, "onBackPressed", "current fragment:" + SignupTravexFragment.class);
            replace_with_masterFragment();
        } else if (f instanceof UpdateMemberDetailsFragment) {
            LogUtility.Log(TAG, "onBackPressed", "current fragment:" + UpdateMemberDetailsFragment.class);

            super.onBackPressed();
        }
        else if (f instanceof LoginTravexFragment) {
            LogUtility.Log(TAG, "onBackPressed", "current fragment:" + UpdateMemberDetailsFragment.class);

            replace_with_masterFragment();
        }
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        LogUtility.Log(TAG, "onAttachFragment", null);

        super.onAttachFragment(fragment);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_login_master_activity_layout);
        LogUtility.Log(TAG, "onCreate", null);


        mFragmentManager = getSupportFragmentManager();
        mSignUpLoginFragment = new SignUpLoginMasterFragment();
        mForgotPasswordFragment = new ForgotPasswordFragment(SignUpLoginMasterActivity.this);
        mSignUpTravexFragment = SignupTravexFragment.newInstance(SignUpLoginMasterActivity.this);
        mLoginTravexFragment = new LoginTravexFragment();
        mCityVisitFragment = new CityVisitFragment();
        mUpdateMemberDetailsFragment = new UpdateMemberDetailsFragment();
        mPreferenceManager = new PreferenceManager(this);
        load_signuploginmaster_fragment();


    }

    @Override
    protected void onDestroy() {
        LogUtility.Log(TAG, "onDestroy", null);

        super.onDestroy();
    }

    @Override
    protected void onPause() {
        LogUtility.Log(TAG, "onPause", null);

        super.onPause();
    }

    @Override
    protected void onRestart() {
        LogUtility.Log(TAG, "onRestart", null);

        super.onRestart();
    }

    @Override
    protected void onResume() {
        LogUtility.Log(TAG, "onResume", null);

        super.onResume();
    }

    @Override
    protected void onStart() {
        LogUtility.Log(TAG, "onStart", null);

        super.onStart();
    }

    @Override
    protected void onStop() {
        LogUtility.Log(TAG, "onStop", null);

        super.onStop();
    }

    public void load_signuploginmaster_fragment() {

        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.add(R.id.container, mSignUpLoginFragment);
        mFragmentTransaction.commitAllowingStateLoss();
    }

    public void load_signupTravex_fragment() {

        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.container, mSignUpTravexFragment);
        mFragmentTransaction.commitAllowingStateLoss();
    }

    public void load_loginTravex_fragment() {

        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.container, mLoginTravexFragment);
        mFragmentTransaction.commitAllowingStateLoss();
    }

    public void replace_with_masterFragment() {

        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.container, mSignUpLoginFragment);
        mFragmentTransaction.commitAllowingStateLoss();
    }

    public void replace_with_forgotFragment() {

        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.container, mForgotPasswordFragment);
        mFragmentTransaction.commitAllowingStateLoss();
    }

    public void replace_with_VisitCityFragment() {

        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.container, mCityVisitFragment);
        mFragmentTransaction.commitAllowingStateLoss();
    }

    public void replace_with_UpdateDetails_MemberFragment() {

        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.container, mUpdateMemberDetailsFragment);
        mFragmentTransaction.commitAllowingStateLoss();
    }

    public void replace_with_UpdateDetails_Member_second_Fragment(String gender,
                                                                  String status,
                                                                  String children,
                                                                  String nationality,
                                                                  String languages,
                                                                  String preffered) {
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mUpdateMemberDetailsSecondFragment = UpdateMemberDetailsSecondFragment.newInstance(gender, status, children, nationality, languages, preffered);
        mFragmentTransaction.replace(R.id.container, mUpdateMemberDetailsSecondFragment);
        mFragmentTransaction.commitAllowingStateLoss();


    }

    public void startProfileActivity() {

        //Intent mIntent = new Intent(SignUpLoginMasterActivity.this, ProfileActivity.class);
        Intent mIntent = new Intent(SignUpLoginMasterActivity.this, AfterloginKnowncity.class);
        startActivity(mIntent);
        //finishActivity();
    }
    public void startProfileActivity_with_OwnFragment(String Fragment_Name) {

        //Intent mIntent = new Intent(SignUpLoginMasterActivity.this, ProfileActivity.class);
        Intent mIntent = new Intent(SignUpLoginMasterActivity.this, AfterloginKnowncity.class);
        mIntent.putExtra("FragmentName",Fragment_Name);
        startActivity(mIntent);
        //finishActivity();
    }

//    public void startAfterloginKnowncity() {
//
//        Intent mIntent = new Intent(SignUpLoginMasterActivity.this, AfterloginKnowncity.class);
//        startActivity(mIntent);
//        finishActivity();
//    }

    public void finishActivity() {
        finish();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
