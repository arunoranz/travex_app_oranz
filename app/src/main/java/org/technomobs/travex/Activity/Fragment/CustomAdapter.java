package org.technomobs.travex.Activity.Fragment;

/**
 * Created by Abhi on 07-03-2016.
 */

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.technomobs.travex.R;

import java.util.ArrayList;

public class CustomAdapter extends BaseAdapter{

    ArrayList<String> result,designation;
    Context context;
    ArrayList<String> imageURL;
    private static LayoutInflater inflater=null;
    public CustomAdapter(Context con , ArrayList<String> prgmNameList, ArrayList<String> prgmImages,ArrayList<String> desigList) {
        // TODO Auto-generated constructor stub
        result=prgmNameList;
        designation=desigList;
        context=con;
        imageURL=prgmImages;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return result.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        TextView tv,tv2;
        ImageView img;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder=new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.programlist, null);
        holder.tv=(TextView) rowView.findViewById(R.id.textView1);
        holder.tv2=(TextView) rowView.findViewById(R.id.textView2);
        holder.img=(ImageView) rowView.findViewById(R.id.imageView1);

        holder.tv.setText(result.get(position));
        holder.tv2.setText(designation.get(position));
        Uri imageUri = Uri.parse(imageURL.get(position));
        holder.img.setImageURI(imageUri);

        rowView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
            }
        });

        return rowView;
    }

}