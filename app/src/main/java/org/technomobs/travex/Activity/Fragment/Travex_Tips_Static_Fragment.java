package org.technomobs.travex.Activity.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.LogUtility;

/**
 * Created by technomobs on 3/3/16.
 */
public class Travex_Tips_Static_Fragment extends Fragment {
    public static final String TAG = Travex_Tips_Static_Fragment.class.getSimpleName();
    SimpleDraweeView mSimpleDraweeView_tips;
    TextView mTextView_tips;
    EditText mEditText_tips_description;
    PreferenceManager mPreferenceManager = null;
    String url_Tips_Image = null;
    String title = null;
    String description = null;


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onActivityCreated", null);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        LogUtility.Log(TAG, "onActivityResult", null);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onAttach(Context context) {
        LogUtility.Log(TAG, "onAttach", null);
        mPreferenceManager = new PreferenceManager(getActivity());
        url_Tips_Image = mPreferenceManager.getCms_image_asset_url_feature().get(0);
        title = mPreferenceManager.getCms_image_title_feature().get(0);
        description = mPreferenceManager.getCms_image_description_feature().get(0);

        LogUtility.Log(TAG, "onAttach", "url_Tips_Image:" + url_Tips_Image);
        LogUtility.Log(TAG, "onAttach", "title:" + title);
        LogUtility.Log(TAG, "onAttach", "description:" + description);

        super.onAttach(context);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        LogUtility.Log(TAG, "onConfigurationChanged", null);
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onCreate", null);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        LogUtility.Log(TAG, "onDestroy", null);
        super.onDestroy();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onCreateView", null);

        View view = inflater.inflate(R.layout.travex_tips_static_fragment_layout, container, false);
        mSimpleDraweeView_tips = (SimpleDraweeView) view.findViewById(R.id.travex_tips_image);
        mTextView_tips = (TextView) view.findViewById(R.id.travex_tips_title);
        mEditText_tips_description = (EditText) view.findViewById(R.id.travex_tips_description);

        renderImage();
        mTextView_tips.setText(title);
        mEditText_tips_description.setText(description);
        return view;
    }

    @Override
    public void onDestroyView() {
        LogUtility.Log(TAG, "onDestroyView", null);
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        LogUtility.Log(TAG, "onDetach", null);
        super.onDetach();
    }

    @Override
    public void onPause() {
        LogUtility.Log(TAG, "onPause", null);
        super.onPause();
    }

    @Override
    public void onResume() {
        LogUtility.Log(TAG, "onResume", null);
        super.onResume();
    }

    @Override
    public void onStart() {
        LogUtility.Log(TAG, "onStart", null);
        super.onStart();
    }

    @Override
    public void onStop() {
        LogUtility.Log(TAG, "onStop", null);
        super.onStop();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        LogUtility.Log(TAG, "onViewCreated", null);
        super.onViewCreated(view, savedInstanceState);


    }

    public void renderImage() {

        Uri uri = Uri.parse(url_Tips_Image);
        mSimpleDraweeView_tips.setImageURI(uri);


    }
}
