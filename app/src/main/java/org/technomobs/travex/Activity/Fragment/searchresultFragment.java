package org.technomobs.travex.Activity.Fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
//import com.twotoasters.jazzylistview.JazzyHelper;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.technomobs.travex.Activity.AfterloginKnowncity;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Adapter.AlphabetAdapter;
import org.technomobs.travex.Controller.Interface.GetResponseSearchResultsGeneral;
import org.technomobs.travex.Controller.Interface.GetSearchItemResponse;
import org.technomobs.travex.Controller.JsonObjectMaker;
import org.technomobs.travex.Controller.NetworkManager;
import org.technomobs.travex.Controller.NetworkOptions;
import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.ListCreatorForNameValuePairRequest;
import org.technomobs.travex.Utillity.LogUtility;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Abhi on 09-03-2016.
 */
public class searchresultFragment extends Fragment implements GetResponseSearchResultsGeneral,Animation.AnimationListener {
//    static Context context;
    static Activity activity;
    SearchResultAdapter adapter;
    int count=0;
    AlphabetAdapter adapter_alphabet;
    RecyclerView mRecyclerView,recyclerView_alphabet;
//    JazzyRecyclerViewScrollListener jazzyScrollListener;
    private static final String KEY_TRANSITION_EFFECT = "transition_effect";
//    private int mCurrentTransitionEffect = JazzyHelper.HELIX;
    View rootView;
    SimpleDraweeView advertise_img;
    int  dp30 ,dp35, dp80;
    static String suggestion_id="",country_id="",selected_category_name="";
    Animation rotateOne, rotateTwo, rotateThree;
    //API Credentials
    static NetworkManager mNetworkManager = null;
    public static final String TAG = ContactFragment.class.getSimpleName();
    JsonObjectMaker mJsonObjectMaker = null;
    ListCreatorForNameValuePairRequest mListCreatorForNameValuePairRequest = null;

    ArrayList<String> item_id,images_list,videos_list,timings_list,alphabet_list;

    String name,adv_image_name="",adv_image_url="",country,description,latitude,logitude,star;
    static String searchterm="";
    ImageView filter_img,alfabet_image;
    static Boolean Filter;
    RelativeLayout lay_srchnotfound,animation_layout;
    static ArrayList<String> id = new ArrayList<>();
    static  ArrayList<String> Title = new ArrayList<>();
    static ArrayList<String> Latitude = new ArrayList<>();
    static ArrayList<String> Longitude = new ArrayList<>();
    static ArrayList<String> Subtitle = new ArrayList<>();
    static ArrayList<String> Location = new ArrayList<>();
    static ArrayList<String> Review = new ArrayList<>();
    static ArrayList<String> Images = new ArrayList<>();
    static ArrayList<String> Videos = new ArrayList<>();
    static ArrayList<String> Description = new ArrayList<>();
    static ArrayList<String> Website = new ArrayList<>();
    static ArrayList<String> ImageUrl_List = new ArrayList<>();
    static ArrayList<String> phone_list = new ArrayList<>();


    public static searchresultFragment newInstance(Activity act,String sugg_id,String coun_id){
        ImageUrl_List = new ArrayList<>();
       id .clear();
        Title .clear();
       Latitude .clear();
       Longitude .clear();
       Subtitle .clear();
       Location .clear();
       Review .clear();
       Images .clear();
       Videos .clear();
       Description .clear();
       Website .clear();
//        ImageUrl_List .clear();
//        phone_list .clear();
        searchterm="";
        Log.e("SEARCHITEM_ID",sugg_id);
        searchresultFragment fragment = new searchresultFragment();
        activity=act;
        suggestion_id = sugg_id.toString().trim();

        country_id = coun_id.toString().trim();
        selected_category_name=MapShowingFragment.selected_category;


        Filter=false;
        return fragment;
    }

    public static searchresultFragment newInstance(Activity act,String sugg_id,String coun_id,String alphabet){

        id .clear();
        Title .clear();
        Latitude .clear();
        Longitude .clear();
        Subtitle .clear();
        Location .clear();
        Review .clear();
        Images .clear();
        Videos .clear();
        Description .clear();
        Website .clear();
        ImageUrl_List .clear();
        phone_list .clear();
        searchterm=alphabet;
        Log.e("SEARCHITEM_ID",sugg_id);
        searchresultFragment fragment = new searchresultFragment();
        activity=act;
        suggestion_id = sugg_id.toString().trim();
        country_id = coun_id.toString().trim();
        selected_category_name=MapShowingFragment.selected_category;


        Filter=false;
        return fragment;
    }

    public  static searchresultFragment newInstance(Activity act,ArrayList<String> ImageUrl_List_filterlist,ArrayList<String> phone_list_filter,ArrayList<String> id_list,ArrayList<String> title_list,ArrayList<String> subtitle_list,ArrayList<String> locatin_list,ArrayList<String> review_list
            ,ArrayList<String> noimages_list,ArrayList<String> novideos_list,ArrayList<String> description_list,ArrayList<String> website_list,ArrayList<String> lon_list,ArrayList<String> lat_list,Boolean filtration){

        searchresultFragment fragment = new searchresultFragment();
        activity=act;
        selected_category_name=MapShowingFragment.selected_category;
        Filter=filtration;

        id=id_list;
        Title=title_list;
        Latitude=lat_list;
        Longitude=lon_list;
        Subtitle=subtitle_list;
        Location=locatin_list;
        Review=review_list;
        Images=noimages_list;
        Videos=novideos_list;
        Description=description_list;
        Website =website_list;
        ImageUrl_List=ImageUrl_List_filterlist;
        phone_list=phone_list_filter;

        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.aftersearchselected_fragment, container, false);
        if(activity==null) {
            activity = getActivity();
        }
        setHasOptionsMenu(true);

        //To change the current design of the Toolbar in this fragment
        ChangeToolBarDesign();

        initiating_APICredentials();
        initialization(rootView);
       advertise_img.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               try {
                   if (!adv_image_url.equals("")) {
                       String website_url = adv_image_url;

                       if (website_url == null) website_url = "";
                       if (!website_url.equalsIgnoreCase("")) {
                           Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(website_url));
                           activity.startActivity(browserIntent);
                       } else {
                           Toast.makeText(activity, "Not available", Toast.LENGTH_SHORT).show();
                       }
                   } else {
                       Toast.makeText(activity, "Not available", Toast.LENGTH_SHORT).show();
                   }
               }catch(Exception e){

               }
           }

       });
        animation_layout=(RelativeLayout)rootView.findViewById(R.id.settings_animation);

        animationInit();

//        //This is for search item details api
//        postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_RESULTS_SEARCHITEM_SPECIFIC_URL,
//                getjsonObject(TravexApplication.GET_SEARCHITEM_TAG), TravexApplication.REQUEST_ID_GET_SEARCH_ITEM);



//        if (savedInstanceState != null) {
//            mCurrentTransitionEffect = savedInstanceState.getInt(KEY_TRANSITION_EFFECT, JazzyHelper.HELIX);
//            setupJazziness(mCurrentTransitionEffect);
//        }
        //


       // mRecyclerView.setTransitionEffect(new SlideInEffect());

        filter_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment=FilterFragment.newInstance(activity);


                if (fragment != null) {
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_container, fragment);
                    fragmentTransaction.addToBackStack(fragment.getClass().getName());
                    fragmentTransaction.commit();
//
//                    backStateName = fragment.getClass().getName();
//                    boolean fragmentPopped = fragmentManager
//                            .popBackStackImmediate(backStateName, 0);

                }
            }
        });
//        SetAdapter();
        if(Filter==null)Filter=false;
        if(Filter==false)
        {
            animationLoading();
            Runnable run = new Runnable() {
            @Override
            public void run() {
//                        for (int j = 0; j < searchListItems.size(); j++) {
//                            if (!((searchListItems.get(j).getTitle().toString()).toLowerCase()).contains(Search_text)) {
//                                searchListItems.remove(j);
//
//                            } else {
//                                search_list.setVisibility(View.VISIBLE);
//                            }
//                        }
            postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.GET_RESULTS_GENERAL_URL,
                    getjsonObject(TravexApplication.GET_GENERAL_RESULT_SEARCH_TERM_TAG), TravexApplication.REQUEST_ID_GET_RESULTS_GENERAL);


            }
        };
            Handler handler = new Handler();
            handler.postDelayed(run,2000);

               }
        else
        {

            animationLoading();
            Runnable run = new Runnable() {
                @Override
                public void run() {
                    if(id.size()!=0) {
                        SetAdapter();
                    }else{
                        lay_srchnotfound.setVisibility(View.VISIBLE);
                    }

                }
            };
            Handler handler = new Handler();
            handler.postDelayed(run,2000);

        }

        alfabet_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if(count%2==0) {
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                        dp35,dp80
                );

                params.setMargins(0, 0, dp30, 0);
                params.addRule(RelativeLayout.CENTER_IN_PARENT);
                params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

                alfabet_image.setLayoutParams(params);

                recyclerView_alphabet.setVisibility(View.VISIBLE);

            }else{
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                        dp35,dp80
                );
                params.addRule(RelativeLayout .CENTER_IN_PARENT);
                params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                params.setMargins(0, 0, 0, 0);
                alfabet_image.setLayoutParams(params);
                recyclerView_alphabet.setVisibility(View.GONE);
            }
            count++;
            }
        });
        return rootView;
    }

    public void ChangeToolBarDesign()
    {
        //To change the current design of the Toolbar in this fragment
        try {
            Toolbar toolbar = (Toolbar)activity.findViewById(R.id.tool_bar);
            TextView textToolHeader = (TextView) toolbar.findViewById(R.id.tv_category);
            textToolHeader.setText(MapShowingFragment.selected_category);
            textToolHeader.setVisibility(View.VISIBLE);
            SimpleDraweeView image = (SimpleDraweeView)toolbar.findViewById(R.id.networkImageViewEqWNH);
            image.setVisibility(View.GONE);

            toolbar.setNavigationIcon(R.drawable.ic_back_small);
        }
        catch(Exception ex)
        {
            
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                Toast.makeText(activity,"Back",Toast.LENGTH_SHORT).show();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public void initiating_APICredentials()
    {
        mJsonObjectMaker = new JsonObjectMaker(activity);
        mListCreatorForNameValuePairRequest = new ListCreatorForNameValuePairRequest(activity);
        mNetworkManager = NetworkManager.getSingleInstance(activity);
        mNetworkManager.setOnGetResponseSearchGeneralResultsListener(this);
    }

    public void initialization(View view) {
        final float scale = getResources().getDisplayMetrics().density;
        dp35 = (int) (35 * scale + 0.5f);
        dp30 = (int) (30 * scale + 0.5f);
        dp80 = (int) (80 * scale + 0.5f);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.my_recycler_view);
        recyclerView_alphabet = (RecyclerView) rootView.findViewById(R.id.recyclerView_alphabet);
        mRecyclerView.setHasFixedSize(true);
        GridLayoutManager mLayoutManager = new GridLayoutManager(activity, 1);
        mRecyclerView.setLayoutManager(mLayoutManager);
        advertise_img=(SimpleDraweeView)rootView.findViewById(R.id.advertisement_icon);
        LinearLayoutManager l_manager=new LinearLayoutManager(activity);
        recyclerView_alphabet.setLayoutManager(l_manager);
        alphabet_list=new ArrayList<String>();
        alphabet_list.add("A");
        alphabet_list.add("B");
        alphabet_list.add("C");
        alphabet_list.add("D");
        alphabet_list.add("E");
        alphabet_list.add("F");
        alphabet_list.add("G");
        alphabet_list.add("H");
        alphabet_list.add("I");
        alphabet_list.add("J");
        alphabet_list.add("K");
        alphabet_list.add("L");
        alphabet_list.add("M");
        alphabet_list.add("N");
        alphabet_list.add("O");
        alphabet_list.add("P");
        alphabet_list.add("Q");
        alphabet_list.add("R");
        alphabet_list.add("S");
        alphabet_list.add("T");
        alphabet_list.add("U");
        alphabet_list.add("V");
        alphabet_list.add("W");
        alphabet_list.add("X");
        alphabet_list.add("Y");
        alphabet_list.add("Z");

        adapter_alphabet = new AlphabetAdapter(activity,alphabet_list,country_id);
        recyclerView_alphabet.setAdapter(adapter_alphabet);
        filter_img=(ImageView)rootView.findViewById(R.id.filter_image);
        alfabet_image=(ImageView)rootView.findViewById(R.id.alfabet_image);
        lay_srchnotfound=(RelativeLayout)rootView.findViewById(R.id.lay_srchnotfound);

    }

    public void SetAdapter()
    {
//        id.add("1");
//        Title.add("New");
//        Latitude.add("1.00");
//        Longitude.add("1.00");
//        Subtitle.add("extra");
//        Location.add("1");
//        Review.add("1");
//        Images.add("1");
//        Videos.add("1");
//        Description.add("kollam");
//        Website.add("jkjbc");
//        ImageUrl_List.add("http://assets3.parliament.uk/iv/main-large//ImageVault/Images/id_7382/scope_0/ImageVaultHandler.aspx.jpg::http://wallpapershdfine.com/wp-content/gallery/hd-images-of-tiger/tigers-wallpapers-hd.jpg");
//        phone_list.add("1655445::1655445::1655445::1655445::1655445::1655445::1655445::1655445::1655445::1655445::1655445");
//
//        id.add("1");
//        Title.add("New");
//        Latitude.add("1.00");
//        Longitude.add("1.00");
//        Subtitle.add("extra");
//        Location.add("1");
//        Review.add("1");
//        Images.add("1");
//        Videos.add("1");
//        Description.add("kollam");
//        Website.add("jkjbc");
//        ImageUrl_List.add("http://assets3.parliament.uk/iv/main-large//ImageVault/Images/id_7382/scope_0/ImageVaultHandler.aspx.jpg::http://wallpapershdfine.com/wp-content/gallery/hd-images-of-tiger/tigers-wallpapers-hd.jpg");
//        phone_list.add("1655445::1655445::1655445::1655445::1655445::1655445::1655445::1655445::1655445::1655445::1655445");
        animation_layout.setVisibility(View.GONE);
        adapter = new SearchResultAdapter(activity,id,Title,Latitude,Longitude,Subtitle,Location,Review,Images,Videos,Subtitle,Website,ImageUrl_List,phone_list);
        mRecyclerView.setAdapter(adapter);

//        jazzyScrollListener = new JazzyRecyclerViewScrollListener();
//        mRecyclerView.setOnScrollListener(jazzyScrollListener);

    }

    public JSONObject getjsonObject(String tag) {
        if (tag.equals(TravexApplication.GET_GENERAL_RESULT_SEARCH_TERM_TAG)) {

            return getJson(mListCreatorForNameValuePairRequest.getListOfGeneralResultsSearchApi
                    (TravexApplication.GET_GENERAL_RESULT_CATEGORY,
                            TravexApplication.GET_GENERAL_RESULT_CITYID,
                            TravexApplication.GET_GENERAL_RESULT_AREA,
                            TravexApplication.GET_GENERAL_RESULT_SEARCHTERM,
                            TravexApplication.GET_GENERAL_RESULT_ALPHABET,
                            TravexApplication.GET_GENERAL_RESULT_START,
                            TravexApplication.GET_GENERAL_RESULT_LIMIT)
                    , mListCreatorForNameValuePairRequest.getListOfGeneralResultsSearchApi
                    (selected_category_name, country_id, "", suggestion_id, searchterm, "", ""));
        }
        return null;
        }


    public void postJsonRequest(int request_type, String url, JSONObject jsonObject, final int request_id) {

        mNetworkManager.postJsonRequest(request_type, url, jsonObject, request_id);
    }

    public JSONObject getJson(ArrayList<String> namePair, ArrayList<String> valuePair) {


        return mJsonObjectMaker.getJson(namePair, valuePair);
    }

    @Override
    public void onGetResponseSearchResultsGeneral(boolean status, HashMap<Integer, ArrayList<String>> jsonObjectItems, HashMap<Integer, HashMap<Integer, ArrayList<String>>> jsonObjectArrayItems,String advertisement_img,String advertisement_url) {
        if (status) {



            if(!advertisement_img.equals("")) {
                advertise_img.setVisibility(View.VISIBLE);
                 adv_image_name = advertisement_img;
                 adv_image_url = advertisement_url;
                Uri img_uri;
                String image_url = AppConstants.GET_RESULTS_GENERAL_IMAGES_URL + "/advertisement/mobile/" + adv_image_name;
                img_uri = Uri.parse(image_url);
                ImageRequest request = ImageRequest.fromUri(img_uri);
                DraweeController controller = Fresco.newDraweeControllerBuilder()
                        .setImageRequest(request)
                        .setOldController(advertise_img.getController()).build();
                advertise_img.setController(controller);
            }
            id = new ArrayList<>();
            Title = new ArrayList<>();
            Latitude = new ArrayList<>();
            Longitude = new ArrayList<>();
            Subtitle = new ArrayList<>();
            Location = new ArrayList<>();
            Review = new ArrayList<>();
            Images = new ArrayList<>();
            Videos = new ArrayList<>();
            Description = new ArrayList<>();
            Website = new ArrayList<>();
            ImageUrl_List = new ArrayList<>();
            phone_list = new ArrayList<>();

            LogUtility.Log(TAG, "onGetResponseSearchResultsGeneral:true", null);
            HashMap<Integer, ArrayList<String>> result_details = jsonObjectItems;

            for (int i = 0; i < result_details.size(); i++) {
                ArrayList<String> list = result_details.get(i);
//                if (map_details != null) {
//                    for (int j = 0; j < map_details.size(); j++) {
//                        ArrayList<String> list = map_details.get(j);
                int NoOfImages=0,NoOfVideos=0;
                if (list != null) {
                    for (int k = 0; k < list.size(); k++) {
                        id.add(list.get(0));
                        if(!id.equals(null))
                        {
                            Title.add(list.get(1));
                            Latitude.add(list.get(2));
                            Longitude.add(list.get(3));
                            Subtitle.add(list.get(4));
                            //Location = list.get(5);
                            Location.add(list.get(5));
                            Review.add(list.get(6));
                            Images.add(list.get(7));
                            Videos.add(list.get(8));
                            Description.add(list.get(9));
                            Website.add(list.get(10));
                            break;
                            //Fill_Details(Type, Title, Details);
                        }
                        else
                        {
                            Toast.makeText(activity, "Search Result is Empty",
                                    Toast.LENGTH_SHORT).show();
                        }




                        //if(list.get(k))
                        LogUtility.Log(TAG, "onContactUsResponse:map details:", "list item:" + list.get(k));
                    }
                } else {
                    LogUtility.Log(TAG, "onContactUsResponse: :success:list is null", null);

                }
            }


            HashMap<Integer, HashMap<Integer, ArrayList<String>>> result_details2 = jsonObjectArrayItems;

            for (int i = 0; i < result_details2.size(); i++) {
                HashMap<Integer, ArrayList<String>> list_details = result_details2.get(i);

                for (int j = 0; j < list_details.size(); j++) {

                    ArrayList<String> phone = list_details.get(0);
                    ArrayList<String> image = list_details.get(1);

                    if(phone != null)
                    {
                        String PhoneNumber = "";
                        for(int ph=0;ph<phone.size();ph++)
                        {
                            PhoneNumber += phone.get(ph)+"::";
                        }
                        phone_list.add(PhoneNumber);
                    }
                    else
                    {
                        phone_list.add("No Numbers::");
                    }
                    if(image != null)
                    {
                        String ImagesURL = "";
                        for(int img=0;img<image.size();img++)
                        {
                            ImagesURL += image.get(img)+"::";
                        }
                        ImageUrl_List.add(ImagesURL);
                    }
                    else
                    {
                        ImageUrl_List.add("No Images::");
                    }
                    break;


                }
//                if (map_details != null) {
//                    for (int j = 0; j < map_details.size(); j++) {
//                        ArrayList<String> list = map_details.get(j);

            }

            //Setting Adapter
            if(id.size()!=0) {
                SetAdapter();
            }else{
                lay_srchnotfound.setVisibility(View.VISIBLE);
            }

        } else {

            LogUtility.Log(TAG, "onGetResponseSearchResultsGeneral:false", null);

        }

    }


//    private void setupJazziness(int effect) {
//        mCurrentTransitionEffect = effect;
//        jazzyScrollListener.setTransitionEffect(mCurrentTransitionEffect);
//    }
    public  void  animationInit(){
        rotateOne = AnimationUtils.loadAnimation(activity, R.anim.rotate_one);
        rotateOne.setAnimationListener(this);
        rotateTwo= AnimationUtils.loadAnimation(activity, R.anim.rotate_two);
        rotateTwo.setAnimationListener(this);
        rotateThree= AnimationUtils.loadAnimation(activity, R.anim.rotate_three);
        rotateThree.setAnimationListener(this);
    }
    public void animationLoading(){
        animation_layout.setVisibility(View.VISIBLE);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                ImageView iv1 = (ImageView) rootView.findViewById(R.id.settings_one);
                ImageView iv2 = (ImageView) rootView.findViewById(R.id.settings_two);
                ImageView iv3 = (ImageView) rootView.findViewById(R.id.settings_three);

                iv1.setAnimation(rotateOne);
                iv2.setAnimation(rotateTwo);
                iv3.setAnimation(rotateThree);

                iv1.startAnimation(rotateOne);
                iv2.startAnimation(rotateTwo);
                iv3.startAnimation(rotateThree);
            }
        };
        Handler mHandler = new Handler();
        mHandler.postDelayed(runnable, 100);

    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}

