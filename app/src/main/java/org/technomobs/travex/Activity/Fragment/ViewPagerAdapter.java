package org.technomobs.travex.Activity.Fragment;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;

import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;

import java.util.ArrayList;

/**
 * Created by Abhi on 01-03-2016.
 */
public class ViewPagerAdapter extends PagerAdapter {
    int size;
    Activity act;
    View layout;
    TextView pagenumber1, pagenumber2, pagenumber3, pagenumber4, pagenumber5;
    ImageView pageImage;
    Button click;
    PreferenceManager mPreferenceManager = null;
    ArrayList<String> Title_List=new ArrayList<String>();
    ArrayList<String> Description_List=new ArrayList<String>();
    ArrayList<String> Image_URL_List=new ArrayList<String>();

    public ViewPagerAdapter(Activity mainActivity, ArrayList<String> Title,ArrayList<String> Description,ArrayList<String> Image_URL) {
        // TODO Auto-generated constructor stub
        Title_List = Title;
        Description_List = Description;
        Image_URL_List = Image_URL;
        act = mainActivity;
        mPreferenceManager = new PreferenceManager(act);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return Title_List.size();
    }

    @Override
    public Object instantiateItem(View container, int position) {
        // TODO Auto-generated method stub
        LayoutInflater inflater = (LayoutInflater) act
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layout = inflater.inflate(R.layout.fragment_one_layout, null);

        SimpleDraweeView mSplashAnimation= (SimpleDraweeView)layout.findViewById(R.id.sdvImage);
        TextView head=(TextView)layout.findViewById(R.id.head);
        TextView description=(TextView)layout.findViewById(R.id.description);
        head.setText(Title_List.get(position).toUpperCase());
        description.setText(Description_List.get(position));
        String img_url = Image_URL_List.get(position);
        Uri uri = Uri.parse(img_url);

        ImageRequest request = ImageRequest.fromUri(uri);

        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setImageRequest(request)
                .setOldController(mSplashAnimation.getController()).build();
        //Log.e(TAG, "ImagePath uri " + img_uri);

        mSplashAnimation.setController(controller);
//
//        ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.pic1).build();
//        mSplashAnimation.setImageURI(uri);
//        int pagenumberTxt = position + 1;
//        if(pagenumberTxt==2){
//             imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.pic2).build();
//        }
//        if(pagenumberTxt==3){
//            imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.pic3).build();
//        }
//        if(pagenumberTxt==4){
//            imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.pic4).build();
//        } if(pagenumberTxt==5){
//            imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.pic5).build();
//        }
//        mSplashAnimation.setImageURI(imageRequest.getSourceUri());
//        //pagenumber1.setText("Now your in Page No  " +pagenumberTxt );


        ((ViewPager) container).addView(layout, 0);
        return layout;
    }
    @Override
    public void destroyItem(View arg0, int arg1, Object arg2) {
        ((ViewPager) arg0).removeView((View) arg2);
    }
    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == ((View) arg1);
    }
    @Override
    public Parcelable saveState() {
        return null;
    }

}
