package org.technomobs.travex.Activity.Fragment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.R;

import java.util.ArrayList;

/**
 * Created by oranz-pc2 on 15/3/16.
 */
public class Categorylistviewadapter extends BaseAdapter{
    Context context;
    ArrayList<CategoryListItems> categoryItemsnew;
    ViewHolder holder;
    private LayoutInflater layoutInflater;
PreferenceManager preferenceManager;
    public Categorylistviewadapter(Context context,ArrayList<CategoryListItems> categoryItemsnew){
    this.context=context;
        layoutInflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    this.categoryItemsnew=categoryItemsnew;
}
    @Override
    public int getCount() {
        return categoryItemsnew.size();
    }

    @Override
    public Object getItem(int position) {
        return categoryItemsnew.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = null ;
        preferenceManager = new PreferenceManager(context);
        try {
            v= layoutInflater.inflate(R.layout.category_adapter, null);
            holder=new ViewHolder();

            holder.icon= (SimpleDraweeView)v.findViewById(R.id.icon);
            holder.layout= (RelativeLayout) v.findViewById(R.id.rl);
            holder.txt = (TextView) v.findViewById(R.id.title);
//            holder.layout.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    int b = Integer.parseInt(v.getTag().toString());
//                    MapShowingFragment.selected_category = categoryItemsnew.get(b).getTitle();
//                    MapShowingFragment.selected_categoryId = categoryItemsnew.get(b).getId();
//                    MapShowingFragment.category_text.setText(categoryItemsnew.get(b).getTitle());
//                    MapShowingFragment.cat_list.setVisibility(View.GONE);
//                    //Toast.makeText(context,"clicked",Toast.LENGTH_LONG).show();
//                }
//            });
            holder.layout.setTag(position);
            holder.layout.setBackgroundResource(R.drawable.bottom_line_black);
            holder.txt.setText(categoryItemsnew.get(position).getTitle().toString());
            if(categoryItemsnew.get(position).getTitle().toString().equals("Airline")) {
                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_airline).build();
                holder.icon.setImageURI(imageRequest.getSourceUri());
            }else if(categoryItemsnew.get(position).getTitle().toString().equals("Shopping")) {
                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_shopping).build();
                holder.icon.setImageURI(imageRequest.getSourceUri());
            }else if(categoryItemsnew.get(position).getTitle().toString().equals("Business")) {
                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_bussiness).build();
                holder.icon.setImageURI(imageRequest.getSourceUri());
            }else if(categoryItemsnew.get(position).getTitle().toString().equals("Real Estate")) {
                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_realestate).build();
                holder.icon.setImageURI(imageRequest.getSourceUri());
            }else if(categoryItemsnew.get(position).getTitle().toString().equals("Money Exchange")) {
                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_moneyexchange).build();
                holder.icon.setImageURI(imageRequest.getSourceUri());
            }else if(categoryItemsnew.get(position).getTitle().toString().equals("Car Rentals")) {
                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_rentcar).build();
                holder.icon.setImageURI(imageRequest.getSourceUri());
            }else if(categoryItemsnew.get(position).getTitle().toString().equals("Embasies & Consulates")) {
                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_embassy).build();
                holder.icon.setImageURI(imageRequest.getSourceUri());
            }else if(categoryItemsnew.get(position).getTitle().toString().equals("Tour & Travel")) {
                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_tour_travel).build();
                holder.icon.setImageURI(imageRequest.getSourceUri());
            }else if(categoryItemsnew.get(position).getTitle().toString().equals("Events")) {
                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_events).build();
                holder.icon.setImageURI(imageRequest.getSourceUri());
            }else if(categoryItemsnew.get(position).getTitle().toString().equals("Eating Out")) {
                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_eatingout).build();
                holder.icon.setImageURI(imageRequest.getSourceUri());
            }else if(categoryItemsnew.get(position).getTitle().toString().equals("Stay/Hotels")) {
                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_stay_hotel).build();
                holder.icon.setImageURI(imageRequest.getSourceUri());
            }else if(categoryItemsnew.get(position).getTitle().toString().equals("Nightlife")) {
                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_nightlife).build();
                holder.icon.setImageURI(imageRequest.getSourceUri());
            }else if(categoryItemsnew.get(position).getTitle().toString().equals("Spa")) {
                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_spa).build();
                holder.icon.setImageURI(imageRequest.getSourceUri());
            }else if(categoryItemsnew.get(position).getTitle().toString().equals("Things To Do")) {
                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_thingstodo).build();
                holder.icon.setImageURI(imageRequest.getSourceUri());
            }else if(categoryItemsnew.get(position).getTitle().toString().equals("Emergencies")) {
                ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_emergencies).build();
                holder.icon.setImageURI(imageRequest.getSourceUri());
            }
//            if(preferenceManager.getboolean("logout")) {
//                if (categoryItemsnew.get(position).getTitle().toString().equals("Logout")) {
//                    ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_emergencies).build();
//                    holder.icon.setImageURI(imageRequest.getSourceUri());
//                }
//            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return v;
    }
    static class ViewHolder
    {

        public RelativeLayout layout;
        public TextView txt;
        SimpleDraweeView icon;
    }
}
