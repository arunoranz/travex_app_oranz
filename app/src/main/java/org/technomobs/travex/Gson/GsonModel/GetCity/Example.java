package org.technomobs.travex.Gson.GsonModel.GetCity;


import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.technomobs.travex.Utillity.LogUtility;

/**
 * Created by technomobs on 22/4/16.
 */
public class Example {
    String TAG = "Example_Get_City";
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("result")
    @Expose
    private List<Result> result = new ArrayList<Result>();

    /**
     * @return The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return The result
     */
    public List<Result> getResult() {
        return result;
    }

    /**
     * @param result The result
     */
    public void setResult(List<Result> result) {
        this.result = result;
    }
    /*{"result":[{"city_id":"pvgl_vq_1","state":"3798","longitude":"55.2708","city_tags":["dubai","Dubai"],"latitude":"25.2048","currency":"AED","country":"229","city":"Dubai"},
        {"city_id":"pvgl_vq_12","state":"3264","longitude":"2.1833","city_tags":["barcelona","Barcelona"],"latitude":"41.3833","currency":"EUR","country":"205","city":"Barcelona"},
        {"city_id":"pvgl_vq_2","state":"3796","longitude":"54.3667","city_tags":["abudhabi","Abudabhi","Abu Dhabi"],"latitude":"24.4667","currency":"AED","country":"229","city":"Abu Dhabi"},
        {"city_id":"pvgl_vq_3","state":"2715","longitude":"58.5400","city_tags":["muscat","Muccat"],"latitude":"23.6100","currency":"OMR","country":"165","city":"Muscat"}],"status":1}

   */

    @Override
    public String toString() {
        LogUtility.Log(TAG, "toString", null);
        StringBuilder mStringBuilder = new StringBuilder();

        mStringBuilder.append("Details:Get City  Api:\n");
        mStringBuilder.append("status:" + getStatus()+"\n");
        for (int i = 0; i < getResult().size(); i++) {
            mStringBuilder.append("city_id:" + getResult().get(i).getCityId()+"\n");
            mStringBuilder.append("state:" + getResult().get(i).getState()+"\n");
            mStringBuilder.append("longitude:" + getResult().get(i).getLongitude()+"\n");
            mStringBuilder.append("latitude:" + getResult().get(i).getLatitude()+"\n");
            mStringBuilder.append("country:" + getResult().get(i).getCountry()+"\n");
            mStringBuilder.append("city:" + getResult().get(i).getCity()+"\n");
            mStringBuilder.append("currency:" + getResult().get(i).getCurrency()+"\n");

            for(int k=0;k<getResult().get(i).getCityTags().size();k++)
            {
                mStringBuilder.append("city tags:" + getResult().get(i).getCityTags().get(k)+"\n");
            }
        }
        LogUtility.Log(TAG, "toString", "mStringBuilder:" + mStringBuilder.toString());


        return mStringBuilder.toString();
    }
}
