package org.technomobs.travex.Gson.GsonModel.Verification_Login;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Created by technomobs on 28/4/16.
 */
public class Result {

    @SerializedName("login_status")
    @Expose
    private String loginStatus;
    @SerializedName("member_profile_image")
    @Expose
    private String memberProfileImage;
    @SerializedName("member_updated_at")
    @Expose
    private String memberUpdatedAt;
    @SerializedName("member_email")
    @Expose
    private String memberEmail;
    @SerializedName("member_login_count")
    @Expose
    private String memberLoginCount;
    @SerializedName("member_last_login")
    @Expose
    private String memberLastLogin;
    @SerializedName("member_name")
    @Expose
    private String memberName;
    @SerializedName("member_status")
    @Expose
    private String memberStatus;
    @SerializedName("member_id")
    @Expose
    private String memberId;

    /**
     *
     * @return
     * The loginStatus
     */
    public String getLoginStatus() {
        return loginStatus;
    }

    /**
     *
     * @param loginStatus
     * The login_status
     */
    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }

    /**
     *
     * @return
     * The memberProfileImage
     */
    public String getMemberProfileImage() {
        return memberProfileImage;
    }

    /**
     *
     * @param memberProfileImage
     * The member_profile_image
     */
    public void setMemberProfileImage(String memberProfileImage) {
        this.memberProfileImage = memberProfileImage;
    }

    /**
     *
     * @return
     * The memberUpdatedAt
     */
    public String getMemberUpdatedAt() {
        return memberUpdatedAt;
    }

    /**
     *
     * @param memberUpdatedAt
     * The member_updated_at
     */
    public void setMemberUpdatedAt(String memberUpdatedAt) {
        this.memberUpdatedAt = memberUpdatedAt;
    }

    /**
     *
     * @return
     * The memberEmail
     */
    public String getMemberEmail() {
        return memberEmail;
    }

    /**
     *
     * @param memberEmail
     * The member_email
     */
    public void setMemberEmail(String memberEmail) {
        this.memberEmail = memberEmail;
    }

    /**
     *
     * @return
     * The memberLoginCount
     */
    public String getMemberLoginCount() {
        return memberLoginCount;
    }

    /**
     *
     * @param memberLoginCount
     * The member_login_count
     */
    public void setMemberLoginCount(String memberLoginCount) {
        this.memberLoginCount = memberLoginCount;
    }

    /**
     *
     * @return
     * The memberLastLogin
     */
    public String getMemberLastLogin() {
        return memberLastLogin;
    }

    /**
     *
     * @param memberLastLogin
     * The member_last_login
     */
    public void setMemberLastLogin(String memberLastLogin) {
        this.memberLastLogin = memberLastLogin;
    }

    /**
     *
     * @return
     * The memberName
     */
    public String getMemberName() {
        return memberName;
    }

    /**
     *
     * @param memberName
     * The member_name
     */
    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    /**
     *
     * @return
     * The memberStatus
     */
    public String getMemberStatus() {
        return memberStatus;
    }

    /**
     *
     * @param memberStatus
     * The member_status
     */
    public void setMemberStatus(String memberStatus) {
        this.memberStatus = memberStatus;
    }

    /**
     *
     * @return
     * The memberId
     */
    public String getMemberId() {
        return memberId;
    }

    /**
     *
     * @param memberId
     * The member_id
     */
    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }
}
