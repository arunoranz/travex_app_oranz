package org.technomobs.travex.Gson.GsonModel.Verification_Login;

/**
 * Created by technomobs on 28/4/16.
 */
public class Request_JsonObject_Tag_Login {


    String member_email=null;
    String member_password=null;

    public String getMember_email() {
        return member_email;
    }

    public void setMember_email(String member_email) {
        this.member_email = member_email;
    }

    public String getMember_password() {
        return member_password;
    }

    public void setMember_password(String member_password) {
        this.member_password = member_password;
    }
}
