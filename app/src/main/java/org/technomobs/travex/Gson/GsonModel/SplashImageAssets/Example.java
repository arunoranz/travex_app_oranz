package org.technomobs.travex.Gson.GsonModel.SplashImageAssets;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.technomobs.travex.Utillity.LogUtility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by technomobs on 22/4/16.
 */
public class Example {

    String TAG = Example.class.getSimpleName();
    @SerializedName("result")
    @Expose
    private List<Result> result = new ArrayList<Result>();
    @SerializedName("status")
    @Expose
    private Integer status;

    /**
     * @return The result
     */
    public List<Result> getResult() {
        return result;
    }

    /**
     * @param result The result
     */
    public void setResult(List<Result> result) {
        this.result = result;
    }

    /**
     * @return The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /*
        {"result":[{"cms_updated_at":"2016-01-23 16:49:38","cms_image":"flp-1.jpg","cms_content":"discovery"},
            {"cms_updated_at":"2016-01-23 16:49:47","cms_image":"flp-2.jpg","cms_content":"food"},
            "cms_updated_at":"2016-01-23 16:50:30","cms_image":"flp-3.jpg","cms_content":"culture"},
        {"cms_updated_at":"2016-01-23 16:50:30","cms_image":"flp-4.jpg","cms_content":"people"},
        {"cms_updated_at":"2016-01-23 16:49:38","cms_image":"flp-0.jpg","cms_content":"discovery_bw"}],"status":1}*/
    @Override
    public String toString() {
        LogUtility.Log(TAG, "toString", null);
        StringBuilder mStringBuilder = new StringBuilder();

        mStringBuilder.append("Details:Splash Image Assets Api:\n");
        mStringBuilder.append("status:" + getStatus());
        for (int i = 0; i < getResult().size(); i++) {
            mStringBuilder.append("cms_updated_at:" + getResult().get(i).getCmsUpdatedAt());
            mStringBuilder.append("cms_image:" + getResult().get(i).getCmsImage());
            mStringBuilder.append("cms_content:" + getResult().get(i).getCmsContent());


        }
        LogUtility.Log(TAG, "toString", "mStringBuilder:"+mStringBuilder.toString());


        return mStringBuilder.toString();
    }
}
