package org.technomobs.travex.Gson.GsonModel.FeatureImageAssets;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.technomobs.travex.Utillity.LogUtility;

/**
 * Created by technomobs on 23/4/16.
 */
public class Example {
    String TAG = "Example_feature_screen";
    @SerializedName("result")
    @Expose
    private List<Result> result = new ArrayList<Result>();
    @SerializedName("status")
    @Expose
    private Integer status;

    /**
     * @return The result
     */
    public List<Result> getResult() {
        return result;
    }

    /**
     * @param result The result
     */
    public void setResult(List<Result> result) {
        this.result = result;
    }

    /**
     * @return The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /* {"result":
         [{"cms_image":"my-plan.jpg",
         "cms_content":"myplan",
         "cms_updated_at":"2016-04-18 08:46:05",
         "cms_title":"My-Plan",
         "cms_description":"Browse through your places of interest around the city and plan your day out in an intelligent way"}],
         "status":1}

    */
    @Override
    public String toString() {


        LogUtility.Log(TAG, "toString", null);
        StringBuilder mStringBuilder = new StringBuilder();

        mStringBuilder.append("Details:Feature Image Assets Api:\n");
        mStringBuilder.append("status:" + getStatus()+"\n");
        for (int i = 0; i < getResult().size(); i++) {
            mStringBuilder.append("cms_updated_at:" + getResult().get(i).getCmsUpdatedAt()+"\n");
            mStringBuilder.append("cms_image:" + getResult().get(i).getCmsImage()+"\n");
            mStringBuilder.append("cms_content:" + getResult().get(i).getCmsContent()+"\n");
            mStringBuilder.append("cms_title:" + getResult().get(i).getCmsTitle()+"\n");
            mStringBuilder.append("cms_description:" + getResult().get(i).getCmsDescription()+"\n");


        }
        LogUtility.Log(TAG, "toString", "mStringBuilder:" + mStringBuilder.toString());


        return mStringBuilder.toString();
    }
}
