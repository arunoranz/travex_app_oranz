package org.technomobs.travex.Gson.GsonModel.SplashImageAssets;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Created by technomobs on 22/4/16.
 */
public class Result {
    @SerializedName("cms_updated_at")
    @Expose
    private String cmsUpdatedAt;
    @SerializedName("cms_image")
    @Expose
    private String cmsImage;
    @SerializedName("cms_content")
    @Expose
    private String cmsContent;

    /**
     *
     * @return
     * The cmsUpdatedAt
     */
    public String getCmsUpdatedAt() {
        return cmsUpdatedAt;
    }

    /**
     *
     * @param cmsUpdatedAt
     * The cms_updated_at
     */
    public void setCmsUpdatedAt(String cmsUpdatedAt) {
        this.cmsUpdatedAt = cmsUpdatedAt;
    }

    /**
     *
     * @return
     * The cmsImage
     */
    public String getCmsImage() {
        return cmsImage;
    }

    /**
     *
     * @param cmsImage
     * The cms_image
     */
    public void setCmsImage(String cmsImage) {
        this.cmsImage = cmsImage;
    }

    /**
     *
     * @return
     * The cmsContent
     */
    public String getCmsContent() {
        return cmsContent;
    }

    /**
     *
     * @param cmsContent
     * The cms_content
     */
    public void setCmsContent(String cmsContent) {
        this.cmsContent = cmsContent;
    }
}
