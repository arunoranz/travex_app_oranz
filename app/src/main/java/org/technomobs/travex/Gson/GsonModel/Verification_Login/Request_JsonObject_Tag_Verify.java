package org.technomobs.travex.Gson.GsonModel.Verification_Login;

/**
 * Created by technomobs on 22/4/16.
 */
public class Request_JsonObject_Tag_Verify {

    String member_id=null;
    String member_varification_code=null;

    public String getMember_id() {
        return member_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }

    public String getMember_varification_code() {
        return member_varification_code;
    }

    public void setMember_varification_code(String member_varification_code) {
        this.member_varification_code = member_varification_code;
    }
}
