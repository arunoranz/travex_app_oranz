package org.technomobs.travex.Gson;

import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import org.technomobs.travex.Activity.TravexApplication;


import org.technomobs.travex.Gson.GsonModel.FeatureImageAssets.Request_JsonObject_Tag;
import org.technomobs.travex.Gson.GsonModel.Verification_Login.Request_JsonObject_Tag_Verify;
import org.technomobs.travex.Utillity.LogUtility;

/**
 * Created by technomobs on 22/4/16.
 */
public class JsonObjectRequestCreator {

    Context mContext = null;
    String TAG = JsonObjectRequestCreator.class.getSimpleName();

    public JsonObjectRequestCreator(Context context) {
        mContext = context;
    }

    public JSONObject createRequestForSplashScreenAssets() {

        org.technomobs.travex.Gson.GsonModel.SplashImageAssets.Request_JsonObject_Tag request_jsonObject_tag = new org.technomobs.travex.Gson.GsonModel.SplashImageAssets.Request_JsonObject_Tag();
        String request = null;
        JSONObject mJsonObject = null;
        request_jsonObject_tag.setType("splashscreen");
        Gson mGson = new Gson();
        request = mGson.toJson(request_jsonObject_tag);

        try {
            mJsonObject = new JSONObject(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LogUtility.Log(TAG, "createRequestForSplashScreenAssets", "mJsonObject:" + mJsonObject);
        return mJsonObject;
    }

    public JSONObject createRequestForFeatureAssets() {

        Request_JsonObject_Tag request_jsonObject_tag = new Request_JsonObject_Tag();
        String request = null;
        JSONObject mJsonObject = null;
        request_jsonObject_tag.setType("features");
        Gson mGson = new Gson();
        request = mGson.toJson(request_jsonObject_tag);

        try {
            mJsonObject = new JSONObject(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LogUtility.Log(TAG, "createRequestForFeatureAssets", "mJsonObject:" + mJsonObject);
        return mJsonObject;
    }

    public JSONObject createRequestForRegistrationTravex(String member_first_name,
                                                         String member_email,
                                                         String member_password,
                                                         String member_device_id,
                                                         String member_device_type,
                                                         String member_auth_method) {

        org.technomobs.travex.Gson.GsonModel.Registration.Request_JsonObject_Tag request_jsonObject_tag = new org.technomobs.travex.Gson.GsonModel.Registration.Request_JsonObject_Tag();
        String request = null;
        JSONObject mJsonObject = null;
        request_jsonObject_tag.setMember_first_name(member_first_name);
        request_jsonObject_tag.setMember_email(member_email);
        request_jsonObject_tag.setMember_password(member_password);
        request_jsonObject_tag.setMember_device_id(member_device_id);
        request_jsonObject_tag.setMember_device_type(member_device_type);
        request_jsonObject_tag.setMember_auth_method(member_auth_method);


        Gson mGson = new Gson();
        request = mGson.toJson(request_jsonObject_tag);

        try {
            mJsonObject = new JSONObject(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LogUtility.Log(TAG, "createRequestForRegistrationTravex", "mJsonObject:" + mJsonObject);
        return mJsonObject;
    }

    public JSONObject createJsonObjectForVerification(String member_id, String member_varification_code) {
        Request_JsonObject_Tag_Verify mRequest_jsonObject_tag_verify = new Request_JsonObject_Tag_Verify();
        mRequest_jsonObject_tag_verify.setMember_id(member_id);
        mRequest_jsonObject_tag_verify.setMember_varification_code(member_varification_code);
        String request = null;

        Gson mGson = new Gson();
        JSONObject mJsonObject = null;
        request = mGson.toJson(mRequest_jsonObject_tag_verify);

        try {
            mJsonObject = new JSONObject(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LogUtility.Log(TAG, "createJsonObjectForVerification", "mJsonObject:" + mJsonObject);

        return mJsonObject;

    }


}
