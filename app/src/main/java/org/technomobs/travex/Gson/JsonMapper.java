package org.technomobs.travex.Gson;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Gson.GsonModel.SplashImageAssets.Example;
import org.technomobs.travex.Utillity.LogUtility;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by technomobs on 22/4/16.
 */
public class JsonMapper {
    Context mContext = null;
    String TAG = JsonMapper.class.getSimpleName();

    public JsonMapper(Context context) {

        mContext = context;
    }

    public void jsonMapperForSplashImages(String filename) {
        Example mExample_Splash_Images = null;

        String fileContent = readFile(filename);
        LogUtility.Log(TAG, "jsonMapperForSplashImages:", "fileContent:" + fileContent);
        mExample_Splash_Images = new Gson().fromJson(fileContent, Example.class);
        LogUtility.Log(TAG, "jsonMapperForSplashImages:", "status:" + mExample_Splash_Images.getStatus());

         mExample_Splash_Images.toString();
    }
    public void jsonMapperForGetCity(String filename) {
        String fileContent = readFile(filename);
        org.technomobs.travex.Gson.GsonModel.GetCity.Example mExample_get_city=null;

        LogUtility.Log(TAG, "jsonMapperForGetCity:", "fileContent:" + fileContent);
        mExample_get_city = new Gson().fromJson(fileContent, org.technomobs.travex.Gson.GsonModel.GetCity.Example.class);
        LogUtility.Log(TAG, "jsonMapperForGetCity:", "status:" + mExample_get_city.getStatus());

        mExample_get_city.toString();
    }
    public void jsonMapperForFeatureAssets(String filename) {

        org.technomobs.travex.Gson.GsonModel.FeatureImageAssets.Example mExample_feature_assets=null;
        String fileContent = readFile(filename);
        LogUtility.Log(TAG, "jsonMapperForFeatureAssets:", "fileContent:" + fileContent);
        mExample_feature_assets = new Gson().fromJson(fileContent, org.technomobs.travex.Gson.GsonModel.FeatureImageAssets.Example.class);
        LogUtility.Log(TAG, "jsonMapperForFeatureAssets:", "status:" + mExample_feature_assets.getStatus());

        mExample_feature_assets.toString();
    }
    public void jsonMapperForRegistration(String filename) {

        org.technomobs.travex.Gson.GsonModel.Registration.Example mExample_registration=null;
        String fileContent = readFile(filename);
        LogUtility.Log(TAG, "jsonMapperForRegistration:", "fileContent:" + fileContent);
        mExample_registration = new Gson().fromJson(fileContent, org.technomobs.travex.Gson.GsonModel.Registration.Example.class);
        LogUtility.Log(TAG, "jsonMapperForFeatureAssets:", "status:" + mExample_registration.getStatus());

        mExample_registration.toString();
    }
    public void jsonMapperForVerifyLogin(String filename) {

        org.technomobs.travex.Gson.GsonModel.Verification_Login.Example mExample_verify_login=null;
        String fileContent = readFile(filename);
        LogUtility.Log(TAG, "jsonMapperForVerifyLogin:", "fileContent:" + fileContent);
        mExample_verify_login = new Gson().fromJson(fileContent, org.technomobs.travex.Gson.GsonModel.Verification_Login.Example.class);
        LogUtility.Log(TAG, "jsonMapperForVerifyLogin:", "status:" + mExample_verify_login.getStatus());

        mExample_verify_login.toString();
    }


    public String readFile(String filename) {
        FileInputStream fin = null;
        int c;
        String temp = "";
        try {
            fin = mContext.openFileInput(filename);
            try {
                while ((c = fin.read()) != -1) {
                    temp = temp + Character.toString((char) c);
                }

                fin.close();
                LogUtility.Log(TAG, "readFile", "file content:" + temp);
                return temp;

            } catch (IOException e) {
                e.printStackTrace();
            }
//string temp contains all the data of the file.
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return null;

    }


}
