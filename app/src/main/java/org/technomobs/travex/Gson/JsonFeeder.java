package org.technomobs.travex.Gson;

import android.content.Context;

import org.json.JSONObject;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Utillity.LogUtility;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by technomobs on 22/4/16.
 */
public class JsonFeeder {

    Context mContext = null;
    String TAG = JsonFeeder.class.getSimpleName();

    public JsonFeeder(Context context) {

        mContext = context;
    }


    public void saveJsonIntoFile(String filename, String response, String tag) {

        if (isFileExist(filename)) {
            deleteFile(filename);
        }
        try {
            FileOutputStream mFileOutputStream = mContext.openFileOutput(filename, Context.MODE_PRIVATE);
            try {
                mFileOutputStream.write(response.getBytes());
                mFileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        if (tag.equals(TravexApplication.SPLASH_TAG)) {
            JsonMapper mJsonMapper = new JsonMapper(mContext);
            mJsonMapper.jsonMapperForSplashImages(filename);

        } else if (tag.equals(TravexApplication.GETCITY_TAG)) {
            JsonMapper mJsonMapper = new JsonMapper(mContext);
            mJsonMapper.jsonMapperForGetCity(filename);

        } else if (tag.equals(TravexApplication.FEATURE_TAG)) {
            JsonMapper mJsonMapper = new JsonMapper(mContext);
            mJsonMapper.jsonMapperForFeatureAssets(filename);

        } else if (tag.equals(TravexApplication.REGISTER_TAG)) {
            JsonMapper mJsonMapper = new JsonMapper(mContext);
            mJsonMapper.jsonMapperForRegistration(filename);

        }
        else if (
                (tag.equals(TravexApplication.VERIFY_REGISTRATION_TAG))
                ||(tag.equals(TravexApplication.LOGIN_TAG))
                ) {
            JsonMapper mJsonMapper = new JsonMapper(mContext);
            mJsonMapper.jsonMapperForVerifyLogin(filename);

        }


    }


    public boolean isFileExist(String filename) {

        File file = mContext.getFileStreamPath(filename);
        if (file.exists()) {

            LogUtility.Log(TAG, "isFileExist:file exist ,going to delete", null);
            return true;
        }
        LogUtility.Log(TAG, "isFileExist:file not exist ", null);

        return false;
    }

    public void deleteFile(String filename) {
        File dir = mContext.getFilesDir();
        File mFile = new File(dir,filename);
        boolean delete = mFile.delete();
        LogUtility.Log(TAG, "deleteFile", "delete file :is :" + delete);

        try {
            boolean d = mFile.createNewFile();
            LogUtility.Log(TAG, "deleteFile", "create file new:is :" + d);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
