package org.technomobs.travex.Gson.GsonModel.Registration;

/**
 * Created by technomobs on 22/4/16.
 */
public class Request_JsonObject_Tag {

    String member_first_name=null;
    String member_email=null;
    String member_password=null;
    String member_device_id=null;
    String member_device_type=null;
    String member_auth_method=null;

    public String getMember_first_name() {
        return member_first_name;
    }

    public void setMember_first_name(String member_first_name) {
        this.member_first_name = member_first_name;
    }

    public String getMember_email() {
        return member_email;
    }

    public void setMember_email(String member_email) {
        this.member_email = member_email;
    }

    public String getMember_password() {
        return member_password;
    }

    public void setMember_password(String member_password) {
        this.member_password = member_password;
    }

    public String getMember_device_id() {
        return member_device_id;
    }

    public void setMember_device_id(String member_device_id) {
        this.member_device_id = member_device_id;
    }

    public String getMember_device_type() {
        return member_device_type;
    }

    public void setMember_device_type(String member_device_type) {
        this.member_device_type = member_device_type;
    }

    public String getMember_auth_method() {
        return member_auth_method;
    }

    public void setMember_auth_method(String member_auth_method) {
        this.member_auth_method = member_auth_method;
    }
}
