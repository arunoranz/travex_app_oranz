package org.technomobs.travex.Gson.GsonModel.Registration;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.technomobs.travex.Utillity.LogUtility;

/**
 * Created by technomobs on 27/4/16.
 */
public class Example {
    String TAG = "Example_register";
    @SerializedName("message")
    @Expose
    private Message message;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("status")
    @Expose
    private Integer status;

    /**
     * @return The message
     */
    public Message getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(Message message) {
        this.message = message;
    }

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {

        String msg = "Example{" +
                "TAG='" + TAG + '\'' +
                ", message=" + message +
                ", id='" + id + '\'' +
                ", status=" + status +
                '}';
        LogUtility.Log(TAG, "toString:", "msg:" + msg);
        return msg;
    }
}
