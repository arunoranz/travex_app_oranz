package org.technomobs.travex.Gson.GsonModel.Verification_Login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.technomobs.travex.Utillity.LogUtility;

/**
 * Created by technomobs on 28/4/16.
 */
public class Example {

    String TAG = "Example_Verification_login";
    @SerializedName("message")
    @Expose
    private Message message;
    @SerializedName("result")
    @Expose
    private Result result;
    @SerializedName("X-API-KEY")
    @Expose
    private String XAPIKEY;
    @SerializedName("status")
    @Expose
    private Integer status;

    /**
     * @return The message
     */
    public Message getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(Message message) {
        this.message = message;
    }

    /**
     * @return The result
     */
    public Result getResult() {
        return result;
    }

    /**
     * @param result The result
     */
    public void setResult(Result result) {
        this.result = result;
    }

    /**
     * @return The XAPIKEY
     */
    public String getXAPIKEY() {
        return XAPIKEY;
    }

    /**
     * @param XAPIKEY The X-API-KEY
     */
    public void setXAPIKEY(String XAPIKEY) {
        this.XAPIKEY = XAPIKEY;
    }

    /**
     * @return The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        String msg = "Example{" +
                "message=" + message +
                ", result=" + result +
                ", XAPIKEY='" + XAPIKEY + '\'' +
                ", status=" + status +
                '}';
        LogUtility.Log(TAG, "toString:", "msg:" + msg);

        return msg;
    }
}
