package org.technomobs.travex.Facebook;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;
import org.technomobs.travex.Activity.ProfileActivity;
import org.technomobs.travex.Activity.TravexApplication;
import org.technomobs.travex.Model.AppConstants;
import org.technomobs.travex.Controller.Interface.GetFacebookLoginResponse;
import org.technomobs.travex.Controller.JsonObjectMaker;
import org.technomobs.travex.Controller.NetworkManager;
import org.technomobs.travex.Controller.NetworkOptions;
import org.technomobs.travex.Preference.PreferenceManager;
import org.technomobs.travex.Utillity.ListCreatorForNameValuePairRequest;
import org.technomobs.travex.Utillity.LogUtility;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by technomobs on 2/3/16.
 */
public class FacebookLogin implements GetFacebookLoginResponse {
    public static final String TAG = FacebookLogin.class.getSimpleName();

    Context mContext = null;
    LoginManager mLoginManager = null;
    CallbackManager mCallbackManager = null;
    TravexApplication mTravexApplication = null;
    static NetworkManager mNetworkManager = null;
    JsonObjectMaker mJsonObjectMaker = null;
    ListCreatorForNameValuePairRequest mListCreatorForNameValuePairRequest = null;
    GraphRequest graphRequest;
    Fragment mFragment = null;
    PreferenceManager mPreferenceManager = null;


    /**
     * For facebook login
     */
    String first_name = null;
    String email_social = null;
    String social_id = null;
    String device_id = null;
    String device_type = null;
    String auth_method = null;

    public FacebookLogin(Context context, Fragment fragment, LoginManager loginManager, CallbackManager callbackManager) {

        mContext = context;
        mFragment = fragment;
        mTravexApplication = TravexApplication.getInstance();
        mLoginManager = loginManager;
        mCallbackManager = callbackManager;
        mJsonObjectMaker = new JsonObjectMaker(mContext);
        mNetworkManager = NetworkManager.getSingleInstance(mContext);
        mNetworkManager.setOnGetFacebookLoginResponseListener(this);
        mListCreatorForNameValuePairRequest = new ListCreatorForNameValuePairRequest(mContext);
        mPreferenceManager = new PreferenceManager(context);


        mLoginManager.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                LogUtility.Log(TAG, "onSuccess", null);
                LogUtility.Log(TAG, "Fb Login:onSuccess", null);
                LogUtility.Log(TAG, "onSuccess:", "access token from login result:" + loginResult.getAccessToken().getToken());
                LogUtility.Log(TAG, "onSuccess:", "user id from login result:" + loginResult.getAccessToken().getUserId());

                AccessToken.setCurrentAccessToken(loginResult.getAccessToken());

                graphRequest = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        LogUtility.Log(TAG, "graphRequest:onCompleted", "object:" + object);
                        try {
                            social_id = object.getString("id");
                            email_social = object.getString("email");
                            first_name = object.getString("name");
                            device_id = mTravexApplication.getDEVICE_ID();
                            device_type = "android";
                            auth_method = "fb";


                            LogUtility.Log(TAG, "onCompleted:graphRequest",
                                    "social_id:" + social_id + ",email_social:" + email_social + "," +
                                            "first_name:" + first_name + ",device_id:" + device_id + "," +
                                            "device_type:" + device_type + ",auth_method:" + auth_method);
                            postJsonRequest(NetworkOptions.REQUEST_TYPE_POST, AppConstants.SOCIAL_LOGIN_URL, getjsonObject(TravexApplication.SOCIAL_LOGIN_FACEBOOK_TAG),
                                    TravexApplication.REQUEST_ID_SOCIAL_LOGIN_FACEBOOK);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,link,email");
                graphRequest.setParameters(parameters);
                graphRequest.executeAsync();
            }

            @Override
            public void onCancel() {
                LogUtility.Log(TAG, "onCancel", null);

            }

            @Override
            public void onError(FacebookException error) {
                LogUtility.Log(TAG, "onError", null);

            }
        });

    }

    public void loginFacebookRequest() {

        if ((AccessToken.getCurrentAccessToken() == null) && (Profile.getCurrentProfile() == null)) {
            mLoginManager.logInWithReadPermissions(mFragment, Arrays.asList("public_profile", "email"));

        }


    }

    private JSONObject getJson(ArrayList<String> namePair, ArrayList<String> valuePair) {


        return mJsonObjectMaker.getJson(namePair, valuePair);
    }


    /**
     * @param request_type
     * @param url
     * @param jsonObject
     * @param request_id
     */
    private void postJsonRequest(int request_type, String url, JSONObject jsonObject, final int request_id) {

        mNetworkManager.postJsonRequest(request_type, url, jsonObject, request_id);

    }

    private JSONObject getjsonObject(String tag) {
        if (tag.equals(TravexApplication.SOCIAL_LOGIN_FACEBOOK_TAG)) {
            return getJson(mListCreatorForNameValuePairRequest.getListofSocialLogin(TravexApplication.SOCIAL_LOGIN_NAME,
                            TravexApplication.SOCIAL_LOGIN_EMAIL,
                            TravexApplication.SOCIAL_LOGIN_ID,
                            TravexApplication.SOCIAL_LOGIN_DEVICE_ID,
                            TravexApplication.SOCIAL_LOGIN_DEVICE_TYPE,
                            TravexApplication.SOCIAL_LOGIN_MEMBER_AUTH_METHOD),
                    mListCreatorForNameValuePairRequest.getListofSocialLogin(first_name,
                            email_social,
                            social_id,
                            device_id,
                            device_type,
                            auth_method));

        }

        return null;

    }


    @Override
    public void onGetFacebookLoginResponse(boolean status) {
        if (status) {
            LogUtility.Log(TAG, "onGetFacebookLoginResponse", "status:social login  ok");
            mPreferenceManager.setLogin_Type(TravexApplication.FACEBOOK_LOGIN);
            mTravexApplication.showToast("facebook login/sign up successful");
            startProfileActivity();


        } else {
            LogUtility.Log(TAG, "onGetFacebookLoginResponse", "status:social login  not ok");
            mTravexApplication.showToast("facebook login/sign up not  successful");

        }
    }

    public void startProfileActivity() {

        Intent mIntent = new Intent(mContext, ProfileActivity.class);
        mContext.startActivity(mIntent);
        mFragment.getActivity().finish();

    }
}
