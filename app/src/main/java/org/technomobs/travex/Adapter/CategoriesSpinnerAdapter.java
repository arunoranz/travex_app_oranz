package org.technomobs.travex.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.technomobs.travex.Model.CategorySpinnerItem;
import org.technomobs.travex.Model.SpinnerCategoryViewHolder;
import org.technomobs.travex.R;

import java.util.ArrayList;

/**
 * Created by technomobs on 8/3/16.
 */
public class CategoriesSpinnerAdapter extends BaseAdapter {


    Context mContext = null;
    ArrayList<CategorySpinnerItem> list = null;
    LayoutInflater mLayoutInflater = null;
    ImageView mImageView = null;
    TextView mTextView = null;


    public CategoriesSpinnerAdapter(Context context, ArrayList<CategorySpinnerItem> categorySpinnerItems) {

        mContext = context;
        list = categorySpinnerItems;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SpinnerCategoryViewHolder mSpinnerCategoryViewHolder = null;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.spinner_categories_item_layout, parent, false);
            mImageView = (ImageView) convertView.findViewById(R.id.spinner_icon);
            mTextView = (TextView) convertView.findViewById(R.id.spinner_text);
            mSpinnerCategoryViewHolder = new SpinnerCategoryViewHolder();
            mSpinnerCategoryViewHolder.setmImageView(mImageView);
            mSpinnerCategoryViewHolder.setmTextView(mTextView);

            convertView.setTag(mSpinnerCategoryViewHolder);

        } else {


            mSpinnerCategoryViewHolder = (SpinnerCategoryViewHolder) convertView.getTag();
        }
        CategorySpinnerItem categorySpinnerItem = list.get(position);
        mSpinnerCategoryViewHolder.getmImageView().setImageResource(categorySpinnerItem.getIcon());
        mSpinnerCategoryViewHolder.getmTextView().setText(categorySpinnerItem.getText());
        return convertView;
    }
}
