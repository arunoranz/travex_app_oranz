package org.technomobs.travex.Adapter;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import org.technomobs.travex.Activity.Fragment.TravexTipsDetailsFragment;
import org.technomobs.travex.R;

import java.util.ArrayList;

/**
 * Created by oranz-pc1 on 6/5/16.
 */
public class TravexTipsCategoryAdapter extends RecyclerView.Adapter<TravexTipsCategoryAdapter.ViewHolder> {

    static Activity activity ;

    String[] category_list;
    //OnItemClickListener mItemClickListener;



    public TravexTipsCategoryAdapter(Activity act, String[] category) {
        activity = act;
        category_list = category;

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder;
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.travextipscategoryadapter_layout, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData
        //Fresco.initialize(activity);

        String category_name = category_list[position];
        holder.tv_categoryname.setText(category_name);
        holder.rel_main.setTag(category_name);
        if(category_name.equals("Airline")) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_airline).build();
            holder.sdv_categoryicon.setImageURI(imageRequest.getSourceUri());
        }else if(category_name.equals("Shopping")) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_shopping).build();
            holder.sdv_categoryicon.setImageURI(imageRequest.getSourceUri());
        }else if(category_name.equals("Business")) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_bussiness).build();
            holder.sdv_categoryicon.setImageURI(imageRequest.getSourceUri());
        }else if(category_name.equals("Real Estate")) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_realestate).build();
            holder.sdv_categoryicon.setImageURI(imageRequest.getSourceUri());
        }else if(category_name.equals("Money Exchange")) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_moneyexchange).build();
            holder.sdv_categoryicon.setImageURI(imageRequest.getSourceUri());
        }else if(category_name.equals("Car Rentals")) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_rentcar).build();
            holder.sdv_categoryicon.setImageURI(imageRequest.getSourceUri());
        }else if(category_name.equals("Embasies & Consulates")) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_embassy).build();
            holder.sdv_categoryicon.setImageURI(imageRequest.getSourceUri());
        }else if(category_name.equals("Tour & Travel")) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_tour_travel).build();
            holder.sdv_categoryicon.setImageURI(imageRequest.getSourceUri());
        }else if(category_name.equals("Events")) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_events).build();
            holder.sdv_categoryicon.setImageURI(imageRequest.getSourceUri());
        }else if(category_name.equals("Eating Out")) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_eatingout).build();
            holder.sdv_categoryicon.setImageURI(imageRequest.getSourceUri());
        }else if(category_name.equals("Stay/Hotels")) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_stay_hotel).build();
            holder.sdv_categoryicon.setImageURI(imageRequest.getSourceUri());
        }else if(category_name.equals("Nightlife")) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_nightlife).build();
            holder.sdv_categoryicon.setImageURI(imageRequest.getSourceUri());
        }else if(category_name.equals("Spa")) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_spa).build();
            holder.sdv_categoryicon.setImageURI(imageRequest.getSourceUri());
        }else if(category_name.equals("Things To Do")) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_thingstodo).build();
            holder.sdv_categoryicon.setImageURI(imageRequest.getSourceUri());
        }else if(category_name.equals("Emergencies")) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.ic_emergencies).build();
            holder.sdv_categoryicon.setImageURI(imageRequest.getSourceUri());
        }

        //viewHolder.sdv_detailsimage.setImageURI(img_uri);


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return category_list.length;
    }

//    public interface OnItemClickListener {
//        public void onItemClick(View view , int position);
//    }
//
//    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
//        this.mItemClickListener = mItemClickListener;
//    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView tv_categoryname;
        SimpleDraweeView sdv_categoryicon;
        RelativeLayout rel_main;



        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tv_categoryname = (TextView) itemLayoutView.findViewById(R.id.tv_categoryname);
            rel_main= (RelativeLayout) itemLayoutView.findViewById(R.id.rel_main);
            sdv_categoryicon= (SimpleDraweeView) itemLayoutView.findViewById(R.id.sdv_categoryicon);

            rel_main.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch(v.getId())
            {
                case R.id.rel_main:
                    String Category_Name= (String) rel_main.getTag();
                    Fragment fragment = TravexTipsDetailsFragment.newInstance(activity,Category_Name);
                    FragmentManager fragmentManager = activity.getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_container, fragment);
                    fragmentTransaction.addToBackStack(fragment.getClass().getName());
                    fragmentTransaction.commit();
                    break;
                default:
                    break;
            }
        }



    }
}
