package org.technomobs.travex.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.technomobs.travex.R;

import java.util.ArrayList;

/**
 * Created by oranz-pc1 on 5/5/16.
 */
public class TipsAdapterMain extends RecyclerView.Adapter<TipsAdapterMain.ViewHolder> {

    static Context Context ;
    ArrayList<String> tv_header_list;
    ArrayList<ArrayList> tv_details_list;
    //OnItemClickListener mItemClickListener;



    public TipsAdapterMain(Context act,ArrayList<String> tv_header, ArrayList<ArrayList> tv_details) {
        Context = act;
        tv_header_list = tv_header;
        tv_details_list = tv_details;

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder;
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tipsadaptermain_layout, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData
        //Fresco.initialize(activity);

        ArrayList<String> details = new ArrayList<>();
        details=tv_details_list.get(position);
        holder.tv_header.setText(tv_header_list.get(position));
        //LinearLayout hor_event_layout=null;
        for(int i=0;i<details.size();i++)
        {
            LinearLayout hor_event_layout = new LinearLayout(Context);
            View inflatedView = View.inflate(Context, R.layout.tipsadapter_layout,hor_event_layout);
            TextView tv_tips=(TextView)inflatedView.findViewById(R.id.tv_tips);
            tv_tips.setText(details.get(i));
            holder.lin_lay_main.addView(inflatedView);
        }
//        viewHolder.recyclerView.setLayoutManager(new LinearLayoutManager(Context));
//        TipsAdapter TipsAdapter = new TipsAdapter(Context,details);
//        viewHolder.recyclerView.setAdapter(TipsAdapter);
        //viewHolder.sdv_detailsimage.setImageURI(img_uri);


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return tv_header_list.size();
    }

//    public interface OnItemClickListener {
//        public void onItemClick(View view , int position);
//    }
//
//    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
//        this.mItemClickListener = mItemClickListener;
//    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        //RecyclerView recyclerView;
        public TextView tv_header;
        LinearLayout lin_lay_main;


        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tv_header = (TextView) itemLayoutView.findViewById(R.id.tv_header);
            lin_lay_main = (LinearLayout) itemLayoutView.findViewById(R.id.lin_lay_main);
            //recyclerView= (RecyclerView) itemLayoutView.findViewById(R.id.recyclerView);

        }




//        @Override
//        public void onClick(View v) {
//            if (mItemClickListener != null) {
//                mItemClickListener.onItemClick(v, getPosition());
//            }
//        }


    }
}
