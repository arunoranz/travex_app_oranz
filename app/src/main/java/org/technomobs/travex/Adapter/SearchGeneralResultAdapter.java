package org.technomobs.travex.Adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import org.technomobs.travex.Model.SearchGeneralResultItem;

import java.util.ArrayList;

/**
 * Created by technomobs on 8/3/16.
 */
public class SearchGeneralResultAdapter extends BaseAdapter {

    Context mContext = null;
    ArrayList<SearchGeneralResultItem> list = null;

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }
}
