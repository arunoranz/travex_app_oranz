package org.technomobs.travex.Adapter;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import org.technomobs.travex.Activity.Fragment.ArticleDetailsFragment;
import org.technomobs.travex.R;

import java.util.ArrayList;

/**
 * Created by oranz-pc1 on 5/5/16.
 */
public class TipsAdapter extends RecyclerView.Adapter<TipsAdapter.ViewHolder> {

    static Context Context ;
    ArrayList<String> tv_details_list;
    //OnItemClickListener mItemClickListener;



    public TipsAdapter(Context act, ArrayList<String> tv_details) {
        Context = act;
        tv_details_list = tv_details;

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder;
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tipsadapter_layout, null);

        // create ViewHolder

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData
        //Fresco.initialize(activity);


        viewHolder.tv_tips.setText(tv_details_list.get(position));

        //viewHolder.sdv_detailsimage.setImageURI(img_uri);


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return tv_details_list.size();
    }

//    public interface OnItemClickListener {
//        public void onItemClick(View view , int position);
//    }
//
//    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
//        this.mItemClickListener = mItemClickListener;
//    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_tips;



        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tv_tips = (TextView) itemLayoutView.findViewById(R.id.tv_tips);
        }




//        @Override
//        public void onClick(View v) {
//            if (mItemClickListener != null) {
//                mItemClickListener.onItemClick(v, getPosition());
//            }
//        }


    }
}
