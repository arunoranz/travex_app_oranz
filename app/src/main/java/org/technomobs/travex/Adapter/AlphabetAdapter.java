package org.technomobs.travex.Adapter;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.SyncStateContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.viewpagerindicator.CirclePageIndicator;

import org.technomobs.travex.Activity.Fragment.MapShowingFragment;
import org.technomobs.travex.Activity.Fragment.searchresultFragment;
import org.technomobs.travex.R;

import java.util.ArrayList;

import static android.widget.LinearLayout.*;

/**
 * Created by Abhi on 09-03-2016.
 */
public class AlphabetAdapter extends RecyclerView.Adapter<AlphabetAdapter.ViewHolder> {

    LayoutInflater mInflater;
    static Activity activity;
    ViewHolder vh;

    String country_id;
    View rootView;

    static ArrayList<String> alphabet_list = new ArrayList<>();


    //  ViewPager vpPager;
    CirclePageIndicator indicator;

    public AlphabetAdapter(Activity act, ArrayList<String> alphabet_list,String c_id) {

        this.activity = act;


        this.alphabet_list = alphabet_list;
        country_id=c_id;

        mInflater = (LayoutInflater) act
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        // create a new view
        rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.alphabet_adapter, parent, false);


        vh = new ViewHolder(rootView);
        return vh;
    }



    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {
        // return bookmarkPostId.size();
        return alphabet_list.size();
    }


    public Object getItem(int position) {
        return position;

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

//        holder.vpPager.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int p=position;
//            }
//        });

//            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(R.drawable.logo).build();
//            holder.icon.setImageURI(imageRequest.getSourceUri());
           holder. tv_alphabet.setText(alphabet_list.get(position));
                holder.tv_alphabet.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                int p=position;
                String search_string = "";
                search_string= MapShowingFragment.selected_category;
                if(search_string==null)search_string="";
                if(search_string.equals(""))
                {
                    Toast.makeText(activity,"Please choose a category",Toast.LENGTH_SHORT).show();
                }
                else
                {

//                    animationLoading();
//
//                    Runnable run = new Runnable() {
//                        @Override
//                        public void run() {
//
//                            onEnterKeyClick();
//                            animation_layout.setVisibility(View.GONE);
//                        }
//                    };
//                    Handler handler = new Handler();
//                    handler.postDelayed(run,2000);
//                    //search_list.setAdapter(adapter1);
                    //suggestion_id = searchResultsIDArrayList.get(position).toString();


                    Fragment fragment= searchresultFragment.newInstance(activity, "", country_id,alphabet_list.get(position));


                    if (fragment != null) {
                        FragmentManager fragmentManager = activity.getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.frame_container, fragment);
                        fragmentTransaction.addToBackStack(fragment.getClass().getName());
                        fragmentTransaction.commit();
                    }
                }

            }
        });

    }


    public static class ViewHolder extends RecyclerView.ViewHolder  {

        public TextView tv_alphabet;



        public ViewHolder(View v) {
            super(v);
            //icon= (SimpleDraweeView)v.findViewById(R.id.sdvImage);

            tv_alphabet = (TextView) v.findViewById(R.id.tv_alphabet);




        }




    }



}
