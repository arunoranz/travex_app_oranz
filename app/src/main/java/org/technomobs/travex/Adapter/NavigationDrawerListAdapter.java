package org.technomobs.travex.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import org.technomobs.travex.Model.NavigationDrawerListItem;
import org.technomobs.travex.Model.ViewHolder;
import org.technomobs.travex.R;
import org.technomobs.travex.Utillity.LogUtility;

import java.util.ArrayList;

/**
 * Created by technomobs on 4/3/16.
 */
public class NavigationDrawerListAdapter extends BaseAdapter {
    Context mContext;
    ArrayList<NavigationDrawerListItem> mArrayList_container = null;
    public static final String TAG = NavigationDrawerListAdapter.class.getSimpleName();
    LayoutInflater mLayoutInflater;

    public NavigationDrawerListAdapter(Context mContext, ArrayList<NavigationDrawerListItem> mArrayList_container) {
        this.mArrayList_container = mArrayList_container;
        this.mContext = mContext;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {

        LogUtility.Log(TAG, "getCount:", "count:" + mArrayList_container.size());
        return mArrayList_container.size();
    }

    @Override
    public Object getItem(int position) {
        LogUtility.Log(TAG, "getItem:", "item:" + mArrayList_container.get(position));

        return mArrayList_container.get(position);
    }

    @Override
    public long getItemId(int position) {
        LogUtility.Log(TAG, "getItemId:", "item id:" + position);

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LogUtility.Log(TAG, "getView:", null);

        TextView mTextView_title = null;
        SimpleDraweeView mSimpleDraweeView = null;
        ViewHolder mViewHolder = null;
        if (convertView == null) {

            LogUtility.Log(TAG, "getView:convertView is null", null);
            convertView = mLayoutInflater.inflate(R.layout.drawer_list_item, parent, false);
            LogUtility.Log(TAG, "getView:", "convertview:" + convertView);
            mTextView_title = (TextView) convertView.findViewById(R.id.title_profile);
            LogUtility.Log(TAG, "getView", "mTextView_title:" + mTextView_title);
            mSimpleDraweeView = (SimpleDraweeView) convertView.findViewById(R.id.icon_profile);
            LogUtility.Log(TAG, "getView", "mSimpleDraweeView:" + mSimpleDraweeView);
            mViewHolder = new ViewHolder();
            mViewHolder.setmTextView_title(mTextView_title);
            mViewHolder.setmSimpleDraweeView_icon(mSimpleDraweeView);
            convertView.setTag(mViewHolder);

        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
            LogUtility.Log(TAG, "getView:convertView is not  null", "mViewHolder:" + mViewHolder);

        }
        NavigationDrawerListItem item = mArrayList_container.get(position);
        mViewHolder.getmTextView_title().setText(item.getTitle());
        mViewHolder.getmSimpleDraweeView_icon().setBackgroundResource(R.drawable.profile_sample);

       /* if (item.isIconVisible()) {
            //renderImage(mViewHolder.getmSimpleDraweeView_icon(), item.getIcon());

        } else {
            mViewHolder.getmSimpleDraweeView_icon().setVisibility(View.GONE);
        }*/

    /* String title_value=   mViewHolder.getmTextView_title().getText().toString();
        LogUtility.Log(TAG,"getView","title:"+title_value); */

        return convertView;
    }

    public void renderImage(SimpleDraweeView simpleDraweeView, int id) {
        LogUtility.Log(TAG, "renderImage:", "id:"+id);
        ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithResourceId(id).build();
        LogUtility.Log(TAG, "renderImage:", "imageRequest:"+imageRequest);
        LogUtility.Log(TAG, "renderImage:", "imageRequest:uri"+imageRequest.getSourceUri());

        simpleDraweeView.setImageURI(imageRequest.getSourceUri());

    }

}
